﻿/* Copyright (c) <2019> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */


using System.Buffers;
using System.Net.Sockets;
using Z.Collections.Generic;
using Z.Net.ByteExchange;

namespace Z.Net.AsyncSocket;

/// <summary>Infinite Object Pool to store and reuse objects</summary>
public class SocketEventArgPool : DisposableZ
{
    #region Fields / Properties
    ///<summary>Async Socket Config to be used for state object generation</summary>
    public virtual SocketConfig Config { get; set; }

    ///<summary>Eventhandler to handle the complete event (Also allows deregistration of such events when set, on dispose)</summary>
    public virtual EventHandler<SocketAsyncEventArgs>? Handler { get; }
    ///<summary>Internal Infinite Pool</summary>
    public virtual CachePool<SocketAsyncEventArgs> Pool { get; }
    ///<summary>Internal Infinite Pool</summary>
    public virtual CachePool<SocketAsyncEventArgs> PoolData { get; }
    #endregion

    #region Constructors
    /// <summary>StateObject constructor.</summary>
    /// <param name="config">Async Socket Config to be used for state object generation</param>
    /// <param name="handler">
    ///     Eventhandler to handle the complete event (Also allows deregistration of such events when set, on
    ///     dispose)
    /// </param>
    public SocketEventArgPool(SocketConfig config, EventHandler<SocketAsyncEventArgs>? handler = null) {
        Config = config;
        var maxLength = Config.Socket.MaxBufferPoolSize;
        Pool = new CachePool<SocketAsyncEventArgs>(maxLength);
        PoolData = new CachePool<SocketAsyncEventArgs>(maxLength);
        Handler = handler;

        Pool.FuncCreate = PoolData.FuncCreate = FuncNew;
        Pool.FuncClear = PoolData.FuncClear = FuncClear;
        //Pool.FuncCache = PoolData.FuncCache = funcCache;
    }
    #endregion

    private SocketAsyncEventArgs FuncNew() {
        var rval = new SocketAsyncEventArgs();
        //if (Handler != null) rval.Completed += Handler;
        return rval;
    }
    private void FuncClear(SocketAsyncEventArgs item) {
        if (item == null) return;
        if (Handler != null) item.Completed -= Handler;
        if (item.Buffer != null) ArrayPool<byte>.Shared.Return(item.Buffer);
        item.SetBuffer(null, 0, 0);
        if (item.UserToken is IDisposable state) {
            state?.Dispose();
            //state = null;
        }
        item.UserToken = null;
    }

    /// <inheritdoc />
    public virtual SocketAsyncEventArgs Pop() {
        var rVal = Pool.Pop();
        if (_disposed) return rVal;
        if (Handler != null) rVal.Completed += Handler;
        return rVal;
    }

    /// <inheritdoc />
    public virtual SocketAsyncEventArgs PopData(IByteExchanger node, ExchangeConnector connector) {
        var rVal = PoolData.Pop();
        if (_disposed) return rVal;
        if (Handler != null) rVal.Completed += Handler;
        var state = rVal.UserToken as ExchangePacket ?? new ExchangePacket(node, connector, true);
        state.Connector = connector;
        if (rVal.MemoryBuffer.IsEmpty) rVal.SetBuffer(ArrayPool<byte>.Shared.Rent(Config.Packet.BufferSize));
        rVal.UserToken = state;
        return rVal;
    }

    /// <inheritdoc />
    public virtual void Push(SocketAsyncEventArgs item) {
        if (_disposed) return;
        if (item == null) return;
        if (Handler != null) item.Completed -= Handler;
        if (item.UserToken is ExchangePacket state) {
            state.Clear();
            PoolData.Push(item);
        } else {
            if (item.UserToken is IDisposable itemD) itemD.Dispose();
            item.UserToken = null;
            item.SetBuffer(null, 0, 0);
            Pool.Push(item);
        }
    }

    /// <inheritdoc />
    public virtual void Clear() {
        if (_disposed) return;
        //ReleaseHandlers
        Pool?.Clear(true);
        PoolData?.Clear(true);
    }

    #region Dispose
    /// <summary>Dispose inner function</summary>
    protected override ValueTask DisposeAsync(bool disposing) {
        if (!_disposed) {
            if (disposing && Pool != null) // Dispose managed resources.
            {
                Clear();
                // Pool = null;
                // PoolData = null;
            }
            // Call the appropriate methods to clean up
            // unmanaged resources here.
        }
        return ValueTask.CompletedTask;
    }
    #endregion Dispose
}