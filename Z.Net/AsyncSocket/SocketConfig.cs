﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using Z.Collections.Specialized;
using Z.Net.ByteExchange;

namespace Z.Net.AsyncSocket;

/// <summary>Async Socket configuration</summary>
[Serializable]
public class SocketConfig : ExchangeConfig
{
    #region Constants / Static Fields
    /// <summary>Set deault values</summary>
    protected const string DEFAULT_IP = "127.0.0.1";
    #endregion

    #region Fields / Properties
    /// <summary>Socket Server Config Parameters</summary>
    public ConfigServer Server { get; protected set; }
    /// <summary>Socket Config Parameters</summary>
    public ConfigSocket Socket { get; protected set; }
    #endregion

    #region Constructors
    /// <summary>Async Socket configuration</summary>
    public SocketConfig(EndPoint remote, bool isServer = false, bool isStream = false, bool useSSL = false) : this(isServer, isStream, useSSL, remote) { }
    /// <summary>Async Socket configuration</summary>
    public SocketConfig(int port, bool isStream = false, bool useSSL = false) : this(true, isStream, useSSL, new IPEndPoint[] { new IPEndPoint(IPAddress.Any, port) }) { } //GetHostAddresses(port)
    /// <summary>Async Socket configuration</summary>
    public SocketConfig(string ip, int port, bool isServer = false, bool isStream = false, bool useSSL = false) : this(isServer, isStream, useSSL, new IPEndPoint(IPAddress.Parse(ip), port)) { }
    /// <summary>Async Socket configuration</summary>
    public SocketConfig(IPAddress ip, int port, bool isServer = false, bool isStream = false, bool useSSL = false) : this(isServer, isStream, useSSL, new IPEndPoint(ip, port)) { }
    /// <summary>Async Socket configuration</summary>
    public SocketConfig(IEnumerable<EndPoint> ip) : this(true, false, false, ip.ToArray()) { }
    /// <summary>Async Socket configuration</summary>
    public SocketConfig(bool isServer, bool isStream, bool useSSL, params EndPoint[] ip) : base(isServer, isStream, useSSL, ip) {
        Socket = new ConfigSocket(this);
        Server = new ConfigServer(this);
        if (isStream || useSSL) Socket.Protocol = ProtocolType.Tcp;
    }
    #endregion

    #region GetHostAddresses()
    /// <summary>Creates IPEndPoints for current host and specified port</summary>
    public static IPEndPoint[] GetHostAddresses(int port) {
        var foundV4 = new List<IPEndPoint>();
        //var foundV6 = new List<IPEndPoint>();

        NetworkInterface.GetAllNetworkInterfaces().ToList().ForEach(ni => {
            //if (ni.GetIPProperties().GatewayAddresses.FirstOrDefault() != null)
            {
                ni.GetIPProperties().UnicastAddresses.ToList().ForEach(ua => {
                    if (ua.Address.AddressFamily == AddressFamily.InterNetwork) foundV4.Add(new IPEndPoint(ua.Address, port));
                    //if (ua.Address.AddressFamily == AddressFamily.InterNetworkV6) foundV6.Add(new IPEndPoint(ua.Address, port));
                });
            }
        });

        //return (foundV4.Distinct().ToList(), foundV6.Distinct().ToList());
        return foundV4.ToArray();
        // try {
        //     var a = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
        //     var rval = new IPEndPoint[a.Length];
        //     var i = 0;
        //     foreach (var ip in a) rval[i++] = new IPEndPoint(ip, port);
        //     return rval;
        // } catch {
        //     var rval = new IPEndPoint[] {new IPEndPoint(IPAddress.Parse(DEFAULT_IP), port)};
        //     return rval;
        // }
    }
    #endregion

    #region Config / Create
    /// <summary>FluentConfig</summary>
    public SocketConfig Config(Action<SocketConfig> configAction) {
        configAction(this);
        return this;
    }
    #endregion

    #region Nested type: ConfigServer
    /// <summary>Async Socket Limits configuration</summary>
    [Serializable]
    public class ConfigServer : ExchangeConfigSubClass
    {
        #region Fields / Properties
        /// <summary>IP Ranges allowed to connect for a server</summary>
        public IPRangeCollection IpRanges { get; set; }
        /// <summary>Number of waiting connections to connect before accept is called to a server (Default = 100)</summary>
        public int ListenerBackLog { get; set; }
        /// <summary>Number of simultaneous connections allowed to connect to a server (-1 = NoLimit)</summary>
        public int MaxSimultaneousConnections { get; set; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConfigServer(ExchangeConfig cfg) : base(cfg) {
            IpRanges = new IPRangeCollection();
            MaxSimultaneousConnections = -1;
            ListenerBackLog = 100;
        }
        #endregion

        /// <summary>Checks for specified limits in this config</summary>
        internal bool CheckAccessForClient(IByteExchanger server, IPEndPoint remotePoint) {
            if (remotePoint == null) return false;
            if (IpRanges.Count > 0 && !IpRanges.Contains(remotePoint.Address)) return false;
            if (MaxSimultaneousConnections >= 0 && server.Remotes.Count > MaxSimultaneousConnections - 1) return false;
            return true;
        }
    }
    #endregion

    #region Nested type: ConfigSocket
    /// <summary>Async Socket Limits configuration</summary>
    [Serializable]
    public class ConfigSocket : ExchangeConfigSubClass
    {
        #region Fields / Properties
        private ProtocolType _protocol = ProtocolType.Tcp;
        /// <summary>Address Family to use (if applicable)</summary>
        public AddressFamily AddressFamily { get; set; }
        /// <summary>IP Ranges allowed to connect for a server</summary>
        public IPRangeCollection IpRanges { get; set; } = new IPRangeCollection();
        /// <summary>Number of Buffers objects to keep in cache</summary>
        public int MaxBufferPoolSize { get; set; } = 100;
        /// <summary>Protocol to be used (if applicable)</summary>
        public ProtocolType Protocol { get => _protocol; set => SetProtocol(value); }
        /// <summary>Socket Type</summary>
        public SocketType SocketType { get; set; }
        /// <summary>Allow nat traversal</summary>
        public bool AllowNetTraversal { get; set; }
        /// <summary>Socket Options that are set to true</summary>
        protected internal IList<SocketOption> Options { get; set; } = new List<SocketOption>();
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConfigSocket(ExchangeConfig cfg) : base(cfg) {
            Protocol = ProtocolType.Tcp;
        }
        #endregion

        private void SetProtocol(ProtocolType value) {
            _protocol = value;
            switch (value) {
                case ProtocolType.Tcp:
                    if (AddressFamily != AddressFamily.InterNetworkV6) AddressFamily = AddressFamily.InterNetwork;
                    SocketType = SocketType.Stream;
                    Cfg.IsConnectionless = false;
                    break;
                case ProtocolType.Udp:
                    if (AddressFamily != AddressFamily.InterNetworkV6) AddressFamily = AddressFamily.InterNetwork;
                    SocketType = SocketType.Dgram;
                    Cfg.IsConnectionless = true;
                    break;
                case ProtocolType.Icmp:
                case ProtocolType.Igmp:
                    if (AddressFamily != AddressFamily.InterNetworkV6) AddressFamily = AddressFamily.InterNetwork;
                    SocketType = SocketType.Raw;
                    Cfg.IsConnectionless = true;
                    break;
                case ProtocolType.IcmpV6:
                    AddressFamily = AddressFamily.InterNetworkV6;
                    SocketType = SocketType.Raw;
                    Cfg.IsConnectionless = false;
                    break;
            }
        }
        /// <summary>AddSocketOption</summary>
        public void AddOption(SocketOptionName name, object value) => Options.Add(new SocketOption(name, value));

        /// <summary>Applies current config to a stream</summary>
        protected internal void ApplySocketConfigTo(Socket socket) {
            if (AllowNetTraversal) {
                if (OperatingSystem.IsWindows()) socket.SetIPProtectionLevel(IPProtectionLevel.Unrestricted);
                else socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.IPProtectionLevel, 10);
            }
            foreach (var opt in Options) {
                if (opt.Value is bool optBool)
                    socket.SetSocketOption(opt.Level, opt.Name, optBool);
                else if (opt.Value is byte[] optByte)
                    socket.SetSocketOption(opt.Level, opt.Name, optByte);
                else if (opt.Value is int optInt)
                    socket.SetSocketOption(opt.Level, opt.Name, optInt);
                else
                    socket.SetSocketOption(opt.Level, opt.Name, opt.Value);
            }
        }

        #region Nested type: SocketOption
        /// <summary>SocketOption</summary>
        protected internal class SocketOption
        {
            #region Fields / Properties
            /// <summary>SocketOptionLevel</summary>
            public SocketOptionLevel Level { get; set; }
            /// <summary>SocketOptionName</summary>
            public SocketOptionName Name { get; set; }
            /// <summary>SocketOptionValue</summary>
            public object Value { get; set; }
            #endregion

            #region Constructors
            /// <summary>SocketOption</summary>
            public SocketOption(SocketOptionName name, object value) {
                Level = SocketOptionLevel.Socket;
                Name = name;
                Value = value;
            }
            #endregion
        }
        #endregion
    }
    #endregion

}
