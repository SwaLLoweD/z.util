/* Copyright (c) <2020> <A. Zafer YURDACALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using Z.Net.ByteExchange;

namespace Z.Net.AsyncSocket;
/// <summary>Async Socket Server for byte[] communication</summary>
public class Socketer : ByteExchanger, ISocketer
{
    #region Fields / Properties
    private readonly System.Threading.SemaphoreSlim lockSend = new(1,1);
    /// <summary>Configuration Parameters</summary>
    public new SocketConfig Config { get => (SocketConfig)base.Config; private set => base.Config = value; }
    /// <summary>SocketAsyncEventArg Pool</summary>
    protected virtual SocketEventArgPool SaePool { get; set; }
    #endregion

    #region Events
    /// <inheritdoc />
    public event EventHandler<Socket> OnSocketCreate {
        add => onSocketCreate += value;
        remove {
            if (onSocketCreate != null && value != null) onSocketCreate -= value;
        }
    }
    /// <summary>Socket Create Event</summary>
    protected EventHandler<Socket>? onSocketCreate;
    /// <inheritdoc />
    public override void DetachEvents() {
        base.DetachEvents();
        onSocketCreate = null;
    }
    #endregion

    #region Constructors
    /// <summary>Client Initializer</summary>
    /// <param name="remote">IP Endpoint of the server to bind to</param>
    /// <param name="isServer">Server node or not</param>
    public Socketer(IPEndPoint remote, bool isServer = false) : this(new SocketConfig(remote, isServer)) { }
    /// <summary>Socket Initializer</summary>
    /// <param name="ip">IP Address of the server to bind to</param>
    /// <param name="port">Port of the server to bind to</param>
    /// <param name="isServer">Server node or not</param>
    public Socketer(IPAddress ip, int port, bool isServer = false) : this(new SocketConfig(ip, port, isServer)) { }

    /// <summary>Socket Initializer</summary>
    /// <param name="ip">IP Address of the server to bind to</param>
    /// <param name="port">Port of the server to bind to</param>
    /// <param name="isServer">Server node or not</param>
    public Socketer(string ip, int port, bool isServer = false) : this(new SocketConfig(ip, port, isServer)) { }

    /// <summary>Server Initializer</summary>
    /// <param name="port">Port of the server to bind to (ips are current host ips)</param>
    public Socketer(int port) : this(new SocketConfig(port)) { }

    /// <summary>Server Initializer</summary>
    /// <param name="config">Configuration parameters</param>
    public Socketer(SocketConfig config) : base(config) {
        //Config = config;
        if ((config.Ssl.Enabled || config.IsStream) && config.Socket.SocketType != SocketType.Stream) throw new NotSupportedException("Configured sockets are not supported");
        //if (!isServer) Remotes = new EndPoint[] {config.BindAddress[0]};
        //else Remotes = new System.Collections.Concurrent.ConcurrentBag<EndPoint>();
        SaePool = new SocketEventArgPool(config, OnAsyncCompleted);

        if (config.IsConnectionless) Start();
    }
    #endregion

    #region Start Stop
    /// <inheritdoc />
    public override bool Start(int waitTimeout = -2) {
        try {
            if (HasStarted) return true;
            Remotes.Clear(); // Reset Clients
            if (Config.BindAddress.Length == 0) return false;
            if (Config.IsConnectionless) {
                if (Config.IsServer) { if (!StartUdpServer()) { return false; } } else if (!StartUdpClient()) { return false; }
            } else {
                if (Config.IsServer) { if (!StartTcpServer(waitTimeout)) { return false; } } else if (!StartTcpClient(waitTimeout)) { return false; }
            }
            return base.Start(waitTimeout);
        } catch (Exception exp) {
            //saePool.Push(sae);
            //(ListenEndPoints as List<EndPoint>).Remove(bp);
            EventCall(onError, this, new ExchangeEventArgs(null, new ByteExchangeException("Could not start listener", exp)));
            return false;
        }
    }
    /// <inheritdoc />
    protected virtual bool StartTcpServer(int waitTimeout = -2) {
        if (Config.IsServer) {
            foreach (var ipe in Config.BindAddress) {
                if (ipe == null || ipe.AddressFamily != AddressFamily.InterNetwork) continue;

                // Bind the socket to the local endpoint and listen for incoming connections.
                var socket = CreateListener(ipe);
                var connector = new ExchangeConnector(socket, ipe);
                LocalEndPoints.Add(ipe, connector);
                var sae = SaePool.Pop();
                sae.UserToken = connector;
                socket?.Socket?.AcceptAsync(sae, AcceptCallback, true);
            }
        }
        return true;
    }
    /// <inheritdoc />
    protected virtual bool StartTcpClient(int waitTimeout = -2) {
        var remote = Config.BindAddress.FirstOrDefault();
        if (remote == null) return false;
        var socket = CreateSocket();
        var connector = new ExchangeConnector(socket, remote);
        LocalEndPoints.Add(remote, connector);
        var sae = SaePool.Pop();
        sae.RemoteEndPoint = remote;
        sae.AcceptSocket = socket.Socket;

        if (socket.Socket == null) throw new Exception("Socket can not be created");
        if (waitTimeout > -2) {
            var result = socket.Socket.BeginConnect(remote, null, null);
            bool success = result.AsyncWaitHandle.WaitOne(waitTimeout, true);
            if (success) {
                socket.Socket.EndConnect(result);
                AcceptCallback(this, sae);
            } else {
                connector?.Dispose();
                throw new ByteExchangeException("Connection timedOut");//SocketException(10060); // Connection timed out.
            }
        } else {
            socket.Socket.ConnectAsync(sae, AcceptCallback, true);
        }
        return true;
    }
    /// <inheritdoc />
    protected virtual bool StartUdpServer() {
        foreach (var ipe in Config.BindAddress) {
            if (ipe == null || ipe.AddressFamily != AddressFamily.InterNetwork) continue;
            // Bind the socket to the local endpoint and listen for incoming connections.
            var socket = CreateListener(ipe);
            var connector = new ExchangeConnector(socket, ipe);
            LocalEndPoints.Add(ipe, connector);

            var sae = SaePool.PopData(this, new ExchangeConnector(new object(), new IPEndPoint(IPAddress.Any, 0)));
            sae.AcceptSocket = socket.Socket;
            sae.RemoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            socket.Socket?.ReceiveFromAsync(sae, ReceiveCallback, true);
        }
        return true;
    }
    /// <inheritdoc />
    protected virtual bool StartUdpClient() {
        var remote = Config.BindAddress[0];
        if (remote == null) return false;
        var socket = CreateSocket();
        var connector = new ExchangeConnector(socket, remote );
        LocalEndPoints.Add(remote, connector);
        Remotes.Add(remote, connector);
        var sae = SaePool.PopData(this, connector);
        sae.RemoteEndPoint = remote;
        sae.AcceptSocket = socket.Socket;
        socket.Socket?.ReceiveFromAsync(sae, ReceiveCallback, true);
        return true;
    }
    #endregion

    #region OnAsyncCompleted
    /// <summary>This method is called whenever a receive or send operation is completed on a socket</summary>
    protected virtual void OnAsyncCompleted(object? sender, SocketAsyncEventArgs e) {
        try {
            // Determine which type of operation just completed and call the associated handler
            switch (e.LastOperation) {
                case SocketAsyncOperation.Connect:
                case SocketAsyncOperation.Accept:
                    AcceptCallback(sender, e);
                    break;
                case SocketAsyncOperation.Receive:
                case SocketAsyncOperation.ReceiveFrom:
                    ReceiveCallback(sender, e);
                    break;
                case SocketAsyncOperation.Send:
                case SocketAsyncOperation.SendTo:
                    SendCallback(sender, e);
                    break;
                default: throw new ArgumentException("The last operation completed on the socket was not a receive or send");
            }
        } catch (Exception exp) {
            EventCall(onError, this, new ExchangeEventArgs(e.RemoteEndPoint, exp));
            throw;
        }
    }
    #endregion

    #region Dispose
    /// <inheritdoc />
    protected override async ValueTask DisposeAsync(bool disposing) {
        if (disposing) {
            SaePool?.Clear();
            if (SaePool != null) await SaePool.DisposeAsync();
            //saePool = null;
        }
        await base.DisposeAsync(disposing);
    }
    #endregion

    #region Certificate operations(disabled)
    //private bool remoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) =>
    //    //if (sslPolicyErrors != SslPolicyErrors.None)
    //    //{
    //    //    WriteLog("SSL Certificate Validation Error!");
    //    //    WriteLog(certificate.Subject);
    //    //    WriteLog(sslPolicyErrors.ToString());
    //    //    return false;
    //    //}
    //    //else
    //    true;

    //private X509Certificate localCertificateSelectionCallback(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
    //{
    //    //Console.WriteLine("Client is selecting a local certificate.");
    //    //if (acceptableIssuers != null &&
    //    //    acceptableIssuers.Length > 0 &&
    //    //    localCertificates != null &&
    //    //    localCertificates.Count > 0) {
    //    //    // Use the first certificate that is from an acceptable issuer. 
    //    //    foreach (X509Certificate certificate in localCertificates) {
    //    //        string issuer = certificate.Issuer;
    //    //        if (Array.IndexOf(acceptableIssuers, issuer) != -1)
    //    //            return certificate;
    //    //    }
    //    //}
    //    //if (localCertificates != null &&
    //    //    localCertificates.Count > 0)
    //    //    return localCertificates[0];

    //    return Config.Ssl.LocalCertificate;
    //}
    // private X509Certificate LocalCertificateSelectionCallback(object? sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate? remoteCertificate,
    //     string[] acceptableIssuers) =>
    //     Config.Ssl.LocalCertificate;
    #endregion

    #region Send
    /// <inheritdoc />
    public override bool Send(ExchangePacket data, bool asyncCallback = false, bool noExceptions = true) {
        Exception? exp = null;
        //lock (this) 
        AsyncSocket? socket = null;
        EndPoint? remote = null;
        if (IsDisposed) return false;
        if (data.Connector == null) {
            exp = new ByteExchangeException($"Could not send to ({remote}), Connector is null", null, remote);
            // if (Config.IsServer) {
            //     if (Config.Socket.SocketType == SocketType.Dgram) {
            //         exp = null;
            //         try {
            //             socket = createSocket();
            //             var sae = saePool.PopData(socket);
            //             sae.RemoteEndPoint = remote;
            //             sae.AcceptSocket = socket.Socket;
            //             socket.Socket.Connect(remote);
            //             socket.Socket.ReceiveFromAsync(sae, ReceiveCallback, true);
            //             //AcceptCallback(this, sae);
            //             Clients[remote] = socket;
            //         } catch {
            //             exp = new ByteExchangeException($"Could not send to ({remote}), Socket is null", null, remote);
            //         }
            //     }
            // }
        } else {
            socket = data.Connector.Connector as AsyncSocket;
            remote = data.Connector.Remote;
            if (socket == null || socket.Socket == null || remote == null) {
                exp = new ByteExchangeException($"Could not send to ({remote}), Socket is null", null, remote);
            } else if (data.Length == 0) {
                exp = new ByteExchangeException($"Could not send to ({remote}), Data is null", null, remote);
            } else if (exp == null) {
                try {
                    //Console.WriteLine($"S{data.Length}");
                    lockSend.Lock(() => {
                        var bufferSize = Config.Packet.BufferSize;
                        if (asyncCallback) {
                            var sae = SaePool.PopData(this, data.Connector);
                            sae.RemoteEndPoint = remote;
                            sae.SetBuffer(data.ToArray()); //TODO: not using stateobject currently
                            if (Config.Socket.Protocol == ProtocolType.Udp)
                                socket.Socket.SendToAsync(sae, SendCallback, true); //udp
                            else
                                socket.Socket.SendAsync(sae, SendCallback, true);
                        } else {
                            if (Config.Socket.Protocol == ProtocolType.Udp) { //udp
                                while (data.Position < data.Length) // Should set partial flag?.
                                    data.ReadBytesDo(bufferSize, (buffer, buffercnt) => socket.Socket.SendTo(buffer[..buffercnt], SocketFlags.None, remote));
                            } else { //tcp
                                socket.Socket.Send(data.ToArray());
                            }

                            EventCall(onSend, this, new ExchangeEventArgs(remote));
                        }
                    });
                    return true;
                } catch (Exception e) {
                    exp = new ByteExchangeException($"Could not send to ({remote}), received: '{e.GetType().Name}'", e, remote);
                    //if (onError == null) {
                    //Console.WriteLine(e.ToString());
                    //}
                }
            }
        }
        EventCall(onError, this, new ExchangeEventArgs(Config.BindAddress[0], (ByteExchangeException)exp));
        Disconnect(remote);
        if (!noExceptions) throw exp;
        return false;
        //}
    }
    /// <inheritdoc />
    public override async Task<bool> SendAsync(ExchangePacket data, bool asyncCallback = false, bool noExceptions = true) {
        Exception? exp = null;
        //lock (this) 
        AsyncSocket? socket = null;
        EndPoint? remote = null;
        if (IsDisposed) return false;
        if (data.Connector == null) {
            exp = new ByteExchangeException("Could not send, Connector is null");
            // if (Config.IsServer) {
            //     if (Config.Socket.SocketType == SocketType.Dgram) {
            //         exp = null;
            //         try {
            //             socket = createSocket();
            //             var sae = saePool.PopData(socket);
            //             sae.RemoteEndPoint = remote;
            //             sae.AcceptSocket = socket.Socket;
            //             socket.Socket.Connect(remote);
            //             socket.Socket.ReceiveFromAsync(sae, ReceiveCallback, true);
            //             //AcceptCallback(this, sae);
            //             Clients[remote] = socket;
            //         } catch {
            //             exp = new ByteExchangeException($"Could not send to ({remote}), Socket is null", null, remote);
            //         }
            //     }
            // }
        } else {
            socket = (AsyncSocket)data.Connector.Connector;
            remote = data.Connector.Remote;
            if (socket == null || socket.Socket == null || data.Connector == null || remote == null) {
                exp = new ByteExchangeException($"Could not send to ({remote}), Socket is null", null, remote);
            } else if (exp == null) {
                try {
                    var bufferSize = Config.Packet.BufferSize;
                    if (asyncCallback) {
                        var sae = SaePool.PopData(this, data.Connector);
                        sae.RemoteEndPoint = remote;
                        sae.SetBuffer(data.ToArray()); //TODO: not using stateobject currently
                        if (Config.Socket.Protocol == ProtocolType.Udp)
                            socket.Socket.SendToAsync(sae, SendCallback, true); //udp
                        else
                            socket.Socket.SendAsync(sae, SendCallback, true);
                    } else {
                        await lockSend.LockAsync(async () => {
                            if (Config.Socket.Protocol == ProtocolType.Udp) { //udp
                                while (data.Position < data.Length) // Should set partial flag?.
                                    await data.ReadBytesDoAsync(bufferSize, async (buffer, buffercnt) => await socket.Socket.SendToAsync(buffer[..buffercnt], SocketFlags.None, remote));
                            } else { //tcp
                                await socket.Socket.SendAsync(data.ToArray(), SocketFlags.None);
                            }
                        });

                        EventCall(onSend, this, new ExchangeEventArgs(remote));
                    }
                    return true;
                } catch (Exception e) {
                    exp = new ByteExchangeException($"Could not send to ({remote}), received: '{e.GetType().Name}'", e, remote);
                    //if (onError == null) {
                    //Console.WriteLine(e.ToString());
                    //}
                }
            }
        }
        EventCall(onError, this, new ExchangeEventArgs(remote, (ByteExchangeException)exp));
        Disconnect(remote);
        if (!noExceptions) throw exp;
        return false;
        //}
    }

    /// <summary>Send callback method</summary>
    protected virtual void SendCallback(object? sender, SocketAsyncEventArgs sae) {
        EndPoint? remotePoint = null;
        try {
            remotePoint = sae.RemoteEndPoint;
            var bytesSent = sae.BytesTransferred;

            if (bytesSent > 0) {
                if (sae?.Buffer?.Length > bytesSent) {
                    if (Config.Socket.Protocol == ProtocolType.Udp)
                        sae.AcceptSocket?.SendToAsync(sae, SendCallback, true); //udp
                    else
                        sae.AcceptSocket?.SendAsync(sae, SendCallback, true);
                }
            }

            if (sae?.SocketError == SocketError.Success)
                SaePool.Push(sae);
            else
                throw new Exception($"Socket error: {sae?.SocketError}");

            EventCall(onSend, this, new ExchangeEventArgs(remotePoint));
        } catch (Exception e) {
            sae.Dispose();
            //saePool.Push(sae);
            if (onError == null) {
                Disconnect(remotePoint);
                //Console.WriteLine(e.ToString());
            } else {
                var exp = new ByteExchangeException($"Could not send to ({remotePoint}), received: '{e.GetType().Name}'", e, Config.BindAddress[0], null);
                EventCall(onError, this, new ExchangeEventArgs(remotePoint, exp));
            }
        }
    }
    #endregion

    #region Callbacks
    /// <summary>Accept Callback</summary>
    protected virtual void AcceptCallback(object? sender, SocketAsyncEventArgs sae) {
        if (IsDisposed) return; //This must be disposed
        EndPoint? remotePoint = null;
        AsyncSocket socket;
        ExchangeConnector? listener = null;
        ExchangeConnector? connector = null;

        try {
            if (sae == null || sae.AcceptSocket == null) throw new Exception("Sae or socket can not be null");
            socket = new AsyncSocket(sae.AcceptSocket); //Accepted socket
            remotePoint = sae.AcceptSocket?.RemoteEndPoint ?? sae.RemoteEndPoint;
            if (remotePoint == null) throw new Exception("Remote can not be null");
            connector = new ExchangeConnector(socket, remotePoint);

            if (Config.IsServer) {
                listener = sae.UserToken as ExchangeConnector;
                //listener = (sae.UserToken as StateObject<Socket>)?.Socket; //listener socket 
                //listener = listeners.Where(x => x.LocalEndPoint.ToString() == socket.LocalEndPoint.ToString()).FirstOrDefault();
                sae.AcceptSocket = null;
                (listener?.Connector as AsyncSocket)?.Socket?.AcceptAsync(sae, AcceptCallback, true); //Relisten
            }
        } catch (Exception exp) {
            //saePool.Push(sae);
            EventCall(onError, this, new ExchangeEventArgs(null, new ByteExchangeException("Could not restart listener after accept", exp)));
            if (Config.IsServer) (listener?.Connector as AsyncSocket)?.Socket?.AcceptAsync(sae, AcceptCallback, true); //Relisten
            return;
        }
        SocketAsyncEventArgs rSae;
        try {
            if (sae.SocketError != SocketError.Success) {
                if (Config.IsServer) return;
                throw new Exception($"Socket error: {sae.SocketError}");
            }

            // Access control
            if (Config.IsServer && !Config.Server.CheckAccessForClient(this, (IPEndPoint)remotePoint)) {
                connector.Dispose();
                return;
            }

            if (Config.IsStream) {
                socket.CreateNetworkStream();
                if (socket.Stream == null) throw new Exception("Socket stream is null");
                // SSL operations
                if (Config.Ssl.Enabled) {
                    if (Config.Ssl.ServerName == null) throw new Exception("ServerName can not be null on ssl connections");
                    if (Config.IsServer) { //Server SSL
                    if (Config.Ssl.LocalCertificate == null) throw new Exception("LocalCertificate can not be null on ssl connections");
                        var sslStream = new SslStream(socket.Stream, false, Config.Ssl.RemoteCertValidator, Config.Ssl.LocalCertSelector);
                        sslStream.AuthenticateAsServer(Config.Ssl.LocalCertificate,
                            Config.Ssl.RequireRemoteCertificate,
                            Config.Ssl.Protocol,
                            Config.Ssl.CheckRevocation);
                        socket.Stream = sslStream;
                    } else { //Client SSL
                             //This part is buggy.. Local Certificate selector portion does not authenticate with its selector.
                        var lCert = Config.Ssl.LocalCertificate;
                        var lCertSelector = Config.Ssl.LocalCertSelector;
                        var sslStream = new SslStream(socket.Stream, false, Config.Ssl.RemoteCertValidator, lCertSelector);
                        try {
                            if (lCert != null) {
                                sslStream.AuthenticateAsClient(Config.Ssl.ServerName,
                                   new X509CertificateCollection(new X509Certificate[] { lCert }),
                                   Config.Ssl.Protocol,
                                   Config.Ssl.CheckRevocation);
                            } else {
                                sslStream.AuthenticateAsClient(Config.Ssl.ServerName ?? ""); //serverSN "nessus-VT01"
                            }

                            socket.Stream = sslStream;
                        } catch (Exception e) {
                            var exp = new ByteExchangeException($"SSL connection could not be established from ({remotePoint}), recieved: '{e.GetType().Name}'", e, remotePoint);
                            EventCall(onError, this, new ExchangeEventArgs(remotePoint, exp));
                            //Stop(); //client.Close();
                            return;
                        }
                    }
                }
                //if (Config.Stream.UseBufferedStream) stream = new BufferedStream(stream, Config.Stream.BufferSize);
                Config.Stream.ApplyStreamConfigTo(socket.Stream);
                if (!socket.Stream.CanRead || !socket.Stream.CanWrite) { throw new Exception("Stream is invalid!"); } else {
                    // Add client to list
                    if (!Config.IsServer) {
                        var localEndpoint = socket.Socket?.LocalEndPoint;
                        if (localEndpoint != null) LocalEndPoints.Add(localEndpoint, new ExchangeConnector(socket, localEndpoint));
                    }
                    connector.Stream = socket.Stream;
                    Remotes[remotePoint] = connector;
                }
            } else {
                // Create the state object.
                rSae = SaePool.PopData(this, connector);
                rSae.RemoteEndPoint = remotePoint;
                rSae.AcceptSocket = socket.Socket;

                // Add client to list
                connector.Session = new ExchangeSession();
                Remotes[remotePoint] = connector;

                if (Config.IsConnectionless)
                    socket.Socket?.ReceiveFromAsync(rSae, ReceiveCallback, true);
                else
                    socket.Socket?.ReceiveAsync(rSae, ReceiveCallback, true);
            }

            Task.Run(() => EventCall(onConnect, this, new ExchangeConnectorEventArgs(connector)));
        } catch (Exception exp) {
            //rSae?.Dispose();
            connector?.Dispose();
            EventCall(onError, this, new ExchangeEventArgs(remotePoint, new ByteExchangeException("Client could not connect", exp)));
            if (!Config.IsServer) throw;
        }
    }

    /// <summary>Receive Callback</summary>
    protected virtual void ReceiveCallback(object? sender, SocketAsyncEventArgs sae) {
        if (!HasStarted) return; //This must be disposed
        var remotePoint = (IPEndPoint?)sae.RemoteEndPoint;
        ExchangePacket? recData = null;
        ExchangeConnector connector;

        try {
            if (remotePoint == null) throw new Exception("Remote can not be null");
            if (sae.AcceptSocket == null) throw new Exception("Accept Socket can not be null");

            var bytesRead = sae.BytesTransferred;

            //Console.WriteLine($"R{bytesRead}");
            if (bytesRead == 0 || sae.SocketError != SocketError.Success) {
                //SocketError
                if (!Config.IsServer || Config.Socket.Protocol != ProtocolType.Udp) SaePool.Push(sae);
                Disconnect(false, remotePoint);
                return;
                //throw new Exception($"Socket error: {sae.SocketError}");
            }
            if (sae.UserToken is not ExchangePacket state) throw new Exception("Socket state error: null state");
            //if (state.Buffer == null) throw new Exception("State buffer can not be null");
            connector = state.Connector;

            //Udp session control (since accept callback is not called)
            if (Config.IsServer && Config.Socket.Protocol == ProtocolType.Udp) { //Server
                connector = new ExchangeConnector(new AsyncSocket(sae.AcceptSocket), remotePoint); //Session = new ExchangeSession()
                if (!Remotes.ContainsKey(remotePoint)) {
                    // Access control
                    if (!Config.Server.CheckAccessForClient(this, remotePoint as IPEndPoint)) {
                        SaePool.Push(sae);
                        Disconnect(remotePoint);
                        return;
                    }
                    // Add connector since acceptcallback is skipped
                    Remotes[remotePoint] = connector; //Accepted socket
                    Task.Run(() => EventCall(onConnect, this, new ExchangeConnectorEventArgs(connector)));
                }
            }

            if (connector == null) throw new Exception("Socket connector error: null connector");

            // Received some data from the server
            state.ReceivePacket(sae.MemoryBuffer.Span[..bytesRead], out recData);
            if (recData != null) recData.Connector = connector;

            //Read Next Message
            //sae.SetBuffer(state.Buffer, 0, state.Buffer.Length);
            if (Config.Socket.Protocol == ProtocolType.Udp)
                sae.AcceptSocket?.ReceiveFromAsync(sae, ReceiveCallback, true);
            else
                sae.AcceptSocket?.ReceiveAsync(sae, ReceiveCallback, true);

            if (Config.Packet.FuncReceive != null) recData = Config.Packet.FuncReceive(recData);
        } catch (Exception e) {
            if (Config.IsServer && Config.Socket.Protocol == ProtocolType.Udp) {
                sae.AcceptSocket?.ReceiveFromAsync(sae, ReceiveCallback, true);
            } else {
                sae?.Dispose();
            }
            if (HasStarted && remotePoint != null && Remotes.ContainsKey(remotePoint)) {
                var exp = new ByteExchangeException($"Could not receive from ({remotePoint}), received: '{e.GetType().Name}'", e, remotePoint);
                Disconnect(remotePoint);
                EventCall(onError, this, new ExchangeEventArgs(remotePoint, exp));
            }
        }

        if (recData != null) {
            Task.Run(() => EventCall(onReceive, this, recData));
        }
    }
    #endregion

    #region Disconnect()
    /// <inheritdoc />
    public override void Disconnect(EndPoint? remotePoint = null) => Disconnect(true, remotePoint);

    /// <inheritdoc />
    internal virtual void Disconnect(bool informOtherEnd, EndPoint? remotePoint = null) {
        if (IsDisposed) return;
        if (Config.IsServer) {
            try {
                var connector = GetConnector(remotePoint);
                if (connector == null) return;
                if (Config.IsConnectionless) {
                    if (informOtherEnd && connector.Remote != null) (connector.Connector as AsyncSocket)?.Socket?.SendTo(Array.Empty<byte>(), connector.Remote); //udp disconnect otherwise listener dies
                }
            } catch { }
        }
        base.Disconnect(remotePoint);
    }
    #endregion RemoveClient()

    #region Methods
    /// <inheritdoc />
    public override bool IsConnectedTo(EndPoint endpoint) {
        if (Config.IsConnectionless) return true;
        var connector = GetConnector(endpoint);
        var s = connector?.Connector as AsyncSocket;
        return s?.Socket != null && s.Socket.IsConnected() && base.IsConnectedTo(endpoint);
    }

    /// <summary>Create a socket</summary>
    protected virtual AsyncSocket CreateSocket() {
        var rval = new AsyncSocket(Config.Socket.AddressFamily, Config.Socket.SocketType, Config.Socket.Protocol);
        if (rval.Socket == null) throw new NullReferenceException("Socket can not be null");
        Config.Socket.ApplySocketConfigTo(rval.Socket);
        onSocketCreate?.Invoke(this, rval.Socket);
        return rval;
    }

    /// <summary>Create a listener socket</summary>
    protected virtual AsyncSocket CreateListener(EndPoint bp) {
        var listener = new AsyncSocket(Config.Socket.AddressFamily, Config.Socket.SocketType, Config.Socket.Protocol);
        if (Config.Socket.Options.All(x => x.Name != SocketOptionName.ReuseAddress)) Config.Socket.AddOption(SocketOptionName.ReuseAddress, true);
        if (listener.Socket == null) throw new NullReferenceException("Listener socket can not be null");
        Config.Socket.ApplySocketConfigTo(listener.Socket);
        onSocketCreate?.Invoke(this, listener.Socket);
        listener.Socket.Bind(bp);
        if (Config.Socket.SocketType != SocketType.Dgram) listener.Socket.Listen(Config.Server.ListenerBackLog);
        return listener;
    }
    #endregion

    #region GetOpCoder, GetClassExchanger
    /// <summary>Gets a wrapping controller organizing communication via opcodes</summary>
    /// <typeparam name="TOpCode">opcode type, preferably and enum with numeric subclass</typeparam>
    /// <returns></returns>
    public virtual OpCodeExchanger<TOpCode> GetOpCoder<TOpCode>(TOpCode requestOpCode, TOpCode responseOpCode) where TOpCode: notnull => new(this,requestOpCode, responseOpCode);
    /// <summary>Gets a wrapping controller which sends and recieves serializable classes only</summary>
    /// <returns></returns>
    public virtual ClassExchanger GetClassExchanger() => new(this);
    #endregion
}