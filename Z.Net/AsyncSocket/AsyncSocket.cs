﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Diagnostics;
using System.Net.Sockets;
using Z.Util;

namespace Z.Net.AsyncSocket;

/// <summary>Event arguments for DataExchange</summary>
public class AsyncSocket : DisposableZ
{
    #region Fields / Properties
    ///<summary>Inner NetworkStream related with the socket </summary>
    public NetworkStream? InnerStream { get; internal set; }
    /// <summary>Real socket</summary>
    public Socket? Socket { get; internal set; }
    /// <summary>Stream related with the socket</summary>
    public Stream? Stream { get; internal set; }
    #endregion

    #region Constructors
    /// <inheritdoc />
    public AsyncSocket(AddressFamily family, SocketType socketType, ProtocolType protocolType) : this(new Socket(family, socketType, protocolType)) { }
    /// <inheritdoc />
    public AsyncSocket(Socket socket) {
        Socket = socket;
    }
    #endregion

    /// <summary>Creates a network stream for the Socket</summary>
    /// <returns></returns>
    public Stream CreateNetworkStream() {
        if (Socket == null) throw new ArgumentNullException(nameof(Socket));
        Stream = InnerStream = new NetworkStream(Socket);
        return Stream;
    }
    // InnerStream = new NetworkStream(Socket);
    // Stream = InnerStream;
    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override async ValueTask DisposeAsync(bool disposing) {
        if (disposing) {
            if (Stream == null) return;
            try {
                //Stream.Close();
                await Stream.DisposeAsync();
            } catch (Exception exp) {
                Debug.Write(exp);
            } finally { Stream = null; }
            try {                        //Socket.Shutdown(SocketShutdown.Both);
                                         //Socket.Close();
                Socket?.Dispose();
            } catch (Exception exp) {
                Debug.Write(exp);
            } finally { Socket = null; }
            //if (ExtraSecurity != null) ExtraSecurity.Dispose();
        }
        #endregion Dispose
    }
}