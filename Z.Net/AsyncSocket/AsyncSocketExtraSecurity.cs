﻿﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Z.Collections.Specialized;
using Z.Cryptography;
using Z.Util;

namespace Z.Net.ByteExchange
{
    /// <summary>Async Socket configuration</summary>
    public class AsyncSocketExtraSecurity
    {
        private volatile uint packetNumber;
        private Z.Cryptography.DiffieHellman dhSend;
        private Z.Cryptography.DiffieHellman dhReceive;
        private volatile uint nextKeySendPacket;
        private volatile uint nextKeyReceivePacket;
        private IEncryptor Encryptor;
        private Scrambler2 Scrambler;
        private int minPacket;
        private int maxPacket;
        private Random rnd;

        public AsyncSocketExtraSecurity(AsyncSocketConfig config)
        {
            var cfg = config.ExtraSecurity;
            packetNumber = 0;
            Scrambler = cfg.Scrambler;
            if (config.ExtraSecurity.Encrypter != null) {
                minPacket = cfg.KeyExchangePacketsMin;
                maxPacket = cfg.KeyExchangePacketsMax;
                Encryptor = cfg.Encrypter;
                dhSend = new DiffieHellman(config.ExtraSecurity.DiffieHellmanBits);
                rnd = new Random();
            }
        }

        public byte[] Pack(byte[] data)
        {
            if (Encryptor!= null) {
                var b = new ByteArray(700);
                b.Add(packetNumber);
                packetNumber = (packetNumber + 1) % uint.MaxValue;
                if (packetNumber == 0 || packetNumber == nextKeySendPacket) {
                    byte[] p, g;
                    var req = dhSend.GenerateRequest(out p, out g);
                    nextKeySendPacket = (packetNumber + (uint) rnd.Next(minPacket, maxPacket)) % uint.MaxValue;
                    b.Add(ESOpCodes.DHRequest);
                    b.Add(dhSend.Bits);
                    b.Add(p, g, req);
                }
            }
            return data;
        }

        private enum ESOpCodes : byte{
            None = 0,
            DHRequest = 1,
            DHResponse = 2,
        }

    }
}