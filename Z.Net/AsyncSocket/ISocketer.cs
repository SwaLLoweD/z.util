﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net;
using Z.Net.ByteExchange;

namespace Z.Net.AsyncSocket
{
    /// <summary>byte[] data Node</summary>
    public interface ISocketer : IByteExchanger
    {
        #region Fields / Properties
        /// <summary>Configuration Parameters</summary>
        new SocketConfig Config { get; }
        #endregion
        }
    #region OpCoder and ClassExchanger (Disabled)
    // /// <summary>byte[] data Node</summary>
    // public interface ISocketer<TOpCode> : IByteExchanger<TOpCode>
    // {
    //     #region Fields / Properties
    //     /// <summary>Byte Transport Node</summary>
    //     new ISocketer Transport { get; }
    //     #endregion
    //
    //     /// <summary>Create new packet for remote</summary>
    //     /// <param name="opCode">Packet opcode if exists</param>
    //     ByteExchangePacket<TOpCode> CreatePacket(TOpCode opCode);
    //     /// <summary>Create a request to read results in sync (after a timeout). flush the request in order to it to work</summary>
    //     ByteExchangeRequest<TOpCode> CreateRequest(TOpCode opCode, int timeout = -2);
    //     /// <inheritdoc />
    //     void AddHandler(TOpCode opCode, Func<ISocketer<TOpCode>, EndPoint, Stream, bool> handler);
    //     /// <inheritdoc />
    //     void AddRequestHandler(TOpCode opCode, Func<ISocketer<TOpCode>, ByteExchangeRequest<TOpCode>, bool> handler);
    // }
    //
    // /// <summary>byte[] data Node</summary>
    // public interface IClassSocketer : IClassExchanger
    // {
    //     /// <summary>Byte Transport Node</summary>
    //     new ISocketer Transport { get; }
    //     /// <summary>Byte OpCode Controller Node</summary>
    //     new ISocketer<ushort> OpCoder { get; }
    //     /// <summary>Send byte[] data to Client</summary>
    //     /// <param name="obj">Data</param>
    //     /// <param name="async">Execute asynchronously</param>
    //     /// <param name="exp">Received Exception</param>
    //     /// <param name="timeout">Request timeout (-2 is default time)</param>
    //     bool Send<T>(T obj, bool async = false, int timeout = -2, Exception exp = null);
    //     /// <summary>Send byte[] data to Client</summary>
    //     /// <param name="req">Request class</param>
    //     /// <param name="response">Response class</param>
    //     /// <param name="async">Execute asynchronously</param>
    //     /// <param name="exp">Received Exception</param>
    //     /// <param name="timeout">Request timeout (-2 is default time)</param>
    //     bool Send<TReq, TResp>(TReq req, out TResp response, bool async = false, int timeout = -2, Exception exp = null)
    //         where TReq : IExchangeRequest<TResp> where TResp : IExchangeable;
    //     // /// <summary>Add a received packet handler</summary>
    //     // void AddHandler<T>(Func<IAsyncSocketClassNode, EndPoint, T, bool> handler);
    //     // /// <summary>Add a received packet handler</summary>
    //     // void AddRequestHandler<TReq, TResp>(ByteExchangeClassNode.RequestHandler<TReq, TResp> handler) where TReq : IByteExchangeRequestClass<TResp> where TResp : IByteExchangeClass;
    // }
    #endregion
}