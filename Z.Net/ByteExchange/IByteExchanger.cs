﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Collections.Concurrent;

namespace Z.Net.ByteExchange;

/// <summary>byte[] data Node</summary>
public interface IByteExchanger : IDisposableZ
{
    #region Fields / Properties
    /// <summary>Remotes of this node (may be connected or not)</summary>
    ConcurrentDictionary<EndPoint, ExchangeConnector> LocalEndPoints { get; }
    ///// <summary>Fetches the socket of another client</summary>
    //AsyncSocket GetSocket(EndPoint remote);

    /// <summary>Remotes of this node (may be connected or not)</summary>
    ConcurrentDictionary<EndPoint, ExchangeConnector> Remotes { get; }
    /// <summary>Configuration Parameters</summary>
    ExchangeConfig Config { get; }
    /// <summary>True if started, false if stopped</summary>
    bool HasStarted { get; }
    #endregion

    #region Events
    /// <summary>Raised when a client has connected to a server</summary>
    event EventHandlerAsync<ExchangeConnectorEventArgs> OnConnect;

    /// <summary>Raised when a client has disconnected from a server</summary>
    event EventHandler<ExchangeEventArgs> OnDisconnect;

    /// <summary>Raised when client recieved an exception</summary>
    event EventHandler<ExchangeEventArgs> OnError;

    /// <summary>Raised when data received data from a node</summary>
    event EventHandlerAsync<ExchangePacket> OnReceive;
    /// <summary>Raised when data has been sent to a node</summary>
    event EventHandler<ExchangeEventArgs> OnSend;
    /// <summary>Raised when started</summary>
    event EventHandler<ExchangeEventArgs> OnStart;

    /// <summary>Raised when stopped</summary>
    event EventHandler<ExchangeEventArgs> OnStop;
    #endregion

    /// <summary>Checks if this node is connected to the specified endpoint</summary>
    /// <param name="endpoint">Endpoint to check the connectivity for</param>
    bool IsConnectedTo(EndPoint endpoint);

    /// <summary>Start server and wait for client connections (-2 = dont wait, -1 = infinite wait)</summary>
    bool Start(int waitTimeout = -2);
    /// <summary>Close connections and stop server</summary>
    void Stop();

    /// <summary>Send byte[] data to Client</summary>
    /// <param name="packet">Data</param>
    /// <param name="asyncCallback">Execute asynchronously</param>
    /// <param name="noExceptions">Redirects exceptions to onerror event if set</param>
    bool Send(ExchangePacket packet, bool asyncCallback = false, bool noExceptions = true);

    /// <summary>Send byte[] data to Client</summary>
    /// <param name="packet">Data</param>
    /// <param name="asyncCallback">Execute asynchronously</param>
    /// <param name="noExceptions">Redirects exceptions to onerror event if set</param>
    Task<bool> SendAsync(ExchangePacket packet, bool asyncCallback = false, bool noExceptions = true);

    /// <summary>Send byte[] data to Client</summary>
    /// <param name="b">Data</param>
    /// <param name="e">Endpoint to send to</param>
    /// <param name="asyncCallback">Execute asynchronously</param>
    /// <param name="noExceptions">Redirects exceptions to onerror event if set</param>
    bool Send(ReadOnlySpan<byte> b, EndPoint? e = null, bool asyncCallback = false, bool noExceptions = true);

    /// <summary>Send byte[] data to Client</summary>
    /// <param name="b">Data</param>
    /// <param name="e">Endpoint to send to</param>
    /// <param name="asyncCallback">Execute asynchronously</param>
    /// <param name="noExceptions">Redirects exceptions to onerror event if set</param>
    Task<bool> SendAsync(ReadOnlyMemory<byte> b, EndPoint? e = null, bool asyncCallback = false, bool noExceptions = true);

    /// <summary>Node tries to handle outer data with its own handlers</summary>
    Task<bool> HandleReceive(IByteExchanger node, EndPoint remote, Stream stream);

    /// <summary>Find connector object from its endpoint</summary>
    /// <param name="remote"></param>
    /// <param name="reconnectTimeout">Auto-Reconnect for sends for client reconnect timeout in ms (-2 = disabled, -1 = infinite)</param>
    /// <returns></returns>
    ExchangeConnector? GetConnector(EndPoint? remote = null, int reconnectTimeout = -2);

    /// <summary>Gets the client endpoint</summary>
    EndPoint GetClientEndPoint(EndPoint? remote = null);

    /// <summary>Detach all events for this server</summary>
    void DetachEvents();

    /// <summary>Disconnect a client</summary>
    void Disconnect(EndPoint? remotePoint = null);
    /// <summary>Call and event of the ByteExchanger</summary>
    bool EventCall<TEventArgs>(EventHandler<TEventArgs>? handler, object node, TEventArgs args);
    /// <summary>Call and event of the ByteExchanger</summary>
    Task<bool> EventCallAsync<TEventArgs>(EventHandler<TEventArgs>? handler, object node, TEventArgs args);

    /// <summary>Create new packet for remote</summary>
    /// <param name="remote">Endpoint to send to</param>
    /// <param name="reconnectTimeout">Auto-Reconnect for sends for client reconnect timeout in ms (-2 = disabled, -1 = infinite)</param>
    ExchangePacket CreatePacket(EndPoint? remote = null, int reconnectTimeout = -2);
}

/// <summary>byte[] data Node</summary>
public interface IOpCoder<TOpCode> : IDisposableZ where TOpCode: notnull
{
    #region Fields / Properties
    /// <summary>Byte Transport Node</summary>
    IByteExchanger Transport { get; }
    /// <summary>OpCode used for requests</summary>
    TOpCode RequestOpCode { get; }
    /// <summary>OpCode used for responses</summary>
    TOpCode ResponseOpCode { get; }
    #endregion

    #region Events
    /// <summary>Raised when client recieved an exception</summary>
    event EventHandler<ExchangeEventArgs> OnError;

    /// <summary>Raised when an opcode is received</summary>
    event EventHandler<OpCodeReceivedEventArgs> OnOpCodeReceived;
    #endregion

    /// <summary>Start server and wait for client connections (-2 = dont wait, -1 = infinite wait)</summary>
    bool Start(int waitTimeout = -2);

    /// <summary>Create new packet for remote</summary>
    /// <param name="opCode">Packet opcode if exists</param>
    /// <param name="remote">Endpoint to send to</param>
    /// <param name="reconnectTimeout">Auto-Reconnect for sends for client reconnect timeout in ms (-2 = disabled, -1 = infinite)</param>
    ExchangePacket<TOpCode> CreatePacket(TOpCode opCode, EndPoint? remote = null, int reconnectTimeout = -2);
    /// <summary>Create a request to read results in sync (after a timeout). flush the request in order to it to work</summary>
    /// <param name="opCode">Packet opcode</param>
    /// <param name="remote">Remote to send to</param>
    /// <param name="timeout">Request timeout in ms</param>
    /// <param name="reconnectTimeout">Auto-Reconnect for sends for client reconnect timeout in ms (-2 = disabled, -1 = infinite)</param>
    ExchangeRequest<TOpCode> CreateRequest(TOpCode opCode, EndPoint? remote = null, int timeout = 2000, int reconnectTimeout = -2);

    /// <summary>Add a received packet handler</summary>
    void AddHandler(TOpCode opCode, Func<ExchangePacket, Task<bool>> handler);
    /// <summary>Add a received packet handler</summary>
    void AddRequestHandler(TOpCode opCode, Func<ExchangeResponse<TOpCode>, Task<bool>> handler);
    /// <summary>Node tries to handle outer data with its own handlers</summary>
    Task<bool> HandleReceive(ExchangePacket b);
    ///// <summary>Handle incoming request stream</summary>
    //bool HandleRequest(EndPoint remote, Stream stream);
    // /// <summary>Set the opcode to be used in Request Mode</summary>
    // void SetRequestOpCode(TOpCode requestOpCode, TOpCode responseOpCode);
    /// <summary>Add a new request to tracking table</summary>
    void AddRequest(ExchangeRequest<TOpCode> req);
    /// <summary>Remove a request from tracking table</summary>
    bool RemoveRequest(Guid id);

    /// <summary>Detach all events for this server</summary>
    void DetachEvents();
}

/// <summary>byte[] data Node</summary>
public interface IClassExchanger : IDisposableZ
{
    #region Fields / Properties
    /// <summary>Byte Transport Node</summary>
    IByteExchanger Transport { get; }
    /// <summary>Byte OpCode Controller Node</summary>
    IOpCoder<ushort> OpCoder { get; }
    // /// <summary>Byte OpCode Controller Node</summary>
    // IDictionary<Type, ushort> ClassMap { get; }
    #endregion

    #region Events
    /// <summary>Raised when client recieved an exception</summary>
    event EventHandler<ExchangeEventArgs> OnError;
    #endregion

    /// <summary>Start server and wait for client connections (-2 = dont wait, -1 = infinite wait)</summary>
    bool Start(int waitTimeout = -2);

    /// <summary>Send byte[] data to Client</summary>
    /// <param name="obj">Data</param>
    /// <param name="remote">Endpoint to send to</param>
    /// <param name="async">Execute asynchronously</param>
    /// <param name="timeout">Request timeout (-2 is default time)</param>
    bool Send<T>(T obj, EndPoint? remote = null, bool async = false, int timeout = -2);
    /// <summary>Send byte[] data to Client</summary>
    /// <param name="req">Request class</param>
    /// <param name="response">Response class</param>
    /// <param name="remote">Endpoint to send to</param>
    /// <param name="async">Execute asynchronously</param>
    /// <param name="timeout">Request timeout (-2 is default time)</param>
    bool Send<TReq, TResp>(TReq req, out TResp? response, EndPoint? remote = null, bool async = false, int timeout = -2)
        where TReq : IExchangeRequest<TResp> where TResp : IExchangeable;

    /// <summary>Maps and identifies the type for byte exchange</summary>
    void Map<T>();
    /// <summary>Maps and identifies the type for byte exchange</summary>
    void Map(Type t);
    /// <summary>Maps and identifies the types implementing IByteExchangeClass for byte exchange</summary>
    void MapAssembly<T>() where T : IExchangeable;
    /// <summary>Maps and identifies the types implementing IByteExchangeClass for byte exchange</summary>
    void MapAssembly(Type asmType);
    /// <summary>Add a received packet handler</summary>
    void AddHandler<T>(Func<IClassExchanger, EndPoint, T?, Task<bool>> handler);
    /// <summary>Add a received packet handler</summary>
    void AddRequestHandler<TReq, TResp>(ClassExchangeRequestHandler<TReq, TResp> handler) where TReq : IExchangeRequest<TResp> where TResp : IExchangeable;
    /// <summary>Detach all events for this server</summary>
    void DetachEvents();
}