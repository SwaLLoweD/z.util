/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Net;

namespace Z.Net.ByteExchange
{
    /// <summary>Pseudo endpoint for testing or internal byte[] communication emulation using delegates</summary>
    public class DummyExchangeEndPoint : EndPoint
    {
        #region Fields / Properties
        // /// <summary>Delegate to handle recieves for this endpoint</summary>
        // public Action<DummyByteExchangeEndPoint, byte[]> Handler { get; private set; }
        /// <summary>Delegate to handle recieves for this endpoint</summary>
        public DummyExchanger EndPoint { get; }
        #endregion

        #region Constructors
        // /// <summary>DummyEndPoint Initializer</summary>
        // /// <param name="ep">Delegate to handle recieves for this endpoint</param>
        // public DummyByteExchangeEndPoint(Action<DummyByteExchangeEndPoint, byte[]> ep) { Handler = ep; }
        /// <summary>DummyEndPoint Initializer</summary>
        /// <param name="ep">Delegate to handle recieves for this endpoint</param>
        public DummyExchangeEndPoint(DummyExchanger ep) => EndPoint = ep;
        #endregion
    }
}