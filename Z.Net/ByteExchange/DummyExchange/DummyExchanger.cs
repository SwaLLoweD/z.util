/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Z.Extensions;
using Z.Util;

namespace Z.Net.ByteExchange;

/// <summary>Pseudo Client for testing or internal byte[] communication emulation using delegates</summary>
public class DummyExchanger : ByteExchanger
{
    #region Fields / Properties
    /// <summary>Origin endpoint</summary>
    protected DummyExchangeEndPoint origin;
    /// <summary>Server/Remote endpoint</summary>
    public DummyExchangeEndPoint Remote { get; set; }
    #endregion

    #region Constructors
    /// <summary>DummyClient Initializer</summary>
    /// <param name="ep">Endpoint of the client</param>
    public DummyExchanger(DummyExchangeEndPoint ep) : base(new ExchangeConfig(ep, false)) {
        origin = new DummyExchangeEndPoint(this);
        Remote = ep;
        Remotes.Add(Remote, new ExchangeConnector(this, ep));
    }
    /// <summary>DummyServer Initializer</summary>
    public DummyExchanger() : base(new ExchangeConfig(new IPEndPoint(IPAddress.Any, 1), true)) {
        origin = new DummyExchangeEndPoint(this);
        Config.BindAddress[0] = origin;
        Remote = origin;
    }
    #endregion

    /// <inheritdoc />
    public virtual void HandleConnect(EndPoint remote) {
        var connector = new ExchangeConnector(this, remote);
        Remotes.Add(remote, connector);
        EventCall(onConnect, this, new ExchangeConnectorEventArgs(connector));
    }
    /// <summary>Receive handler</summary>
    public virtual void HandleReceive(EndPoint remote, Stream s) {
        var connector = GetConnector(remote);
        if (connector == null) {
            EventCall(onError, this, new ExchangeEventArgs(remote, new ByteExchangeException("No remote found during receive")));
            return;
        }
        EventCall(onReceive, this, new ExchangePacket(this, connector, true));
    }
    /// <inheritdoc />
    public override bool Send(ExchangePacket data, bool asyncCallback = false, bool noExceptions = true) {
        using (var s = ZUtilCfg.GlobalMemoryStreamManager.GetStream(data.ToArray())) {
            ((DummyExchangeEndPoint)data.Connector.Remote).EndPoint.HandleReceive(origin, s);
        }
        return true;
    }
    /// <inheritdoc />
    public override async Task<bool> SendAsync(ExchangePacket data, bool asyncCallback = false, bool noExceptions = true) {
        using (var s = ZUtilCfg.GlobalMemoryStreamManager.GetStream(data.ToArray())) {
            await Task.Run(() => ((DummyExchangeEndPoint)data.Connector.Remote).EndPoint.HandleReceive(origin, s));
        }
        return true;
    }

    #region Start()
    /// <inheritdoc />
    public override bool Start(int waitTimeout = -2) {
        if (!Config.IsServer) {
            (Remote.EndPoint as DummyExchanger)?.HandleConnect(origin);
            EventCall(onConnect, this, new ExchangeConnectorEventArgs(Remotes.Values.First()));
        }
        return base.Start(waitTimeout);
    }
    #endregion Start()

    /// <inheritdoc />
    public override void Disconnect(EndPoint? remotePoint = null) {
        if (Config.IsServer) {
            base.Disconnect(remotePoint);
        } else if (remotePoint == null || remotePoint as DummyExchangeEndPoint == Remote) {
            base.Disconnect(remotePoint);
        }

        //No disconnect
    }
}