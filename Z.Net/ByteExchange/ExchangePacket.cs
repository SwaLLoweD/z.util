/* Copyright (c) <2020> <A. Zafer YURDACALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Threading;
namespace Z.Net.ByteExchange;

/// <summary>Byte Transfer Packet Builder.. Needs to be flushed to be sent</summary>
public class ExchangePacket : Stream, IDisposableZ
{
    #region Fields / Properties
    ///<summary>Is disposed</summary>
    public virtual bool IsDisposed => _disposed;
    /// <summary>Disposed signal</summary>
    protected object lockDispose = new();
    /// <summary>Disposed signal</summary>
    protected bool _disposed = false;
    /// <summary>Stream of the request</summary>
    protected internal Stream Stream { get; set; }

    /// <summary>Date and time request is created</summary>
    public DateTime DateCreated { get; internal set; }
    /// <summary>Remote of the request</summary>
    public EndPoint Remote => Connector.Remote;
    /// <summary>ByteExchange Transport Node</summary>
    public IByteExchanger Transport { get; internal set; }
    /// <summary>ByteExchange Connector</summary>
    public ExchangeConnector Connector { get; internal set; }
    /// <summary>ByteExchange Session object</summary>
    public ExchangeSession Session => Connector.Session;
    /// <summary>Redirects exceptions to onerror event if set</summary>
    public bool NoExceptions { get; set; } = true;
    /// <summary>Restarts the Transport to send if not connected</summary>
    public bool AutoRestartClientToSend { get; set; } = false;

    // /// <summary>Receive buffer.</summary>
    // internal byte[]? Buffer { get; private set; }
    /// <summary>Received Data size in bytes</summary>
    protected long ExpectedDataSize { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    internal ExchangePacket(IByteExchanger node, ExchangeConnector connector, Stream stream, bool noExceptions = true) : this(node, connector, stream, false, noExceptions) { }
    /// <summary>Constructor</summary>
    internal ExchangePacket(IByteExchanger node, ExchangeConnector connector, bool isReceiving = false, bool noExceptions = true) : this(node, connector, null, isReceiving, noExceptions) { }
    /// <summary>Constructor</summary>
    protected ExchangePacket(IByteExchanger node, ExchangeConnector connector, Stream? stream, bool isReceiving, bool noExceptions) {
        Transport = node;
        Connector = connector;
        DateCreated = DateTime.UtcNow;
        NoExceptions = noExceptions;
        if (stream != null) Stream = stream;
        else if (node.Config.IsStream) {
            if (connector.Stream != null) Stream = connector.Stream;
            else throw new ArgumentNullException(nameof(stream));
        } 
        else if (Stream == null) {
            if (isReceiving) Stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream("ByteExchangePacketReceive", Transport.Config.Packet.BufferSize);
            else {
                Stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream("ByteExchangePacketSend");
                if (node.Config.Packet.PacketEof.IsEmpty) this.Writez((long)0); //space for packet length
            }
        }
    }
    #endregion

    /// <summary>Clear all data.</summary>
    public void Clear() {
        Stream.SetLength(0); //.Clear();
        Stream.Position = 0;
        ExpectedDataSize = 0;
        //Connector = null;
    }

    /// <summary>Create Request object for use by server.</summary>
    protected internal ExchangeResponse<TOpCode> CreateResponseForServer<TOpCode>(Guid id, TOpCode opCode, IOpCoder<TOpCode> node) where TOpCode : notnull =>
        new(id, opCode, node, Connector, Stream);

    #region ReceivePacket
    /// <summary>Process a packet.</summary>
    public bool ReceivePacket(ReadOnlySpan<byte> buffer, out ExchangePacket? receivedPacket) {
        //if (Buffer == null) throw new NullReferenceException($"{nameof(Buffer)} can not be null");
        if (buffer.Length <= 0) throw new ByteExchangeException("Exchange packet received zero bytes"); //NoData
        receivedPacket = null;
        if (Stream.Length > Transport.Config.Packet.MaxPacketSize) throw new ByteExchangeException("Max packet size limit exceeded"); //NoData
        return Transport.Config.Packet.PacketEof.IsEmpty ? ReceivePacketWithLen(buffer, out receivedPacket) : ReceivePacketWithEof(buffer, out receivedPacket);
    }
    /// <summary>Process a packet by using len method</summary>
    protected bool ReceivePacketWithLen(ReadOnlySpan<byte> buffer, out ExchangePacket? receivedPacket) {
        receivedPacket = null;
        if (ExpectedDataSize == 0) { //New Packet
            if (buffer.Length <= 8) throw new ByteExchangeException("Exchange packet received no length data"); //NoData
            //Get PacketLen
            ExpectedDataSize = BitConverter.ToInt64(buffer[..8]);
            Stream.Write(buffer[8..]);
        } else Stream.Write(buffer); //Continue previous packet
        if (ExpectedDataSize <= (Stream.Length + 8)) return ClearAfterReceive(out receivedPacket); //Packet Received
        return false; //Next Part
    }
    /// <summary>Process a packet by using eof method.</summary>
    protected bool ReceivePacketWithEof(ReadOnlySpan<byte> buffer, out ExchangePacket? receivedPacket) {
        receivedPacket = null;
        var eof = Transport.Config.Packet.PacketEof;
        Stream.Write(buffer);
        if (eof.Length == 0) return ClearAfterReceive(out receivedPacket); //Raw packet received
        var eofFind = Stream.IndexOf(eof.ToArray()).FirstOrDefault(-1);
        if (eofFind < 0) {
            return false; //Next Part
        }
        Stream.Position = eofFind;
        return ClearAfterReceive(out receivedPacket); //Packet Received
    }
    /// <summary>Clear after a recieve event.</summary>
    protected bool ClearAfterReceive(out ExchangePacket receivedPacket) {
        Stream.Position = 0;
        receivedPacket = new(Transport, Connector, true, NoExceptions) {
            Stream = Stream,
            ExpectedDataSize = ExpectedDataSize,
            DateCreated = DateCreated
        };

        Stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream("ByteExchangePacketReceive", Transport.Config.Packet.BufferSize);
        DateCreated = DateTime.UtcNow;
        ExpectedDataSize = 0;
        return true;
    }
    #endregion

    #region Send
    /// <summary>Flush All Data into the stream.</summary>
    public async Task<bool> SendAsync(bool async = false) {
        if (Transport == null) throw new NullReferenceException($"{nameof(Transport)} can not be null");
        try {
            if (!SendPrepareAndCheck()) return true;
            return await Transport.SendAsync(this, async, NoExceptions);
        } catch (Exception) {
            if (NoExceptions) return false;
            throw;
        }
    }

    /// <summary>Flush All Data into the stream.</summary>
    public bool Send(bool async = false) {
        if (Transport == null) throw new NullReferenceException($"{nameof(Transport)} can not be null");
        try {
            if (!SendPrepareAndCheck()) return true;
            return Transport.Send(this, async, NoExceptions);
        } catch (Exception) {
            if (NoExceptions) return false;
            throw;
        }
    }
    /// <summary>Final checks and preparetions before sending.</summary>
    protected bool SendPrepareAndCheck() {
        //Write packet headers and eofs or flush stream
        if (Transport.Config.IsStream) { Stream.FlushAsync(); return false; }
        if (Transport.Config.Packet.PacketEof.IsEmpty) SendPacketWithLen();
        else Write(Transport.Config.Packet.PacketEof.Span);
        //Auto restart for client
        if (AutoRestartClientToSend && !Transport.Config.IsServer && Remote != null && !Transport.IsConnectedTo(Remote)) {
            if (Transport.HasStarted) Transport.Stop();
            if (!Transport.Start(5000)) throw new ByteExchangeException("Client Auto-Restart for send failed");
        }
        //Check connection
        if (Remote == null || !Transport.IsConnectedTo(Remote)) throw new ByteExchangeException("Exchange packet send failed - Transport is not connected", null, Remote);
        return true;
    }
    /// <summary>Flush All Data into the stream.</summary>
    protected bool SendSteamFlush(bool async) {
        if (async)
            _ = Stream.FlushAsync();
        else
            Stream.Flush();
        return true;
    }
    /// <summary>Write Len header to stream.</summary>
    protected void SendPacketWithLen() {
        //Write Len to the Start of the Packet
        var pos = Stream.Position;
        Stream.Position = 0;
        this.Writez(pos);
        Stream.Position = pos;
    }
    ///// <summary>Flush All Data into the stream async.</summary>
    //public System.Threading.Tasks.Task FlushAsync() { return Stream.FlushAsync(); }
    #endregion

    #region Stream Members
    /// <inheritdoc />
    public override int GetHashCode() => Stream.GetHashCode();
    /// <inheritdoc />
    public override void CopyTo(Stream destination, int bufferSize) => Stream.CopyTo(destination, bufferSize);
    /// <inheritdoc />
    public override Task CopyToAsync(Stream destination, int bufferSize, CancellationToken cancellationToken) => Stream.CopyToAsync(destination, bufferSize, cancellationToken);
    /// <inheritdoc />
    public override void Flush() => Stream.Flush();
    /// <inheritdoc />
    public override int Read(byte[] buffer, int offset, int count) => Stream.Read(buffer, offset, count);
    /// <inheritdoc />
    public override int Read(Span<byte> buffer) => Stream.Read(buffer);
    /// <inheritdoc />
    public override long Seek(long offset, SeekOrigin origin) => Stream.Seek(offset, origin);
    /// <inheritdoc />
    public override void SetLength(long value) => Stream.SetLength(value);
    /// <inheritdoc />
    public override void Write(byte[] buffer, int offset, int count) => Stream.Write(buffer, offset, count);
    /// <inheritdoc />
    public override void Write(ReadOnlySpan<byte> buffer) => Stream.Write(buffer);
    /// <inheritdoc />
    public override bool CanRead { get => Stream.CanRead; }
    /// <inheritdoc />
    public override bool CanSeek { get => Stream.CanSeek; }
    /// <inheritdoc />
    public override bool CanWrite { get => Stream.CanWrite; }
    /// <inheritdoc />
    public override long Length { get => Stream.Length; }
    /// <inheritdoc />
    public override long Position { get => Stream.Position; set => Stream.Position = value; }
    #endregion

    /// <summary>Gets all contents as byteArray</summary>
    public byte[] ToArray() => Stream.ReadAllBytes();

    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override void Dispose(bool disposing) {
        lock (lockDispose) {
            if (!_disposed) {
                if (disposing) {
                    //DetachEvents();
                    if (Stream is MemoryStream) Stream?.Dispose();
                    // if (Buffer != null) {
                    //     ArrayPool<byte>.Shared.Return(Buffer);
                    //     Buffer = null;
                    // }
                }
                // Call the appropriate methods to clean up
                // unmanaged resources here.
            }
        }
        base.Dispose(disposing);
        _disposed = true;
    }
    #endregion Dispose
}

/// <summary>Byte Transfer Packet Builder.. Needs to be flushed to be sent</summary>
public class ExchangePacket<TOpCode> : ExchangePacket where TOpCode : notnull
{
    /// <summary>OpCode of the request</summary>
    public TOpCode OpCode { get; set; }
    /// <summary>OpCode Controller Node</summary>
    public IOpCoder<TOpCode> Node { get; set; }

    #region Constructors
    /// <summary>Constructor</summary>
    internal ExchangePacket(TOpCode opCode, IOpCoder<TOpCode> node, ExchangeConnector connector, bool isReceiving = false, bool noExceptions = true) : base(node.Transport, connector, isReceiving, noExceptions) {
        Node = node;
        OpCode = opCode;
        if (!isReceiving) this.Writez(opCode);
    }
    #endregion
}

    // /// <summary>Byte Transfer Packet Builder.. Needs to be flushed to be sent</summary>
    // public class ByteClassPacket<T> : ByteExchangePacket<ushort>
    // {
    //     #region Constructors
    //     /// <summary>Constructor</summary>
    //     public ByteClassPacket(IByteExchangeClassNode node, EndPoint remote, ushort classId) : base(node.OpCoder, remote, classId) { }
    //     /// <summary>Constructor for Response type situations</summary>
    //     public ByteClassPacket(IByteExchangeClassNode node, EndPoint remote, Stream stream, ushort classId) : base(node.OpCoder, remote, stream, classId) { }
    //     #endregion
    // }