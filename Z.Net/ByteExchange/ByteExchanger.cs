/* Copyright (c) <2020> <A. Zafer YURDACALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Collections.Concurrent;
namespace Z.Net.ByteExchange;

#region ByteExchanger
/// <summary>Base Node for byte[] communication requires byte exchange implementation</summary>
public abstract class ByteExchanger : DisposableZ, IByteExchanger
{
    #region Fields / Properties
    /// <summary>Has started</summary>
    protected volatile bool _hasStarted = false;

    /// <summary>Configuration Parameters</summary>
    public ExchangeConfig Config { get; protected set; }
    /// <inheritdoc />
    public virtual bool HasStarted { get => _hasStarted && !_disposed; protected set => _hasStarted = value; }

    /// <inheritdoc />
    public virtual ConcurrentDictionary<EndPoint, ExchangeConnector> Remotes { get; protected set; }
    /// <inheritdoc />
    public virtual ConcurrentDictionary<EndPoint, ExchangeConnector> LocalEndPoints { get; protected set; }
    #endregion

    #region Constructors
    /// <summary>Node Initializer</summary>
    /// <param name="cfg">Configuration Params</param>
    protected ByteExchanger(ExchangeConfig cfg) {
        Config = cfg;
        LocalEndPoints = new ConcurrentDictionary<EndPoint, ExchangeConnector>();
        Remotes = new ConcurrentDictionary<EndPoint, ExchangeConnector>();
    }
    #endregion

    #region Events
    /// <summary>onConnect</summary>
    protected EventHandlerAsync<ExchangeConnectorEventArgs>? onConnect;
    /// <summary>onDisconnect</summary>
    protected EventHandler<ExchangeEventArgs>? onDisconnect;
    /// <summary>onError</summary>
    protected EventHandler<ExchangeEventArgs>? onError;
    /// <summary>onReceive</summary>
    protected EventHandlerAsync<ExchangePacket>? onReceive;
    /// <summary>onSend</summary>
    protected EventHandler<ExchangeEventArgs>? onSend;
    /// <summary>onStart</summary>
    protected EventHandler<ExchangeEventArgs>? onStart;
    /// <summary>onStop</summary>
    protected EventHandler<ExchangeEventArgs>? onStop;

    /// <inheritdoc />
    public event EventHandler<ExchangeEventArgs> OnStart {
        add => onStart += value;
        remove {
            if (onStart != null) onStart -= value;
        }
    }
    /// <inheritdoc />
    public event EventHandler<ExchangeEventArgs> OnStop {
        add => onStop += value;
        remove {
            if (onStop != null) onStop -= value;
        }
    }
    /// <inheritdoc />
    public event EventHandlerAsync<ExchangeConnectorEventArgs> OnConnect {
        add => onConnect += value;
        remove {
            if (onConnect != null) onConnect -= value;
        }
    }
    /// <inheritdoc />
    public event EventHandler<ExchangeEventArgs> OnDisconnect {
        add => onDisconnect += value;
        remove {
            if (onDisconnect != null) onDisconnect -= value;
        }
    }
    /// <inheritdoc />
    public event EventHandler<ExchangeEventArgs> OnError {
        add => onError += value;
        remove {
            if (onError != null) onError -= value;
        }
    }

    /// <inheritdoc />
    public event EventHandlerAsync<ExchangePacket> OnReceive {
        add => onReceive += value;
        remove {
            if (onReceive != null) onReceive -= value;
        }
    }
    /// <inheritdoc />
    public event EventHandler<ExchangeEventArgs> OnSend {
        add => onSend += value;
        remove {
            if (onSend != null) onSend -= value;
        }
    }

    /// <inheritdoc />
    public virtual void DetachEvents() {
        //lock (eventLock) {
        onStart = null;
        onStop = null;
        onConnect = null;
        onDisconnect = null;
        onError = null;
        onReceive = null;
        onSend = null;
        //}
    }
    #endregion

    #region Start Stop
    /// <inheritdoc />
    public virtual bool Start(int waitTimeout = -2) {
        HasStarted = true;
        EventCall(onStart, this, ExchangeEventArgs.Empty);
        return HasStarted;
        //if (Config.ExtraSecurity.Encrypter != null) ExtraSecurity = new AsyncSocketExtraSecurity(Config);
    }
    /// <inheritdoc />
    public virtual void Stop() {
        if (LocalEndPoints != null) {
            foreach (var connector in LocalEndPoints.Values) connector?.Dispose();
            LocalEndPoints.Clear();
        }
        if (Remotes != null) {
            foreach (var connector in Remotes.Values) connector?.Dispose();
            Remotes.Clear();
        }
        HasStarted = false;
        if (!Config.IsServer) EventCall(onDisconnect, this, ExchangeEventArgs.Empty);
        EventCall(onStop, this, ExchangeEventArgs.Empty);
        //if (Config.ExtraSecurity.Encrypter != null) ExtraSecurity = new AsyncSocketExtraSecurity(Config);
    }
    #endregion

    #region Send
    /// <inheritdoc />
    public abstract bool Send(ExchangePacket packet, bool asyncCallback = false, bool noExceptions = true); //=> PSend(packet.ToArray(), packet.Connector, asyncCallback);
    /// <inheritdoc />
    public abstract Task<bool> SendAsync(ExchangePacket packet, bool asyncCallback = false, bool noExceptions = true); //=> PSendAsync(packet.ToArray(), packet.Connector, asyncCallback);
    /// <inheritdoc />
    public virtual bool Send(ReadOnlySpan<byte> data, EndPoint? remote = null, bool asyncCallback = false, bool noExceptions = true) {
        using var packet = CreatePacket(remote);
        packet.Write(data);
        return Send(packet, asyncCallback, noExceptions);
    }
    /// <inheritdoc />
    public virtual async Task<bool> SendAsync(ReadOnlyMemory<byte> data, EndPoint? remote = null, bool asyncCallback = false, bool noExceptions = true) {
        await using var packet = CreatePacket(remote);
        await packet.WriteAsync(data);
        return await SendAsync(packet, asyncCallback, noExceptions);
    }
    // /// <inheritdoc />
    // protected abstract bool PSend(byte[] data, ExchangeConnector connector, bool asyncCallback = false, bool noExceptions = true);
    // /// <inheritdoc />
    // protected abstract Task<bool> PSendAsync(byte[] data, ExchangeConnector connector, bool asyncCallback = false, bool noExceptions = true);
    #endregion

    #region Methods
    /// <summary>Node tries to handle outer data with its own handlers</summary>
    public virtual async Task<bool> HandleReceive(IByteExchanger node, EndPoint remote, Stream b) {
        var connector = GetConnector(remote);
        if (connector == null) throw new Exception("Connector can not be null");
        var packet = new ExchangePacket(node, connector, b);
        return await EventCallAsync(onReceive, this, packet);
    }
    /// <inheritdoc />
    public ExchangePacket CreatePacket(EndPoint? remote = null, int reconnectTimeout = -2) => new(this, GetConnector(remote) ?? throw new Exception("Connector is null"));

    /// <inheritdoc />
    public virtual ExchangeConnector? GetConnector(EndPoint? remote = null, int reconnectTimeout = -2) {
        if (!Config.IsServer) remote = GetClientEndPoint(remote);
        else if (remote == null) return null;
        if (!Remotes.TryGetValue(remote, out var rVal)) {
            if (reconnectTimeout == -2) reconnectTimeout = Config.AutoReconnectClientTimeout; //Reconnect
            if (reconnectTimeout == -2) return null;
            if (Config.IsServer) {
                if (!HasStarted && !Start(reconnectTimeout)) return null; //Server start
            } else {
                if (!HasStarted && !Start(reconnectTimeout)) return null; //Client Start
                if (!IsConnectedTo(remote)) { //Client Restart
                    Stop();
                    if (!Start(reconnectTimeout)) return null;
                }
            }
            if (!Remotes.TryGetValue(remote, out rVal)) return null; //Still no connection 
        }
        return rVal;
    }

    /// <inheritdoc />
    public virtual EndPoint GetClientEndPoint(EndPoint? remote = null) {
        if (!Config.IsServer) return Config.BindAddress[0];
        if (remote == null) throw new ArgumentNullException(nameof(remote));
        return remote;
    }
    /// <inheritdoc />
    public virtual bool IsConnectedTo(EndPoint endpoint) {
        if (Remotes == null) return false;
        return Remotes.ContainsKey(endpoint);
    }
    /// <inheritdoc />
    public virtual void Disconnect(EndPoint? endPoint = null) {
        endPoint = GetClientEndPoint(endPoint);
        if (endPoint != null) {
            Remotes.TryRemove(endPoint, out var connector);
            if (!Config.IsServer || !Config.IsConnectionless) connector?.Dispose(); //Udp server should not dispose socket because it is also listener
        }
        if (!Config.IsConnectionless && !Config.IsServer) { //TcpClient disconnect
            Stop();
        } else {
            EventCall(onDisconnect, this, new ExchangeEventArgs(endPoint));
        }
    }
    #endregion

    #region EventCall
    /// <inheritdoc />
    public virtual bool EventCall<TEventArgs>(EventHandlerAsync<TEventArgs>? handler, object node, TEventArgs args) {
        try {
            handler?.Invoke(node, args);
            return true;
        } catch (Exception exp) {
            onError?.Invoke(this, new ExchangeEventArgs(null, new ByteExchangeException("Could not perform outer event call", exp)));
            return false;
        }
    }
    /// <inheritdoc />
    public virtual bool EventCall<TEventArgs>(EventHandler<TEventArgs>? handler, object node, TEventArgs args) {
        try {
            handler?.Invoke(node, args);
            return true;
        } catch (Exception exp) {
            onError?.Invoke(this, new ExchangeEventArgs(null, new ByteExchangeException("Could not perform outer event call", exp)));
            return false;
        }
    }
    /// <inheritdoc />
    public virtual async Task<bool> EventCallAsync<TEventArgs>(EventHandler<TEventArgs>? handler, object node, TEventArgs args) =>
        await Task.Run(() => EventCall(handler, node, args)).ConfigureAwait(false);
    /// <inheritdoc />
    public virtual async Task<bool> EventCallAsync<TEventArgs>(EventHandlerAsync<TEventArgs>? handler, object node, TEventArgs args) =>
        await Task.Run(() => EventCall(handler, node, args)).ConfigureAwait(false);
    #endregion

    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override async ValueTask DisposeAsync(bool disposing) {
        if (disposing) {
            DetachEvents();
            if (LocalEndPoints != null) {
                foreach (var connector in LocalEndPoints.Values) if (connector != null) await connector.DisposeAsync();
                LocalEndPoints.Clear();
                //LocalEndPoints = null;
            }
            if (Remotes != null) {
                foreach (var connector in Remotes.Values) if (connector != null) await connector.DisposeAsync();
                Remotes.Clear();
                //Remotes = null;
            }
            if (!Config.IsServer) await EventCallAsync(onDisconnect, this, ExchangeEventArgs.Empty);
            await EventCallAsync(onStop, this, ExchangeEventArgs.Empty);
            //if (ExtraSecurity != null) ExtraSecurity.Dispose();
        }
        // Call the appropriate methods to clean up
        // unmanaged resources here.
        HasStarted = false;
    }
    #endregion Dispose
}
#endregion