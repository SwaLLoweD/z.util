/* Copyright (c) <2008> <A. Zafer YURDA�ALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Z.Extensions;

namespace Z.Net.ByteExchange;

/// <summary>
///     Byte Stream Request for sync operation.. Needs to be flushed to work and closed to resume normal stream
///     operation
/// </summary>
public class ExchangeRequest<TOpCode> : ExchangePacket<TOpCode> where TOpCode : notnull
{
    #region Fields / Properties
    /// <summary>Request Id</summary>
    public Guid Id { get; protected set; }
    private AutoResetEvent Handle { get; }
    private AutoResetEvent HandleResponse { get; }
    private int Timeout { get; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    public ExchangeRequest(TOpCode opCode, IOpCoder<TOpCode> node, ExchangeConnector connector, int timeout = -2) : base(node.RequestOpCode, node, connector) {
        if (timeout == -2) timeout = node.Transport.Config.Packet.RequestTimeout;
        this.Timeout = timeout;
        Id = Guid.NewGuid();
        Handle = new AutoResetEvent(false);
        HandleResponse = new AutoResetEvent(false);
        OpCode = opCode;
        NoExceptions = false;
        this.Writez(Id);
        this.Writez(opCode);
    }
    #endregion

    #region Response
    /// <summary>Receive the response for current request</summary>
    protected internal void ReceiveResponse(ExchangePacket s) {
        Stream = s.Stream;
        Handle.Set();
        if (!HandleResponse.WaitOne(Timeout)) Close(); //Timeout this request
    }
    #endregion

    #region Flush
    ///<summary>Flush All Data into the stream async.</summary>
    public async Task<Exception?> FlushAndWaitAsync() {
        try {
            var result = await FlushAndWaitAsyncInner();
            if (!result) throw new ByteExchangeException("Flush failed");
        } catch (Exception exp) {
            return exp;
        }
        return null;
    }
    /// <summary>Flushes the request and waits for response until timeout is elapsed</summary>
    public Exception? FlushAndWait() {
        try {
            var result = FlushAndWaitInner();
            if (!result) throw new ByteExchangeException("Flush failed");
        } catch (Exception exp) {
            return exp;
        }
        return null;
    }
    ///<summary>Flush All Data into the stream async.</summary>
    protected virtual async Task<bool> FlushAndWaitAsyncInner() {
        if (Node == null || Transport?.Config == null) throw new ByteExchangeException("Exchanger can not be null");
        if (Stream == null) throw new ByteExchangeException("Write stream can not be null");
        if (!Stream.CanWrite) throw new ByteExchangeException("Write stream can not be written");
        //if (!Transport.IsConnectedTo(Remote)) throw new ByteExchangeException("Remote is not connected");
        Node.AddRequest(this);
        Handle.Reset();
        try {
            //if (Stream.ReadTimeout == -1 && Stream.CanTimeout) Stream.ReadTimeout = timeout;
            if (!await SendAsync().ConfigureAwait(false)) throw new ByteExchangeException("Send failed");
        } catch (Exception) {
            Node?.RemoveRequest(Id);
            throw;
        }
        if (Transport.Config.IsStream) //ByteStreamNodes block read stream per handler so we need to re-trigger readOpCode
            _ = Task.Run(() => Node.HandleReceive(this));
        var timedOut = !(await Handle.WaitOneAsync(Timeout).ConfigureAwait(false));
        Node?.RemoveRequest(Id);
        if (Node == null || Transport == null) throw new ByteExchangeException("Exchanger can not be null");
        if (Stream == null) throw new ByteExchangeException("Write stream can not be null");
        else if (!Stream.CanRead) throw new ByteExchangeException("Read stream can not be read");
        //if (!Transport.IsConnectedTo(Remote)) throw new ByteExchangeException("Remote is not connected");
        if (timedOut) throw new TimeoutException("Request timed out");
        return true;
    }
    ///<summary>Flush All Data into the stream async.</summary>
    protected virtual bool FlushAndWaitInner() {
        if (Node == null || Transport?.Config == null) throw new ByteExchangeException("Exchanger can not be null");
        if (Stream == null) throw new ByteExchangeException("Write stream can not be null");
        if (!Stream.CanWrite) throw new ByteExchangeException("Write stream can not be written");
        //if (!Transport.IsConnectedTo(Remote)) throw new ByteExchangeException("Remote is not connected");
        Node.AddRequest(this);
        Handle.Reset();
        try {
            //if (Stream.ReadTimeout == -1 && Stream.CanTimeout) Stream.ReadTimeout = timeout;
            if (!Send()) throw new ByteExchangeException("Send failed");
        } catch (Exception) {
            Node?.RemoveRequest(Id);
            throw;
        }
        if (Transport.Config.IsStream) //ByteStreamNodes block read stream per handler so we need to re-trigger readOpCode
            _ = Task.Run(() => Node.HandleReceive(this));
        var timedOut = !Handle.WaitOne(Timeout);
        Node.RemoveRequest(Id);
        if (Node == null || Transport == null) throw new ByteExchangeException("Exchanger can not be null");
        if (Stream == null) throw new ByteExchangeException("Write stream can not be null");
        else if (!Stream.CanRead) throw new ByteExchangeException("Read stream can not be read");
        //if (!Transport.IsConnectedTo(Remote)) throw new ByteExchangeException("Remote is not connected");
        if (timedOut) throw new TimeoutException("Request timed out");
        return true;
    }
    #endregion

    /// <summary>Closes and the request. Releases the stream for normal operation</summary>
    public override void Close() {
        try {
            Node?.RemoveRequest(Id);
            Handle?.Set();
            HandleResponse?.Set();
        } catch { }

        //base.Close(); //Calls dispose
    }

    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override void Dispose(bool disposing) {
        lock (lockDispose) {
            if (!_disposed) {
                if (disposing) {
                    //DetachEvents();
                    //Close();
                    //Node?.RemoveRequest(Id);
                    try {
                        Node?.RemoveRequest(Id);
                        if (Handle != null) {
                            Handle.Set();
                            Handle.Dispose();
                        }
                        if (HandleResponse != null) {
                            HandleResponse.Set();
                            HandleResponse.Dispose();
                        }
                    } catch { }
                }
                // Call the appropriate methods to clean up
                // unmanaged resources here.
            }
        }
        base.Dispose(disposing);
        _disposed = true;
    }
    #endregion Dispose 

}