﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Z.Util;

namespace Z.Net.ByteExchange;

/// <summary>DataExchange Session object</summary>
public class ExchangeConnector : DisposableZ
{
    #region Fields / Properties
    /// <summary>Exchange Session</summary>
    public ExchangeSession Session { get; set; }
    /// <summary>Exchange Connector</summary>
    public object Connector { get; set; }
    /// <summary>Exchange Stream</summary>
    public Stream? Stream { get; set; }
    /// <summary>Connector Remote</summary>
    public EndPoint Remote { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    public ExchangeConnector(object connector, EndPoint remote) {
        Connector = connector;
        Remote = remote;
        Session = new ExchangeSession();
    }
    #endregion

    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override async ValueTask DisposeAsync(bool disposing) {
        if (disposing) {
            if (Connector is IDisposable connectorDisposable) connectorDisposable.Dispose();
            //Connector = null;
            if (Session != null) await Session.DisposeAsync();
            //Session = null;
            Stream?.Dispose();
            Stream = null;
            //if (ExtraSecurity != null) ExtraSecurity.Dispose();
        }
    }
    #endregion Dispose
}
