﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Z.Cryptography;

namespace Z.Net.ByteExchange;

/// <summary>Async Socket configuration</summary>
[Serializable]
public class ExchangeConfig
{
    #region Fields / Properties
    /// <summary>Server IP Address(es)</summary>
    public EndPoint[] BindAddress { get; set; }
    /// <summary>Extra Security Options</summary>
    public ConfigSecurity ExtraSecurity { get; protected set; }
    /// <summary>Node is Server Or Client</summary>
    public bool IsServer { get; set; }
    /// <summary>Operates in stream or packet mode</summary>
    public bool IsStream { get; set; }
    /// <summary>Operates in stream or packet mode</summary>
    public bool IsConnectionless { get; set; } = false;
    /// <summary>Auto-Reconnect for sends for client reconnect timeout in ms (-2 = disabled, -1 = infinite)</summary>
    public int AutoReconnectClientTimeout { get; set; } = -2;
    /// <summary>Packet Config Parameters</summary>
    public ConfigPacket Packet { get; protected set; }
    /// <summary>SSL Tunnel Config Parameters</summary>
    public ConfigSsl Ssl { get; protected set; }
    /// <summary>Stream Config Parameters</summary>
    public ConfigStream Stream { get; protected set; }
    #endregion

    #region Constructors
    /// <summary>Async Socket configuration</summary>
    public ExchangeConfig(EndPoint bindAddress, bool isServer = false, bool isStream = false, bool useSSL = false): this(isServer, isStream, useSSL, new EndPoint[] {bindAddress}){}
    /// <summary>Async Socket configuration</summary>
    public ExchangeConfig(bool isServer, bool isStream, bool useSSL, params EndPoint[] bindAddresses) {
        BindAddress = bindAddresses.ToArray();
        IsServer = isServer;
        Packet = new ConfigPacket(this);
        Stream = new ConfigStream(this);
        Ssl = new ConfigSsl(this);
        ExtraSecurity = new ConfigSecurity(this);
        Ssl.Enabled = useSSL;
        IsStream = isStream;
    }
    #endregion

    #region Nested type: ExchangeConfigSubClass
    /// <summary>Config subClass</summary>
    [Serializable]
    public class ExchangeConfigSubClass
    {
        /// <summary>Base Config</summary>
        protected virtual ExchangeConfig Cfg { get; set; }
        /// <summary>Constructor</summary>
        public ExchangeConfigSubClass(ExchangeConfig cfg) => Cfg = cfg;
    }
    #endregion

    #region Nested type: ConfigPacket
    /// <summary>Async Socket Options configuration</summary>
    [Serializable]
    public class ConfigPacket : ExchangeConfigSubClass
    {
        #region Fields / Properties
        /// <summary>Buffer size in bytes</summary>
        public int BufferSize { get; set; } = 4096;
        /// <summary>Maximum allowed packet size in bytes (-1 = unlimited)</summary>
        public int MaxPacketSize { get; set; } = 50 * 1024 * 1024; //50 MBytes
        /// <summary>Delegate to pre-process Data to be Received</summary>
        public Func<ExchangePacket?, ExchangePacket?>? FuncReceive { get; set; }
        /// <summary>Delegate to pre-process Data to be Sent</summary>
        public Func<ExchangePacket, ExchangePacket>? FuncSend { get; set; }
        /// <summary>Uses specified sequence as an eof indicator.. if left null, packet size headers are used for packet integrity</summary>
        public ReadOnlyMemory<byte> PacketEof { get; set; } // No PacketEof = UsePacketSizeHeader
        /// <summary>Default request timeout</summary>
        public int RequestTimeout { get; set; } = 2000;
        /// <summary>Maximum request timeout</summary>
        public int RequestTimeoutMax { get; set; } = 60000;
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConfigPacket(ExchangeConfig cfg) : base(cfg) { }
        #endregion
    }
    #endregion

    #region Nested type: ConfigSecurity
    /// <summary>Extra security measures</summary>
    [Serializable]
    public class ConfigSecurity : ExchangeConfigSubClass
    {
        #region Fields / Properties
        /// <summary>Diffie-Hellman Key exchange bits (Used only if Encrypter is set)</summary>
        public int DiffieHellmanBits { get; set; }

        /// <summary>Symmetric encryption to use</summary>
        public IEncryptor? Encrypter { get; set; }
        /// <summary>Maximum keyExchange packet count</summary>
        public int KeyExchangePacketsMax { get; set; }

        /// <summary>Minimum keyExchange packet count</summary>
        public int KeyExchangePacketsMin { get; set; }
        /// <summary>Packet scrambler</summary>
        public Util.Scrambler2? Scrambler { get; set; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConfigSecurity(ExchangeConfig cfg) : base(cfg) {
            DiffieHellmanBits = 256;
            KeyExchangePacketsMax = 250;
            KeyExchangePacketsMin = 100;
        }
        #endregion
    }
    #endregion

    #region Nested type: ConfigSsl
    /// <summary>Async Socket SSL configuration</summary>
    [Serializable]
    public class ConfigSsl : ExchangeConfigSubClass
    {
        #region Fields / Properties
        private X509Certificate2? localCertificate;

        /// <summary>Constructor</summary>
        public bool CheckRevocation { get; set; } //For SSL Connection
        /// <summary>SSL Stream is enabled or not</summary>
        public bool Enabled { get; set; }
        /// <summary>x509 certificate to use for server</summary>
        public X509Certificate2? LocalCertificate {
            get => localCertificate;
            set {
                localCertificate = value;
                Enabled = true;
            }
        } //For SSl Conn

        /// <summary>Local Certificate Selection Delegate</summary>
        public LocalCertificateSelectionCallback? LocalCertSelector { get; set; }

        /// <summary>Constructor</summary>
        public SslProtocols Protocol { get; set; } //For SSl Conn

        //Only for .Net 4.5
        ///// <summary>Encryption Policy to be used, Default: Require Encryption</summary>
        //public EncryptionPolicy EncryptionPolicy { get; set; } // For SSL Connection

        /// <summary>Remote Certificate Validation Delegate</summary>
        public RemoteCertificateValidationCallback? RemoteCertValidator { get; set; }

        /// <summary>true if clients should have a certificate</summary>
        public bool RequireRemoteCertificate { get; set; } //For SSl Conn

        /// <summary>SSL Socket Server Name String</summary>
        public string? ServerName { get; set; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConfigSsl(ExchangeConfig cfg) : base(cfg) {
            RequireRemoteCertificate = false;
            Protocol = SslProtocols.None;
        }
        #endregion
    }
    #endregion

    #region Nested type: ConfigStream
    /// <summary>Async Socket Options configuration</summary>
    [Serializable]
    public class ConfigStream : ExchangeConfigSubClass
    {
        #region Fields / Properties
        /// <summary>Buffer size in bytes</summary>
        public int BufferSize { get; set; }
        ///// <summary>Encapsulate NetworkStream in a BufferedStream for performance</summary>
        //public bool UseBufferedStream { get; set; } 

        /// <summary>ReadTimeout in msecs (default=2000)</summary>
        public int ReadTimeout { get; set; }
        /// <summary>WriteTimeout in msecs (default=5000)</summary>
        public int WriteTimeout { get; set; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConfigStream(ExchangeConfig cfg) : base(cfg) {
            BufferSize = 8192;
            ReadTimeout = -2;
            WriteTimeout = -2;
            //UseBufferedStream = true;
        }
        #endregion

        /// <summary>Applies current config to a stream</summary>
        protected internal void ApplyStreamConfigTo(Stream stream) {
            if (stream.CanTimeout) {
                if (ReadTimeout > -2) stream.ReadTimeout = ReadTimeout;
                if (WriteTimeout > -2) stream.WriteTimeout = WriteTimeout;
            }
        }
    }
    #endregion
    // #region Config / Create
    // /// <summary>FluentConfig</summary>
    // public ByteExchangeConfig Config(Action<ByteExchangeConfig> configAction) {
    //     configAction(this);
    //     return this;
    // }
    // #endregion
}