﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Net;

#pragma warning disable
namespace Z.Net.ByteExchange
{
    /// <summary>Exception arguments for DataExchange</summary>
    public class ByteExchangeException : Exception
    {
        #region Fields / Properties
        /// <summary>Data related with the event</summary>
        public Stream? EventData { get => Data["EventData"] as Stream; set => Data["EventData"] = value; }
        /// <summary>Remote Endpoint</summary>
        public EndPoint? Remote { get => Data["Remote"] as EndPoint; set => Data["Remote"] = value; }
        #endregion

        #region Constructors
        /// <summary>Exception initializer</summary>
        /// <param name="message">Exception Message</param>
        /// <param name="innerException">Inner Exception</param>
        /// <param name="remote">Remote Endpoint</param>
        /// <param name="data">Data related with the event</param>
        public ByteExchangeException(string? message = null, Exception? innerException = null, EndPoint? remote = null, Stream? data = null)
            : base(message, innerException) {
            EventData = data;
            Remote = remote;
        }
        #endregion
    }
}