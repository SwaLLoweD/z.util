﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Net;

namespace Z.Net.ByteExchange;

    /// <summary>Event arguments for DataExchange</summary>
    public class ExchangeEventArgs<TArg> : ExchangeEventArgs
{
    #region Fields / Properties
    /// <summary>Empty event argument for DataExchange</summary>
    public new static ExchangeEventArgs<TArg> Empty => new();
    /// <summary>Arguments related with the event</summary>
    public TArg? Args { get; set; }
    #endregion

    #region Constructors
    /// <summary>Event argument initializer</summary>
    /// <param name="remote">Remote Endpoint</param>
    /// <param name="args">Arguments</param>
    /// <param name="exp">Returned exception if any</param>
    public ExchangeEventArgs(EndPoint? remote = null, TArg? args = default, ByteExchangeException? exp = null) : base(remote, exp) => Args = args;
    #endregion
}

/// <inheritdoc />
public class ExchangeConnectorEventArgs : ExchangeEventArgs
{
    #region Fields / Properties
    /// <summary>Connector related with the event</summary>
    public ExchangeConnector Connector { get; set; }
    /// <summary>Stream related with the event</summary>
    public Stream? Stream => Connector.Stream;
    #endregion

    #region Constructors
    /// <summary>Event argument initializer</summary>
    /// <param name="connector"></param>
    /// <param name="exp">Returned exception if any</param>
    public ExchangeConnectorEventArgs(ExchangeConnector connector, Exception? exp = null) : base(connector.Remote, exp) { Connector = connector; }
    #endregion
}

/// <summary>Event arguments for DataExchange</summary>
public class ExchangeEventArgs : EventArgs
{
    #region Fields / Properties
    /// <summary>Empty event argument for DataExchange</summary>
    public new static ExchangeEventArgs Empty => new();

    /// <summary>Exception related with the event</summary>
    public virtual Exception? Exception { get; set; }
    /// <summary>Remote Endpoint</summary>
    public virtual EndPoint? Remote { get; set; }
    #endregion

    #region Constructors
    /// <summary>Event argument initializer</summary>
    /// <param name="remote">Remote Endpoint</param>
    /// <param name="exp">Returned exception if any</param>
    public ExchangeEventArgs(EndPoint? remote = null, Exception? exp = null) {
        Remote = remote;
        Exception = exp;
    }
    #endregion
}

/// <summary>Event arguments for DataExchange</summary>
public class ExchangePacketEventArgs : ExchangeEventArgs
{
    #region Fields / Properties
    /// <summary>Empty event argument for DataExchange</summary>
    public new static ExchangePacketEventArgs Empty => new();
    /// <summary>Received Packet</summary>
    public ExchangePacket? Packet { get; set; }
    #endregion

    #region Constructors
    /// <summary>Event argument initializer</summary>
    /// <param name="packet">Packet</param>
    /// <param name="exp">Returned exception if any</param>
    public ExchangePacketEventArgs(ExchangePacket? packet = null, ByteExchangeException? exp = null) : base(packet?.Remote, exp) => Packet = packet;
    #endregion
}

/// <summary>Event arguments for DataExchange</summary>
public class OpCodeReceivedEventArgs : ExchangePacketEventArgs
{
    #region Fields / Properties
    /// <summary>Empty event argument for DataExchange</summary>
    public new static OpCodeReceivedEventArgs Empty => new();
    /// <summary>Received OpCode</summary>
    public object? OpCode { get; set; }
    #endregion

    #region Constructors
    /// <summary>Event argument initializer</summary>
    /// <param name="opCode">OpCode</param>
    /// <param name="packet">Packet</param>
    /// <param name="exp">Returned exception if any</param>
    public OpCodeReceivedEventArgs(object? opCode = null, ExchangePacket? packet = null, ByteExchangeException? exp = null) : base(packet, exp) => OpCode = opCode;
    #endregion
}