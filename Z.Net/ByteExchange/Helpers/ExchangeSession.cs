﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Net;

namespace Z.Net.ByteExchange
{
    /// <summary>DataExchange Session object</summary>
    public class ExchangeSession : Z.Util.DisposableZ
    {
        #region Fields / Properties
        /// <summary>User or session id</summary>
        public int Id { get; set; }
        /// <summary>Session or auth token</summary>
        public string? Token { get; set; }
        /// <summary>Data object</summary>
        public object? Data { get; set; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ExchangeSession() { }
        #endregion

        #region Dispose
        /// <summary>Dispose internals</summary>
        protected override async ValueTask DisposeAsync(bool disposing) {
            if (disposing) {
                await Task.Run(() => (Data as IDisposable)?.Dispose());
                //if (ExtraSecurity != null) ExtraSecurity.Dispose();
            }
        }
        #endregion Dispose
    }
}