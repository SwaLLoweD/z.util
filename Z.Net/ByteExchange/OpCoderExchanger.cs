/* Copyright (c) <2020> <A. Zafer YURDACALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using Z.Collections.Generic;
namespace Z.Net.ByteExchange;

#region OpCoderExchanger
/// <summary>Node for OpCode based byte[] communication</summary>
public class OpCodeExchanger<TOpCode> : DisposableZ, IOpCoder<TOpCode> where TOpCode : notnull
{
    #region Fields / Properties
    /// <inheritdoc />
    public virtual IByteExchanger Transport { get; protected set; }
    /// <inheritdoc />
    public TOpCode RequestOpCode { get; protected set; }
    /// <inheritdoc />
    public TOpCode ResponseOpCode { get; protected set; }
    private readonly CacheTable<Guid, ExchangeRequest<TOpCode>> requests;

    /// <summary>Packet Handler table</summary>
    protected virtual IDictionary<TOpCode, Func<ExchangePacket, Task<bool>>> Handlers { get; set; } = new Dictionary<TOpCode, Func<ExchangePacket, Task<bool>>>();
    /// <summary>Request Handler table</summary>
    protected virtual IDictionary<TOpCode, Func<ExchangeResponse<TOpCode>, Task<bool>>> RHandlers { get; set; } = new Dictionary<TOpCode, Func<ExchangeResponse<TOpCode>, Task<bool>>>();
    #endregion

    #region Constructors
    /// <summary>Node Initializer</summary>
    /// <param name="transport">Transport Node</param>
    /// <param name="requestOpCode">Request OpCode</param>
    /// <param name="responseOpCode">Response OpCode</param>
    public OpCodeExchanger(IByteExchanger transport, TOpCode requestOpCode, TOpCode responseOpCode) {
        Transport = transport;
        if (transport == null) throw new ArgumentNullException(nameof(transport));
        var opCodeType = typeof(TOpCode);
        if (!opCodeType.Is().IntegerBased && (!opCodeType.Is().Enum || !Enum.GetUnderlyingType(opCodeType).Is().IntegerBased)) throw new ArgumentException("OpCode must be integer based");
        requests = new CacheTable<Guid, ExchangeRequest<TOpCode>>(Transport.Config.Packet.RequestTimeoutMax);
        RequestOpCode = requestOpCode;
        ResponseOpCode = responseOpCode;
        Handlers[RequestOpCode] = HandleRequest;
        Handlers[ResponseOpCode] = HandleResponse;
    }
    #endregion

    #region Events
    /// <summary>onError</summary>
    protected EventHandler<ExchangeEventArgs>? onError;
    /// <summary>onOpCodeReceived</summary>
    protected EventHandler<OpCodeReceivedEventArgs>? onOpCodeReceived;
    /// <inheritdoc />
    public event EventHandler<OpCodeReceivedEventArgs> OnOpCodeReceived {
        add => onOpCodeReceived += value;
        remove {
            if (onOpCodeReceived != null) onOpCodeReceived -= value;
        }
    }
    /// <inheritdoc />
    public event EventHandler<ExchangeEventArgs> OnError {
        add => onError += value;
        remove {
            if (onError != null) onError -= value;
        }
    }
    /// <inheritdoc />
    public virtual void DetachEvents() {
        //lock (eventLock) {
        onError = null;
        onOpCodeReceived = null;
        //}
    }
    /// <inheritdoc />
    protected virtual void OnErrorCatcher(object? s, ExchangeEventArgs b) => Transport.EventCall(onError, this, b);
    #endregion

    #region Start Stop
    /// <inheritdoc />
    public virtual bool Start(int waitTimeout = -2) {
        if (Transport == null) return false;
        if (!Transport.Start(waitTimeout)) return false;
        if (Transport.Config.IsStream)
            Transport.OnConnect += ByteStreamNode_OnConnect;
        else
            Transport.OnReceive += ByteTransferNode_OnReceive;
        if (RequestOpCode != null) Transport.OnDisconnect += ByteExchangeNode_OnDisconnect;

        Transport.OnError += OnErrorCatcher;
        return true;
    }
    #endregion

    #region Handler operations
    /// <summary>Add Stream Handler for specified opcode</summary>
    public virtual void AddHandler(TOpCode opCode, Func<ExchangePacket, Task<bool>> handler) => Handlers[opCode] = handler;
    /// <summary>Add Stream Request Handler for specified opcode</summary>
    public virtual void AddRequestHandler(TOpCode opCode, Func<ExchangeResponse<TOpCode>, Task<bool>> handler) => RHandlers[opCode] = handler;
    // /// <summary>Set the opcode to be used for requests</summary>
    // public virtual void SetRequestOpCode(TOpCode reqOpCode, TOpCode respOpCode) {
    //     RequestOpCode = reqOpCode;
    //     ResponseOpCode = respOpCode;
    //     Handlers[RequestOpCode] = HandleRequest;
    //     Handlers[respOpCode] = HandleResponse;
    // }
    private async Task ByteTransferNode_OnReceive(object? sender, ExchangePacket e) {
        try {
            if (!await HandleReceive(e)) Transport.Disconnect(e.Remote);
        } catch (Exception exp) {
            Transport.EventCall(onError, this, new ExchangeEventArgs(e.Remote, new ByteExchangeException("Stream could not be opened for read/write", exp, e.Remote)));
            Transport.Disconnect(e.Remote);
            return;
        }
    }
    private void ByteExchangeNode_OnDisconnect(object? sender, ExchangeEventArgs e) {
        if (!Transport.Config.IsServer) {
            requests.Clear();
        } else {
            var reqRemove = requests.Where(x => x.Value.DateCreated <= DateTime.UtcNow.AddMinutes(-30)).Select(x => x.Value.Id).ToList();
            reqRemove.ForEach(x => requests.Remove(x));
        }
    }
    private async Task ByteStreamNode_OnConnect(object? sender, ExchangeConnectorEventArgs e) =>
        await Task.Run(async delegate { //New thread per connection which reads opcodes after each handler is done processing.
            var packet = new ExchangePacket(Transport, e.Connector, true);
            while (e.Stream?.CanRead == true && await HandleReceive(packet)) { }
            Transport.Disconnect(e.Remote);
        }).ConfigureAwait(false);

    /// <summary>Node tries to handle outer data with its own handlers</summary>
    public virtual async Task<bool> HandleReceive(ExchangePacket e) {
        if (!TryReadOpCode(e, out var opCode) || opCode == null) return false;
        if (!Handlers.TryGetValue(opCode, out var handler) || handler == null) {
            await Transport.EventCallAsync(onError, this, new ExchangeEventArgs(e.Remote, new ByteExchangeException($"Received unhandled opcode: {opCode}")));
            return false;
        }
        try {
            if (!await handler(e)) return false;
        } catch (Exception exp) {
            await Transport.EventCallAsync(onError, this, new ExchangeEventArgs(e.Remote, new ByteExchangeException($"Opcode handler failed: {opCode}", exp, e.Remote)));
            return false;
        }

        return true;
    }

    /// <summary>Handle incoming request stream</summary>
    protected virtual async Task<bool> HandleRequest(ExchangePacket b) {
        var reqId = b.Readz<Guid>();
        if (!TryReadOpCode(b, out var opCode) || opCode == null) return false;
        if (!RHandlers.TryGetValue(opCode, out var handler) || handler == null) {
            await Transport.EventCallAsync(onError, this, new ExchangeEventArgs(b.Remote, new ByteExchangeException($"Received unhandled request opcode: {opCode}")));
            return false;
        }

        try {
            using var req = b.CreateResponseForServer(reqId, opCode, this);
            if (!await handler(req)) return false;
        } catch (Exception exp) {
            await Transport.EventCallAsync(onError, this, new ExchangeEventArgs(b.Remote, new ByteExchangeException($"Opcode request handler failed: {opCode}", exp, b.Remote)));
            return false;
        }

        return true;
    }

    /// <summary>Handle incoming response stream</summary>
    protected virtual async Task<bool> HandleResponse(ExchangePacket b) {
        var reqId = await b.ReadzAsync<Guid>();
        if (!requests.TryGetValue(reqId, out var req) || req == null || !TryReadOpCode(b, out var opCode) || opCode == null || !opCode.Equals(req.OpCode))
            return false;
        req.ReceiveResponse(b);
        return true;
    }

    /// <summary>Read Incoming OpCode</summary>
    protected virtual bool TryReadOpCode(ExchangePacket data, out TOpCode? opCode) {
        opCode = default;
        var timeout = Transport.Config.IsStream ? -1 : 2000; //Stream should wait forever, but others are instantaneous requests
        try {
            opCode = data.Readz<TOpCode>(timeout, 1024); //limit read bytes for safety
            if (opCode != null && !(opCode.Equals(RequestOpCode) || opCode.Equals(ResponseOpCode))) Transport.EventCall(onOpCodeReceived, this, new OpCodeReceivedEventArgs(opCode, data));
        } catch (Exception exp) {
            switch (exp) {
                case ObjectDisposedException _:
                case IOException _:
                    return false; //stream is disposed so no error is necessary
                case AggregateException agg: return false;
                default:
                    //Unhandled exception
                    Transport.EventCall(onError, this, new ExchangeEventArgs(data.Remote, new ByteExchangeException("Read OpCode failed", exp, data.Remote)));
                    return false;
            }
        }

        return true;
    }
    #endregion

    #region Requests
    /// <inheritdoc />
    public ExchangePacket<TOpCode> CreatePacket(TOpCode opCode, EndPoint? remote = null, int reconnectTimeout = -2)
        => new(opCode, this, Transport.GetConnector(remote, reconnectTimeout) ?? throw new NullReferenceException("Remote can not be found or not connected"));

    /// <summary>Create a new Request (The Request must be closed for normal reads to continue)</summary>
    public virtual ExchangeRequest<TOpCode> CreateRequest(TOpCode opCode, EndPoint? remote = null, int timeout = -2, int reconnectTimeout = -2) =>
        new(opCode, this, Transport.GetConnector(remote, reconnectTimeout) ?? throw new NullReferenceException("Remote can not be found or not connected"), timeout);

    /// <summary>Add a new request to tracking table</summary>
    public void AddRequest(ExchangeRequest<TOpCode> req) => requests[req.Id] = req;
    /// <summary>Remove a request from tracking table</summary>
    public bool RemoveRequest(Guid id) {
        requests.Remove(id);
        return true;
    }
    #endregion

    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override async ValueTask DisposeAsync(bool disposing) {
        if (disposing) {
            DetachEvents();
            if (Transport != null) {
                if (Transport.Config.IsStream)
                    Transport.OnConnect -= ByteStreamNode_OnConnect;
                else
                    Transport.OnReceive -= ByteTransferNode_OnReceive;
                Transport.OnError -= OnErrorCatcher;
                await Transport.DisposeAsync();
            }
            requests?.Clear();
            //Transport = null;
            //if (ExtraSecurity != null) ExtraSecurity.Dispose();
        }
    }
    #endregion Dispose
}
#endregion