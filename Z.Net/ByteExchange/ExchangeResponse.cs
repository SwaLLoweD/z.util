/* Copyright (c) <2008> <A. Zafer YURDA�ALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Z.Extensions;

namespace Z.Net.ByteExchange;

/// <summary>
///     Byte Stream Request for sync operation.. Needs to be flushed to work and closed to resume normal stream
///     operation
/// </summary>
public class ExchangeResponse<TOpCode> : ExchangePacket<TOpCode> where TOpCode : notnull
{
    #region Fields / Properties
    /// <summary>Request Id</summary>
    public Guid Id { get; protected set; }
    #endregion

    #region Constructors

    /// <summary>Constructor for Response type situations</summary>
    internal ExchangeResponse(Guid id, TOpCode opCode, IOpCoder<TOpCode> node, ExchangeConnector connector, Stream stream) : base(opCode, node, connector, true) {
        Id = id;
        Stream = stream;
        NoExceptions = false;
        //handle = new AutoResetEvent(false);
    }
    #endregion

    #region Response
    /// <summary>Writes Response header</summary>
    public void StartResponse() {
        if (!Transport.Config.IsStream) {
            Stream.Position = 0;
            Stream.SetLength(0);
            if (Transport.Config.Packet.PacketEof.IsEmpty) this.Writez((long)0); //space for packet length
        }
        Stream.Writez(Node.ResponseOpCode);
        Stream.Writez(Id);
        Stream.Writez(OpCode);
    }
    #endregion
    /// <summary>Closes and the request. Releases the stream for normal operation</summary>
    public override void Close() {
        try {
            Node?.RemoveRequest(Id);
        } catch { }

        //base.Close(); //Calls dispose
    }

    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override void Dispose(bool disposing) {
        lock (lockDispose) {
            if (!_disposed) {
                if (disposing) {
                    //DetachEvents();
                    //Close();
                    //Node?.RemoveRequest(Id);
                    try {
                        Node?.RemoveRequest(Id);
                    } catch { }
                }
                // Call the appropriate methods to clean up
                // unmanaged resources here.
            }
        }
        base.Dispose(disposing);
        _disposed = true;
    }
    #endregion Dispose 
}