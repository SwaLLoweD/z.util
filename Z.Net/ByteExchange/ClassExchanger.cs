/* Copyright (c) <2020> <A. Zafer YURDACALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Data;
using System.Reflection;

namespace Z.Net.ByteExchange;

#region ClassExchanger
/// <summary>Base Class Node for byte[] communication</summary>
public class ClassExchanger: DisposableZ, IClassExchanger {
    #region Fields / Properties
    /// <inheritdoc />
    public virtual IByteExchanger Transport => OpCoder.Transport;
    /// <inheritdoc />
    public virtual IOpCoder<ushort> OpCoder { get; protected set; }
    /// <summary>Mapped classes for byte exchange</summary>
    protected IDictionary<Type, ushort> ClassMap { get; set; }
    /// <summary>Unmapped class handler for byte exchange</summary>
    protected IDictionary<string, Func<IClassExchanger, EndPoint, object?, Task<bool>>> Handlers { get; set; }
    #endregion

    #region Constructors
    /// <summary>Node Initializer</summary>
    /// <param name="transport">Transport</param>
    public ClassExchanger(ByteExchanger transport) : this(new OpCodeExchanger<ushort>(transport, 1, 2)) { }
    /// <summary>Node Initializer</summary>
    /// <param name="opcodeHandler">OpCodeHandler</param>
    public ClassExchanger(OpCodeExchanger<ushort> opcodeHandler) {
        OpCoder = opcodeHandler;
        ClassMap = new Dictionary<Type, ushort> {  //Space for request and response opcodes
                {typeof(DummyExchanger), 0},
                {typeof(DummyExchangeEndPoint), 1},
                {typeof(ExchangeEventArgs), 2} //Space for free form send and receives
            };
        Handlers = new Dictionary<string, Func<IClassExchanger, EndPoint, object?, Task<bool>>>();
        //opcodeHandler.SetRequestOpCode(0, 1);
        opcodeHandler.AddHandler(2, FreeTypeHandler);
    }
    #endregion

    #region Events
    /// <summary>onError</summary>
    protected EventHandler<ExchangeEventArgs>? onError;
    /// <inheritdoc />
    public event EventHandler<ExchangeEventArgs> OnError {
        add => onError += value;
        remove {
            if (onError != null) onError -= value;
        }
    }
    /// <inheritdoc />
    public virtual void DetachEvents() {
        //lock (eventLock) {
        onError = null;
        //}
    }
    /// <inheritdoc />
    protected virtual void OnErrorCatcher(object? s, ExchangeEventArgs b) => Transport.EventCall(onError, this, b);
    #endregion

    #region ClassMap
    /// <inheritdoc />
    public virtual void Map<T>() => Map(typeof(T));
    /// <inheritdoc />
    public virtual void Map(Type t) {
        var index = Convert.ToUInt16(ClassMap.Count);
        ClassMap.Add(t, index);
    }
    /// <inheritdoc />
    public virtual void MapAssembly<T>() where T : IExchangeable => MapAssembly(typeof(T));
    /// <inheritdoc />
    public virtual void MapAssembly(Type asmType) {
        foreach (var t in asmType.Assembly.ExportedTypes.Where(x => x.Is().HasInterface<IExchangeable>()).OrderBy(x => x.FullName)) Map(t);
    }
    #endregion

    #region Handlers
    /// <inheritdoc />
    public void AddHandler<T>(Func<IClassExchanger, EndPoint, T?, Task<bool>> handler) {
        var t = typeof(T);
        if (!ClassMap.TryGetValue(t, out var opCode)) {
            //if (!t.IsSerializable) throw new ArgumentException($"Type {t.Name} is not serializable and is not mapped");
            Handlers[t.FullName ?? "none"] = async (n, e, o) => await handler(n, e, (T?)o);
            return; //Goes to freetypehandler
        }
        OpCoder.AddHandler(opCode, async (s) => {
            T? tObj;
            if (t.Is().HasInterface<IExchangeable>()) {
                tObj = t.CreateInstance<T>();
                if (tObj is not IExchangeable tObjExchange) throw new Exception($"Cannot create object {t.Name}");
                tObjExchange.ExchangeStreamParse(s);
            } else {
                tObj = s.Readz<T>();
            }
            return await handler(this, s.Remote, tObj);
        });
    }
    /// <inheritdoc />
    public void AddRequestHandler<TReq, TResp>(ClassExchangeRequestHandler<TReq, TResp> handler) where TReq : IExchangeRequest<TResp> where TResp : IExchangeable {
        var tReq = typeof(TReq);
        //var tResp = typeof(TResp);
        if (!ClassMap.ContainsKey(tReq)) {
            var exp = new ByteExchangeException($"Request {tReq.Name} is not mapped");
            Transport.EventCall(onError, this, new ExchangeEventArgs(null, exp));
            throw exp;
        }
        //if (!ClassMap.ContainsKey(tResp)) Map(tResp);
        OpCoder.AddRequestHandler(ClassMap[tReq], async (s) => {
            try {
                TReq? tObj;
                if (tReq.Is().HasInterface<IExchangeable>()) {
                    tObj = tReq.CreateInstance<TReq>();
                    if (tObj is not IExchangeable tObjExchange) throw new Exception($"Cannot create object {tReq.Name}");
                    tObjExchange.ExchangeStreamParse(s);
                } else {
                    tObj = s.Readz<TReq>();
                }
                if (tObj == null) return false;
                if (!handler(this, s, tObj, out var response)) return false;
                s.StartResponse();
                response.ExchangeStreamFill(s);
                return s.Send();
            } catch (Exception exp) {
                await Transport.EventCallAsync(onError, this, new ExchangeEventArgs(s?.Remote, new ByteExchangeException($"Could process request {tReq.Name}", exp, s?.Remote, s)));
                return false;
            }
        });
    }

    /// <inheritdoc />
    private async Task<bool> FreeTypeHandler(ExchangePacket s) {
        var typeStr = s.Readz<string>();
        Type? t = null;
        if (typeStr != null) Assembly.GetEntryAssembly()?.GetType(typeStr, true);
        if (t == null || typeStr == null || !Handlers.TryGetValue(typeStr, out var sHandler)) {
            throw new ArgumentNullException($"Unknown type {typeStr} called.");
        }
        object? o;
        if (t.Is().HasInterface<IExchangeable>()) {
            o = t.CreateInstance();
            if (t is not IExchangeable tExchange) throw new Exception($"Cannot create object {t.Name}");
            tExchange.ExchangeStreamParse(s);
        } else {
            o = await s.ReadzAsync(t);
        }
        return await sHandler(this, s.Remote, o);
    }
    #endregion

    #region Start Stop
    /// <inheritdoc />
    public virtual bool Start(int waitTimeout = -2) {
        OpCoder.OnError += OnErrorCatcher;
        return OpCoder.Start(waitTimeout);
    }
    #endregion

    #region Send
    /// <inheritdoc />
    public virtual bool Send<TReq, TResp>(TReq req, out TResp? response, EndPoint? remote = null, bool async = false, int timeout = -2)
        where TReq : IExchangeRequest<TResp> where TResp : IExchangeable {
        var connector = Transport.GetConnector(remote);
        remote ??= connector?.Remote;
        var type = typeof(TReq);
        response = default;
        if (!ClassMap.TryGetValue(type, out var requestTypeOpCode)) {
            var exp = new ByteExchangeException($"Request {type.Name} is not mapped");
            Transport.EventCall(onError, this, new ExchangeEventArgs(remote, exp));
            throw exp;
        }
        if (connector == null) {
            var exp = new ByteExchangeException("Connector can not be null");
            Transport.EventCall(onError, this, new ExchangeEventArgs(remote, exp));
            throw exp;
        }
        //Check if request type
        using var s = new ExchangeRequest<ushort>(requestTypeOpCode, this.OpCoder, connector, timeout);
        req.ExchangeStreamFill(s);
        if (s.FlushAndWait() != null) return false;
        response = Activator.CreateInstance<TResp>();
        response.ExchangeStreamParse(s);
        //req.Response = response;
        return true;
    }
    /// <inheritdoc />
    public virtual bool Send<T>(T obj, EndPoint? remote, bool async = false, int timeout = -2) {
        var type = typeof(T);
        var connector = Transport.GetConnector(remote);
        if (connector == null) return false;
        if (!ClassMap.TryGetValue(type, out var requestTypeOpCode)) {
            //Non-mapped free form
            using var s = new ExchangePacket<ushort>(2, this.OpCoder, connector);
            s.Writez(type.FullName);
            s.Writez(obj);
            return s.Send(async);
        }
        if (obj is IExchangeable beObj) {
            //Check if request type
            var typeInterfaces = type.GetInterfaces();
            foreach (var typeInterface in typeInterfaces) {
                if (typeInterface != typeof(IExchangeRequest<>)) continue;
                var responseType = typeInterface.GetGenericArguments().FirstOrDefault();
                if (responseType == null || !ClassMap.TryGetValue(responseType, out var responseTypeOpCode)) {
                    Transport.EventCall(onError, this,
                        new ExchangeEventArgs(remote, new ByteExchangeException($"Request {type.Name} does not have its Response {responseType?.Name} mapped")));
                    return false;
                }
                using var s = new ExchangeRequest<ushort>(requestTypeOpCode, this.OpCoder, connector, timeout);
                beObj.ExchangeStreamFill(s);
                if (s.FlushAndWait() != null) return false;
                var response = responseType.CreateInstance();
                if (response is not IExchangeable respExchange) {
                    Transport.EventCall(onError, this, new ExchangeEventArgs(remote, new ByteExchangeException($"Request {responseType.Name} can not be mapped")));
                    return false;
                }
                respExchange.ExchangeStreamParse(s);
                type.GetProperty("Response")?.SetValue(beObj, response);
                return true;
            }
            //Non-request
            using (var s = new ExchangePacket<ushort>(requestTypeOpCode, this.OpCoder, connector)) {
                beObj.ExchangeStreamFill(s);
                return s.Send(async);
            }
        }
        //Non-request and non interface
        using (var s = new ExchangePacket<ushort>(requestTypeOpCode, this.OpCoder, connector)) {
            s.Writez(obj, type);
            return s.Send(async);
        }
    }
    #endregion

    #region Dispose
    /// <summary>Dispose internals</summary>
    protected override async ValueTask DisposeAsync(bool disposing) {
        if (disposing) {
            DetachEvents();
            //OpCoder.DetachEvents();
            if (OpCoder != null) await OpCoder.DisposeAsync();
            //OpCoder = null;
            //if (ExtraSecurity != null) ExtraSecurity.Dispose();
        }
    }
    #endregion Dispose
}
#endregion

/// <inheritdoc />
public delegate bool ClassExchangeRequestHandler<TReq, TResp>(IClassExchanger node, ExchangeResponse<ushort> request, TReq req, out TResp resp) where TReq : IExchangeRequest<TResp> where TResp : IExchangeable;