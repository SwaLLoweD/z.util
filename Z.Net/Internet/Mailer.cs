﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Z.Extensions;

namespace Z.Net;

/// <summary>Simple SMTP Client.</summary>
[Serializable]
public static class Mailer
{
    #region Fields / Properties
    /// <summary>Enable SSL for SMTP Server Communication</summary>
    public static bool? EnableSSL { get; set; }
    /// <summary>SMTP Server password.</summary>
    public static string? SmtpPassword { get; set; }
    /// <summary>SMTP Server Port</summary>
    public static int SmtpPort { get; set; }
    /// <summary>SMTP Server dns/ip.</summary>
    public static string? SmtpServer { get; set; }
    /// <summary>SMTP Server username.</summary>
    public static string? SmtpUser { get; set; }
    /// <summary>Check if the Server Certificate Valid or Not</summary>
    public static bool ValidateServerCertificate { get; set; }
    #endregion

    #region Constructors
    static Mailer() => ValidateServerCertificate = true;
    #endregion

    /// <summary>Send an e-mail. (SMTP Server values must be set)</summary>
    /// <param name="sender">Sender address</param>
    /// <param name="to">Recepient</param>
    /// <param name="subject">Subject</param>
    /// <param name="body">HTML or Text body</param>
    /// <param name="files">File paths to attach</param>
    public static async Task<bool> Send(string sender, string to, string subject, string body, IEnumerable<string>? files = null) {
        var t = new MailAddressCollection {
                new MailAddress(to)
            };
        return await Send(new MailAddress(sender), t, subject, body, null, null, null, files).ConfigureAwait(false);
    }
    /// <summary>Send an e-mail. (SMTP Server values must be set)</summary>
    /// <param name="sender">Sender address</param>
    /// <param name="to">Recepient</param>
    /// <param name="subject">Subject</param>
    /// <param name="body">HTML or Text body</param>
    /// <param name="cc">Carbon-copy recepients</param>
    /// <param name="bcc">Blind Carbon-copy recepients</param>
    /// <param name="replyTo">Reply-To addresses</param>
    /// <param name="files">File paths to attach</param>
    public static async Task<bool> Send(MailAddress sender, MailAddressCollection to, string subject, string body, MailAddressCollection? cc = null, MailAddressCollection? bcc = null,
        MailAddressCollection? replyTo = null, IEnumerable<string>? files = null) {
        var mail = new MailMessage {
            From = sender
        };
        mail.To.AddUnique(to);
        if (cc != null) mail.CC.AddUnique(cc);
        if (bcc != null) mail.Bcc.AddUnique(bcc);
        if (replyTo != null) mail.ReplyToList.AddUnique(replyTo);
        mail.Priority = MailPriority.Normal;
        if (files != null) {
            foreach (var f in files) {
                if (File.Exists(f))
                    mail.Attachments.Add(new Attachment(f));
            }
        }

        mail.Subject = subject;
        mail.Body = body;
        mail.IsBodyHtml = body.Contains("<body>") || body.Contains("</body>");
        mail.BodyEncoding = mail.HeadersEncoding = mail.SubjectEncoding = Encoding.UTF8;
        return await Send(mail).ConfigureAwait(false);
    }
    /// <summary>Send an e-mail.</summary>
    /// <param name="mail">Mail message</param>
    /// <param name="smtpServer">SMTP Server Address</param>
    /// <param name="port">SMTP Server port</param>
    public static async Task<bool> Send(MailMessage mail, string? smtpServer = null, int port = 0) {
        try {
            using var sm = new SmtpClient();
            if (!string.IsNullOrWhiteSpace(SmtpUser)) {
                var cred = new System.Net.NetworkCredential(SmtpUser, SmtpPassword);
                sm.Credentials = cred;
                sm.UseDefaultCredentials = false;
            }
            if (!string.IsNullOrWhiteSpace(smtpServer))
                sm.Host = smtpServer;
            else if (!string.IsNullOrWhiteSpace(SmtpServer)) sm.Host = SmtpServer;
            if (port > 0)
                sm.Port = (int)port;
            else if (SmtpPort > 0) sm.Port = (int)SmtpPort;
            if (EnableSSL.HasValue) sm.EnableSsl = EnableSSL.Value;
            if (!ValidateServerCertificate)
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (_, _, _, _) => true; //Skip certificate validation
            await Task.Run(() => sm.Send(mail)).ConfigureAwait(false);
            return true;
        } catch {
            return false;
        }
    }
    /// <summary>Send bulk e-mail.</summary>
    /// <param name="mails">Mail messages</param>
    /// <param name="smtpServer">SMTP Server Address</param>
    /// <param name="port">SMTP Server port</param>
    public static async Task<int> SendBulk(IEnumerable<MailMessage> mails, string? smtpServer = null, int port = 0) {
        var rval = 0;
        foreach (var mail in mails) rval += await Send(mail, smtpServer, port) ? 1 : 0;
        return rval;
    }
}