/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Collections.Specialized;

namespace Z.Banking
{
    ///<summary>VPos Pluggable Interface</summary>
    public interface IVPos
    {
        ///<summary>3D Authentication Request</summary>
        Task<VPos3DRequest> Auth3D(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, string okurl, string failurl, int installment = 0, string? userId = null);
        ///<summary>Validate 3DResponse</summary>
        Task<VPos3DResponse> Validate3DResponse(NameValueCollection form, string? userId = null);
        ///<summary>Perform Payment</summary>
        Task<VPosPayResponse> Pay(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, int installment = 0, string? userId = null);
        ///<summary>Perform Payment</summary>
        Task<VPosPayResponse> Pay(VPos3DResponse result, string? userId = null);
        ///<summary>Set Default VPos Configuration</summary>
        void SetDefaultConfig(VPosConfig config);
    }
}