/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

namespace Z.Banking;

///<summary>VPos Credentials and other config</summary>
public class VPosConfig
{
    #region Fields / Properties
    ///<summary>3DSecure Url</summary>
    public string? Auth3DUrl { get; set; }
    ///<summary>Store Id</summary>
    public string? ClientId { get; set; }
    ///<summary>Store Payment API mode (T=Test, P=Production)</summary>
    public string? Modeval { get; set; }
    ///<summary>Store Payment API username</summary>
    public string? Nameval { get; set; }
    ///<summary>Store Payment API password</summary>
    public string? Passwordval { get; set; }
    ///<summary>3DSecure Payment Url</summary>
    public string? Payment3DUrl { get; set; }
    ///<summary>Payment Url</summary>
    public string? PaymentUrl { get; set; }
    ///<summary>Store Key</summary>
    public string? Storekey { get; set; }
    ///<summary>Auth and payment Method (3d, 3d-pay etc.)</summary>
    public string? Storetype { get; set; }
    ///<summary>TestMode is on or not (May not do anything)</summary>
    public bool TestMode { get; set; }
    ///<summary>Store Payment Type (Auth PreAuth PostAuth Credit Void)</summary>
    public string? TypeVal { get; set; }
    #endregion

    #region Constructors
    ///<summary>Constructor</summary>
    public VPosConfig() => TestMode = false;
    #endregion
}