/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Z.Extensions;

namespace Z.Banking;

///<summary>VPos Manager</summary>
public static partial class VPosManager<T> where T : IVPos
{
    #region Constants / Static Fields
    private static readonly IDictionary<Type, VPosConfig> configStore = new Dictionary<Type, VPosConfig>();
    #endregion

    ///<summary>Request 3D Secure Authentication</summary>
    public static async Task<VPos3DRequest> Auth3D(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, string okurl, string failurl, int installment = 0, string? userId = null) {
        var pos = CreateInstance();
        return await ((T)pos).Auth3D(card, orderId, currency, totalPrice, okurl, failurl, installment, userId);
    }
    ///<summary>Validate 3D Secure Response</summary>
    public static async Task<VPos3DResponse> Validate3DResponse(NameValueCollection form, string? userId = null) {
        var pos = CreateInstance();
        return await ((T)pos).Validate3DResponse(form, userId);
    }
    ///<summary>Perform Payment Transaction</summary>
    public static async Task<VPosPayResponse> Pay(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, int installment = 0, string? userId = null) {
        var pos = CreateInstance();
        return await ((T)pos).Pay(card, orderId, currency, totalPrice, installment, userId);
    }
    ///<summary>Perform Payment Transaction</summary>
    public static async Task<VPosPayResponse> Pay(VPos3DResponse result, string? userId = null) {
        var pos = CreateInstance();
        return await ((T)pos).Pay(result, userId);
    }
    ///<summary>Set VPos Plugin Default Configuration</summary>
    public static void SetDefaultConfig(VPosConfig config) => configStore[typeof(T)] = config;
    //createInstance().SetDefaultConfig(config);
    private static T CreateInstance() {
        var pos = typeof(T).CreateInstance<T>()!;
        if (configStore.ContainsKey(typeof(T))) pos.SetDefaultConfig(configStore[typeof(T)]);
        return (T)pos;
    }
}