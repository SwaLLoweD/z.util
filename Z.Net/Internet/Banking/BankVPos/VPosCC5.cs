/* Copyright (c) <2008> <A. Zafer YURDACALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using Z.Extensions;

namespace Z.Banking;

///<summary>CC5 VPos Gateway base class</summary>
public class VPosCC5 : IVPos
{
    #region Fields / Properties
    ///<summary>VPos configuration</summary>
    protected virtual VPosConfig Cfg { get; set; } = new VPosConfig();
    #endregion

    #region IVPos Members
    /// <inheritdoc />
    public virtual void SetDefaultConfig(VPosConfig config) => Cfg = config;
    #endregion

    #region PreparePayRequest
    ///<summary>PreparePayRequest</summary>
    protected virtual string PreparePayRequest(CreditCard card, string? orderId, VPosCurrencies currency, decimal totalPrice = 0, int installment = 0, string? userId = null, string? xid = null,
        string? eci = null, string? cavv = null) {
        var doc = new XmlDocument();
        var dec = doc.CreateXmlDeclaration("1.0", "ISO-8859-9", "yes");
        doc.AppendChild(dec);

        if (Cfg == null) return doc.OuterXml;

        var expiry = card.ExpirationMonth.ToString("00") + "/" + card.ExpirationYear.ToString("00");
        if (card.ExpirationMonth <= 0 || card.ExpirationYear <= 0) expiry = null;

        var cc5Request = doc.CreateChildNode("CC5Request");
        cc5Request.CreateChildNode("Name").InnerText = Cfg.Nameval ?? "";
        cc5Request.CreateChildNode("Password").InnerText = Cfg.Passwordval ?? "";
        cc5Request.CreateChildNode("ClientId").InnerText = Cfg.ClientId ?? "";
        cc5Request.CreateChildNode("IPAddress");
        cc5Request.CreateChildNode("Email");
        cc5Request.CreateChildNode("Mode").InnerText = Cfg.Modeval ?? ""; //T=Test, P=Production
        cc5Request.CreateChildNode("OrderId").InnerText = orderId ?? "";
        cc5Request.CreateChildNode("GroupId");
        cc5Request.CreateChildNode("TransId");
        cc5Request.CreateChildNode("UserId").InnerText = userId ?? "";
        cc5Request.CreateChildNode("Type").InnerText = Cfg.TypeVal ?? ""; //Auth PreAuth PostAuth Credit Void olabilir.
        cc5Request.CreateChildNode("Number").InnerText = card.CardNumber ?? "";
        cc5Request.CreateChildNode("Expires").InnerText = expiry ?? "";
        cc5Request.CreateChildNode("Cvv2Val").InnerText = card.CV2 < 0 ? "" : card.CV2.ToString("000");
        cc5Request.CreateChildNode("Total").InnerText = totalPrice <= 0 ? "" : totalPrice.ToString();
        cc5Request.CreateChildNode("Currency").InnerText = currency.To().Type(0).ToString();
        cc5Request.CreateChildNode("Taksit").InnerText = installment < 2 ? "" : installment.ToString();
        cc5Request.CreateChildNode("PayerTxnId").InnerText = xid ?? "";
        cc5Request.CreateChildNode("PayerSecurityLevel").InnerText = eci ?? "";
        cc5Request.CreateChildNode("PayerAuthenticationCode").InnerText = cavv ?? "";
        cc5Request.CreateChildNode("CardholderPresentCode").InnerText = 13.ToString(); //?
        var billTo = cc5Request.CreateChildNode("BillTo");

        billTo.CreateChildNode("Name");
        billTo.CreateChildNode("Street1");
        billTo.CreateChildNode("Street2");
        billTo.CreateChildNode("Street3");
        billTo.CreateChildNode("City");
        billTo.CreateChildNode("StateProv");
        billTo.CreateChildNode("PostalCode");

        var shipTo = cc5Request.CreateChildNode("ShipTo");
        shipTo.CreateChildNode("Name");
        shipTo.CreateChildNode("Street1");
        shipTo.CreateChildNode("Street2");
        shipTo.CreateChildNode("Street3");
        shipTo.CreateChildNode("City");
        shipTo.CreateChildNode("StateProv");
        shipTo.CreateChildNode("PostalCode");

        cc5Request.CreateChildNode("Extra");

        return doc.OuterXml; //Olu�turulan xml string olarak al�n�yor.
    }
    #endregion

    #region 3DSecure (Non-existent)
    /// <inheritdoc />
    protected virtual string Prepare3DRequest(VPos3DRequest req) => throw new NotImplementedException();
    /// <inheritdoc />
    public virtual async Task<VPos3DRequest> Auth3D(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, string okurl, string failurl, int installment = 0, string? userId = null) {
        var rval = new VPos3DRequest {
            Card = card,
            OrderId = orderId,
            Currency = currency,
            Price = totalPrice,
            UrlOk = okurl,
            UrlFail = failurl,
            Installment = installment,
            UserId = userId
        };

        try {
            var post = Prepare3DRequest(rval);
            using var client = new HttpClient();
            client.DefaultRequestVersion = HttpVersion.Version10;

            var myRequest = new StringContent(post, Encoding.UTF8, "application/x-www-form-urlencoded");

            var response = await client.PostAsync(Cfg.Auth3DUrl, myRequest);
            rval.IsValid = response.IsSuccessStatusCode;
            rval.Output = await response.Content.ReadAsStringAsync(); //Bankadan donenleri sayfaya yazdiriyoruz/
        } catch (Exception ex) {
            rval.IsValid = false;
            rval.Exception = ex;
        }
        return rval;
    }
    /// <inheritdoc />
    public virtual Task<VPos3DResponse> Validate3DResponse(NameValueCollection form, string? userId = null) => throw new NotImplementedException();
    #endregion

    #region Pay
    /// <inheritdoc />
    public virtual Task<VPosPayResponse> Pay(VPos3DResponse result, string? userId = null) => throw new NotImplementedException();
    /// <inheritdoc />
    public virtual async Task<VPosPayResponse> Pay(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, int installment = 0, string? userId = null) =>
        await Pay(PreparePayRequest(card, orderId, currency, totalPrice, installment, userId), Cfg.PaymentUrl ?? "");
    ///<summary>Pay</summary>
    protected virtual async Task<VPosPayResponse> Pay(string requestData, string url) {
        var rval = new VPosPayResponse();
        try {
            var respXml = await PostPayXml(requestData, url, rval);
            if (respXml == null) return rval;
            var list = respXml.GetElementsByTagName("Response");
            var xmlResponse = list[0]?.GetInnerText();

            if ("Approved".Equals(xmlResponse)) {
                var list2 = respXml.GetElementsByTagName("TransId");
                rval.TransId = list2[0]?.GetInnerText();
                if (!string.IsNullOrWhiteSpace(rval.TransId)) {
                    rval.IsValid = true;
                    rval.Result = VPosResults.Ok;
                } else {
                    rval.IsValid = false;
                    rval.Result = VPosResults.BankSystemFailure;
                }
            } else {
                rval.IsValid = false;
                var list3 = respXml.GetElementsByTagName("ErrMsg");
                var errMsg = list3[0]?.InnerText;
                var errCode = string.Empty;
                var codeTags = respXml.GetElementsByTagName("ERRORCODE");
                if (codeTags.Count > 0) errCode = codeTags[0]?.InnerText;
                rval.Output = $"{errMsg} - {errCode}";
                rval.Result = VPosResults.Failure;
            }
        } catch (Exception ex) {
            rval.IsValid = false;
            rval.Result = VPosResults.SystemFailure;
            rval.Exception = ex;
        }
        return rval;
    }
    #endregion

    #region Utilities
    /// <inheritdoc />
    protected virtual string GetSHA1Hex(string SHA1Data) {
        using var sha = System.Security.Cryptography.SHA1.Create();
        var HashedPassword = SHA1Data;
        var hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
        var inputbytes = sha.ComputeHash(hashbytes);
        return GetHexaDecimal(inputbytes);
    }
    /// <inheritdoc />
    private static string GetHexaDecimal(byte[] bytes) {
        var s = new StringBuilder();
        var length = bytes.Length;
        for (var n = 0; n <= length - 1; n++) s.Append(string.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
        return s.ToString();
    }
    /// <inheritdoc />
    protected virtual async Task<string> GetSHA1Base64(string SHA1Data) {
        using var sha = System.Security.Cryptography.SHA1.Create();
        var hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(SHA1Data);
        return await Task.Run(() => {
            var inputbytes = sha.ComputeHash(hashbytes);
            var hash = Convert.ToBase64String(inputbytes);
            return hash;
        });
    }
    /// <inheritdoc />
    protected virtual async Task<XmlDocument?> PostPayXml(string requestData, string url, VPosPayResponse response) {
        HttpResponseMessage? resp = null;
        try {
            using var client = new HttpClient();
            var request = new StringContent("DATA=" + requestData, Encoding.GetEncoding("ISO-8859-9"), "application/x-www-form-urlencoded");
            resp = await client.PostAsync(url, request);
            var respXmlString = await resp.Content.ReadAsStringAsync(); //Gelen xml string olarak al�nd�.
            response.IsValid = resp.IsSuccessStatusCode;
            var respXml = new XmlDocument();
            respXml.LoadXml(respXmlString); //string xml d�kuman�na �evrildi.
            return respXml;
        } catch (Exception ex) {
            response.IsValid = false;
            response.Result = VPosResults.SystemFailure;
            response.Exception = ex;
        } finally {
            resp?.Dispose();
        }
        return null;
    }
    ///<summary>Get Test Card for NestPay Gateways</summary>
    public static CreditCard TestCreditCard(CreditCardTypes cardType = CreditCardTypes.MasterCard) {
        //3d Secure password is a;
        if (cardType == CreditCardTypes.MasterCard) {
            return new CreditCard {
                CardNumber = "5456165456165454",
                CardType = CreditCardTypes.MasterCard,
                CV2 = 0,
                ExpirationMonth = 12,
                ExpirationYear = 18,
                NameSurname = "Test User"
            };
        } else {
            return new CreditCard {
                CardNumber = "4022774022774026",
                CardType = CreditCardTypes.Visa,
                CV2 = 0,
                ExpirationMonth = 12,
                ExpirationYear = 18,
                NameSurname = "Test User"
            };
        }
    }
    #endregion

    #region Example XMLs
    //Payment Request
    //<?xml version=""1.0"" encoding=""ISO-8859-9""?>
    //<CC5Request>
    //    <Name>Name</Name>
    //    <Password>Password</Password>
    //    <ClientId>ClientId</ClientId>
    //    <Mode>P</Mode>
    //    <OrderId></OrderId>
    //    <Type>Auth</Type>
    //    <Number>Number</Number>
    //    <Expires>gExpire</Expires>
    //    <Cvv2Val>Cvv2Val</Cvv2Val>
    //    <Total>total</Total>
    //    <Taksit>taksit</Taksit>
    //    <Currency>949</Currency>
    //    <UserId></UserId>
    //    <email></email>

    //    <BillTo>
    //    <Name></Name>
    //        <Street1></Street1>
    //        <Street2></Street2>
    //        <Street3></Street3>
    //        <City></City>
    //        <StateProv></StateProv>
    //        <PostalCode></PostalCode>
    //        <Country></Country>
    //        <Company></Company>
    //        <TelVoice></TelVoice>
    //    </BillTo>
    //    <ShipTo>
    //        <Name></Name>
    //        <Street1></Street1>
    //        <Street2></Street2>
    //        <Street3></Street3>
    //        <City></City>
    //        <StateProv></StateProv> 
    //        <PostalCode></PostalCode>
    //        <Country></Country>
    //        <Company></Company>
    //        <TelVoice></TelVoice>
    //    </ShipTo>
    //</CC5Request>

    //Payment Response
    //<?xml version="1.0" encoding="ISO-8859-9"?> 
    //<CC5Response> 
    //  <OrderId>19127148</OrderId> 
    //  <GroupId></GroupId> 
    //  <Response>Error</Response> 
    //  <AuthCode></AuthCode> 
    //  <HostRefNum></HostRefNum> 
    //  <ProcReturnCode>99</ProcReturnCode> 
    //  <TransId></TransId> 
    //  <ErrMsg>Insufficient permissions to perform requested operation.</ErrMsg> 
    //  <Extra> 
    //    <HOSTMSG>Islem yetkisi yok, client id, kullanici adi veya sifre hatali girilmis olabilir.</HOSTMSG> 
    //    <NUMCODE>00009900641096</NUMCODE> 
    //  </Extra> 
    //</CC5Response>
    #endregion
}