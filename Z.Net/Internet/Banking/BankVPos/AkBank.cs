/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

namespace Z.Banking;

///<summary>Akbank VPos</summary>
///<remarks>https://testsanalpos.est.com.tr/akbank/report/user.login</remarks>
public class Akbank : VPosNestPay
{
    #region Fields / Properties
    ///<summary>TestConfig</summary>
    protected static VPosConfig TestConfig { get; set; } =
        //test info
        new() {
            ClientId = "100100000",
            Storekey = "123456",
            Storetype = "3d",
            Nameval = "AKTESTAPI",
            Passwordval = "AKBANK01",
            Modeval = "T",
            TypeVal = "Auth",
            Auth3DUrl = "https://testsanalpos.est.com.tr/servlet/est3Dgate",
            Payment3DUrl = "https://testsanalpos.est.com.tr/servlet/cc5ApiServer",
            PaymentUrl = "https://testsanalpos.est.com.tr/servlet/cc5ApiServer",
        };
    #endregion

    #region Constructors
    ///<summary>Constructor</summary>
    public Akbank() => base.Cfg = TestConfig;
    #endregion
}