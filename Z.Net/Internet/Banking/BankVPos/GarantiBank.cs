using System;
using System.Text;
using System.Web;
using System.Xml;
using Z.Extensions;
using Z.Util;

/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

namespace Z.Banking;

///<summary>GarantiBank VPos</summary>
///<remarks></remarks>
public class GarantiBank : VPosCC5
{
    #region Constants / Static Fields
    #endregion

    #region Fields / Properties
    ///<summary>Constructor</summary>
    protected static VPosConfig TestConfig { get; set; } =
        //test info
        new VPosConfig {
            ClientId = "600218", //TerminalMerchantID
                Storekey = "12345678",
            Storetype = "3D", //Secure3DSecurityLevel
                Nameval = "30690116", //TerminalID
                Passwordval = "123qweASD",
            Modeval = "TEST", //Mode
                TypeVal = "PROVAUT", //provUserID
                Auth3DUrl = "http://sanalposprovtest.garanti.com.tr/servlet/gt3dengine",
            Payment3DUrl = "http://sanalposprovtest.garanti.com.tr/VPServlet",
            PaymentUrl = "https://ccpos.garanti.com.tr/servlet/cc5ApiServer",
        };
    #endregion

    #region Constructors
    //public const string strTerminalProvUserID = "PROVAUT";
    //public const string strApiVersion = "v0.01";
    //public const string strMode = "TEST";
    //public const string strType = "sales";
    //public const string strTerminalMerchantID = "600218";
    //public const string strStoreKey = "12345678";
    //public const string strProvisionPassword = "123qweASD";
    //public const string strTerminalID = "30690116";

    //public const string postUrl = "http://sanalposprovtest.garanti.com.tr/servlet/gt3dengine";
    //public const string paymentUrl = "http://sanalposprovtest.garanti.com.tr/VPServlet";

    ///<summary>Constructor</summary>
    public GarantiBank() => base.Cfg = TestConfig;
    #endregion

    /// <inheritdoc />
    protected override string Prepare3DRequest(VPos3DRequest req) {
        var amount = req.Price.ToString("N2").Replace(",", "").Replace(".", ""); //İşlem Tutarı 1.00 TL için 100 gönderilmeli
        var installmentStr = req.Installment < 2 ? null : req.Installment.ToString();
        var SecurityData = GetSHA1Hex(Cfg.Passwordval + "0" + Cfg.Nameval).ToUpper();
        var hashStr = new StringBuilder();
        hashStr.AppendString(Cfg.Nameval, req.OrderId, amount, req.UrlOk, req.UrlFail, Cfg.TypeVal, installmentStr, Cfg.Storekey, SecurityData);
        var HashData = GetSHA1Hex(hashStr.ToString()).ToUpper();

        var post = new StringBuilder();

        if (req.Card != null) {
            //kredi kartı bilgileri
            post.AppendFormat("cardnumber={0}", req.Card.CardNumber);
            post.AppendFormat("&cardexpiredatemonth={0}", req.Card.ExpirationMonth);
            post.AppendFormat("&cardexpiredateyear={0}", req.Card.ExpirationYear);
            post.AppendFormat("&cardcvv2={0}", req.Card.CV2);
        }

        //pos bilgişleri
        post.AppendFormat("&mode={0}", Cfg.Modeval);
        post.AppendFormat("&secure3dsecuritylevel={0}", Cfg.Storetype);
        post.AppendFormat("&apiversion={0}", "v0.01");
        post.AppendFormat("&terminalprovuserid={0}", Cfg.TypeVal); //Iptal PROVRFN Satış PROVAUT
        post.AppendFormat("&terminaluserid={0}", HttpUtility.HtmlEncode(req.Card?.NameSurname));
        post.AppendFormat("&terminalmerchantid={0}", Cfg.ClientId);

        post.AppendFormat("&txntype={0}", Cfg.TypeVal == "PROVAUT" ? "sales" : "void"); //Iptal void Satış sales
        post.AppendFormat("&txnamount={0}", amount);
        post.AppendFormat("&txncurrencycode={0}", req.Currency.To().Type(0).ToString());
        post.AppendFormat("&txninstallmentcount={0}", installmentStr);
        post.AppendFormat("&orderid={0}", HttpUtility.HtmlEncode(req.OrderId));

        post.AppendFormat("&terminalid={0}", Cfg.Nameval);
        post.AppendFormat("&successurl={0}", HttpUtility.HtmlEncode(req.UrlOk));
        post.AppendFormat("&errorurl={0}", HttpUtility.HtmlEncode(req.UrlFail));
        post.AppendFormat("&customeremailaddress={0}", HttpUtility.HtmlEncode("eticaret@garanti.com.tr"));
        post.AppendFormat("&customeripaddress={0}", "1.1.111.111");
        post.AppendFormat("&secure3dhash={0}", HashData);

        //order bilgileri.
        //if (orderInfo.billingAddressId != "") post.AppendFormat("&BillingAddressId={0}", orderInfo.billingAddressId);
        //if (orderInfo.shippingAddressId != "") post.AppendFormat("&ShippingAddressId={0}", orderInfo.shippingAddressId);
        //if (orderInfo.billType != "") post.AppendFormat("&BillType={0}", orderInfo.billType);
        //if (orderInfo.bankRate != "") post.AppendFormat("&bankRate={0}", orderInfo.bankRate);
        //post.AppendFormat("&bankRateCost={0}", orderInfo.bankRateCost);
        //post.AppendFormat("&bankId={0}", orderInfo.bankId);

        return post.ToString();
    }

    /// <inheritdoc />
    public override Task<VPos3DResponse> Validate3DResponse(System.Collections.Specialized.NameValueCollection form, string? userId = null) {
        var rval = new VPos3DResponse();

        #region 3DS values
        var card = new CreditCard {
            CardNumber = form["cardnumber"],
            CV2 = form["cardcvv2"].Emptify().To().Type(0),
            ExpirationYear = form["cardexpiredateyear"].Emptify().To().Type<short>(),
            ExpirationMonth = form["cardexpiredatemonth"].Emptify().To().Type<short>(),
            CardType = form["cardType"].Emptify().To().Type<CreditCardTypes>(),
            NameSurname = HttpUtility.HtmlDecode(form["terminaluserid"])
        };
        var req = new VPos3DRequest {
            Card = card,
            Currency = form["txncurrencycode"].Emptify().To().Type<int>().To().Type<VPosCurrencies>(),
            Installment = form["txninstallmentcount"].Emptify().To().Type(0),
            IsValid = true,
            OrderId = HttpUtility.HtmlDecode(form["orderid"]),
            Price = form["txnamount"].Emptify().To().Type<decimal>(),
            UrlFail = HttpUtility.HtmlDecode(form["errorUrl"]),
            UrlOk = HttpUtility.HtmlDecode(form["successurl"]),
            UserId = HttpUtility.HtmlDecode(form["userid"]),
        };
        rval.Request = req;

        rval.MdStatus = form["mdstatus"].Emptify().To().Type(0);
        var mdErrorMsg = form["mderrormessage"];
        var strType = form["txntype"];
        var strAmount = form["txnamount"];
        var strInstallmentCount = form["txninstallmentcount"];
        //string strMode = form["mode"];
        //string strApiVersion = form["apiversion"];
        //string strTerminalProvUserID = form["terminalprovuserid"];
        //string strCurrencyCode = form["txncurrencycode"];
        //string strTerminalUserID = form["terminaluserid"];
        //string strCustomeripaddress = form["customeripaddress"];
        //string strcustomeremailaddress = form["customeremailaddress"];
        //string strTerminalMerchantID = form["terminalmerchantid"];
        var okurl = form["successurl"];
        var failurl = form["errorurl"];

        rval.Md = form["md"];
        rval.Oid = form["oid"];
        rval.Xid = form["xid"];
        rval.Eci = form["eci"];
        rval.Cavv = form["cavv"];
        #endregion

        try {
            //Provizyon için xml'in post edileceği adres
            var SecurityData = GetSHA1Hex(Cfg.Passwordval + "0" + Cfg.Nameval).ToUpper();
            var HashData = GetSHA1Hex(rval.Oid + Cfg.Nameval + strAmount + SecurityData).ToUpper();
            //Daha kısıtlı bilgileri HASH ediyoruz.

            //Hashdata kontrolü için bankadan dönen secure3dhash değeri alınıyor.
            var strHashData = form["secure3dhash"];
            var ValidateHashData = GetSHA1Hex(Cfg.Nameval + rval.Oid + strAmount + okurl + failurl + strType + strInstallmentCount + Cfg.Storekey + SecurityData).ToUpper();

            //İlk gönderilen ve bankadan dönen HASH değeri yeni üretilenle eşleşiyorsa;

            if (strHashData != ValidateHashData) {
                rval.IsValid = false;
                rval.Result = VPosResults.SecurityViolation;
                rval.ErrorMessage = "Sayısal imza geçersiz";
                return Task.FromResult(rval);
            }
            rval.ErrorMessage = mdErrorMsg;
        } catch (Exception ex) {
            rval.IsValid = false;
            rval.Result = VPosResults.SystemFailure;
            rval.Exception = ex;
        }

        var logMessage = new StringBuilder();
        foreach (string reqKey in form.Keys) logMessage.AppendString(reqKey, " = ", form[reqKey], SysInf.lf);
        rval.DebugText = logMessage.ToString();

        return Task.FromResult(rval);
    }

    #region PreparePayRequest
    ///<summary>Constructor</summary>
    protected virtual string PreparePayRequest(VPos3DResponse result, string? userId = null) {
        var doc = new XmlDocument();
        var dec = doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");
        doc.AppendChild(dec);

        if (result.Request == null) return doc.OuterXml;
        var amount = result.Request.Price.ToString("N2").Replace(",", "").Replace(".", ""); //İşlem Tutarı 1.00 TL için 100 gönderilmeli
        var installmentStr = result.Request.Installment < 2 ? null : result.Request.Installment.ToString();

        var SecurityData = GetSHA1Hex(Cfg.Passwordval + "0" + Cfg.Nameval).ToUpper();
        var hashStr = new StringBuilder();
        hashStr.AppendString(Cfg.Nameval, result.Request.OrderId, amount, result.Request.UrlOk, result.Request.UrlFail, Cfg.TypeVal, installmentStr, Cfg.Storekey, SecurityData);
        var HashData = GetSHA1Hex(hashStr.ToString()).ToUpper();

        var gvpsRequest = doc.CreateChildNode("GVPSRequest");
        gvpsRequest.CreateChildNode("Mode").InnerText = Cfg.Modeval ?? "";
        gvpsRequest.CreateChildNode("Version").InnerText = "v0.01";
        gvpsRequest.CreateChildNode("ChannelCode");

        var terminal = gvpsRequest.CreateChildNode("Terminal");
        terminal.CreateChildNode("ProvUserID").InnerText = Cfg.TypeVal ?? "";
        terminal.CreateChildNode("HashData").InnerText = HashData;
        terminal.CreateChildNode("UserID").InnerText = result.Request.Card?.NameSurname ?? "";
        terminal.CreateChildNode("ID").InnerText = Cfg.Nameval ?? "";
        terminal.CreateChildNode("MerchantID").InnerText = Cfg.ClientId ?? "";

        var customer = gvpsRequest.CreateChildNode("Customer");
        customer.CreateChildNode("IPAddress").InnerText = "1.1.111.111";
        customer.CreateChildNode("EmailAddress").InnerText = "eticaret@garanti.com.tr";

        var cardN = gvpsRequest.CreateChildNode("Card");
        cardN.CreateChildNode("Number");
        cardN.CreateChildNode("ExpireDate");
        cardN.CreateChildNode("CVV2");

        var order = gvpsRequest.CreateChildNode("Order");
        order.CreateChildNode("OrderID").InnerText = result.Request.OrderId ?? "";
        order.CreateChildNode("GroupID");
        var addressList = order.CreateChildNode("AddressList");
        var address = addressList.CreateChildNode("Address");
        address.CreateChildNode("Type").InnerText = "B";
        address.CreateChildNode("Name");
        address.CreateChildNode("LastName");
        address.CreateChildNode("Company");
        address.CreateChildNode("Text");
        address.CreateChildNode("District");
        address.CreateChildNode("City");
        address.CreateChildNode("PostalCode");
        address.CreateChildNode("Country");
        address.CreateChildNode("PhoneNumber");

        var transaction = gvpsRequest.CreateChildNode("Transaction");
        transaction.CreateChildNode("Type").InnerText = Cfg.TypeVal == "PROVAUT" ? "sales" : "void";
        transaction.CreateChildNode("InstallmentCnt").InnerText = installmentStr ?? "";
        transaction.CreateChildNode("Amount").InnerText = amount;
        transaction.CreateChildNode("CurrencyCode").InnerText = result.Request.Currency.To().Type(0).ToString();
        transaction.CreateChildNode("CardholderPresentCode").InnerText = "13"; //3DS için 13 
        transaction.CreateChildNode("MotoInd").InnerText = Cfg.TypeVal == "PROVAUT" ? "N" : "H"; //H iptal N satış

        var secure3d = transaction.CreateChildNode("Secure3D");
        secure3d.CreateChildNode("AuthenticationCode").InnerText = result.Cavv ?? "";
        secure3d.CreateChildNode("SecurityLevel").InnerText = Cfg.Storetype ?? "";
        secure3d.CreateChildNode("TxnID").InnerText = result.Xid ?? "";
        secure3d.CreateChildNode("Md").InnerText = result.Md ?? "";

        return doc.OuterXml; //Oluşturulan xml string olarak alınıyor.
    }
    #endregion

    #region Pay
    /// <inheritdoc />
    public new virtual async Task<VPosPayResponse> Pay(VPos3DResponse result, string? userId = null) {
        var request = PreparePayRequest(result, userId);
        return await Pay(request, Cfg.Payment3DUrl ?? "");
    }
    /// <inheritdoc />
    protected override async Task<VPosPayResponse> Pay(string requestData, string url) {
        var rval = new VPosPayResponse();
        try {
            var respXml = await PostPayXml(requestData, url, rval);
            if (respXml == null) return rval;
            //Console.WriteLine(responseFromServer);

            var list = respXml.GetElementsByTagName("ReasonCode");
            var xmlResponse = list[0]?.GetInnerText();
            //00 ReasonCode döndüğünde işlem başarılıdır. Müşteriye başarılı veya başarısız şeklinde göstermeniz tavsiye edilir. (Fraud riski)
            if (xmlResponse == "00") {
                var list2 = respXml.GetElementsByTagName("RetrefNum");
                rval.TransId = list2[0]?.GetInnerText();
                if (!string.IsNullOrWhiteSpace(rval.TransId)) {
                    rval.IsValid = true;
                    rval.Result = VPosResults.Ok;
                } else {
                    rval.IsValid = false;
                    rval.Result = VPosResults.BankSystemFailure;
                }
            } else {
                rval.IsValid = false;
                var list3 = respXml.GetElementsByTagName("ErrMsg");
                rval.Output = list3[0]?.GetInnerText();
                rval.Result = VPosResults.Failure;
            }
        } catch (Exception ex) {
            rval.IsValid = false;
            rval.Result = VPosResults.SystemFailure;
            rval.Exception = ex;
        }
        return rval;
    }
    #endregion

    #region Example Xml
    //Pay Response
    //<GVPSResponse>
    //    <Mode></Mode>
    //    <Order>
    //        <OrderID>Deneme</OrderID>
    //        <GroupID></GroupID>
    //    </Order>
    //    <Transaction>
    //        <Response>
    //            <Source>HOST</Source>
    //            <Code>12</Code>
    //            <ReasonCode>12</ReasonCode>
    //            <Message>Declined</Message>
    //            <ErrorMsg>İşleminizi gerçekleştiremiyoruz.Tekrar deneyiniz</ErrorMsg>
    //            <SysErrMsg>POSNO Tanimsiz</SysErrMsg>
    //        </Response>
    //        <RetrefNum>100411623653</RetrefNum>
    //        <AuthCode></AuthCode>
    //        <BatchNum>000037</BatchNum>
    //        <SequenceNum>000001</SequenceNum>
    //        <ProvDate>20110104 11:01:03</ProvDate>
    //        <CardNumberMasked></CardNumberMasked>
    //        <CardHolderName></CardHolderName>
    //        <HashData>F3755B935FE142F122B14EDD1 3292F79A559C951</HashData>
    //        <HostMsgList></HostMsgList>
    //        <RewardInqResult>
    //            <RewardList></RewardList>
    //            <ChequeList></ChequeList>
    //        </RewardInqResult>
    //    </Transaction>
    //</GVPSResponse>
    #endregion
}