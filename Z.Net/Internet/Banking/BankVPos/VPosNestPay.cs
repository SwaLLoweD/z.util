/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Z.Extensions;
using Z.Util;

namespace Z.Banking;

#pragma warning disable IDE0059
///<summary>EST NestPay VPos Gateway base class</summary>
public class VPosNestPay : VPosCC5
{
    #region PreparePayRequest
    ///<summary>PreparePayRequest</summary>
    protected virtual string PreparePayRequest(VPos3DResponse result, string? userId = null) {
        var cc = new CreditCard {
            CardNumber = result.Md,
            CV2 = -1
        };
        return PreparePayRequest(cc, result.Oid, VPosCurrencies.TL, 0, 0, userId, result.Xid, result.Eci, result.Cavv);
    }
    #endregion

    #region Pay
    /// <inheritdoc />
    public override async Task<VPosPayResponse> Pay(VPos3DResponse result, string? userId = null) {
        var request = PreparePayRequest(result, userId);
        return await Pay(request, Cfg.Payment3DUrl ?? "");
    }
    #endregion

    #region 3DSecure
    ///<summary>Constructor</summary>
    protected override string Prepare3DRequest(VPos3DRequest req) {
        var amount = req.Price.ToString();
        var rnd = DateTime.Now.ToString(); //Güvenlik ve kontrol amaçlı tarih yada sürekli değişen bir değer

        var hash = GetSHA1Base64(Cfg.ClientId + req.OrderId + amount + req.UrlOk + req.UrlFail + rnd + Cfg.Storekey).Result; //Güvenlik amaçlı oluşturulan hash

        /******************          GEREKLİ PARAMETRELER             *********************/

        /***************       ISTEĞE BAĞLI ALANLAR    *********************************/
        //string description = ""; //Açıklama
        //string xid = "";    //İşlem takip numarası 3D için XID i mağaza üretirse o kullanir, yoksa sistem üretiyor. (3D secure işlemleri için işlem takip numarası 20 bytelik bilgi 28 karaktere base64 olarak kodlanmalı, geçersiz yada boş ise sistem tarafından üretilir.)
        //string lang = "";     //gösterim dili boş ise Türkçe (tr), İngilizce için (en)
        //string email = "";    //email adresi
        //string userid = "";   //Kullanıcı takibi için id
        // islemtipi = Auth, PreAuth, PostAuth, Void, Credit
        /***************       ISTEĞE BAĞLI ALANLAR autocomplete="off"     ********************************/

        var post = new StringBuilder();

        //pos bilgişleri
        post.AppendFormat("clientid={0}", Cfg.ClientId);
        post.AppendFormat("&amount={0}", amount);
        post.AppendFormat("&oid={0}", HttpUtility.HtmlEncode(req.OrderId));
        post.AppendFormat("&okUrl={0}", HttpUtility.HtmlEncode(req.UrlOk));
        post.AppendFormat("&failUrl={0}", HttpUtility.HtmlEncode(req.UrlFail));
        post.AppendFormat("&rnd={0}", HttpUtility.HtmlEncode(rnd));
        post.AppendFormat("&hash={0}", HttpUtility.HtmlEncode(hash));
        post.AppendFormat("&storetype={0}", Cfg.Storetype);
        post.AppendFormat("&lang={0}", "tr");
        post.AppendFormat("&description={0}", HttpUtility.HtmlEncode("Gezbidi.com müzayede katılım bedeli"));
        if (!string.IsNullOrEmpty(req.UserId)) post.AppendFormat("&userid={0}", HttpUtility.HtmlEncode(req.UserId));

        if (req.Card != null) {
            //kredi kartı bilgileri
            post.AppendFormat("&pan={0}", HttpUtility.HtmlEncode(req.Card.CardNumber ?? ""));
            post.AppendFormat("&cv2={0}", HttpUtility.HtmlEncode(req.Card.CV2.ToString("000")));
            post.AppendFormat("&Ecom_Payment_Card_ExpDate_Year={0}", HttpUtility.HtmlEncode(req.Card.ExpirationYear.ToString("00")));
            post.AppendFormat("&Ecom_Payment_Card_ExpDate_Month={0}", HttpUtility.HtmlEncode(req.Card.ExpirationMonth.ToString("00")));
            post.AppendFormat("&cardType={0}", HttpUtility.HtmlEncode(req.Card.CardType.To().Type(0)));
            post.AppendFormat("&currency={0}", req.Currency.To().Type(0)); //949 TL
            post.AppendFormat("&description={0}", HttpUtility.HtmlEncode(req.Card.NameSurname));
        }

        //order bilgileri.
        //post.AppendFormat("&BillingAddressId={0}", orderInfo.billingAddressId);
        //post.AppendFormat("&ShippingAddressId={0}", orderInfo.shippingAddressId);
        //post.AppendFormat("&BillType={0}", orderInfo.billType);
        post.AppendFormat("&bankInstallment={0}", req.Installment < 2 ? null : req.Installment.ToString());
        //post.AppendFormat("&bankRateCost={0}", orderInfo.bankRateCost);
        //post.AppendFormat("&bankRate={0}", orderInfo.bankRate);
        //post.AppendFormat("&bankId={0}", orderInfo.bankId);

        return post.ToString();
    }
    /// <inheritdoc />
    public override async Task<VPos3DResponse> Validate3DResponse(NameValueCollection form, string? userId = null) {
        var rval = new VPos3DResponse();
        var clientid = form["clientid"];
        if (clientid == null) {
            rval.IsValid = false;
            rval.Result = VPosResults.SecurityViolation;
            rval.ErrorMessage = "Şirket bilgileri girilmemiş";
            return rval;
        }

        #region 3DSecure değişkenleri
        var card = new CreditCard {
            CardNumber = form["pan"],
            CV2 = form["cv2"].Emptify().To().Type(0),
            ExpirationYear = form["Ecom_Payment_Card_ExpDate_Year"].Emptify().To().Type<short>(),
            ExpirationMonth = form["Ecom_Payment_Card_ExpDate_Month"].Emptify().To().Type<short>(),
            CardType = form["cardType"].Emptify().To().Type<CreditCardTypes>(),
            NameSurname = HttpUtility.HtmlDecode(form["description"])
        };
        var req = new VPos3DRequest {
            Card = card,
            Currency = form["currency"].Emptify().To().Type<VPosCurrencies>(),
            Installment = form["bankInstallment"].Emptify().To().Type(0),
            IsValid = true,
            OrderId = HttpUtility.HtmlDecode(form["oid"]),
            Price = form["amount"].Emptify().To().Type<decimal>(),
            UrlFail = HttpUtility.HtmlDecode(form["failUrl"]),
            UrlOk = HttpUtility.HtmlDecode(form["okUrl"]),
            UserId = HttpUtility.HtmlDecode(form["userid"]),
        };
        rval.Request = req;
        rval.MdStatus = form["mdStatus"].Emptify().To().Type(9);
        var mdErrorMsg = form["mdErrorMsg"];
        var rnd = form["rnd"];
        var HASH = form["HASH"];
        var PAResSyntaxOK = form["PAResSyntaxOK"];
        var PAResVerified = form["PAResVerified"];
        var version = form["version"];
        var mercantID = form["mercantID"];
        var txstatus = form["txstatus"];
        var iReqCode = form["iReqCode"];
        var iReqDetail = form["iReqDetail"];
        var vendorCode = form["vendorCode"];
        var cavvAlgorithm = form["cavvAlgorithm"];
        var HASHPARAMS = form["HASHPARAMS"];
        var HASHPARAMSVAL = form["HASHPARAMSVAL"]; //?

        rval.Md = form["md"];
        rval.Oid = form["oid"];
        rval.Xid = form["xid"];
        rval.Eci = form["eci"];
        rval.Cavv = form["cavv"];
        #endregion

        try {
            if (HASHPARAMS != null && HASHPARAMSVAL != null) {
                var paramsval = new StringBuilder();
                int index1 = 0, index2 = 0;
                // hash hesaplamada kullanılacak değerler ayrıştırılıp değerleri birleştiriliyor.
                do {
                    index2 = HASHPARAMS.IndexOf(":", index1);
                    var formKey = HASHPARAMS[index1..index2];
                    var formValues = form.GetValues(formKey);
                    var val = string.Empty;
                    if (formValues?.Length > 0) val = formValues[0];
                    paramsval.Append(val);
                    index1 = index2 + 1;
                } while (index1 < HASHPARAMS.Length);

                var hash = await GetSHA1Base64(paramsval.ToString() + Cfg.Storekey); //Güvenlik ve kontrol amaçlı oluşturulan hash

                //oluşturulan hash ile gelen hash ve hash parametreleri değerleri ile ayrıştırılıp edilen edilen aynı olmalı.
                if (!paramsval.ToString().Equals(HASHPARAMSVAL) || !hash.Equals(HASH)) {
                    rval.IsValid = false;
                    rval.Result = VPosResults.SecurityViolation;
                    rval.ErrorMessage = "Sayısal imza geçersiz";
                    return rval;
                }
            }
            rval.ErrorMessage = mdErrorMsg;
        } catch (Exception ex) {
            rval.IsValid = false;
            rval.Result = VPosResults.SystemFailure;
            rval.Exception = ex;
        }

        var logMessage = new StringBuilder();
        foreach (string reqKey in form.Keys) logMessage.AppendString(reqKey, " = ", form[reqKey], SysInf.lf);
        rval.DebugText = logMessage.ToString();

        return rval;
    }
    #endregion
}