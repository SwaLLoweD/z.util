/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

namespace Z.Banking;

///<summary>FinansBank VPos</summary>
///<remarks>https://testsanalpos.est.com.tr/finans/report/user.login</remarks>
public class FinansBank : VPosNestPay
{
    #region Fields / Properties
    ///<summary>Constructor</summary>
    protected static VPosConfig TestConfig { get; set; } =
        //test info
        new() {
            ClientId = "600100000", //Banka tarafından mağazaya verilen iş yeri numarası
                Storekey = "123456", //işyeri anahtarı
                Storetype = "3d",
            Nameval = "FINANSAPI",
            Passwordval = "FINANS06",
            Modeval = "T",
            TypeVal = "Auth",
            Auth3DUrl = "https://entegrasyon.asseco-see.com.tr/fim/est3Dgate",
            Payment3DUrl = "https://entegrasyon.asseco-see.com.tr/fim/api",
            PaymentUrl = ": https://entegrasyon.asseco-see.com.tr/fim/api",
        };
    #endregion

    #region Constructors
    ///<summary>Constructor</summary>
    public FinansBank() => base.Cfg = TestConfig;
    #endregion
}