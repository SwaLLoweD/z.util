/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using Z.Extensions;

namespace Z.Banking;

//http://setmpos.ykb.com/PosnetF1/yardim_tr/programlama/xml/xml.htm

///<summary>Yapı Kredi VPos</summary>
public class YapiKredi : IVPos
{
    #region Constants / Static Fields
    #endregion

    #region Fields / Properties
    ///<summary>Config</summary>
    protected static VPosConfig Cfg { get; set; } =
        //test info
        new() {
            ClientId = "100100000",
            Storekey = "123456",
            Storetype = "3d",
            Nameval = "AKTESTAPI",
            Passwordval = "AKBANK01",
            Modeval = "T",
            TypeVal = "Auth",
            Auth3DUrl = "https://testsanalpos.est.com.tr/servlet/est3Dgate",
            Payment3DUrl = "https://testsanalpos.est.com.tr/servlet/cc5ApiServer",
            PaymentUrl = "https://testsanalpos.est.com.tr/servlet/cc5ApiServer",
        };
    #endregion

    #region IVPos Members
    /// <inheritdoc />
    public void SetDefaultConfig(VPosConfig config) => Cfg = config;
    #endregion

    // private string GenerateHash(string orderId, string amount, string okurl, string failurl, string rnd) {
    //     var hashstr = Cfg.ClientId + orderId + amount + okurl + failurl + rnd + Cfg.Storekey;
    //     using System.Security.Cryptography.SHA1 sha = System.Security.Cryptography.SHA1.Create();
    //     var hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(hashstr);
    //     var inputbytes = sha.ComputeHash(hashbytes);
    //     var hash = Convert.ToBase64String(inputbytes);

    //     return hash;
    // }
    ///<summary>Get Test Card for Posnet Gateways</summary>
    public static CreditCard TestCreditCard(CreditCardTypes cardType = CreditCardTypes.Visa) {
        //3d Secure password is a;
        if (cardType == CreditCardTypes.MasterCard) {
            return new CreditCard {
                CardNumber = "5456165456165454",
                CardType = CreditCardTypes.MasterCard,
                CV2 = 0,
                ExpirationMonth = 12,
                ExpirationYear = 15,
                NameSurname = "Test User"
            };
        } else {
            return new CreditCard {
                CardNumber = "4022774022774026",
                CardType = CreditCardTypes.Visa,
                CV2 = 0,
                ExpirationMonth = 12,
                ExpirationYear = 15,
                NameSurname = "Test User"
            };
        }
    }

    #region 3DSecure
    /// <inheritdoc />
    public Task<VPos3DRequest> Auth3D(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, string okurl, string failurl, int installment = 0, string? userId = null) {
        var rval = new VPos3DRequest {
            IsValid = false,
            Output = "3d secure ödeme biçimi desteklenmemektedir.",
            Card = card,
            OrderId = orderId,
            Currency = currency,
            Price = totalPrice,
            UrlOk = okurl,
            UrlFail = failurl,
            Installment = installment,
            UserId = userId
    };
        return Task.FromResult(rval);
    }
    /// <inheritdoc />
    public Task<VPos3DResponse> Validate3DResponse(NameValueCollection form, string? userId = null) {
        var rval = new VPos3DResponse {
            IsValid = false,
            Result = VPosResults.Secure3DNotSupported,
            ErrorMessage = "3d secure ödeme biçimi desteklenmemektedir.",
            ClientId = userId,
            DebugText = form.ToString()
    };
        return Task.FromResult(rval);
    }
    #endregion

    #region PreparePayRequest
    ///<summary>PreparePayRequest</summary>
    protected static string PreparePayRequest(VPos3DResponse result, string? userId = null) {
        var cc = new CreditCard {
            CardNumber = result.Md,
        };
        return PreparePayRequest(cc, result.Oid, VPosCurrencies.TL, 0, 0, userId, result.Xid, result.Eci, result.Cavv);
    }
    ///<summary>PreparePayRequest</summary>
    protected static string PreparePayRequest(CreditCard card, string? orderId, VPosCurrencies currency, decimal totalPrice = 0, int installment = 0, string? userId = null, string? xid = null, string? eci = null, string? cavv = null) {
        var doc = new System.Xml.XmlDocument();
        var dec = doc.CreateXmlDeclaration("1.0", "ISO-8859-9", "yes");
        doc.AppendChild(dec);

        var posnetRequest = doc.CreateChildNode("posnetRequest");
        posnetRequest.CreateChildNode("mid").InnerText = Cfg.Nameval ?? "";
        posnetRequest.CreateChildNode("tid").InnerText = Cfg.Passwordval ?? "";
        //posnetRequest.CreateChildNode("ClientId").InnerText = cfg.clientId;
        var sale = posnetRequest.CreateChildNode("sale");
        sale.CreateChildNode("amount").InnerText = totalPrice <= 0 ? "" : totalPrice.ToString();
        sale.CreateChildNode("ccno").InnerText = card.CardNumber ?? "";
        sale.CreateChildNode("currencyCode").InnerText = currency.ToString();
        sale.CreateChildNode("cvc").InnerText = card.CV2.ToString("000");
        sale.CreateChildNode("expDate").InnerText = card.ExpirationMonth.ToString("00") + card.ExpirationYear.ToString("00");
        sale.CreateChildNode("orderID").InnerText = orderId ?? "";
        sale.CreateChildNode("installment").InnerText = installment < 2 ? "" : installment.ToString();

        var extraUnused = userId + xid + eci + cavv;
        Console.WriteLine(extraUnused);
        //sale.CreateChildNode("dueDate").InnerText; //2006-01-31
        return doc.OuterXml; //Oluşturulan xml string olarak alınıyor.
    }
    #endregion

    #region Pay
    /// <inheritdoc />
    public async Task<VPosPayResponse> Pay(VPos3DResponse result, string? userId = null) {
        var request = PreparePayRequest(result, userId);
        return await Pay(request);
    }
    /// <inheritdoc />
    public async Task<VPosPayResponse> Pay(CreditCard card, string orderId, VPosCurrencies currency, decimal totalPrice, int installment = 0, string? userId = null) {
        var request = PreparePayRequest(card, orderId, currency, totalPrice, installment, userId);
        return await Pay(request);
    }
    ///<summary>Pay</summary>
    protected static async Task<VPosPayResponse> Pay(string requestData) {
        var rval = new VPosPayResponse();
        HttpResponseMessage? resp = null;
        try {
            using var client = new HttpClient();
            var request = new StringContent("DATA=" + requestData, Encoding.GetEncoding("ISO-8859-9"), "application/x-www-form-urlencoded");
            resp = await client.PostAsync(Cfg.PaymentUrl, request);
            var respXmlString = await resp.Content.ReadAsStringAsync(); //Gelen xml string olarak al�nd�.
            rval.IsValid = resp.IsSuccessStatusCode;

            var respXml = new System.Xml.XmlDocument();
            respXml.LoadXml(respXmlString); //string xml dökumanına çevrildi.

            var list = respXml.GetElementsByTagName("approved");
            if (list == null || list.Count <= 0 || list[0] == null) throw new Exception("Invalid response format");
            var xmlResponse = list[0]?.GetInnerText().Emptify().To().Type(0);

            // 0 is error
            if (xmlResponse != 0) {
                var list2 = respXml.GetElementsByTagName("authCode");
                if (list2 == null || list2.Count <= 0 || list2[0] == null) throw new Exception("Invalid response format");
                rval.TransId = list2[0]?.GetInnerText();
                if (!string.IsNullOrWhiteSpace(rval.TransId)) {
                    rval.IsValid = true;
                    rval.Result = VPosResults.Ok;
                } else {
                    rval.IsValid = false;
                    rval.Result = VPosResults.BankSystemFailure;
                }
                //lblOrderCode.Text = OrderCode;
            } else {
                rval.IsValid = false;
                var list3 = respXml.GetElementsByTagName("respText");
                if (list3 == null || list3.Count <= 0 || list3[0] == null) throw new Exception("Invalid response format");
                rval.Output = list3[0]?.GetInnerText();
                rval.Result = VPosResults.Failure;
            }
        } catch (Exception ex) {
            rval.IsValid = false;
            rval.Result = VPosResults.SystemFailure;
            rval.Exception = ex;
        } finally {
            resp?.Dispose();
        }
        return rval;
    }
    #endregion

    //Pay Request
    //<posnetRequest> 
    //    <mid>Name</mid> 
    //    <tid>Password</tid> 
    //    <sale> 
    //        <amount>total</amount> 
    //        <ccno>Number</ccno> 
    //        <currencyCode>YT</currencyCode> 
    //        <cvc>Cvv2Val</cvc> 
    //        <expDate>gExpire</expDate> 
    //        <orderID>OrderId</orderID> 
    //        <installment>taksit</installment> 
    //    </sale> 
    //</posnetRequest> 

    //Pay Response(Success)
    //<posnetResponse>
    //    <approved>1</approved>
    //    <hostlogkey>0001000004P0503281</hostlogkey>
    //    <authCode>007912</authCode>
    //    <tranDate>050531135646</tranDate>
    //    <instInfo>
    //        <!--islem kac takside bolundu:(00: islemin pesin oldugunu gosterir)-->
    //        <inst1>03</inst1>
    //        <!--her taksidin tutari:(son iki hane mutlaka kurus)(islem pesinse 0 donulur)-->
    //        <amnt1>000000005000</amnt1>
    //    </instInfo>
    //    <pointInfo>
    //        <!-- islemden kazanilan puan miktari: -->
    //        <point>00000010</point>
    //        <!--islemden kazanilan puanin YTL karsiligi:(son iki hane mutlaka kurus)-->
    //        <pointAmount>000000000005</pointAmount>
    //        <!--karttaki toplam puan.Puan verilmeyen islemler icin 0 olabilir:-->
    //        <totalPoint>00011742</totalPoint>
    //        <!-- karttaki toplam puanin YTL karsiligi. Puan verilmeyen islemler icin 0 olabilir: (son iki hane mutlaka kurus)-->
    //        <totalPointAmount>000000005871</totalPointAmount>
    //    </pointInfo>
    //</posnetResponse>

    //Pay Response (Fail)
    //<?xml version='1.0'?> 
    //<posnetResponse> 
    //    <approved>0</approved> 
    //    <respCode>0003</respCode> 
    //    <respText>java.lang.IllegalArgumentException: INVALID REQUEST</respText> 
    //    <yourIP>127.0.0.1</yourIP> 
    //</posnetResponse>
}