/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;

namespace Z.Banking;

///<summary>3D Secure Response</summary>
public class VPos3DResponse
{
    #region Fields / Properties
    private int _mdStatus = 0;
    ///<summary>cavv</summary>
    public string? Cavv { get; set; }
    ///<summary>Store Id</summary>
    public string? ClientId { get; set; }
    ///<summary>Debugging Information</summary>
    public string? DebugText { get; set; }
    ///<summary>eci</summary>
    public string? Eci { get; set; }
    ///<summary>Error Message</summary>
    public string? ErrorMessage { get; set; }
    ///<summary>Exception if any is recieved</summary>
    public Exception? Exception { get; set; }
    ///<summary>Authentcation succesful or not</summary>
    public bool IsValid { get; set; }
    ///<summary>Authentication Result Key</summary>
    public string? Md { get; set; }
    ///<summary>Authentication Status</summary>
    public int MdStatus { get => _mdStatus; set => SetmdStatus(value); }
    ///<summary>Order Id</summary>
    public string? Oid { get; set; }
    ///<summary>Request Values</summary>
    public VPos3DRequest? Request { get; set; }

    ///<summary>VPos Result</summary>
    public VPosResults Result { get; set; }
    ///<summary>xid</summary>
    public string? Xid { get; set; }
    #endregion

    #region Constructors
    ///<summary>Constructor</summary>
    public VPos3DResponse() => IsValid = false;
    #endregion

    private void SetmdStatus(int value) {
        _mdStatus = value;
        switch (_mdStatus) {
            case 1:
                IsValid = true;
                Result = VPosResults.Ok;
                break;
            case 2:
                IsValid = true;
                Result = VPosResults.PayerOrBankNotRegistered;
                break;
            case 3:
                IsValid = true;
                Result = VPosResults.CardBankNotRegistered;
                break;
            case 4:
                IsValid = true;
                Result = VPosResults.PayerWillRegisterLater;
                break;
            case 5:
                IsValid = false;
                Result = VPosResults.ValidationFailure;
                break;
            case 6:
                IsValid = false;
                Result = VPosResults.Bank3DSError;
                break;
            case 7:
                IsValid = false;
                Result = VPosResults.BankSystemFailure;
                break;
            case 8:
                IsValid = false;
                Result = VPosResults.CardNumberError;
                break;
            case 0:
                IsValid = false;
                Result = VPosResults.Failure;
                break;
        }
    }
}

///<summary>3D Secure Authentication Request</summary>
public class VPos3DRequest
{
    #region Fields / Properties
    ///<summary>Credit Card</summary>
    public CreditCard? Card { get; set; }
    ///<summary>Currency</summary>
    public VPosCurrencies Currency { get; set; }
    ///<summary>Exception if any is recieved</summary>
    public Exception? Exception { get; set; }
    ///<summary>InstallmentCount</summary>
    public int Installment { get; set; }
    ///<summary>Request has been succesfully sent or not</summary>
    public bool IsValid { get; set; }
    ///<summary>OrderId</summary>
    public string? OrderId { get; set; }
    ///<summary>3D Secure Page output if request was successful</summary>
    public string? Output { get; set; }
    ///<summary>Price</summary>
    public decimal Price { get; set; }
    ///<summary>Fail url</summary>
    public string? UrlFail { get; set; }
    ///<summary>Success Url</summary>
    public string? UrlOk { get; set; }
    ///<summary>User Id</summary>
    public string? UserId { get; set; }
    #endregion

    #region Constructors
    ///<summary>Constructor</summary>
    public VPos3DRequest() => IsValid = false;
    #endregion
}

///<summary>Payment Response</summary>
public class VPosPayResponse
{
    #region Fields / Properties
    ///<summary>Exception if any is recieved</summary>
    public Exception? Exception { get; set; }
    ///<summary>Payment was successful or not</summary>
    public bool IsValid { get; set; }
    ///<summary>Error Message</summary>
    public string? Output { get; set; }
    ///<summary>VPos Result</summary>
    public VPosResults Result { get; set; }
    ///<summary>Transaction Id</summary>
    public string? TransId { get; set; }
    #endregion

    #region Constructors
    ///<summary>Constructor</summary>
    public VPosPayResponse() => IsValid = false;
    #endregion
}

///<summary>Possible VPos Result Codes</summary>
public enum VPosResults
{
    ///<summary>Failure</summary>
    Failure = 0,
    ///<summary>Auth Succeded</summary>
    Ok = 1,
    ///<summary>Auth Succeded</summary>
    PayerOrBankNotRegistered = 2,
    ///<summary>Auth Succeded</summary>
    CardBankNotRegistered = 3,
    ///<summary>Auth Succeded</summary>
    PayerWillRegisterLater = 4,
    ///<summary>5</summary>
    ValidationFailure = 5,
    ///<summary>6</summary>
    Bank3DSError = 6,
    ///<summary>7</summary>
    BankSystemFailure = 7,
    ///<summary>8</summary>
    CardNumberError = 8,

    ///<summary>18</summary>
    RequestError = 18,
    ///<summary>19</summary>
    SecurityViolation = 19,
    ///<summary>20</summary>
    SystemFailure = 20,
    ///<summary>21</summary>
    Secure3DNotSupported = 21,
    ///<summary>22</summary>
    InstallmentNotValid = 22,
}

///<summary>Currency Types</summary>
public enum VPosCurrencies
{
    ///<summary>Japanese Yen</summary>
    JPY = 392,
    ///<summary>British Pound</summary>
    GBP = 826,
    ///<summary>US Dollar</summary>
    US = 840,
    ///<summary>Turkish Lira</summary>
    TL = 949,
    ///<summary>Euro</summary>
    EUR = 978,
    ///<summary>Russian Ruble</summary>
    RUB = 643,
}