/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using Z.Extensions;

namespace Z.Banking
{
    ///<summary>Credit Card Structure</summary>
    [Serializable]
    public class CreditCard
    {
        #region Fields / Properties
        ///<summary>Card Information is Valid or Not</summary>
        public bool IsValid {
            get {
                if (string.IsNullOrEmpty(CardNumber)) return false;
                if (CV2.ToString().Length > 3) return false;
                if (ExpirationMonth < 1 || ExpirationMonth > 12) return false;
                if (ExpirationYear < DateTime.UtcNow.Year.ToString().Right(2).To().Type(0) || ExpirationYear > 99) return false;
                if (string.IsNullOrEmpty(NameSurname) || NameSurname?.Length < 3) return false;
                var DELTAS = new int[] {0, 1, 2, 3, 4, -4, -3, -2, -1, 0};
                var checksum = 0;
                var chars = CardNumber?.ToCharArray();
                if (chars == null) return false;
                for (var i = chars.Length - 1; i > -1; i--) {
                    var j = (int) chars[i] - 48;
                    checksum += j;
                    if ((i - chars.Length) % 2 == 0) checksum += DELTAS[j];
                }

                return checksum % 10 == 0;
            }
        }
        ///<summary>Card No</summary>
        public string? CardNumber { get; set; }
        ///<summary>Card Type (Visa/MasterCard)</summary>
        public CreditCardTypes CardType { get; set; }
        ///<summary>CVC2</summary>
        public int CV2 { get; set; }
        ///<summary>Expiration Month</summary>
        public short ExpirationMonth { get; set; }
        ///<summary>Expiration Year</summary>
        public short ExpirationYear { get; set; }
        ///<summary>Card Owner Name and Surname</summary>
        public string? NameSurname { get; set; }
        #endregion

        #region Constructors
        ///<summary>Constructor</summary>
        public CreditCard() => CV2 = -1;
        #endregion
    }

    ///<summary>Credit Card Types</summary>
    public enum CreditCardTypes : short
    {
        ///<summary>0</summary>
        None = 0,
        ///<summary>1</summary>
        Visa = 1,
        ///<summary>2</summary>
        MasterCard = 2,
    }
}