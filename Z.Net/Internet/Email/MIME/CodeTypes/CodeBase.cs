﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Text;
#endregion Usings

namespace Z.Net.Email.MIME.CodeTypes;

/// <summary>Default base coder</summary>
public class CodeBase : Code
{
    #region Constructors
    #region Constructor
    /// <summary>Constructor</summary>
    public CodeBase() { }
    #endregion Constructor
    #endregion

    #region Public Overridden Functions
    /// <summary>Decodes a string</summary>
    /// <param name="Input">Input string</param>
    /// <param name="Output">Output string</param>
    public override void Decode(string Input, out string Output) {
        Output = "";
        var Index = 0;
        while (Index < Input.Length) {
            var CurrentIndex = Input.IndexOf("=?", Index);
            if (CurrentIndex != -1) {
                Output += Input[Index..CurrentIndex];
                var CurrentIndex2 = Input.IndexOf("?=", CurrentIndex + 2);
                if (CurrentIndex2 != -1) {
                    CurrentIndex += 2;
                    var CurrentIndex3 = Input.IndexOf('?', CurrentIndex);
                    if (CurrentIndex3 != -1 && Input[CurrentIndex3 + 2] == '?') {
                        CharacterSet = Input[CurrentIndex..CurrentIndex3];
                        var DECString = Input.Substring(CurrentIndex3 + 3, CurrentIndex2 - CurrentIndex3 - 3);
                        if (Input[CurrentIndex3 + 1] == 'Q') {
                            var TempCode = CodeManager.Instance["quoted-printable"];
                            TempCode.CharacterSet = CharacterSet;
                            TempCode.Decode(DECString, out string? TempString);
                            Output += TempString;
                        } else if (Input[CurrentIndex3 + 1] == 'B') {
                            var TempCode = CodeManager.Instance["base64"];
                            TempCode.CharacterSet = CharacterSet;
                            TempCode.Decode(DECString, out string? TempString);
                            Output += TempString;
                        } else {
                            Output += DECString;
                        }
                    } else {
                        Output += Input[CurrentIndex3..CurrentIndex2];
                    }
                    Index = CurrentIndex2 + 2;
                } else {
                    Output += Input[CurrentIndex..];
                    break;
                }
            } else {
                Output += Input[Index..];
                break;
            }
        }
    }

    /// <summary>Encodes a string</summary>
    /// <param name="Input">Input string</param>
    /// <returns>encoded string</returns>
    public override string Encode(string Input) {
        var Builder = new StringBuilder();
        if (DelimeterNeeded)
            Builder.Append(EncodeDelimeter(Input));
        else
            Builder.Append(EncodeNoDelimeter(Input));

        if (IsAutoFold) {
            var FoldCharacters = this.FoldCharacters;
            if (FoldCharacters != null) {
                foreach (var FoldCharacter in FoldCharacters) {
                    var NewFoldString = FoldCharacter + "\r\n\t";
                    Builder.Replace(FoldCharacter, NewFoldString);
                }
            }
        }
        return Builder.ToString();
    }
    #endregion Public Overridden Functions

    #region Protected Properties
    /// <summary>Fold characters</summary>
    protected virtual string[]? FoldCharacters => null;

    /// <summary>Is folding used</summary>
    protected virtual bool IsAutoFold => false;

    /// <summary>Are delimeter's needed</summary>
    protected virtual bool DelimeterNeeded => false;

    /// <summary>Delimeter characters</summary>
    protected virtual char[]? DelimeterCharacters => null;
    #endregion Protected Properties

    #region Protected Functions
    /// <summary>Encodes a string based on delimeters specified</summary>
    /// <param name="Input">Input string</param>
    /// <returns>A string encoded based off of delimeters</returns>
    protected string EncodeDelimeter(string Input) {
        var Builder = new StringBuilder();
        var Filter = DelimeterCharacters;
        var InputArray = Input.Split(Filter);
        var Index = 0;
        foreach (var TempString in InputArray) {
            if (TempString != null) {
                Index += TempString.Length;
                if (string.IsNullOrEmpty(CharacterSet)) CharacterSet = Encoding.Default.BodyName;
                var EncodingUsing = SelectEncoding(Input).ToLower();
                if (EncodingUsing.Equals("non", StringComparison.InvariantCultureIgnoreCase)) {
                    Builder.Append(TempString);
                } else if (EncodingUsing.Equals("base64", StringComparison.InvariantCultureIgnoreCase)
                           || EncodingUsing.Equals("quoted-printable", StringComparison.InvariantCultureIgnoreCase)) {
                    var TempCode = CodeManager.Instance[EncodingUsing];
                    TempCode.CharacterSet = CharacterSet;
                    Builder.AppendFormat("=?{0}?Q?{1}?=", CharacterSet, TempCode.Encode(TempString));
                }
                if (Index < Input.Length) Builder.Append(Input, Index, 1);
                ++Index;
            }
        }

        return Builder.ToString();
    }

    /// <summary>Encodes a string without the use of delimeters</summary>
    /// <param name="Input">Input string</param>
    /// <returns>An encoded string</returns>
    protected string EncodeNoDelimeter(string Input) {
        var Builder = new StringBuilder();
        if (string.IsNullOrEmpty(CharacterSet)) CharacterSet = Encoding.Default.BodyName;

        var EncodingUsing = SelectEncoding(Input).ToLower();
        if (EncodingUsing.Equals("non", StringComparison.InvariantCultureIgnoreCase)) {
            Builder.Append(Input);
        } else if (EncodingUsing.Equals("base64", StringComparison.InvariantCultureIgnoreCase)
                   || EncodingUsing.Equals("quoted-printable", StringComparison.InvariantCultureIgnoreCase)) {
            var TempCode = CodeManager.Instance[EncodingUsing];
            TempCode.CharacterSet = CharacterSet;
            Builder.AppendFormat("=?{0}?Q?{1}?=", CharacterSet, TempCode.Encode(Input));
        }
        return Builder.ToString();
    }

    /// <summary>Selects an encoding type</summary>
    /// <param name="Input">Input string</param>
    /// <returns>A string containing the encoding type that should be used</returns>
    protected static string SelectEncoding(string Input) {
        var NumberOfNonASCII = 0;
        for (var x = 0; x < Input.Length; ++x) {
            if (IsNonASCIICharacter(Input[x]))
                ++NumberOfNonASCII;
        }

        if (NumberOfNonASCII == 0) return "non";
        var QuotableSize = Input.Length + (NumberOfNonASCII * 2);
        var Base64Size = (Input.Length + 2) / 3 * 4;
        return QuotableSize <= Base64Size || NumberOfNonASCII * 5 <= Input.Length ? "quoted-printable" : "base64";
    }

    /// <summary>Determines if this is a non ASCII character (greater than 255)</summary>
    /// <param name="Input"></param>
    /// <returns>True if it is, false otherwise</returns>
    protected static bool IsNonASCIICharacter(char Input) => (int)Input > 255;
    #endregion Protected Functions
}