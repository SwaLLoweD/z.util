﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
#endregion Usings

namespace Z.Net.Email.MIME
{
    /// <summary>Class containing constant used by the MIME parser</summary>
    public static class Constants
    {
        #region Fields / Properties
        /// <summary>Boundary</summary>
        public static string Boundary => "boundary";
        /// <summary>Charset</summary>
        public static string Charset => "charset";
        /// <summary>Content Description</summary>
        public static string ContentDescription => "Content-Description";
        /// <summary>Content disposition</summary>
        public static string ContentDisposition => "Content-Disposition";
        /// <summary>Content ID</summary>
        public static string ContentID => "Content-ID";
        /// <summary>Content type</summary>
        public static string ContentType => "Content-Type";
        /// <summary>Encoding 7bit</summary>
        public static string Encoding7Bit => "7bit";
        /// <summary>Encoding 8bit</summary>
        public static string Encoding8Bit => "8bit";
        /// <summary>Encoding base64</summary>
        public static string EncodingBase64 => "base64";
        /// <summary>Encoding binary</summary>
        public static string EncodingBinary => "binary";
        /// <summary>Encoding QP</summary>
        public static string EncodingQP => "quoted-printable";
        /// <summary>Filename</summary>
        public static string Filename => "filename";
        /// <summary>From</summary>
        public static string From => "From";
        /// <summary>Media application</summary>
        public static string MediaApplication => "application";
        /// <summary>Media audio</summary>
        public static string MediaAudio => "audio";
        /// <summary>Media image</summary>
        public static string MediaImage => "image";
        /// <summary>Media message</summary>
        public static string MediaMessage => "message";
        /// <summary>Media multi part</summary>
        public static string MediaMultiPart => "multipart";
        /// <summary>Media text</summary>
        public static string MediaText => "text";
        /// <summary>Media video</summary>
        public static string MediaVideo => "vedio";
        /// <summary>MIME version</summary>
        public static string MimeVersion => "MIME-Version";
        /// <summary>Name</summary>
        public static string Name => "name";
        /// <summary>Subject</summary>
        public static string Subject => "Subject";
        /// <summary>To</summary>
        public static string To => "To";
        /// <summary>Transfer encoding</summary>
        public static string TransferEncoding => "Content-Transfer-Encoding";
        #endregion
    }
}