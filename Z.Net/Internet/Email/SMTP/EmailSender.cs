﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using Z.Extensions;

//using Utilities.DataTypes.ExtensionMethods;
#endregion Usings

namespace Z.Net.Email.SMTP;

/// <summary>Utility for sending an email</summary>
public class EmailSender : Message, IDisposable
{
    #region Constructors
    /// <summary>Default Constructor</summary>
    public EmailSender() {
        Attachments = new List<Attachment>();
        EmbeddedResources = new List<LinkedResource>();
        Priority = MailPriority.Normal;
    }
    #endregion

    #region Public Functions
    /// <summary>Sends an email</summary>
    /// <param name="MessageBody">The body of the message</param>
    public virtual void SendMail(string MessageBody = "") {
        if (MessageBody.Is().NotEmpty) Body = MessageBody;
        using var message = new MailMessage();
        char[] Splitter = { ',', ';' };
        var AddressCollection = To?.Split(Splitter);
        if (AddressCollection != null) {
            for (var x = 0; x < AddressCollection.Length; ++x) {
                if (!string.IsNullOrEmpty(AddressCollection[x].Trim()))
                    message.To.Add(AddressCollection[x]);
            }
        }

        if (!string.IsNullOrEmpty(CC)) {
            AddressCollection = CC.Split(Splitter);
            for (var x = 0; x < AddressCollection.Length; ++x) {
                if (!string.IsNullOrEmpty(AddressCollection[x].Trim()))
                    message.CC.Add(AddressCollection[x]);
            }
        }
        if (!string.IsNullOrEmpty(Bcc)) {
            AddressCollection = Bcc.Split(Splitter);
            for (var x = 0; x < AddressCollection.Length; ++x) {
                if (!string.IsNullOrEmpty(AddressCollection[x].Trim()))
                    message.Bcc.Add(AddressCollection[x]);
            }
        }
        message.Subject = Subject;
        message.From = new MailAddress(From ?? "");
        using var BodyView = AlternateView.CreateAlternateViewFromString(Body ?? "", null, MediaTypeNames.Text.Html);
        foreach (var Resource in EmbeddedResources) BodyView.LinkedResources.Add(Resource);
        message.AlternateViews.Add(BodyView);
        //message.Body = Body;
        message.Priority = Priority;
        message.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
        message.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
        message.IsBodyHtml = true;
        foreach (var TempAttachment in Attachments) {
            message.Attachments.Add(TempAttachment);
        }

        using var smtp = new SmtpClient(Server, Port);
        if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password)) smtp.Credentials = new System.Net.NetworkCredential(UserName, Password);
        smtp.EnableSsl = UseSSL;
        smtp.Send(message);
    }

    /// <summary>Sends a piece of mail asynchronous</summary>
    /// <param name="MessageBody">The body of the message</param>
    public virtual async Task SendMailAsync(string MessageBody = "") {
        if (MessageBody.Is().NotEmpty) Body = MessageBody;
        await Task.Run(() => SendMail());
    }

    /// <summary>Disposes of the objects (attachments, embedded resources, etc) associated with the email</summary>
    public void Dispose() {
        Attachments.ForEach(x => x.Dispose());
        EmbeddedResources.ForEach(x => x.Dispose());
        GC.SuppressFinalize(this);
    }
    #endregion Public Functions

    #region Properties
    /// <summary>Any attachments that are included with this message.</summary>
    public List<Attachment> Attachments { get; set; }

    /// <summary>Any attachment (usually images) that need to be embedded in the message</summary>
    public List<LinkedResource> EmbeddedResources { get; set; }

    /// <summary>The priority of this message</summary>
    public MailPriority Priority { get; set; }

    /// <summary>Server Location</summary>
    public string? Server { get; set; }

    /// <summary>User Name for the server</summary>
    public string? UserName { get; set; }

    /// <summary>Password for the server</summary>
    public string? Password { get; set; }

    /// <summary>Port to send the information on</summary>
    public int Port { get; set; }

    /// <summary>Decides whether we are using STARTTLS (SSL) or not</summary>
    public bool UseSSL { get; set; }

    /// <summary>Carbon copy send (seperate email addresses with a comma)</summary>
    public string? CC { get; set; }

    /// <summary>Blind carbon copy send (seperate email addresses with a comma)</summary>
    public string? Bcc { get; set; }
    #endregion Properties
}