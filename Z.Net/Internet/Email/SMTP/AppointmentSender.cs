﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System.Net.Mail;
using Z.Util.FileFormats;
#endregion Usings

namespace Z.Net.Email.SMTP;

/// <summary>Sends appointments through SMTP</summary>
public class AppointmentSender : EmailSender
{
    #region Fields / Properties
    #region Properties
    /// <summary>Contains all of the appointment info</summary>
    public VCalendar AppointmentInfo { get; set; }
    #endregion Properties
    #endregion

    #region Constructors
    #region Constructor
    /// <summary>Constructor</summary>
    public AppointmentSender()
        : base() =>
        AppointmentInfo = new VCalendar();
    #endregion Constructor
    #endregion

    #region Public Functions
    /// <summary>Sends an email</summary>
    /// <param name="MessageBody">The body of the message</param>
    public override void SendMail(string MessageBody = "") {
        using var Mail = new MailMessage();
        using var TextView = AlternateView.CreateAlternateViewFromString(AppointmentInfo.ToString(), new System.Net.Mime.ContentType("text/plain"));
        using var HTMLView = AlternateView.CreateAlternateViewFromString(AppointmentInfo.GetHCalendar(), new System.Net.Mime.ContentType("text/html"));
        var CalendarType = new System.Net.Mime.ContentType("text/calendar");
        CalendarType.Parameters.Add("method", AppointmentInfo.Cancel ? "CANCEL" : "REQUEST");
        CalendarType.Parameters.Add("name", "meeting.ics");
        using var CalendarView = AlternateView.CreateAlternateViewFromString(AppointmentInfo.GetICalendar(), CalendarType);
        CalendarView.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
        Mail.AlternateViews.Add(TextView);
        Mail.AlternateViews.Add(HTMLView);
        Mail.AlternateViews.Add(CalendarView);
        char[] Splitter = { ',', ';' };
        var AddressCollection = To?.Split(Splitter);
        for (var x = 0; x < AddressCollection?.Length; ++x) {
            if (!string.IsNullOrEmpty(AddressCollection[x].Trim()))
                Mail.To.Add(AddressCollection[x]);
        }

        if (!string.IsNullOrEmpty(CC)) {
            AddressCollection = CC.Split(Splitter);
            for (var x = 0; x < AddressCollection?.Length; ++x) {
                if (!string.IsNullOrEmpty(AddressCollection[x].Trim()))
                    Mail.CC.Add(AddressCollection[x]);
            }
        }
        if (!string.IsNullOrEmpty(Bcc)) {
            AddressCollection = Bcc.Split(Splitter);
            for (var x = 0; x < AddressCollection?.Length; ++x) {
                if (!string.IsNullOrEmpty(AddressCollection[x].Trim()))
                    Mail.Bcc.Add(AddressCollection[x]);
            }
        }
        Mail.From = new MailAddress(From ?? "");
        Mail.Subject = Subject;
        foreach (var Attachment in Attachments) Mail.Attachments.Add(Attachment);
        Mail.Priority = Priority;
        Mail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
        Mail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
        using var smtp = new SmtpClient(Server, Port);
        if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password)) smtp.Credentials = new System.Net.NetworkCredential(UserName, Password);
        smtp.EnableSsl = UseSSL;
        smtp.Send(Mail);
    }
    #endregion Public Functions
}
