/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Xml;
using Z.Extensions;

namespace Z.Net;

/// <summary>TCMB Foreign Exchange rate gather/parse</summary>
public class TcmbForeignExchangeRates : Dictionary<MoneyTypes, TcmbForeignExchangeData>, IDisposable
{
    #region Fields / Properties
    private bool refreshing = false;
    private readonly System.Timers.Timer Tmr = new(10 * 60000);
    /// <summary>Orginial data last refresh time</summary>
    public string LastPageUpdate { get; set; }
    /// <summary>Gets the last refresh date.</summary>
    public DateTime LastRefresh { get; private set; }
    /// <summary>Refresh timer interval (in minutes)</summary>
    public int TimeoutMinutes {
        get => (int)Tmr.Interval / 60000;
        set => Tmr.Interval = value * 60000;
    }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    /// <param name="timeoutInMinutes">Refresh timeout</param>
    /// <param name="useTimer">Start timer automatically</param>
    public TcmbForeignExchangeRates(int timeoutInMinutes = 10, bool useTimer = true) {
        LastPageUpdate = DateTime.MinValue.ToString();
        Tmr.Elapsed += Tmr_Elapsed;
        TimeoutMinutes = timeoutInMinutes;
        if (useTimer) {
            Tmr.AutoReset = true;
            StartRefreshTimer();
        }
        RefreshData().ConfigureAwait(false);
    }
    #endregion

    #region IDisposable Members
    /// <inheritdoc />
    public void Dispose() {
        StopRefreshTimer();
        if (Tmr != null) {
            Tmr.Elapsed -= Tmr_Elapsed;
            Tmr.Dispose();
        }
        GC.SuppressFinalize(this);
    }
    #endregion

    /// <summary>Force refresh all data</summary>
    public async Task RefreshData() =>
        //var xmlDoc = new XmlDocument();
        //xmlDoc.Load(today);
        //// Xml i�inden tarihi alma - gerekli olabilir
        //DateTime exchangeDate = Convert.ToDateTime(xmlDoc.SelectSingleNode("//Tarih_Date").Attributes["Tarih"].Value);
        //string USD = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='USD']/BanknoteSelling").InnerXml;
        //string EURO = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='EUR']/BanknoteSelling").InnerXml;
        //string POUND = xmlDoc.SelectSingleNode("Tarih_Date/Currency[@Kod='GBP']/BanknoteSelling").InnerXml;
        //Console.WriteLine(string.Format("Tarih {0} USD   : {1}", exchangeDate.ToShortDateString(), USD));
        //Console.WriteLine(string.Format("Tarih {0} EURO  : {1}", exchangeDate.ToShortDateString(), EURO));
        //Console.WriteLine(string.Format("Tarih {0} POUND : {1}", exchangeDate.ToShortDateString(), POUND));
        await Task.Run(() => {
            var HTMLData = string.Empty;
            if (refreshing) return;
            refreshing = true;
            try {
                using var rdr = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
                var xml = new XmlDocument();
                xml.Load(rdr);

                var usd = new TcmbForeignExchangeData { MoneyType = MoneyTypes.USD };
                usd.Sell = xml.SelectNodes("/Tarih_Date/Currency[@Kod ='USD']/ForexSelling")?.Item(0)?.InnerText ?? "";
                usd.Buy = xml.SelectNodes("/Tarih_Date/Currency[@Kod ='USD']/ForexBuying")?.Item(0)?.InnerText ?? "";
                var eur = new TcmbForeignExchangeData { MoneyType = MoneyTypes.USD };
                eur.Sell = xml.SelectNodes("/Tarih_Date/Currency[@Kod ='EUR']/ForexSelling")?.Item(0)?.InnerText ?? "";
                eur.Buy = xml.SelectNodes("/Tarih_Date/Currency[@Kod ='EUR']/ForexBuying")?.Item(0)?.InnerText ?? "";

                var date = xml.SelectSingleNode("/Tarih_Date")?.GetAttribute("Tarih");
                this[MoneyTypes.USD] = usd;
                this[MoneyTypes.EUR] = eur;

                LastRefresh = DateTime.UtcNow;
            } catch { } finally { refreshing = false; }
        }).ConfigureAwait(false);

    /// <summary>Start refresh timer</summary>
    public void StartRefreshTimer() {
        Tmr.Stop();
        Tmr.Start();
    }
    /// <summary>Stop refresh timer</summary>
    //Tmr.Stop();
    public void StopRefreshTimer() => Tmr.Stop();
    private void Tmr_Elapsed(object? sender, System.Timers.ElapsedEventArgs args) => RefreshData().ConfigureAwait(false);

    #region Old Code
    ///// <summary>Force refresh all data</summary>
    //public void RefreshData()
    //{
    //    string HTMLData = string.Empty;
    //    try {
    //        using (var wc = new WebClient()) {
    //            HTMLData = wc.DownloadData("http://www.tcmb.gov.tr/kurlar/today.html").To().String.AsAscii;
    //        }
    //    }
    //    catch { }

    //    if (HTMLData.Length > 80) {
    //        var usdValues = new TcmbForeignExchangeData { MoneyType = MoneyTypes.USD };
    //        int poz = HTMLData.IndexOf("USD");
    //        if ((poz + 38 <= HTMLData.Length) && (poz > 0)) usdValues.Buy = HTMLData.Substring(poz + 38, 10).Trim();
    //        if ((poz + 52 <= HTMLData.Length) && (poz > 0)) usdValues.Sell = HTMLData.Substring(poz + 52, 10).Trim();

    //        var eurValues = new TcmbForeignExchangeData { MoneyType = MoneyTypes.EUR };
    //        poz = HTMLData.IndexOf("EUR");
    //        if ((poz + 38 <= HTMLData.Length) && (poz > 0)) eurValues.Buy = HTMLData.Substring(poz + 38, 10).Trim();
    //        if ((poz + 52 <= HTMLData.Length) && (poz > 0)) eurValues.Sell = HTMLData.Substring(poz + 52, 10).Trim();

    //        poz = HTMLData.IndexOf("SAAT");
    //        if ((poz + 5 <= HTMLData.Length) && (poz > 0)) LastPageUpdate = HTMLData.Substring(poz + 5, 5).Trim();

    //        this[MoneyTypes.USD] = usdValues;
    //        this[MoneyTypes.EUR] = eurValues;

    //        LastRefresh = DateTime.UtcNow;
    //    }
    //}
    #endregion Old Code
}

/// <summary>Class containing buy sell values</summary>
public struct TcmbForeignExchangeData
{
    /// <summary>Foreign Money</summary>
    public MoneyTypes MoneyType { get; set; }
    /// <summary>Buy Value</summary>
    public string Buy { get; set; }
    /// <summary>Sell Value</summary>
    public string Sell { get; set; }
}

/// <summary>Foreign Money</summary>
public enum MoneyTypes : short
{
    /// <summary>U.S. Dollars</summary>
    USD = 0,
    /// <summary>Euro</summary>
    EUR = 1,
}