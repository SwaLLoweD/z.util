﻿/* Copyright (c) <2020> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Z.Net;

/// <summary>Push Notification Sender for Firebase</summary>
public class FcmPushNotification
{
    #region Fields / Properties
    /// <summary>Received exception</summary>
    public Exception? Error { get; set; }
    /// <summary>Response received from firebase server</summary>
    public string? ResponseHtml { get; set; }
    /// <summary>Send result</summary>
    public bool Successful { get; set; }
    /// <summary>Firebase server address</summary>
    private string ServerAddress { get; }
    /// <summary>Application server token</summary>
    private string ServerToken { get; }
    #endregion

    #region Constructors
    // /// <summary>Push Notification Sender for Firebase</summary>
    // public FcmPushNotification() { }

    /// <summary>Push Notification Sender for Firebase</summary>
    public FcmPushNotification(string serverAddress, string serverToken) {
        ServerAddress = serverAddress;
        ServerToken = serverToken;
    }
    #endregion

    /// <summary>Send Push Notification to device or topic</summary>
    public async Task<FcmPushNotification> Notify(string title, string message, string topicOrDeviceId = "/topics/all") {
        //This format sends background notifications
        // var data = new {
        //     to = topicOrDeviceId, // this is for topic or DeviceID /topics/all sends to all
        //     notification = new {
        //         title = title,
        //         body = message
        //         //icon = "myicon"
        //     }
        // };

        var json = $@"""notification"":{{""title"":""{title ?? ""}"",""body"":""{message ?? ""}""}}";
        return await SendJson(json, topicOrDeviceId).ConfigureAwait(false);
    }
    /// <summary>Send Data Message to device or topic</summary>
    public async Task<FcmPushNotification> SendData<T>(T data, Func<object?, string> jsonSerialize, string topicOrDeviceId = "/topics/all", bool highPriority = false) => await SendData(jsonSerialize(data), topicOrDeviceId, highPriority);
    /// <summary>Send Data Message to device or topic</summary>
    public async Task<FcmPushNotification> SendData(string jsonData, string topicOrDeviceId = "/topics/all", bool highPriority = false) {
        //     var data = new {
        //         to = _topicOrDeviceId, // this is for topic or DeviceID /topics/all sends to all
        //         data = new { opc = (int)code, d = mData }
        //         //new {
        //         //    key = "value",
        //         //    key2 = true,
        //         //}
        //     };
        jsonData = $@"""data"": {jsonData ?? ""}";
        return await SendJson(jsonData, topicOrDeviceId, highPriority);
    }
    //Legacy Api
    /// <summary>Send Data Message or notification to device or topic</summary>
    private async Task<FcmPushNotification> SendJson(string jsonFcm, string topicOrDeviceId = "/topics/all", bool highPriority = false) {
        try {
            if (ServerAddress == null || ServerToken == null) throw new NullReferenceException("Server Token or Address can not be null");

            Successful = true;
            ResponseHtml = null;
            Error = null;

            using var client = new HttpClient();
            //client.BaseAddress = new Uri(KLSDef.FCMServerAddr);
            //client.DefaultRequestHeaders
            //                      .Accept
            //                      .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("key", "={0}".ToFormat(KLSDef.FCMServerToken));
            //client.DefaultRequestHeaders.Add("Sender", "id={0}".ToFormat(KLSDef.FCMSenderId));
            var request = new HttpRequestMessage(HttpMethod.Post, ServerAddress);
            request.Headers.TryAddWithoutValidation("Authorization", $"key={ServerToken}");

            var prioStr = highPriority ? @"""priority"": ""high"", " : "";
            var json = $@"{{""to"":""{topicOrDeviceId}"", {prioStr}{jsonFcm}}}";
            //Newtonsoft.Json.JsonConvert.SerializeObject(data);
            request.Content = new StringContent(json, Encoding.UTF8, "application/json"); //CONTENT-TYPE header

            var response = await client.SendAsync(request).ConfigureAwait(false);
            if (response.StatusCode != HttpStatusCode.OK) {
                //TODO: handle retry-timeout for 500 messages
                var errorMessage = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                throw new Exception($"{response.StatusCode} {errorMessage}");
            }
            //var resObj = new {
            //    multicast_id = "0",
            //    success = 0,
            //    failure = 1,
            //    canonical_ids = 0,
            //    results = new {
            //        error = ""
            //    }
            //};
            ResponseHtml = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            //var resObj = Newtonsoft.Json.JsonConvert.DeserializeObject<FCMResponse>(result.Response);
            if (ResponseHtml.Contains("\"failure\":1")) Successful = false;
            //{ "multicast_id":5494634791049072039,"success":0,"failure":1,"canonical_ids":0,"results":[{"error":"NotRegistered"}]}"

            ////if contains a multicast_id field, it's a downstream message
            //if (result.Response.Contains("multicast_id")) {
            //    return _serializer.Deserialize<DownstreamMessageResponse>(result.Response);
            //}

            ////otherwhise it's a topic message
            //return _serializer.Deserialize<TopicMessageResponse>(result.Response);

            //WebRequest webRequest = WebRequest.Create(KLSDef.FCMServerAddr);
            //webRequest.Method = "POST";
            //webRequest.Headers.Add(string.Format("Authorization: key={0}", KLSDef.FCMServerToken));
            //webRequest.Headers.Add(string.Format("Sender: id={0}", KLSDef.FCMSenderId));
            //webRequest.ContentType = "application/json";

            //var data = new {
            //    to = _topicOrDeviceId, // this is for topic or DeviceID /topics/all sends to all
            //    notification = new {
            //        title = _title,
            //        body = _message,
            //        //icon="myicon"
            //    }
            //};
            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            //Byte[] byteArray = Encoding.UTF8.GetBytes(json);

            //webRequest.ContentLength = byteArray.Length;
            //using (var dataStream = webRequest.GetRequestStream()) {
            //    dataStream.Write(byteArray, 0, byteArray.Length);

            //    using (WebResponse webResponse = webRequest.GetResponse()) {
            //        using (var dataStreamResponse = webResponse.GetResponseStream()) {
            //            using (var tReader = new StreamReader(dataStreamResponse)) {
            //                String sResponseFromServer = tReader.ReadToEnd();
            //                result.Response = sResponseFromServer;
            //            }
            //        }
            //    }
            //}

            //try {
            //var result = "-1";

            //var httpWebRequest = (HttpWebRequest)WebRequest.Create(KLSDef.FCMServerAddr);
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Headers.Add("Authorization:key={0}".ToFormat(KLSDef.FCMServerToken));
            //httpWebRequest.Headers.Add("Sender: id={0}".ToFormat(KLSDef.FCMSenderId));
            //httpWebRequest.Method = "POST";
            //httpWebRequest.ContentType = "application/json";

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) {
            //    string json = "{\"to\": \"{0}\",\"data\": {\"message\": \"{1}\",}}".ToFormat(token, msg);
            //    streamWriter.Write(json);
            //    streamWriter.Flush();
            //}

            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
            //    result = streamReader.ReadToEnd();
            //}

            // return result;
            //} catch (Exception ex) {
            //  Response.Write(ex.Message);
            //}
        } catch (Exception ex) {
            Successful = false;
            ResponseHtml = null;
            Error = ex;
        }
        return this;
    }
}
