﻿/*
    Copyright © 2002, The KPD-Team
    All rights reserved.
    http://www.mentalis.org/
    & A. Zafer YURDAÇALIŞ

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    - Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.

    - Neither the name of the KPD-Team, nor the names of its contributors
       may be used to endorse or promote products derived from this
       software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Z.Net
{
    /// <summary>Queries the appropriate whois server for a given domain name and returns the results.</summary>
    public sealed class WhoisResolver
    {
        #region Constructors
        /// <summary>Do not allow any instances of this class.</summary>
        private WhoisResolver() { }
        #endregion

        /// <summary>Queries an appropriate whois server for the given domain name.</summary>
        /// <param name="domain">The domain name to retrieve the information of.</param>
        /// <returns>A string that contains the whois information of the specified domain name.</returns>
        /// <exception cref="ArgumentNullException"><c>domain</c> is null.</exception>
        /// <exception cref="ArgumentException"><c>domain</c> is invalid.</exception>
        /// <exception cref="SocketException">A network error occured.</exception>
        public static async Task<string> WhoisDnsAsync(string domain) => await Task.Run(() => WhoisDns(domain)).ConfigureAwait(false);

        /// <summary>Queries an appropriate whois server for the given domain name.</summary>
        /// <param name="domain">The domain name to retrieve the information of.</param>
        /// <returns>A string that contains the whois information of the specified domain name.</returns>
        /// <exception cref="ArgumentNullException"><c>domain</c> is null.</exception>
        /// <exception cref="ArgumentException"><c>domain</c> is invalid.</exception>
        /// <exception cref="SocketException">A network error occured.</exception>
        public static string WhoisDns(string domain) {
            if (domain == null) throw new ArgumentNullException(nameof(domain));
            var ccStart = domain.LastIndexOf(".");
            if (ccStart < 0 || ccStart == domain.Length) throw new ArgumentOutOfRangeException(nameof(domain));

            var ret = "";
            Socket? s = null;
            try {
                var cc = domain[(ccStart + 1)..];
                s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                s.Connect(new IPEndPoint(Dns.GetHostEntry(cc + ".whois-servers.net").AddressList[0], 43));
                s.Send(Encoding.ASCII.GetBytes(domain + "\r\n"));
                var buffer = new byte[1024];
                var recv = s.Receive(buffer);
                while (recv > 0) {
                    ret += Encoding.Default.GetString(buffer, 0, recv);
                    recv = s.Receive(buffer);
                }
                s.Shutdown(SocketShutdown.Both);
            } catch {
                throw new SocketException();
            } finally {
                s?.Close();
            }
            return ret;
        }

        /// <summary>Gets WHOIS data for IP Address</summary>
        /// <param name="ip">IP Address to lookup</param>
        /// <returns></returns>
        public static async Task<string> WhoisIpAsync(IPAddress ip) => await Task.Run(() => WhoisIp(ip)).ConfigureAwait(false);
        /// <summary>Gets WHOIS data for IP Address</summary>
        /// <param name="ip">IP Address to lookup</param>
        /// <returns></returns>
        private static string WhoisIp(IPAddress ip) {
            if (ip == null) throw new ArgumentNullException(nameof(ip));
            var ret = "";
            Socket? s = null;
            try {
                s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                s.Connect(new IPEndPoint(Dns.GetHostEntry("whois.ripe.net").AddressList[0], 43)); //may be also arin.net
                s.Send(Encoding.ASCII.GetBytes(ip.ToString() + "\r\n"));
                var buffer = new byte[1024];
                var recv = s.Receive(buffer);
                while (recv > 0) {
                    ret += Encoding.Default.GetString(buffer, 0, recv);
                    recv = s.Receive(buffer);
                }
                s.Shutdown(SocketShutdown.Both);
            } catch {
                throw new SocketException();
            } finally {
                s?.Close();
            }
            return ret;
        }
    }
}