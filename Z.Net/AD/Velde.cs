using System;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Security;
using System.Text;

namespace Z.Net.AD
{
    /// <summary>Active Directory,Database,File Utilities </summary>
    public class Velde
    {
        /// <summary>Active Directory</summary>
        public class AD
        {
            /// <summary>Error</summary>
            public string err = "";
            /// <summary>Domain DN</summary>
            public string domainDN = "";

            /// <summary>Debug mode</summary>
            public bool debug = false; //set to true to enable messages

            /// <summary>Constructor</summary>
            public AD()
            {
                domainDN = getDomainDN();
            }
            /// <summary></summary>
            public bool AddToGroup(string userDN, string groupDN)
            {
                try {
                    DirectoryEntry de = new DirectoryEntry("LDAP://" + groupDN);

                    de.Properties["member"].Add(userDN);
                    de.CommitChanges();
                    de.Close();
                    return true;
                }
                catch (Exception ex) {
                    var err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.addToGroup():\n\n" + ex.Message);
                    }
                    return false;
                }
            } //add a user to a group

            /// <summary></summary>
            public bool ChangePassword(string userDn, string oldPassword, string newPassword)
            {
                try {
                    DirectoryEntry uEntry = new DirectoryEntry("LDAP://" + userDn);
                    uEntry.Invoke("ChangePassword", new object[] { oldPassword, newPassword });
                    uEntry.Properties["LockOutTime"].Value = 0; //unlock account
                    uEntry.CommitChanges();
                    uEntry.Close();
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.resetPassword():\n\n" + err);
                    }
                    return false;
                }
            } //change a users password.

            /// <summary></summary>
            public bool CNExists(string cn, string baseDN)
            {
                DirectoryEntry entry = GetDE(baseDN);

                try {
                    DirectorySearcher search = new DirectorySearcher(entry);
                    search.Filter = "(cn=" + cn + ")";
                    SearchResult result = search.FindOne();

                    if (result != null) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception ex) {
                    string err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.samExists()\n\n" + err);
                    }
                    return false;
                }
            } //find user by cn

            /// <summary></summary>
            public bool CreateUserAccount(string parentOUDN, string samName, string userPassword, string firstName, string lastName)
            {
                try {
                    string connectionPrefix = "LDAP://" + parentOUDN;
                    DirectoryEntry de = new DirectoryEntry(connectionPrefix);
                    DirectoryEntry newUser = de.Children.Add("CN=" + firstName + " " + lastName, "user");
                    newUser.Properties["samAccountName"].Value = samName;
                    newUser.Properties["userPrincipalName"].Value = samName;
                    newUser.Properties["sn"].Add(lastName);
                    newUser.Properties["name"].Value = firstName + " " + lastName;
                    newUser.Properties["givenName"].Add(firstName);

                    newUser.CommitChanges();
                    newUser.Invoke("SetPassword", new object[] { userPassword });
                    newUser.CommitChanges();
                    int val = (int)newUser.Properties["userAccountControl"].Value;
                    newUser.Properties["userAccountControl"].Value = val | 0x0200;
                    newUser.CommitChanges();
                    de.Close();
                    newUser.Close();
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.createUserAccount():\n\n" + err);
                    }
                    return false;
                }
                /*
                    //Add this to the create account method
                    int val = (int)newUser.Properties["userAccountControl"].Value;
                         //newUser is DirectoryEntry object
                    newUser.Properties["userAccountControl"].Value = val | 0x80000;
                        //ADS_UF_TRUSTED_FOR_DELEGATION

                 *
                 * UserAccountControlFlags
                 * CONST   HEX
                    -------------------------------
                    SCRIPT 0x0001
                    ACCOUNTDISABLE 0x0002
                    HOMEDIR_REQUIRED 0x0008
                    LOCKOUT 0x0010
                    PASSWD_NOTREQD 0x0020
                    PASSWD_CANT_CHANGE 0x0040
                    ENCRYPTED_TEXT_PWD_ALLOWED 0x0080
                    TEMP_DUPLICATE_ACCOUNT 0x0100
                    NORMAL_ACCOUNT 0x0200
                    INTERDOMAIN_TRUST_ACCOUNT 0x0800
                    WORKSTATION_TRUST_ACCOUNT 0x1000
                    SERVER_TRUST_ACCOUNT 0x2000
                    DONT_EXPIRE_PASSWORD 0x10000
                    MNS_LOGON_ACCOUNT 0x20000
                    SMARTCARD_REQUIRED 0x40000
                    TRUSTED_FOR_DELEGATION 0x80000
                    NOT_DELEGATED 0x100000
                    USE_DES_KEY_ONLY 0x200000
                    DONT_REQ_PREAUTH 0x400000
                    PASSWORD_EXPIRED 0x800000
                    TRUSTED_TO_AUTH_FOR_DELEGATION 0x1000000
                 * */
            } //create a user account

            /* public void denyChangePassword(string userDN)
             {
                 Velde.AD adc = new AD();
                 DirectoryEntry User = adc.getDE(userDN);

                 const string PASSWORD_GUID = "{ab721a53-1e2f-11d0-9819-00aa0040529b}";
                 // const int ADS_UF_ACCOUNTDISABLE = 2;
                 // const int ADS_UF_PASSWORD_EXPIRED = 0x800000;
                 // const int ADS_UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION = 0x1000000;

                 string[] trustees = new string[] { @"NT AUTHORITY\SELF", "EVERYONE" };

                 ActiveDs.IADsSecurityDescriptor sd = (ActiveDs.IADsSecurityDescriptor)
                    User.Properties["ntSecurityDescriptor"].Value;
                 ActiveDs.IADsAccessControlList acl = (ActiveDs.IADsAccessControlList)sd.DiscretionaryAcl;
                 ActiveDs.IADsAccessControlEntry ace = new ActiveDs.AccessControlEntry();

                 foreach (string trustee in trustees)
                 {
                     ace.Trustee = trustee;
                     ace.AceFlags = 0;
                     ace.AceType = (int)ActiveDs.ADS_ACETYPE_ENUM.ADS_ACETYPE_ACCESS_DENIED_OBJECT;
                     ace.Flags = (int)ActiveDs.ADS_FLAGTYPE_ENUM.ADS_FLAG_OBJECT_TYPE_PRESENT;
                     ace.ObjectType = PASSWORD_GUID;
                     ace.AccessMask = (int)ActiveDs.ADS_RIGHTS_ENUM.ADS_RIGHT_DS_CONTROL_ACCESS;
                     acl.AddAce(ace);
                 }
                 sd.DiscretionaryAcl = acl;
                 User.Properties["ntSecurityDescriptor"].Value = sd;
                 User.CommitChanges();
             } //deny the ability for a user to change their password
             */
            /// <summary></summary>
            public bool DisableAccount(string userDn)
            {
                try {
                    DirectoryEntry user = new DirectoryEntry(userDn);
                    int val = (int)user.Properties["userAccountControl"].Value;
                    user.Properties["userAccountControl"].Value = val | 0x2;
                    //ADS_UF_ACCOUNTDISABLE;
                    user.CommitChanges();
                    user.Close();
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.disableAccount():\n\n" + err);
                    }
                    return false;
                }
            } //disable an account

            /// <summary></summary>
            public string EmpIDtoSamName(string empID)
            {
                DirectoryEntry entry = GetDE();

                try {
                    DirectorySearcher search = new DirectorySearcher(entry);
                    search.Filter = "(employeeID=" + empID + ")";
                    SearchResult result = search.FindOne();

                    if (result != null) {
                        return result.Properties["SAMAccountName"][0].ToString();
                    }
                    else {
                        return "Could not find any employee with that ID#";
                    }
                }
                catch (Exception ex) {
                    string err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.empIDtoName()\n\n" + err);
                    }
                    return err;
                }
            } //return samName from employeeID
            /// <summary></summary>
            public bool EnableAccount(string userDn)
            {
                try {
                    DirectoryEntry user = new DirectoryEntry("LDAP://" + userDn);
                    user.Properties["userAccountControl"].Value = 0x200;
                    //ADS_UF_NORMAL_ACCOUNT;
                    user.CommitChanges();
                    user.Close();
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.enableAccount():\n\n" + err);
                    }
                    return false;
                }
            } //enable a user account
            /// <summary></summary>
            public string GeneratePassword(string seed, int length)
            {
                string input = seed;
                string /*rndNum,*/ specChar;

                Random rnd = new Random();
                int num1 = rnd.Next(0, 9);
                int num2 = rnd.Next(0, 9);

                switch (num2) {
                    case 0:
                        specChar = "!";
                        break;

                    case 1:
                        specChar = "@";
                        break;

                    case 2:
                        specChar = "#";
                        break;

                    case 3:
                        specChar = "$";
                        break;

                    case 4:
                        specChar = "%";
                        break;

                    case 5:
                        specChar = "^";
                        break;

                    case 6:
                        specChar = "&";
                        break;

                    case 7:
                        specChar = "*";
                        break;

                    case 8:
                        specChar = "(";
                        break;

                    case 9:
                        specChar = ")";
                        break;

                    default:
                        specChar = "!";
                        break;
                }

                //rndNum = num1.ToString();
                input = specChar + input + num1;

                while (input.Length < length) {
                    input += "0";
                }

                input = input.Replace('a', '@');
                input = input.Replace('d', 'D');
                input = input.Replace('i', '!');
                input = input.Replace('l', '1');
                input = input.Replace('o', '0');
                input = input.Replace('O', '0');
                input = input.Replace('s', '$');
                input = input.Replace('q', 'Q');
                input = input.Replace('b', 'B');
                input = input.Replace('e', '3');
                input = input.Replace('h', 'H');
                input = input.Replace('n', 'N');

                return input;
            } //generate passwird based on seed string
            /// <summary></summary>
            public string GeneratePassword(int length)
            {
                string pass = "";

                Random rnd = new Random();
                int ch;
                while (pass.Length < length) {
                    ch = rnd.Next(32, 127);
                    pass += Convert.ToChar(ch);
                }
                return pass;
            } //generate password of given length
            /// <summary></summary>
            public DirectoryEntry GetDE(string baseDN = null)
            {
                DirectoryEntry de = new DirectoryEntry("LDAP://" + (baseDN ?? domainDN));
                de.AuthenticationType = AuthenticationTypes.Secure;

                return de;
            } //create directory entry object
            /// <summary></summary>
            public SearchResultCollection GetOUComputers(string ouDN = null)
            {
                DirectorySearcher deSearch;
                if (ouDN == null) deSearch = new DirectorySearcher("LDAP://" + domainDN);
                else deSearch = new DirectorySearcher(GetDE(ouDN));

                deSearch.Filter = "(objectClass=Computer)";
                deSearch.SearchScope = SearchScope.OneLevel;
                SearchResultCollection results = deSearch.FindAll();

                return results;
            } //
            /// <summary></summary>
            private string getDomainDN()
            {
                try {
                    DirectoryEntry de = new DirectoryEntry("LDAP://RootDSE");
                    de.AuthenticationType = AuthenticationTypes.Secure;

                    return de.Properties["defaultnamingcontext"][0].ToString();
                }
                catch (Exception ex) {
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.getDomainDN(): \n\n" + ex.Message);
                    }
                    err = ex.Message;
                    return null;
                }
            } //gets the distinguished name of your domain
            /// <summary></summary>
            public string GetUserDN(string samAccountName)
            {
                DirectoryEntry entry = GetDE();
                String account = samAccountName.Replace(@"Domain\", "");

                try {
                    DirectorySearcher search = new DirectorySearcher(entry);
                    search.Filter = "(SAMAccountName=" + account + ")";
                    SearchResult result = search.FindOne();

                    if (result != null) {
                        return result.Properties["distinguishedName"][0].ToString();
                    }
                    else {
                        return "";
                    }
                }
                catch (Exception ex) {
                    string err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.getUserDN:\n\n" + err);
                    }
                    return "";
                }
            }  //find user by sam name.
            /// <summary></summary>
            private string incName(string samInitial)
            {
                if (SamExists(samInitial) == true) {
                    int i = 1;
                    string newSam = samInitial;
                    while (SamExists(newSam)) {
                        newSam = samInitial + i.ToString();
                        Console.WriteLine(newSam);
                        Console.Read();
                        i++;
                    }

                    return newSam;
                }
                else {
                    return samInitial;
                }
            } //increments a number on the end of a given samName until it is unique
            /// <summary></summary>
            public string[] ListOUs(string baseDN = null)
            {
                DirectoryEntry entry = baseDN == null ? GetDE() : GetDE(baseDN);
                string[] ous = new string[255];
                try {
                    DirectorySearcher search = new DirectorySearcher(entry);
                    search.SearchScope = SearchScope.OneLevel;
                    search.Filter = "(objectCategory=organizationalUnit)";
                    SearchResultCollection results = search.FindAll();

                    for (int i = 0; i < results.Count; i++) {
                        ous[i] = results[i].Properties["ADsPath"][0].ToString();
                    }
                    return ous;
                }
                catch (Exception ex) {
                    var err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.listOUs()\n\n" + ex.Message);
                    }
                    return null;
                }
            } //list child ous of an object
            /// <summary></summary>
            public bool MoveObject(string objectDN, string targetDN)
            {
                try {
                    DirectoryEntry eLocation = new DirectoryEntry("LDAP://" + objectDN);
                    DirectoryEntry nLocation = new DirectoryEntry("LDAP://" + targetDN);
                    string newName = eLocation.Name;
                    eLocation.MoveTo(nLocation, newName);
                    nLocation.Close();
                    eLocation.Close();
                    return true;
                }
                catch (Exception ex) {
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.moveObject()\n\n" + ex.Message);
                    }
                    err = ex.Message;
                    return false;
                }
            } //move an obect in active directory
            /// <summary></summary>
            public bool RemoveUserFromGroup(string userDN, string groupDN)
            {
                try {
                    DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + groupDN);
                    dirEntry.Properties["member"].Remove(userDN);
                    dirEntry.CommitChanges();
                    dirEntry.Close();
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.removeUserFromGroup():\n\n" + err);
                    }
                    return false;
                }
            } //remove a user from a group
            /// <summary></summary>
            public bool RenameObject(string objectDn, string newName)
            {
                try {
                    DirectoryEntry child = new DirectoryEntry("LDAP://" + objectDn);
                    child.Rename("CN=" + newName);
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.renameObject():\n\n" + err);
                    }
                    return false;
                }
            } //rename an AD object
            /// <summary></summary>
            public bool ResetPassword(string userDn, string password)
            {
                try {
                    DirectoryEntry uEntry = new DirectoryEntry("LDAP://" + userDn);
                    uEntry.Invoke("SetPassword", new object[] { password });
                    uEntry.Properties["LockOutTime"].Value = 0; //unlock account
                    uEntry.CommitChanges();
                    uEntry.Close();
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.resetPassword():\n\n" + err);
                    }
                    return false;
                }
            } //reset a users password
            /// <summary></summary>
            public string ReturnGroupDN(string groupName)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(&(SAMAccountName=" + groupName + "))");
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();

                if (results != null) {
                    try {
                        DirectoryEntry user = results.GetDirectoryEntry();
                        return user.Properties["distinguishedName"].Value.ToString();
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.returnGroupDN():\n\n" + err);
                        }
                        return err;
                    }
                }
                return err;
            } //get distinguished name of a group
            /// <summary></summary>
            public string ReturnProperty(string objectDN, string propertyName, bool execute)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(distinguishedName=" + objectDN + ")");
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();

                if (results != null) {
                    try {
                        DirectoryEntry user = results.GetDirectoryEntry();
                        return user.Properties[propertyName].Value.ToString();
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.returnProperty():\n\n" + err);
                        }
                        return err;
                    }
                }
                return err;
            } //get AD property
            /// <summary></summary>
            public string ReturnProperty(string samAccountName, string propertyName)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(&(objectclass=user)(objectcategory=person)(SAMAccountName=" + samAccountName + "))");
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();

                if (results != null) {
                    try {
                        DirectoryEntry user = results.GetDirectoryEntry();
                        return user.Properties[propertyName].Value.ToString();
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.returnProperty():\n\n" + err);
                        }
                        return err;
                    }
                }
                return err;
            } //get AD property
            /// <summary></summary>
            public string[] ReturnPropertyNames(string objectDN)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(distinguishedName=" + objectDN + ")");
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();
                string[] props = new string[512];
                int i = 0;
                if (results != null) {
                    try {
                        DirectoryEntry user = results.GetDirectoryEntry();
                        //build the string[] of properties

                        foreach (string name in user.Properties.PropertyNames) {
                            props[i] = name;
                            i++;
                        }
                        return props;
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.returnPropertyCollection():\n\n" + err);
                        }
                        return null;
                    }
                }
                return null;
            } //return ad properties of an object
            /// <summary></summary>
            public string ReturnPropertyByCN(string cn, string propertyName, string baseOUDN)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(&(objectclass=user)(objectcategory=person)(cn=" + cn + "))");
                ds.SearchRoot = GetDE(baseOUDN);
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();

                if (results != null) {
                    try {
                        DirectoryEntry user = results.GetDirectoryEntry();
                        if (user.Properties[propertyName].Value.ToString() != "Exception has been thrown by the target of an invocation.") {
                            return user.Properties[propertyName].Value.ToString();
                        }
                        else {
                            return null;
                        }
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.returnPropertyByCN():\n\n" + err);
                        }
                        return null;
                    }
                }
                return err;
            } //return a property by cn
            /// <summary></summary>
            public bool SamExists(string samAccountName)
            {
                DirectoryEntry entry = GetDE();
                String account = samAccountName.Replace(@"Domain\", "");

                try {
                    DirectorySearcher search = new DirectorySearcher(entry);
                    search.Filter = "(SAMAccountName=" + account + ")";
                    SearchResult result = search.FindOne();

                    if (result != null) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                catch (Exception ex) {
                    string err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.AD.samExists()\n\n" + err);
                    }
                    return false;
                }
            }  //find user by sam name.
            /// <summary></summary>
            public bool SetProperty(string samAccountName, string propertyName, string newValue)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(&(objectclass=user)(objectcategory=person)(SAMAccountName=" + samAccountName + "))");
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();

                if (results != null) {
                    try {
                        DirectoryEntry updateEntry = results.GetDirectoryEntry();
                        updateEntry.Properties[propertyName].Value = newValue;
                        updateEntry.CommitChanges();
                        updateEntry.Close();
                        return true;
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.getDE():\n\n" + err);
                        }
                        return false;
                    }
                }
                return false;
            } //set value of property
            /// <summary></summary>
            public bool SetProperty(string objectDN, string propertyName, string newValue, bool execute)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(distinguishedName=" + objectDN + ")");
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();

                if (results != null) {
                    try {
                        DirectoryEntry updateEntry = results.GetDirectoryEntry();
                        updateEntry.Properties[propertyName].Value = newValue;
                        updateEntry.CommitChanges();
                        updateEntry.Close();
                        return true;
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.getDE():\n\n" + err);
                        }
                        return false;
                    }
                }
                return false;
            } //set value of property
            /// <summary></summary>
            public bool SetProperty(string samAccountName, string propertyName, int newValue)
            {
                DirectoryEntry de = GetDE();
                DirectorySearcher ds = new DirectorySearcher(de);
                ds.Filter = ("(&(objectclass=user)(objectcategory=person)(SAMAccountName=" + samAccountName + "))");
                ds.SearchScope = SearchScope.Subtree;
                SearchResult results = ds.FindOne();

                if (results != null) {
                    try {
                        DirectoryEntry updateEntry = results.GetDirectoryEntry();
                        updateEntry.Properties[propertyName].Value = newValue;
                        updateEntry.CommitChanges();
                        updateEntry.Close();
                        return true;
                    }
                    catch (Exception ex) {
                        err = ex.Message;
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.AD.getDE():\n\n" + err);
                        }
                        return false;
                    }
                }
                return false;
            } //set value of property
        }

        /// <summary></summary>
        public class Network
        {
            /// <summary></summary>
            public bool debug = false;//set to true to enable messages
            /// <summary></summary>
            public string err = "";
            /// <summary></summary>
            public string getHttpSource(string url)
            {
                try {
                    // used to build entire input
                    StringBuilder sb = new StringBuilder();

                    // used on each read operation
                    byte[] buf = new byte[8192];

                    // prepare the web page we will be asking for
                    HttpWebRequest request = (HttpWebRequest)
                        WebRequest.Create(url);

                    // execute the request
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    // we will read data via the response stream
                    Stream resStream = response.GetResponseStream();

                    string tempString = null;
                    int count = 0;

                    do {
                        // fill the buffer with data
                        count = resStream.Read(buf, 0, buf.Length);

                        // make sure we read some data
                        if (count != 0) {
                            // translate from bytes to ASCII text
                            tempString = Encoding.ASCII.GetString(buf, 0, count);

                            // continue building the string
                            sb.Append(tempString);
                        }
                    }
                    while (count > 0); // any more data to read?

                    // print out page source
                    return sb.ToString();
                }
                catch (Exception e) {
                    return e.Message;
                }
            } //returns HTTP source to a string
            /// <summary></summary>
            public bool isAlive(string svr) // check if host is alive
            {
                // create the ping process
                Process ping = new Process();
                ping.StartInfo.FileName = "ping.exe";
                ping.StartInfo.CreateNoWindow = true;
                ping.StartInfo.UseShellExecute = false;
                ping.StartInfo.RedirectStandardError = true;
                ping.StartInfo.RedirectStandardOutput = true;

                // add arguments to the ping process
                ping.StartInfo.Arguments = "-n 1 -w 1500 " + svr;

                ping.Start();
                ping.WaitForExit();

                // store ping's sdtOut
                string tmp = ping.StandardOutput.ReadToEnd();
                ping.Dispose();

                // check DNS
                if (tmp.Contains("could not find")) {
                    return false;
                }

                // check to see if host is alive
                if (tmp.Contains("TTL=")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            /// <summary></summary>
            public bool isAlive(string ipAddress, int port)
            {
                try {
                    //create the socket instance...
                    Socket m_socClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    // get the remote IP address...
                    IPAddress ip = IPAddress.Parse(ipAddress);
                    int iPortNo = port;

                    //create the end point
                    IPEndPoint ipEnd = new IPEndPoint(ip, iPortNo);

                    //connect to the remote host...
                    m_socClient.Connect(ipEnd);

                    m_socClient.Close();
                    return true;
                }
                catch (SocketException se) {
                    var err = se.Message;
                    if (debug) {
                        //MessageBox.Show(se.Message);
                    }
                    return false;
                }
            } //check TCP Port
            /// <summary></summary>
            public bool killHost(string svr)
            {
                // create the shutdown process
                Process shutdown = new Process();
                shutdown.StartInfo.FileName = "shutdown.exe";
                shutdown.StartInfo.CreateNoWindow = true;
                shutdown.StartInfo.UseShellExecute = false;
                shutdown.StartInfo.RedirectStandardOutput = true;
                shutdown.StartInfo.RedirectStandardError = true;

                // Add arguments to the shutdown command
                shutdown.StartInfo.Arguments = "-f -s -t 0 /m " + svr;

                try {
                    shutdown.Start();
                    shutdown.WaitForExit();

                    // store the stdOut from shutdown
                    string tmp = shutdown.StandardError.ReadToEnd();

                    if (tmp != "") {
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.Network.killHost(): \n\n" + tmp);
                        }
                        return false;
                    }

                    return true;
                }
                catch {
                    return false;
                }
            } //shutdown host
            /// <summary></summary>
            public bool killHost(string svr, string username, string password)
            {
                // create the shutdown process
                Process shutdown = new Process();
                shutdown.StartInfo.FileName = "shutdown.exe";
                shutdown.StartInfo.UserName = username;
                SecureString pww = new SecureString();

                for (int i = 0; i < password.Length; i++) {
                    pww.AppendChar(password[i]);
                }
                shutdown.StartInfo.Password = pww;
                shutdown.StartInfo.CreateNoWindow = true;
                shutdown.StartInfo.UseShellExecute = false;
                shutdown.StartInfo.RedirectStandardOutput = true;
                shutdown.StartInfo.RedirectStandardError = true;

                // Add arguments to the shutdown command
                shutdown.StartInfo.Arguments = "-f -s -t 0 /m " + svr;

                try {
                    shutdown.Start();
                    shutdown.WaitForExit();

                    // store the stdOut from shutdown
                    string tmp = shutdown.StandardError.ReadToEnd();

                    if (tmp != "") {
                        if (debug) {
                            //MessageBox.Show("Velde.Utilities.Network.killHost(): \n\n" + tmp);
                        }
                        return false;
                    }
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.Network.killHost(): \n\n" + err);
                    }
                    return false;
                }
            } //shutdown host using password
            /// <summary></summary>
            public bool mapDrive(char letter, string path)
            {
                Process net = new Process();
                net.StartInfo.FileName = "net.exe";
                net.StartInfo.Arguments = "use " + letter + ": " + path;
                net.StartInfo.CreateNoWindow = true;
                net.StartInfo.UseShellExecute = false;
                net.StartInfo.RedirectStandardError = true;
                net.Start();
                net.WaitForExit();
                string res = net.StandardError.ReadToEnd();

                if (res != "") {
                    err = res;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.Network.mapDrive(): \n\n" + err);
                    }
                    return false;
                }
                return true;
            } //map network drive
            /// <summary></summary>
            public bool sendMail(string from, string to, string subject, string body, bool html, string server, int port)
            {
                try {
                    MailMessage newMail = new MailMessage();
                    newMail.From = new MailAddress(from);
                    newMail.To.Add(to);
                    newMail.Subject = subject;
                    newMail.Body = body;
                    newMail.IsBodyHtml = html;

                    SmtpClient svr = new SmtpClient();
                    svr.Host = server;
                    svr.Port = port;
                    svr.Send(newMail);
                    return true;
                }
                catch (Exception ex) {
                    err = ex.Message;
                    if (debug) {
                        //MessageBox.Show("Velde.Utilities.Network.sendMail(): \n\n" + err);
                    }
                    return false;
                }
            } //send email
        }
    }
}