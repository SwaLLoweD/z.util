﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using Z.Extensions;

namespace Z.Net.AD
{
    /// <summary>LDAP Client</summary>
    public class LdapConnection : IDisposable
    {
        /// <summary>LDAP Client</summary>
        public LdapConnection()
        {
            this.AuthenticationTypes = AuthenticationTypes.ReadonlyServer | AuthenticationTypes.Secure;
        }
        /// <inheritdoc/>
        public void Dispose()
        {
            if (this.RootEntry != null)
                this.RootEntry.Dispose();
        }
        /// <summary>Default Domain</summary>
        public string DefaultDomain { get; set; }
        /// <summary>Authentication Type</summary>
        public AuthenticationTypes AuthenticationTypes { get; set; }
        /// <summary>Root DN</summary>
        public DirectoryEntry RootEntry { get; set; }
        /// <summary>LDAP Server Url</summary>
        public string ServerUrl { get; set; }
        /// <summary></summary>
        public string Root { get; set; }
        /// <summary>LDAP Server Username</summary>
        public string UserName { get; set; }
        /// <summary>LDAP Server Password</summary>
        public string Password { get; set; }

        /// <summary>Connect to the LDAP Server</summary>
        public bool Connect()
        {
            if (this.RootEntry != null)
                this.RootEntry.Dispose();

            string tmpUserName = this.UserName;

            if (!string.IsNullOrEmpty(DefaultDomain) && (tmpUserName.IndexOf("\\") < 0))
                tmpUserName = DefaultDomain + "\\" + this.UserName;

            if (!ServerUrl.StartsWith("LDAP://", StringComparison.InvariantCultureIgnoreCase)) ServerUrl = "LDAP://" + ServerUrl;

            string ldapPath = (this.ServerUrl.EndsWith("/") ? this.ServerUrl : this.ServerUrl + "/") + this.Root;

            try {
                DirectoryEntry entry = new DirectoryEntry(ldapPath, tmpUserName, this.Password, this.AuthenticationTypes);

                if (!string.IsNullOrEmpty(entry.Name)) {
                    this.UserName = tmpUserName;
                    this.RootEntry = entry;

                    return true;
                }
            }
            catch (COMException ex) {
                if ((uint)ex.ErrorCode == 0x8007052E) return false;//logon failure
                else if (ex.ErrorCode == -2147016665) {
                    //Fallback to unsecure connection
                    AuthenticationTypes = AuthenticationTypes & (~AuthenticationTypes.Secure);
                    Connect();
                }
                else if (ex.ErrorCode == -2147016654) {
                    throw new LdapException("Invalid DN Specified");
                }
                throw new LdapException("Unknown exception while connecting to ldap server.", ex);
            }
            catch (Exception ex) {
                throw new LdapException("Unknown exception while connecting to ldap server.", ex);
            }

            return false;
        }

        /// <summary>Search directory using filter</summary>
        public DirectorySearcher Search(string filter)
        {
            if (this.RootEntry == null)
                throw new LdapException("No connection available.");

            DirectorySearcher searcher = new DirectorySearcher(this.RootEntry, filter);

            searcher.SearchScope = SearchScope.Subtree;
            searcher.ReferralChasing = ReferralChasingOption.All; // use mirrored ad

            return searcher;
        }

        /// <summary>Get Properties</summary>
        public IDictionary<string, string> GetProperties(params string[] properties)
        {
            Dictionary<string, string> resolvedProperties = new Dictionary<string, string>();

            string filter = "(&(" + LogonId + "=" + this.UserName + "))";
            string strip = "CN=";

            using (DirectorySearcher searcher = Search(filter)) {
                SearchResult result = searcher.FindOne();

                if (result != null) {
                    foreach (string property in properties) {
                        ResultPropertyValueCollection attributeValue = result.Properties[property];

                        if (attributeValue.Count > 0) {
                            List<string> values = new List<string>();

                            foreach (object singleValue in attributeValue) {
                                string v = singleValue.ToString();

                                if (v.StartsWith(strip)) {
                                    // strip xx from "CN=xx,CN=yy,DC=zzz"
                                    int comma = v.IndexOf(",", strip.Length);

                                    if (comma > 0) {
                                        v = v.Substring(strip.Length, comma - strip.Length);
                                    }
                                }

                                values.Add(v);
                            }

                            resolvedProperties[property] = values.ToArray().To().CSV(";");
                        }
                    }
                }
            }
            return resolvedProperties;
        }
        /// <summary></summary>
        public static string LogonId = "sAMAccountName"; // ad uses SAMAccountName, use "uid" for other types of ldap.
        /// <summary></summary>
        public static string EMail = "mail";
        /// <summary></summary>
        public static string Name = "givenName";
        /// <summary></summary>
        public static string Surname = "sn";
        /// <summary></summary>
        public static string Groups = "memberOf";
    }

    /// <summary>LDAP Exception</summary>
    public class LdapException : Exception
    {
        /// <summary>LDAP Exception</summary>
        public LdapException(string message) : base(message) { }

        /// <summary>LDAP Exception</summary>
        public LdapException(string message, Exception innerException) : base(message, innerException) { }
    }
}