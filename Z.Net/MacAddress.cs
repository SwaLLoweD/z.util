/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Globalization;
using System.Text;
using Z.Extensions;

namespace Z.Net;

/// <summary>MAC Address</summary>
[Serializable]
public class MacAddress
{
    #region Fields / Properties
    /// <summary>MAC Address in bytes</summary>
    public byte[] Address { get; protected set; }
    #endregion

    #region Constructors
    /// <summary>MAC Address</summary>
    public MacAddress() => Address = new byte[6];

    /// <summary>MAC Address</summary>
    /// <param name="b">initial address</param>
    public MacAddress(byte[] b) => Address = b;
    #endregion

    /// <summary>Dump MacAddress to byte[]</summary>
    public byte[] GetAddressBytes() => Address;

    /// <summary>Parse string containing a mac address</summary>
    /// <param name="s">Mac Address string</param>
    public static MacAddress Parse(string s) {
        if (s.Is().Empty) return new MacAddress(new byte[6]);
        try {
            var value = long.Parse(s.Replace(":", "").Trim(), NumberStyles.HexNumber, CultureInfo.CurrentCulture.NumberFormat);
            var convBytes = BitConverter.GetBytes(value);
            var macBytes = new byte[6];
            Buffer.BlockCopy(convBytes, 0, macBytes, 0, 6);
            Array.Reverse(macBytes);
            return new MacAddress(macBytes);
        } catch {
            throw new FormatException();
        }
    }
    /// <summary>Parse string containing a mac address</summary>
    /// <param name="s">Mac Address string</param>
    /// <param name="m">Mac Address to set</param>
    /// <returns>true, if successful, false if string could not be parsed</returns>
    public static bool TryParse(string s, out MacAddress? m) {
        try {
            m = Parse(s);
            return true;
        } catch {
            m = null;
            return false;
        }
    }

    /// <summary>Dump MacAddress to string</summary>
    /// <param name="seperator">seperator between byte digits</param>
    public string ToString(char seperator) {
        if (Address == null) return "";

        var sb = new StringBuilder();
        sb.AppendString(Address[0].ToString("X").PadLeft(2, '0'), seperator);
        sb.AppendString(Address[1].ToString("X").PadLeft(2, '0'), seperator);
        sb.AppendString(Address[2].ToString("X").PadLeft(2, '0'), seperator);
        sb.AppendString(Address[3].ToString("X").PadLeft(2, '0'), seperator);
        sb.AppendString(Address[4].ToString("X").PadLeft(2, '0'), seperator);
        sb.AppendString(Address[5].ToString("X").PadLeft(2, '0'));
        return sb.ToString();
    }
    /// <inheritdoc />
    public override string ToString() => ToString(':');
}