﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using Z.Extensions;
using Z.Net.AsyncSocket;
using Z.Net.ByteExchange;

namespace Z.Util.Test.Items;

#pragma warning disable

public class ByteExchange : ITest
{
    #region Constants / Static Fields
    internal static X509Certificate2 ServerTestCertificate =
        new Cryptography.X509Util().Generate("test", DateTime.Now, DateTime.Now.AddYears(50), 2048, "1234");
    internal static byte[] BigBytes = Encrypter.CreateRandomBytes(10000);
    #endregion

    #region ITest Members
    public bool Execute() {
        var cfg = new SocketConfig("127.0.0.1", 4320);
        cfg.Ssl.ServerName = "test";
        cfg.Ssl.RemoteCertValidator = delegate { return true; };

        //AsyncSocketTests(cfg);
        AsyncSocketOpCodeTests(cfg);
        //AsyncSocketOpCodeTests(cfg, false); //udp
        //AsyncSocketOpCodeTests(cfg, true, true); //tcp stream
        //AsyncSocketClassTests(cfg, true, true, true); //tcp stream ssl
        //AsyncSocketClassTests(cfg, true, false, true); //tcp ssl
        return true;
    }
    #endregion

    public void AsyncSocketTests(SocketConfig cfg, bool tcp = true, bool stream = false, bool ssl = false) {
        GetSocketConfigs(cfg, out var cCfg, out var sCfg, tcp, stream, ssl);
        using var serv = new Socketer(sCfg);
        using var cli = new Socketer(cCfg);
        if (stream)
            StreamSocketTest(serv, cli);
        else
            AsyncSocketTest(serv, cli);
    }
    public void AsyncSocketOpCodeTests(SocketConfig cfg, bool tcp = true, bool stream = false, bool ssl = false) {
        GetSocketConfigs(cfg, out var cCfg, out var sCfg, tcp, stream, ssl);
        using var serv = new Socketer(sCfg).GetOpCoder<OpCodes>(OpCodes.Request, OpCodes.Response);
        using var cli = new Socketer(cCfg).GetOpCoder<OpCodes>(OpCodes.Request, OpCodes.Response);
        if (stream)
            StreamOpCodeTest(serv, cli);
        else
            AsyncOpCodeTest(serv, cli);
    }
    public void AsyncSocketClassTests(SocketConfig cfg, bool tcp = true, bool stream = false, bool ssl = false) {
        GetSocketConfigs(cfg, out var cCfg, out var sCfg, tcp, stream, ssl);
        using var serv = new Socketer(sCfg).GetClassExchanger();
        using var cli = new Socketer(cCfg).GetClassExchanger();
        ClassTest(serv, cli);
    }
    private void AsyncSocketTest(ISocketer server, ISocketer client) {
        server.OnStart += (sender, args) => {
            System.Threading.Thread.Sleep(200);
            client.Start();
        };

        const string sendText = "abcsdfgeerer";
        var finished = false;
        server.OnError += (sender, args) => {
            Error(server, args.Exception);
            finished = true;
        };
        client.OnError += (sender, args) => {
            Error(client, args.Exception);
            finished = true;
        };
        server.OnConnect += async (_, _) => {
            //server.Send(args.Remote, sendText.To().ByteArray.AsUtf8, false);
        };
        client.OnConnect += async (_, _) => {
            System.Threading.Thread.Sleep(100);
            client.Send(sendText.To().ByteArray.AsUtf8);
        };
        server.OnReceive += async (_, args) => {
            var serverRecieved = args.ReadAllBytes(false).ToArray().To().String.AsUtf8;
            if (serverRecieved != sendText) Error(server, null, "Send/Receive error)");
            server.Send((serverRecieved + "2").To().ByteArray.AsUtf8, args.Remote);
        };
        client.OnReceive += async (_, args) => {
            var clientRecieved = args.ReadAllBytes(false).ToArray().To().String.AsUtf8;
            if (clientRecieved != sendText + "2") Error(client, null, "Send/Receive error)");
            client.Disconnect(args.Remote);
            finished = true;
        };
        server.Start();
        while (!finished) System.Threading.Thread.Sleep(200);
    }

    private void StreamSocketTest(ISocketer server, ISocketer client) {
        server.OnStart += (sender, args) => {
            System.Threading.Thread.Sleep(500);
            client.Start();
        };

        var sendText = "abcsdfgeer";
        var finished = false;
        server.OnError += (sender, args) => {
            Error(server, args.Exception);
            finished = true;
        };
        client.OnError += (sender, args) => {
            Error(client, args.Exception);
            finished = true;
        };
        client.OnConnect += async (_, args) => {
            args.Stream.Writez(sendText);
            args.Stream.Flush();

            var clientRecieved = args.Stream.Readz<string>();
            if (clientRecieved != sendText + "2") Error(client, args.Exception, "Send/Receive error)");
            client.Disconnect(null);
            finished = true;
        };
        server.OnConnect += async (_, args) => {
            var serverRecieved = args.Stream.Readz<string>();
            if (serverRecieved != sendText) Error(server, args.Exception, "Send/Receive error)");
            args.Stream.Writez(serverRecieved + "2");
            args.Stream.Flush();
        };
        server.Start();
        while (!finished) System.Threading.Thread.Sleep(200);
    }

    private void StreamOpCodeTest(IOpCoder<OpCodes> server, IOpCoder<OpCodes> client) {
        var finished = false;

        server.AddRequestHandler(OpCodes.Add, async (s) => {
            var a = s.Readz<int>();
            var b = s.Readz<int>();

            s.StartResponse();

            s.Writez(6);
            s.Writez(3);
            if (!s.Send()) Error(server, null, "Send/Receive error)");

            if (a + b != 10) Error(server, null, "Send/Receive error)");
            return true;
        });

        server.Transport.OnStart += (sender, args) => {
            System.Threading.Thread.Sleep(500);
            client.Start();
        };

        server.OnError += (sender, args) => {
            Error(server, args.Exception);
            finished = true;
        };
        client.OnError += (sender, args) => {
            Error(client, args.Exception);
            finished = true;
        };
        client.Transport.OnConnect += async (sender, args) => {
            var s = client.CreateRequest(OpCodes.Add, args.Remote);
            s.Writez(8);
            s.Writez(2);

            var sResult = s.FlushAndWait();
            if (sResult != null) {
                Error(client, sResult);
                finished = true;
                return;
            }

            var a = s.Readz<int>();
            var b = s.Readz<int>();

            if (a - b != 3) Error(client, null, "Send/Receive error)");
            client.Transport.Disconnect(null);
            finished = true;
        };
        server.Start();
        while (!finished) System.Threading.Thread.Sleep(200);
    }

    private void ByteStreamMInner(IOpCoder<OpCodes> server, IOpCoder<OpCodes> client) {
        var finished = false;

        server.AddHandler(OpCodes.Add, async (s) => {
            var a = s.Readz<byte[]>();
            var b = s.Readz<byte[]>();

            s.Writez(OpCodes.Substract);
            s.Writez(b);
            s.Writez(a);
            s.Send();

            //if (a + b != 10) Console.WriteLine("StreamSocketTests Failed ({0}:Send/Receive error)".ToFormat(client.GetType().Name));
            return false;
        });

        client.AddHandler(OpCodes.Substract, async (s) => {
            var a = s.Readz<byte[]>();
            var b = s.Readz<byte[]>();

            //if (a - b != 3) Console.WriteLine("StreamSocketTests Failed ({0}:Send/Receive error)".ToFormat(client.GetType().Name));
            client.Transport.Disconnect(null);
            finished = true;
            return true;
        });

        server.Transport.OnStart += (sender, args) => {
            System.Threading.Thread.Sleep(500);
            client.Start();
        };

        server.OnError += async (sender, args) => {
            Error(server, args.Exception);
            finished = true;
        };
        client.OnError += async (sender, args) => {
            Error(client, args.Exception);
            finished = true;
        };
        client.Transport.OnConnect += async (sender, args) => {
            var s = args.Stream;
            s.Writez(OpCodes.Add);
            s.Writez(new byte[] { 0, 2, 1 });
            s.Writez(new byte[] { 0, 0, 2 });
            s.Flush();
        };
        server.Start();
        while (!finished) System.Threading.Thread.Sleep(200);
        //client.Stop();
        //finished = false;
        //client.Start();
        //while (!finished) {
        //    System.Threading.Thread.Sleep(200);
        //}
    }

    private void AsyncOpCodeTest(IOpCoder<OpCodes> server, IOpCoder<OpCodes> client) {
        var finished = false;

        server.AddRequestHandler(OpCodes.Add, async (s) => {
            var sess = s.Readz<int>();
            var a = s.Readz<int>();
            var b = s.Readz<int>();

            s.StartResponse();

            s.Writez(a + b);
            s.Send();

            if (!s.Transport.Config.IsConnectionless && sess != s.Session.Id) Error(server, null, "Session error)");
            if (a + b != 10) Error(server, null, "Send/Receive error)");
            return true;
        });
        server.AddRequestHandler(OpCodes.Transfer, async (s) => {
            var a = s.Readz<byte[]>();
            if (!s.Transport.Config.IsConnectionless) s.Session.Id = 12;

            s.StartResponse();
            s.Writez(12);
            s.Send();

            if (!a.SequenceEqual(BigBytes)) Error(server, null, "Server Transfer Receive error)");
            return true;
        });

        // client.AddHandler(OpCodes.Add, (IByteExchangeNode n, EndPoint e, Stream s) => {
        //
        // });

        // server.Transport.OnStart += (sender, args) => {
        //     System.Threading.Thread.Sleep(500);
        //     client.Start();
        // };

        server.OnError += (sender, args) => {
            Error(server, args.Exception);
            finished = true;
        };
        client.OnError += (sender, args) => {
            Error(client, args.Exception);
            finished = true;
        };
        client.Transport.OnConnect += async (_, _) => {  };
        server.Start();
        Thread.Sleep(200);
        client.Start(2000);

        var bigBytes = client.CreateRequest(OpCodes.Transfer, null, 5000);
        bigBytes.Writez(BigBytes);
        bigBytes.Writez(12);
        var bigBytesResult = bigBytes.FlushAndWait();
        if (bigBytesResult != null) {
            Error(client, bigBytesResult, "Send/Receive error");
            finished = true;
            return;
        }
        var cliSess = bigBytes.Readz<int>();

        var cAddReq = client.CreateRequest(OpCodes.Add);
        cAddReq.Writez(cliSess);
        cAddReq.Writez(8);
        cAddReq.Writez(2);
        var cAddReqResult = cAddReq.FlushAndWait();
        if (cAddReqResult != null) {
            Error(client, cAddReqResult, "Send/Receive error)");
            finished = true;
            return;
        }
        var numA = cAddReq.Readz<int>();
        if (numA != 10) Error(client, cAddReqResult, "Logic/Packet error)");

        var cAdd2Req = client.CreateRequest(OpCodes.Add);
        cAdd2Req.Writez(cliSess);
        cAdd2Req.Writez(4);
        cAdd2Req.Writez(6);
        var cAdd2ReqResult = cAdd2Req.FlushAndWait();
        if (cAdd2ReqResult != null) {
            Error(client, cAdd2ReqResult, "Send/Receive error)");
            finished = true;
            return;
        }
        var t = cAdd2Req.Position;
        numA = cAdd2Req.Readz<int>();
        if (numA != 10) Error(client, null, "Logic/Packet error)");

        client.Transport.Disconnect(null);
        finished = true;
        while (!finished) System.Threading.Thread.Sleep(200);
    }

    private void ClassTest(IClassExchanger server, IClassExchanger client) {
        var finished = false;
        client.MapAssembly<STest1>();
        server.MapAssembly<STest1>();
        server.AddRequestHandler<STest1, STest2>((IClassExchanger _, ExchangeResponse<ushort> _, STest1 req, out STest2 g) => {
            if (req.A + req.B != 10) Error(server, null, "Send/Receive error)");

            g = new STest2 {
                R = req.A + req.B
            };
            return true;
        });
        server.AddHandler(async (IClassExchanger _, EndPoint _, STest3 req) => {
            if (req.A != "testing") Error(server, null, "Send/Receive error)");
            return true;
        });

        server.Transport.OnStart += (sender, args) => {
            System.Threading.Thread.Sleep(500);
            client.Start();
        };

        server.OnError += (sender, args) => {
            Error(server, args.Exception);
            finished = true;
        };
        client.OnError += (sender, args) => {
            Error(client, args.Exception);
            finished = true;
        };
        client.Transport.OnConnect += async (sender, args) => {
            var t = new STest1 {
                A = 8,
                B = 2
            };

            if (!client.Send(t, out STest2 r, args.Remote)) {
                Error(client);
                finished = true;
                return;
            }
            if (r.R != 10) Error(client, null, "Send/Receive error)");

            var t3 = new STest3 {
                A = "testing"
            };

            if (!client.Send(t3, args.Remote)) {
                Error(client);
                finished = true;
                return;
            }

            client.Transport.Disconnect(null);
            finished = true;
        };
        server.Start();
        while (!finished) System.Threading.Thread.Sleep(200);
    }

    private static void GetSocketConfigs(SocketConfig cfg, out SocketConfig client, out SocketConfig server, bool tcp = true, bool stream = false, bool ssl = false) {
        cfg = cfg.Clone(CloneMethods.ReflectionDeep);

        if (!tcp) {
            cfg.Socket.Protocol = ProtocolType.Udp;
            cfg.Socket.SocketType = SocketType.Dgram;
        }
        if (stream) cfg.IsStream = true;
        client = cfg;
        server = cfg.Clone(CloneMethods.ReflectionDeep).Config(x => x.IsServer = true);

        if (ssl) {
            client.Ssl.Enabled = true;
            client.Ssl.RemoteCertValidator = delegate { return true; };

            server.Ssl.RemoteCertValidator = delegate { return true; };
            server.Ssl.LocalCertificate = ServerTestCertificate;
        }
    }
    private static string GetErrorText(object node, Exception exp = null, string other = null, [CallerMemberName] string caller = null) {
        ExchangeConfig cfg = null;
        if (node is IByteExchanger btn) cfg = btn.Config;
        if (node is IOpCoder<OpCodes> btn2) cfg = btn2.Transport.Config;
        //else if (node is IByteStreamNode bsn) cfg = bsn.Config;
        var sb = new StringBuilder();
        sb.Append("Error in ");
        if (caller != null) sb.Append(caller);
        if (node != null) {
            sb.AppendString("(", node.GetType().Name);
            if (cfg != null) {
                sb.AppendString(",Serv:", cfg?.IsServer, ", Udp:", cfg.IsConnectionless, " ,SSL:", cfg?.Ssl?.Enabled);
            }
            sb.Append(')');
        }
        if (exp != null) sb.AppendString(" - ", exp.Message);
        if (other != null) sb.AppendString(" - ", other);
        return sb.ToString();
    }
    private void Error(object node, Exception exp = null, string other = null, [CallerMemberName] string caller = null) {
        Console.WriteLine(GetErrorText(node, exp, other, caller));
        #if DEBUG
        if (exp != null) throw exp;
        #endif
    }

    public class STest2 : IExchangeable
    {
        public int R;
        public void ExchangeStreamFill(ExchangePacket s) { s.Writez(R); }
        public void ExchangeStreamParse(ExchangePacket s) { R = s.Readz<int>(); }
    }

    public class STest1 : IExchangeRequest<STest2>
    {
        public int A;
        public int B;
        public void ExchangeStreamFill(ExchangePacket s) { s.Writez(A); s.Writez(B); }
        public void ExchangeStreamParse(ExchangePacket s) {
            A = s.Readz<int>();
            B = s.Readz<int>();
        }
    }
    [Serializable]
    public class STest3
    {
        public string A;
    }
}