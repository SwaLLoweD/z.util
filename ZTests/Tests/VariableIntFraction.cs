﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Z.Extensions;

namespace Z.Util.Test.Items;

public class VariableIntFraction : ITest
{
    #region ITest Members
    public bool Execute() {
        var bools = new bool[] { true, false, true };
        var chars = new char[] { 'a', 'b', 'c', '1' };
        var bytes = new byte[] { 0, 1, 121, 0x3f, 0x40, 0x7f, 0x80, 0xFF };
        var ushorts = bytes.Select(x => (ushort)x).Concat(new ushort[] { 26723, 0x3fff, 0x4000, 0x7fff, 0x8000, 0xFFFF });
        var uints = ushorts.Select(x => (uint)x).Concat(new uint[] { 0x3fffff, 0x400000, 0x7fffff, 0x800000, 0x3fffffff, 0x40000000, 0x7fffffff, 0x80000000, 0xffffffff });
        var ulongs = uints.Select(x => (ulong)x).Concat(new ulong[] { 0x3fffffffffffffff, 0x4000000000000000, 0x7fffffffffffffff, 0x8000000000000000, 0xffffffffffffffff });

        var floats = new float[] { float.MinValue, -0xFFFF, -0x8000, -0x80, -0x7f, -0x40, -0x3f, -200.2233f, -1, 0, 1, 2435.9622f, 0x3f, 0x40, 0x7f, 0x80, 0x8000, 0xFFFF, float.MaxValue };
        var doubles = floats.Select(x => (double)x).Concat(new double[]
            {double.Epsilon, double.NegativeInfinity, double.MinValue, -200.22111233f, 2435.9611122f, double.PositiveInfinity, double.MaxValue});
        var decimals = new decimal[]
            {decimal.MinValue, -0xFFFF, -0x8000, -0x80, -0x7f, -0x40, -0x3f, -200.2233M, -1, 0, 1, 2435.9623322M, 0x3f, 0x40, 0x7f, 0x80, 0x8000, 0xFFFF, decimal.MaxValue};

        var sbytes = bytes.Select(x => (sbyte)(-1 * x)).Concat(bytes.Select(x => (sbyte)x));
        var shorts = ushorts.Select(x => (short)(-1 * x)).Concat(ushorts.Select(x => (short)x));
        var ints = uints.Select(x => (int)(-1 * x)).Concat(uints.Select(x => (int)x));
        var longs = ulongs.Select(x => (long)(0 - x)).Concat(ulongs.Select(x => (long)x));

        var nbools = bools.Select(x => (bool?)x).Concat(new bool?[] { null });
        var nchars = chars.Select(x => (char?)x).Concat(new char?[] { null });
        var nbytes = bytes.Select(x => (byte?)x).Concat(new byte?[] { null });
        var nushorts = ushorts.Select(x => (ushort?)x).Concat(new ushort?[] { null });
        var nuints = uints.Select(x => (uint?)x).Concat(new uint?[] { null });
        var nulongs = ulongs.Select(x => (ulong?)x).Concat(new ulong?[] { null });

        var nfloats = floats.Select(x => (float?)x).Concat(new float?[] { null });
        var ndoubles = doubles.Select(x => (double?)x).Concat(new double?[] { null });
        var ndecimals = decimals.Select(x => (decimal?)x).Concat(new decimal?[] { null });

        var nsbytes = sbytes.Select(x => (sbyte?)x).Concat(new sbyte?[] { null });
        var nshorts = ushorts.Select(x => (short?)x).Concat(new short?[] { null });
        var nints = ints.Select(x => (int?)x).Concat(new int?[] { null });
        var nlongs = longs.Select(x => (long?)x).Concat(new long?[] { null });

        var dct = new Stopwatch().Execute(() => {
            using var m = new MemoryStream();
            using var r = new BinaryReader(m);
            using var w = new BinaryWriter(m);
            //inner(w.BaseStream, bools, x => w.WriteVar((bool)x), () => r.ReadBoolean(), y=> BitConverterVar.GetBytes((bool)y), z => BitConverterVar.ToBool(z));
            //inner(w.BaseStream, chars, x => w.WriteVar((char)x), () => r.ReadChar(), y => BitConverterVar.GetBytes((char)y), z => BitConverterVar.ToChar(z));
            //inner(w.BaseStream, bytes, x => w.WriteVar((byte)x), () => r.ReadByte(), y => BitConverterVar.GetBytes((byte)y), z => BitConverterVar.ToByte(z));
            Inner(w.BaseStream, ushorts, x => w.WriteVar((ushort)x), () => r.ReadVarUInt16(), y => BitConverterVar.GetBytes((ushort)y), z => BitConverterVar.ToUInt16(z));
            Inner(w.BaseStream, uints, x => w.WriteVar((uint)x), () => r.ReadVarUInt32(), y => BitConverterVar.GetBytes((uint)y), z => BitConverterVar.ToUInt32(z));
            Inner(w.BaseStream, ulongs, x => w.WriteVar((ulong)x), () => r.ReadVarUInt64(), y => BitConverterVar.GetBytes((ulong)y), z => BitConverterVar.ToUInt64(z));

            Inner(w.BaseStream, floats, x => w.WriteVar((float)x), () => r.ReadVarSingle(), y => BitConverterVar.GetBytes((float)y), z => BitConverterVar.ToSingle(z));
            Inner(w.BaseStream, doubles, x => w.WriteVar((double)x), () => r.ReadVarDouble(), y => BitConverterVar.GetBytes((double)y), z => BitConverterVar.ToDouble(z));
            Inner(w.BaseStream, decimals, x => w.WriteVar((decimal)x), () => r.ReadVarDecimal(), y => BitConverterVar.GetBytes((decimal)y), z => BitConverterVar.ToDecimal(z));

            //inner(w.BaseStream, sbytes, x => w.Write((sbyte)x), () => r.ReadSByte(), y => BitConverterVar.GetBytes((sbyte)y), z => BitConverterVar.ToSbyte(z));
            Inner(w.BaseStream, shorts, x => w.WriteVar((short)x), () => r.ReadVarInt16(), y => BitConverterVar.GetBytes((short)y), z => BitConverterVar.ToInt16(z));
            Inner(w.BaseStream, ints, x => w.WriteVar((int)x), () => r.ReadVarInt32(), y => BitConverterVar.GetBytes((int)y), z => BitConverterVar.ToInt32(z));
            Inner(w.BaseStream, longs, x => w.WriteVar((long)x), () => r.ReadVarInt64(), y => BitConverterVar.GetBytes((long)y), z => BitConverterVar.ToInt64(z));

            Inner(w.BaseStream, nbools, x => w.WriteVar((bool?)x), () => r.ReadNVarBoolean(), y => BitConverterVar.GetBytes((bool?)y), z => BitConverterVar.ToNBool(z));
            Inner(w.BaseStream, nchars, x => w.WriteVar((char?)x), () => r.ReadNVarChar(), y => BitConverterVar.GetBytes((char?)y), z => BitConverterVar.ToNChar(z));
            Inner(w.BaseStream, nbytes, x => w.WriteVar((byte?)x), () => r.ReadNVarByte(), y => BitConverterVar.GetBytes((byte?)y), z => BitConverterVar.ToNByte(z));
            Inner(w.BaseStream, nushorts, x => w.WriteVar((ushort?)x), () => r.ReadNVarUInt16(), y => BitConverterVar.GetBytes((ushort?)y), z => BitConverterVar.ToNUInt16(z));
            Inner(w.BaseStream, nuints, x => w.WriteVar((uint?)x), () => r.ReadNVarUInt32(), y => BitConverterVar.GetBytes((uint?)y), z => BitConverterVar.ToNUInt32(z));
            Inner(w.BaseStream, nulongs, x => w.WriteVar((ulong?)x), () => r.ReadNVarUInt64(), y => BitConverterVar.GetBytes((ulong?)y), z => BitConverterVar.ToNUInt64(z));

            Inner(w.BaseStream, nfloats, x => w.WriteVar((float?)x), () => r.ReadNVarSingle(), y => BitConverterVar.GetBytes((float?)y), z => BitConverterVar.ToNSingle(z));
            Inner(w.BaseStream, ndoubles, x => w.WriteVar((double?)x), () => r.ReadNVarDouble(), y => BitConverterVar.GetBytes((double?)y), z => BitConverterVar.ToNDouble(z));
            Inner(w.BaseStream, ndecimals, x => w.WriteVar((decimal?)x), () => r.ReadNVarDecimal(), y => BitConverterVar.GetBytes((decimal?)y), z => BitConverterVar.ToNDecimal(z));

            Inner(w.BaseStream, nsbytes, x => w.WriteVar((sbyte?)x), () => r.ReadNVarSByte(), y => BitConverterVar.GetBytes((sbyte?)y), z => BitConverterVar.ToNSbyte(z));
            Inner(w.BaseStream, nshorts, x => w.WriteVar((short?)x), () => r.ReadNVarInt16(), y => BitConverterVar.GetBytes((short?)y), z => BitConverterVar.ToNInt16(z));
            Inner(w.BaseStream, nints, x => w.WriteVar((int?)x), () => r.ReadNVarInt32(), y => BitConverterVar.GetBytes((int?)y), z => BitConverterVar.ToNInt32(z));
            Inner(w.BaseStream, nlongs, x => w.WriteVar((long?)x), () => r.ReadNVarInt64(), y => BitConverterVar.GetBytes((long?)y), z => BitConverterVar.ToNInt64(z));
        });

        Console.Write($"(Time: {dct})");
        return true;
    }
    #endregion

    private static void Inner(Stream s, IEnumerable e, Action<object> write, Func<object> read, Func<object, byte[]> gb, Func<byte[], object> go) {
        s.Position = 0;
        foreach (var i in e) write(i);
        s.Position = 0;
        foreach (var i in e) {
            var t = read();
            if (!Equals(t, i)) WriteError("ReaderWriter:" + (i != null ? i.GetType().Name : e.GetType().Name));
            var bytes = gb(i);
            var resultObj = go(bytes);
            if (!Equals(resultObj, i)) WriteError("Converter:" + (i != null ? i.GetType().Name : e.GetType().Name));
        }
    }
    public static void WriteError(string error) => Console.Write($"VariableInt Error: {error}");
}