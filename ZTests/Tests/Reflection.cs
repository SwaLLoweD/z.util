﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

namespace Z.Util.Test.Items;

public class ExtReflection : ITest
{
    #region ITest Members
    //var pc = new PocoBasic(Guid.NewGuid(), 0, "test", "testdesc", DateTime.UtcNow);
    //pc.Children.Add(new PocoBasic(Guid.NewGuid(), 1, "child1", "child1Desc", DateTime.UtcNow));
    //var pc1 = new PocoBasic();
    //var pc2 = new PocoBasic2();
    //var pc3 = new PocoBasic2();
    //var pop1 = pc.Populate(pc1);
    //var pop2 = pc.Populate(pc2);
    //if (pop1.Id != pc.Id || pop1.Description != pc.Description)
    //    Console.WriteLine("Reflection tests error..(Populate same type)");
    //if (pop2.Id != pc.Id || pop2.Name != pc.Name)
    //    Console.WriteLine("Reflection tests error..(Populate similar type)");
    //Console.WriteLine("Reflection tests ended..");
    public bool Execute() => true;
    #endregion
}