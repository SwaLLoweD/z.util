﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using Z.Extensions;

namespace Z.Util.Test.Items;

public class ExtEnumerable : ITest
{
    #region ITest Members
    public bool Execute() {
        var arr2 = new int[] { 2, 6 };
        var arr = new int[] { 5, 4, 1, 2, 3, 6 };
        var en = arr.To().Enumerable();
        var lst = en.To().List();

        var rndlst = lst.To().Array();
        rndlst.Transform().RandomShuffle();

        var ordBag = rndlst.To().OrderedBag();

        var ordLst = ordBag.To().List();

        ordLst.Add(10);
        ordLst.Add(10);

        var set = ordLst.To().HashSet();

        var ordLst2 = set.To().List();

        var dictLst2 = set.To().KeyValue(x => x).SortedDictionary();

        var sub = ordLst2.SubList(2, 2);
        var diff = arr.To().Bag().Except(arr2);

        var hList = ordLst2.To().HashList();
        hList.Remove(3);
        hList.Add(8);

        var hLList = ordLst2.To().HashLinkedList();
        hLList.Remove(3);
        hLList.Add(8);

        var pQ = ordLst2.To().PriorityQueue();
        var pQMin = pQ.FindMin();
        var pQMax = pQ.FindMax();
        pQ.Add(12);
        pQMax = pQ.FindMax();
        pQ.DeleteMax();
        pQ.Remove(4);

        var cst = rndlst.To().KeyValue(x => x).SortedDictionary();
        cst.Remove(5);
        cst[20] = 1;
        cst[20] = 2;

        var fail = false;
        if (set.Count != 7) fail = true;
        if (ordLst2.IndexOf(new int[] { 6, 10 }).Count != 1) fail = true;
        if (!sub.To().Bag().SetEquals(new int[] { 4, 3 })) fail = true;
        if (diff.Contains(2)) fail = true;
        if (!hList.Contains(8) || hList.Contains(3)) fail = true;
        if (!hLList.Contains(8) || hLList.Contains(3)) fail = true;
        if (pQMax != 12 || pQMin != 1) fail = true;

        return !fail;

        //concurrencyTest();
    }
    #endregion
}