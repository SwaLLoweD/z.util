﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using Z.Extensions;

namespace Z.Util.Test.Items;

public class ExtStream : ITest
{
    #region ITest Members
    public bool Execute() {
        const int a = 0;
        const decimal b = (decimal)0.23f;
        var c = DateTime.UtcNow;
        const string d = "abcdef";
        const OpCodes e = OpCodes.Add;
        var f = System.Net.IPAddress.Parse("192.168.0.1");
        var g = new byte[4] { 1, 2, 3, 4 };
        var h = Guid.NewGuid();
        byte[] i = null;

        using (var m = new MemoryStream()) {
            m.Write(g, 0, 4);
            m.Writez(g);

            m.Writez(a);
            m.Writez(b);
            m.Writez(c);
            m.Writez(d);
            m.Writez(e);
            m.Writez(f);
            m.Writez(h);
            m.Writez(i);

            m.Flush();

            m.Position = 0;
            var og = new byte[4];
            m.Read(og, 0, 4);

            var og1 = m.Readz<byte[]>();

            var oa = m.Readz<int>();
            var ob = m.Readz<decimal>();
            var oc = m.Readz<DateTime>();
            var od = m.Readz<string>();
            var oe = m.Readz<OpCodes>();
            var of = m.Readz<System.Net.IPAddress>();
            var oh = m.Readz<Guid>();
            var oi = m.Readz<byte[]>();

            var rbytes = m.ReadAllBytes();

            if (a != oa || b != ob || c != oc || d != od || e != oe || !f.Equals(of) || h != oh || i != oi) return false;
        }
        return true;
    }
    #endregion
}