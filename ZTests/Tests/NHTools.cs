﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Linq;
using Z.Data.NHTools;
using Z.Extensions;
using Z.Util.Test.Entities;

namespace Z.Util.Test;

public partial class Tests
{
    public static void NHToolsTests() {
        var cfg = new NHConfig();

        cfg.LoadMaps<Tests>();
        cfg.FactoriesStartup();
        using (var repo = Repo.Ts) {
            var lst1 = repo.Get<PocoSql>().ByProperty(x => x.Id, 1);
            var lst2 = repo.Get<PocoSql>().ByProperty(x => x.Id, 1, 2);
            var lstAll = repo.Get<PocoSql>().All();

            var lstPaged = repo.Get<PocoSql>().Paged(2, 2);

            //var newEnt = new PocoSql { Name = "test5", Description = "testDesc1", DateCreated = DateTime.Parse("2012-02-01") };
            //repo.Save(newEnt);
            var dt = DateTime.Parse("1970-01-01");
            var lst1Linq = repo.LinQ<PocoSql>().Where(x => x.Id == 1 && x.Id < 9 && x.DateCreated >= dt).FirstOrDefault();

            var ent3 = repo.Get<PocoSql>().ById(3);
            ent3.Flags = new Bitmask(2, 8);
            ent3.Description = "dd{0}".ToFormat(new Random().Next(120));
            repo.Update(ent3);
            //var ent3 = repo.Get<BasicPoco>().ById(3);
            //repo.Delete(ent3);

            var lstFlag = repo.LinQ<PocoSql>().Where(x => x.Flags.Has(2)).ToList();
        }
        cfg.FactoriesShutDown();

        Console.WriteLine("NHTools tests ended..");
    }
}