﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Z.Extensions;
using Z.Util.Test.Entities;

namespace Z.Util.Test.Items;
#pragma warning disable CA1822, IDE0051

public class Serialization: ITest {
    #region ITest Members
    public bool Execute() {
        var objList = new List<PocoBasic> {
                new PocoBasic(Guid.NewGuid(), 0, "test", "testdesc", DateTime.UtcNow),
                new PocoBasic(Guid.NewGuid(), 1, "test1", "desc1", DateTime.UtcNow),
                new PocoBasic(Guid.NewGuid(), 2, "test2", "testdesc2", DateTime.UtcNow)
            };

        return
        SerializeTest(objList) &&
        //SerializeBench() &&
        SerializationTestInheritence();
    }
    #endregion

    private static bool SerializeTest<T>(T objToSerialize) {
        bool rval = true;
        // if (!SerializeDeserializeTestInner(objToSerialize, (o) => o.Serialize().DotNetBinary(), (r) => r.Deserialize().DotNetBinary<T>())) {
        //     Console.WriteLine("DotNetBinary Serialization failed");
        //     rval = false;
        // }

        if (!SerializeDeserializeTestInner(objToSerialize, (o) => o.Serialize().DotNetXml(), (r) => r.Deserialize().DotNetXml<T>())) {
            Console.WriteLine("DotNetXml Serialization failed");
            rval = false;
        }

        if (!SerializeDeserializeTestInner(objToSerialize, (o) => o.Serialize().DotNetJson(), (r) => r.Deserialize().DotNetJson<T>())) {
            Console.WriteLine("DotNetJson Serialization failed");
            rval = false;
        }

        if (!SerializeDeserializeTestInner(objToSerialize, (o) => o.Serialize().BoisBinary(ZTypeCacheFlags.Default | ZTypeCacheFlags.NonPublic | ZTypeCacheFlags.Fields), (r) => r.Deserialize().BoisBinary<T>(ZTypeCacheFlags.Default | ZTypeCacheFlags.NonPublic | ZTypeCacheFlags.Fields))) {
            Console.WriteLine("BoisBinary Serialization failed");
            rval = false;
        }

        if (!SerializeDeserializeTestInner(objToSerialize, (o) => o.Serialize().ZXml(), (r) => r.Deserialize().ZXml<T>())) {
            Console.WriteLine("ZXml Serialization failed");
            rval = false;
        }
        return rval;
    }
    private static bool SerializeDeserializeTestInner<T, U>(T obj, Func<T, U> serialize, Func<U, T> deserialize, Func<T, U, bool> additionalTest = null) {
        var r = serialize(obj);
        var d = deserialize(r);
        if (d == null && obj == null) return true;
        else if (d == null || obj == null) return false;

        if (obj is System.Collections.IList cObj) {
            if (d is not System.Collections.IList cD) return false;
            for (int i = 0; i < cObj.Count; i++)
                if (!cObj[i].Equals(cD[i])) return false;
            return true;
        }
        if (!obj.Equals(d)) return false;
        if (additionalTest != null && !additionalTest(obj, r)) return false;
        return true;
    }

    private static bool SerializationTestInheritence() {
        var objList = new List<PocoBasic4> {
                new PocoBasic4(Guid.NewGuid(), 0, "test", "testdesc", DateTime.UtcNow, "testdesc"),
                new PocoBasic4(Guid.NewGuid(), 1, "test1", "desc1", DateTime.UtcNow, "desc1"),
                new PocoBasic4(Guid.NewGuid(), 2, "test2", "testdesc2", DateTime.UtcNow, "testdesc2")
            }.ToArray();

        // using (var m = new MemoryStream()) {
        //     m.Write<PocoBasic[]>(objList);
        //     m.Position = 0;
        //     var r2objList = m.Read<PocoBasic[]>();
        // }
        var sbytes = objList.Serialize<PocoBasic[]>().BoisBinary();
        var robjlist = sbytes.Deserialize().BoisBinary<PocoBasic[]>();

        for (var i = 0; i < objList.Length; i++) {
            var o = objList[i];
            var r = robjlist[i];
            if (o.Id != r.Id || o.Index != r.Index || o.Name != r.Name || o.Description != r.Description || o.DateCreated != r.DateCreated) {
                Console.WriteLine("Serialization inheritence tests failed..");
                return false;
            }
        }
        return true;
    }

    private static bool SerializeBench() {
        var objList = new List<PocoBasic>();
        System.Threading.Thread.Sleep(300);
        var now = DateTime.UtcNow;

        for (var i = 0; i <= 1000; i++) objList.Add(new PocoBasic(Guid.NewGuid(), i, $"test{i}", $"testdesc{i}", now));

        var len = 0L;
        var dct = Program.Bench(() => {
            var dc = objList.Serialize().BoisBinary(ZTypeCacheFlags.Default | ZTypeCacheFlags.NonPublic);
            len += dc.LongLength;
        }, 1);

        Console.Write($"(time:{dct}, len:{len})");

        //var bs = new Salar.Bois.BoisSerializer();
        //using (var m = new MemoryStream()) {
        //    sw.Start();
        //    bs.Serialize(objList, m);
        //    len = (int)m.Position;
        //    m.Position = 0;
        //    bs.Deserialize<List<PocoBasic>>(m);
        //    sw.Stop();
        //    dct = sw.ElapsedMilliseconds;
        //    sw.Reset();
        //}
        //Console.WriteLine($"Salar Serialization tests ended.. (time:{dct}, len:{len})");
        return true;
    }
}