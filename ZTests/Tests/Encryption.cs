﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Z.Cryptography;
using Z.Extensions;

namespace Z.Util.Test.Items;

public class Encryption : ITest
{
    #region ITest Members
    public bool Execute() {
        SymTests();
        AesGcmTest();
        X509Test();
        RsaTest();
        DiffieHellmanTest();
        //diffieHellmanCustomTest();
        // Console.WriteLine(Program.Bench(() => diffieHellmanTest(), 100));
        // Console.WriteLine(Program.Bench(() => diffieHellmanCustomTest(), 100));
        return true;
    }
    #endregion

    private static bool SymTests() {
        var originalBytes = Encrypter.CreateRandomBytes(10);
        //var originalString = "This is a test string";

        var password = "something".To().ByteArray.AsUtf8;

        var encryptors = new IEncryptor[] {
                Encrypter.Default.Aes(password,  null, CipherPrepend.IV),
                Encrypter.Default.RC2(password, null, CipherPrepend.IV),
                //Encrypter.Default.TDES(password),  //padding invalid?
                Encrypter.Default.AesGcmz(password),
                new Rsaz(RSA.Create(2048), () => Encrypter.Default.CreateCryptoEngine<Aes>())
            };
        var functions = new List<Func<IEncryptor, byte[], bool>> {
            SymByte2Byte,
            SymByte2ByteAsync,
            SymStream2Stream,
            SymStream2StreamWLen
        };

        foreach (var encryptor in encryptors) {
            foreach (var func in functions)
            if (!func(encryptor, originalBytes)) {
                Console.WriteLine($"{encryptor.GetType().GenericTypeArguments.First().Name} - {func.Method.Name} encryption test failed!");
                return false;
            }
        }
        var longBytes = new byte[10240];

        foreach (var encryptor in encryptors) {
            foreach (var func in functions)
            if (!func(encryptor, longBytes)) {
                Console.WriteLine($"{encryptor.GetType().GenericTypeArguments.First().Name} - {func.Method.Name} encryption long byte test failed!");
                return false;
            }
        }

        return true;
    }

    private static bool SymByte2Byte(IEncryptor encryptor, byte[] originalBytes) {
            var encBytes = encryptor.Encrypt(originalBytes);
            var decBytes = encryptor.Decrypt(encBytes);
            return originalBytes.To().String.AsBase64 == decBytes.To().String.AsBase64;
    }
    private static bool SymByte2ByteAsync(IEncryptor encryptor, byte[] originalBytes) {
        var encBytes = encryptor.EncryptAsync(originalBytes).Result;
        var decBytes = encryptor.DecryptAsync(encBytes).Result;
        return originalBytes.To().String.AsBase64 == decBytes.To().String.AsBase64;
    }
    private static bool SymStream2StreamWLen(IEncryptor encryptor, byte[] originalBytes) {
        if (encryptor is AesGcmz) return true; //AesGcm does not support stream encryption

        using var s = new MemoryStream();
        s.Write(originalBytes);
        s.Position = 0;
        using var d = ZUtilCfg.GlobalMemoryStreamManager.GetStream();
        var encLen = encryptor.Encrypt(s, originalBytes.Length, d);
        d.Position = 0;
        s.Position = 0;
        var decLen = encryptor.Decrypt(d, encLen, s);
        var decBytes = s.ToArray();
        return originalBytes.To().String.AsBase64 == decBytes.To().String.AsBase64;
    }
    private static bool SymStream2Stream(IEncryptor encryptor, byte[] originalBytes) {
        if (encryptor is AesGcmz) return true; //AesGcm does not support stream encryption
        using var s = new MemoryStream();
        s.Write(originalBytes);
        s.Position = 0;
        using var d = ZUtilCfg.GlobalMemoryStreamManager.GetStream();
        var encLen = encryptor.Encrypt(s, -1, d);
        d.Flush();
        d.Position = 0;
        s.Position = 0;
        var decLen = encryptor.Decrypt(d, -1, s);
        d.Flush();
        var decBytes = s.ToArray();
        return originalBytes.To().String.AsBase64 == decBytes.To().String.AsBase64;
    }

    private static bool X509Test() {
        const string originalString = "This is a test string";

        var x509gen = new X509Util();
        var cerCA = x509gen.Generate(new X509GenerationRequest("testCA", null, "1234"));
        var signx509 = cerCA.Sign(originalString);
        //var signx509 = Z.Cryptography.X509UtilM.Sign(originalString, cerCA);
        if (!cerCA.SignVerify(originalString, signx509)) Console.WriteLine("X509 signature verification failed!");

        //var x509gen2 = new Z.Cryptography.X509UtilB();
        var cer2 = x509gen.Generate(new X509GenerationRequest("test", cerCA, "1234"));
        //var signx5092 = Z.Cryptography.X509UtilM.Sign(originalString, cer2);
        var signx5092 = cer2.Sign(originalString);
        if (!cer2.SignVerify(originalString, signx5092)) Console.WriteLine("X509 signature verification failed!");

        //var x509gen2 = new Z.Cryptography.X509UtilB();
        var cer3 = x509gen.Generate(new X509GenerationRequest("tes", cer2, "1234"));
        //var signx5092 = Z.Cryptography.X509UtilM.Sign(originalString, cer2);
        var signx5093 = cer3.Sign(originalString);
        if (!cer3.SignVerify(originalString, signx5093)) Console.WriteLine("X509 signature verification failed!");

        if (!cerCA.VerifyChild(cer2)) Console.WriteLine("X509 CA chain verification failed!");
        if (!cer2.VerifyChild(cer3)) Console.WriteLine("X509 CA chain verification failed!");
        return true;
    }
    private static bool RsaTest() {
        var testLen = new int[] { 54, 125, 1024, 8192 };
        if (testLen.Any(len => !RsaTestInner(len))) { return false; }
        { }
        return true;
    }
    private static bool RsaTestInner(int byteLen) {
        var rsaBytes = Encrypter.CreateRandomBytes(byteLen);
        using (var rsa = RSA.Create(1024)) {
            var rsaOutput = rsa.Crypto<Aes>().Encrypt(rsaBytes);
            var rsaDecrypt = rsa.Crypto<Aes>().Decrypt(rsaOutput);
            var rsaOutput2 = rsa.CryptoAesGcm().Encrypt(rsaBytes);
            var rsaDecrypt2 = rsa.CryptoAesGcm().Decrypt(rsaOutput2);
            if (rsaDecrypt.To().String.AsBase64 != rsaBytes.To().String.AsBase64) Console.WriteLine("RSA Auto Encryption using aes has failed!");
            if (rsaDecrypt2.To().String.AsBase64 != rsaBytes.To().String.AsBase64) Console.WriteLine("RSA Auto Encryption using aesgcmz has failed!");
        }
        return true;
    }
    private static bool DiffieHellmanTest() {
        var curve = ECCurve.NamedCurves.nistP256;
        var dhA = ECDiffieHellman.Create(curve); //1024-bit
        var dhB = ECDiffieHellman.Create(curve);
        //byte[] primeBytes, gBytes;
        var reqA = dhA.ExportSubjectPublicKeyInfo();
        var keyB = dhB.DeriveKeyFromHash(dhB.CreateOtherPartyPublicKey(reqA), HashAlgorithmName.SHA256);
        var reqB = dhB.ExportSubjectPublicKeyInfo();
        var keyA = dhA.DeriveKeyFromHash(dhA.CreateOtherPartyPublicKey(reqB), HashAlgorithmName.SHA256);

        if (keyA.To().String.AsBase64 != keyB.To().String.AsBase64) Console.WriteLine("DiffieHellman Key exchage failed!");
        return true;
    }
    // private bool diffieHellmanCustomTest() {
    //     var ecdhA = new ECDHManaged();
    //     var ecdhB = new ECDHManaged();
    //     var reqdhA = ecdhA.CreateKeyExchange();
    //     var respdhB = ecdhB.DecryptKeyExchange(reqdhA);
    //     var respdhA = ecdhA.DecryptKeyExchange(ecdhB.CreateKeyExchange());

    //     if (respdhA.To().String.AsBase64 != respdhB.To().String.AsBase64) Console.WriteLine("ECDiffieHellman Key exchage failed!");
    //     return true;
    // }

    private static bool AesGcmTest() {
        const string originalString = "This is a test string";
        var originalBytes = originalString.To().ByteArray.AsUtf8;
        var A = Encrypter.Default.AesGcmz(new byte[32]);
        var encA = A.Encrypt(originalBytes);
        var decA = A.Decrypt(encA);
        var resultString = decA.To().String.AsUtf8;

        if (originalString != resultString) Console.WriteLine("Aes-Gcm encryption failed!");
        return true;
    }
}