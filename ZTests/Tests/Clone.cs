﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using Z.Extensions;
using Z.Util.Test.Entities;

namespace Z.Util.Test.Items;

public class Clone : ITest
{
    #region ITest Members
    public bool Execute() {
        var a = new PocoBasic(Guid.NewGuid(), 0, "test", "testdesc", DateTime.UtcNow);
        var objList = new List<PocoBasic> {
                a,
                new PocoBasic(Guid.NewGuid(), 1, "test1", "desc1", DateTime.UtcNow),
                new PocoBasic(Guid.NewGuid(), 2, "test2", "testdesc2", DateTime.UtcNow)
            };
        var shallowA = objList[0].Clone(CloneMethods.ReflectionShallow);
        var shallowList = objList.Clone(CloneMethods.ReflectionShallow);
        var deepList = objList.Clone(CloneMethods.ReflectionDeep);
        var copyA = a.Populate<PocoBasic, PocoBasic2>();
        var copyAA = a.Populate(new PocoBasic2(Guid.NewGuid(), "r", DateTime.UtcNow));

        if (shallowA.Id != a.Id || shallowA.Name != a.Name || shallowA.Description != a.Description || shallowA.DateCreated != a.DateCreated || shallowA == a)
            Console.WriteLine("Clone tests Failed: Shallow Clone Poco");
        if (shallowList == objList || shallowList[0] != objList[0]) Console.WriteLine("Clone tests Failed: Shallow Clone List");
        if (deepList == objList || deepList[0] == objList[0] || deepList[0].Id != objList[0].Id) Console.WriteLine("Clone tests Failed: Deep Clone List");
        if (copyA.Id != a.Id || copyA.Name != a.Name) Console.WriteLine("Clone tests Failed: Shallow Clone Into new Poco");
        if (copyAA.Id != a.Id || copyAA.Name != a.Name) Console.WriteLine("Clone tests Failed: Shallow Clone Into different type Poco");

        //System.Threading.Thread.Sleep(300);
        //var now = DateTime.UtcNow;

        //for (int i = 0; i <= 100000; i++) {
        //    objList.Add(new PocoBasic(Guid.NewGuid(), i, $"test{i}", $"testdesc{i}", now));
        //}

        //var sw = new Stopwatch();
        //sw.Start();
        ////var dc = DeepCopyByExpressionTrees.DeepClone(objList);
        //foreach (var i in objList) {
        //    var dc = DeepCopyByExpressionTrees.CloneTo<PocoBasic, PocoBasic2>(i);
        //}
        //sw.Stop();
        //var dct = sw.ElapsedMilliseconds;
        //sw.Reset();
        //Console.WriteLine($"{dct}");
        return true;
    }
    #endregion

    public static void CurrentTest() {
        ////ZTypeCache.GetTypeInfo<PocoBasic>(ZTypeCacheOptions.CacheSmart, ZTypeCacheFlags.Default | ZTypeCacheFlags.NonPublic);
        //var objList = new List<PocoBasic>();

        //System.Threading.Thread.Sleep(300);
        //var now = DateTime.UtcNow;

        //for (int i = 0; i <= 1000; i++) {
        //    objList.Add(new PocoBasic(Guid.NewGuid(), i, $"test{i}", $"testdesc{i}", now));
        //}

        //var sw = new Stopwatch();

        //sw.Start();

        //sw.Stop();
        //var dct = sw.ElapsedMilliseconds;
        //sw.Reset();
        ////Console.WriteLine($"{dct}");

        //Console.WriteLine($"Z Serialization tests ended.. (time:{dct})");

        //var sw = new Stopwatch();
        //sw.Start();
        ////var dc = DeepCopyByExpressionTrees.DeepClone(objList);
        //foreach (var i in objList) {
        //    var dc = DeepCopyByExpressionTrees.CloneTo<PocoBasic, PocoBasic2>(i);
        //}
        //sw.Stop();
        //var dct = sw.ElapsedMilliseconds;
        //sw.Reset();
        ////Console.WriteLine($"{dct}");
        //Console.WriteLine($"Clone tests ended.. (Time: {dct})");
    }
}