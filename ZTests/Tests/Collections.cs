﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using Z.Collections.Concurrent;
using Z.Collections.Generic;
using Z.Extensions;

namespace Z.Util.Test.Items;
#pragma warning disable CA1822, IDE0051

public partial class Collection : ITest
{
    #region ITest Members
    public bool Execute() {
        var arr2 = new int[] { 2, 6, 7, 9 };

        var que = new SyncCircularQueue<int>(arr2);

        que.Enqueue(12);

        var a6 = que.Dequeue();

        var a7 = que.Dequeue();

        var arr = new int[] { 5, 4 };

        que.Enqueue(arr);

        var t = que.Dequeue(3);

        var scr = new Scrambler2();
        var s = scr.Scramble(arr2);
        var s2 = scr.Unscramble(s);

        var fail = false;
        if (a6 != 6) fail = true;
        if (a7 != 7) fail = true;
        if (!t.To().Bag().SetEquals(new int[] { 9, 12, 5 })) fail = true;

        if (fail) Console.WriteLine("CollectionTests Failed");
        if (!s2.Is().EqualTo(arr2)) Console.WriteLine("Collection Scramble tests failed");

        CacheTableTest();
        //concurrencyTest();

        return true;
    }
    #endregion

    private void ConcurrencyTest() {
        var tbl = new SyncSortedDictionary<int, int>();

        for (var i = 1; i < 5; i++) tbl[i] = i + 1;
        var cnt = 0;
        //var re = new System.Threading.ManualResetEvent(false);
        System.Threading.ThreadPool.QueueUserWorkItem(delegate {
            System.Threading.Thread.Sleep(1);
            for (var j = 1; j < 100000; j++) {
                System.Threading.ThreadPool.QueueUserWorkItem(delegate {
                    for (var i = 1; i < 4; i++) {
                        if (!tbl.TryUpdate(i, (_, oldValue) => oldValue + 1)) cnt++;
                    }
                    //var v = sd[i];
                    //sd[i] = sd[i] + 1;
                    //v = sd[i];
                });
            }
            //re.Set();
        });

        //re.WaitOne();

        while (tbl[3] < 100000) Console.WriteLine(tbl.To().ArrayString());
        //Console.WriteLine("fails: {0}".ToFormat(cnt));
        System.Threading.Thread.Sleep(100);
        Console.WriteLine(tbl.To().ArrayString());
    }

    private static void CacheTableTest() {
        var c = new CacheTable<int, int>(50, 5d) {
            TableTtlMs = 100
        };
        c.Add(1, 1);
        System.Threading.Thread.Sleep(20);
        c.Add(2, 2);
        if (c[1] != 1 && c.Count != 2) Console.WriteLine("CollectionTests - CacheTable Tests Failed 1");
        System.Threading.Thread.Sleep(20);
        c[2] = 2;
        System.Threading.Thread.Sleep(30);
        c[2] = 2;
        System.Threading.Thread.Sleep(30);
        if (c.ContainsKey(1)) Console.WriteLine("CollectionTests - CacheTable Tests Failed 2");
        //var t = c[2];
        System.Threading.Thread.Sleep(13);
        if (c.ContainsKey(1) || c[2] != 2) Console.WriteLine("CollectionTests - CacheTable Tests Failed 3");
        System.Threading.Thread.Sleep(150);
        if (c.Count != 0) Console.WriteLine("CollectionTests - CacheTable Tests Failed 4");
    }
}