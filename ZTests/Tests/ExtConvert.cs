﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using Z.Extensions;

namespace Z.Util.Test.Items;

public class ExtConverter : ITest
{
    #region ITest Members
    public bool Execute() {
        const string dateStr = "2001-02-03 11:20";
        var dateResult = dateStr.To().Type<DateTime>();

        const string ipStr = "192.168.0.1";
        var ipResult = ipStr.To().Type<System.Net.IPAddress>();

        const string nIntStr = "12";
        var nInt = nIntStr.To().Type<int?>();

        return dateResult == DateTime.Parse("2001-02-03 11:20")
            && ipResult.Equals(System.Net.IPAddress.Parse("192.168.0.1"))
            && nInt == 12;
    }
    #endregion
}