﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Diagnostics;
using Z.Cryptography;
using Z.Extensions;

namespace Z.Util.Test;

public static class Program
{
    #region Constants / Static Fields
    public static SLog Log { get; } = new();
    #endregion

    private static void Main(string[] args) {
        if (args != null) Console.WriteLine("args");
        //console.writeline("press any key to stop");
        //do
        //{
        //    while (!console.keyavailable)
        //    {
        //        long res = 0;
        //        parallel.for((long)0, long.maxvalue, i =>
        //        {
        //            res |= i;
        //        });
        //    }
        //} while (console.readkey(true).key != consolekey.escape);

        Log.OnLogAdd += Log_OnLogAdd;

        ExecuteTest<Items.VariableIntFraction>();
        ExecuteTest<Items.Clone>();
        ExecuteTest<Items.Serialization>();
        ExecuteTest<Items.ExtReflection>();
        ExecuteTest<Items.ExtConverter>();
        ExecuteTest<Items.ExtStream>();
        ExecuteTest<Items.ExtEnumerable>();
        ExecuteTest<Items.Collection>();
        ExecuteTest<Items.Encryption>();
        ExecuteTest<Items.ByteExchange>();
        ExecuteTest<Items.Zip>();

        //Tests.PSqlTests();
        //Tests.NHToolsTests();

        // System.IO.File.WriteAllText("/data/tf.txt", "This is a test text");
        // Console.WriteLine(System.IO.File.ReadAllText("/data/tf.txt"));

        Console.WriteLine("ALL tests have been concluded...");
        //Console.ReadKey(true);
    }
    private static bool ExecuteTest<T>() where T : ITest, new() {
        Console.Write($"{typeof(T).Name} testing.. ");
        var test = new T();
        var success = test.Execute();
        Console.WriteLine($"{success}");
        return success;
    }

    public static long Bench(Action func, int loopCount = 1) {
        var sw = new Stopwatch();

        sw.Start();
        for (int i = 0; i <= loopCount; i++) {
            func();
        }
        sw.Stop();
        var elapsedTime = sw.ElapsedMilliseconds;
        sw.Reset();
        return elapsedTime;
    }
    private static bool Log_OnLogAdd(SLog arg1, SLogEventArgs arg2) {
        var err = arg2?.Entry?.ToString();
        if (err != null) Console.WriteLine(err);
        return false;
    }
}