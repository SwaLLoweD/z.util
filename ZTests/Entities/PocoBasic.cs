﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;

namespace Z.Util.Test.Entities;

[Serializable]
public class PocoBasic: IEquatable<PocoBasic>
{
    #region Fields / Properties
    //public List<PocoBasic> Children; //{ get; set; }
    public List<PocoBasic> Children { get; set; }
    public DateTime DateCreated { get; set; }
    public string Description { get; set; }
    public Guid Id { get; set; }
    //public string Name; //{ get; set; }
    public int? Index { get; set; }
    public string Name { get; set; }
    #endregion

    #region Constructors
    public PocoBasic() => Children = new List<PocoBasic>();
    public PocoBasic(Guid id, int index, string name, string desc, DateTime dateCreated)
        : this() {
        Id = id;
        Index = index;
        Name = name;
        Description = desc;
        DateCreated = dateCreated;
    }

    public bool Equals(PocoBasic other) {
        if (ReferenceEquals(this, other)) return true;
        if (Children != null && other.Children != null && Children.Count == other.Children.Count) {
            for (int i = 0; i < Children.Count; i++)
                if (!Children[i].Equals(other.Children[i])) return false;
        } else { return false; }
        if (DateCreated.Ticks != other.DateCreated.Ticks) return false;
        if (Description != other.Description) return false;
        if (Id.ToString() != other.Id.ToString()) return false;
        if (Index != other.Index) return false;
        if (Name != other.Name) return false;
        return true;
    }

    public override bool Equals(object obj) => Equals(obj as PocoBasic);

    public override int GetHashCode() => HashCode.Combine(Children, DateCreated, Description, Id, Index, Name);
    #endregion
}

[Serializable]
public class PocoBasic2 : IEquatable<PocoBasic2>
{
    #region Fields / Properties
    public DateTime DateCreated;
    public Guid Id { get; set; }
    public string Name { get; set; }
    #endregion

    #region Constructors
    public PocoBasic2() { }
    public PocoBasic2(Guid id, string name, DateTime dateCreated) {
        Id = id;
        Name = name;
        DateCreated = dateCreated;
    }
    public override bool Equals(object obj) => Equals(obj as PocoBasic2);

    public override int GetHashCode() => HashCode.Combine(DateCreated, Id, Name);

    public bool Equals(PocoBasic2 other) {
        if (ReferenceEquals(this, other)) return true;
        if (DateCreated.Ticks != other.DateCreated.Ticks) return false;
        if (Id.ToString() != other.Id.ToString()) return false;
        if (Name != other.Name) return false;
        return true;
    }
    #endregion
}

[Serializable]
public class PocoBasic3 : PocoBasic
{
    #region Fields / Properties
    private readonly Guid privId;
    #endregion

    #region Constructors
    public PocoBasic3() => Children = new List<PocoBasic>();
    public PocoBasic3(Guid id, int index, string name, string desc, DateTime dateCreated) : base(id, index, name, desc, dateCreated) => privId = Guid.NewGuid();
    #endregion

    public bool Equals(PocoBasic3 other) {
        if (privId != other.privId) return false;
        return base.Equals(other);
    }
}

[Serializable]
public class PocoBasic4 : PocoBasic
{
    #region Fields / Properties
    public string Data { get; set; }
    #endregion

    #region Constructors
    public PocoBasic4() => Children = new List<PocoBasic>();
    public PocoBasic4(Guid id, int index, string name, string desc, DateTime dateCreated, string data) : base(id, index, name, desc, dateCreated) => Data = data;
    #endregion
    public bool Equals(PocoBasic4 other) {
        if (Data != other.Data) return false;
        return base.Equals(other);
    }
}