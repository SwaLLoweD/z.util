﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Linq;

namespace Z.Util.Test.Entities;

[Serializable]
public class PocoSql
{
    public virtual int Id { get; set; }
    public virtual string Name { get; set; }
    public virtual string Description { get; set; }
    public virtual DateTime DateCreated { get; set; }
    public virtual Bitmask Flags { get; set; }

    public PocoSql() {
        Flags = new Bitmask();
    }
}

public class PocoSqlPMap : Z.Data.PSql.ClassMap<PocoSql>
{
    public PocoSqlPMap() {
        Table("pocosqls"); //To use same table with nhibernate
        Id(x => x.Id);
        Property(x => x.Name);
        Property(x => x.Description);
        Property(x => x.DateCreated);
    }
}

public class PocoSqlNHMap : Z.Data.NHTools.NHClassMap<PocoSql>
{
    public PocoSqlNHMap() {
        Id(x => x.Id);
        Property(x => x.Name);
        Property(x => x.Description);
        Property(x => x.DateCreated);
        PropertyNullable(x => x.Flags);
    }
}