﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Resources;
using Z.Extensions;

namespace Z.Util.Text;

/// <summary>Resource File manager and text replacer (texts in {} are considered resource file keys)</summary>
public static class LocalizationHelperBase
{
    #region Fields / Properties
    /// <summary>Base resource manager to use</summary>
    public static ResourceManager? RM { get; set; }
    #endregion

    /// <summary>Replaces localized keys with culture appropriate values</summary>
    /// <param name="s">String containing resource keys marked with {}</param>
    public static IEnumerable<string> Translate(params string[] s) => Translate(RM ?? throw new NullReferenceException(), s);

    /// <summary>Replaces localized keys with culture appropriate values</summary>
    /// <param name="rm">Custom Resource manager to use first</param>
    /// <param name="s">String containing resource keys marked with {}</param>
    public static IEnumerable<string> Translate(ResourceManager rm, params string[] s) {
        if (s == null) yield break;
        for (var i = 0; i < s.Length; i++) yield return Translate(s[i], rm);
    }
    /// <summary>Replaces localized keys with culture appropriate values</summary>
    /// <param name="s">String containing resource keys marked with {}</param>
    /// <param name="rm">Custom Resource manager to use first</param>
    public static string Translate(string s, ResourceManager? rm = null) {
        var ss = s.IndexOf('{');
        if (ss < 0) return s;
        var se = s.IndexOf('}', ss + 1);
        if (se < 0) return s;
        var found = s.Substring(ss + 1, se - ss - 1);
        if (string.IsNullOrWhiteSpace(found)) return s;
        var left = s[..ss];
        var leftover = s.Replace(left + "{" + found + "}", "");

        //Do Translation (Recurse until nothing to translate)
        var foundval = TranslateName(found, rm);
        if (foundval != "{" + found + "}") foundval = Translate(foundval, rm);
        found = foundval;

        //Leftover parts
        if (!leftover.Is().Empty) found += Translate(leftover, rm);
        return left + found;
    }

    /// <summary>Returns localized values matching the specified key</summary>
    /// <param name="s">Resource manager key string</param>
    /// <param name="rm">Custom Resource manager to use first</param>
    public static string TranslateName(string s, ResourceManager? rm = null) {
        if (s.Is().Empty) return s;
        string? rval = null;
        if (rm != null) {
            rval = rm.GetString(s);
            if (string.IsNullOrEmpty(rval) && RM != null) rval = RM.GetString(s);
        } else if (RM != null) {
            rval = RM.GetString(s);
        }

        if (string.IsNullOrEmpty(rval)) rval = "{" + s + "}"; //"[" + s + "]";
        return rval;
    }
}