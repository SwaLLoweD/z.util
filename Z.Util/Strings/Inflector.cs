﻿/*
https://github.com/srkirkland/Inflector

The MIT License(MIT)
Copyright(c) 2013 Scott Kirkland

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System.Text.RegularExpressions;

namespace Z.Util.Inflector;

/// <summary>Inflector string manipulator.</summary>
public static class Inflector
{
    /// <summary>Convert to titlized style.</summary>
    /// <returns>titlized word</returns>
    /// <example>the man who sold the world => The Man Who Sold The World</example>
    public static string Titleize(string word) =>
        Regex.Replace(Humanize(Underscore(word)), @"\b([a-z])", match => match.Captures[0].Value.ToUpper());

    /// <summary>
    ///     Capitalizes the first word and turns underscores into spaces and strips a trailing “_id”, if any. Like
    ///     titleize, this is meant for creating pretty output
    /// </summary>
    /// <example>employee_salary_id => Employee salary</example>
    public static string Humanize(string lowercaseAndUnderscoredWord) => Capitalize(Regex.Replace(lowercaseAndUnderscoredWord, "_", " "));

    /// <summary>Convert to pascalized form.</summary>
    public static string Pascalize(string lowercaseAndUnderscoredWord) =>
        Regex.Replace(lowercaseAndUnderscoredWord, "(?:^|_)(.)", match => match.Groups[1].Value.ToUpper());

    /// <summary>Convert to camel-case form.</summary>
    /// <example>active_model => ActiveModel</example>
    public static string Camelize(string lowercaseAndUnderscoredWord) => Uncapitalize(Pascalize(lowercaseAndUnderscoredWord));

    /// <summary>Returns an underscore-syntaxed ($like_this_dear_reader) version of the CamelCasedWord.</summary>
    /// <example>ActiveModel => active_model</example>
    public static string Underscore(string pascalCasedWord) =>
        Regex.Replace(
            Regex.Replace(
                Regex.Replace(pascalCasedWord, "([A-Z]+)([A-Z][a-z])", "$1_$2"), @"([a-z\d])([A-Z])",
                "$1_$2"), @"[-\s]", "_").ToLower();

    /// <summary>Capitalize</summary>
    public static string Capitalize(string word) => word[..1].ToUpper() + word[1..].ToLower();

    /// <summary>Uncapitalize</summary>
    public static string Uncapitalize(string word) => word[..1].ToLower() + word[1..];

    /// <summary>Returns ordinal form of a number string</summary>
    /// <example>1 => 1st</example>
    public static string Ordinalize(string numberString) => Ordanize(int.Parse(numberString), numberString);

    /// <summary>Returns ordinal form of a number string</summary>
    /// <example>1 => 1st</example>
    public static string Ordinalize(int number) => Ordanize(number, number.ToString());

    private static string Ordanize(int number, string numberString) {
        var nMod100 = number % 100;

        if (nMod100 >= 11 && nMod100 <= 13) return numberString + "th";

        return (number % 10) switch {
            1 => numberString + "st",
            2 => numberString + "nd",
            3 => numberString + "rd",
            _ => numberString + "th",
        };
    }

    /// <summary>Replaces underscores with dashes in the string.</summary>
    /// <example>active_model => active-model</example>
    public static string Dasherize(string underscoredWord) => underscoredWord.Replace('_', '-');

    #region Nested type: Rule
    private class Rule
    {
        #region Fields / Properties
        private readonly Regex _regex;
        private readonly string _replacement;
        #endregion

        #region Constructors
        public Rule(string pattern, string replacement) {
            _regex = new Regex(pattern, RegexOptions.IgnoreCase);
            _replacement = replacement;
        }
        #endregion

        public string? Apply(string word) {
            if (!_regex.IsMatch(word)) return null;

            return _regex.Replace(word, _replacement);
        }
    }
    #endregion
}