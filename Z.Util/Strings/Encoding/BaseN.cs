/* Copyright (c) <2011> <xanatos, A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Numerics;
using System.Text;

namespace Z.Util;

/// <summary>Converts Byte Arrays into strings using a fixed alphabet</summary>
public static class BaseN
{
    #region Constants / Static Fields
    /// <summary>Alphabet used in Base36 Conversions</summary>
    public const string Base36Alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    #endregion

    /// <summary>Byte Array representation of given string</summary>
    /// <param name="str">String</param>
    /// <param name="alphabet">Alphabet to decode</param>
    /// <returns></returns>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public static byte[] FromBaseN(string str, string alphabet) {
        // This should be the maximum length of the byte[] array. It's the opposite of the one used
        // in ToBaseN.
        var len = (int)Math.Ceiling(str.Length * Math.Log(alphabet.Length, 2) / 8);

        var bytes = new List<byte>(len);

        // We strip initial characters that are 'zeros'.
        var i = 0;

        while (i < str.Length && str[i] == alphabet[0]) {
            i++;
            bytes.Add(0);
        }

        // If the string was composed only of 'zeros', we have finished.
        if (i == str.Length) return bytes.ToArray();

        var bi = BigInteger.Zero;
        BigInteger length = alphabet.Length;
        var mult = BigInteger.One;

        for (var j = str.Length - 1; j >= i; j--) {
            var ix = alphabet.IndexOf(str[j]);

            // We didn't find the character
            if (ix == -1) throw new ArgumentOutOfRangeException(nameof(alphabet));

            bi += ix * mult;

            mult *= length;
        }

        var bytes2 = bi.ToByteArray();

        for (var j = bytes2[^1] == 0 ? bytes2.Length - 2 : bytes2.Length - 1; j >= 0; j--) bytes.Add(bytes2[j]);

        return bytes.ToArray();
    }
    /// <summary>String representation of given byte[]</summary>
    /// <param name="bytes">Byte Array</param>
    /// <param name="alphabet">Alphabet to encode in</param>
    /// <returns></returns>
    public static string ToBaseN(IEnumerable<byte> bytes, string alphabet) {
        if (bytes == null) return string.Empty;
        var array = bytes.ToArray();
        if (array.Length == 0) return string.Empty;

        // We try to pre-calc the length of the string. We know the bits of "information"
        // of a byte are 8. Using Log2 we calc the bits of information of our new base.
        var len = (int)Math.Ceiling(array.Length * 8 / Math.Log(alphabet.Length, 2));

        var sb = new StringBuilder(len);

        // We strip initial bytes set to 0. This because 01 == 1, but we are working on
        // binary data, so the initial zeros are important! We will save these zeros "manually"
        var i = 0;

        while (i < array.Length && array[i] == 0) i++;

        // If the byte array was composed only of zeros, we have finished.
        if (i == array.Length) {
            sb.Append(alphabet[0], i);
            return sb.ToString();
        }

        var newLength = array.Length - i;

        // BigInteger saves in the last byte the sign. > 7F negative, <= 7F positive.
        // If we have a "negative" number, we will prepend a 0 byte.
        // (Here we are working with a pre-Reverse array, so we have to look at the
        // first byte that will be copied)
        if (array[i] > 0x7F) newLength++;

        var bytes2 = new byte[newLength];

        // We reverse the array (and we strip the initial zeros)
        for (int j = array.Length - 1, k = 0; j >= i; j--, k++) bytes2[k] = array[j];

        var bi = new BigInteger(bytes2);

        // A little optimization. We will do many divisions based on chars.Length
        BigInteger length = alphabet.Length;

        while (bi > 0) {
            bi = BigInteger.DivRem(bi, length, out BigInteger remainder);

            sb.Append(alphabet[(int)remainder]);
        }

        // We append the zeros that we skipped at the beginning
        sb.Append(alphabet[0], i);

        // We reverse the StringBuilder
        var length2 = sb.Length / 2;

        for (int j = 0, k = sb.Length - 1; j < length2; j++, k--) {
            (sb[k], sb[j]) = (sb[j], sb[k]);
        }

        return sb.ToString();
    }
    /// <summary>Get Max Value for specified string length and alphabet</summary>
    public static byte[] GetMaxValue(int targetStrLength, string alphabet) {
        var testB = new List<byte> {
                0x00
            };
        byte firstByteMaxValue = 0xFF;
        //long maxLength = 0;
        string? val;
        for (var i = 1; i < 10000; i++) {
            testB.Add(0xFF);
            val = ToBaseN(testB, alphabet);
            if (val.Length > targetStrLength) {
                testB.RemoveAt(i);
                //maxLength = i;
                break;
            }
        }
        for (byte i = 0x00; i < 0xFF; i++) {
            testB[0] = i;
            val = ToBaseN(testB, alphabet);
            if (val.Length > targetStrLength) {
                firstByteMaxValue = (byte)(i - 1);
                break;
            }
        }
        testB[0] = firstByteMaxValue;
        return testB.ToArray();
    }
}
