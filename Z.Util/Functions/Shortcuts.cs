﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;

namespace Z.Util;

/// <summary>General Function Library</summary>
public static partial class Fnc
{
    /// <summary>Packs two long values into a Guid</summary>
    /// <param name="lo">Lower bits of the new Guid.</param>
    /// <param name="hi">Higher bits of the new Guid.</param>
    /// <returns>New guid created from two long values</returns>
    public static Guid GuidParse(long lo, long hi = 0) {
        var b = new byte[16];
        var lb = BitConverter.GetBytes(lo);
        var hb = BitConverter.GetBytes(hi);
        Buffer.BlockCopy(lb, 0, b, 8, 8);
        Buffer.BlockCopy(hb, 0, b, 0, 8);
        var g = new Guid(b);
        return g;
    }
    ///<summary>Find last time a daily event (at specific time) occured</summary>
    public static DateTime DailyTimeBeforeNow(DateTime dt, DateTime? now = null) {
        now ??= DateTime.UtcNow;
        if (dt == now) return dt.AddDays(-1);
        if (dt > now) return DailyTimeBeforeNow(dt.AddDays(-1), now);
        var pDt = dt.AddDays(1);
        if (dt < now && pDt <= now) return DailyTimeBeforeNow(pDt, now);
        return dt;
    }
    ///<summary>Returns 1970-01-01 in utc</summary>
    public static DateTime MinDate() => new(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

    ///<summary>Fill all possible values of an enum into an IEnumerable</summary>
    public static IEnumerable<T> EnumList<T>() where T : struct, IConvertible {
        if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enumerated type");
        return (T[])Enum.GetValues(typeof(T));
    }
    ///<summary>Gets date from a milisecond value (if no start date is fiven 1970-1-1 is used)</summary>
    public static DateTime DateFromMs(double ms, DateTimeKind kind = DateTimeKind.Utc, DateTime? startDate = null) {
        var time = TimeSpan.FromMilliseconds(ms);
        var rval = startDate ?? new DateTime(1970, 1, 1, 0, 0, 0, kind) + time;
        return rval;
    }
}