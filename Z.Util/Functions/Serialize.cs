﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml;
using Z.Extensions;

namespace Z.Util;

/// <summary>Custom Serialization Information</summary>
public class ZSerializationInfo {
    #region Fields / Properties
    /// <summary>Properties to skip</summary>
    public Dictionary<Type, HashSet<PropertyInfo>> SkipProperties = new();
    #endregion

    /// <summary>Add properties to skip</summary>
    /// <param name="properties">Property lambda expression</param>
    public void AddSkipProperty<T>(params Expression<Func<T, object>>[] properties) {
        foreach (var property in properties) {
            if (property == null) return;
            AddSkipProperty(Fnc.LambdaProperty(property));
        }
    }

    /// <summary>Add properties to skip</summary>
    /// <param name="properties"></param>
    public void AddSkipProperty(params PropertyInfo[] properties) {
        foreach (var property in properties) {
            if (property == null || property.ReflectedType == null) return;
            var value = SkipProperties.TryGetValue(property.ReflectedType) ?? new HashSet<PropertyInfo>();
            value.AddUnique(property);
            SkipProperties[property.ReflectedType] = value;
        }
    }

    /// <summary>Property will be skipped or not</summary>
    /// <param name="pi">Property</param>
    public bool SkipProperty(PropertyInfo pi) {
        if (pi == null || pi.ReflectedType == null) return false;
        var value = SkipProperties.TryGetValue(pi.ReflectedType);
        if (value == null) return false;
        if (!value.Contains(pi)) return false;
        return true;
    }
}

/// <summary>Encryption related common functions</summary>
public static partial class Fnc {
    #region Custom Xml Serialization
    /// <summary>Custom XML Serialization</summary>
    /// <param name="e">Object to serialize</param>
    /// <param name="info">Other serialization information (ex. which properties to skip etc.)</param>
    public static XmlDocument SerializeXml2(object? e, ZSerializationInfo? info = null) {
        var xml = new XmlDocument();
        xml.CreateXmlDeclaration("1.0", null, null);
        if (e == null) return xml;

        var type = e.GetType();
        var valueInfo = ZTypeCache.GetValueInfo(type);
        var typeName = type.Name.Replace("`", "");
        foreach (var genTyp in type.GetGenericArguments()) typeName += genTyp.Name;

        var xroot = xml.CreateChildNode(typeName);

        if (valueInfo.IsCollection) {
            foreach (var colitem in (IEnumerable)e) {
                if (colitem.GetType().IsValueType || colitem.GetType().IsAssignableTo<string>()) {
                    xroot.InnerXml += "<ListItem>" + colitem + "</ListItem>";
                    //xroot.InnerXml += colitem.ToString();
                    continue;
                }
                var colitemXml = SerializeXml2(colitem, info);
                if (colitemXml == null) continue;
                xroot.InnerXml += colitemXml.InnerXml;
            }
            return xml;
        }

        var defaultObj = Activator.CreateInstance(e.GetType()) ?? throw new NullReferenceException();

        if (valueInfo.ContainerType == null) return xml;
        var members = valueInfo.ContainerType.GetMembers(ZTypeCacheFlags.Properties);
        foreach (var memInfo in members) {
            //if (!pi.Member.CanRead) continue;
            if (memInfo.Member is PropertyInfo pinfo && info?.SkipProperty(pinfo) == true) continue;

            var settingVal = SerializeProperty(memInfo.Getter(e), memInfo.Value, info);
            var defaultVal = SerializeProperty(memInfo.Getter(defaultObj), memInfo.Value, info);

            // skip if it has default value
            if (settingVal == defaultVal) continue;

            var node = xroot.CreateChildNode(memInfo.Member.Name);
            //var name = node.CreateChildNode("Name");
            //var val = node.CreateChildNode("Value");
            //name.InnerText = pi.Name;

            if (settingVal.StartsWith("<") && settingVal.EndsWith(">"))
                node.InnerXml = settingVal;
            else
                node.InnerText = settingVal;

            var attrCi = memInfo.Member.GetCustomAttributes(typeof(AttrEncrypt), true);
            if (attrCi.Length > 0) {
                var attrEnc = (AttrEncrypt)attrCi[0];
                if (node.InnerXml.Is().NotEmpty) node.InnerXml = Encrypter.Default.Aes(attrEnc.Key.To().ByteArray.AsUtf8).Encrypt(node.InnerXml.To().ByteArray.AsUtf8).To().String.AsBase64;
            }
        }
        return xml;
    }
    private static string SerializeProperty(object? propertyVal, ZValueInfo pi, ZSerializationInfo? info) {
        var propValue = "";
        if (propertyVal == null) return propValue;
        if (pi.Type.IsAssignableTo<DateTime>()) {
            propValue = ((DateTime)propertyVal).ToUniversalTime().ToBinary().ToString();
        } else if (pi.Type.IsAssignableTo<Bitmask>()) {
            propValue = ((Bitmask)propertyVal).Mask.ToString();
        } else if (pi.Type.IsAssignableTo<Version>()) {
            propValue = ((Version)propertyVal ?? Version.Parse("0.0")).ToString();
        } else if (pi.IsCollection) {
            foreach (var colitem in (IEnumerable)propertyVal) {
                if (colitem.GetType().IsValueType || colitem.GetType().IsAssignableTo<string>()) {
                    propValue += "<ListItem>" + colitem + "</ListItem>";
                    continue;
                }
                var colitemXml = SerializeXml2(colitem, info);
                if (colitemXml == null) continue;
                propValue += colitemXml.InnerXml;
            }
        } else if (pi.Type.IsValueType || pi.Type.IsAssignableTo<string>()) {
            propValue = Convert.ToString(propertyVal) ?? "";
            //if (pi.GetCustomAttributes(typeof(AttrEncrypt), true).Length > 0) propValue = Encrypt.AesEncrypt(propValue, Opertum.Get(Data.Keys.Key1), 256);
        } else {
            var propXml = SerializeXml2(propertyVal, info);
            if (propXml != null) propValue = propXml.InnerXml;
        }
        return propValue;
    }

    /// <summary>Custom Deserialize from XML</summary>
    /// <param name="instance">Current instance of object to fill</param>
    /// <param name="xml">Custom XML data</param>
    /// <param name="parsedProperties">Properties that have been found and filled</param>
    public static T? DeserializeXml2<T>(T instance, XmlDocument xml, ISet<ZMemberInfo>? parsedProperties = null) {
        if (xml == null) return default;
        var defaultSettings = Activator.CreateInstance<T>() ?? throw new NullReferenceException();

        var rval = instance ?? Activator.CreateInstance<T>();
        if (rval == null) return rval;

        var type = rval.GetType();
        var valueInfo = ZTypeCache.GetValueInfo(type);
        var typeName = type.Name.Replace("`", "");
        foreach (var genTyp in type.GetGenericArguments()) typeName += genTyp.Name;

        XmlNode? xroot;
        try {
            xroot = xml.SelectSingleNode(typeName);
        } catch {
            return rval;
        }
        if (xroot == null) return instance;

        if (valueInfo.IsCollection) {
            var genericType = rval.GetType().GetGenericArguments()[0];
            var listnodes = xroot.ChildNodes;

            if (genericType.IsValueType || genericType.IsAssignableTo<string>()) listnodes = xroot.SelectNodes("ListItem");
            if (listnodes == null) return rval;
            foreach (XmlNode colitem in listnodes) {
                if (genericType.IsValueType || genericType.IsAssignableTo<string>()) {
                    rval.GetType().GetMethod("Add")?.Invoke(rval, new[] { Convert.ChangeType(colitem.InnerText, genericType) });
                    //rval.Add(Convert.ChangeType(colitem.InnerText, genericType));
                    continue;
                }
                var colXml = new XmlDocument();
                colXml.LoadXml(colitem.OuterXml);
                dynamic? colObj = DeserializeXml2(Activator.CreateInstance(genericType), colXml);
                if (colObj != null)
                    rval.GetType().GetMethod("Add")?.Invoke(rval, new[] { colObj });
                //rval.Add(colObj);
            }
            return rval;
        }
        if (valueInfo.ContainerType == null) return default;
        foreach (var pi in valueInfo.ContainerType.GetMembers(ZTypeCacheFlags.Properties)) {
            var node = xroot.SelectSingleNode(pi.Member.Name);
            if (node == null) continue;

            string val;
            if (node.InnerXml.StartsWith("<") && node.InnerXml.EndsWith(">"))
                val = node.InnerXml;
            else
                val = node.InnerText;
            try {
                var attrCi = pi.Member.GetCustomAttributes(typeof(AttrEncrypt), true);
                if (attrCi.Length > 0) {
                    var attrEnc = (AttrEncrypt)attrCi[0];
                    if (val.Is().NotEmpty && attrEnc.Key != null) val = Encrypter.Default.Aes(attrEnc.Key.To().ByteArray.AsUtf8).Decrypt(val.To().ByteArray.AsBase64).To().String.AsUtf8;
                }
            } catch { }
            try {
                var memType = pi.Value.Type;
                if (pi.Value.IsNullable) {
                    if (val == null) {
                        pi.Setter(rval, null);
                        continue;
                    }
                    if (pi.Value.NullableUnderlyingType != null) memType = pi.Value.NullableUnderlyingType;
                }
                if (memType == typeof(DateTime)) {
                    pi.Setter(rval, DateTime.FromBinary(Convert.ToInt64(val)));
                } else if (memType.IsAssignableTo<Bitmask>()) {
                    (pi.Getter(rval) as Bitmask ?? new Bitmask())
                        .Mask = Convert.ToInt64(val);
                } else if (memType.IsAssignableTo<Version>()) {
                    _ = Version.TryParse(val, out var v);
                    pi.Setter(rval, v);
                } else if (memType.IsAssignableTo<Guid>()) {
                    pi.Setter(rval, new Guid(val));
                } else if (memType.IsEnum) {
                    pi.Setter(rval, Enum.Parse(memType, val, true));
                } else if (pi.Value.IsCollection) {
                    var genericType = memType.GetGenericArguments()[0];
                    var listnodes = node.ChildNodes;
                    if (genericType.IsValueType || genericType.IsAssignableTo<string>()) listnodes = node.SelectNodes("ListItem");
                    var curCollection = pi.Getter(rval) ?? Activator.CreateInstance(pi.Value.Type);
                    if (curCollection == null || listnodes == null) continue;
                    foreach (XmlNode colitem in listnodes) {
                        if (genericType.IsValueType || genericType.IsAssignableTo<string>()) {
                            curCollection.GetType().GetMethod("Add")?.Invoke(curCollection, [Convert.ChangeType(colitem.InnerText, genericType)]);
                            //curCollection.Add(Convert.ChangeType(colitem.InnerText, genericType));
                            continue;
                        }
                        var colXml = new XmlDocument();
                        colXml.LoadXml(colitem.OuterXml);
                        dynamic? colObj = DeserializeXml2(Activator.CreateInstance(genericType), colXml);
                        curCollection.GetType().GetMethod("Add")?.Invoke(curCollection, new[] { colObj });
                        //curCollection.Add(colObj);
                    }
                } else if (memType.IsValueType || memType.IsAssignableTo<string>()) {
                    pi.Setter(rval, Convert.ChangeType(val, memType));
                } else {
                    var clXml = new XmlDocument();
                    clXml.LoadXml(val);
                    //dynamic
                    var clObj = DeserializeXml2(Activator.CreateInstance(memType), clXml);
                    pi.Setter(rval, Convert.ChangeType(clObj, memType));
                }
            } catch (Exception exp) {
                Console.WriteLine(exp.ToString());
            }
            if (parsedProperties != null && !Equals(pi.Getter(rval), pi.Getter(defaultSettings))) parsedProperties.Add(pi);
        }
        return rval;
    }
    #endregion Custom Xml Serialization
}
