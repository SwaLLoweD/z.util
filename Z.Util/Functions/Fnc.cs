﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Z.Util;

public static partial class Fnc
{
    /// <summary>Get PropertyInfo from LambdaExpression</summary>
    /// <typeparam name="T">Object property belongs to</typeparam>
    /// <typeparam name="TProp">Property Return type</typeparam>
    /// <param name="property">Lambda Expression</param>
    /// <returns></returns>
    public static PropertyInfo LambdaProperty<T, TProp>(Expression<Func<T, TProp>> property) {
        return property.Body is MemberExpression expression
            ? (PropertyInfo)expression.Member
            : (PropertyInfo)((MemberExpression)((UnaryExpression)property.Body).Operand).Member;
    }

    /// <summary>Get Property Value from LambdaExpression</summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TProp">Property Return type</typeparam>
    /// <param name="property">Lambda Expression</param>
    /// <param name="obj">Object instance</param>
    /// <returns></returns>
    public static object? LambdaPropertyVal<T, TProp>(Expression<Func<T, TProp>> property, T obj) => typeof(T).GetProperty(LambdaProperty(property).Name)!.GetValue(obj, null);

    /// <summary>Get Property name from LambdaExpression</summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TProp">Property Return type</typeparam>
    /// <param name="property">Lambda Expression</param>
    /// <returns></returns>
    public static string LambdaPropertyName<T, TProp>(Expression<Func<T, TProp>> property) => LambdaProperty(property).Name;

    /// <summary>Get PropertyInfo from LambdaExpression</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="property">Lambda Expression</param>
    /// <returns></returns>
    public static PropertyInfo LambdaProperty<T>(Expression<Func<T, object>> property) => LambdaProperty<T, object>(property);

    /// <summary>Get Property Value from LambdaExpression</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="property">Lambda Expression</param>
    /// <param name="obj">Object instance</param>
    /// <returns></returns>
    public static object? LambdaPropertyVal<T>(Expression<Func<T, object>> property, T obj) => typeof(T).GetProperty(LambdaProperty<T, object>(property).Name)!.GetValue(obj, null);

    /// <summary>Get Property name from LambdaExpression</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="property">Lambda Expression</param>
    /// <returns></returns>
    public static string LambdaPropertyName<T>(Expression<Func<T, object>> property) => LambdaProperty<T, object>(property).Name;
}