﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Timers;
using Microsoft.IO;

namespace Z.Util {
    /// <summary>Z.Util Library Configuration</summary>
    [Serializable]
    public static class ZUtilCfg {
        #region Constants / Static Fields
        private static volatile Random? globalRandom;

        private static System.Timers.Timer? globalCacheTimer;

        private static RecyclableMemoryStreamManager? globalMemoryStreamManager;
        #endregion

        #region Fields / Properties
        /// <summary>Return a private random number generator to use if the user doesn't supply one.</summary>
        /// <returns>The private random number generator. Only one is ever created and is always returned.</returns>
        public static Random Random => globalRandom ??= new Random();
        /// <summary>Encrypter cache max number of items.</summary>
        public static int EncrypterCacheMax { get; set; } = 5000;
        /// <summary>Encrypter cache min number of items.</summary>
        public static int EncrypterCacheMin { get; set; } = 10;
        /// <summary>Encrypter cache timeout in milliseconds.</summary>
        public static int EncrypterCacheTtl { get; set; } = 200000;
        /// <summary>Encrypter Default Salt Value to derive keys if none is specified.</summary>
        public static byte[] EncrypterDefaultSalt { get; set; } = { 0x16, 0x03, 0xF2, 0x54, 0x23, 0xA1, 0xDD, 0x25, 0x85, 0x36 };
        /// <summary>Encrypter Default Buffer size to copy between encryption streams</summary>
        public static int EncrypterStreamBufferSize { get; set; } = 4096;
        /// <summary>Z.Util CacheTable implementation global timer.</summary>
        public static System.Timers.Timer GlobalCacheTimer {
            get => globalCacheTimer ??= new System.Timers.Timer(GlobalCacheTimerInterval) {
                AutoReset = false,
                Enabled = false
            };
            private set => globalCacheTimer = value;
        }
        /// <summary>Z.Util Recyclable Memory Stream Pool.</summary>
        public static RecyclableMemoryStreamManager GlobalMemoryStreamManager {
            get => globalMemoryStreamManager ??= new RecyclableMemoryStreamManager();
            private set => globalMemoryStreamManager = value;
        }
        /// <summary>Z.Util Cache Implementations global check timer interval in milliseconds.</summary>
        public static double GlobalCacheTimerInterval { get => globalCacheTimer == null ? 10000 : globalCacheTimer.Interval; set => GlobalCacheTimer.Interval = value; }
        /// <summary>Some platforms do not allow code generation.</summary>
        public static bool UseLambdaCodeGen { get; set; } = true;
        /// <summary>Type cache timeout in milliseconds.</summary>
        public static int ZTypeCacheTtl { get; set; } = 600000;
        /// <summary>ZTypeCache Lambda Property Compilation switch point on the number of cache hits</summary>
        public static int ZTypeCacheUseHeavyCacheHitCount { get; set; } = 5;
        #endregion

        #region Constructors
        static ZUtilCfg() {
            //globalCacheTimer.Elapsed += delegate {
            //    //Stop when all caches are empty
            //    if ((CompiledCopyFunctions.Count == 0) &&
            //        (CompiledShallowCloneFunctions.Count == 0) &&
            //        (CompiledCloneFunctions.Count == 0) &&
            //        (IsStructTypeToDeepCopyDictionary.Count == 0)
            //        ) Timer.Stop();
            //};
        }
        #endregion
        /// <summary>Add default options setter for this encrypter for related symmetric algorithm type.</summary>
        public static void EncrypterDefaultOptionAdd<T>(Action<T> optSetter) where T : System.Security.Cryptography.SymmetricAlgorithm => Encrypter.Default.DefaultOptionAdd(optSetter);
    }
}