﻿using System;
using System.Linq.Expressions;
using Z.Extensions;

namespace Z.Util;
/// <summary>Dynamic Predicate builder</summary>
public static class PredicateBuilder
{
    ///// <summary>expr1 OR expr2</summary>
    //public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
    //                                                    Expression<Func<T, bool>> expr2)
    //{
    //    var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.To().Type<Expression>());
    //    return Expression.Lambda<Func<T, bool>>
    //          (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
    //}

    ///// <summary>expr1 AND expr2</summary>
    //public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
    //                                                     Expression<Func<T, bool>> expr2)
    //{
    //    var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.To().Type<Expression>());
    //    return Expression.Lambda<Func<T, bool>>
    //          (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
    //}
    /// <summary>Base condition is true (useful for AND conditions)</summary>
    public static Expression<Func<T, bool>> True<T>() => Expression.Lambda<Func<T, bool>>(Expression.Constant(true), Expression.Parameter(typeof(T)));
    /// <summary>Base condition is false (useful for OR conditions)</summary>
    public static Expression<Func<T, bool>> False<T>() => Expression.Lambda<Func<T, bool>>(Expression.Constant(false), Expression.Parameter(typeof(T)));
    /// <summary>Expression</summary>
    public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> self, Expression<Func<T, bool>> expression) => self.Combine(expression, Expression.OrElse);
    /// <summary>Expression</summary>
    public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> self, Expression<Func<T, bool>> expression) => self.Combine(expression, Expression.AndAlso);
    /// <summary>Expression</summary>
    public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> self) => self.Combine(Expression.Not);
}