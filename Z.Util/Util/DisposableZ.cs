﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
namespace Z.Util;

/// <summary>Disposable implementation</summary>
public interface IDisposableZ : IAsyncDisposableZ, IDisposable { }
/// <summary>Disposable implementation</summary>
public interface IAsyncDisposableZ : IAsyncDisposable
{
    ///<summary>Is disposed</summary>
    bool IsDisposed { get; }
}

/// <summary>Disposable implementation</summary>
[Serializable]
public abstract class DisposableZ : IDisposable, IAsyncDisposable
{
    #region Constants / Static Fields
    ///<summary>Semaphore providing Async lock operation for this object</summary>
    protected readonly System.Threading.SemaphoreSlim _lockSemaphore = new(1, 1);
    ///<summary>Is disposed</summary>
    public virtual bool IsDisposed => _disposed;
    /// <summary>Disposed signal</summary>
    protected volatile bool _disposed = false;
    #endregion

    #region Constructors
    ///<summary>Constructor</summary>
    protected DisposableZ() { }
    #endregion

    /// <inheritdoc />
    public void Dispose() {
        Task.Run(async () => {
            await DisposeSafe(true);
            GC.SuppressFinalize(this);
        }).Wait();
    }
    /// <inheritdoc />
    public async ValueTask DisposeAsync() {
        await DisposeSafe(true);
        GC.SuppressFinalize(this);
    }
    /// <inheritdoc />
    protected async ValueTask DisposeSafe(bool disposing) {
        await _lockSemaphore.LockAsync(async () => {
            if (!_disposed && disposing) {
                await DisposeAsync(disposing);
                _disposed = true;
            }
        });
    }
    /// <summary>Dispose internals</summary>
    protected abstract ValueTask DisposeAsync(bool disposing);

    // {
    //         if (disposing) {
    //             //Release managed and unmanaged
    //         }
    //         //Release unmanaged
    // }
}