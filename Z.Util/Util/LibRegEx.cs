﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using Z.Extensions;

namespace Z.Util;

/// <summary>LibRegEx</summary>
public static class LibRegEx
{
    #region Fields / Properties
    /// <summary>Credit Card (All major)</summary>
    public static string CreditCard =>
        "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|622((12[6-9]|1[3-9][0-9])|([2-8][0-9][0-9])|(9(([0-1][0-9])|(2[0-5]))))[0-9]{10}|64[4-9][0-9]{13}|65[0-9]{14}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})*$";

    /// <summary>Rfc Compliant only e-mail address accepting country codes and ip addresses</summary>
    /// <example>struk@bilgi.org.ca</example>
    public static string Email => @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

    /// <summary>Rfc Compliant only e-mail address accepting country codes and ip addresses and recipient name</summary>
    /// <example>&lt;Admin&gt; admin@abc.com.in></example>
    public static string EmailRf2822 =>
        @"^((?>[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+\x20*|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*""\x20*)*(?<angle><))?((?!\.)(?>\.?[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+)+|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*"")@(((?!-)[a-zA-Z\d\-]+(?<!-)\.)+[a-zA-Z]{2,}|\[(((?(?<!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d)){4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\[\x01-\x7f])+)\])(?(angle)>)$";

    /// <summary>Valid single File name (Windows rules)</summary>
    public static string FileName => @"^[^\\\./:\*\?\""<>\|]{1}[^\\/:\*\?\""<>\|]{0,254}$";

    /// <summary>Valid Single Folder name (Windows only?)</summary>
    public static string FolderName => @"^[^\\\/\?\*\&quot;\'\&gt;\&lt;\:\|]*$";
    /// <summary>Hexadecimal string</summary>
    /// <example>#5dc26</example>
    public static string Hexadecimal => "^#?([a-f0-9]{6}|[a-f0-9]{3})$";

    /// <summary>Any valid HTML tag with RFC compliant attributes</summary>
    public static string HtmlTag => @"^<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)$";

    /// <summary>V4 IP address</summary>
    /// <example>192.76.7.2</example>
    public static string IPv4 => @"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";

    /// <summary>V6 IP address (compressed or uncompressed) RFC2373</summary>
    /// <example>FEDC:BA98:7654:3210:FEDC:BA98:7654:3210</example>
    public static string IPv6 =>
        @"^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$";

    /// <summary>http|https uri</summary>
    /// <example>http://192.76.7.2:81/default.htm</example>
    public static string Url => @"^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$";
    #endregion

    /// <summary>Multi-protocol uri</summary>
    /// <param name="protocols">Uri protocol (Ex: "http", "https", "ftp")</param>
    /// <example>sftp://232.123.65.2</example>
    public static string UrlParam(params string[] protocols) {
        if (protocols == null || protocols.Length == 0) protocols = new[] { "http", "https" };
        return $@"({protocols.To().CSV("|")}):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";
    }

    /// <summary>Files with specified extension (may also contain directory path)</summary>
    /// <param name="extensions">Case-sensitive file extensions (Ex: "Exe", "exe", "Jpg")</param>
    public static string FilesWithSpecifiedExtension(params string[] extensions) {
        if (extensions == null || extensions.Length == 0) extensions = new[] { "Exe", "exe" };
        return $@"^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w ]*.*))+\.({extensions.To().CSV("|")})$";
    }
}