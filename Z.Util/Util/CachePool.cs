﻿/* Copyright (c) <2019> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Concurrent;

namespace Z.Collections.Generic;

/// <summary>Infinite Object Pool to store and reuse objects</summary>
public class CachePool<T> : IDisposable
{
    #region Fields / Properties
    /// <summary>Dispose indicator</summary>
    private bool _disposed;
    private readonly object _lockObject = new();

    /// <summary>Count of items the pool is handling</summary>
    public virtual int Length => Pool.Count;
    /// <summary>Action to perform on objects put into the cache in sleep state</summary>
    public virtual Func<T, T>? FuncCache { get; set; }
    /// <summary>Action to perform on objects that are cleared off from the pool</summary>
    public virtual Action<T>? FuncClear { get; set; }

    /// <summary>Action to perform on newly created items</summary>
    public virtual Func<T>? FuncCreate { get; set; }
    /// <summary>Action to perform on objects retrieved from the cache in wake state</summary>
    public virtual Func<T, T>? FuncPop { get; set; }
    /// <summary>
    ///     Maximum count of items the pool can handle (new items will still be created but will not be pushed to the
    ///     pool)
    /// </summary>
    public virtual int MaxLength { get; set; }
    /// <summary>Object Pool</summary>
    public virtual ConcurrentStack<T> Pool { get; set; }
    #endregion

    #region Constructors
    /// <summary>InfinitePool Constructor.</summary>
    /// <param name="maxLength">Maximum count of items the pool can handle</param>
    public CachePool(int maxLength = 100) {
        MaxLength = maxLength;
        Pool = new ConcurrentStack<T>();
    }
    #endregion

    ///// <summary>InfinitePool Constructor.</summary>
    ///// <param name="maxLength">Maximum count of items the pool can handle</param>
    ///// <param name="collection">The collection whose elements are copied to the new Pool</param>
    //public InfinitePool(IEnumerable<T> collection, int maxLength = int.MaxValue)
    //{
    //    MaxLength = maxLength;
    //    Pool = new ConcurrentStack<T>(collection);
    //    Length = Pool.Count;
    //}

    /// <summary>Clear all data.</summary>
    public virtual void Clear(bool disposeItems = false) {
        if (disposeItems) {
            foreach (var item in Pool) {
                if (item == null) continue;
                FuncClear?.Invoke(item);
                if (item is IDisposable itemD) itemD.Dispose();
            }
        }

        Pool.Clear();
    }
    /// <summary>Attempts to pop and return the object at the top of the Pool</summary>
    public virtual T Pop() {
        //if (Length >= MaxLength) return default(T);
        if (Pool.TryPop(out var rval)) return rval;
        //Length++;
        if (FuncCreate != null)
            rval = FuncCreate();
        else
            rval = Activator.CreateInstance<T>();
        return rval;
    }
    /// <summary>Attempts to pop and return the object at the top of the Pool</summary>
    public virtual int PopRange(T[] items) {
        if (items == null) return 0;
        //var countToPop = MaxLength - Length;
        //if (countToPop > items.Length) countToPop = items.Length;
        for (var i = 0; i < items.Length; i++) {
            var item = Pop();
            if (item == null) return i;
            items[i] = item;
        }
        return items.Length;
    }
    /// <summary>Inserts an object at the top of the pool automatically</summary>
    public virtual void Push(T item) {
        if (item == null) return;
        if (Length >= MaxLength) {
            FuncClear?.Invoke(item);
            if (item is IDisposable itemD) itemD.Dispose();
            return;
        }
        Pool.Push(item);
    }

    /// <summary> Inserts multiple objects at the top of the pool automatically</summary>
    public virtual void PushRange(T[] items) {
        if (items == null) return;
        foreach (var item in items) Push(item);
    }

    #region Dispose
    /// <inheritdoc />
    public virtual void Dispose() {
        lock (_lockObject) {
            Dispose(true);
        }
        GC.SuppressFinalize(this);
    }
    /// <summary>Dispose inner function</summary>
    protected virtual void Dispose(bool disposing) {
        if (!_disposed) {
            // Dispose managed resources.
            if (disposing) {
                if (Pool != null) {
                    Clear(true);
                    //Pool = null;
                }
            }
            // Call the appropriate methods to clean up
            // unmanaged resources here.
            _disposed = true;
        }
    }
    #endregion Dispose
}