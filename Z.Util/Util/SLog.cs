/* Copyright (c) <2008> <A. Zafer YURDACALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Extensions;

namespace Z.Util;

/// <summary>Simple Log Entry</summary>
[Serializable]
public class SLogEntry
{
    #region Fields / Properties
    /// <summary>CreationDate</summary>
    public virtual DateTime? DateCreated { get; set; }
    /// <summary>Log Description</summary>
    public virtual string? Description { get; set; }
    /// <summary>Log Exception</summary>
    public virtual Exception? Exception { get; set; }
    /// <summary>Log Id</summary>
    public virtual Guid Id { get; set; }
    /// <summary>Queue Index</summary>
    public virtual int Index { get; set; }
    /// <summary>Log Level</summary>
    public virtual int? Level { get; set; }
    /// <summary>Log Title</summary>
    public virtual string? Title { get; set; }
    /// <summary>Log Type</summary>
    public virtual int? Type { get; set; }
    #endregion

    #region Constructors
    /// <summary>Log Entry</summary>
    public SLogEntry() {
        Id = Guid.NewGuid();
        Index = -1;
    }
    #endregion

    /// <summary>Simple String representation of the entry</summary>
    public override string ToString() => $"({Type:00}:{Level:00}) {Title}:{Description} E:({Exception?.GetBaseException()?.Message})";
}

/// <summary>Simple log event arguments</summary>
public class SLogEventArgs : EventArgs
{
    #region Fields / Properties
    /// <summary>Log entry</summary>
    public SLogEntry Entry { get; set; }
    #endregion

    /// <summary>Constructor</summary>
    public SLogEventArgs(SLogEntry entry) {
        Entry = entry;
    }
}

/// <summary>Simple Logging Facility</summary>
public class SLog
{
    #region Fields / Properties
    private readonly object flushLock = new();
    /// <summary>Triggered when a log entry is added to queue</summary>
    protected Func<SLog, SLogEventArgs, bool>? onLogAdd;
    /// <summary>Minimum Log Level</summary>
    public int MinLevel { get; set; }
    /// <summary>Log File Path</summary>
    public string? LogFile { get; set; }
    /// <summary>Queued Logs</summary>
    protected virtual ConcurrentQueue<SLogEntry> LogQueue { get; set; }
    private volatile bool logFlushLock = false;
    #endregion

    #region Events
    /// <summary>Triggered when a log entry is added to queue</summary>
    public event Func<SLog, SLogEventArgs, bool> OnLogAdd {
        add => onLogAdd += value;
        remove {
            if (onLogAdd != null) onLogAdd -= value;
        }
    }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    public SLog(Enum minLevel, string? logFile = null) : this(minLevel.To().Type<int>(), logFile) { }
    /// <summary>Constructor</summary>
    public SLog(int minLevel = 1, string? logFile = null) {
        LogQueue = new ConcurrentQueue<SLogEntry>();
        MinLevel = minLevel;
        LogFile = logFile;
    }
    #endregion

    /// <summary>Create</summary>
    public static SLogEntry? Create(string? title, string? message, Exception? exp = null, int level = 0, int type = 0) {
        if (string.IsNullOrEmpty(title) && (exp == null || exp.GetBaseException() == null)) return null;
        var ex = exp?.GetBaseException();
        var l = new SLogEntry {
            DateCreated = DateTime.UtcNow,
            Type = type,
            //l.DateAdded = l.DateModified = DateTime.UtcNow;
            Level = level
        };
        if (ex != null) l.Exception = ex;
        //{
        //    l.ExpType = ex.GetType().Name;
        //    l.ExpMessage = ex.Message.Trim();
        //    if (ex.StackTrace.Is().NotEmpty) l.ExpTrace = ex.StackTrace;
        //}
        if (!string.IsNullOrEmpty(title))
            l.Title = title;
        else if (ex != null) l.Title = ex.Message.Trim();

        if (!string.IsNullOrEmpty(message)) l.Description = message;
        return l;
    }
    /// <summary>Add</summary>
    public void Add(string? title, string? message, Exception? exp = null, Enum? level = default, Enum? type = default) => Add(title, message, exp, Convert.ToInt32(level), Convert.ToInt32(type));

    /// <summary>Add</summary>
    public void Add(string? title, string? message, Exception? exp = null, int level = 0, int type = 0) {
        if (level < MinLevel) return;
        var entry = Create(title, message, exp, level, type);
        if (entry != null) Add(entry);
    }
    /// <summary>Add</summary>
    public void Add(SLogEntry log) {
        if (log == null) return;
        if (onLogAdd == null && LogFile != null) {
            log.Index = LogQueue.Count;
            LogQueue.Enqueue(log);
            Task.Run(() => FlushQueue(LogFile, true));
        } else {
            var success = onLogAdd?.Invoke(this, new SLogEventArgs(log)) ?? true;
            if (success) {
                log.Index = LogQueue.Count;
                LogQueue.Enqueue(log);
            }
        }
    }
    // /// <summary>Remove selected index from queue (fast)</summary>
    // public void Remove(int index) {
    //     lock (logQueue) {
    //         logQueue.
    //         logQueue.RemoveAt(index);
    //     }
    // }
    // /// <summary>Remove selected id from queue (slow)</summary>
    // public void Remove(Guid id) => Remove(x => x.Id == id);
    // /// <summary>Remove selected items from queue</summary>
    // public void Remove(Predicate<SLogEntry> predicate) {
    //     lock (logQueue) {
    //         logQueue.Remove().Where(predicate);
    //     }
    // }
    // /// <summary>Remove selected log from queue</summary>
    // public void Remove(SLogEntry entry) {
    //     if (entry == null) return;
    //     if (entry.Index >= 0)
    //         Remove(entry.Index);
    //     else
    //         Remove(entry.Id);
    // }
    // /// <summary>Remove selected items from queue</summary>
    // public IList<SLogEntry> Get(Func<SLogEntry, bool> predicate) {
    //     lock (logQueue) {
    //         return logQueue.Where(predicate).ToList();
    //     }
    // }
    // /// <summary>Get selected id from queue (slow)</summary>
    // public SLogEntry Get(Guid id) => Get(x => x.Id == id).FirstOrDefault();
    // /// <summary>Remove selected index from queue (fast)</summary>
    // public SLogEntry Get(int index) => logQueue[index];

    /// <summary>Clear queue</summary>
    public void Clear() {
        lock (LogQueue) {
            //logQueue.Clear();
            LogQueue = new ConcurrentQueue<SLogEntry>();
        }
    }
    /// <summary>Flush queue to file</summary>
    public void FlushQueue(string path, bool clearAfterFlush = true) {
        lock (flushLock) {
            if (logFlushLock) return; //Already flushing.
            logFlushLock = true;
            var fileWriteCnt = 0;
        WriteFile:
            try {
                lock (LogQueue) {
                    using (var fw = new StreamWriter(path, true)) {
                        while (LogQueue.TryDequeue(out var l)) {
                            var sb = new StringBuilder();
                            sb.AppendString(l.DateCreated, "\t", l.Level, "\t", l.Type, "\t");
                            if (!string.IsNullOrEmpty(l.Title)) sb.AppendString(l.Title?.Replace("\n", "  "), "\t");
                            if (!string.IsNullOrEmpty(l.Description)) sb.AppendString(l.Description?.Replace("\n", "  "), "\t");
                            if (l.Exception != null) {
                                sb.AppendString(l.Exception.GetType().Name.Replace("\n", "  "), "\t");
                                if (l.Exception.Message.Is().NotEmpty) sb.AppendString(l.Exception.Message?.Trim().Replace("\r\n", "  ").Replace("\n", "  "), "\t");
                                if (!string.IsNullOrEmpty(l.Exception.StackTrace)) sb.AppendString(l.Exception.StackTrace?.Trim().Replace("\r\n", "  ").Replace("\n", "  "), "\t");
                            }
                            fw.WriteLine(sb.ToString());
                        }
                        fw.Flush();
                        fw.Close();
                    }
                    if (clearAfterFlush) LogQueue = new ConcurrentQueue<SLogEntry>(); //logQueue.Clear();
                }
            } catch (Exception exp) {
#if DEBUG
                Console.Write($"Log Error: {exp.GetOriginalException().Message}");
#endif
                if (fileWriteCnt++ > 30) return; //Max 30 retries - Then > Fail
                goto WriteFile; //Retry
            } finally {
                logFlushLock = false;
            }
        }
    }
    /// <summary>Flush queue to Array</summary>
    public SLogEntry[] FlushQueue(bool clearAfterFlush = true) {
        lock (flushLock) {
            SLogEntry[] rval;
            lock (LogQueue) {
                rval = LogQueue.ToArray();
            }
            if (clearAfterFlush) LogQueue = new ConcurrentQueue<SLogEntry>(); //logQueue.Clear();
            return rval;
        }
    }

    #region Overloads
    /// <summary>Add</summary>
    public void Add(string title, string message, int level = 0, int type = 0) => Add(title, message, null, level, type);
    /// <summary>Add</summary>
    public void Add(Exception exp, int level = 0, int type = 0) => Add(null, null, exp, level, type);
    /// <summary>Add</summary>
    public void Add(string title, string message, Enum? level = default, Enum? type = default) => Add(title, message, null, level, type);
    /// <summary>Add</summary>
    public void Add(Exception exp, Enum? level = default, Enum? type = default) => Add(null, null, exp, level, type);
    /// <summary>Add</summary>
    public static SLogEntry? Create(string title, string message, int level = 0, int type = 0) => Create(title, message, null, level, type);
    /// <summary>Add</summary>
    public static SLogEntry? Create(Exception exp, int level = 0, int type = 0) => Create(null, null, exp, level, type);
    #endregion Overloads
}
