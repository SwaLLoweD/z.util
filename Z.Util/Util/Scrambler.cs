/* Copyright (c) <2011> <xanatos, A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Z.Util;

/// <summary>Scramble Array using a seed</summary>
public static class Scrambler
{
    /// <summary>Scramble Array using a seed</summary>
    public static T[] Scramble<T>(IEnumerable<T> values, int? key = null) {
        var array = values.ToArray();
        if (array.Length <= 1) return array;
        var rKey = key ?? 22;
        var random = new Random(rKey);
        for (var i = array.Length; i > 1; i--) Swap(array, random.Next(i), i - 1);
        return array;
    }
    /// <summary>Unscramble Array using a seed</summary>
    public static T[] Unscramble<T>(IEnumerable<T> values, int? key = null) {
        var array = values.ToArray();
        if (array.Length <= 1) return array;
        var rKey = key ?? 22;
        var random = new Random(rKey);
        var swaps = new List<int>(array.Length);
        for (var i = array.Length; i > 1; i--) swaps.Add(random.Next(i));
        swaps.Reverse();
        for (var i = 0; i < swaps.Count; ++i) Swap(array, swaps[i], i + 1);
        return array;
    }

    private static void Swap<T>(T[] array, int i1, int i2) {
        (array[i1], array[i2]) = (array[i2], array[i1]);
    }
}

/// <summary>Scramble Array using a dictionary</summary>
public class Scrambler2
{
    #region Fields / Properties
    /// <summary>Scramble Dictionary (Must be even number length)</summary>
    public uint[] Dictionary { get; set; }
    #endregion

    #region Constructors
    /// <summary>Scramble Array using a seed</summary>
    public Scrambler2(int length = 50) {
        Dictionary = GenerateDictionary(length);
    }
    /// <summary>Scramble Array using a seed</summary>
    public Scrambler2(uint[] dictionary) {
        Dictionary = dictionary;
    }
    #endregion

    /// <summary>Generate a new dictionary</summary>
    public uint[] GenerateDictionary(int len) {
        len *= 2;
        var blen = len * 4;
        var bArr = Encrypter.CreateRandomBytes(blen);
        var rval = new uint[len];
        Buffer.BlockCopy(bArr, 0, rval, 0, blen);
        Dictionary = rval;
        return Dictionary;
    }

    /// <summary>Scramble Array using a seed</summary>
    public T[] Scramble<T>(IEnumerable<T> values) {
        var array = values.ToArray();
        if (array.Length <= 1) return array;
        for (var i = 0; i < Dictionary.Length / 2; i++) Swap(array, Dictionary[i], Dictionary[^(i + 1)]);
        return array;
    }
    /// <summary>Unscramble Array using a seed</summary>
    public T[] Unscramble<T>(IEnumerable<T> values) {
        var array = values.ToArray();
        if (array.Length <= 1) return array;
        for (var i = (Dictionary.Length / 2) - 1; i >= 0; i--) Swap(array, Dictionary[^(i + 1)], Dictionary[i]);
        return array;
    }

    private static void Swap<T>(T[] array, uint i1, uint i2) {
        i1 %= (uint)array.Length;
        i2 %= (uint)array.Length;
        (array[i1], array[i2]) = (array[i2], array[i1]);
    }
}