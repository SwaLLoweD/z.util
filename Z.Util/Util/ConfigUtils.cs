﻿using System;
using System.Collections.Generic;
/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System.Configuration;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml;
using Z.Extensions;

namespace Z.Util
{
    /// <summary>Configuration Utilities</summary>
    public partial class ConfigUtils
    {
        /// <summary>Protect configuration section</summary>
        public static bool AppConfigProtect(bool protect = true, string sectionName = "connectionStrings", Configuration config = null, string provider = "RsaProtectedConfigurationProvider", bool save = true)
        {
            // Define the Dpapi provider name.
            //string strProvider = "DataProtectionConfigurationProvider";

            ConfigurationSection section = null;
            config = config ?? ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (config == null) return false;
            bool blnChanged = false;

            section = config.GetSection(sectionName);

            if (section == null || section.ElementInformation.IsLocked || section.SectionInformation.IsLocked) return false;

            if (protect && !section.SectionInformation.IsProtected) {
                blnChanged = true;
                section.SectionInformation.ProtectSection(provider);
            }
            else if (!protect && section.SectionInformation.IsProtected) {
                blnChanged = true;
                section.SectionInformation.UnprotectSection();
            }

            if (blnChanged && save) {
                section.SectionInformation.ForceSave = true;
                config.Save();
            }
            return blnChanged;
        }
        /// <summary>Fills config values to object with matching and convertible fields or properties</summary>
        public static void AppConfigToClass<T>(T obj = null) where T : class
        {
            var type = typeof(T);
            obj = obj ?? type.CreateInstance<T>();
            var settings = System.Configuration.ConfigurationManager.AppSettings;
            foreach (string key in settings) {
                var value = settings[key];
                keyValueToProperty<T>(obj, key, value);
            }
        }
        /// <summary>Fills config values to object with matching and convertible fields or properties</summary>
        public static void XmlConfigToClass<T>(string xmlFile, T obj = null) where T : class
        {

            var xmlDoc = new XmlDocument();
            XmlNode rootNode = null;
            try {
                xmlDoc.Load(xmlFile);
                rootNode = xmlDoc.DocumentElement;
            }
            catch { }
            if (rootNode == null) return;

            var type = typeof(T);
            obj = obj ?? type.CreateInstance<T>();

            var settings = rootNode.ChildNodes;
            foreach (XmlNode node in settings) {
                var key = node.Name;
                var value = node.InnerText;
                keyValueToProperty<T>(obj, key, value);
            }
        }

        private static bool keyValueToProperty<T>(T obj, string key, string value) where T : class
        {
            var type = typeof(T);
            var searchFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy;
            MemberInfo member = null;
            var pi = type.GetProperty(key, searchFlags);
            if (pi != null && pi.CanWrite) member = pi;
            else member = type.GetField(key, searchFlags);
            if (member == null) return false;
            var valueType = member.GetMemberType();
            try {
                if (valueType.IsGenericType && valueType.GetGenericTypeDefinition() == typeof(System.Collections.Generic.IList<>)) {
                    var valueCol = member.GetValue(member) as System.Collections.IList;
                    var valItems = value.Split(',');
                    var typeCol = valueType.GetGenericArguments()[0];
                    foreach (var valItem in valItems) {
                        var convertedVal = ZConverter.To(valItem.Trim(), typeCol);
                        if (convertedVal == null) continue;
                        valueCol.Add(convertedVal);
                    }
                    member.SetValue(type, valueCol);
                }
                else {
                    var convertedValue = ZConverter.To(value, valueType);
                    member.SetValue(type, convertedValue);
                }
            }
            catch {
                return false;
            }
            return true;
        }
        /// <summary>Write a property value to a custom xml file</summary>
        public static bool WritePropertyToXml<T, U>(string xmlFile, T obj, Expression<Func<T, U>> property)
        {
            IList<string> values = new List<string>();
            string key = null;
            try {
                var type = typeof(T);
                var member = Fnc.LambdaProperty(property);
                key = member.Name;

                var propValue = member.GetValue(obj);
                var valueType = member.GetMemberType();
                if (valueType.IsGenericType && valueType.GetGenericTypeDefinition() == typeof(System.Collections.Generic.IList<>)) {
                    var valueCol = member.GetValue(member) as System.Collections.IList;
                    var typeCol = valueType.GetGenericArguments()[0];
                    foreach (var valItem in valueCol) {
                        try {
                            var convertedVal = ZConverter.To(valItem, typeof(string));
                            if (convertedVal == null) continue;
                            values.Add(convertedVal as string);
                        }
                        catch {
                            continue;
                        }
                    }
                }
                else {
                    var convertedValue = ZConverter.To(propValue, typeof(string));
                    values.Add(convertedValue as string);
                }
            }
            catch {
                return false;
            }

            var value = values.To().CSV();
            return WriteValueToXml(xmlFile, key, value);
        }
        /// <summary>Write a key-value combination to a custom xml file</summary>
        public static bool WriteValueToXml(string xmlFile, string key, string value)
        {
            try {
                XmlDocument xml = new XmlDocument();
                XmlNode root = null;
                //XmlNode settings = null;
                XmlNode node = null;
                if (!System.IO.File.Exists(xmlFile)) {
                    root = xml.CreateChildNode("settings");
                    //settings = root.CreateChildNode("settings");
                    node = root.CreateChildNode(key);
                }
                else {
                    xml.Load(xmlFile);
                    root = xml.DocumentElement;
                    //settings = root.ChildNodes[0];
                    var nodes = xml.GetElementsByTagName(key);
                    node = (nodes.Count <= 0) ? root.CreateChildNode(key) : nodes[0];
                }

                if (value != null) node.InnerText = value;
                else root.RemoveChild(node);
                xml.Save(xmlFile);
            }
            catch {
                return false;
            }
            return true;
        }
    }
}