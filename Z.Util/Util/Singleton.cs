﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;

namespace Z.Util;

/// <summary>Singleton implementation</summary>
[Serializable]
public sealed class Singleton
{
    #region Constants / Static Fields
    private static Singleton _instance = null!;
    #endregion

    #region Fields / Properties
    /// <summary>Get instance</summary>
    public static Singleton Instance {
        get {
            return _instance ??= new Singleton();
        }
    }
    #endregion

    #region Constructors
    private Singleton() { }
    #endregion
}

/// <summary>Singleton implementation</summary>
[Serializable]
public static class Singleton<T> where T : class
{
    #region Constants / Static Fields
    private static T instance = null!;
    private static readonly object initLock = new();
    #endregion

    #region Fields / Properties
    /// <summary>Get instance</summary>
    public static T Instance {
        get {
            if (instance == null) CreateInstance();
            return instance!;
        }
    }
    #endregion

    private static void CreateInstance() {
        lock (initLock) {
            if (instance == null) {
                var t = typeof(T);

                // Ensure there are no public constructors...
                var ctors = t.GetConstructors();
                if (ctors.Length > 0) throw new InvalidOperationException(string.Format("{0} should not have any accessible ctor methods", t.Name));

                // Create an instance via the private constructor
                instance = (T)(Activator.CreateInstance(t, true) ?? throw new NullReferenceException($"Could not create object of type {t.Name}"));
            }
        }
    }
}