﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Xml;
using Z.Extensions;
using Z.Serialization;

namespace Z.Util;

//READ WRITE OPERATIONS ARE THE SAME WITH EXTBINARYREADERWRITER() and EXTSTREAM.. APPLY FIXES THERE TOO..
/// <summary>Custom Converter Implementation</summary>
[Serializable]
public class ZConverter {
    #region Constants / Static Fields
    /// <summary>Custom Converter Index</summary>
    protected readonly static Lazy<IDictionary<string, Func<object?, IFormatProvider?, object?>>> customConverters = new(InitCustomConverters, true);
    #endregion

    #region To
    /// <summary>Convert the input</summary>
    public static object? To(object? value, Type type, object? defaultValue = null, IFormatProvider? format = null) {
        if (type == null) return defaultValue;
        var rval = defaultValue;
        if (rval == null) {
            try {
                rval = type.GetDefault();
            } catch { }
        }

        // if (type.IsGenericType) {
        //     var genericTypeDefinition = type.GetGenericTypeDefinition();
        //     if (genericTypeDefinition == typeof(Nullable<>)) {
        //         if (value == null) return rval;
        //         type = type.GetGenericArguments()[0]; //Convert to notNullable original version instead
        //     }
        // }
        if (type.Is().Nullable(out var underLyingType)) {
            if (value == null) return rval;
            if (underLyingType != null) type = underLyingType; //Convert to notNullable original version instead
        }
        if (TryBuiltinConvert(value, type, ref rval, format)) return rval;
        if (TryCast(value, type, ref rval)) return rval;
        if (TryTypeDescriptor(value, type, ref rval)) return rval;
        if (TryCustom(value, type, ref rval, format)) return rval;
        return rval;
    }
    #endregion

    #region CanConvert
    /// <summary>Tests if conversion can be successful (Has heavy resource usage, Better to directly convert)</summary>
    public static bool CanConvert(object? value, Type type) {
        if (type == null) return true;
        object? rval = null;
        try {
            rval = type.GetDefault();
        } catch { }
        // Nullable type.
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)) {
            if (value == null) return true;

            type = type.GetGenericArguments()[0]; //Convert to notNullable original version instead
        }

        if (TryBuiltinConvert(value, type, ref rval)) return true;
        if (TryCast(value, type, ref rval)) return true;
        if (TryTypeDescriptor(value, type, ref rval)) return true;
        if (TryCustom(value, type, ref rval)) return true;
        return false;
    }
    #endregion

    #region AddConverter
    /// <summary>Add a custom converter function</summary>
    public static void AddConverter<FromType, ToType>(Func<object?, IFormatProvider?, object?> func) {
        if (func == null) return;
        InitCustomConverters();
        customConverters.Value[CreateId<FromType, ToType>()] = func;
    }
    #endregion

    #region Nested type: ByteConvertible
    /// <summary>ByteArray Converters</summary>
    public class ByteConvertible {
        #region Fields / Properties
        /// <summary>Length of the Byte Array Representation Data (-1 means unknown length)</summary>
        public int Length;
        /// <summary>Reader</summary>
        public ReadDelegate Reader;
        /// <summary>Corresponding Type</summary>
        public Type Type;
        /// <summary>Writer</summary>
        public WriteDelegate Writer;
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ByteConvertible(Type type, ReadDelegate read, WriteDelegate write, int length) {
            Type = type;
            Reader = read;
            Writer = write;
            Length = length;
        }
        #endregion
        #region Delegates
        /// <summary>Reader</summary>
        public delegate object ReadDelegate(ReadOnlySpan<byte> span, Type t);
        /// <summary>Writer</summary>
        public delegate ReadOnlySpan<byte> WriteDelegate(object o);
        #endregion
    }
    #endregion

    #region LookupTable for Byte conversion
    private static readonly Lazy<IDictionary<Type, ByteConvertible>> tblByteLookup = new(InitByteLookupTable, true);
    /// <summary>ByteArray Converters Lookup Table per Type</summary>
    public static IDictionary<Type, ByteConvertible> ByteConverterLookup => tblByteLookup.Value;

    private static readonly Lazy<IList<ByteConvertible>> tblByteCastable = new(InitByteCastableTable, true);

    private static IDictionary<Type, ByteConvertible> InitByteLookupTable() => new Dictionary<Type, ByteConvertible> {
                { typeof(bool), new ByteConvertible(typeof(bool), (r, _) => r[0] != 0, o => new byte[1] { (byte)((bool)o ? 1 : 0) }, 1) },
                { typeof(byte), new ByteConvertible(typeof(byte), (r, _) => r[0], o => new byte[1] { (byte)o }, 1) },
                { typeof(sbyte), new ByteConvertible(typeof(sbyte), (r, _) => (sbyte)r[0], o => new byte[1] { (byte)(sbyte)o }, 1) },
                { typeof(short), new ByteConvertible(typeof(short), (r, _) => BitConverter.ToInt16(r), o => BitConverter.GetBytes((short)o), 2) },
                { typeof(ushort), new ByteConvertible(typeof(ushort), (r, _) => BitConverter.ToUInt16(r), o => BitConverter.GetBytes((ushort)o), 2) },
                { typeof(int), new ByteConvertible(typeof(int), (r, _) => BitConverter.ToInt32(r), o => BitConverter.GetBytes((int)o), 4) },
                { typeof(uint), new ByteConvertible(typeof(uint), (r, _) => BitConverter.ToUInt32(r), o => BitConverter.GetBytes((uint)o), 4) },
                { typeof(long), new ByteConvertible(typeof(long), (r, _) => BitConverter.ToInt64(r), o => BitConverter.GetBytes((long)o), 8) },
                { typeof(ulong), new ByteConvertible(typeof(ulong), (r, _) => BitConverter.ToUInt16(r), o => BitConverter.GetBytes((ulong)o), 8) },
                { typeof(double), new ByteConvertible(typeof(double), (r, _) => BitConverter.ToDouble(r), o => BitConverter.GetBytes((double)o), 8) },
                { typeof(float), new ByteConvertible(typeof(float), (r, _) => BitConverter.ToSingle(r), o => BitConverter.GetBytes((float)o), 4) },
                { typeof(char), new ByteConvertible(typeof(char), (r, _) => BitConverter.ToChar(r), o => BitConverter.GetBytes((char)o), 2) },
                { typeof(string), new ByteConvertible(typeof(string), (r, _) => r.ToArray().To().String.AsUtf8, o => ((string)o).To().ByteArray.AsUtf8, -1) },
                {
                    typeof(DateTime),
                    new ByteConvertible(typeof(DateTime), (r, t) => DateTime.FromBinary((long)ZConverter.tblByteLookup.Value[typeof(long)].Reader(r, t)),
                    o => BitConverter.GetBytes(((DateTime)o).ToUniversalTime().ToBinary()), 8)
                },
                {
                    typeof(TimeSpan),
                    new ByteConvertible(typeof(TimeSpan), (r, t) => TimeSpan.FromMilliseconds((double)ZConverter.tblByteLookup.Value[typeof(double)].Reader(r, t)),
                    o => BitConverter.GetBytes(((TimeSpan)o).TotalMilliseconds), 8)
                },
                { typeof(byte[]), new ByteConvertible(typeof(byte[]), (r, _) => r.ToArray(), o => (byte[])o, -1) },
                { typeof(char[]), new ByteConvertible(typeof(char[]), (r, _) => Encoding.UTF8.GetChars(r.ToArray()), o => Encoding.UTF8.GetBytes((char[])o), -1) },
                {
                    typeof(decimal),
                    new ByteConvertible(typeof(decimal),
                (r, _) => new decimal(
                    BitConverter.ToInt32(r),
                    BitConverter.ToInt32(r[4..]),
                    BitConverter.ToInt32(r[8..]),
                    r[15]== 128, //DecimalSignBit
                    r[14]),
                w => {
                    var rval = new byte[16];
                    var n = decimal.GetBits((decimal)w);
                    Buffer.BlockCopy(BitConverter.GetBytes(n[0]), 0, rval, 0, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(n[1]), 0, rval, 4, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(n[2]), 0, rval, 8, 4);
                    var l = BitConverter.GetBytes(n[3]);
                    rval[15] = l[3];
                    rval[14] = l[2];
                    return rval;
                }, 16)
                },

                {
                    typeof(BigInteger),
                    new ByteConvertible(typeof(BigInteger),
                (r, _) => r.IsEmpty ? BigInteger.Zero : new BigInteger(r),
                w => ((BigInteger)w).ToByteArray(), -1)
                },
                { typeof(Guid), new ByteConvertible(typeof(Guid), (r, _) => new Guid(r), w => ((Guid)w).ToByteArray(), 16) },
                {
                    typeof(IPAddress),
                    new ByteConvertible(typeof(IPAddress),
                (r, _) => r.IsEmpty ? IPAddress.None : new IPAddress(r),
                w => ((IPAddress)w).GetAddressBytes(), -1)
                },
                {
                    typeof(IPEndPoint),
                    new ByteConvertible(typeof(IPEndPoint),
                (r, _) => {
                    var port = BitConverter.ToInt32(r);
                    var len = BitConverter.ToInt32(r[4..]);
                    var ipBytes = new byte[len];
                    var ip = IPAddress.None;
                    if (len > 0) {
                        r[8..].CopyTo(ipBytes.AsSpan(0, len));
                        ip = new IPAddress(ipBytes);
                    }
                    return new IPEndPoint(ip, port);
                },
                w => {
                    var ipe = (IPEndPoint)w;
                    var ipBytes = ipe.Address.GetAddressBytes();
                    var rval = new byte[8 + ipBytes.Length];
                    Buffer.BlockCopy(BitConverter.GetBytes(ipe.Port), 0, rval, 0, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(ipBytes.Length), 0, rval, 4, 8);
                    Buffer.BlockCopy(ipBytes, 0, rval, 8, ipBytes.Length);
                    return rval;
                }, -1)
                }
        };
    private static IList<ByteConvertible> InitByteCastableTable() => new List<ByteConvertible> {
                new ByteConvertible(typeof(IEnumerable<byte>),
                (b, type) => {
                    if (type == null) type = typeof(IEnumerable<byte>);
                    if (type.Equals<List<byte>>())
                        return b.ToArray().To().List();
                    else if (type.Equals<HashSet<byte>>()) return b.ToArray().To().HashSet();
                    throw new NullReferenceException();//return type.DefaultValue();
                },
                w => ((IEnumerable<byte>) w).To().Array(), -1),

                new ByteConvertible(typeof(Enum),
                (b, type) => {
                    if (type == null) type = typeof(Enum);
                    var rval = type.GetDefault();
                    var t = Enum.GetUnderlyingType(type);
                    string? enumVal = null;
                    if (tblByteLookup.Value.ContainsKey(t)) {
                        var result = tblByteLookup.Value[t].Reader(b, t);
                        if (t == typeof(string))
                            enumVal = (string)result;
                        else
                            enumVal = Convert.ToString(result);
                    } else {
                        enumVal = tblByteLookup.Value[typeof(int)].Reader(b, typeof(int)).ToString();
                    }
                    //if (t.Equals<int>()) enumVal = get<int>(ref index).ToString();
                    //else if (t.Equals<short>()) enumVal = get<short>(ref index).ToString();
                    //else if (t.Equals<long>()) enumVal = get<long>(ref index).ToString();
                    //else if (t.Equals<byte>()) enumVal = get<byte>(ref index).ToString();
                    //else if (t.Equals<uint>()) enumVal = get<uint>(ref index).ToString();
                    //else if (t.Equals<ushort>()) enumVal = get<ushort>(ref index).ToString();
                    //else if (t.Equals<ulong>()) enumVal = get<ulong>(ref index).ToString();
                    //else if (t.Equals<sbyte>()) enumVal = get<sbyte>(ref index).ToString();
                    //else enumVal = get<int>(ref index).ToString();
                    if (enumVal == null) throw new NullReferenceException();
                    try {
                        rval = Enum.Parse(type, enumVal);
                    } catch { }
                    if (rval == null) throw new NullReferenceException();
                    return rval;
                },
                w => {
                    var en = (Enum)w;
                    var t = Enum.GetUnderlyingType(en.GetType());
                    if (tblByteLookup.Value.ContainsKey(t))
                        return tblByteLookup.Value[t].Writer(en);
                    else
                        return tblByteLookup.Value[typeof(int)].Writer(en);
                    //if (t == typeof(int)) add(ref index, Convert.ToInt32(b));
                    //else if (t == typeof(short)) add(ref index, Convert.ToInt16(b));
                    //else if (t == typeof(long)) add(ref index, Convert.ToInt64(b));
                    //else if (t == typeof(byte)) add(ref index, Convert.ToByte(b));
                    //else if (t == typeof(uint)) add(ref index, Convert.ToUInt32(b));
                    //else if (t == typeof(ushort)) add(ref index, Convert.ToUInt16(b));
                    //else if (t == typeof(ulong)) add(ref index, Convert.ToUInt64(b));
                    //else if (t == typeof(sbyte)) add(ref index, Convert.ToSByte(b));
                    //else add(ref index, Convert.ToInt32(b));
                }, -1)
            };
    #endregion

    #region Switch based lookup (Disabled)
    // private ByteConvertible getByteConvertible(Type inType) {
    //     switch (true) {
    //         case bool _ when inType == typeof(bool): return new ByteConvertible(typeof(bool), (r, t) => r[0] != 0, (o) => new byte[1] {(byte) (((bool) o) ? 1 : 0)}, 1);
    //         case bool _ when inType == typeof(byte): return new ByteConvertible(typeof(byte), (r, t) => r[0], (o) => new byte[1] {(byte) o}, 1);
    //         case bool _ when inType == typeof(sbyte): return new ByteConvertible(typeof(sbyte), (r, t) => (sbyte) r[0], (o) => new byte[1] {((byte) (sbyte) o)}, 1);
    //         case bool _ when inType == typeof(short): return new ByteConvertible(typeof(short), (r, t) => BitConverter.ToInt16(r, 0), (o) => BitConverter.GetBytes((short) o), 2);
    //         case bool _ when inType == typeof(ushort): return new ByteConvertible(typeof(ushort), (r, t) => BitConverter.ToUInt16(r, 0), (o) => BitConverter.GetBytes((ushort) o), 2);
    //         case bool _ when inType == typeof(int): return new ByteConvertible(typeof(int), (r, t) => BitConverter.ToInt32(r, 0), (o) => BitConverter.GetBytes((int) o), 4);
    //         case bool _ when inType == typeof(uint): return new ByteConvertible(typeof(uint), (r, t) => BitConverter.ToUInt32(r, 0), (o) => BitConverter.GetBytes((uint) o), 4);
    //         case bool _ when inType == typeof(long): return new ByteConvertible(typeof(long), (r, t) => BitConverter.ToInt64(r, 0), (o) => BitConverter.GetBytes((long) o), 8);
    //         case bool _ when inType == typeof(ulong): return new ByteConvertible(typeof(ulong), (r, t) => BitConverter.ToUInt16(r, 0), (o) => BitConverter.GetBytes((ulong) o), 8);
    //         case bool _ when inType == typeof(double): return new ByteConvertible(typeof(double), (r, t) => BitConverter.ToDouble(r, 0), (o) => BitConverter.GetBytes((double) o), 8);
    //         case bool _ when inType == typeof(float): return new ByteConvertible(typeof(float), (r, t) => BitConverter.ToSingle(r, 0), (o) => BitConverter.GetBytes((float) o), 4);
    //         case bool _ when inType == typeof(char): return new ByteConvertible(typeof(char), (r, t) => BitConverter.ToChar(r, 0), (o) => BitConverter.GetBytes((char) o), 2);
    //         case bool _ when inType == typeof(string): return new ByteConvertible(typeof(string), (r, t) => r.To().String.AsUtf8, (o) => ((string) o).To().ByteArray.AsUtf8, -1);
    //         case bool _ when inType == typeof(DateTime):
    //             return new ByteConvertible(typeof(DateTime), (r, t) => DateTime.FromBinary((long) getByteConvertible(typeof(long)).Reader(r, t)),
    //                 (o) => BitConverter.GetBytes(((DateTime) o).ToUniversalTime().ToBinary()), 8);
    //         case bool _ when inType == typeof(TimeSpan):
    //             return new ByteConvertible(typeof(TimeSpan), (r, t) => TimeSpan.FromMilliseconds((double) getByteConvertible(typeof(double)).Reader(r, t)),
    //                 (o) => BitConverter.GetBytes((((TimeSpan) o).TotalMilliseconds)), 8);
    //         case bool _ when inType == typeof(byte[]): return new ByteConvertible(typeof(byte[]), (r, t) => r, (o) => (byte[]) o, -1);
    //         case bool _ when inType == typeof(char[]): return new ByteConvertible(typeof(char[]), (r, t) => System.Text.UTF8Encoding.UTF8.GetChars(r), (o) => System.Text.UTF8Encoding.UTF8.GetBytes((char[]) o), -1);
    //         case bool _ when inType == typeof(decimal):
    //             return new ByteConvertible(typeof(decimal),
    //                 (r, t) => {
    //                     return new decimal(
    //                         BitConverter.ToInt32(r, 0),
    //                         BitConverter.ToInt32(r, 4),
    //                         BitConverter.ToInt32(r, 8),
    //                         r[15] == 128, //DecimalSignBit
    //                         r[14]);
    //                 },
    //                 (w) => {
    //                     var rval = new byte[16];
    //                     var n = Decimal.GetBits((decimal) w);
    //                     Buffer.BlockCopy(BitConverter.GetBytes(n[0]), 0, rval, 0, 4);
    //                     Buffer.BlockCopy(BitConverter.GetBytes(n[1]), 0, rval, 4, 4);
    //                     Buffer.BlockCopy(BitConverter.GetBytes(n[2]), 0, rval, 8, 4);
    //                     var l = BitConverter.GetBytes(n[3]);
    //                     rval[15] = l[3];
    //                     rval[14] = l[2];
    //                     return rval;
    //                 }, 16);
    //
    //         case bool _ when inType == typeof(System.Numerics.BigInteger):
    //             return new ByteConvertible(typeof(System.Numerics.BigInteger),
    //                 (r, t) => ((r == null) ? System.Numerics.BigInteger.Zero : new System.Numerics.BigInteger(r)),
    //                 (w) => ((System.Numerics.BigInteger) w).ToByteArray(), -1);
    //         case bool _ when inType == typeof(Guid): return new ByteConvertible(typeof(Guid), (r, t) => new Guid(r), (w) => ((Guid) w).ToByteArray(), 16);
    //         case bool _ when inType == typeof(System.Net.IPAddress):
    //             return new ByteConvertible(typeof(System.Net.IPAddress),
    //                 (r, t) => ((r == null) ? System.Net.IPAddress.None : new System.Net.IPAddress(r)),
    //                 (w) => ((System.Net.IPAddress) w).GetAddressBytes(), -1);
    //         case bool _ when inType == typeof(System.Net.IPEndPoint):
    //             return new ByteConvertible(typeof(System.Net.IPEndPoint),
    //                 (r, t) => {
    //                     var port = BitConverter.ToInt32(r, 0);
    //                     var len = BitConverter.ToInt32(r, 4);
    //                     var ipBytes = new byte[len];
    //                     var ip = System.Net.IPAddress.None;
    //                     if (len > 0) {
    //                         Buffer.BlockCopy(r, 8, ipBytes, 0, len);
    //                         ip = new System.Net.IPAddress(ipBytes);
    //                     }
    //                     return new System.Net.IPEndPoint(ip, port);
    //                 },
    //                 (w) => {
    //                     var ipe = (System.Net.IPEndPoint) w;
    //                     var ipBytes = ipe.Address.GetAddressBytes();
    //                     var rval = new byte[8 + ipBytes.Length];
    //                     Buffer.BlockCopy(BitConverter.GetBytes(ipe.Port), 0, rval, 0, 4);
    //                     Buffer.BlockCopy(BitConverter.GetBytes(ipBytes.Length), 0, rval, 4, 8);
    //                     Buffer.BlockCopy(ipBytes, 0, rval, 8, ipBytes.Length);
    //                     return rval;
    //                 }, -1);
    //         default: return null;
    //     }
    // }
    // private ByteConvertible getByteCastable(Type inType) {
    //     switch (true) {
    //         case bool _ when inType == typeof(IEnumerable<byte>):
    //             return new ByteConvertible(typeof(IEnumerable<byte>),
    //                 (b, type) => {
    //                     if (type == null) type = typeof(IEnumerable<byte>);
    //                     var rval = type.DefaultValue();
    //                     if (type.Equals<List<byte>>()) rval = (object) b.To().List();
    //                     else if (type.Equals<HashSet<byte>>()) rval = (object) b.To().HashSet();
    //                     return rval;
    //                 },
    //                 (w) => {
    //                     var b = (w as IEnumerable<byte>).To().Array();
    //                     return b;
    //                 }, -1);
    //         case bool _ when inType == typeof(Enum):
    //             return new ByteConvertible(typeof(Enum),
    //                 (b, type) => {
    //                     if (type == null) type = typeof(Enum);
    //                     var rval = type.DefaultValue();
    //                     var t = Enum.GetUnderlyingType(type);
    //                     string enumVal = null;
    //                     var typeConverter = getByteConvertible(t);
    //                     if (typeConverter != null) {
    //                         object result = typeConverter.Reader(b, t);
    //                         if (t == typeof(string)) enumVal = (string) result;
    //                         else enumVal = Convert.ToString(result);
    //                     }
    //                     else enumVal = getByteConvertible(typeof(int)).Reader(b, typeof(int)).ToString();
    //                     //if (t.Equals<int>()) enumVal = get<int>(ref index).ToString();
    //                     //else if (t.Equals<short>()) enumVal = get<short>(ref index).ToString();
    //                     //else if (t.Equals<long>()) enumVal = get<long>(ref index).ToString();
    //                     //else if (t.Equals<byte>()) enumVal = get<byte>(ref index).ToString();
    //                     //else if (t.Equals<uint>()) enumVal = get<uint>(ref index).ToString();
    //                     //else if (t.Equals<ushort>()) enumVal = get<ushort>(ref index).ToString();
    //                     //else if (t.Equals<ulong>()) enumVal = get<ulong>(ref index).ToString();
    //                     //else if (t.Equals<sbyte>()) enumVal = get<sbyte>(ref index).ToString();
    //                     //else enumVal = get<int>(ref index).ToString();
    //                     try {
    //                         rval = Enum.Parse(type, enumVal);
    //                     }
    //                     catch { }
    //                     return rval;
    //                 },
    //                 (w) => {
    //                     var en = w as Enum;
    //                     Type t = Enum.GetUnderlyingType(en.GetType());
    //                     byte[] rval = null;
    //                     var typeConverter = getByteConvertible(t);
    //                     if (typeConverter != null) rval = typeConverter.Writer(en);
    //                     else rval = getByteConvertible(typeof(int)).Writer(en);
    //                     //if (t == typeof(int)) add(ref index, Convert.ToInt32(b));
    //                     //else if (t == typeof(short)) add(ref index, Convert.ToInt16(b));
    //                     //else if (t == typeof(long)) add(ref index, Convert.ToInt64(b));
    //                     //else if (t == typeof(byte)) add(ref index, Convert.ToByte(b));
    //                     //else if (t == typeof(uint)) add(ref index, Convert.ToUInt32(b));
    //                     //else if (t == typeof(ushort)) add(ref index, Convert.ToUInt16(b));
    //                     //else if (t == typeof(ulong)) add(ref index, Convert.ToUInt64(b));
    //                     //else if (t == typeof(sbyte)) add(ref index, Convert.ToSByte(b));
    //                     //else add(ref index, Convert.ToInt32(b));
    //                     return rval;
    //                 }, -1);
    //         default: return null;
    //     }
    // }
    #endregion

    #region tryConvert
    private static bool TryCast(object? value, Type type, ref object? rval) {
        if (value?.GetType().IsAssignableFrom(type) == true) {
            rval = value; //Can be cast to
            return true;
        }
        return false;
    }
    private static bool TryBuiltinConvert(object? value, Type type, ref object? rval, IFormatProvider? format = null) {
        if (type.GetTypeInfo().GetInterface<IConvertible>() == null) //If value is IConvertible
            return false;

        try {
            rval = format == null ? Convert.ChangeType(value, type) : Convert.ChangeType(value, type, format);
            return true;
        } catch { }

        return false;
    }
    private static bool TryTypeDescriptor(object? value, Type type, ref object? rval) {
        if (value?.GetType().IsAssignableFrom(type) != true) return false;

        try {
            if (value == null) return false;
            if (value.GetType() == type) {
                rval = value;
            } else {
                var converter = TypeDescriptor.GetConverter(value); //Get converter from source
                if (converter.CanConvertTo(type)) {
                    rval = converter.ConvertTo(value, type);
                    return true;
                }
                converter = TypeDescriptor.GetConverter(type); //Get converter from target
                if (converter.CanConvertFrom(value.GetType())) {
                    rval = converter.ConvertFrom(value);
                    return true;
                }
            }

            return false;
        } catch { }

        return false;
    }
    private static bool TryCustom(object? value, Type type, ref object? rval, IFormatProvider? format = null) {
        if (value == null) return false;
        var fromType = value.GetType();
        var conv = customConverters.Value.TryGetValue(CreateId(fromType, type));
        if (conv == null) return false;
        try {
            rval = conv(value, format);
            return true;
        } catch { }
        return false;
    }
    #endregion

    #region createId
    private static string CreateId<T, U>() => CreateId(typeof(T), typeof(U));
    private static string CreateId(Type t, Type u) => t.FullName + u.FullName;
    #endregion

    #region Custom Convert Implementation
    private static IDictionary<string, Func<object?, IFormatProvider?, object?>> InitCustomConverters() =>
        new Dictionary<string, Func<object?, IFormatProvider?, object?>> {
            [CreateId<string, IPAddress>()] = CStringToIpAddress,
            [CreateId<string, XmlDocument>()] = CStringToXmlDocument,
            [CreateId<string, Guid>()] = CStringToGuid,
            [CreateId<string, Uri>()] = CStringToUri,
            [CreateId<string, TimeSpan>()] = CStringToTimeSpan,

            [CreateId<IPAddress, string>()] = CObjectToString,
            [CreateId<XmlDocument, string>()] = CObjectToString,
            [CreateId<Guid, string>()] = CObjectToString,
            [CreateId<Uri, string>()] = CObjectToString
        };
    private static object? CObjectToString(object? value, IFormatProvider? format) => value?.ToString();
    private static object? CStringToIpAddress(object? value, IFormatProvider? format) => value is not string strValue ? null : IPAddress.Parse(strValue);
    private static object? CStringToXmlDocument(object? value, IFormatProvider? format) {
        if (value is not string valueStr) return null;
        var rval = new XmlDocument();
        rval.LoadXml(valueStr);
        return rval;
    }
    private static object? CStringToGuid(object? value, IFormatProvider? format) {
        var inStr = value as string;
        if (Guid.TryParse(inStr, out Guid rval)) return rval;
        return null;
    }
    private static object? CStringToUri(object? value, IFormatProvider? format) => value is not string strValue ? null : new Uri(strValue);
    private static object? CStringToTimeSpan(object? value, IFormatProvider? format) {
        var str = value as string;
        if (!string.IsNullOrEmpty(str) && TimeSpan.TryParse(str, out TimeSpan rval)) return rval;
        return null;
    }
    #endregion Custom Convert Implementation

    #region Static GetByteConverter
    /// <summary>Finds and Returns Appropriate Byte Converter for Type </summary>
    public static ByteConvertible? GetByteConverter<T>() => GetByteConverter(typeof(T));
    /// <summary>Finds and Returns Appropriate Byte Converter for Type </summary>
    public static ByteConvertible? GetByteConverter(Type type) {
        if (tblByteLookup.Value.TryGetValue(type, out var rval)) return rval;
        foreach (var c in tblByteCastable.Value) {
            if (type.IsAssignableTo(c.Type)) {
                if (type.GetTypeInfo().IsEnum) {
                    var uType = Enum.GetUnderlyingType(type);
                    var size = -1;
                    if (uType == typeof(int) || uType == typeof(uint))
                        size = 4;
                    else if (uType == typeof(byte) || uType == typeof(sbyte) || uType == typeof(bool))
                        size = 1;
                    else if (uType == typeof(short) || uType == typeof(ushort))
                        size = 2;
                    else if (uType == typeof(long) || uType == typeof(ulong)) size = 8;
                    c.Length = size;
                }
                return c;
            }
        }

        foreach (var l in tblByteLookup.Value)
            if (type.IsAssignableFrom(l.Key)) return l.Value;
        //throw new NotImplementedException("Binary Formatter is not available in this platform");

        try {
            //if (o != null && !o.GetType().IsSerializable) throw new ArgumentException("Type ({0}) is not Serializable".ToFormat(typeof(T).Name));
            return new ByteConvertible(type,
                //(r, t) => r.Deserialize().DotNetBinary(t),
                //(w) => w.Serialize().DotNetBinary(),
                (r, t) => {
                    //r.Deserialize().BoisBinary(t)
                    var bs = new ZBoisSerializer();
                    return bs.Deserialize(r.ToArray(), t, 0, r.Length) ?? throw new NullReferenceException();
                },
                w => {
                    //w.Serialize().BoisBinary();
                    using var ms = new MemoryStream();
                    var bs = new ZBoisSerializer();
                    bs.Serialize(w, type, ms);
                    return ms.ToArray();
                },
                -1);
        } catch { }
        return null;
    }
    #endregion
}