﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using Z.Extensions;
using Z.Util;

/* 
 * Salar BOIS (Binary Object Indexed Serialization)
 * by Salar Khalilzadeh
 * 
 * https://github.com/salarcode/Bois
 * Mozilla Public License v2
 */
namespace Z.Serialization
{
    /// <summary>
    ///     Salar.Bois serializer. Which provides binary serialization and deserialzation for .NET objects. BOIS stands
    ///     for 'Binary Object Indexed Serialization'.
    /// </summary>
    /// <Author>Salar Khalilzadeh</Author>
    public class ZBoisSerializer
    {
        //private static readonly DateTime UnixEpoch = new(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        #region Fields / Properties
        private readonly ZTypeCacheFlags cacheFlags;
        //private int _serializeDepth;

        /// <summary>Character encoding for strings.</summary>
        public Encoding Encoding { get; set; }
        #endregion

        #region Constructors
        /// <summary>Initializing a new instance of Bois serializar.</summary>
        public ZBoisSerializer(ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            cacheFlags = opts;
            Encoding = Encoding.UTF8;
        }
        #endregion

        /// <summary>Serializing an object to binary bois format.</summary>
        /// <param name="obj">The object to be serialized.</param>
        /// <param name="output">The output of the serialization in binary.</param>
        /// <typeparam name="T">The object type.</typeparam>
        public void Serialize<T>(T obj, Stream output) => Serialize(obj, typeof(T), output);

        /// <summary>Serializing an object to binary bois format.</summary>
        /// <param name="obj">The object to be serialized.</param>
        /// <param name="type">The objects' type.</param>
        /// <param name="output">The output of the serialization in binary.</param>
        public void Serialize(object? obj, Type type, Stream output) {
            if (obj == null) throw new ArgumentNullException(nameof(obj), "Object cannot be null.");
            //_serializeDepth = 0;
            var writer = new BinaryWriter(output, Encoding);
            var bionType = GetZValueInfo(type);
            WriteValue(writer, obj, bionType);
        }
        /// <summary>Deserilizing binary data to a new instance.</summary>
        /// <param name="objectData">The binary data.</param>
        /// <typeparam name="T">The object type.</typeparam>
        /// <returns>New instance of the deserialized data.</returns>
        public T? Deserialize<T>(Stream objectData) => (T?) Deserialize(objectData, typeof(T));
        /// <summary>Deserilizing binary data to a new instance.</summary>
        /// <param name="objectData">The binary data.</param>
        /// <param name="objectType">The object type.</param>
        /// <returns>New instance of the deserialized data.</returns>
        public object? Deserialize(Stream objectData, Type objectType) {
            var reader = new BinaryReader(objectData, Encoding);
            return ReadMember(reader, objectType);
        }

        /// <summary>Deserilizing binary data to a new instance.</summary>
        /// <param name="objectBuffer">The binary data.</param>
        /// <param name="index">The index in buffer at which the stream begins.</param>
        /// <param name="count">The length of the stream in bytes.</param>
        /// <typeparam name="T">The object type.</typeparam>
        /// <returns>New instance of the deserialized data.</returns>
        public T? Deserialize<T>(byte[] objectBuffer, int index, int count) => (T?) Deserialize(objectBuffer, typeof(T), index, count);

        /// <summary>Deserilizing binary data to a new instance.</summary>
        /// <param name="objectBuffer">The binary data.</param>
        /// <param name="type">The objects' type.</param>
        /// <param name="index">The index in buffer at which the stream begins.</param>
        /// <param name="count">The length of the stream in bytes.</param>
        /// <returns>New instance of the deserialized data.</returns>
        public object? Deserialize(byte[] objectBuffer, Type type, int index, int count) {
            using var mem = new MemoryStream(objectBuffer, index, count, false);
            var reader = new BinaryReader(mem, Encoding);
            return ReadMember(reader, type);
        }
        // private static long ConvertDateTimeToEpochTime(DateTime value) {
        //     var d = new DateTime(value.Ticks - (value.Ticks % 10000000L));
        //     return (long) (d - UnixEpoch).TotalSeconds;
        // }

        // private static DateTime ConvertToDateTimeFromEpoch(long seconds) => UnixEpoch.AddSeconds(seconds);

        private ZTypeInfo GetZTypeInfo(Type objType, bool useLambdaCache = false) =>
            ZTypeCache.GetTypeInfo(objType, useLambdaCache ? ZTypeCacheOptions.Lambda : ZTypeCacheOptions.Smart, cacheFlags);
        // private ZMemberInfo GetZMemberInfo(Type objType, bool useLambdaCache = false) =>
        //     ZTypeCache.GetMemberInfo(objType, useLambdaCache ? ZTypeCacheOptions.Lambda : ZTypeCacheOptions.Smart, nonPublic ? ZTypeCacheFlags.NonPublicWFields : ZTypeCacheFlags.Default);
        private ZValueInfo GetZValueInfo(Type objType, bool useLambdaCache = false) =>
             ZTypeCache.GetValueInfo(objType, useLambdaCache ? ZTypeCacheOptions.Lambda : ZTypeCacheOptions.Smart, cacheFlags);

        #region Serialization methods
        private void WriteObject(BinaryWriter writer, ZTypeInfo typeInfo, object? obj) {
            if (obj == null) {
                // null indicator
                WriteNullableType(writer, true);
                return;
            }
            if (typeInfo.IsNullable) //This is a nullable struct and is not null
                WriteNullableType(writer, false);

            var type = typeInfo.Type;
            if (type == null) {
                type = obj.GetType();
                typeInfo = GetZTypeInfo(type);
            }

            //_serializeDepth++;
            // Use this member info if avaiable. it is more accurate because it came from the object holder,
            // not the object itseld.

            // writing the members
            foreach (var m in typeInfo.Members) {
                var mem = m.Value;
                //Skip flag non-selected members
                if ((!cacheFlags.HasFlag(ZTypeCacheFlags.NonPublic) && !mem.IsPublic) || (!cacheFlags.HasFlag(ZTypeCacheFlags.Fields) && !mem.IsProperty))
                    continue;
                var value = mem.Getter(obj);
                WriteValue(writer, value, mem.Value);
            }

            //_serializeDepth--;
        }

        private static void WriteNullableType(BinaryWriter writer, bool isnull) => writer.WriteVar(isnull ? (byte) 1 : (byte) 0);

        /// <summary>Also called by root</summary>
        private void WriteValue(BinaryWriter writer, object? value, ZValueInfo? bionType = null) {
            if (value == null && bionType == null) { //No typeinfo is known, write null
                WriteNullableType(writer, true);
                return;
            }

            bionType ??= GetZValueInfo(value!.GetType());

            if (value == null) {
                if (bionType.IsNullable) WriteNullableType(writer, true);
                else throw new NullReferenceException($"Object marked as not nullable is null ({bionType.Type.FullName})");
                return;
            }
            if (bionType.IsNullable) WriteNullableType(writer, false);

            switch (bionType.KnownType) {
                case ZTypeCacheKnownTypes.Unknown:
                    if (bionType.ContainerType != null) {
                        WriteObject(writer, bionType.ContainerType, value);
                    } else if (bionType.IsDictionary) {
                        WriteDictionary(writer, (IDictionary)value);
                    } else if (bionType.IsCollection || bionType.IsArray || bionType.IsEnumerable) {
                        if (bionType.IsGeneric)
                            WriteGenericList(writer, (IEnumerable)value);
                        else
                            WriteArray(writer, value, bionType);
                    }
                    break;
                case ZTypeCacheKnownTypes.Int16: writer.WriteVar((short) value); break;
                case ZTypeCacheKnownTypes.UInt16: writer.WriteVar((ushort) value); break;
                case ZTypeCacheKnownTypes.Int32: writer.WriteVar((int) value); break;
                case ZTypeCacheKnownTypes.Int64: writer.WriteVar((long) value); break;
                case ZTypeCacheKnownTypes.UInt64: writer.WriteVar((ulong) value); break;
                case ZTypeCacheKnownTypes.UInt32: writer.WriteVar((uint) value); break;
                case ZTypeCacheKnownTypes.Double: writer.WriteVar((double) value); break;
                case ZTypeCacheKnownTypes.Decimal: writer.WriteVar((decimal) value); break;
                case ZTypeCacheKnownTypes.Single: writer.WriteVar((float) value); break;
                case ZTypeCacheKnownTypes.Byte: writer.WriteVar((byte) value); break;
                case ZTypeCacheKnownTypes.SByte: writer.Write((sbyte) value); break;
                case ZTypeCacheKnownTypes.ByteArray:
                    var bytes = (byte[])value;
                    writer.WriteVar(bytes.Length);
                    writer.Write(bytes);
                    break;
                case ZTypeCacheKnownTypes.String: WriteString(writer, (string)value); break;
                case ZTypeCacheKnownTypes.Char: writer.WriteVar((ushort) (char) value); break;
                case ZTypeCacheKnownTypes.Bool: writer.WriteVar((byte) ((bool) value ? 1 : 0)); break;
                case ZTypeCacheKnownTypes.Enum: WriteEnum(writer, (Enum) value); break;
                case ZTypeCacheKnownTypes.DateTime: WriteDateTime(writer, (DateTime) value); break;
                case ZTypeCacheKnownTypes.DateTimeOffset: WriteDateTimeOffset(writer, (DateTimeOffset) value); break;
                case ZTypeCacheKnownTypes.TimeSpan: writer.WriteVar(((TimeSpan) value).Ticks); break;
                case ZTypeCacheKnownTypes.DataSet: WriteDataset(writer, (DataSet)value); break;
                case ZTypeCacheKnownTypes.DataTable: WriteDataTable(writer, (DataTable)value); break;
                case ZTypeCacheKnownTypes.NameValueColl: WriteNameValueCollection(writer, (NameValueCollection)value); break;
                case ZTypeCacheKnownTypes.Color: writer.WriteVar(((Color) value).ToArgb()); break;
                case ZTypeCacheKnownTypes.Version: WriteString(writer, ((Version)value).ToString()); break;
                case ZTypeCacheKnownTypes.DbNull: break; // Do not write anything, it is already written as Nullable object. //WriteNullableType(true);
                case ZTypeCacheKnownTypes.Guid: WriteGuid(writer, (Guid) value); break;
                case ZTypeCacheKnownTypes.Uri: WriteString(writer, ((Uri)value).OriginalString); break;
                default: throw new ArgumentException("Type is not known");
            }
        }

        private static string GetXmlSchema(DataTable dt) {
            using var writer = new StringWriter();
            dt.WriteXmlSchema(writer);
            return writer.ToString();
        }

        private void WriteDataset(BinaryWriter writer, DataSet ds) {
            // Int32
            writer.WriteVar(ds.Tables.Count);
            foreach (DataTable table in ds.Tables) WriteDataTable(writer, table);
        }
        private void WriteDataTable(BinaryWriter writer, DataTable table) {
            if (string.IsNullOrEmpty(table.TableName)) table.TableName = "tbl_" + DateTime.Now.Ticks.GetHashCode();
            WriteString(writer, GetXmlSchema(table));

            // Int32
            writer.WriteVar(table.Rows.Count);

            var colsCount = table.Columns.Count;
            foreach (DataRow row in table.Rows) WriteDataRow(writer, row, colsCount);
        }
        private void WriteDataRow(BinaryWriter writer, DataRow row, int columnCount) {
            var values = new Dictionary<int, object>();
            for (var i = 0; i < columnCount; i++) {
                var val = row[i];
                if (val != null && !Convert.IsDBNull(val)) values.Add(i, val);
            }

            // count of non-null columns
            // Int32
            writer.WriteVar(values.Count);

            foreach (var value in values) {
                // Int32
                writer.WriteVar(value.Key);

                WriteValue(writer, value.Value);
            }
        }

        //Z Fixed: For different underlying types
        private static void WriteEnum(BinaryWriter writer, Enum e) {
            var EType = Enum.GetUnderlyingType(e.GetType());
            if (EType == typeof(byte))
                writer.WriteVar((byte) (object) e);
            else if (EType == typeof(sbyte))
                writer.Write((sbyte) (object) e);
            else if (EType == typeof(short))
                writer.WriteVar((short) (object) e);
            else if (EType == typeof(ushort))
                writer.WriteVar((ushort) (object) e);
            else if (EType == typeof(uint))
                writer.WriteVar((uint) (object) e);
            else if (EType == typeof(long))
                writer.WriteVar((long) (object) e);
            else if (EType == typeof(ulong))
                writer.WriteVar((ulong) (object) e);
            else
                writer.WriteVar((int) (object) e);
        }
        private void WriteGenericList(BinaryWriter writer, IEnumerable array) {
            var count = 0;
            if (array is ICollection col) {
                count = col.Count;
            }
            else {
                foreach (var obj in array)
                    count++;
            }

            var itemType = GetZValueInfo(array.GetType().GetGenericArguments()[0], count > ZUtilCfg.ZTypeCacheUseHeavyCacheHitCount);
            // Int32
            writer.WriteVar(count);
            foreach (var obj in array) WriteValue(writer, obj, itemType);
        }
        private void WriteArray(BinaryWriter writer, object array, ZValueInfo valInfo) {
            if (array is not Array arr) {
                var enumerable = (IEnumerable)array;

                var count = 0;
                if (array is ICollection col) {
                    count = col.Count;
                } else {
                    foreach (var obj in enumerable)
                        count++;
                }

                // Int32
                writer.WriteVar(count);
                foreach (var obj in enumerable) WriteValue(writer, obj);
            } else {
                // Int32
                writer.WriteVar(arr.Length);

                var type = ReflectionHelper.FindUnderlyingArrayElementType(valInfo.Type ?? arr.GetType());
                if (type != null && type != valInfo.Type)
                    valInfo = GetZValueInfo(type, arr.Length > ZUtilCfg.ZTypeCacheUseHeavyCacheHitCount);
                for (var i = 0; i < arr.Length; i++)
                    WriteValue(writer, arr.GetValue(i), valInfo);
            }
        }
        private void WriteDictionary(BinaryWriter writer, IDictionary dic) {
            // Int32
            writer.WriteVar(dic.Count);

            var theType = dic.GetType();
            var genericTypes = ReflectionHelper.FindUnderlyingGenericDictionaryElementType(theType);

            if (genericTypes == null) {
                var dictionaryType = ReflectionHelper.FindUnderlyingGenericElementType(theType);
                genericTypes = dictionaryType?.GetGenericArguments();
                if (genericTypes == null) throw new NullReferenceException($"Object instance can not be created ({theType.FullName})");
            }

            var keyType = GetZValueInfo(genericTypes[0], dic.Count > ZUtilCfg.ZTypeCacheUseHeavyCacheHitCount);
            var valType = GetZValueInfo(genericTypes[1], dic.Count > ZUtilCfg.ZTypeCacheUseHeavyCacheHitCount);

            foreach (DictionaryEntry entry in dic) {
                WriteValue(writer, entry.Key, keyType);
                WriteValue(writer, entry.Value, valType);
            }
        }
        private void WriteNameValueCollection(BinaryWriter writer, NameValueCollection nameValue) {
            // Int32
            writer.WriteVar(nameValue.Count);
            var stringInfo = GetZValueInfo(typeof(string));

            foreach (string key in nameValue) {
                WriteValue(writer, key, stringInfo);
                WriteValue(writer, nameValue[key], stringInfo);
            }
        }
        private static void WriteDateTime(BinaryWriter writer, DateTime dateTime) {
            var dt = dateTime;
            var kind = (byte) dt.Kind;

            if (dateTime == DateTime.MinValue) {
                writer.Write(kind);
                // min datetime indicator
                writer.WriteVar(0L);
            } else if (dateTime == DateTime.MaxValue) {
                writer.Write(kind);
                // max datetime indicator
                writer.WriteVar(1L);
            } else {
                writer.Write(kind);
                //Int64
                writer.WriteVar(dt.Ticks);
            }
        }
        private static void WriteDateTimeOffset(BinaryWriter writer, DateTimeOffset dateTimeOffset) {
            var dt = dateTimeOffset;
            var offset = dateTimeOffset.Offset;
            short offsetMinutes;
            unchecked {
                offsetMinutes = (short) ((offset.Hours * 60) + offset.Minutes);
            }
            // int16
            writer.WriteVar(offsetMinutes);

            // int64
            writer.WriteVar(dt.Ticks);
        }
        private void WriteString(BinaryWriter writer, string str) {
            // if (str == null) {
            //     writer.WriteVar((int?) null);
            // } else 
            if (str.Length == 0) {
                writer.WriteVar((int) 0);
            } else {
                var strBytes = Encoding.GetBytes(str);
                // Int32
                writer.WriteVar((int) strBytes.Length);
                writer.Write(strBytes);
            }
        }
        private static void WriteGuid(BinaryWriter writer, Guid g) {
            if (g == Guid.Empty) {
                // Int32
                writer.WriteVar(0);
                return;
            }

            var data = g.ToByteArray();
            // Int32
            writer.WriteVar(data.Length);
            writer.Write(data);
        }
        #endregion

        #region Deserialization methods
        private object? ReadObject(BinaryReader reader, Type type, ZTypeInfo? bionType = null) {
            bionType ??= (ZTypeInfo)GetZTypeInfo(type);
            var resultObj = ZTypeCache.CreateInstance(type);
            if (resultObj == null) throw new NullReferenceException($"Object instance can not be created: {type.FullName}");
            // Read the members
            ReadMembers(reader, resultObj, bionType.Members.Values);
            return resultObj;
        }

        private void ReadMembers(BinaryReader reader, object obj, IEnumerable<ZMemberInfo> memberList) {
            // while all members are processed
            foreach (var mem in memberList) {
                //Skip flag non-selected members
                if ((!cacheFlags.HasFlag(ZTypeCacheFlags.NonPublic) && !mem.IsPublic) || (!cacheFlags.HasFlag(ZTypeCacheFlags.Fields) && !mem.IsProperty))
                    continue;
                //var memberType = mem.MemberType == ZTypeCacheMembers.Property ? ((PropertyInfo)mem.Info).PropertyType : ((FieldInfo)mem.Info).FieldType;
                // set the value
                var value = ReadMember(reader, mem.Value);
                mem.Setter(obj, value);
            }
        }

        private object? ReadMember(BinaryReader reader, Type memType) {
            var valInfo = GetZValueInfo(memType);
            return ReadMember(reader, valInfo);
        }

        private object? ReadMember(BinaryReader reader, ZValueInfo valInfo) {
            if (valInfo.IsNullable && reader.ReadByte() != 0) return null;

            var actualMemberType = valInfo.Type;
            if (valInfo.IsNullable && valInfo.NullableUnderlyingType != null) actualMemberType = valInfo.NullableUnderlyingType;

            switch (valInfo.KnownType) {
                case ZTypeCacheKnownTypes.Unknown:
                    if (valInfo.ContainerType != null) return ReadObject(reader, actualMemberType, valInfo.ContainerType);
                    if (valInfo.IsDictionary) return ReadDictionary(reader, actualMemberType);
                    if (valInfo.IsCollection || valInfo.IsArray || valInfo.IsEnumerable)
                        return (valInfo.IsGeneric) ? ReadGenericList(reader, actualMemberType) : ReadArray(reader, actualMemberType);
                    break;
                case ZTypeCacheKnownTypes.Int16: return reader.ReadVarInt16();
                case ZTypeCacheKnownTypes.UInt16: return reader.ReadVarUInt16();
                case ZTypeCacheKnownTypes.Int32: return reader.ReadVarInt32();
                case ZTypeCacheKnownTypes.Int64: return reader.ReadVarInt64();
                case ZTypeCacheKnownTypes.UInt64: return reader.ReadVarUInt64();
                case ZTypeCacheKnownTypes.UInt32: return reader.ReadVarUInt32();
                case ZTypeCacheKnownTypes.Double: return reader.ReadVarDouble();
                case ZTypeCacheKnownTypes.Decimal: return reader.ReadVarDecimal();
                case ZTypeCacheKnownTypes.Single: return reader.ReadVarSingle();
                case ZTypeCacheKnownTypes.Byte: return reader.ReadByte();
                case ZTypeCacheKnownTypes.SByte: return reader.ReadSByte();
                case ZTypeCacheKnownTypes.ByteArray: return ReadBytes(reader);
                case ZTypeCacheKnownTypes.String: return ReadString(reader);
                case ZTypeCacheKnownTypes.Char: return (char) reader.ReadVarUInt16();
                case ZTypeCacheKnownTypes.Bool: return reader.ReadByte() != 0;
                case ZTypeCacheKnownTypes.Enum: return ReadEnum(reader, actualMemberType);
                case ZTypeCacheKnownTypes.DateTime: return ReadDateTime(reader);
                case ZTypeCacheKnownTypes.DateTimeOffset: return ReadDateTimeOffset(reader);
                case ZTypeCacheKnownTypes.TimeSpan: return new TimeSpan(reader.ReadVarInt64());
                case ZTypeCacheKnownTypes.DataSet: return ReadDataset(reader, actualMemberType);
                case ZTypeCacheKnownTypes.DataTable: return ReadDataTable(reader);
                case ZTypeCacheKnownTypes.NameValueColl: return ReadCollectionNameValue(reader, actualMemberType);
                case ZTypeCacheKnownTypes.Color: return Color.FromArgb(reader.ReadVarInt32());
                case ZTypeCacheKnownTypes.DbNull: return DBNull.Value;
                case ZTypeCacheKnownTypes.Version: return new Version(ReadString(reader));
                case ZTypeCacheKnownTypes.Guid: return ReadGuid(reader);
                case ZTypeCacheKnownTypes.Uri: return new Uri(ReadString(reader));
                default: throw new ArgumentException("Type can not be read");
            }
            return null;
        }

        //Z Fixed for different underlying types
        private static object ReadEnum(BinaryReader reader, Type type) {
            var EType = Enum.GetUnderlyingType(type);
            if (EType == typeof(byte)) return Enum.ToObject(type, reader.ReadByte());
            if (EType == typeof(sbyte)) return Enum.ToObject(type, reader.ReadSByte());
            if (EType == typeof(short)) return Enum.ToObject(type, reader.ReadVarInt16());
            if (EType == typeof(ushort)) return Enum.ToObject(type, reader.ReadVarUInt16());
            if (EType == typeof(uint)) return Enum.ToObject(type, reader.ReadVarUInt32());
            if (EType == typeof(long)) return Enum.ToObject(type, reader.ReadVarInt64());
            if (EType == typeof(ulong)) return Enum.ToObject(type, reader.ReadVarUInt64());
            return Enum.ToObject(type, reader.ReadVarInt32());
        }

        private object ReadArray(BinaryReader reader, Type type) {
            var count = reader.ReadVarInt32();

            var itemType = type.GetElementType();
            if (itemType == null) {
                itemType = ReflectionHelper.FindUnderlyingGenericElementType(type);
                if (itemType == null) throw new ArgumentException("Unknown 'Object' array type is not supported.\n" + type);
            }

            IList lst;
            if (type.IsArray) {
                var arr = ReflectionHelper.CreateArray(itemType, count);
                lst = arr;

                for (var i = 0; i < count; i++) {
                    var val = ReadMember(reader, itemType);
                    lst[i] = val;
                }
                return arr;
            }
            lst = (IList?)ZTypeCache.CreateInstance(type) ?? throw new NullReferenceException($"Object instance can not be created ({type.FullName})");

            for (var i = 0; i < count; i++) {
                var val = ReadMember(reader, itemType);
                lst.Add(val);
            }
            return lst;
        }

        private IList ReadGenericList(BinaryReader reader, Type type) {
            var count = reader.ReadVarInt32();

            var typeToCreate = type;
            var itemType = type.GetGenericArguments()[0];

            var ilistBase = typeof(IList<>);
            if (ilistBase == type.GetGenericTypeDefinition()) typeToCreate = typeof(List<>).MakeGenericType(itemType);

            var listObj = (IList?)ZTypeCache.CreateInstance(typeToCreate) ?? throw new NullReferenceException($"Object instance can not be created ({type.FullName})");

            for (var i = 0; i < count; i++) {
                var val = ReadMember(reader, itemType);
                listObj.Add(val);
            }

            return listObj;
        }

        // private IList ReadIListImpl(BinaryReader reader, Type type) {
        //     var count = reader.ReadVarInt32();

        //     var listObj = (IList) ZTypeCache.CreateInstance(type);

        //     var itemType = type.GetElementType();
        //     if (itemType == null) throw new ArgumentException("Unknown ICollection implementation is not supported.\n" + type);

        //     for (var i = 0; i < count; i++) {
        //         var val = ReadMember(reader, itemType);
        //         listObj.Add(val);
        //     }

        //     return listObj;
        // }
        private static byte[] ReadBytes(BinaryReader reader) {
            var length = reader.ReadVarInt32();
            return reader.ReadBytes(length);
        }
        private object ReadCollectionNameValue(BinaryReader reader, Type type) {
            var count = reader.ReadVarInt32();
            var nameValue = (NameValueCollection?) ZTypeCache.CreateInstance(type) ?? throw new NullReferenceException($"Object instance can not be created ({type.FullName})");
            var strType = typeof(string);
            for (var i = 0; i < count; i++) {
                var name = ReadMember(reader, strType) as string;
                var val = ReadMember(reader, strType) as string;
                nameValue.Add(name, val);
            }
            return nameValue;
        }

        private DataTable ReadDataTable(BinaryReader reader) {
            var dt = (DataTable?)ZTypeCache.CreateInstance(typeof(DataTable)) ?? throw new NullReferenceException($"Object instance can not be created ({typeof(DataTable).FullName})");

            var schema = ReadString(reader);
            //dt.TableName = name;
            SetXmlSchema(dt, schema);

            var cols = dt.Columns;

            var rowCount = reader.ReadVarInt32();
            for (var i = 0; i < rowCount; i++) {
                var row = dt.Rows.Add();
                ReadDataRow(reader, row, cols);
            }
            return dt;
        }

        private void ReadDataRow(BinaryReader reader, DataRow row, DataColumnCollection cols) {
            var itemCount = reader.ReadVarInt32();
            var colCount = cols.Count;
            var passedIndex = 0;
            while (passedIndex < colCount && passedIndex < itemCount) {
                passedIndex++;

                var colIndex = reader.ReadVarInt32();
                var col = cols[colIndex];
                var val = ReadMember(reader, col.DataType);
                row[col] = val ?? DBNull.Value;
            }
        }

        private object ReadDataset(BinaryReader reader, Type memType) {
            var count = reader.ReadVarInt32();
            var ds = ZTypeCache.CreateInstance(memType) as DataSet ?? throw new NullReferenceException($"Object instance can not be created ({typeof(DataSet).FullName})");
            for (var i = 0; i < count; i++) {
                var dt = ReadDataTable(reader);
                ds.Tables.Add(dt);
            }
            return ds;
        }
        private static void SetXmlSchema(DataTable dt, string schema) {
            using var reader = new StringReader(schema);
            dt.ReadXmlSchema(reader);
        }

        private object ReadDictionary(BinaryReader reader, Type memType) {
            var count = reader.ReadVarInt32();

            var typeToCreate = memType;
            var genericArgs = memType.GetGenericArguments();

            var genericBase = typeof(IDictionary<,>);
            if (genericBase == memType.GetGenericTypeDefinition()) typeToCreate = typeof(Dictionary<,>).MakeGenericType(genericArgs);

            var dic = ZTypeCache.CreateInstance(typeToCreate) as IDictionary ?? throw new NullReferenceException($"Object instance can not be created ({typeof(IDictionary).FullName})");

            var keyType = genericArgs[0];
            var valType = genericArgs[1];

            for (var i = 0; i < count; i++) {
                var key = ReadMember(reader, keyType) ?? throw new NullReferenceException($"Object instance can not be created ({keyType.FullName})");
                var val = ReadMember(reader, valType);
                dic.Add(key, val);
            }
            return dic;
        }

        private static DateTime ReadDateTime(BinaryReader reader) {
            var kind = reader.ReadByte();
            var ticks = reader.ReadVarInt64();
            if (ticks == 0L) return DateTime.MinValue;
            if (ticks == 1L) return DateTime.MaxValue;

            return new DateTime(ticks, (DateTimeKind) kind);
        }

        private static DateTimeOffset ReadDateTimeOffset(BinaryReader reader) {
            var offsetMinutes = reader.ReadVarInt16();
            var ticks = reader.ReadVarInt64();
            return new DateTimeOffset(ticks, TimeSpan.FromMinutes(offsetMinutes));
        }

        private string ReadString(BinaryReader reader) {
            var length = reader.ReadVarInt32();
            if (length == 0) return string.Empty;
            var strBuff = reader.ReadBytes(length);
            return Encoding.GetString(strBuff, 0, strBuff.Length);
        }

        private static object ReadGuid(BinaryReader reader) {
            var gbuff = ReadBytes(reader);
            if (gbuff.Length == 0) return Guid.Empty;
            return new Guid(gbuff);
        }
        #endregion
    }
}