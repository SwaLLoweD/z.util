/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted,free of charge,to any person obtaining a copy
of this software and associated documentation files (the "Software"),to deal
in the Software without restriction,including without limitation the rights
to use,copy,modify,merge,publish,distribute,sublicense,and/or sell
copies of the Software,and to permit persons to whom the Software is
furnished to do so,subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS",WITHOUT WARRANTY OF ANY KIND,EXPRESS OR
IMPLIED,INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,DAMAGES OR OTHER
LIABILITY,WHETHER IN AN ACTION OF CONTRACT,TORT OR OTHERWISE,ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System;
using System.Collections.Generic;
using Z.Collections.Specialized;

namespace Securist.Util {
/*     
     * Keep LastRun Date at Registry and lock license if lastrundate > DateTime.Now
     * TempIPStore(IP)>IPStore(IP+MACID)
     * ProLicense (Locks only updates) / RentalLicense(Locks whole) / EnterpriseLicense ?? / ConsultantLicense

 SetupKey 6 byte
    ProductID 2
    Extensions 3
    Check Byte 1
 
 MachineKey 5 byte
    Machine FingerPrint CRC32 4
    Check Byte

 UnlockKey  5 byte (20 karakter)
    Machine FingerPrint Zamazingola (
    Expiry Date 2 byte (2012.01.01 + x days)
    Addr Count 2 byte (x*10) Max 655350
    Scan Count 1 byte (x*100) Max 25500 
     )
 
     
    SetupKey = 5 * 4  (20)      80 byte
        ProductID               2 byte
        Extensions              4 byte
        CompanyName             16 byte
        CRC32 of SetupKey       4 byte
    Total Data = 26 byte
    * Random Data Acceptable


    MachineKey = 2 + 6 (8)          32 byte
        MD5 Hash of Machine Values  16 byte
        MD5 of MachineKey           16 byte
    Total Data = 32 byte
    * NO Random Data Acceptable
    * ToReturn Answer => Check ProductID,MD5 Hash of MachineKey


    UnlockKey = 3 * 4 (12)          48 byte
        AES Encyrpted 
            ToEncrypt=>(MD5 Hash of Machine Values) 
            Key=>(MD5 Hash of (MachineKey + SetupKey) (16 byte)) 
                                    16 byte
      	Scrambled Expiry Date       2 byte
        Scrambled Address Count     2 byte (*10 real value)
        Scrambled Scan Count        2 byte (*10 real value)
        MD5 Hash of UnlockKey       16 bytes
    Total Data = 38 byte
    * Random Data Acceptable
    * ToValidate => Decrypt and compare Machine Value hash,validate MD5 UnlockKey
    */
    [Serializable]
    public class KeyGen {
        public uint salt; //4 byte
        //Product Key
        public ushort ProductID; //2 byte
        public uint Extensions; //4 byte
        public char[] Name; // 16 byte [16]

        //Machine Key
        public ushort MKProductID; //2 byte
        public Guid MachineValueHash; // 16 byte MD5(Machine)

        //Unlock Key
        public Guid MachineValueHashAes; // 16 byte (AES(MD5(Machine)))
        public ushort ExpiryDate; //2 byte (Days from 1.1.2010)
        public ushort CountAddress; // 2 byte (calculate real value as *10) [MD5(IP+MAC) / MD5(DNS+MAC)]
        public uint CountScan; //2 byte (*10 real value)

        public void DistributeToArray(ByteArray pool, IEnumerable<ByteArray> bytes)
        {
            var tlen = pool.Length;
            var rlen = 0;
            var cntBytes = 0;
            foreach (ByteArray b in bytes) {
                rlen += b.Length;
                cntBytes++;
            }

            var tdiff = tlen - rlen; //Difference between real length and target length
            if (tdiff < 1) { throw new ArgumentException(); } //No space

            var sa = getslideArray(getScrambleKey(pool), cntBytes - 1, tlen, tdiff);
        }
        private int getScrambleIndex(ByteArray pool) { return (pool.Length / 2) % Math.Abs((pool.Length / 2) - 26); }
        private int getScrambleKey(ByteArray pool) { return pool[getScrambleIndex(pool)]; }
        private byte[] getslideArray(int seed, int slidecnt, int tlen, int tdiff)
        {
            var rval = new byte[slidecnt];
            int avgdiff = tdiff / slidecnt;
            int totalslide = 0;
            while (slidecnt > 1) {
                rval[slidecnt--] = (byte) avgdiff; //Some formula to calculate slide amounts based on key is required
                totalslide += avgdiff;
            }
            rval[slidecnt--] = (byte) (tdiff - totalslide); //remaining part
            return rval;
        }
        private byte scrambleByte1(byte b) { return (byte) ((b & 0x0F) << 4 | (b & 0xF0) >> 4); }
        private byte scrambleByte2(byte b) { return (byte) ~b; }
        private byte scrambleByte3(byte b) { return (byte) ((b & 0x03) << 2 | (b & 0x0C) >> 2 | (b & 0x30) << 2 | (b & 0xC0) >> 2); }

        #region Pseudo-random tables
        public static byte[] tbl1 = new byte[] {
            1,95,205,200,31,173,220,249,14,12,17,57,170,207,211,156,
            253,244,113,137,130,221,97,187,74,86,122,223,223,29,254,95,
            150,208,176,251,174,102,152,74,182,28,60,29,64,70,150,204,
            40,106,216,72,240,223,24,57,228,73,181,142,105,167,196,86,
            236,85,99,32,228,42,63,161,252,75,211,207,68,245,144,241,
            182,0,42,163,51,54,87,158,160,87,107,251,59,44,241,236,
            101,204,7,114,67,107,4,98,68,39,113,246,169,34,159,247,
            197,128,5,31,164,135,180,112,55,194,254,194,108,93,196,97,
            1,165,238,84,245,72,39,34,187,86,122,91,62,216,100,203,
            217,72,168,93,187,5,198,187,63,81,193,212,211,43,83,216,
            132,47,181,102,193,93,120,186,100,88,195,221,227,255,7,144,
            96,204,243,212,236,95,67,64,40,93,167,154,142,98,35,132,
            108,200,243,158,144,120,126,109,62,192,203,2,25,87,157,120,
            56,203,5,136,110,12,37,165,37,8,72,16,144,56,51,247,
            124,18,131,132,180,101,239,86,94,51,14,108,215,221,77,115,
            188,238,59,102,59,198,199,120,27,128,79,168,171,232,141 };
        public static byte[] tbl2 = new byte[] {
            179,246,37,61,108,102,125,1,54,204,164,193,156,110,186,1,
            25,208,35,81,95,186,173,225,47,9,91,157,168,93,67,255,
            114,2,130,76,67,16,174,49,133,224,132,220,44,121,193,104,
            90,181,209,116,158,24,66,106,248,239,122,40,67,93,251,135,
            169,124,222,49,5,171,146,162,169,28,44,221,210,177,160,119,
            255,37,110,152,100,214,179,85,65,94,111,210,96,104,218,59,
            171,72,81,254,199,147,177,141,212,188,87,149,47,177,233,158,
            249,155,3,197,224,135,222,168,138,162,122,237,194,49,53,91,
            23,237,230,200,172,21,218,206,247,109,32,81,139,26,118,45,
            206,111,187,51,163,244,69,124,234,87,236,122,217,1,95,35,
            80,29,203,150,223,72,4,199,185,113,172,227,17,63,225,104,
            107,206,81,1,217,25,227,192,49,225,189,115,47,64,8,33,
            242,108,6,31,248,107,225,214,19,94,231,118,226,146,223,110,
            204,16,147,10,244,40,23,45,100,82,143,159,92,66,254,111,
            65,186,244,206,39,153,3,28,47,128,172,7,76,162,117,167,
            198,111,23,175,80,159,106,119,144,11,118,129,22,149,220 };
        public static byte[] tbl3 = new byte[] {
            37,51,246,213,208,39,175,85,234,234,59,197,231,194,253,12,
            209,151,131,195,160,183,135,203,71,131,242,92,63,138,35,169,
            7,113,90,181,143,171,25,37,90,150,175,152,219,191,50,229,
            155,90,198,204,42,110,188,3,117,75,111,61,194,15,139,203,
            44,189,226,201,124,211,165,83,155,22,163,41,161,189,161,38,
            59,197,211,147,200,157,17,178,158,234,245,31,139,254,142,147,
            74,211,76,104,129,185,198,253,228,4,42,238,118,25,144,247,
            55,129,91,131,204,134,68,170,251,39,135,166,134,221,207,5,
            249,68,162,117,126,135,64,147,61,178,8,98,93,98,231,238,
            98,152,190,225,122,6,196,217,153,199,175,137,136,71,113,57,
            148,86,21,250,235,189,71,225,208,57,75,218,174,185,119,153,
            196,7,170,206,53,217,96,221,217,18,177,117,77,66,86,244,
            70,41,166,39,227,33,78,7,15,172,151,79,185,92,180,226,
            47,60,16,93,75,167,4,192,66,15,179,199,44,96,161,212,
            132,249,123,78,83,137,0,49,255,92,56,240,50,133,239,110,
            126,115,177,104,123,151,131,142,49,197,186,103,0,94,128 };
        #endregion
    }
}