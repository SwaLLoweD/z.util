﻿using System;
using System.Collections.Generic;
using System.Reflection;

/* 
 * Salar BOIS (Binary Object Indexed Serialization)
 * by Salar Khalilzadeh
 * 
 * https://github.com/salarcode/Bois
 * Mozilla Public License v2
 */
namespace Z.Util;

internal static class ReflectionHelper
{
    public static Type? FindUnderlyingArrayElementType(Type arrayType) => arrayType.GetElementType();

    /// <summary>Finds the underlying element type of a contained generic type Less acurate but cpu cheaper</summary>
    public static Type? FindUnderlyingGenericElementType(Type type) {
        var generics = type.GetGenericArguments();
        if (generics.Length > 0) return generics[0];
        if (type.BaseType == null) return null;

        foreach (var inter in type.GetInterfaces()) {
            if (!inter.IsGenericType) continue;
            // it should have only one argument
            var args = inter.GetGenericArguments();
            if (args.Length == 1) return args[0];
        }
        return null;
    }
    /// <summary>Finds the underlying element type of a contained generic type Less acurate but cpu cheaper</summary>
    public static Type[]? FindUnderlyingGenericDictionaryElementType(Type type) {
        var generics = type.GetGenericArguments();
        if (generics.Length == 2) return generics;
        if (type.BaseType == null) return null;

        foreach (var inter in type.GetInterfaces()) {
            if (!inter.IsGenericType) continue;
            // it should have two arguments
            var args = inter.GetGenericArguments();
            if (args.Length == 2) return args;
        }
        return null;
    }

    // /// <summary>Finds the underlying element type of a contained generic type CPU heavy but more accurate!</summary>
    // public static Type? FindUnderlyingIEnumerableElementType(Type type) {
    //     if (type.BaseType == null) return null;
    //     foreach (var inter in type.GetInterfaces()) {
    //         if (inter.IsGenericType) {
    //             // it should have only one argument
    //             var args = inter.GetGenericArguments();
    //             if (args.Length == 1) {
    //                 var enumGeneric = typeof(IEnumerable<>).MakeGenericType(args[0]);
    //                 if (enumGeneric.IsAssignableFrom(type)) return args[0];
    //             }
    //         }
    //     }
    //     return null;
    // }
    /// <summary>Check to see if the type implements an specific generic interface type</summary>
    public static bool CompareInterfaceGenericTypeDefinition(Type type, Type genericType) => CompareInterfaceGenericTypeDefinition(type.GetInterfaces(), genericType);

    /// <summary>Check to see if the type implements an specific generic interface type</summary>
    public static bool CompareInterfaceGenericTypeDefinition(Type[] typeInterfaces, Type genericType) =>
    // if needed, you can also return the type used as generic argument
    typeInterfaces.Any((@interface) => @interface.IsGenericType && @interface.GetGenericTypeDefinition() == genericType);

    public static bool CompareSubType(Type t1, Type t2) => t1 == t2 || t1.IsSubclassOf(t2);
    public static bool CompareInterface(Type type, Type interfaceType) => type == interfaceType || interfaceType.IsAssignableFrom(type);
    public static Array CreateArray(Type elementType, int length) => Array.CreateInstance(elementType, length);
    public static void SetValue(object obj, object value, FieldInfo memInfo) => memInfo.SetValue(obj, value);

    #region IsNullable
    public static bool IsNullable(Type typeofResult) => !typeofResult.IsValueType || Nullable.GetUnderlyingType(typeofResult) != null; // ref-type || Nullable<T> => if not then value-type
    public static bool IsNullable(Type typeofResult, out Type? underlyingType) {
        underlyingType = null;
        if (!typeofResult.IsValueType) return true; // ref-type
        underlyingType = Nullable.GetUnderlyingType(typeofResult);
        if (underlyingType != null) return true; // Nullable<T>
        return false; // value-type
    }
    public static bool IsNullable<T>() => IsNullable(typeof(T));
    public static bool IsNullable<T>(out Type? underlyingType) => IsNullable(typeof(T), out underlyingType);
    public static bool IsNullable<T>(T obj) => obj == null || IsNullable(typeof(T));
    public static bool IsNullable<T>(T obj, out Type? underlyingType) {
        underlyingType = null;
        return obj == null || IsNullable(typeof(T), out underlyingType);
    }
    #endregion
}