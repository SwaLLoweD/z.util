﻿using System;
using System.Collections.Generic;
using System.Reflection;

/* 
 * Salar BOIS (Binary Object Indexed Serialization)
 * by Salar Khalilzadeh
 * 
 * https://github.com/salarcode/Bois
 * Mozilla Public License v2
 */

namespace Z.Util;

/// <summary>Value Info to be stored in TypeCache</summary>
public class ZValueInfo
{
    #region Fields / Properties
    /// <summary>IsArray</summary>
    public bool IsArray { get; internal set; }
    /// <summary>IsCollection</summary>
    public bool IsCollection { get; internal set; }
    /// <summary>IsEnumerable</summary>
    public bool IsEnumerable { get; internal set; }
    /// <summary>IsDictionary</summary>
    public bool IsDictionary { get; internal set; }
    /// <summary>IsSet</summary>
    public bool IsSet { get; internal set; }
    /// <summary>IsGeneric</summary>
    public bool IsGeneric { get; internal set; }
    /// <summary>IsNullable</summary>
    public bool IsNullable { get; internal set; }
    /// <summary>IsValueType</summary>
    public bool IsStruct { get; internal set; }
    /// <summary>Is Primitive</summary>
    public bool IsSupportedPrimitive { get; internal set; }
    /// <summary>Primitive type</summary>
    public ZTypeCacheKnownTypes KnownType { get; internal set; }
    /// <summary>Type Has Fields or Properties</summary>
    public ZTypeInfo? ContainerType { get; internal set; }
    /// <summary>Underlying unnullable type</summary>
    public Type? NullableUnderlyingType { get; internal set; }
    /// <summary>Type set by user</summary>
    public Type Type { get; internal set; }
    #endregion

    /// <summary>Constructor</summary>
    public ZValueInfo(Type Type) {
        this.Type = Type;
    }
}

/// <summary>Member Info to be stored in TypeCache</summary>
public class ZMemberInfo {
    /// <summary>MemberInfo</summary>
    public MemberInfo Member { get; internal set; }
    /// <summary>Value Information</summary>
    public ZValueInfo Value { get; internal set; }
    /// <summary>Get method for this member</summary>
    public Func<object, object?> Getter { get; internal set; }
    /// <summary>Set method for this member</summary>
    public Action<object, object?> Setter { get; internal set; }
    /// <summary>Public member or not</summary>
    public bool IsPublic { get; protected set; }
    /// <summary>Field Property or object</summary>
    public bool IsProperty { get; protected set; }
        /// <summary>Getters are setters lambda expressions or reflection methods</summary>
    public bool IsLambdaCached { get; internal set; }
    /// <summary>Constructor</summary>
    public ZMemberInfo(MemberInfo member, ZValueInfo value, bool isPublic, bool isLambdaCached, Func<object, object?> getter, Action<object, object?> setter) {
        Member = member;
        Value = value;
        IsProperty = member is PropertyInfo;
        IsPublic = isPublic;
        Getter = getter;
        Setter = setter;
        IsLambdaCached = isLambdaCached;
    }
}

/// <summary>Type Info to be stored in TypeCache</summary>
public class ZTypeInfo
{
    #region Fields / Properties
    private volatile short hitCount;
    /// <summary>Members of this type</summary>
    public IDictionary<string, ZMemberInfo> Members { get; internal set; }
    /// <summary>Cache flags used</summary>
    public ZTypeCacheFlags UsedFlags { get; protected set; }
    /// <summary>Has Fields or Properties</summary>
    public bool IsContainerObject { get; internal set; }
    /// <summary>IsNullable</summary>
    public bool IsNullable { get; internal set; }
    /// <summary>IsValueType</summary>
    public bool IsStruct { get; internal set; }
    /// <summary>Getters are setters lambda expressions or reflection methods</summary>
    public bool IsLambdaCached { get; internal set; } = false;
    /// <summary>Primitive type</summary>
    public ZTypeCacheKnownTypes KnownType { get; internal set; }
    /// <summary>Type set by user</summary>
    public Type Type { get; internal set; }
    #endregion

    /// <summary>Constructor</summary>
    public ZTypeInfo(Type Type, ZTypeCacheFlags usedFlags) {
        Members = new Dictionary<string, ZMemberInfo>();
        this.Type = Type;
        UsedFlags = usedFlags;
    }

    /// <summary>Get Members chosen according to flags</summary>
    public virtual IEnumerable<ZMemberInfo> GetMembers(ZTypeCacheFlags flags = ZTypeCacheFlags.Properties) => Members.Values.Where(x =>
     (flags.HasFlag(ZTypeCacheFlags.NonPublic) || x.IsPublic) &&
     ((flags.HasFlag(ZTypeCacheFlags.Properties) && x.IsProperty) || (flags.HasFlag(ZTypeCacheFlags.Fields) && !x.IsProperty))
    );

    /// <summary>This needs to be cached again or not</summary>
    public virtual bool NeedsRecache(ZTypeCacheFlags inFlags) => !UsedFlags.HasFlag(inFlags);
    //return base.NeedsRecache(inFlags);
    //if ((!HasProps && inFlags.HasFlag(ZTypeCacheFlags.Properties)) || (!HasFields && inFlags.HasFlag(ZTypeCacheFlags.Fields))) return true;
    //return base.NeedsRecache(inFlags);
    /// <summary>This needs to be cached with lambda getter and setters or not</summary>
    public virtual bool NeedsLambdaCache() {
        if (IsLambdaCached || !ZUtilCfg.UseLambdaCodeGen) return false;
        if (hitCount < short.MaxValue) hitCount++;
        return hitCount > ZUtilCfg.ZTypeCacheUseHeavyCacheHitCount;
    }
    ///// <summary>This needs to be cached again or not</summary>
    //public virtual bool NeedsRecache(ZTypeCacheFlags flags) => false;
}