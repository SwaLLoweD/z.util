﻿using System;

/* 
 * Salar BOIS (Binary Object Indexed Serialization)
 * by Salar Khalilzadeh
 * 
 * https://github.com/salarcode/Bois
 * Mozilla Public License v2
 */
namespace Z.Util
{
    /// <summary>Specifies a field or peroperty settings for serialization.</summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ZMemberAttribute : Attribute
    {
        #region Fields / Properties
        ///// <summary>
        ///// In which order should this member be serialized.
        ///// </summary>
        //public int Index { get; private set; }

        /// <summary>Specifies that should this member be included in serialization.</summary>
        public bool Included { get; }
        #endregion

        #region Constructors
        ///// <summary>
        ///// Specifies a field or peroperty settings for serialization.
        ///// </summary>
        ///// <param name="index">In which order should this member be serialized.</param>
        ///// <param name="included">Specifies that should this member be included in serialization.</param>
        //public ZMemberAttribute(int index, bool included)
        //{
        //    //Index = index;
        //    Included = included;
        //}

        /// <summary>Specifies a field or peroperty settings for serialization.</summary>
        public ZMemberAttribute() : this(true) { }

        ///// <summary>
        ///// Specifies a field or peroperty settings for serialization.
        ///// </summary>
        ///// <param name="index">In which order should this member be serialized.</param>
        //public ZMemberAttribute(int index)
        //    : this(index, true)
        //{ }

        /// <summary>Specifies a field or peroperty settings for serialization.</summary>
        /// <param name="included">Specifies that should this member be included in serialization.</param>
        public ZMemberAttribute(bool included) => Included = included;
        #endregion
    }

    /// <summary>
    ///     Can be used for classes and structs to specify that the serializer should serialize fields and properties or
    ///     not.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class ZContractAttribute : Attribute
    {
        #region Fields / Properties
        /// <summary>Specifies that fields should be serialized or not.</summary>
        public bool Fields { get; set; }

        /// <summary>Specifies that properties should be serialized or not.</summary>
        public bool Properties { get; set; }
        #endregion

        #region Constructors
        /// <summary>Can be used for classes and structs to specify that the serializer should serialize fields or properties.</summary>
        /// <param name="fields">Specifies that fields should be serialized or not.</param>
        /// <param name="properties">Specifies that properties should be serialized or not.</param>
        public ZContractAttribute(bool fields, bool properties) {
            Fields = fields;
            Properties = properties;
        }

        /// <summary>Can be used for classes and structs to specify that the serializer should serialize fields or properties.</summary>
        public ZContractAttribute()
            : this(true, true) { }
        #endregion
    }
}