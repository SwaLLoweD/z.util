﻿using System;

namespace Z.Util;

/// <summary>Read fields and or properties into typecache</summary>
[Serializable]
[Flags]
public enum ZTypeCacheFlags : byte
{
    /// <summary>None</summary>
    None = 0,
    /// <summary>Fields</summary>
    Fields = 1 << 1,
    /// <summary>Properties</summary>
    Properties = 1 << 2,
    /// <summary>Default</summary>
    Default = Properties,
    /// <summary>NonPublic</summary>
    NonPublic = 1 << 3,
    /// <summary>Flatten</summary>
    Flatten = 1 << 4
}

/// <summary>Cache property getters and setters into typecache using reflection or lambda expressions or none</summary>
[Serializable]
public enum ZTypeCacheOptions : byte
{
    /// <summary>None</summary>
    None = 0,
    /// <summary>Smart</summary>
    Smart = 1,
    /// <summary>Reflection</summary>
    Reflection = 2,
    /// <summary>Lambda</summary>
    Lambda = 3
}

/// <summary>Basic .Net Structures and types known</summary>
[Serializable]
public enum ZTypeCacheKnownTypes : byte
{
    /// <summary>Unknown</summary>
    Unknown = 0,
    /// <summary>short</summary>
    Int16,
    /// <summary>int</summary>
    Int32,
    /// <summary>long</summary>
    Int64,
    /// <summary>ushort</summary>
    UInt16,
    /// <summary>uint</summary>
    UInt32,
    /// <summary>ulong</summary>
    UInt64,
    /// <summary>double</summary>
    Double,
    /// <summary>decimal</summary>
    Decimal,
    /// <summary>float</summary>
    Single,
    /// <summary>byte</summary>
    Byte,
    /// <summary>sbyte</summary>
    SByte,
    /// <summary>byte[]</summary>
    ByteArray,
    /// <summary>string</summary>
    String,
    /// <summary>char</summary>
    Char,
    /// <summary>guid</summary>
    Guid,
    /// <summary>bool</summary>
    Bool,
    /// <summary>enum</summary>
    Enum,
    /// <summary>datetime</summary>
    DateTime,
    /// <summary>datetimeoffset</summary>
    DateTimeOffset,
    /// <summary>timespan</summary>
    TimeSpan,
    /// <summary>dataset</summary>
    DataSet,
    /// <summary>datatable</summary>
    DataTable,
    /// <summary>namevaluecoll</summary>
    NameValueColl,
    /// <summary>color</summary>
    Color,
    /// <summary>version</summary>
    Version,
    /// <summary>dbnull</summary>
    DbNull,
    /// <summary>uri</summary>
    Uri
}

// /// <summary>Member is Object, type or field</summary>
// [Serializable]
// public enum ZTypeCacheMembers : byte
// {
//     /// <summary>Object</summary>
//     Object = 0,
//     /// <summary>Property</summary>
//     Property = 1,
//     /// <summary>Field</summary>
//     Field = 2
// }