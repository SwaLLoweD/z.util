﻿/* 
 * Heavily modified 
 * by A. Zafer Yurdacalis
 * 
 * Taken from
 * Salar BOIS (Binary Object Indexed Serialization)
 * by Salar Khalilzadeh
 * 
 * https://github.com/salarcode/Bois
 * Mozilla Public License v2
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Z.Collections.Generic;

// #nullable disable
// #pragma warning disable CS8601, CS8600, CS8603, CS8604, RCS1163, IDE0060
namespace Z.Util;

/// <summary>Cached information about types, for internal use.</summary>
public static class ZTypeCache
{
    #region Constants / Static Fields
    /// <summary>typeof(object)</summary>
    public static readonly Type ObjectType = typeof(object);
    /// <summary>typeof(fieldInfo)</summary>
    public static readonly Type FieldInfoType = typeof(FieldInfo);
    /// <summary>FieldInfo.SetValue(instance, value)</summary>
    public static readonly MethodInfo FieldSetValueMethod = FieldInfoType.GetMethod("SetValue", new[] { ObjectType, ObjectType })!;
    #endregion

    #region CacheTable
    private static Lazy<CacheTable<Type, ZTypeInfo>> CacheType { get; } = new(() => new CacheTable<Type, ZTypeInfo>(ZUtilCfg.ZTypeCacheTtl, 0, 0, 100, 10000), true);
    private static Lazy<CacheTable<MemberInfo, ZMemberInfo>> CacheMember { get; } = new(() => new CacheTable<MemberInfo, ZMemberInfo>(ZUtilCfg.ZTypeCacheTtl, 0, 0, 100, 10000), true);
    private static Lazy<CacheTable<Type, ZValueInfo>> CacheValue { get; } = new(() => new CacheTable<Type, ZValueInfo>(ZUtilCfg.ZTypeCacheTtl, 0, 0, 100, 10000), true);

    /// <summary> Removes all cached information about types.</summary>
    public static void ClearCache() {
        if (CacheType.IsValueCreated)
            CacheType.Value.Clear();
        if (CacheMember.IsValueCreated)
            CacheMember.Value.Clear();
        if (CacheValue.IsValueCreated)
            CacheValue.Value.Clear();
    }
    private static void CacheInsert(Type type, ZTypeInfo typeInfo) => CacheType.Value[type] = typeInfo;
    private static void CacheInsert(MemberInfo type, ZMemberInfo memberInfo) => CacheMember.Value[type] = memberInfo;
    private static void CacheInsert(Type type, ZValueInfo valueInfo) => CacheValue.Value[type] = valueInfo;

    #endregion

    #region Initialize GetTypeInfo GetMemberInfo
    /// <summary>Reads type information and caches it.</summary>
    /// <param name="types">The objects types.</param>
    public static void Initialize(params Type[] types) {
        foreach (var t in types) GetTypeInfo(t);
    }
    /// <summary>Reads type information and caches it.</summary>
    /// <typeparam name="T">The object type.</typeparam>
    public static ZTypeInfo GetTypeInfo<T>(ZTypeCacheOptions opts = ZTypeCacheOptions.Smart, ZTypeCacheFlags flags = ZTypeCacheFlags.Default) => GetTypeInfo(typeof(T), opts, flags);
    /// <summary>Reads type information and caches it.</summary>
    public static ZTypeInfo GetTypeInfo(Type type, ZTypeCacheOptions opts = ZTypeCacheOptions.Smart, ZTypeCacheFlags flags = ZTypeCacheFlags.Default) {
        if (CacheType.Value.TryGetValue(type, out var typeInfo)) {
            if (opts == ZTypeCacheOptions.None) {
                if (typeInfo.UsedFlags == flags) return typeInfo; //otherwise flags don't match and needs reading through
            }
            if (opts == ZTypeCacheOptions.Smart && ZUtilCfg.UseLambdaCodeGen && typeInfo.NeedsLambdaCache()) {
                opts = ZTypeCacheOptions.Lambda; //Console.WriteLine($"UseHeavyCache for {type.Name}");
            } else if (!typeInfo.NeedsRecache(flags)) {
                return typeInfo;
            }
        }
        typeInfo = ReadObject(type, opts, flags);
        if (opts != ZTypeCacheOptions.None) CacheInsert(type, typeInfo);
        return typeInfo;
    }
    /// <summary>Reads type information and caches it.</summary>
    public static ZMemberInfo GetMemberInfo(Type container, MemberInfo memberInfo, ZTypeCacheOptions opts = ZTypeCacheOptions.Smart, ZTypeCacheFlags flags = ZTypeCacheFlags.Default) {
        if (CacheMember.Value.TryGetValue(memberInfo, out var memInfo)) return memInfo;
        var typeInfo = GetTypeInfo(container, opts, flags);
        if (CacheMember.Value.TryGetValue(memberInfo, out memInfo)) return memInfo;
        return typeInfo.Members[memberInfo.Name];
    }
    /// <summary>Reads type information and caches it.</summary>
    /// <typeparam name="T">The object type.</typeparam>
    public static ZValueInfo GetValueInfo<T>(ZTypeCacheOptions opts = ZTypeCacheOptions.Smart, ZTypeCacheFlags flags = ZTypeCacheFlags.Default) => GetValueInfo(typeof(T), opts, flags);
    /// <summary>Reads type information and caches it.</summary>
    public static ZValueInfo GetValueInfo(Type type, ZTypeCacheOptions opts = ZTypeCacheOptions.Smart, ZTypeCacheFlags flags = ZTypeCacheFlags.Default) {
        if (CacheValue.Value.TryGetValue(type, out var valueInfo)) return valueInfo;
        valueInfo = ReadValueInfo(type, opts, flags);
        if (opts != ZTypeCacheOptions.None) CacheInsert(type, valueInfo);
        return valueInfo;
    }
    #endregion

    #region Read
    private static ZTypeInfo ReadObject(Type type, ZTypeCacheOptions opts, ZTypeCacheFlags flags) {
        var readProps = flags.HasFlag(ZTypeCacheFlags.Properties);
        var readFields = flags.HasFlag(ZTypeCacheFlags.Fields);

        var zContract = type.GetCustomAttributes<ZContractAttribute>(false).FirstOrDefault();
        if (zContract != null) {
            readFields = zContract.Fields;
            readProps = zContract.Properties;
        }
        var typeInfo = new ZTypeInfo (type, flags) {
            KnownType = ZTypeCacheKnownTypes.Unknown,
            IsContainerObject = true,
            IsStruct = type.IsValueType
        };

        var members = new SortedDictionary<string, ZMemberInfo>(ReadMembers(type, opts, flags).ToDictionary(x => x.Member.Name, y => y),
            StringComparer.Create(CultureInfo.InvariantCulture, true));
        //var members = ReadMembers(type, opts, flags).ToArray();

        typeInfo.Type = type;
        typeInfo.IsLambdaCached = opts == ZTypeCacheOptions.Lambda;
        typeInfo.Members = members;

        if (opts != ZTypeCacheOptions.None) CacheInsert(type, typeInfo);
        return typeInfo;
    }
    private static IEnumerable<ZMemberInfo> ReadMembers(Type type, ZTypeCacheOptions opts, ZTypeCacheFlags flags) {
        //Set binding flags;
        var bFlags = BindingFlags.Instance;
        if (flags.HasFlag(ZTypeCacheFlags.Flatten)) bFlags |= BindingFlags.FlattenHierarchy;

        var rval = Enumerable.Empty<ZMemberInfo>();

        if (flags.HasFlag(ZTypeCacheFlags.Properties)) {
            //Z: Important fix to get properties in the right order anywhere
            var props = type.GetProperties(bFlags | BindingFlags.Public); //.OrderBy(x => x.Name);
            rval = rval.Concat(ReadMembers(props, opts, flags, true));
            if (flags.HasFlag(ZTypeCacheFlags.NonPublic)) {
                props = type.GetProperties(bFlags | BindingFlags.NonPublic); //.OrderBy(x => x.Name);
                rval = rval.Concat(ReadMembers(props, opts, flags, false));
            }
        }

        if (flags.HasFlag(ZTypeCacheFlags.Fields)) {
            //Z: Important fix to get properties in the right order anywhere
            var fields = type.GetFields(bFlags | BindingFlags.Public); //.OrderBy(x => x.Name);
            rval = rval.Concat(ReadMembers(fields, opts, flags, true));
            if (flags.HasFlag(ZTypeCacheFlags.NonPublic)) {
                fields = type.GetFields(bFlags | BindingFlags.NonPublic); //.OrderBy(x => x.Name);
                rval = rval.Concat(ReadMembers(fields, opts, flags, false));
            }
        }
        return rval;
    }
    private static IEnumerable<ZMemberInfo> ReadMembers(IEnumerable<MemberInfo> members, ZTypeCacheOptions opts, ZTypeCacheFlags flags, bool isPublic) {
        foreach (var m in members) {
            //var index = -1;
            var memProp = m.GetCustomAttributes(typeof(ZMemberAttribute), false);
            if (memProp.Length > 0 && memProp[0] is ZMemberAttribute boisMember && !boisMember.Included)
                continue;
            //index = boisMember.Index;
            var isProperty = m is PropertyInfo;
            PropertyInfo pi;
            FieldInfo fi;
            ZMemberInfo memInfo;
            ZValueInfo valInfo;
            if (isProperty) {
                pi = (PropertyInfo)m;
                valInfo = GetValueInfo(pi.PropertyType, opts, flags);
                memInfo = CreateMemberInfo(valInfo, pi, opts, isPublic);
            } else {
                fi = (FieldInfo)m;
                valInfo = GetValueInfo(fi.FieldType, opts, flags);
                memInfo = CreateMemberInfo(valInfo, fi, opts, isPublic);
            }
            if (opts != ZTypeCacheOptions.None)
                CacheInsert(m, memInfo);
            yield return memInfo;
        }
    }
    private static ZValueInfo ReadValueInfo(Type memType, ZTypeCacheOptions opts = ZTypeCacheOptions.Smart, ZTypeCacheFlags flags = ZTypeCacheFlags.Default) {
        if (memType == typeof(string)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.String,
                IsNullable = true,
                IsSupportedPrimitive = true
            };
        }

        var memActualType = memType;
        var isNullable = ReflectionHelper.IsNullable(memType, out var underlyingTypeNullable);

        // check the underling type
        if (isNullable && underlyingTypeNullable != null)
            memActualType = underlyingTypeNullable;
        else
            underlyingTypeNullable = null;

        // is struct and uses Nullable<>
        if (memActualType == typeof(char)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Char,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        // is struct and uses Nullable<>
        if (memActualType == typeof(bool)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Bool,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        // is struct and uses Nullable<>
        if (memActualType == typeof(DateTime)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.DateTime,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        // is struct and uses Nullable<>
        if (memActualType == typeof(DateTimeOffset)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.DateTimeOffset,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        if (memActualType == typeof(byte[])) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.ByteArray,
                IsNullable = isNullable,
                IsArray = true,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        if (ReflectionHelper.CompareSubType(memActualType, typeof(Enum))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Enum,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        if (ReflectionHelper.CompareSubType(memActualType, typeof(Array))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Unknown,
                IsNullable = isNullable,
                IsArray = true,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        var isGenericType = memActualType.IsGenericType;
        Type[] interfaces = memActualType.GetInterfaces();
        if (isGenericType) {
            if (ReflectionHelper.CompareInterfaceGenericTypeDefinition(interfaces, typeof(IDictionary<,>)) ||
                memActualType.GetGenericTypeDefinition() == typeof(IDictionary<,>)) {
                return new ZValueInfo(memType) {
                    KnownType = ZTypeCacheKnownTypes.Unknown,
                    IsNullable = isNullable,
                    IsDictionary = true,
                    IsGeneric = true,
                    IsCollection = true,
                    NullableUnderlyingType = underlyingTypeNullable
                };
            }
        }

        if (ReflectionHelper.CompareInterface(memActualType, typeof(IDictionary))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Unknown,
                IsNullable = isNullable,
                IsDictionary = true,
                IsGeneric = true,
                IsCollection = true,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        // the ISet should be checked before ICollection<>
        if (isGenericType && (ReflectionHelper.CompareInterface(memType, typeof(ISet<>)) ||
                memActualType.GetGenericTypeDefinition() == typeof(ISet<>))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Unknown,
                IsNullable = isNullable,
                IsGeneric = true,
                IsCollection = true,
                IsSet = true,
                NullableUnderlyingType = underlyingTypeNullable,
            };
        }

        // the IDictionary should be checked before IList<>
        if (isGenericType && (ReflectionHelper.CompareInterfaceGenericTypeDefinition(interfaces, typeof(IList<>)) ||
                ReflectionHelper.CompareInterfaceGenericTypeDefinition(interfaces, typeof(ICollection<>)))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Unknown,
                IsNullable = isNullable,
                IsGeneric = true,
                IsCollection = true,
                IsArray = true,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        if (ReflectionHelper.CompareSubType(memActualType, typeof(NameValueCollection))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.NameValueColl,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        // checking for IList and ICollection should be after NameValueCollection
        if (ReflectionHelper.CompareInterface(memActualType, typeof(IList)) ||
            ReflectionHelper.CompareInterface(memActualType, typeof(ICollection))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Unknown,
                IsNullable = isNullable,
                IsGeneric = memActualType.IsGenericType,
                IsCollection = true,
                IsArray = true,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        if (ReflectionHelper.CompareSubType(memActualType, typeof(DataSet))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.DataSet,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        if (ReflectionHelper.CompareSubType(memActualType, typeof(DataTable))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.DataTable,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        // checking for IEnumerable<> should be after all collections
        if (isGenericType && ReflectionHelper.CompareInterfaceGenericTypeDefinition(interfaces, typeof(IEnumerable<>))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Unknown,
                IsNullable = isNullable,
                IsGeneric = true,
                IsEnumerable = true,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        // checking for IEnumerable should be after all collections
        if (ReflectionHelper.CompareInterface(memActualType, typeof(IEnumerable))) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Unknown,
                IsNullable = isNullable,
                IsGeneric = memActualType.IsGenericType,
                IsEnumerable = true,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        // is struct and uses Nullable<>
        if (memActualType == typeof(Color)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Color,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        // is struct and uses Nullable<>
        if (memActualType == typeof(TimeSpan)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.TimeSpan,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        if (memActualType == typeof(Version)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Version,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }

        if (TryReadNumber(memActualType, out var output)) {
            output.IsNullable = isNullable;
            output.NullableUnderlyingType = underlyingTypeNullable;
            output.Type = memType;
            return output;
        }
        // is struct and uses Nullable<>
        if (memActualType == typeof(Guid)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Guid,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        // ignore!
        if (memActualType == typeof(DBNull)) {
            return new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.DbNull,
                IsNullable = isNullable,
                NullableUnderlyingType = underlyingTypeNullable
            };
        }
        return new ZValueInfo(memType) {
            ContainerType = ReadObject(memType, opts, flags),
            NullableUnderlyingType = underlyingTypeNullable,
            IsNullable = isNullable
        };
    }
    private static bool TryReadNumber(Type memType, [System.Diagnostics.CodeAnalysis.NotNullWhen(true)] out ZValueInfo? output) {
        if (memType.IsClass) {
            output = null;
            return false;
        }
        if (memType == typeof(int)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Int32,
                IsSupportedPrimitive = true
            };
        } else if (memType == typeof(long)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Int64,
                IsSupportedPrimitive = true
            };
        } else if (memType == typeof(short)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Int16,
                IsSupportedPrimitive = true
            };
        } else if (memType == typeof(double)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Double
            };
        } else if (memType == typeof(decimal)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Decimal
            };
        } else if (memType == typeof(float)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Single
            };
        } else if (memType == typeof(byte)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.Byte
            };
        } else if (memType == typeof(sbyte)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.SByte
            };
        } else if (memType == typeof(ushort)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.UInt16
            };
        } else if (memType == typeof(uint)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.UInt32
            };
        } else if (memType == typeof(ulong)) {
            output = new ZValueInfo(memType) {
                KnownType = ZTypeCacheKnownTypes.UInt64
            };
        } else {
            output = null;
            return false;
        }
        return true;
    }
    #endregion

    #region CreateInstance
    internal static object? CreateInstance(Type t) => Activator.CreateInstance(t, null);

    #region CreateInstanceByIL
    //private readonly Dictionary<Type, GenericConstructor> _constructorCache;
    //internal object CreateInstanceByIL(Type t)
    //{
    //    // Read from cache
    //    GenericConstructor info = null;
    //    _constructorCache.TryGetValue(t, out info);

    //    if (info == null) {
    //        ConstructorInfo ctor = t.GetConstructor(Type.EmptyTypes);
    //        if (ctor == null) {
    //            // Falling back to default parameterless constructor.
    //            return Activator.CreateInstance(t, null);
    //        }

    //        var dynamicCtor = new DynamicMethod("_", t, Type.EmptyTypes, t, true);

    //        var il = dynamicCtor.GetILGenerator();

    //        il.Emit(OpCodes.Newobj, ctor);
    //        il.Emit(OpCodes.Ret);

    //        info = (GenericConstructor)dynamicCtor.CreateDelegate(typeof(GenericConstructor));

    //        _constructorCache[t] = info;
    //    }
    //    if (info == null)
    //        throw new MissingMethodException(string.Format("No parameterless constructor defined for '{0}'.", t));
    //    return info.Invoke();
    //}
    #endregion

    #region CreateInstance using Lambda Expressions (Disabled)
    //private static CacheTable<Type, Func<object>> _const = new CacheTable<Type, Func<object>>();
    ///// <summary>Create instance of T with parameters using ConstructorInfo.Invoke</summary>
    //public static object CreateInstanceByLambda(Type type, params object[] args)
    //{
    //    if (args == null || args.Length == 0) {
    //        if (!_const.TryGetValue(type, out var ctor)) {
    //            ctor = GetLambdaCreator(type);
    //            _const[type] = ctor;
    //        }
    //        return ctor();
    //    }

    //    var createdActivator = GetParameterdLambdaCreator(type);
    //    return createdActivator(args);
    //}
    //public static Func<object> GetLambdaCreator(Type t)
    //{
    //    // Make a NewExpression that calls the parameterless constructor.
    //    NewExpression newExpression = Expression.New(t);
    //    LambdaExpression lambda = Expression.Lambda(newExpression);
    //    return (Func<object>)lambda.Compile();
    //}
    //public static Func<object[], object> GetParameterdLambdaCreator(Type t)
    //{
    //    // Get constructor information?
    //    ConstructorInfo[] constructors = t.GetConstructors();

    //    // Is there at least 1?
    //    if (constructors.Length >= 0) {
    //        // Get our one constructor.
    //        ConstructorInfo constructor = constructors[0];

    //        // Yes, does this constructor take some parameters?
    //        ParameterInfo[] paramsInfo = constructor.GetParameters();

    //        if (paramsInfo.Length > 0) {
    //            // Create a single param of type object[].
    //            ParameterExpression param = Expression.Parameter(typeof(object[]), "args");

    //            // Pick each arg from the params array and create a typed expression of them.
    //            Expression[] argsExpressions = new Expression[paramsInfo.Length];

    //            for (int i = 0; i < paramsInfo.Length; i++) {
    //                Expression index = Expression.Constant(i);
    //                Type paramType = paramsInfo[i].ParameterType;
    //                Expression paramAccessorExp = Expression.ArrayIndex(param, index);
    //                Expression paramCastExp = Expression.Convert(paramAccessorExp, paramType);
    //                argsExpressions[i] = paramCastExp;
    //            }

    //            // Make a NewExpression that calls the constructor with the args we just created.
    //            NewExpression newExpression = Expression.New(constructor, argsExpressions);

    //            // Create a lambda with the NewExpression as body and our param object[] as arg.
    //            //LambdaExpression lambda = Expression.Lambda(typeof(Creator<T>), newExpression, param);
    //            LambdaExpression lambda = Expression.Lambda(newExpression, param);

    //            // Compile it
    //            return (Func<object[], object>)lambda.Compile();
    //        }
    //    }
    //    return null;
    //}
    #endregion
    #endregion

    #region GetProperty Getter/Setter
    private static ZMemberInfo CreateMemberInfo(ZValueInfo meminfo, PropertyInfo propertyInfo, ZTypeCacheOptions opt, bool isPublic) {
        if (opt == ZTypeCacheOptions.Lambda)
            return new ZMemberInfo(propertyInfo, meminfo, isPublic, true, GetPropertyGetterExp(propertyInfo), GetPropertySetterExp(propertyInfo));
        return new ZMemberInfo(propertyInfo, meminfo, isPublic, false, GetPropertyGetter(propertyInfo), GetPropertySetter(propertyInfo));
    }
    private static ZMemberInfo CreateMemberInfo(ZValueInfo meminfo, FieldInfo fieldInfo, ZTypeCacheOptions opt, bool isPublic) {
        if (opt == ZTypeCacheOptions.Lambda)
            return new ZMemberInfo(fieldInfo, meminfo, isPublic, true, GetFieldGetterExp(fieldInfo), GetFieldSetterExp(fieldInfo));
        return new ZMemberInfo(fieldInfo, meminfo, isPublic, false, GetFieldGetter(fieldInfo), GetFieldSetter(fieldInfo));
    }
    private static Func<object, object?> GetPropertyGetter(PropertyInfo propertyInfo) => target => propertyInfo.GetGetMethod()?.Invoke(target, null);
    private static Action<object, object?> GetPropertySetter(PropertyInfo propertyInfo) => (target, value) => propertyInfo.GetSetMethod(true)?.Invoke(target, new[] { value });

    private static Func<object, object?> GetFieldGetter(FieldInfo fieldInfo) => target => fieldInfo.GetValue(target);
    private static Action<object, object?> GetFieldSetter(FieldInfo fieldInfo) => (target, value) => fieldInfo.SetValue(target, value);

    /// <summary>Slow to init, fast to execute Property Getter by Lambda Expressions</summary>
    private static Func<object, object?> GetPropertyGetterExp(PropertyInfo propertyInfo) {
        //if (objType != propertyInfo.DeclaringType) throw new ArgumentException();

        var instance = Expression.Parameter(typeof(object), "instance"); //propertyInfo.DeclaringType
        var property = Expression.Property(Expression.Convert(instance, propertyInfo.DeclaringType!), propertyInfo);
        var convert = Expression.TypeAs(property, typeof(object));
        return (Func<object, object?>)Expression.Lambda(convert, instance).Compile();
    }
    /// <summary>Slow to init, fast to execute Property Getter by Lambda Expressions</summary>
    private static Action<object, object?> GetPropertySetterExp(PropertyInfo propertyInfo) {
        //Fast Expression Method
        //if (objType != propertyInfo.DeclaringType) throw new ArgumentException();
        var instance = Expression.Parameter(typeof(object), "instance");
        var argument = Expression.Parameter(typeof(object), "value");

        var property = Expression.Property(Expression.Convert(instance, propertyInfo.DeclaringType!), propertyInfo);
        var assign = Expression.Assign(property, Expression.Convert(argument, propertyInfo.PropertyType));
        //var setterCall = Expression.Call(
        //    Expression.Convert(instance, propertyInfo.DeclaringType),
        //    propertyInfo.GetSetMethod(),
        //    Expression.Convert(argument, propertyInfo.PropertyType));
        return (Action<object, object?>)Expression.Lambda(typeof(Action<object, object?>), assign, instance, argument).Compile();
    }
    /// <summary>Slow to init, fast to execute Property Getter by Lambda Expressions</summary>
    private static Func<object, object?> GetFieldGetterExp(FieldInfo fieldInfo) {
        //if (objType != propertyInfo.DeclaringType) throw new ArgumentException();
        var instance = Expression.Parameter(typeof(object), "instance"); //propertyInfo.DeclaringType
        var field = Expression.Field(Expression.Convert(instance, fieldInfo.DeclaringType!), fieldInfo);
        var convert = Expression.TypeAs(field, typeof(object));
        return (Func<object, object?>)Expression.Lambda(convert, instance).Compile();
    }
    /// <summary>Slow to init, fast to execute Property Getter by Lambda Expressions</summary>
    private static Action<object, object?> GetFieldSetterExp(FieldInfo fieldInfo) {
        //Fast Expression Method
        //if (objType != propertyInfo.DeclaringType) throw new ArgumentException();
        var instance = Expression.Parameter(typeof(object), "instance");
        var argument = Expression.Parameter(typeof(object), "value");
        Expression setterCall;

        if (!fieldInfo.IsInitOnly) {
            var field = Expression.Field(Expression.Convert(instance, fieldInfo.DeclaringType!), fieldInfo);
            setterCall = Expression.Assign(field, Expression.Convert(argument, fieldInfo.FieldType));
        } else {
            //fieldInfo.SetValue(instance, argument);
            //setterCall = Expression.Call(
            //    Expression.Constant(fieldInfo, ZTypeCache.FieldInfoType),
            //    ZTypeCache.FieldSetValueMethod,
            //    boxingVariable,
            //    Expression.Call(
            //        DeepCopyByExpressionTreeObjMethod,
            //        Expression.Convert(fieldFrom, ZTypeCache.ObjectType),
            //        Expression.Constant(forceDeepCopy, typeof(Boolean)),
            //        inputDictionary));

            setterCall = Expression.Call(Expression.Constant(fieldInfo, FieldInfoType),
                FieldSetValueMethod,
                Expression.Convert(instance, fieldInfo.DeclaringType!),
                argument);
        }
        return (Action<object, object?>)Expression.Lambda(typeof(Action<object, object?>), setterCall, instance, argument).Compile();
    }
    #endregion

    #region Reflection.Emit IL Getters and setters (Disabled)
    //private Func<object, object> GetPropertyGetterIL(Type objType, PropertyInfo propertyInfo)
    //{
    //    //Fast IL Method
    //    if (objType.IsInterface) {
    //        throw new Exception("Type is an interface or abstract class and cannot be instantiated.");
    //    }
    //    if (objType.IsValueType &&
    //        !objType.IsPrimitive &&
    //        !objType.IsArray &&
    //        objType != typeof(string)) {
    //        // this is a fallback to slower method.
    //        var method = propertyInfo.GetGetMethod(true);

    //        // generating the caller.
    //        return new GenericGetter(target => method.Invoke(target, null));
    //    } else {
    //        //var method = objType.GetMethod("get_" + propertyInfo.Name, BindingFlags.Instance | BindingFlags.Public);
    //        var method = propertyInfo.GetGetMethod(true);
    //        return GetFastGetterFunc(propertyInfo, method);
    //    }
    //}
    //private Action<object, object> GetPropertySetterIL(Type objType, PropertyInfo propertyInfo)
    //{
    //    //Fast IL Method
    //    if (objType.IsValueType &&
    //        !objType.IsPrimitive &&
    //        !objType.IsArray &&
    //        objType != typeof(string)) {
    //        // this is a fallback to slower method.
    //        var method = propertyInfo.GetSetMethod(true);

    //        // generating the caller.
    //        return new Function<object, object, object>((target, value) => method.Invoke(target, new object[] { value }));
    //    } else {
    //        //var method = objType.GetMethod("set_" + propertyInfo.Name, BindingFlags.Instance | BindingFlags.Public);
    //        var method = propertyInfo.GetSetMethod(true);
    //        return GetFastSetterFunc(propertyInfo, method);
    //    }
    //    ////var method = objType.GetMethod("set_" + propertyInfo.Name, BindingFlags.Instance | BindingFlags.Public);
    //    //var method = propertyInfo.GetSetMethod();
    //    //return GetFastSetterFunc(propertyInfo, method);
    //}

    //#if DotNet || DotNetStandard
    //		/// <summary>
    //		///  Creates a dynamic setter for the property
    //		/// </summary>
    //		/// <author>
    //		/// Gerhard Stephan 
    //		/// http://jachman.wordpress.com/2006/08/22/2000-faster-using-dynamic-method-calls/
    //		/// </author>

    //		private GenericSetter CreateSetMethod(PropertyInfo propertyInfo)
    //		{
    //			/*
    //			* If there's no setter return null
    //			*/
    //			MethodInfo setMethod = propertyInfo.GetSetMethod();
    //			if (setMethod == null)
    //				return null;

    //			/*
    //			* Create the dynamic method
    //			*/
    //			var arguments = new Type[2];
    //			arguments[0] = arguments[1] = typeof(object);
    //			var setter = new DynamicMethod(
    //			  String.Concat("_Set", propertyInfo.Name, "_"),
    //			  typeof(void), arguments, propertyInfo.DeclaringType);

    //			ILGenerator generator = setter.GetILGenerator();
    //			generator.Emit(OpCodes.Ldarg_0);
    //			generator.Emit(OpCodes.Castclass, propertyInfo.DeclaringType);
    //			generator.Emit(OpCodes.Ldarg_1);

    //			if (propertyInfo.PropertyType.IsClass)
    //				generator.Emit(OpCodes.Castclass, propertyInfo.PropertyType);
    //			else
    //				generator.Emit(OpCodes.Unbox_Any, propertyInfo.PropertyType);

    //			generator.EmitCall(OpCodes.Callvirt, setMethod, null);
    //			generator.Emit(OpCodes.Ret);

    //			/*
    //			* Create the delegate and return it
    //			*/
    //			return (GenericSetter)setter.CreateDelegate(typeof(GenericSetter));
    //		}
    //#endif

    //#if DotNet || DotNetStandard
    //        /// <summary>
    //        /// Creates a dynamic getter for the property
    //        /// </summary>
    //        /// <author>
    //        /// Gerhard Stephan 
    //        /// http://jachman.wordpress.com/2006/08/22/2000-faster-using-dynamic-method-calls/
    //        /// </author>
    //		private GenericGetter CreateGetMethod(PropertyInfo propertyInfo)
    //		{
    //			/*
    //			* If there's no getter return null
    //			*/
    //			MethodInfo getMethod = propertyInfo.GetGetMethod();
    //			if (getMethod == null)
    //				return null;

    //			/*
    //			* Create the dynamic method
    //			*/
    //			var arguments = new Type[1];
    //			arguments[0] = typeof(object);

    //			var getter = new DynamicMethod(
    //			  String.Concat("_Get", propertyInfo.Name, "_"),
    //			  typeof(object), arguments, propertyInfo.DeclaringType);

    //			ILGenerator generator = getter.GetILGenerator();
    //			generator.DeclareLocal(typeof(object));
    //			generator.Emit(OpCodes.Ldarg_0);
    //			generator.Emit(OpCodes.Castclass, propertyInfo.DeclaringType);
    //			generator.EmitCall(OpCodes.Callvirt, getMethod, null);

    //			if (!propertyInfo.PropertyType.IsClass)
    //				generator.Emit(OpCodes.Box, propertyInfo.PropertyType);

    //			generator.Emit(OpCodes.Ret);

    //			/*
    //			* Create the delegate and return it
    //			*/
    //			return (GenericGetter)getter.CreateDelegate(typeof(GenericGetter));
    //		}
    //#endif
    //#if SILVERLIGHT || DotNet || DotNetStandard
    //        /// <summary>
    //        /// http://social.msdn.microsoft.com/Forums/en-US/netfxbcl/thread/8754500e-4426-400f-9210-554f9f2ad58b/
    //        /// </summary>
    //        /// <returns></returns>

    //		private GenericGetter GetFastGetterFunc(PropertyInfo p, MethodInfo getter) // untyped cast from Func<T> to Func<object> 
    //		{
    //			var g = new DynamicMethod("_", typeof(object), new[] { typeof(object) }, p.DeclaringType, true);
    //			var il = g.GetILGenerator();

    //			il.Emit(OpCodes.Ldarg_0);//load the delegate from function parameter
    //			il.Emit(OpCodes.Castclass, p.DeclaringType);//cast
    //			il.Emit(OpCodes.Callvirt, getter);//calls it's get method

    //			if (p.PropertyType.IsValueType)
    //				il.Emit(OpCodes.Box, p.PropertyType);//box

    //			il.Emit(OpCodes.Ret);

    //			//return (bool)((xViewModel)param1).get_IsEnabled();

    //			var _func = (GenericGetter)g.CreateDelegate(typeof(GenericGetter));
    //			return _func;
    //		}
    //#endif

    //        /// <summary>
    //        /// http://social.msdn.microsoft.com/Forums/en-US/netfxbcl/thread/8754500e-4426-400f-9210-554f9f2ad58b/
    //        /// </summary>
    //#if SILVERLIGHT || DotNet || DotNetStandard
    //		private Function<object, object, object> GetFastSetterFunc(PropertyInfo p, MethodInfo setter)
    //		{
    //			var s = new DynamicMethod("_", typeof(object), new[] { typeof(object), typeof(object) }, p.DeclaringType, true);
    //			var il = s.GetILGenerator();

    //			il.Emit(OpCodes.Ldarg_0);
    //			il.Emit(OpCodes.Castclass, p.DeclaringType);

    //			il.Emit(OpCodes.Ldarg_1);
    //			if (p.PropertyType.IsClass)
    //			{
    //				il.Emit(OpCodes.Castclass, p.PropertyType);
    //			}
    //			else
    //			{
    //				il.Emit(OpCodes.Unbox_Any, p.PropertyType);
    //			}
    //			il.EmitCall(OpCodes.Callvirt, setter, null);
    //			il.Emit(OpCodes.Ldarg_0);
    //			il.Emit(OpCodes.Ret);

    //			//(xViewModel)param1.set_IsEnabled((bool)param2)
    //			// return param1;

    //			var _func = (Function<object, object, object>)s.CreateDelegate(typeof(Function<object, object, object>));
    //			return _func;
    //		}
    //#endif
    #endregion

    #region Getter and Setter Alternatives
    //      private GenericGetter GetPropertyGetter_(Type objType, PropertyInfo propertyInfo)
    //{
    //	var method = objType.GetMethod("get_" + propertyInfo.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
    //	var dlg = Delegate.CreateDelegate(typeof(GenericGetter), method);
    //	return (GenericGetter)dlg;
    //}

    //private GenericGetter GetPropertyGetter(object obj, string propertyName)
    //{
    //	var t = obj.GetType();
    //	var method = t.GetMethod("get_" + propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
    //	var dlg = Delegate.CreateDelegate(t, obj, method);
    //	return (GenericGetter)dlg;
    //}

    //private GenericSetter GetPropertySetter(object obj, string propertyName)
    //{
    //	var t = obj.GetType();
    //	var method = t.GetMethod("set_" + propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
    //	var dlg = Delegate.CreateDelegate(t, obj, method);
    //	return (GenericSetter)dlg;
    //}
    #endregion
}