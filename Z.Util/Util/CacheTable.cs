﻿/* Copyright (c) <2018> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Timers;
using Z.Extensions;
using Z.Util;
using Timer = System.Timers.Timer;

namespace Z.Collections.Generic;

//use Lazy<TValue> as value type if lazy caching, and therefore refills, are required
/// <summary>
///     Caches the objects for specified amount of time in a hashtable format, after the time is elapsed, the object is
///     removed from the table
/// </summary>
/// <typeparam name="TKey">Key</typeparam>
/// <typeparam name="TValue">Value</typeparam>
[Serializable]
public class CacheTable<TKey, TValue> : IDictionary<TKey, TValue>, IDisposable where TKey : notnull
{
    #region Constructors
    /// <summary>Constructor</summary>
    /// <param name="itemTtlMs">Time to live (cache time) for one item in the table, in milliseconds (Negative values: Absolute ttl, disregards accesstime, Positive values: Sliding ttl, ttl is counted after last access, 0 : Item never expires)</param>
    /// <param name="checkIntervalMs">How often entries will be checked for expiry (in milliseconds) (0: use global static timer)</param>
    /// <param name="tableTtlMs">Time to live for the whole table, in milliseconds (Negative values: Absolute ttl, disregards accesstime, Positive values: Sliding ttl, ttl is counted after last access, 0 : Table never expires)</param>
    /// <param name="cachedItemsMin">Minimum number of elements to keep in cache</param>
    /// <param name="cachedItemsMax">Maximum number of elements to keep in cache (oldest ones are prunned)</param>
    public CacheTable(int itemTtlMs = 0, double checkIntervalMs = 0d, int tableTtlMs = 0, int cachedItemsMin = 0, int cachedItemsMax = int.MaxValue) {
        //control.Write(() => {
            ItemTtlMs = itemTtlMs;
            CheckInterval = checkIntervalMs;
            _internalTimer = checkIntervalMs > 0; //use static zutilcfg timer if interval is <= 0
            TableTtlMs = tableTtlMs;
            CachedItemsMin = cachedItemsMin;
            CachedItemsMax = cachedItemsMax;
            if (tableTtlMs == 0) DateExpiry = DateTime.MaxValue;
            else if (tableTtlMs > 0) DateExpiry = LastFill.AddMilliseconds(tableTtlMs);
            else DateExpiry = LastFill.AddMilliseconds(-tableTtlMs);
        //});
    }
    /// <summary>Constructor</summary>
    /// <param name="itemTtlMs">Time to live (cache time) for one item in the table, in milliseconds (-1: items do not expire)</param>
    /// <param name="checkTimer">Timer to be used for checking entries for expiry (0: use global static timer)</param>
    /// <param name="tableTtlMs">Time to live for the whole table, in milliseconds</param>
    /// <param name="cachedItemsMin">Minimum number of elements to keep in cache</param>
    /// <param name="cachedItemsMax">Maximum number of elements to keep in cache (oldest ones are prunned)</param>
    public CacheTable(Timer checkTimer, int itemTtlMs = 0, int tableTtlMs = 0, int cachedItemsMin = 0, int cachedItemsMax = int.MaxValue)
        : this(itemTtlMs, checkTimer?.Interval ?? 0d, tableTtlMs, cachedItemsMin, cachedItemsMax) {
        if (checkTimer != null) Timer = checkTimer; //?? throw new ArgumentNullException(nameof(checkTimer));
    }
    #endregion

    #region Nested type: CacheItem
    /// <summary>CacheItem</summary>
    public class CacheItem
    {
        #region Fields / Properties
        /// <summary>Date Expiry</summary>
        public DateTime DateExpiry { get; set; }
        /// <summary>LastAccess</summary>
        public DateTime DateAdded { get; set; }
        /// <summary>Time to live (cache time) in milliseconds (Negative values: Absolute ttl, disregards accesstime, Positive values: Sliding ttl, ttl is counted after last access, 0 : Item never expires)</summary>
        public int Ttl { get; set; }
        /// <summary>Ttl</summary>
        public TValue Value { get; set; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public CacheItem(TValue item, int ttl, DateTime? lastAccess = default, DateTime? dateAdded = default) {
            Value = item;
            Ttl = ttl;
            DateAdded = dateAdded ?? DateTime.UtcNow;
            if (ttl == 0) DateExpiry = DateTime.MaxValue;
            else if (ttl > 0) DateExpiry = (lastAccess ?? DateTime.UtcNow).AddMilliseconds(ttl);
            else DateExpiry = DateAdded.AddMilliseconds(-ttl);
        }
        #endregion

        /// <summary>IsExpired</summary>
        public bool IsExpired() => IsExpired(DateTime.UtcNow);
        /// <summary>IsExpired</summary>
        public bool IsExpired(DateTime dt) => Ttl != 0 && DateExpiry < dt;
        /// <summary>Sets LastAccess and Expiry</summary>
        public void IsAccessedAt(DateTime? lastAccess = default) {
            if (Ttl <= 0) return;
            DateExpiry = (lastAccess ?? DateTime.UtcNow).AddMilliseconds(Ttl);
        }
    }
    #endregion

    #region Nested type: ValuesCollection
    /// <summary>
    ///     A private class that implements ICollection&lt;TValue&gt; and ICollection for the Values collection. The
    ///     collection is read-only.
    /// </summary>
    [Serializable]
    private class ValuesCollection : ICollection<TValue>
    {
        #region Fields / Properties
        private readonly IDictionary<TKey, CacheItem> myDictionary;
        #endregion

        #region Constructors
        /// <inheritdoc />
        public ValuesCollection(IDictionary<TKey, CacheItem> myDictionary) => this.myDictionary = myDictionary;
        #endregion

        #region ICollection<TValue> Members
        /// <inheritdoc />
        public virtual int Count => myDictionary.Count;
        /// <inheritdoc />
        public bool IsReadOnly => true;
        /// <inheritdoc />
        public void Add(TValue item) => throw new NotImplementedException();
        /// <inheritdoc />
        public void Clear() => myDictionary.Values.Clear();
        /// <inheritdoc />
        public bool Contains(TValue item) => myDictionary.Values.Any(x => Equals(x.Value, item));
        /// <inheritdoc />
        public void CopyTo(TValue[] array, int arrayIndex) => myDictionary.Values.Select(x => x.Value).ToList().CopyTo(array, arrayIndex);
        /// <inheritdoc />
        public virtual IEnumerator<TValue> GetEnumerator() {
            foreach (var pair in myDictionary) yield return pair.Value.Value;
        }
        /// <inheritdoc />
        public bool Remove(TValue item) => throw new NotImplementedException();
        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        #endregion
    }
    #endregion

    #region Properties fields events
    private readonly ConcurrentDictionary<TKey, CacheItem> _dict = new();
    /// <summary>How often entries will be checked for expiry (in milliseconds) (0 = Use global timer interval)</summary>
    public double CheckInterval { get; set; }
    /// <summary>Time to live (cache time) in milliseconds (Negative values: Absolute ttl, disregards accesstime, Positive values: Sliding ttl, ttl is counted after last access, 0 : Table never expires)</summary>
    public int TableTtlMs { get; set; } = 0;
    /// <summary>Time to live (cache time) in milliseconds (Negative values: Absolute ttl, disregards accesstime, Positive values: Sliding ttl, ttl is counted after last access, 0 : Item never expires)</summary>
    public int ItemTtlMs { get; set; } = 120000;
    /// <summary>Maximum number of items to keep in cache</summary>
    public int CachedItemsMax { get; set; } = int.MaxValue;
    /// <summary>Minimum number of items to keep in cache</summary>
    public int CachedItemsMin { get; set; }
    /// <summary>Try to dispose objects when they expire</summary>
    public bool DisposeOnExpiry { get; set; } = false;
    /// <summary>Last Time the Table is Accessed</summary>
    public DateTime DateExpiry { get; protected set; }
    /// <summary>Last Time the Table is filled</summary>
    public DateTime LastFill { get; protected set; } = DateTime.MinValue;
    /// <summary>Checks if the table cache has expired or not</summary>
    public bool IsExpired => TableTtlMs != 0 && DateExpiry < DateTime.UtcNow;
    /// <summary>Internal timer</summary>
    public Timer? Timer { get; set; }
    ///// <summary>Internal time table for data</summary>
    //protected ConcurrentDictionary<TKey, DateTime> TimeTable { get; set; }

    /// <summary>Fired when an item expires and gets removed from table</summary>
    public event EventHandler<EventArgs<TValue>>? OnItemExpire;
    /// <summary>Fired when the table expires and all data is removed from table</summary>
    public event EventHandler<EventArgs>? OnTableExpire;

    private bool _internalTimer = true;
    /// <summary>Refresh lock</summary>
    protected readonly ReaderWriterLockSlim control = new();
    /// <summary>Timer lock</summary>
    protected object lockTimer = new();
    private bool _disposed;
    private readonly object _lockObject = new();
    #endregion

    #region Timer Operations
    /// <summary>Start internal timer</summary>
    protected void StartTimer() {
        lock (lockTimer) {
            StartTimerInner();
        }
    }
    /// <summary>Stop internal timer</summary>
    protected void StopTimer() {
        lock (lockTimer) {
            StopTimerInner();
        }
    }
    /// <summary>Start internal timer</summary>
    protected void StartTimerInner() {
        if (Timer == null) {
            if (CheckInterval == 0) {
                Timer = ZUtilCfg.GlobalCacheTimer;
                Timer.AutoReset = false;
                CheckInterval = Timer.Interval;
                _internalTimer = false;
            } else {
                Timer = new Timer(CheckInterval) {
                    AutoReset = false
                };
            }
            Timer.Elapsed += TmrTimeout_Elapsed;
        }
        if (!Timer.Enabled) Timer.Start();
    }
    /// <summary>Stop internal timer</summary>
    protected void StopTimerInner() {
        if (Timer == null) return;
        if (Timer.Enabled && _internalTimer) Timer.Stop();
    }
    /// <summary>Timer Elapsed event</summary>
    protected void TmrTimeout_Elapsed(object? sender, ElapsedEventArgs args) {
        lock (lockTimer) {
            if (_dict.IsEmpty) {
                StopTimerInner();
            } else if (IsExpired) {
                OnTableExpire?.Invoke(this, EventArgs.Empty);
                Clear();
            } else if (_dict.Count > CachedItemsMin) {
                var now = DateTime.UtcNow;
                var toRemove = _dict.Where(x => x.Value.IsExpired(now)).OrderBy(x => x.Value.DateExpiry);
                foreach (var item in toRemove) {
                    if (_dict.Count <= CachedItemsMin) break;
                    if (!item.Value.IsExpired(now)) continue;
                    var key = item.Key;
                    TryRemove(key, out var obj);
                    OnItemExpire?.Invoke(this, new EventArgs<TValue>(obj));
                    if (DisposeOnExpiry && obj is IDisposable disp) disp.Dispose();
                    //if (obj == null) continue;
                }
                if (_dict.IsEmpty)
                    StopTimerInner();
                else
                    StartTimerInner();
            }
        }
    }
    /// <summary>Sets LastAccess and Expiry</summary>
    public void IsAccessedAt(DateTime? lastAccess = default) {
        if (TableTtlMs <= 0) return;
        DateExpiry = (lastAccess ?? DateTime.UtcNow).AddMilliseconds(TableTtlMs);
    }
    #endregion Timer Operations

    #region Dictionary Methods
    /// <inheritdoc />
    public TValue this[TKey key, int ttl] {
        get => TryGetValue(key, out var rval) ? rval : throw new KeyNotFoundException();
        set {
            if (TryGetValue(key, out var existing))
                TryUpdate(key, value, existing, ttl);
            else
                TryAdd(key, value, ttl);
        }
    }
    /// <inheritdoc />
    public TValue this[TKey key] { get => this[key, 0]; set => this[key, ItemTtlMs] = value; }
    /// <inheritdoc />
    public virtual ICollection<TKey> Keys {
        get {
            IsAccessedAt();
            return _dict.Keys;
        }
    }
    /// <inheritdoc />
    public virtual ICollection<TValue> Values {
        get {
            IsAccessedAt();
            return new ValuesCollection(_dict);
        }
    }
    /// <inheritdoc />
    public virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
        control.EnterReadLock();
        try {
            IsAccessedAt();
            foreach (var pair in _dict) {
                pair.Value.IsAccessedAt();
                yield return new KeyValuePair<TKey, TValue>(pair.Key, pair.Value.Value);
            }
        } finally {
            control.ExitReadLock();
        }
    }
    //IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() { LastAccess = DateTime.UtcNow; return base.GetEnumerator(); }
    /// <inheritdoc />
    public virtual int Count =>
        control.Read(() => {
            IsAccessedAt();
            return _dict.Count;
        });
    /// <inheritdoc />
    public void Clear() {
        if (_dict.IsEmpty) return;
        var v = Values.Where(x => x is IDisposable).ToList();
        control.Write(() => {
            StopTimer();
            _dict.Clear();
        });
        foreach (IDisposable? value in v.Select(v => (IDisposable?)v))
            value?.Dispose();
    }
    /// <summary>Internal add method (should always be called in a write lock) </summary>
    protected virtual void AddInternal(TKey key, TValue value, int itemTtl, DateTime? dateNow = default) {
        var now = dateNow ?? DateTime.UtcNow;
        if (_dict.IsEmpty) {
            LastFill = now;
            if (TableTtlMs < 0) DateExpiry = now.AddMilliseconds(-TableTtlMs);
        }
        //else if (_dict.ContainsKey(key)) return;
        _dict.Add(key, new CacheItem(value, itemTtl, now, now));
        IsAccessedAt(now);
        CheckAfterAdd();
    }

    /// <summary>CheckAfterAdd</summary>
    protected void CheckAfterAdd() {
        StartTimer();
        var removeCount = _dict.Count - CachedItemsMax;
        if (removeCount <= 0) return; //nothing to do
                                      //control.Write(() => {
        var orderedKeys = _dict.OrderBy(x => x.Value.DateExpiry).Select(x => x.Key).Take(removeCount);
        foreach (var key in orderedKeys) _dict.TryRemove(key, out var temp);
        //});
    }
    /// <summary>CheckAfterRemove</summary>
    protected void CheckAfterRemove() {
        if (_dict.IsEmpty) StopTimer();
    }
    /// <inheritdoc />
    public virtual bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value) {
        control.EnterReadLock();
        value = default;
        if (!_dict.TryGetValue(key, out var item)) {
            control.ExitReadLock();
            return false;
        }
        var now = DateTime.UtcNow;
        IsAccessedAt(now);
        item.IsAccessedAt(now);
        value = item.Value;
        control.ExitReadLock();
        StartTimer();
        return true;
    }
    /// <inheritdoc />
    public virtual bool TryAdd(TKey key, TValue value, int itemTtl) =>
        control.Write(() => {
            if (_dict.ContainsKey(key)) return false;
            AddInternal(key, value, itemTtl);
            return true;
        });
    //if (!rval) return false;
    //checkAfterAdd();
    //return true;
    /// <inheritdoc />
    public virtual bool TryAdd(TKey key, TValue value) => TryAdd(key, value, ItemTtlMs);
    /// <inheritdoc />
    public virtual bool TryRemove(TKey key, [MaybeNullWhen(false)] out TValue value) {
        control.EnterWriteLock();
        value = default;
        //if (!dict.TryGetValue(key, out var item)) {
        //    control.ExitWriteLock();
        //    return false;
        //}
        if (!_dict.TryRemove(key, out var item)) {
            control.ExitWriteLock();
            return false;
        }
        value = item.Value;
        control.ExitWriteLock();
        CheckAfterRemove();
        return true;
    }
    /// <inheritdoc />
    public virtual bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, int itemTtl) =>
        control.Write(() => {
            var now = DateTime.UtcNow;
            if (!_dict.TryGetValue(key, out var existingItem)) return false;
            if (existingItem.Value?.Equals(comparisonValue) == false) return false;
            existingItem.Value = newValue;
            existingItem.IsAccessedAt(now);
            existingItem.Ttl = itemTtl;
                //var newItem = existingItem;
                //if (!dict.TryUpdate(key, newItem, existingItem)) return false;
                IsAccessedAt(now);
            return true;
        });
    /// <inheritdoc />
    public virtual bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue) => TryUpdate(key, newValue, comparisonValue, ItemTtlMs);

    /// <inheritdoc />
    public virtual TValue GetOrAdd(TKey key, int itemTtl, Func<TKey, TValue> valueFactory) =>
        control.Write(() => {
            var now = DateTime.UtcNow;
            IsAccessedAt(now);
            if (_dict.TryGetValue(key, out var existing)) return existing.Value;
            var newValue = valueFactory(key);
            AddInternal(key, newValue, itemTtl, now);
            return newValue;
        });
    //var newFunc = new Func<TKey, CacheItem>((TKey keyf) => new CacheItem(valueFactory(keyf), itemTtl, now));
    //var rval = dict.GetOrAdd(key, newFunc);
    //checkAfterAdd();
    //return rval.Value;
    /// <inheritdoc />
    public virtual TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory) => GetOrAdd(key, ItemTtlMs, valueFactory);
    /// <inheritdoc />
    protected virtual TValue GetOrAdd(TKey key, TValue value, int itemTtl) =>
        control.Write(() => {
            var now = DateTime.UtcNow;
            IsAccessedAt(now);
            if (_dict.TryGetValue(key, out var existing)) return existing.Value;
            AddInternal(key, value, itemTtl);
            return value;
        });
    //var rval = dict.GetOrAdd(key, new CacheItem(value, itemTtl, now));
    //checkAfterAdd();
    //return rval.Value;
    /// <inheritdoc />
    public virtual TValue GetOrAdd(TKey key, TValue value) => GetOrAdd(key, value, ItemTtlMs);
    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TKey key, int itemTtl, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory) =>
        control.Write(() => {
            var now = DateTime.UtcNow;
            IsAccessedAt(now);
            if (_dict.TryGetValue(key, out var existingItem)) {
                var updateItem = updateValueFactory(key, existingItem.Value);
                //if (updateItem == null) return default;
                existingItem.Value = updateItem;
                existingItem.IsAccessedAt(now);
                existingItem.Ttl = itemTtl;
                return updateItem;
            }
            var newValue = addValueFactory(key);
            AddInternal(key, newValue, itemTtl, now);
            return newValue;
        });
    //var newFunc = new Func<TKey, CacheItem>((TKey keyf) => new CacheItem(addValueFactory(keyf), itemTtl, now));
    //var newUpdateFunc = new Func<TKey, CacheItem, CacheItem>((TKey keyf, CacheItem oldValue) => new CacheItem(updateValueFactory(keyf, oldValue.Value), itemTtl, now));
    //var rval = dict.AddOrUpdate(key, newFunc, newUpdateFunc);
    //checkAfterAdd();
    //return rval.Value;
    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory) => AddOrUpdate(key, addValueFactory, updateValueFactory);
    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TKey key, TValue addValue, int itemTtl, Func<TKey, TValue, TValue> updateValueFactory) =>
        control.Write(() => {
            var now = DateTime.UtcNow;
            IsAccessedAt(now);

            if (_dict.TryGetValue(key, out var existingItem)) {
                var updateItem = updateValueFactory(key, existingItem.Value);
                //if (updateItem == null) return default;
                existingItem.Value = updateItem;
                existingItem.IsAccessedAt(now);
                existingItem.Ttl = itemTtl;
                return updateItem;
            }
            AddInternal(key, addValue, itemTtl, now);
            return addValue;
        });
    //var newUpdateFunc = new Func<TKey, CacheItem, CacheItem>((TKey keyf, CacheItem oldValue) => new CacheItem(updateValueFactory(keyf, oldValue.Value), itemTtl, now));
    //var rval = dict.AddOrUpdate(key, new CacheItem(addValue, itemTtl, now), newUpdateFunc);
    //checkAfterAdd();
    //return rval.Value;
    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory) => AddOrUpdate(key, addValue, ItemTtlMs, updateValueFactory);

    /// <inheritdoc />
    public bool IsReadOnly => false;
    /// <inheritdoc />
    public void Add(TKey key, TValue value) => TryAdd(key, value);
    /// <inheritdoc />
    public bool ContainsKey(TKey key) => control.Read(() => _dict.ContainsKey(key));
    /// <inheritdoc />
    public bool Remove(TKey key) => TryRemove(key, out _);
    /// <inheritdoc />
    bool IDictionary<TKey, TValue>.TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value) => TryGetValue(key, out value);
    /// <inheritdoc />
    public void Add(KeyValuePair<TKey, TValue> item) => Add(item.Key, item.Value);
    /// <inheritdoc />
    public bool Contains(KeyValuePair<TKey, TValue> item) => ContainsKey(item.Key);
    /// <inheritdoc />
    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => throw new NotImplementedException();
    /// <inheritdoc />
    public bool Remove(KeyValuePair<TKey, TValue> item) => Remove(item.Key);
    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion Keys & Values & GetEnumerator & Count

    #region GetHashCode Equals
    /// <inheritdoc />
    public override int GetHashCode() => control.Read(() => _dict.GetHashCode());
    /// <inheritdoc />
    public override bool Equals(object? obj) {
        if (obj is not CacheTable<TKey, TValue> ct) return false;
        return GetHashCode() == ct.GetHashCode();
    }
    #endregion

    #region Dispose
    /// <inheritdoc />
    public void Dispose() {
        lock (_lockObject) {
            Dispose(true);
        }
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing) {
        if (!_disposed) {
            if (disposing) {
                // Dispose managed resources.
                try {
                    Clear();
                    if (_internalTimer && Timer != null) Timer.Dispose();
                } catch { }
                // Timer = null;
                // _dict = null;
            }
            // Call the appropriate methods to clean up
            // unmanaged resources here.
            _disposed = true;
        }
    }
    #endregion Dispose
}

#region CacheTableContent (Unupdated)
///// <summary>Caches the Hashtable output from a delegate for specifed amount of time, and refill the table if data is required after expiry</summary>
///// <typeparam name="TKey">Key</typeparam>
///// <typeparam name="TValue">Value</typeparam>
//public partial class CacheTableContent<TKey, TValue> : ConcurrentDictionary<TKey, TValue>, IEnumerable<KeyValuePair<TKey, TValue>>
//{
//    #region Entity
//    /// <summary>Time to live (cache time) in milliseconds</summary>
//    public int Ttl { get; set; }
//    /// <summary>Delegate to fill or refill the table (if null, only current data will exist until expiry and no refills will execute)</summary>
//    public Func<object, IDictionary<TKey, TValue>> Refill { get; set; }
//    /// <summary>Parameter to be passed onto the delegate</summary>
//    public object Data { get; set; }
//    /// <summary>Checks if the table cache has expired or not</summary>
//    public bool IsExpired { get { return (tmrTimeout == null); } }
//    private Timer tmrTimeout { get; set; }
//    private object lockRefresh = new object();
//    private object lockTimer = new object();

//    /// <summary>Constructor</summary>
//    /// <param name="ttl">Time to live (cache time) in milliseconds</param>
//    /// <param name="refill">Delegate to fill or refill the table (if null, only current data will exist until expiry and no refills will execute)</param>
//    /// <param name="data">Parameter to be passed onto the delegate</param>
//    public CacheTableContent(int ttl = 120000, Func<object, IDictionary<TKey, TValue>> refill = null, object data = null)
//    {
//        Ttl = ttl;
//        Refill = refill;
//        Data = data;
//    }
//    #endregion Entity

//    private void check(bool restartTmr = false)
//    {
//        lock (lockRefresh) {
//            if (IsExpired) Refresh();
//            if (restartTmr) restartTimer();
//        }
//    }
//    /// <summary>Clear and Refill the table</summary>
//    public void Refresh()
//    {
//        if (Refill != null) {
//            Clear();
//            this.AddUnique(Refill(Data));
//            restartTimer();
//        }
//    }

//    #region Timer Operations
//    private void startTimer()
//    {
//        lock (lockTimer) {
//            if (tmrTimeout == null) {
//                tmrTimeout = new Timer(Ttl);
//                tmrTimeout.Elapsed += tmrTimeout_Elapsed;
//            }
//                //tmrTimeout_Elapsed, null, Ttl, Ttl);
//        }
//    }
//    private void stopTimer()
//    {
//        lock (lockTimer) {
//            if (tmrTimeout == null) return;
//            tmrTimeout.Dispose();
//            tmrTimeout = null;
//            tmrTimeout.Start();
//        }
//    }
//    private void restartTimer()
//    {
//        lock (lockTimer) {
//            if (tmrTimeout == null) {
//                startTimer();
//            }
//            else {
//                stopTimer();
//                startTimer();
//            }
//        }
//    }
//    private void tmrTimeout_Elapsed(object sender, ElapsedEventArgs args)
//    {
//        stopTimer();
//        this.Clear();
//    }
//    #endregion Timer Operations

//    #region Keys & Values & GetEnumerator & Count
//    /// <inheritdoc/>
//    public new TValue this[TKey key]
//    {
//        get
//        {
//            check();
//            TValue rval = default(TValue);
//            if (!this.TryGetValue(key, out rval)) return default(TValue);
//            restartTimer();
//            return rval;
//        }
//        set
//        {
//            check(true);
//            base[key] = value;
//        }
//    }
//    /// <inheritdoc/>
//    public virtual new ICollection<TKey> Keys { get { check(); return base.Keys; } }
//    /// <inheritdoc/>
//    public virtual new ICollection<TValue> Values { get { check(true); return base.Values; } }
//    /// <inheritdoc/>
//    public virtual new IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() { check(true); return base.GetEnumerator(); }
//    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() { return GetEnumerator(); }
//    /// <inheritdoc/>
//    public virtual new int Count
//    {
//        get
//        {
//            check();
//            return base.Count;
//        }
//    }
//    #endregion Keys & Values & GetEnumerator & Count

//    /// <inheritdoc/>
//    public new virtual bool TryAdd(TKey key, TValue value)
//    {
//        check(true);
//        restartTimer();
//        return base.TryAdd(key, value);
//    }
//}
#endregion