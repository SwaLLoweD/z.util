﻿/* Copyright (c) <2018> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Buffers;
using Z.Extensions;

namespace Z.Util;

/// <summary>BinaryWriter for Varint variable format (does not pack unnulable bool, byte, sbyte, char types)</summary>
public class BinaryWriterVar : BinaryWriter
{
    /// <summary>Initializes a new instance of the System.IO.BinaryReader class based on the specified stream and using UTF-8 encoding.</summary>
    /// <param name="input">The input stream.</param>
    public BinaryWriterVar(Stream input) : base(input) { }

    /// <summary>Initializes a new instance of the System.IO.BinaryReader class based on the specified stream and using UTF-8 encoding.</summary>
    /// <param name="input">The input stream.</param>
    /// <param name="encoding">The character encoding to use.</param>
    public BinaryWriterVar(Stream input, Encoding encoding) : base(input, encoding) { }
    /// <summary>Initializes a new instance of the System.IO.BinaryReader class based on the specified stream and using UTF-8 encoding.</summary>
    /// <param name="input">The input stream.</param>
    /// <param name="encoding">The character encoding to use.</param>
    /// <param name="leaveOpen">true to leave the stream open after the System.IO.BinaryReader object is disposed; otherwise false</param>
    public BinaryWriterVar(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding) { }

    #region Unused
    ///// <inheritdoc/>
    //protected Stream OutStream;
    ///// <inheritdoc/>
    //public virtual Stream BaseStream { get; }
    ///// <inheritdoc/>
    //public virtual void Close();
    ///// <inheritdoc/>
    //public void Dispose();
    ///// <inheritdoc/>
    //public virtual void Flush();
    ///// <inheritdoc/>
    //public virtual long Seek(int offset, SeekOrigin origin);
    ///// <inheritdoc/>
    //protected virtual void Dispose(bool disposing);
    ///// <inheritdoc/>
    //protected void Write7BitEncodedInt(int value);

    ///// <inheritdoc/>
    //public override void Write(string value);
    ///// <inheritdoc/>
    //public override void Write(char[] chars, int index, int count);
    ///// <inheritdoc/>
    //public override void Write(char[] chars);
    ///// <inheritdoc/>
    //public override void Write(byte[] buffer, int index, int count);
    ///// <inheritdoc/>
    //public override void Write(byte[] buffer);
    #endregion

    #region Writers
    /// <inheritdoc/>
    public override void Write(sbyte value) => base.Write(value);
    /// <inheritdoc/>
    public virtual void WriteVar(short value) => WriteVar((ulong)EncodeZigZag(value, 16));
    /// <inheritdoc/>
    public virtual void WriteVar(int value) => WriteVar((ulong)EncodeZigZag(value, 32));
    /// <inheritdoc/>
    public virtual void WriteVar(long value) => WriteVar((ulong)EncodeZigZag(value, 64));

    /// <inheritdoc/>
    public virtual void WriteVar(float value) => writeFraction(this, BitConverter.GetBytes(value));
    /// <inheritdoc/>
    public virtual void WriteVar(double value) => writeFraction(this, BitConverter.GetBytes(value));
    /// <inheritdoc/>
    public virtual void WriteVar(decimal value) => writeFraction(this, value);

    /// <inheritdoc/>
    public virtual void WriteVar(bool value) => base.Write(value);
    /// <inheritdoc/>
    public virtual void WriteVar(char ch) => base.Write(ch);
    /// <inheritdoc/>
    public virtual void WriteVar(byte value) => base.Write(value);
    /// <inheritdoc/>
    public virtual void WriteVar(ushort value) => WriteVar((ulong)value);
    /// <inheritdoc/>
    public virtual void WriteVar(uint value) => WriteVar((ulong)value);
    /// <inheritdoc/>
    public virtual void WriteVar(ulong value) {
        do {
            var byteVal = value & 0x7f;
            value >>= 7;

            if (value != 0)
                byteVal |= 0x80;

            base.Write((byte)byteVal);

        } while (value != 0);
    }
    #endregion

    #region Nullables
    /// <inheritdoc/>
    public virtual void WriteVar(sbyte? value) => WriteVar((ulong?)EncodeZigZag(value, 8));
    /// <inheritdoc/>
    public virtual void WriteVar(short? value) => WriteVar((ulong?)EncodeZigZag(value, 16));
    /// <inheritdoc/>
    public virtual void WriteVar(int? value) => WriteVar((ulong?)EncodeZigZag(value, 32));
    /// <inheritdoc/>
    public virtual void WriteVar(long? value) => WriteVar((ulong?)EncodeZigZag(value, 64));

    /// <inheritdoc/>
    public virtual void WriteVar(float? value) => writeFraction(this, value.HasValue ? BitConverter.GetBytes(value.Value) : null, true);
    /// <inheritdoc/>
    public virtual void WriteVar(double? value) => writeFraction(this, value.HasValue ? BitConverter.GetBytes(value.Value) : null, true);
    /// <inheritdoc/>
    public virtual void WriteVar(decimal? value) => writeFraction(this, value, true);

    /// <inheritdoc/>
    public virtual void WriteVar(bool? value) => WriteVar((byte)(value.HasValue ? (value.Value ? 1 : 0) : 0x80));
    /// <inheritdoc/>
    public virtual void WriteVar(char? ch) => WriteVar((ulong?)ch);
    /// <inheritdoc/>
    public virtual void WriteVar(byte? value) => WriteVar((ulong?)value);
    /// <inheritdoc/>
    public virtual void WriteVar(ushort? value) => WriteVar((ulong?)value);
    /// <inheritdoc/>
    public virtual void WriteVar(uint? value) => WriteVar((ulong?)value);
    /// <inheritdoc/>
    public virtual void WriteVar(ulong? value) {
        if (value == null)
            base.Write((byte)0x80); //null flag
        else if (value.Value < 0x40)
            base.Write((byte)(value.Value)); //with two flags gone only 6 bits are left yet this fits
        else {
            var portion = value.Value & 0x3f;
            base.Write((byte)(portion | 0x40)); //add has continuation flag
            var rest = value.Value >> 6; //this part is written;
            WriteVar(rest);  //do the rest normally
        }
    }

    #endregion

    #region Helpers
    private static void writeFraction(BinaryWriter writer, byte[]? fractionBytes, bool isNullable = false) {
        // Float  &double numeric formats stores valuable bytes from right to left
        //var valueBuff = BitConverter.GetBytes(value);

        if (isNullable && fractionBytes == null) {
            writer.Write((byte)0x80);
            return;
        }

        for (int i = 0; i <= fractionBytes.Length - 2; i++) {
            if (fractionBytes[i] == 0) continue;
            writer.Write((byte)(fractionBytes.Length - i));
            for (int j = i; j < fractionBytes.Length; j++)
                writer.Write(fractionBytes[j]);
            return;
        }
        var lastValue = fractionBytes[fractionBytes.Length - 1];
        //Will it fit with packed flag set
        if (isNullable ? lastValue < 0x40 : lastValue < 0x80) {
            // set the flag inside
            lastValue |= (byte)(isNullable ? 0x40 : 0x80);
            writer.Write(lastValue);
        } else {
            writer.Write((byte)1);
            writer.Write(lastValue);
        }
    }
    private static void writeFraction(BinaryWriter writer, decimal? value, bool isNullable = false) {
        if (isNullable && value == null) {
            writer.Write((byte)0x80);
            return;
        }

        var bits = decimal.GetBits(value.Value);
        var bitsSize = 16;

        ArrayPool<byte>.Shared.RentDo(bitsSize, (bitsArray) => {
            for (byte i = 0; i < bits.Length; i++) {
                var bytes = BitConverter.GetBytes(bits[i]);
                Array.Copy(bytes, 0, bitsArray, i * 4, 4);
            }
            // finding the empty characters
            for (int i = bitsSize - 1; i > 0; i--) {
                if (bitsArray[i] == 0) continue;
                writer.Write((byte)(i + 1));
                for (int j = 0; j <= i; j++)
                    writer.Write(bitsArray[j]);
                return;
            }
            var lastValue = bitsArray[0];
            //Will it fit with packed flag set
            if (isNullable ? lastValue < 0x40 : lastValue < 0x80) {
                // set the flag inside
                lastValue |= (byte)(isNullable ? 0x40 : 0x80);
                writer.Write(lastValue);
            } else {
                writer.Write((byte)1);
                writer.Write(lastValue);
            } 
        });
    }

    private static long EncodeZigZag(long value, int bitLength) => BitConverterVar.EncodeZigZag(value, bitLength);
    private static long? EncodeZigZag(long? value, int bitLength) => BitConverterVar.EncodeZigZag(value, bitLength);
    #endregion;
}
