﻿/* Copyright (c) <2018> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Z.Util;

/// <summary>BinaryReader for Varint variable format (does not pack unnulable bool, byte, sbyte, char types)</summary>
public class BinaryReaderVar : BinaryReader
{
    /// <summary>Initializes a new instance of the System.IO.BinaryReader class based on the specified stream and using UTF-8 encoding.</summary>
    /// <param name="input">The input stream.</param>
    public BinaryReaderVar(Stream input) : base(input) { }

    /// <summary>Initializes a new instance of the System.IO.BinaryReader class based on the specified stream and using UTF-8 encoding.</summary>
    /// <param name="input">The input stream.</param>
    /// <param name="encoding">The character encoding to use.</param>
    public BinaryReaderVar(Stream input, Encoding encoding) : base(input, encoding) { }
    /// <summary>Initializes a new instance of the System.IO.BinaryReader class based on the specified stream and using UTF-8 encoding.</summary>
    /// <param name="input">The input stream.</param>
    /// <param name="encoding">The character encoding to use.</param>
    /// <param name="leaveOpen">true to leave the stream open after the System.IO.BinaryReader object is disposed; otherwise false</param>
    public BinaryReaderVar(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding) { }

    #region Unused
    //public virtual Stream BaseStream { get; }
    //public virtual void Close();
    //public void Dispose();
    //public virtual int PeekChar();
    //public virtual int Read();

    ///// <inheritdoc/>
    //public virtual int Read(byte[] buffer, int index, int count);
    ///// <inheritdoc/>
    //public virtual int Read(char[] buffer, int index, int count);
    ///// <inheritdoc/>
    //public override byte ReadByte() => (byte) ToTarget(8);
    ///// <inheritdoc/>
    //public override byte[] ReadBytes(int count)
    //{
    //    var rval = new byte[count];
    //    for (int i = 0; i < count; i++)
    //        rval[i] = ReadByte();
    //    return rval;
    //}
    ///// <inheritdoc/>
    //public override char ReadChar() => (char)ToTarget(8);
    ///// <inheritdoc/>
    //public override char[] ReadChars(int count)
    //{
    //    var rval = new char[count];
    //    for (int i = 0; i < count; i++)
    //        rval[i] = ReadChar();
    //    return rval;
    //}
    ///// <inheritdoc/>
    //public override string ReadString();
    //protected virtual void Dispose(bool disposing);
    //protected virtual void FillBuffer(int numBytes);
    //protected internal int Read7BitEncodedInt();
    #endregion

    #region Readers
    /// <inheritdoc/>
    public override sbyte ReadSByte() => base.ReadSByte(); //(sbyte)DecodeZigZag(ToTarget(8));
    /// <inheritdoc/>
    public virtual short ReadVarInt16() => (short)DecodeZigZag(ToTarget(this, 16));
    /// <inheritdoc/>
    public virtual int ReadVarInt32() => (int)DecodeZigZag(ToTarget(this, 32));
    /// <inheritdoc/>
    public virtual long ReadVarInt64() => (long)DecodeZigZag(ToTarget(this, 64));

    /// <inheritdoc/>
    public virtual float ReadVarSingle() => (float)ToTargetFraction(this, 32);
    /// <inheritdoc/>
    public virtual double ReadVarDouble() => (double)ToTargetFraction(this, 64);
    /// <inheritdoc/>
    public virtual decimal ReadVarDecimal() => (decimal)ToTargetDecimal(this);

    /// <inheritdoc/>
    public override bool ReadBoolean() => base.ReadBoolean();
    /// <inheritdoc/>
    public override char ReadChar() => base.ReadChar(); //(char)ToTarget(8);
    /// <inheritdoc/>
    public override byte ReadByte() => base.ReadByte(); //(byte)ToTarget(8);
    /// <inheritdoc/>
    public virtual ushort ReadVarUInt16() => (ushort)ToTarget(this, 16);
    /// <inheritdoc/>
    public virtual uint ReadVarUInt32() => (uint)ToTarget(this, 32);
    /// <inheritdoc/>
    public virtual ulong ReadVarUInt64() => ToTarget(this, 64);
    #endregion

    #region Nullables
    /// <inheritdoc/>
    public virtual sbyte? ReadNVarSByte() => (sbyte?)DecodeZigZag(ToTargetN(this, 8));
    /// <inheritdoc/>
    public virtual short? ReadNVarInt16() => (short?)DecodeZigZag(ToTargetN(this, 16));
    /// <inheritdoc/>
    public virtual int? ReadNVarInt32() => (int?)DecodeZigZag(ToTargetN(this, 32));
    /// <inheritdoc/>
    public virtual long? ReadNVarInt64() => (long?)DecodeZigZag(ToTargetN(this, 64));

    /// <inheritdoc/>
    public virtual float? ReadNVarSingle() => (float?)ToTargetFraction(this, 32, true);
    /// <inheritdoc/>
    public virtual double? ReadNVarDouble() => ToTargetFraction(this, 64, true);
    /// <inheritdoc/>
    public virtual decimal? ReadNVarDecimal() => ToTargetDecimal(this, true);

    /// <inheritdoc/>
    public virtual bool? ReadNVarBoolean() {
        var val = base.ReadByte();
        if ((val & 0x80) != 0) return null;
        return val == 1;
    }
    /// <inheritdoc/>
    public virtual char? ReadNVarChar() => (char?)ToTargetN(this, 8);
    /// <inheritdoc/>
    public virtual byte? ReadNVarByte() => (byte?)ToTargetN(this, 8);
    /// <inheritdoc/>
    public virtual ushort? ReadNVarUInt16() => (ushort?)ToTargetN(this, 16);
    /// <inheritdoc/>
    public virtual uint? ReadNVarUInt32() => (uint?)ToTargetN(this, 32);
    /// <inheritdoc/>
    public virtual ulong? ReadNVarUInt64() => ToTargetN(this, 64);
    #endregion

    #region Helpers
    private static ulong ToTarget(BinaryReader reader, int sizeBites) {
        int shift = 0;
        ulong result = 0;
        int i = 0;
        ulong byteValue = 0;
        do {
            byteValue = reader.ReadByte();
            ulong tmp = byteValue & 0x7f;
            result |= tmp << shift;

            if (shift > sizeBites) throw new ArgumentOutOfRangeException("bytes", "Byte array is too large.");
            if (i++ > 10) throw new Exception("Invalid integer long in the input stream.");

            shift += 7;
        } while ((byteValue & 0x80) != 0);

        return result;
    }
    private static ulong? ToTargetN(BinaryReader reader, int sizeBites) {
        var input = reader.ReadByte();
        if ((input & 0x80) != 0) //null check at first byte (only at first byte)
            return null;
        if ((input & 0x40) == 0) //No continuation flag (second flag only at first byte)
            return (ulong)(input & 0x3f);
        int shift = 6; //2 flags are used 8-2= 6 for next
        ulong result = (ulong)(input & 0x3f);
        int i = 1;
        ulong byteValue = 0;
        do {
            byteValue = reader.ReadByte();
            ulong tmp = byteValue & 0x7f;
            result |= tmp << shift;

            if (shift > sizeBites) throw new ArgumentOutOfRangeException("bytes", "Byte array is too large.");
            if (i++ > 10) throw new Exception("Invalid integer long in the input stream.");

            shift += 7;
        } while ((byteValue & 0x80) != 0);

        return result;
    }

    private static double? ToTargetFraction(BinaryReader reader, int sizeBites, bool isNullable = false) {
        var sizeBytes = sizeBites / 8;
        var input = reader.ReadByte();
        byte[] bytes = null;
        if (isNullable && ((input & 0x80) != 0))
            return null;
        if (isNullable ? (input & 0x40) != 0 : (input & 0x80) != 0) {
            bytes = new byte[sizeBytes];
            bytes[sizeBytes - 1] = (byte)(isNullable ? input & 0x3f : input & 0x7f);
        } else {
            bytes = new byte[sizeBytes];
            for (int i = 0; i < input; i++)
                bytes[bytes.Length - input + i] = reader.ReadByte();
        }
        return bytes.Length <= 4 ? BitConverter.ToSingle(bytes, 0) : BitConverter.ToDouble(bytes, 0);
    }
    private static decimal? ToTargetDecimal(BinaryReader reader, bool isNullable = false) {
        var input = reader.ReadByte();
        if (isNullable && ((input & 0x80) != 0))
            return null;
        int[] decimalBits = new int[4];
        if (isNullable ? (input & 0x40) != 0 : (input & 0x80) != 0) { //Read packed
            input &= (byte)(isNullable ? 0x3f : 0x7f);
            if (input == 0)
                return 0m;
            decimalBits[0] = input;
        } else {
            decimalBits = new int[4];
            var shift = 0;
            for (int i = 0; i < input; i++) {
                shift = shift % 32;
                var di = i / 4;
                decimalBits[di] |= reader.ReadByte() << shift;
                shift = (shift + 8) % 32;
            }
        }
        return new decimal(decimalBits);
    }

    private static long DecodeZigZag(ulong value) => BitConverterVar.DecodeZigZag(value);
    private static long? DecodeZigZag(ulong? value) => BitConverterVar.DecodeZigZag(value);
    #endregion
}
