﻿/*
VarintBitConverter:
https://github.com/topas/VarintBitConverter 
Copyright (c) 2011 Tomas Pastorek, Ixone.cz. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

THIS SOFTWARE IS PROVIDED BY TOMAS PASTOREK AND CONTRIBUTORS ``AS IS'' 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL TOMAS PASTOREK OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE. 
*/

using System;
using System.Buffers;

namespace Z.Util;

/// <summary>
///     Static utility class very similar to BitConverter. Varint is 128 base encoding for numbers. For signed numbers
///     is using ZigZag encoding.
/// </summary>
public static class BitConverterVar
{
    #region Value to Byte
    /// <summary>Returns the specified byte value as varint encoded array of bytes.</summary>
    /// <param name="value">Byte value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(sbyte value) => GetBytes((ulong)EncodeZigZag(value, 8));
    /// <summary>Returns the specified 16-bit signed value as varint encoded array of bytes.</summary>
    /// <param name="value">16-bit signed value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(short value) => GetBytes((ulong)EncodeZigZag(value, 16));
    /// <summary>Returns the specified 32-bit signed value as varint encoded array of bytes.</summary>
    /// <param name="value">32-bit signed value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(int value) => GetBytes((ulong)EncodeZigZag(value, 32));
    /// <summary>Returns the specified 64-bit signed value as varint encoded array of bytes.</summary>
    /// <param name="value">64-bit signed value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(long value) => GetBytes((ulong)EncodeZigZag(value, 64));

    /// <summary>Returns the specified float value as varint encoded array of bytes.</summary>
    /// <param name="value">Float value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(float value) => WriteFraction(BitConverter.GetBytes(value));
    /// <summary>Returns the specified double value as varint encoded array of bytes.</summary>
    /// <param name="value">Double value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(double value) => WriteFraction(BitConverter.GetBytes(value));
    /// <summary>Returns the specified decimal value as varint encoded array of bytes.</summary>
    /// <param name="value">Decimal value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(decimal value) => WriteFraction(value);

    /// <summary>Returns the specified bool value as varint encoded array of bytes.</summary>
    /// <param name="value">Bool value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(bool value) => new[] { (byte)(value ? 1 : 0) };
    /// <summary>Returns the specified char value as varint encoded array of bytes.</summary>
    /// <param name="value">Char value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(char value) => GetBytes((ulong)value);
    /// <summary>Returns the specified byte value as varint encoded array of bytes.</summary>
    /// <param name="value">Byte value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(byte value) => GetBytes((ulong)value);
    /// <summary>Returns the specified 16-bit unsigned value as varint encoded array of bytes.</summary>
    /// <param name="value">16-bit unsigned value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(ushort value) => GetBytes((ulong)value);
    /// <summary>Returns the specified 32-bit unsigned value as varint encoded array of bytes.</summary>
    /// <param name="value">32-bit unsigned value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(uint value) => GetBytes((ulong)value);
    /// <summary>Returns the specified 64-bit unsigned value as varint encoded array of bytes.</summary>
    /// <param name="value">64-bit unsigned value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(ulong value) {
        var buffer = new byte[10];
        var pos = 0;
        do {
            var byteVal = value & 0x7f;
            value >>= 7;

            if (value != 0) byteVal |= 0x80;

            buffer[pos++] = (byte)byteVal;
        } while (value != 0);
        return buffer.AsSpan()[..pos].ToArray();
        // var result = new byte[pos];
        // Buffer.BlockCopy(buffer, 0, result, 0, pos);

        // return result;

        //Alternative
        //using (var mem = new MemoryStream(new byte[10])) {
        //    using (var writer = new BinaryWriterVar(mem)) {
        //        writer.Write(value);
        //        return mem.ToArray();
        //    }
        //}
    }
    #endregion

    #region Nullable Value to Byte
    /// <summary>Returns the specified byte value as varint encoded array of bytes.</summary>
    /// <param name="value">Byte value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(sbyte? value) => GetBytes((ulong?)EncodeZigZag(value, 8));
    /// <summary>Returns the specified 16-bit signed value as varint encoded array of bytes.</summary>
    /// <param name="value">16-bit signed value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(short? value) => GetBytes((ulong?)EncodeZigZag(value, 16));
    /// <summary>Returns the specified 32-bit signed value as varint encoded array of bytes.</summary>
    /// <param name="value">32-bit signed value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(int? value) => GetBytes((ulong?)EncodeZigZag(value, 32));
    /// <summary>Returns the specified 64-bit signed value as varint encoded array of bytes.</summary>
    /// <param name="value">64-bit signed value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(long? value) => GetBytes((ulong?)EncodeZigZag(value, 64));

    /// <summary>Returns the specified float value as varint encoded array of bytes.</summary>
    /// <param name="value">Float value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(float? value) => WriteFraction(value.HasValue ? BitConverter.GetBytes(value.Value) : null, true);
    /// <summary>Returns the specified double value as varint encoded array of bytes.</summary>
    /// <param name="value">Double value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(double? value) => WriteFraction(value.HasValue ? BitConverter.GetBytes(value.Value) : null, true);
    /// <summary>Returns the specified decimal value as varint encoded array of bytes.</summary>
    /// <param name="value">Decimal value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(decimal? value) => WriteFraction(value, true);

    /// <summary>Returns the specified bool value as varint encoded array of bytes.</summary>
    /// <param name="value">Bool value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(bool? value) => new[] { value.HasValue ? (byte)(value.Value ? 1 : 0) : (byte)0x80 };
    /// <summary>Returns the specified char value as varint encoded array of bytes.</summary>
    /// <param name="value">Char value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(char? value) => GetBytes((ulong?)value);
    /// <summary>Returns the specified byte value as varint encoded array of bytes.</summary>
    /// <param name="value">Byte value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(byte? value) => GetBytes((ulong?)value);
    /// <summary>Returns the specified 16-bit unsigned value as varint encoded array of bytes.</summary>
    /// <param name="value">16-bit unsigned value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(ushort? value) => GetBytes((ulong?)value);
    /// <summary>Returns the specified 32-bit unsigned value as varint encoded array of bytes.</summary>
    /// <param name="value">32-bit unsigned value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(uint? value) => GetBytes((ulong?)value);
    /// <summary>Returns the specified 64-bit unsigned value as varint encoded array of bytes.</summary>
    /// <param name="value">64-bit unsigned value</param>
    /// <returns>Varint array of bytes.</returns>
    public static byte[] GetBytes(ulong? value) {
        var buffer = new byte[10];
        var pos = 0;
        if (value == null) {
            buffer[pos++] = 0x80; //null flag
        } else if (value.Value < 0x40) {
            buffer[pos++] = (byte)value.Value; //with two flags gone only 6 bits are left yet this fits
        } else {
            var portion = value.Value & 0x3f;
            buffer[pos++] = (byte)(portion | 0x40); //add has continuation flag
            var rest = value.Value >> 6; //this part is written;
            var restBytes = GetBytes(rest); //do the rest normally
            restBytes.CopyTo(buffer.AsSpan(pos, restBytes.Length));
            //Buffer.BlockCopy(restBytes, 0, buffer, pos, restBytes.Length);
            pos += restBytes.Length;
        }
        return buffer.AsSpan()[..pos].ToArray();

        // var result = new byte[pos];
        // Buffer.BlockCopy(buffer, 0, result, 0, pos);
        // return result;

        ////Alternative
        //using (var mem = new MemoryStream(new byte[10])) {
        //    using (var writer = new BinaryWriterVar(mem)) {
        //        writer.WriteN(value);
        //        return mem.ToArray();
        //    }
        //}
    }
    #endregion

    #region Byte to Value with byte size
    /// <summary>Returns signed byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>Signed Byte value</returns>
    public static sbyte ToSbyte(ReadOnlySpan<byte> bytes, out int size) => (sbyte)DecodeZigZag(ToTarget(bytes, 8, out size));
    /// <summary>Returns 16-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>16-bit signed value</returns>
    public static short ToInt16(ReadOnlySpan<byte> bytes, out int size) => (short)DecodeZigZag(ToTarget(bytes, 16, out size));
    /// <summary>Returns 32-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>32-bit signed value</returns>
    public static int ToInt32(ReadOnlySpan<byte> bytes, out int size) => (int)DecodeZigZag(ToTarget(bytes, 32, out size));
    /// <summary>Returns 64-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>64-bit signed value</returns>
    public static long ToInt64(ReadOnlySpan<byte> bytes, out int size) => DecodeZigZag(ToTarget(bytes, 64, out size));

    /// <summary>Returns float value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>Float value</returns>
    public static float ToSingle(ReadOnlySpan<byte> bytes, out int size) => (float?)ToTargetFraction(bytes, 32, out size) ?? 0f;
    /// <summary>Returns double value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>Double value</returns>
    public static double ToDouble(ReadOnlySpan<byte> bytes, out int size) => ToTargetFraction(bytes, 64, out size) ?? 0d;
    /// <summary>Returns decimal value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>Decimal value</returns>
    public static decimal ToDecimal(ReadOnlySpan<byte> bytes, out int size) => ToTargetDecimal(bytes, out size) ?? decimal.Zero;

    /// <summary>Returns bool value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>Bool value</returns>
    public static bool ToBool(ReadOnlySpan<byte> bytes, out int size) { size = 1; return bytes[0] == 1; }
    /// <summary>Returns byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>Char value</returns>
    public static char ToChar(ReadOnlySpan<byte> bytes, out int size) => (char)ToTarget(bytes, 8, out size);
    /// <summary>Returns byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>Byte value</returns>
    public static byte ToByte(ReadOnlySpan<byte> bytes, out int size) => (byte)ToTarget(bytes, 8, out size);
    /// <summary>Returns 16-bit usigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>16-bit usigned value</returns>
    public static ushort ToUInt16(ReadOnlySpan<byte> bytes, out int size) => (ushort)ToTarget(bytes, 16, out size);
    /// <summary>Returns 32-bit unsigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>32-bit unsigned value</returns>
    public static uint ToUInt32(ReadOnlySpan<byte> bytes, out int size) => (uint)ToTarget(bytes, 32, out size);
    /// <summary>Returns 64-bit unsigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <param name="size">Number of bytes processed.</param>
    /// <returns>64-bit unsigned value</returns>
    public static ulong ToUInt64(ReadOnlySpan<byte> bytes, out int size) => ToTarget(bytes, 64, out size);

    #endregion

    #region Byte to Value
    /// <summary>Returns signed byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Signed Byte value</returns>
    public static sbyte ToSbyte(ReadOnlySpan<byte> bytes) => (sbyte)DecodeZigZag(ToTarget(bytes, 8, out _));
    /// <summary>Returns 16-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>16-bit signed value</returns>
    public static short ToInt16(ReadOnlySpan<byte> bytes) => (short)DecodeZigZag(ToTarget(bytes, 16, out _));
    /// <summary>Returns 32-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>32-bit signed value</returns>
    public static int ToInt32(ReadOnlySpan<byte> bytes) => (int)DecodeZigZag(ToTarget(bytes, 32, out _));
    /// <summary>Returns 64-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>64-bit signed value</returns>
    public static long ToInt64(ReadOnlySpan<byte> bytes) => DecodeZigZag(ToTarget(bytes, 64, out _));

    /// <summary>Returns float value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Float value</returns>
    public static float ToSingle(ReadOnlySpan<byte> bytes) => (float?) ToTargetFraction(bytes, 32, out _) ?? 0f;
    /// <summary>Returns double value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Double value</returns>
    public static double ToDouble(ReadOnlySpan<byte> bytes) => ToTargetFraction(bytes, 64, out _) ?? 0d;
    /// <summary>Returns decimal value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Decimal value</returns>
    public static decimal ToDecimal(ReadOnlySpan<byte> bytes) => ToTargetDecimal(bytes, out _) ?? decimal.Zero;

    /// <summary>Returns bool value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Bool value</returns>
    public static bool ToBool(ReadOnlySpan<byte> bytes) => bytes[0] == 1;
    /// <summary>Returns byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Char value</returns>
    public static char ToChar(ReadOnlySpan<byte> bytes) => (char)ToTarget(bytes, 8, out _);
    /// <summary>Returns byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Byte value</returns>
    public static byte ToByte(ReadOnlySpan<byte> bytes) => (byte)ToTarget(bytes, 8, out _);
    /// <summary>Returns 16-bit usigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>16-bit usigned value</returns>
    public static ushort ToUInt16(ReadOnlySpan<byte> bytes) => (ushort)ToTarget(bytes, 16, out _);
    /// <summary>Returns 32-bit unsigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>32-bit unsigned value</returns>
    public static uint ToUInt32(ReadOnlySpan<byte> bytes) => (uint)ToTarget(bytes, 32, out _);
    /// <summary>Returns 64-bit unsigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>64-bit unsigned value</returns>
    public static ulong ToUInt64(ReadOnlySpan<byte> bytes) => ToTarget(bytes, 64, out _);
    #endregion

    #region Nullable Byte to Value
    /// <summary>Returns signed byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Signed Byte value</returns>
    public static sbyte? ToNSbyte(ReadOnlySpan<byte> bytes) => (sbyte?)DecodeZigZag(ToTargetN(bytes, 8, out _));
    /// <summary>Returns 16-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>16-bit signed value</returns>
    public static short? ToNInt16(ReadOnlySpan<byte> bytes) => (short?)DecodeZigZag(ToTargetN(bytes, 16, out _));
    /// <summary>Returns 64-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>64-bit signed value</returns>
    public static long? ToNInt64(ReadOnlySpan<byte> bytes) => DecodeZigZag(ToTargetN(bytes, 64, out _));
    /// <summary>Returns 32-bit signed value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>32-bit signed value</returns>
    public static int? ToNInt32(ReadOnlySpan<byte> bytes) => (int?)DecodeZigZag(ToTargetN(bytes, 32, out _));

    /// <summary>Returns float value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Float value</returns>
    public static float? ToNSingle(ReadOnlySpan<byte> bytes) => (float?)ToTargetFraction(bytes, 32, out _, true);
    /// <summary>Returns double value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Double value</returns>
    public static double? ToNDouble(ReadOnlySpan<byte> bytes) => ToTargetFraction(bytes, 64, out _, true);
    /// <summary>Returns decimal value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Decimal value</returns>
    public static decimal? ToNDecimal(ReadOnlySpan<byte> bytes) => ToTargetDecimal(bytes, out _, true);

    /// <summary>Returns bool value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Bool value</returns>
    public static bool? ToNBool(ReadOnlySpan<byte> bytes) => (bytes[0] & 0x80) == 0 ? (bool?)(bytes[0] == 1) : null;
    /// <summary>Returns byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Char value</returns>
    public static char? ToNChar(ReadOnlySpan<byte> bytes) => (char?)ToTargetN(bytes, 8, out _);
    /// <summary>Returns byte value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>Byte value</returns>
    public static byte? ToNByte(ReadOnlySpan<byte> bytes) => (byte?)ToTargetN(bytes, 8, out _);
    /// <summary>Returns 16-bit usigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>16-bit usigned value</returns>
    public static ushort? ToNUInt16(ReadOnlySpan<byte> bytes) => (ushort?)ToTargetN(bytes, 16, out _);
    /// <summary>Returns 32-bit unsigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>32-bit unsigned value</returns>
    public static uint? ToNUInt32(ReadOnlySpan<byte> bytes) => (uint?)ToTargetN(bytes, 32, out _);
    /// <summary>Returns 64-bit unsigned value from varint encoded array of bytes.</summary>
    /// <param name="bytes">Varint encoded array of bytes.</param>
    /// <returns>64-bit unsigned value</returns>
    public static ulong? ToNUInt64(ReadOnlySpan<byte> bytes) => ToTargetN(bytes, 64, out _);
    #endregion

    #region Helpers
    private static ulong ToTarget(ReadOnlySpan<byte> bytes, int sizeBites, out int pos) {
        var shift = 0;
        ulong result = 0;
        ulong byteValue;
        pos = 0;
        do {
            byteValue = bytes[pos++];
            var tmp = byteValue & 0x7f;
            result |= tmp << shift;

            if (shift > sizeBites) throw new ArgumentOutOfRangeException(nameof(bytes), "Byte array is too large.");
            if (pos > 10) throw new Exception("Invalid integer long in the input stream.");

            shift += 7;
        } while ((byteValue & 0x80) != 0);
        //throw new ArgumentException("Cannot decode varint from byte array.", nameof(bytes));
        return result;
    }
    //uses byte[0] as a nullcheck
    private static ulong? ToTargetN(ReadOnlySpan<byte> bytes, int sizeBites, out int pos) {
        pos = 0;
        var input = bytes[pos++];
        if ((input & 0x80) != 0) //null check at first byte (only at first byte)
            return null;
        if ((input & 0x40) == 0) //No continuation flag (second flag only at first byte)
            return (ulong)(input & 0x3f);
        var shift = 6; //2 flags are used 8-2= 6 for next
        var result = (ulong)(input & 0x3f);
        ulong byteValue;
        do {
            byteValue = bytes[pos++];
            var tmp = byteValue & 0x7f;
            result |= tmp << shift;

            if (shift > sizeBites) throw new ArgumentOutOfRangeException(nameof(bytes), "Byte array is too large.");
            if (pos > 10) throw new Exception("Invalid integer long in the input stream.");

            shift += 7;
        } while ((byteValue & 0x80) != 0);

        return result;
    }
    private static double? ToTargetFraction(ReadOnlySpan<byte> reader, int sizeBites, out int pos, bool isNullable = false) {
        pos = 0;
        var sizeBytes = sizeBites / 8;
        var input = reader[pos++];
        if (isNullable && (input & 0x80) != 0) return null;
        var poolB = ArrayPool<byte>.Shared.Rent(sizeBytes);
        var bytes = poolB.AsSpan(0, sizeBytes);
        if (isNullable ? (input & 0x40) != 0 : (input & 0x80) != 0) {
            //bytes = new byte[sizeBytes];
            bytes[sizeBytes - 1] = (byte)(isNullable ? input & 0x3f : input & 0x7f);
        } else {
            //bytes = new byte[sizeBytes];
            for (var i = 0; i < input; i++) bytes[bytes.Length - input + i] = reader[pos++];
        }
        var rval =  bytes.Length <= 4 ? BitConverter.ToSingle(bytes) : BitConverter.ToDouble(bytes);
        ArrayPool<byte>.Shared.Return(poolB);
        return rval;
    }
    private static decimal? ToTargetDecimal(ReadOnlySpan<byte> reader, out int pos, bool isNullable = false) {
        pos = 0;
        var input = reader[pos++];
        if (isNullable && (input & 0x80) != 0) return null;
        var poolB = ArrayPool<int>.Shared.Rent(4);
        var decimalBits = poolB.AsSpan(0, 4);
        //var decimalBits = new int[4];
        if (isNullable ? (input & 0x40) != 0 : (input & 0x80) != 0) { //Read packed
            input &= (byte)(isNullable ? 0x3f : 0x7f);
            if (input == 0) return 0m;
            decimalBits[0] = input;
        } else {
            //decimalBits = new int[4];
            decimalBits.Clear();
            var shift = 0;
            for (var i = 0; i < input; i++) {
                shift %= 32;
                var di = i / 4;
                decimalBits[di] |= reader[pos++] << shift;
                shift = (shift + 8) % 32;
            }
        }
        var rval = new decimal(decimalBits);
        ArrayPool<int>.Shared.Return(poolB);
        return rval;
    }

    private static byte[] WriteFraction(ReadOnlySpan<byte> fractionBytes, bool isNullable = false) {
        // Float  &double numeric formats stores valuable bytes from right to left
        //var valueBuff = BitConverter.GetBytes(value);

        if (fractionBytes.IsEmpty) {
            if (isNullable) return new[] { (byte)0x80 };
            else throw new ArgumentNullException(nameof(fractionBytes));
        }

        for (var i = 0; i <= fractionBytes.Length - 2; i++) {
            if (fractionBytes[i] == 0) continue;
            var len = fractionBytes.Length - i;
            var bytes = new byte[len + 1];
            bytes[0] = (byte)len;
            fractionBytes[i..].CopyTo(bytes.AsSpan(1, len));
            //Buffer.BlockCopy(fractionBytes, i, bytes, 1, len);
            return bytes;
        }
        var lastValue = fractionBytes[^1];
        //Will it fit with packed flag set
        if (isNullable ? lastValue < 0x40 : lastValue < 0x80) {
            // set the flag inside
            lastValue |= (byte)(isNullable ? 0x40 : 0x80);
            return new[] { lastValue };
        }
        return new byte[] { 1, lastValue };
    }
    private static byte[] WriteFraction(decimal? value, bool isNullable = false) {
        if (value == null) {
            if (isNullable) return new[] { (byte)0x80 };
            else throw new ArgumentNullException(nameof(value));
        }

        var bits = decimal.GetBits(value.Value);

        var poolB = ArrayPool<byte>.Shared.Rent(16);
        var bitsArray = poolB.AsSpan(0, 16);
        //var bitsArray = new byte[16];

        for (byte i = 0; i < bits.Length; i++) {
            var bytes = BitConverter.GetBytes(bits[i]);
            bytes.AsSpan()[..4].CopyTo(bitsArray[(i * 4)..]);
            //Array.Copy(bytes, 0, bitsArray, i * 4, 4);
        }
        // finding the empty characters
        for (var i = bitsArray.Length - 1; i > 0; i--) {
            if (bitsArray[i] == 0) continue;
            var len = (byte)(i + 1);
            var bytes = new byte[len + 1];
            bytes[0] = len;
            bitsArray[..len].CopyTo(bytes.AsSpan(1));
            //Buffer.BlockCopy(bitsArray, 0, bytes, 1, len);
            return bytes;
        }
        var lastValue = bitsArray[0];
        ArrayPool<byte>.Shared.Return(poolB);
        
        //Will it fit with packed flag set
        if (isNullable ? lastValue < 0x40 : lastValue < 0x80) {
            // set the flag inside
            lastValue |= (byte)(isNullable ? 0x40 : 0x80);
            return new[] { lastValue };
        }
        return new byte[] { 1, lastValue };
    }
    #endregion

    #region ZigZag
    /// <summary>ZigZag Encoder</summary>
    public static long EncodeZigZag(long value, int bitLength) => (value << 1) ^ (value >> (bitLength - 1));
    /// <summary>ZigZag Encoder</summary>
    public static long? EncodeZigZag(long? value, int bitLength) => value.HasValue ? (long?)EncodeZigZag(value.Value, bitLength) : null;
    /// <summary>ZigZag Decoder</summary>
    public static long DecodeZigZag(ulong value) => (value & 0x1) == 0x1 ? -1 * ((long)(value >> 1) + 1) : (long)(value >> 1);
    /// <summary>ZigZag Decoder</summary>
    public static long? DecodeZigZag(ulong? value) => value.HasValue ? (long?)DecodeZigZag(value.Value) : null;
    #endregion
}