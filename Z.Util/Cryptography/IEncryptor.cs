﻿/* Copyright (c) <2019> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
using Z.Util;

namespace Z.Cryptography;

/// <summary>BlockCipher encryption engine</summary>
public interface IEncryptor : IDisposableZ
{
    #region Fields / Properties
    /// <summary>Encryption algorithm to use</summary>
    SymmetricAlgorithm Algorithm { get; }
    /// <summary>Encryption algorithm type to use</summary>
    Type AlgorithmType { get; }
    /// <summary>IV</summary>
    ReadOnlyMemory<byte> IV { get; set; }
    /// <summary>Key</summary>
    ReadOnlyMemory<byte> Key { get; set; }
    /// <summary>Encryption algorithm block size in bytes</summary>
    int BlockSize { get; set; }
    /// <summary>Encryption algorithm key size in bytes</summary>
    int KeySize { get; set; }
    /// <summary>Tag size in bytes</summary>
    int TagSize { get; }
    /// <summary>Total package extra length per encryption</summary>
    int TotalExtraLength { get; }
    #endregion

    #region Encrypt / Decrypt

    /// <summary>Encrypt data</summary>
    /// <param name="source">Data to Encrypt</param>
    /// <param name="destination">Destination stream the encryption result will be written into</param>
    /// <param name="iv">Vector</param>
    /// <param name="headless">Stream mode: write no headers or make iv checks</param>
    long Encrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false);

    /// <summary>Encrypt data</summary>
    /// <param name="source">Data to Encrypt</param>
    /// <param name="destination">Destination stream the encryption result will be written into</param>
    /// <param name="iv">Vector</param>
    /// <param name="headless">Stream mode: write no headers or make iv checks</param>
    Task<long> EncryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false);

    /// <summary>Decryption</summary>
    /// <param name="source">Data to Decrypt</param>
    /// <param name="destination">Destination stream the decryption result will be written into</param>
    /// <param name="iv">Vector</param>
    /// <param name="headless">Stream mode: write no headers or make iv checks</param>
    long Decrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false);

    /// <summary>Decryption</summary>
    /// <param name="source">Data to Decrypt</param>
    /// <param name="destination">Destination stream the decryption result will be written into</param>
    /// <param name="iv">Vector</param>
    /// <param name="headless">Stream mode: write no headers or make iv checks</param>
    Task<long> DecryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false);
    #endregion

    #region Headers
    /// <summary>Prepend information needed for decryption to encrypted byte array</summary>
    int HeaderWrite(Stream destination, ReadOnlySpan<byte> iv = default);
    /// <summary>Prepend information needed for decryption to encrypted byte array</summary>
    Task<int> HeaderWriteAsync(Stream destination, ReadOnlyMemory<byte> iv = default);
    /// <summary>Extract information needed for decryption from encrypted byte array</summary>
    int HeaderRead(Stream source, ReadOnlySpan<byte> iv = default);
    /// <summary>Extract information needed for decryption from encrypted byte array</summary>
    Task<int> HeaderReadAsync(Stream source, ReadOnlyMemory<byte> iv = default);
    /// <summary>Extract information needed for decryption from encrypted byte array</summary>
    int HeaderRead(ReadOnlySpan<byte> source, ReadOnlySpan<byte> iv = default);
    #endregion

    /// <summary>Create an encryption/decryption stream in write mode</summary>
    CryptoSafeStream CreateCryptoStream(Stream destination, bool isEncrypt);
    /// <summary>Generate an iv</summary>
    void GenerateIV();
}
