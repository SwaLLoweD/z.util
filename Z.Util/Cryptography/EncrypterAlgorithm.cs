﻿/* Copyright (c) <2022> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Buffers;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Z.Extensions;
using Z.Util;

namespace Z.Cryptography;

/// <summary>Base Encrpytor</summary>
public class EncrypterAlgorithm<T> : EncrypterAlgorithm where T : SymmetricAlgorithm
{
    /// <summary>Constructor</summary>
    public EncrypterAlgorithm(ReadOnlyMemory<byte> key = default, Encrypter? e = null, ReadOnlyMemory<byte> salt = default, CipherPrepend cipherPrepend = CipherPrepend.IV, Action<T>? algorithmOptions = null) 
    : base(key, e, salt, cipherPrepend) {
        AlgorithmType = typeof(T);
        Algorithm = Encrypter.CreateSymmetricAlgorithm<T>();
        (e ?? Encrypter.Default).DefaultOptionsApply(Algorithm);
        algorithmOptions?.Invoke((T)Algorithm);
        if (!inputKey.IsEmpty) CheckAndDeriveKey();
    }
}

/// <summary>Base Encrpytor</summary>
public class EncrypterAlgorithm : DisposableZ, IEncryptor 
{
    #region Constructors
    #pragma warning disable CS8618
    /// <inheritdoc />
    public EncrypterAlgorithm(ReadOnlyMemory<byte> key = default, Encrypter? e = null, ReadOnlyMemory<byte> salt = default, CipherPrepend cipherPrepend = CipherPrepend.IV) {
        Helper = e ?? Encrypter.Default;
        CipherPrepend = cipherPrepend;        
        inputKey = key;
        pSalt = salt.IsEmpty ? Helper.Salt : salt; //Also Calculates real key when set
        //Helper?.CacheAdd(this, inputKey);
    }
    #pragma warning restore CS8618
    #endregion

    #region Fields and Properties
    ///// <inheritdoc/>
    //public virtual int KeySize => Algorithm.KeySize;
    ///// <inheritdoc/>
    //public virtual int BlockSize => Algorithm.BlockSize;

    /// <inheritdoc />
    public virtual CipherPrepend CipherPrepend { get; }
    /// <inheritdoc />
    protected virtual Encrypter Helper { get; set; }
    /// <inheritdoc />
    protected ReadOnlyMemory<byte> pSalt;
    /// <inheritdoc />
    protected virtual ReadOnlyMemory<byte> Salt { get => pSalt; set { pSalt = value; CheckAndDeriveKey(); } }
    /// <inheritdoc />
    public virtual SymmetricAlgorithm Algorithm { get; protected set; }
    /// <inheritdoc />
    public virtual Type AlgorithmType { get; protected set; }
    /// <inheritdoc />
    public virtual ReadOnlyMemory<byte> IV { get => Algorithm.IV; set => Algorithm.IV = value.ToArray(); }
    /// <inheritdoc />
    public virtual ReadOnlyMemory<byte> Key { get => Algorithm.Key; set => Algorithm.Key = value.ToArray(); }
    /// <inheritdoc />
    public virtual int BlockSize { get => Algorithm.BlockSize / 8; set => Algorithm.BlockSize = value * 8; }
    /// <inheritdoc />
    public virtual int KeySize { get => Algorithm.KeySize / 8; set => Algorithm.KeySize = value * 8; }
    /// <inheritdoc />
    public virtual int TagSize { get; protected set; } = 0;
    /// <inheritdoc />
    public virtual int TotalExtraLength => (CipherPrepend.HasFlag(CipherPrepend.IV) ? BlockSize : 0) + (CipherPrepend.HasFlag(CipherPrepend.Salt) ? Salt.Length : 0) + TagSize;
    /// <inheritdoc />
    protected ReadOnlyMemory<byte> inputKey;
    //private byte[] finalKey = null;

    /// <summary>Element used for encyption once and may need refreshing.</summary>
    protected volatile bool usedForEncryptOnce = false;
    #endregion

    #region Helpers
    /// <summary>Check key for length if it is not adequate derive a new key</summary>
    protected virtual ReadOnlyMemory<byte> CheckAndDeriveKey() {
        return _lockSemaphore.Lock(() => {
            if (_disposed) return Array.Empty<byte>();
            ReadOnlyMemory<byte> realKey;
            if (inputKey.IsEmpty) {
                realKey = (Key.Length == KeySize) ? Key : Encrypter.CreateRandomBytes(KeySize); //if no input is given use the existing random key
            } else {
                if (inputKey.Length == KeySize) {
                    realKey = inputKey;
                } else if (inputKey.Length < KeySize) {
                    realKey = DeriveBytes(inputKey.Span, KeySize, Salt.Span); //short key
                } else { //try to set as key, if fails derive
                    try {
                        KeySize = inputKey.Length;
                        realKey = inputKey;
                    } catch {
                        realKey = DeriveBytes(inputKey.Span, KeySize, Salt.Span);
                    }
                }
            }
            Key = realKey;
            return realKey;
        });
    }
    /// <summary>Generate an iv from key or generate a random iv if it will be prepended to the output</summary>
    protected virtual ReadOnlyMemory<byte> CheckAndGenerateIV(ReadOnlySpan<byte> iv = default) {
        if (_disposed) return Array.Empty<byte>();
        if (!iv.IsEmpty) {
                IV = (iv.Length != BlockSize) ? DeriveBytes(iv, BlockSize, Salt.Span) : iv.ToArray(); //short iv size check
        } else if (CipherPrepend.HasFlag(CipherPrepend.IV)) { //random iv everytime
            if (usedForEncryptOnce || IV.IsEmpty) {
                GenerateIV(); //Generate a new iv since the last one is used.
                usedForEncryptOnce = false;
            }
        } else if (!usedForEncryptOnce){ //reuse existing bool to check if initialized
            IV = DeriveBytes(Key.Span, BlockSize, Salt.Span); //static iv
            usedForEncryptOnce = true;
        } 
        return IV;
    }
    /// <summary>Generate an iv</summary>
    public virtual void GenerateIV() => Algorithm.GenerateIV();

    ///// <summary>Generate an iv from key or generate a random iv if it will be prepended to the output</summary>
    //protected async virtual Task checkAndGenerateIVAsync(byte[] iv)
    //{
    //    if (iv != null) {
    //        if (iv.Length < Algorithm.BlockSize / 8)  //short iv
    //            Algorithm.IV = await deriveBytesAsync(iv, Algorithm.BlockSize / 8, Salt);
    //        else
    //            Algorithm.IV = iv;  //iv match
    //    } else if (CipherPrepend.HasFlag(CipherPrepend.IV)) {  //random iv everytime
    //        if (usedForEncryptOnce)
    //            await Task.Run(() => Algorithm.GenerateIV()).ConfigureAwait(false); //Generate a new iv since the last one is used.
    //    } else
    //        Algorithm.IV = await deriveBytesAsync(Algorithm.Key, Algorithm.BlockSize / 8, Salt).ConfigureAwait(false);  //static iv
    //}

    /// <inheritdoc />
    protected virtual byte[] DeriveBytes(ReadOnlySpan<byte> pwd, int len, ReadOnlySpan<byte> salt = default) => Helper == null ? Encrypter.DeriveBytes(pwd, len, salt) : Helper.DeriveBytesCached(pwd, len, salt);

    /// <inheritdoc />
    protected virtual byte[] DeriveBytes(string pwd, int len, ReadOnlySpan<byte> salt = default) => Helper == null ? Encrypter.DeriveBytes(pwd, len, salt) : Helper.DeriveBytesCached(pwd, len, salt);
    ///// <inheritdoc/>
    //protected async virtual Task<byte[]> deriveBytesAsync(byte[] pwd, int len, byte[] salt = null) => await Task.Run(() => deriveBytes(pwd, len, salt)).ConfigureAwait(false);
    ///// <inheritdoc/>
    //protected async virtual Task<byte[]> deriveBytesAsync(string pwd, int len, byte[] salt = null) => await Task.Run(() => deriveBytes(pwd, len, salt)).ConfigureAwait(false);

    #endregion Helpers

    #region cryptoSym (Disabled)
    ///// <summary>General symmetric encryption method</summary>
    ///// <param name="enc">Encryptor</param>
    ///// <param name="b">Array to encrypt</param>
    ///// <param name="key">The key.</param>
    ///// <param name="iv">The vector</param>
    ///// <param name="encrypt">if set to <c>true</c> encrypt else decrypt.</param>
    ///// <returns>Encrypted array</returns>
    //protected virtual byte[] cryptoSym(SymmetricAlgorithm enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
    //{
    //    ICryptoTransform encryptor = null;
    //    if (helper != null) encryptor = getEncryptor(enc, key, encrypt);
    //    if (encryptor == null) encryptor = encrypt ? enc.CreateEncryptor(key, iv) : enc.CreateDecryptor(key, iv);
    //    byte[] rval = encryptor.Encrypt(b);
    //    if (helper != null) setEncryptor(enc, key, encrypt, encryptor);
    //    return rval;
    //}
    #endregion cryptoSym

    #region Encrypt / Decrypt
    /// <inheritdoc />
    public virtual long Encrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false) {
        if (_disposed) return 0;
        var initialDestPos = destination.Position;
        _lockSemaphore.Wait();
        try {
            if (!headless) HeaderWrite(destination, iv);
            using var cStream = CreateCryptoStream(destination, true);
            cStream.Write(source);
        } finally { _lockSemaphore.Release(); }
        return destination.Position - initialDestPos;
    }

    /// <inheritdoc />
    public virtual async Task<long> EncryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false) {
        if (_disposed) return 0;
        var initialDestPos = destination.Position;
        await _lockSemaphore.WaitAsync();
        try {
        if (!headless) await HeaderWriteAsync(destination, iv);
            using var cStream = CreateCryptoStream(destination, true);
            await cStream.WriteAsync(source);
        } finally { _lockSemaphore.Release(); }
        return destination.Position - initialDestPos;
    }

    /// <inheritdoc />
    public virtual long Decrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false) {
        var initialDestPos = destination.Position;
        _lockSemaphore.Wait();
        try {
            var read = (headless) ? 0 : HeaderRead(source, iv);
            using var cStream = CreateCryptoStream(destination, false);
            cStream.Write(source[read..]);
        } finally { _lockSemaphore.Release(); }
        return destination.Position - initialDestPos;
    }

    /// <inheritdoc />
    public virtual async Task<long> DecryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false) {
        var initialDestPos = destination.Position;
        await _lockSemaphore.WaitAsync();
        try {
            var read = (headless) ? 0 : HeaderRead(source.Span, iv.Span);
            using var cStream = CreateCryptoStream(destination, false);
            await cStream.WriteAsync(source[read..]);
        } finally { _lockSemaphore.Release(); }
        return destination.Position - initialDestPos;
    }
    #endregion

    #region Headers
    /// <inheritdoc />
    public virtual int HeaderWrite(Stream destination, ReadOnlySpan<byte> iv = default) {
        var initialDestPos = destination.Position;
        CheckAndGenerateIV(iv); //generate if not used, or use the default iv
        usedForEncryptOnce = true;
        if (CipherPrepend.HasFlag(CipherPrepend.IV)) destination.Write(IV.Span);
        if (CipherPrepend.HasFlag(CipherPrepend.Salt)) destination.Writez(Salt.ToArray());
        return (int) (destination.Position - initialDestPos);
    }
    /// <inheritdoc />
    public virtual async Task<int> HeaderWriteAsync(Stream destination, ReadOnlyMemory<byte> iv = default) {
        var initialDestPos = destination.Position;
        CheckAndGenerateIV(iv.Span); //generate if not used, or use the default iv
        usedForEncryptOnce = true;
        if (CipherPrepend.HasFlag(CipherPrepend.IV)) await destination.WriteAsync(IV);
        if (CipherPrepend.HasFlag(CipherPrepend.Salt)) await destination.WritezAsync(Salt.ToArray());
        return (int) (destination.Position - initialDestPos);
    }
    /// <inheritdoc />
    public virtual int HeaderRead(Stream source, ReadOnlySpan<byte> iv = default) {
        var initialPos = source.Position;
        if (CipherPrepend.HasFlag(CipherPrepend.IV)) IV = source.ReadBytes(BlockSize).ToArray();
        else CheckAndGenerateIV(iv);
        if (CipherPrepend.HasFlag(CipherPrepend.Salt)) Salt = source.Readz<byte[]>() ?? Array.Empty<byte>();
        return (int) (source.Position - initialPos);
    }
    /// <inheritdoc />
    public virtual async Task<int> HeaderReadAsync(Stream source, ReadOnlyMemory<byte> iv = default) {
        var initialPos = source.Position;
        if (CipherPrepend.HasFlag(CipherPrepend.IV)) IV = await source.ReadBytesAsync(BlockSize);
        else CheckAndGenerateIV(iv.Span);
        if (CipherPrepend.HasFlag(CipherPrepend.Salt)) Salt = await source.ReadzAsync<byte[]>() ?? Array.Empty<byte>();
        return (int) (source.Position - initialPos);
    }
    /// <inheritdoc />
    public virtual int HeaderRead(ReadOnlySpan<byte> source, ReadOnlySpan<byte> iv = default) {
        int read = 0;
        if (CipherPrepend.HasFlag(CipherPrepend.IV)) IV = source.Slicez(ref read, BlockSize).ToArray();
        else CheckAndGenerateIV(iv);

        if (CipherPrepend.HasFlag(CipherPrepend.Salt)) {
            int saltLen = BitConverter.ToInt32(source.Slicez(ref read, 4));
            Salt = saltLen <= 0 ? Array.Empty<byte>() : source.Slicez(ref read, saltLen).ToArray();
        }
        return read;
    }
    #endregion
    
    #region CreateStream
    /// <inheritdoc />
    public virtual CryptoSafeStream CreateCryptoStream(Stream destination, bool isEncrypt)
        => new (destination, isEncrypt ? Algorithm.CreateEncryptor() : Algorithm.CreateDecryptor(), CryptoStreamMode.Write);
    #endregion

    #region Dispose
    /// <summary>Dispose</summary>
    protected override ValueTask DisposeAsync(bool disposing) {
        if (!_disposed && disposing) {
            //Helper?.CacheRemove(this, inputKey);
            //Clear();
            Algorithm?.Dispose();
        }
        // Call the appropriate methods to clean up
        // unmanaged resources here.
        return ValueTask.CompletedTask;
    }
    #endregion Dispose
}