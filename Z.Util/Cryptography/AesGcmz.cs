﻿/* Copyright (c) <2020> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
namespace Z.Cryptography;

/// <summary>AesGcm implementation</summary>
public class AesGcmz: EncrypterAlgorithm {
    #region Fields / Properties
    /// <summary>AesGcm dotnet implementation</summary>
    public AesGcm Inner { get; protected set; }
    private ReadOnlyMemory<byte> pKey;
    #endregion

    #region Constructors
#pragma warning disable CS8618
    /// <inheritdoc />
    public AesGcmz(ReadOnlyMemory<byte> key = default, Encrypter? e = null, ReadOnlyMemory<byte> salt = default, CipherPrepend prepend = CipherPrepend.IV) : base(key, e ?? Encrypter.Default, salt, prepend) {
        IV = Array.Empty<byte>();
        if (!inputKey.IsEmpty) CheckAndDeriveKey();
        if (Key.IsEmpty) CheckAndDeriveKey();
    }
    /// <inheritdoc />
    public AesGcmz(AesGcm aesgcm, Encrypter? e = null, CipherPrepend prepend = CipherPrepend.IV) : this(default, e, default, prepend) {
        Inner = aesgcm;
        //Helper?.CacheAdd(this, key);
    }
#pragma warning restore
    #endregion

    #region IEncryptor Members
    /// <inheritdoc />
    public override SymmetricAlgorithm Algorithm => Aes.Create();
    /// <inheritdoc />
    public override Type AlgorithmType => typeof(AesGcm);
    /// <inheritdoc />
    public override ReadOnlyMemory<byte> IV { get; set; }
    /// <inheritdoc />
    public override ReadOnlyMemory<byte> Key { get => pKey; set { pKey = value; Inner = new AesGcm(Key.ToArray()); } }
    /// <inheritdoc />
    public override int BlockSize => 12;
    /// <inheritdoc />
    public override int KeySize => 32;
    /// <inheritdoc />
    public override int TagSize => 16;


    /// <inheritdoc />
    public override long Encrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false) {

        var initialPos = destination.Position;
        var len = source.Length;
        var cipherBytes = new byte[len];
        var tag = new byte[16];
        _lockSemaphore.Wait();
        try {
            if (!headless) HeaderWrite(destination, iv);
            Inner.Encrypt(IV.Span, source, cipherBytes, tag);
        } finally { _lockSemaphore.Release(); }

        destination.Write(tag);
        destination.Write(cipherBytes);
        return destination.Position - initialPos;
    }
    /// <inheritdoc />
    public override async Task<long> EncryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false) {
        var initialPos = destination.Position;
        var len = source.Length;
        var cipherBytes = new byte[len];
        var tag = new byte[16];
        await _lockSemaphore.WaitAsync();
        try {
            if (!headless) HeaderWrite(destination, iv.Span);
            Inner.Encrypt(IV.Span, source.Span, cipherBytes, tag);
        } finally { _lockSemaphore.Release(); }

        await destination.WriteAsync(tag);
        await destination.WriteAsync(cipherBytes);
        return destination.Position - initialPos;
    }

    /// <inheritdoc />
    public override long Decrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false) {
        byte[] rval;
        int read;
        _lockSemaphore.Wait();
        try {
            var tag = new byte[TagSize];
            read = (headless) ? 0 : HeaderRead(source, iv);
            source.Slicez(ref read, tag.Length).CopyTo(tag);
            rval = new byte[source.Length - read];
            Inner.Decrypt(IV.Span, source[read..], tag, rval);
        } finally { _lockSemaphore.Release(); }
        destination.Write(rval);
        return rval.LongLength;
    }
    /// <inheritdoc />
    public override async Task<long> DecryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false) {
        byte[] rval;
        int read;
        await _lockSemaphore.WaitAsync();
        try {
            var tag = new byte[TagSize];
            read = (headless) ? 0 : HeaderRead(source.Span, iv.Span);
            source.Slicez(ref read, tag.Length).CopyTo(tag);
            rval = new byte[source.Length - read];
            Inner.Decrypt(IV.Span, source[read..].Span, tag, rval);
        } finally { _lockSemaphore.Release(); }
        await destination.WriteAsync(rval);
        return rval.LongLength;
    }

    /// <inheritdoc />
    public override CryptoSafeStream CreateCryptoStream(Stream destination, bool isEncrypt) => throw new NotSupportedException();
    #endregion

    /// <inheritdoc />
    public override void GenerateIV() => IV = Encrypter.CreateRandomBytes(12);
    /// <inheritdoc />
    protected override ReadOnlyMemory<byte> CheckAndDeriveKey() {
        base.CheckAndDeriveKey();
        Inner?.Dispose();
        Inner = new AesGcm(Key.Span);
        return Key;
    }

    #region Dispose
    /// <summary>Dispose</summary>
    protected override ValueTask DisposeAsync(bool disposing) {
        if (!_disposed && disposing) {
            //Clear();
            Inner?.Dispose();
        }
        // Call the appropriate methods to clean up
        // unmanaged resources here.
        _disposed = true;
        return ValueTask.CompletedTask;
    }
    #endregion Dispose
}
