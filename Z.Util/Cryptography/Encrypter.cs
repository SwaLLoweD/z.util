﻿/* Copyright (c) <2019> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
using Z.Collections.Generic;
using Z.Cryptography;

namespace Z.Util;

/// <summary>Encryption Engine</summary>
public class Encrypter
{
    #region Constants / Static Fields
    private static Encrypter? def = null;
    #endregion

    #region Fields / Properties
    /// <summary>Default encrypter using global caches</summary>
    public static Encrypter Default => def ??= new Encrypter();
    // /// <summary>Encryption engine cache</summary>
    // public CacheTable<byte[], IEncryptor> CacheEncrypter { get; set; } =
    //     new CacheTable<byte[], IEncryptor>(ZUtilCfg.EncrypterCacheTtl, 0, -1, ZUtilCfg.EncrypterCacheMin, ZUtilCfg.EncrypterCacheMax);
    // /// <summary>Cache time to live per encryption</summary>
    // public int CacheTtl { get => CacheEncrypter.ItemTtlMs; set => CacheEncrypter.ItemTtlMs = CacheKey.ItemTtlMs = value; }
    //public Dictionary<string, Dictionary<Type, CacheTable<byte[], object>>> CacheEncrypter { get; set; }
    /// <summary>Default Salt</summary>
    public ReadOnlyMemory<byte> Salt { get; protected set; }

    /// <summary>Key creator cache</summary>
    protected CacheTable<string, byte[]> CacheKey { get; set; } = new CacheTable<string, byte[]>(ZUtilCfg.EncrypterCacheTtl, 0, 0, ZUtilCfg.EncrypterCacheMin, ZUtilCfg.EncrypterCacheMax);
    /// <summary>Default creation options for Symmetric Algorithms</summary>
    protected Lazy<IDictionary<Type, Action<SymmetricAlgorithm>>> TblDefaults { get; set; } = new(() => new Dictionary<Type, Action<SymmetricAlgorithm>>(), false);
    #endregion

    #region Constructors
    ///// <summary>Default salt if none is specified</summary>
    //private static byte[] defaultSalt = ZUtilCfg.EncrypterDefaultSalt;

    /// <summary>Initializes a new instance of the <see cref="Encrypter" /> class.</summary>
    /// <param name="defaultSalt">The default salt.</param>
    public Encrypter(byte[]? defaultSalt = null) {
        CacheKey.DisposeOnExpiry = true;
        //CacheEncrypter.DisposeOnExpiry = true;
        //CacheEncrypter = new Dictionary<string, Dictionary<Type, CacheTable<byte[], object>>>();
        Salt = defaultSalt ?? ZUtilCfg.EncrypterDefaultSalt;
    }
    #endregion

    /// <summary>Creates an instance of the specified SymmetricAlgorithm Type</summary>
    public static T CreateSymmetricAlgorithm<T>(Encrypter? e = null) where T : SymmetricAlgorithm {
        SymmetricAlgorithm? rval;
        if (typeof(T) == typeof(Aes)) rval = Aes.Create();
        //else if (typeof(T) == typeof(Rijndael)) rval = Rijndael.Create();
        else if (typeof(T) == typeof(TripleDES)) rval = TripleDES.Create();
        else rval = (T?)typeof(T).GetMethod("Create", Array.Empty<Type>())?.Invoke(null, null);
        if (rval == null) throw new TypeLoadException($"Type ({typeof(T).FullName}) can not be created as Symmetric Algorithm");
        (e ?? Encrypter.Default).DefaultOptionsApply(rval);
        return (T)rval;
    }
    /// <summary>Returns an encrypter of specified type. which is anchored to the cache by its key</summary>
    public virtual IEncryptor CreateCryptoEngine<T>(ReadOnlyMemory<byte> key = default, ReadOnlyMemory<byte> salt = default, CipherPrepend cipherPrepend = CipherPrepend.IV, Action<T>? algorithmOptions = null)
        where T : SymmetricAlgorithm {
        IEncryptor? createNew() => (IEncryptor?)Activator.CreateInstance(typeof(EncrypterAlgorithm<T>), key, this, salt, cipherPrepend, algorithmOptions);
        // if (key == null || algorithmOptions != null) //Do not cache encrypters without a key or having customized config
        //     return createNew() ?? throw new TypeLoadException($"Type ({typeof(T).FullName}) can not be created as Symmetric Algorithm");

        // var cacheKey = GetEncryptorCacheId<T>(key);
        // if (CacheEncrypter.TryGetValue(cacheKey, out var rval)) {
        //     if (rval == null || !cacheKey.SequenceEqual(GetEncryptorCacheId<T>(rval.Algorithm.Key))) {
        //         CacheEncrypter.TryRemove(cacheKey, out _);
        //     } else {
        //         return rval;
        //     }
        // }

        //CacheEncrypter.TryAdd(cacheKey, rval); //Algorithm adds itself
        return createNew() ?? throw new TypeLoadException($"Type ({typeof(T).FullName}) can not be created as Symmetric Algorithm");
    }
    // internal void CacheRemove(IEncryptor enc, byte[] inputkey) {
    //     if (inputkey == null) return;
    //     Type t = enc.GetType();
    //     var genTyp = t.GenericTypeArguments.FirstOrDefault();
    //     if (genTyp != null) t = genTyp;
    //     CacheEncrypter.TryRemove(GetEncryptorCacheId(t, inputkey), out _);
    // }
    // internal void CacheAdd(IEncryptor enc, byte[] inputkey) {
    //     if (inputkey == null || enc == null) return;
    //     Type t = enc.GetType();
    //     var genTyp = t.GenericTypeArguments.FirstOrDefault();
    //     if (genTyp != null) t = genTyp;
    //     CacheEncrypter.TryAdd(GetEncryptorCacheId(t, inputkey), enc);
    // }

    /// <summary>Add default options setter for this encrypter for related symmetric algorithm type.</summary>
    public void DefaultOptionAdd<T>(Action<T> optionsSetter) where T : SymmetricAlgorithm {
        if (optionsSetter == null) throw new ArgumentNullException(nameof(optionsSetter));
        ClearCaches();
        TblDefaults.Value.Add(typeof(T), x => optionsSetter((T)x));
    }
    /// <summary>Apply default options setter for this encrypter</summary>
    public T DefaultOptionsApply<T>(T alg) where T : SymmetricAlgorithm {
        if (TblDefaults != null && TblDefaults.Value.TryGetValue(typeof(T), out var defaultOpts)) defaultOpts(alg);
        return alg;
    }

    /// <summary>Clears the caches.</summary>
    public virtual void ClearCaches() {
        CacheKey?.Clear();
        //CacheEncrypter?.Clear();
    }

    #region Helpers
    /// <summary>Creates a random byteArray</summary>
    /// <param name="length">Length of the array</param>
    public static byte[] CreateRandomBytes(int length) => RandomNumberGenerator.GetBytes(length);

    /// <summary>Clears the byte array (reset all to 0)</summary>
    /// <param name="buffer">The array.</param>
    public static void ClearBytes(byte[] buffer) {
        if (buffer == null) return;
        Array.Clear(buffer, 0, buffer.Length);
    }
    /// <summary>Create a byte array from given password and salt</summary>
    /// <param name="pwd">String to create a byte array from</param>
    /// <param name="len">Byte array length</param>
    /// <param name="salt">Salt (null = default salt)</param>
    /// <param name="iterations"> Number of iterations</param>
    /// <returns></returns>
    public byte[] DeriveBytesCached(string pwd, int len, ReadOnlySpan<byte> salt = default, int iterations = 1000) {
        var s = salt.IsEmpty ? Salt.Span : salt;
        var pwdId = GetPwdCacheId(pwd, len, s);
        var prevVal = CacheKey.TryGetValue(pwdId);
        if (prevVal != null) return prevVal;
        var rval = DeriveBytes(pwd, len, s, iterations);
        CacheKey[pwdId] = rval;
        return rval;
    }
    /// <summary>Create a byte array from given password and salt</summary>
    /// <param name="pwd">String to create a byte array from</param>
    /// <param name="len">Byte array length</param>
    /// <param name="salt">Salt (null = default salt)</param>
    /// <param name="iterations"> Number of iterations</param>
    /// <returns></returns>
    public byte[] DeriveBytesCached(ReadOnlySpan<byte> pwd, int len, ReadOnlySpan<byte> salt = default, int iterations = 1000) {
        var s = salt.IsEmpty ? Salt.Span : salt;
        var pwdId = GetPwdCacheId(pwd, len, s);
        var prevVal = CacheKey.TryGetValue(pwdId);
        if (prevVal != null) return prevVal;
        var rval = DeriveBytes(pwd, len, s, iterations);
        CacheKey[pwdId] = rval;
        return rval;
    }

    /// <summary>Create a byte array from given password and salt</summary>
    /// <param name="pwd">String to create a byte array from</param>
    /// <param name="len">Byte array length</param>
    /// <param name="salt">Salt (null = default salt)</param>
    /// <param name="iterations"> Number of iterations</param>
    /// <returns></returns>
    public static byte[] DeriveBytes(string pwd, int len, ReadOnlySpan<byte> salt = default, int iterations = 1000) {
        //var pwdId = getPwdId(pwd, len);
        //var prevVal = cacheKey[pwdId];
        //if (prevVal != null) return prevVal;
        // var rdb = Rfc2898DeriveBytes(pwd, salt ?? ZUtilCfg.EncrypterDefaultSalt);
        // var rval = rdb.GetBytes(len);
        if (string.IsNullOrEmpty(pwd)) return CreateRandomBytes(len);
        var rval = Rfc2898DeriveBytes.Pbkdf2(pwd, salt.IsEmpty ? ZUtilCfg.EncrypterDefaultSalt : salt, iterations, HashAlgorithmName.SHA256, len);
        //cacheKey[pwdId] = rval;
        return rval;
    }

    /// <summary>Create a byte array from given password and salt</summary>
    /// <param name="pwd">Byte[] to create a byte array from</param>
    /// <param name="len">Byte array length</param>
    /// <param name="salt">Salt (null = default salt)</param>
    /// <param name="iterations"> Number of iterations</param>
    /// <returns></returns>
    public static byte[] DeriveBytes(ReadOnlySpan<byte> pwd, int len, ReadOnlySpan<byte> salt = default, int iterations = 1000) {
        //var pwdId = getPwdId(pwd, len);
        //var prevVal = cacheKey[pwdId];
        //if (prevVal != null) return prevVal;
        // var rdb =  new Rfc2898DeriveBytes(pwd.ToArray(), salt ?? ZUtilCfg.EncrypterDefaultSalt, iterations, HashAlgorithmName.SHA256);
        // var rval = rdb.GetBytes(len);
        //cacheKey[pwdId] = rval;
        if (pwd.IsEmpty) return CreateRandomBytes(len);
        var rval = Rfc2898DeriveBytes.Pbkdf2(pwd, salt.IsEmpty ? ZUtilCfg.EncrypterDefaultSalt : salt, iterations, HashAlgorithmName.SHA256, len);
        return rval;
    }

    /// <summary>Create id for password-key cache</summary>
    protected static string GetPwdCacheId(string pwd, int len, ReadOnlySpan<byte> salt) => $"{pwd}:{len}{Convert.ToBase64String(salt)}";

    /// <summary>Create id for password-key cache</summary>
    protected static string GetPwdCacheId(ReadOnlySpan<byte> pwd, int len, ReadOnlySpan<byte> salt) => GetPwdCacheId(Convert.ToBase64String(pwd), len, salt);
    // /// <summary>Returns an encrypter of specified type.</summary>
    // protected virtual byte[] GetEncryptorCacheId<T>(byte[] key) => GetEncryptorCacheId(typeof(T), key);
    // /// <summary>Returns an encrypter of specified type.</summary>
    // protected virtual byte[] GetEncryptorCacheId(Type t, byte[] key) => t.Name.To().ByteArray.AsAscii!.Concat(key).ToArray();
    #endregion Helpers
}