﻿/*
Copyright (c) 2022 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
namespace Z.Cryptography;

/// <summary>X509Certificate Generator</summary>
public class X509Util
{
    /// <summary>Create a X509 certificate generation request</summary>
    /// <param name="subjectName">Qualified name of the subject</param>
    /// <param name="issuerCA">Certificate authority (if not specified the generated cert will be self-signed)</param>
    public virtual X509GenerationRequest CreateRequest(string subjectName, X509Certificate2? issuerCA = null) => new(subjectName, issuerCA);

    /// <summary>Generate a X509 certificate</summary>
    public X509Certificate2 Generate(string subjectName, DateTime startDate, DateTime expiryDate, int strength = 512, string? exportPwd = null,
        SignatureAlgorithms sAlgorithm = SignatureAlgorithms.SHA256WithRSA, X509Certificate2? issuerCA = null) {
        var xgr = new X509GenerationRequest(subjectName, startDate, expiryDate, strength, exportPwd, sAlgorithm, issuerCA);
        return Generate(xgr);
    }
    /// <summary>Generate a X509 certificate</summary>
    public virtual X509Certificate2 Generate(X509GenerationRequest request) {
        X500DistinguishedName distinguishedName = new(request.SubjectName ?? "");
        var algStr = request.Algorithm.ToString();
        var hashAlgName = algStr.SubstringBefore("With");
        X509Certificate2? cert = null;

        if (algStr.EndsWith("WithRSA")) {
            using var rsa = RSA.Create(request.Strength);
            var req = new CertificateRequest(distinguishedName, rsa, new HashAlgorithmName(hashAlgName), RSASignaturePadding.Pkcs1);
            cert = GenerateInner(request, req);
            if (!cert.HasPrivateKey) cert = cert.CopyWithPrivateKey(rsa);
        } else if (algStr.EndsWith("WithECDSA")) {
            using var ecdsa = ECDsa.Create();
            var req = new CertificateRequest(distinguishedName, ecdsa, new HashAlgorithmName(hashAlgName));
            cert = GenerateInner(request, req);
            if (!cert.HasPrivateKey) cert = cert.CopyWithPrivateKey(ecdsa);
        }
        if (cert == null) throw new NotSupportedException("Unsupported Algorithm.");
        return cert;

        // var rawData = certificate.Export(X509ContentType.Pfx, request.ExportPassword);
        // return new X509Certificate2(rawData, request.ExportPassword, X509KeyStorageFlags.Exportable);

        #region Other Methods
        // Method 1: export as raw data, and then import into X509
        //byte[] certBytes = null;
        //certBytes = GenerateAndExport(request);
        //return new X509Certificate2(certBytes, request.ExportPassword, X509KeyStorageFlags.Exportable);

        // Method 2: merge into X509Certificate2  (does not validate itself, can not be used for sslsockets)
        //AsymmetricCipherKeyPair kp;
        //var newCert = generate(out kp, subjectName, alg, strength, startDate, expiryDate);
        //X509Certificate2 x509 = new System.Security.Cryptography.X509Certificates.X509Certificate2(newCert.GetEncoded());

        //// correcponding private key
        //PrivateKeyInfo info = PrivateKeyInfoFactory.CreatePrivateKeyInfo(kp.Private);
        //Asn1Sequence seq = (Asn1Sequence)Asn1Object.FromByteArray(info.PrivateKey.GetDerEncoded());
        //if (seq.Count != 9) {
        //    throw new Exception("malformed sequence in RSA private key");
        //}

        //RsaPrivateKeyStructure rsa = new RsaPrivateKeyStructure(seq);
        //RsaPrivateCrtKeyParameters rsaparams = new RsaPrivateCrtKeyParameters(
        //    rsa.Modulus, rsa.PublicExponent, rsa.PrivateExponent, rsa.Prime1, rsa.Prime2, rsa.Exponent1, rsa.Exponent2, rsa.Coefficient);

        //x509.PrivateKey = DotNetUtilities.ToRSA(rsaparams);
        //return x509;

        //Method 3
        //var dotnetCert = DotNetUtilities.ToX509Certificate(newCert);
        //var rval = new System.Security.Cryptography.X509Certificates.X509Certificate2(dotnetCert);
        //rval.PrivateKey = DotNetUtilities.ToRSA((RsaPrivateCrtKeyParameters)kp.Private);

        //return rval;
        //return DotNetUtilities.ToX509Certificate(newCert).Export(System.Security.Cryptography.X509Certificates.X509ContentType.Pkcs12, "password");
        #endregion
    }

    /// <summary>Generate a X509 certificate</summary>
    protected virtual X509Certificate2 GenerateInner(X509GenerationRequest request, CertificateRequest req) {
        // req.CertificateExtensions.Add(
        //     new X509KeyUsageExtension(X509KeyUsageFlags.DataEncipherment | X509KeyUsageFlags.KeyEncipherment | X509KeyUsageFlags.DigitalSignature , false));
        if (request.IssuerCert == null) { //create a ca cert
            req.CertificateExtensions.Add(
                new X509BasicConstraintsExtension(true, false, 0, true));
            req.CertificateExtensions.Add(
                new X509SubjectKeyIdentifierExtension(req.PublicKey, false));
        } else {
            req.CertificateExtensions.Add(
                new X509BasicConstraintsExtension(true, false, 0, false));
            req.CertificateExtensions.Add(
                new X509SubjectKeyIdentifierExtension(req.PublicKey, false));
        }
        foreach (var keyPurpose in request.Purposes) {
            req.CertificateExtensions.Add(
                new X509EnhancedKeyUsageExtension(
                    new OidCollection { new Oid($"1.3.6.1.5.5.7.3.{(byte)keyPurpose}") }, false));
        }

        SubjectAlternativeNameBuilder sanBuilder = new();
        // sanBuilder.AddIpAddress(IPAddress.Loopback);
        // sanBuilder.AddIpAddress(IPAddress.IPv6Loopback);
        // sanBuilder.AddDnsName("localhost");
        // sanBuilder.AddDnsName(Environment.MachineName);
        req.CertificateExtensions.Add(sanBuilder.Build());
        if (request.IssuerCert == null) {
            return req.CreateSelfSigned(request.StartDate, request.ExpiryDate);
        }

        // // set the AuthorityKeyIdentifier. There is no built-in 
        // // support, so it needs to be copied from the Subject Key 
        // // Identifier of the signing certificate and massaged slightly.
        // // AuthorityKeyIdentifier is "KeyID="
        // var issuerSubjectKey = request.IssuerCert.Extensions["X509v3 Subject Key Identifier"].RawData;
        // var segment = new ArraySegment<byte>(issuerSubjectKey, 2, issuerSubjectKey.Length - 2);
        // var authorityKeyIdentifer = new byte[segment.Count + 4];
        // // these bytes define the "KeyID" part of the AuthorityKeyIdentifer
        // authorityKeyIdentifer[0] = 0x30;
        // authorityKeyIdentifer[1] = 0x16;
        // authorityKeyIdentifer[2] = 0x80;
        // authorityKeyIdentifer[3] = 0x14;
        // segment.CopyTo(authorityKeyIdentifer, 4);
        // req.CertificateExtensions.Add(new X509Extension("2.5.29.35", authorityKeyIdentifer, false));

        var sn = Guid.NewGuid().ToByteArray();
        if ((sn[0] & 0x80) == 0x80) sn[0] -= 0x80; //Serial Key must be positive
        var cert = req.Create(request.IssuerCert, request.StartDate, request.ExpiryDate, sn);
        return cert;
    }

    /// <summary>Fetch Certificate from Store</summary>
    public static X509Certificate2? FetchCertificate(string certificateName) {
        var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
        store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
        return store.Certificates.Cast<X509Certificate2>().FirstOrDefault(cert => cert.Subject.Contains(certificateName, StringComparison.CurrentCulture));
    }
}