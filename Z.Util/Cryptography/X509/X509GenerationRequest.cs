﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Z.Extensions;

namespace Z.Cryptography;

/// <summary>Certificate request options</summary>
public class X509GenerationRequest
{
    #region Fields / Properties
    private string? subName;
    /// <summary>Signature algorithm</summary>
    public SignatureAlgorithms Algorithm { get; set; }
    /// <summary>Certificate expiry date</summary>
    public DateTime ExpiryDate { get; set; }
    /// <summary>Exported data password</summary>
    public string? ExportPassword { get; set; }
    /// <summary>Certificate authority (if exists)</summary>
    public X509Certificate2? IssuerCert { get; set; }
    /// <summary>Key usage purposes</summary>
    public IList<IdKeyPurposes> Purposes { get; set; }
    /// <summary>Certificate start date</summary>
    public DateTime StartDate { get; set; }
    /// <summary>Encryption strength in bits</summary>
    public int Strength { get; set; }
    /// <summary>Name</summary>
    public string? SubjectName { get => subName; set => subName = value?.EnsureStartsWith("CN="); }
    #endregion

    #region Constructors
    /// <summary>Certificate request options</summary>
    public X509GenerationRequest(string subjectName, X509Certificate2? issuerCA = null, string? exportPwd = null) : this(subjectName, DateTime.MinValue, DateTime.MaxValue, 512, exportPwd,
        SignatureAlgorithms.SHA256WithRSA, issuerCA) {
        StartDate = DateTime.UtcNow.AddDays(-1);
        ExpiryDate = issuerCA?.NotAfter ?? StartDate.AddYears(30);
    }
    /// <summary>Certificate request options</summary>
    public X509GenerationRequest(string subjectName, DateTime startDate, DateTime expiryDate, int strength = 512, string? exportPwd = null,
        SignatureAlgorithms sAlgorithm = SignatureAlgorithms.SHA256WithRSA, X509Certificate2? issuerCA = null) {
        SubjectName = subjectName;
        Algorithm = sAlgorithm;
        Strength = strength;
        ExportPassword = exportPwd;
        StartDate = startDate;
        ExpiryDate = expiryDate;
        Purposes = new List<IdKeyPurposes>();
        IssuerCert = issuerCA;
    }
    #endregion

    /// <summary>Generate a X509 certificate</summary>
    public X509Certificate2 Generate<T>() where T : X509Util {
        var u = typeof(T).CreateInstance<T>()!;
        return u.Generate(this);
    }
}

/// <summary>SignatureAlgorithm</summary>
public enum SignatureAlgorithms
{
    /// <summary>MD2WithRSA</summary>
    MD2WithRSA,
    ///<summary>MD5WithRSA</summary>
    MD5WithRSA,
    ///<summary>SHA1WithRSA</summary>
    SHA1WithRSA,
    ///<summary>SHA224WithRSA</summary>
    SHA224WithRSA,
    ///<summary>SHA256WithRSA</summary>
    SHA256WithRSA,
    ///<summary>SHA384WithRSA</summary>
    SHA384WithRSA,
    ///<summary>SHA512WithRSA</summary>
    SHA512WithRSA,
    ///<summary>SHA1WithRSAANDMGF1</summary>
    SHA1WithRSAANDMGF1,
    ///<summary>SHA224WithRSAANDMGF1</summary>
    SHA224WithRSAANDMGF1,
    ///<summary>SHA256WithRSAANDMGF1</summary>
    SHA256WithRSAANDMGF1,
    ///<summary>SHA384WithRSAANDMGF1</summary>
    SHA384WithRSAANDMGF1,
    ///<summary>SHA512WithRSAANDMGF1</summary>
    SHA512WithRSAANDMGF1,
    ///<summary>RIPEMD160WithRSA</summary>
    RIPEMD160WithRSA,
    ///<summary>RIPEMD128WithRSA</summary>
    RIPEMD128WithRSA,
    ///<summary>RIPEMD256WithRSA</summary>
    RIPEMD256WithRSA,
    ///<summary>SHA1WithDSA</summary>
    SHA1WithDSA,
    ///<summary>SHA224WithDSA</summary>
    SHA224WithDSA,
    ///<summary>SHA256WithDSA</summary>
    SHA256WithDSA,
    ///<summary>SHA384WithDSA</summary>
    SHA384WithDSA,
    ///<summary>SHA512WithDSA</summary>
    SHA512WithDSA,
    ///<summary>SHA1WithECDSA</summary>
    SHA1WithECDSA,
    ///<summary>SHA224WithECDSA</summary>
    SHA224WithECDSA,
    ///<summary>SHA256WithECDSA</summary>
    SHA256WithECDSA,
    ///<summary>SHA384WithECDSA</summary>
    SHA384WithECDSA,
    ///<summary>SHA512WithECDSA</summary>
    SHA512WithECDSA,
    ///<summary>GOST3411WithGOST3410</summary>
    GOST3411WithGOST3410
    //GOST3411WithECGOST3410,
}

///<summary>Key Purposes</summary>
public enum IdKeyPurposes : byte
{
    ///<summary>IdKpServerAuth</summary>
    IdKpServerAuth = 1,
    ///<summary>IdKpClientAuth</summary>
    IdKpClientAuth = 2,
    ///<summary>IdKpCodeSigning</summary>
    IdKpCodeSigning = 3,
    ///<summary>IdKpEmailProtection</summary>
    IdKpEmailProtection = 4,
    ///<summary>IdKpIpsecEndSystem</summary>
    IdKpIpsecEndSystem = 5,
    ///<summary>IdKpIpsecTunnel</summary>
    IdKpIpsecTunnel = 6,
    ///<summary>IdKpIpsecUser</summary>
    IdKpIpsecUser = 7,
    ///<summary>IdKpTimeStamping</summary>
    IdKpTimeStamping = 8,
    ///<summary>IdKpOCSPSigning</summary>
    IdKpOCSPSigning = 9
}