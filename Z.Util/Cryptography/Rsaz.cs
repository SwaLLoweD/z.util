﻿/* Copyright (c) <2020> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
namespace Z.Cryptography;

/// <summary>Rsa with symmetric encryption implementation</summary>
public class Rsaz : EncrypterAlgorithm
{
    #region Fields / Properties
    /// <summary>Rsa dotnet implementation</summary>
    public RSA InnerRsa { get; protected set; }
    /// <summary>Rsa dotnet implementation</summary>
    public Func<IEncryptor> CreateEncryptor { get; protected set; }
    /// <summary>Rsa dotnet implementation</summary>
    public Lazy<IEncryptor> Inner { get; protected set; }
    /// <summary>Rsa padding</summary>
    public RSAEncryptionPadding Padding { get; protected set; }
    #endregion

    #region Constructors
#pragma warning disable CS8618
    /// <inheritdoc />
    public Rsaz(ReadOnlySpan<byte> spkiPublickey, ReadOnlySpan<byte> pkcs8PrivateKey, Func<IEncryptor> encryptor, RSAEncryptionPadding? padding = null, Encrypter? e = null, ReadOnlyMemory<byte> salt = default, bool prependIv = true) 
     :this(default, encryptor, padding, e, salt, prependIv) {
        InnerRsa = RSA.Create();
        InnerRsa.ImportSubjectPublicKeyInfo(spkiPublickey, out _);
        if (!pkcs8PrivateKey.IsEmpty) InnerRsa.ImportPkcs8PrivateKey(pkcs8PrivateKey, out _);
        //Helper?.CacheAdd(this, key);
    }
    /// <inheritdoc />
    public Rsaz(RSA? rsa, Func<IEncryptor> encryptor, RSAEncryptionPadding? padding = null, Encrypter? e = null, ReadOnlyMemory<byte> salt = default, bool prependIv = true)
    : base(default, e ?? Encrypter.Default, salt, prependIv ? CipherPrepend.IV : CipherPrepend.None) {
        CreateEncryptor = encryptor;
        Inner = new(CreateEncryptor);
        pSalt = salt.IsEmpty ? Helper.Salt : salt;
        Padding = padding ?? RSAEncryptionPadding.OaepSHA256;
        if (rsa != null) InnerRsa = rsa;
        //Helper?.CacheAdd(this, key);
    }
    #pragma warning restore
    #endregion

    #region IEncryptor Members
    /// <inheritdoc />
    public override SymmetricAlgorithm Algorithm => Inner.Value.Algorithm;
    /// <inheritdoc />
    public override Type AlgorithmType => typeof(Rsaz);
    /// <inheritdoc />
    public override ReadOnlyMemory<byte> IV { get => Inner.Value.IV; set => Inner.Value.IV = value; }
    /// <inheritdoc />
    public override ReadOnlyMemory<byte> Key { get => Inner.Value.Key; set => Inner.Value.Key = value; }
    /// <inheritdoc />
    public override int BlockSize { get => Inner.Value.BlockSize; }
    /// <inheritdoc />
    public override int KeySize { get => Inner.Value.KeySize; }
    /// <inheritdoc />
    public override int TagSize { get => Inner.Value.TagSize; }


    /// <inheritdoc />
    public override long Encrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false) {
        var initialDestPos = destination.Position;
        var maxLength = GetMaxRsaLength(InnerRsa.KeySize, Padding);
        if (source.Length <= maxLength) { //data fits
            var toWrite = InnerRsa.Encrypt(source.ToArray(), Padding);
            destination.Write(toWrite);
        } else {
            if (maxLength < KeySize) throw new InvalidOperationException("RSA key size and padding does not allow enough room for the selected AES encryption");
            if (Key.IsEmpty) CheckAndDeriveKey(); //Encrypter.CreateRandomBytes(sa.KeySize / 8); //Already random
            var keyEnc = InnerRsa.Encrypt(Key.ToArray(), Padding);
            destination.Write(keyEnc); //Length = rsaKeysize/8 (in bytes)
            Inner.Value.Encrypt(source, destination, iv);
        }
        return destination.Position - initialDestPos;
    }
    /// <inheritdoc />
    public override async Task<long> EncryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false) {
        var initialDestPos = destination.Position;
        var maxLength = GetMaxRsaLength(InnerRsa.KeySize, Padding);
        if (source.Length <= maxLength) { //data fits
            var toWrite = InnerRsa.Encrypt(source.ToArray(), Padding);
            await destination.WriteAsync(toWrite);
        } else {
            if (maxLength < KeySize) throw new InvalidOperationException("RSA key size and padding does not allow enough room for the selected AES encryption");
            if (Key.IsEmpty) CheckAndDeriveKey(); //Encrypter.CreateRandomBytes(sa.KeySize / 8); //Already random
            var keyEnc = InnerRsa.Encrypt(Key.ToArray(), Padding);
            await destination.WriteAsync(keyEnc); //Length = rsaKeysize/8 (in bytes)
            await Inner.Value.EncryptAsync(source, destination, iv);
        }
        return destination.Position - initialDestPos;
    }

    /// <inheritdoc />
    public override long Decrypt(ReadOnlySpan<byte> source, Stream destination, ReadOnlySpan<byte> iv = default, bool headless = false) {
        var initialDestPos = destination.Position;
        var read = 0;
        var rsaDataLength = InnerRsa.KeySize / 8;
        var rsaDecryptedData = InnerRsa.Decrypt(source.Slicez(ref read, rsaDataLength).ToArray(), Padding);
        if (source.Length <= rsaDataLength) { //only rsa encryption is present.
            destination.Write(rsaDecryptedData);
        } else {
            Inner.Value.Key = rsaDecryptedData;
            Inner.Value.Decrypt(source[read..], destination, iv);
        }
        return destination.Position - initialDestPos;
    }
    /// <inheritdoc />
    public override  Task<long> DecryptAsync(ReadOnlyMemory<byte> source, Stream destination, ReadOnlyMemory<byte> iv = default, bool headless = false)
        => Task.FromResult(Decrypt(source.Span, destination, iv.Span, headless));



    /// <inheritdoc />
    public override CryptoSafeStream CreateCryptoStream(Stream destination, bool isEncrypt) => Inner.Value.CreateCryptoStream(destination, isEncrypt) ?? throw new NullReferenceException();

    #endregion

    /// <summary>Finds the encryption capacity of the rsa encryption</summary>
    public static int GetMaxRsaLength(int rsaKeySize, RSAEncryptionPadding padding) {
        int paddingOverhead;
        if (padding.Mode == RSAEncryptionPaddingMode.Pkcs1) {
            paddingOverhead = 11;
        } else {
            if (padding.OaepHashAlgorithm == HashAlgorithmName.SHA1)
                paddingOverhead = 42;
            else if (padding.OaepHashAlgorithm == HashAlgorithmName.SHA256)
                paddingOverhead = 66;
            else if (padding.OaepHashAlgorithm == HashAlgorithmName.SHA384)
                paddingOverhead = 98;
            else if (padding.OaepHashAlgorithm == HashAlgorithmName.SHA512)
                paddingOverhead = 130;
            else if (padding.OaepHashAlgorithm == HashAlgorithmName.MD5)
                paddingOverhead = 34;
            else
                throw new ArgumentOutOfRangeException(nameof(padding));
        }

        //var paddingOverhead = fOAEP ? 42 : 11; //OAEP-SHA1: 42, OAEP-SHA256: 66 (forumla: 2+(2*hlen)) , PKCS1 = 11
        return (rsaKeySize / 8) - paddingOverhead;
    }

    #region Dispose
    /// <summary>Dispose</summary>
    protected override ValueTask DisposeAsync(bool disposing) {
        if (!_disposed && disposing) {
            //Clear();
            Inner.Value?.Dispose();
            InnerRsa?.Dispose();
        }
        // Call the appropriate methods to clean up
        // unmanaged resources here.
        _disposed = true;
        return ValueTask.CompletedTask;
    }
    #endregion Dispose
}
