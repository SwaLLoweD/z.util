﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
#endregion Usings

namespace Z.Cryptography
{
    /// <summary>Symmetric algorithm cipher prepend data to encryption result</summary>
    [Flags]
    public enum CipherPrepend
    {
        /// <summary>Prepend no data</summary>
        None = 0,
        /// <summary>Prepend IV in encrypted result</summary>
        IV = 1,
        /// <summary>Prepend Salt or Tag in encrypted result</summary>
        Salt = 2,
    }

    /// <summary>Symmetric algorithm cipher modes</summary>
    public enum CipherMode
    {
        /// <summary>Encrypter default mode</summary>
        Default,
        /// <summary>Cipher Block Chaining</summary>
        CBC,
        /// <summary>Cipher Feedback</summary>
        CFB,
        /// <summary>Cipher Text Stealing</summary>
        CTS,
        /// <summary>Electronic Codebook (Not recommended)</summary>
        ECB,
        /// <summary>Output Feedback</summary>
        OFB,
        /// <summary>CCM</summary>
        CCM,
        /// <summary>GCM</summary>
        GCM,
        /// <summary>EAX</summary>
        EAX,
        /// <summary>or CTR</summary>
        SIC,
        /// <summary>OpenPGPCF</summary>
        OpenPGPCF,
        /// <summary>GOFB</summary>
        GOFB
    }

    /// <summary>Options used in directory copying</summary>
    public enum CipherPadding
    {
        /// <summary>Encrypter default padding</summary>
        Default,
        /// <summary>No padding</summary>
        None,
        /// <summary>PKCS7/PKCS5 padding</summary>
        PKCS7,
        /// <summary>ISO 10126-2 padding</summary>
        ISO10126d2,
        /// <summary>X9.23 padding</summary>
        X932,
        /// <summary>ISO 7816-4 padding (ISO 9797-1 scheme 2)</summary>
        ISO7816d4,
        /// <summary>Pad with Zeros (not recommended)</summary>
        ZeroByte
    }
}