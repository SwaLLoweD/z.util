﻿/* Copyright (c) <2019> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;

namespace Z.Util;

/// <summary>CryptoStream which does not close the underlying stream on dispose and keeps track of the underlying stream.</summary>
public class CryptoSafeStream<T> : CryptoStream<T> where T : Stream
{
    #region Constructors
    /// <summary>Constructor</summary>
    public CryptoSafeStream(T stream, ICryptoTransform transform, CryptoStreamMode mode) : base(stream, transform, mode) { }
    /// <summary>Constructor</summary>
    public CryptoSafeStream(ICryptoTransform transform, CryptoStreamMode mode) : this(Activator.CreateInstance<T>(), transform, mode) { }
    #endregion

    /// <inheritdoc />
    protected override void Dispose(bool disposing) {
        if (!HasFlushedFinalBlock) FlushFinalBlock();

        base.Dispose(false);
    }
}

/// <summary>CryptoStream which keeps track of its underlying inner stream</summary>
public class CryptoStream<T> : CryptoStream where T : Stream
{
    #region Fields / Properties
    /// <summary>Underlying stream</summary>
    public T InnerStream { get; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    public CryptoStream(T stream, ICryptoTransform transform, CryptoStreamMode mode) : base(stream, transform, mode) => InnerStream = stream;
    /// <summary>Constructor</summary>
    public CryptoStream(ICryptoTransform transform, CryptoStreamMode mode) : this(Activator.CreateInstance<T>(), transform, mode) { }
    #endregion

    /// <summary>Get the Encrypted byte array from Inner Stream</summary>
    public Span<byte> ToCryptoArray(bool flushFirst = true) {
        if (flushFirst && !HasFlushedFinalBlock) FlushFinalBlock();
        return CanRead ? this.ReadAllBytes() : InnerStream.ReadAllBytes();
    }
}
/// <summary>CryptoStream which does not close the underlying stream on dispose</summary>
public class CryptoSafeStream : CryptoStream
{
    #region Fields / Properties
    /// <summary>Underlying stream</summary>
    public Stream InnerStream { get; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    public CryptoSafeStream(Stream stream, ICryptoTransform transform, CryptoStreamMode mode) : base(stream, transform, mode) => InnerStream = stream;
    #endregion

    /// <inheritdoc />
    protected override void Dispose(bool disposing) {
        if (!HasFlushedFinalBlock) FlushFinalBlock();

        base.Dispose(false);
    }
    /// <summary>Get the Encrypted byte array from Inner Stream</summary>
    public Span<byte> ToCryptoArray(bool flushFirst = true) {
        if (flushFirst && !HasFlushedFinalBlock) FlushFinalBlock();
        return CanRead ? this.ReadAllBytes() : InnerStream.ReadAllBytes();
    }
}