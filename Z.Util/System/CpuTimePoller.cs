/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Diagnostics;
namespace Z.Util;

/// <summary>Polls the cpu usage for specified intervals, returning average cpu usage during that time</summary>
public class CpuTimePoller : DisposableZ
{
    #region Fields / Properties
    private readonly Process proc;
    private readonly Timer pollTimer;

    /// <summary>Cpu time used during the interval</summary>
    public TimeSpan CpuTime { get; set; }
    /// <summary>Current average cpu load</summary>
    public float CurrentLoad { get; set; }
    /// <summary>Current average percentage of cpu usage</summary>
    public float CurrentPct { get; set; }
    /// <summary>Last check time</summary>
    public DateTime LastRead { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    /// <param name="pollIntervalSecs">Poll interval in seconds</param>
    /// <param name="process">Get usage of the specified process (null = current process)</param>
    public CpuTimePoller(int pollIntervalSecs, Process? process = null) {
        proc = process ?? Process.GetCurrentProcess();
        pollTimer = new Timer(pollIntervalSecs * 1000) {
            AutoReset = true
        };
        pollTimer.Elapsed += PollTimer_Elapsed;
        pollTimer.Start();
    }
    #endregion

    private void Poll() {
        var oldCpuTime = CpuTime;
        var oldReadTime = LastRead;
        try {
            CpuTime = proc.TotalProcessorTime;
            LastRead = DateTime.UtcNow;

            float cpuTimeDiff = (CpuTime - oldCpuTime).Ticks;
            float timeDiff = (LastRead - oldReadTime).Ticks;
            if (timeDiff != 0) {
                CurrentLoad = cpuTimeDiff / timeDiff;
                CurrentPct = CurrentLoad / SysInf.CpuCount * 100;
            }
        } catch { }
    }
    private void PollTimer_Elapsed(object? state, EventArgs args) {
        if (proc == null) {
            pollTimer.Dispose();
            //pollTimer = null;
            return;
        }
        Poll();
    }

    #region Dispose
    /// <inheritdoc />
    protected override ValueTask DisposeAsync(bool disposing) {
        if (!_disposed && disposing && pollTimer != null) {
            pollTimer.Elapsed -= PollTimer_Elapsed;
            pollTimer.Dispose();
            //pollTimer = null;
        }
        // Call the appropriate methods to clean up
        // unmanaged resources here.
        return ValueTask.CompletedTask;
    }
    #endregion Dispose
}