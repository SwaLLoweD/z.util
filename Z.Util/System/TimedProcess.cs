﻿using System.Diagnostics;
using System.Text;

namespace Z.Util;

/// <summary>Process that is killed after a timeout carrying a strongly typed data</summary>
public class TimedProcess<T> : TimedProcessBase
{
    #region Fields / Properties
    /// <summary>Process Data</summary>
    public virtual T? Data { get; set; }
    #endregion

    #region Constructors
    /// <summary>Default constructor for timed process</summary>
    public TimedProcess(int timeout) : base(timeout) { }
    #endregion
}

/// <summary>Process that is killed after a timeout</summary>
public class TimedProcess : TimedProcessBase
{
    #region Fields / Properties
    /// <summary>Process Data</summary>
    public virtual object? Data { get; set; }
    /// <summary>Process data as string</summary>
    public virtual string? DataString { get; set; }
    #endregion

    #region Constructors
    /// <summary>Default constructor for timed process</summary>
    public TimedProcess(int timeout) : base(timeout) { }
    #endregion
}

/// <summary>Process that is killed after a timeout</summary>
public class TimedProcessBase : DisposableZ
{
    #region Fields / Properties
    //public StringBuilder Results;

    /// <summary>Process Errors</summary>
    public virtual StringBuilder Errors { get; set; }
    /// <summary>Process Id</summary>
    public virtual int Id { get; set; }
    /// <summary>Process Outputs</summary>
    public virtual StringBuilder Outputs { get; set; }
    /// <summary>Real Process</summary>
    public virtual Process Proc { get; set; }
    /// <summary>Process timeout timer</summary>
    public virtual Timer TimerTimeout { get; set; }
    #endregion

    #region Events
    /// <summary>Fired on Process error output received</summary>
    public event EventHandler? ErrorDataReceived;
    /// <summary>Fired on Process exited</summary>
    public event EventHandler? Exited;
    /// <summary>Fired on Process output received</summary>
    public event EventHandler? OutputDataReceived;

    /// <summary>Fired on Process timeout</summary>
    public event EventHandler? TimedOut;
    #endregion

    #region Constructors
    /// <summary>Default constructor for timed process</summary>
    public TimedProcessBase(int timeout) {
        TimerTimeout = new Timer(timeout) {
            AutoReset = false
        };
        TimerTimeout.Elapsed += Timer_Elapsed;
        TimerTimeout.Start();
        Proc = new Process();
        Proc.OutputDataReceived += Proc_OutputDataReceived;
        Proc.ErrorDataReceived += Proc_ErrorDataReceived;
        Proc.Exited += Proc_Exited;
        Outputs = new StringBuilder();
        Errors = new StringBuilder();
        //Results = new StringBuilder();
    }
    #endregion

    private void Proc_OutputDataReceived(object? sender, EventArgs e) => OutputDataReceived?.Invoke(this, e);
    private void Proc_ErrorDataReceived(object? sender, EventArgs e) => ErrorDataReceived?.Invoke(this, e);
    private void Timer_Elapsed(object? state, ElapsedEventArgs args) {
        TimedOut?.Invoke(this, EventArgs.Empty);
        Exited?.Invoke(this, EventArgs.Empty);
    }
    private void Proc_Exited(object? sender, EventArgs e) {
        TimerTimeout.Stop();
        Exited?.Invoke(this, EventArgs.Empty);
    }

    #region Dispose
    // /// <summary>Detach all events</summary>
    // public void DetachEvents() {
    //     // lock (this) {
    //     //     OutputDataReceived = null;
    //     //     TimedOut = null;
    //     //     Exited = null;
    //     // }
    // }
    /// <inheritdoc />
    protected override ValueTask DisposeAsync(bool disposing) {
        if (!_disposed && disposing) {
            if (Proc != null) {
                try {
                    //Causes No process is associated with this object
                    //Proc.Kill();
                    if (!Proc.HasExited) Proc?.WaitForExit(10);
                } catch { }
                Proc?.Dispose();
            }
            TimerTimeout?.Dispose();
        }
        // Call the appropriate methods to clean up
        // unmanaged resources here.
        return ValueTask.CompletedTask;
    }
    #endregion Dispose
}