/* Copyright (c) <2008> <A. Zafer YURDAÇALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Xml;
using Z.Extensions;

#nullable disable
#pragma warning disable

namespace Z.Util
{
    /// <summary>JobScheduler</summary>
    /// <remarks>
    ///     Timer is not automatic, IsElapsed should be checked manually. This class uses UTC Datetime, supply the
    ///     parameters as utc;
    /// </remarks>
    [Serializable]
    public class JobScheduler : List<JobScheduler.Job>
    {
        #region Delegates
        /// <summary>Job Start delegate</summary>
        public delegate JobResults JobStart(object data, int timeout);
        #endregion

        #region Nested type: Job
        /// <summary>Job</summary>
        [Serializable]
        public class Job
        {
            #region Events
            /// <summary>Job Execution end delegate</summary>
            public event EventHandler OnEndJobExecute;
            /// <summary>Job Execution start delegate</summary>
            public event EventHandler OnStartJobExecute;
            #endregion

            #region Constructors
            /// <summary>Job</summary>
            public Job() {
                Type = JobTypes.Fixed;
                TimeToRun = DateTime.MaxValue;
                Interval = new TimeSpan(long.MaxValue);
                TimeLastRun = DateTime.Parse("01/01/1970", SysInf.InvariantFormat, DateTimeStyles.AssumeUniversal);
                TimeOut = -1;
                MaxCount = -1;
                MaxRetries = 1;
                State = JobStates.Queued;
            }
            #endregion

            /// <summary>Detach all events</summary>
            public void DetachEvents() {
                lock (this) {
                    if (OnStartJobExecute != null)
                        foreach (var d in OnStartJobExecute.GetInvocationList())
                            OnStartJobExecute -= (EventHandler) d;
                    if (OnEndJobExecute != null)
                        foreach (var d in OnEndJobExecute.GetInvocationList())
                            OnEndJobExecute -= (EventHandler) d;
                }
            }

            #region Variables
            /// <summary>Type of the Job (Timing)</summary>
            public JobTypes Type { get; set; }
            /// <summary>When the job will execute</summary>
            public DateTime TimeToRun { get; set; }
            /// <summary>Job repeat interval</summary>
            public TimeSpan Interval { get; set; }
            /// <summary>Job Schedule</summary>
            public Schedule Schedule { get; set; }
            /// <summary>Job Execution start time</summary>
            public DateTime TimeStarted { get; set; }
            /// <summary>Last Job Execution time</summary>
            public DateTime TimeLastRun { get; set; }
            /// <summary>Job timeout period</summary>
            public int TimeOut { get; set; }
            /// <summary>Maximum number of job executions</summary>
            public int MaxCount { get; set; }
            /// <summary>Current number of job executions</summary>
            public int CurCount { get; set; }
            /// <summary>Maximum job failures to abort</summary>
            public int MaxRetries { get; set; }
            /// <summary>Current job failure amount</summary>
            public int CurRetries { get; set; }
            /// <summary>Job Delegate</summary>
            public JobStart JobDelegate { get; set; }
            /// <summary>Parameter for the job delegate</summary>
            public object Data { get; set; }
            /// <summary>Last Job result</summary>
            public JobResults Result { get; set; }
            /// <summary>Last Job state</summary>
            public JobStates State { get; set; }
            #endregion Variables

            #region IsElpased
            /// <summary>Checks if it is time to execute the Job</summary>
            public bool IsElapsed {
                get {
                    //if (IsDone) return false;
                    switch (Type) {
                        case JobTypes.Fixed:
                        case JobTypes.Scheduled:
                        case JobTypes.Interval:
                            if (DateTime.UtcNow < TimeToRun) return false;
                            break;

                        default: return false;
                    }
                    return true;
                }
            }
            /// <summary>Calculate the next run time</summary>
            /// <param name="searchFrom">Calculation starts from</param>
            public DateTime NextRun(DateTime? searchFrom = null) {
                var from = (searchFrom ?? TimeLastRun).ToUniversalTime();
                switch (Type) {
                    case JobTypes.Fixed:
                        if (TimeToRun < from && State == JobStates.Done) return DateTime.MaxValue;
                        return TimeToRun;

                    case JobTypes.Scheduled: return Schedule == null ? DateTime.MaxValue : Schedule.NextRun(from);

                    case JobTypes.Interval:
                        if (State == JobStates.Done) return DateTime.MaxValue;
                        var testDate = from.Add(Interval);
                        if (TimeToRun > testDate) return TimeToRun;
                        while (testDate < DateTime.Parse("2999-01-01")) {
                            if (testDate > from) return testDate;
                            testDate = testDate.Add(Interval);
                        }
                        break;
                }
                return DateTime.MaxValue;
            }
            #endregion IsElpased

            #region JobStart CheckAndStart
            /// <summary>Check if the job should start and start the job if elapsed</summary>
            public bool CheckAndStart() {
                lock (this) {
                    if (MaxCount > 0 && MaxCount <= CurCount) {
                        State = JobStates.Done;
                        return true;
                    }

                    if (State == JobStates.Running && TimeStarted > DateTime.UtcNow.AddDays(1)) State = JobStates.Queued; // Hard timeout is 1 days

                    if (State == JobStates.Queued && IsElapsed) {
                        State = JobStates.Running;
                        ThreadPool.QueueUserWorkItem(delegate { thread_Callback(); });
                    }
                    return false;
                }
            }
            private void thread_Callback() {
                lock (this) {
                    OnStartJobExecute?.Invoke(this, EventArgs.Empty);
                    TimeStarted = DateTime.UtcNow;
                    State = JobStates.Running;
                    Result = JobDelegate(Data, TimeOut);
                    TimeLastRun = DateTime.UtcNow;
                    State = JobStates.Queued;
                    if (Result == JobResults.Error) {
                        CurRetries++;
                        if (CurRetries >= MaxRetries) {
                            var nr = NextRun(TimeLastRun);
                            if (nr == DateTime.MaxValue)
                                State = JobStates.Done;
                            else
                                TimeToRun = nr;
                        }
                        OnEndJobExecute?.Invoke(this, EventArgs.Empty);
                        return;
                    }
                    if (MaxCount > 0 && MaxCount <= CurCount || Type == JobTypes.Fixed) State = JobStates.Done;

                    var nrun = NextRun(TimeLastRun);
                    if (nrun == DateTime.MaxValue)
                        State = JobStates.Done;
                    else
                        TimeToRun = nrun;

                    CurCount++;
                    if (State == JobStates.Done) {
                        OnEndJobExecute?.Invoke(this, EventArgs.Empty);
                        return;
                    }
                    CurRetries = 0;
                    OnEndJobExecute?.Invoke(this, EventArgs.Empty);
                }
            }
            #endregion JobStart CheckAndStart

            #region ToTimingXml/ParseTimingXml
            /// <summary>Dump Job Timing to Xml</summary>
            public string ToTimingXml() //Only creates data related to timing
            {
                var xml = new XmlDocument();
                xml.CreateXmlDeclaration("1.0", null, null);
                var xroot = xml.CreateChildNode("Job");
                xroot.CreateChildNode("Type").InnerText = ((short) Type).ToString();
                switch (Type) {
                    case JobTypes.Fixed:
                    case JobTypes.Scheduled:
                        xroot.CreateChildNode("TimeToRun").InnerText = TimeToRun.ToUniversalTime().ToString(SysInf.InvariantFormat);
                        if (Schedule != null) xroot.CreateChildNode("JobSchedule").InnerXml = Schedule.ToXml();
                        break;

                    case JobTypes.Interval:
                        xroot.CreateChildNode("TimeLastRun").InnerText = TimeLastRun.ToUniversalTime().ToString(SysInf.InvariantFormat);
                        xroot.CreateChildNode("Interval").InnerText = Interval.ToString();
                        //xroot.CreateChildNode("TimeToRun").InnerText = TimeToRun.ToUniversalTime().ToString();
                        break;
                }
                return xroot.OuterXml;
            }
            /// <summary>Create Job (only timing) from Xml dump</summary>
            /// <param name="xmlStr">Job timing Xml</param>
            public static Job ParseTimingXml(string xmlStr) {
                var xml = new XmlDocument();
                try {
                    xml.LoadXml(xmlStr);
                } catch {
                    return null;
                }
                var job = xml.SelectSingleNode("Job");
                if (job == null) return null;
                var rval = new Job();
                try {
                    rval.Type = (JobTypes) job.GetInnerText("Type").To().Type<short>();
                } catch {
                    return null;
                }
                rval.TimeToRun = DateTime.Parse(job.GetInnerText("TimeToRun") ?? DateTime.MaxValue.ToString(SysInf.InvariantFormat), SysInf.InvariantFormat,
                    DateTimeStyles.AssumeUniversal);
                rval.TimeLastRun = DateTime.Parse(job.GetInnerText("TimeLastRun") ?? DateTime.MinValue.ToString(SysInf.InvariantFormat), SysInf.InvariantFormat,
                    DateTimeStyles.AssumeUniversal);
                rval.Interval = TimeSpan.Parse(job.GetInnerText("Interval") ?? "00:00");
                var sch = job.SelectSingleNode("JobSchedule");
                if (sch != null) rval.Schedule = Schedule.ParseXml(sch.InnerXml);
                return rval;
            }
            #endregion ToTimingXml/ParseTimingXml
        }
        #endregion

        #region Nested type: Schedule
        /// <summary>Variable/conditional Job </summary>
        [Serializable]
        public class Schedule
        {
            #region Fields / Properties
            #region IsElapsed
            /// <summary>Schedule is elapsed or not</summary>
            public bool IsElapsed {
                get {
                    var n = DateTime.UtcNow;
                    if (_minute != null && _minute > n.Minute) return false;
                    if (_hour != null && _hour > n.Hour) return false;
                    if (_dayofWeek != null && _dayofWeek > (int) n.DayOfWeek) return false;
                    if (_day != null && _day > n.Day) return false;
                    if (_month != null && _month > n.Month) return false;
                    if (_year != null && _year > n.Year - 2000) return false;
                    return true;
                }
            }
            #endregion IsElapsed
            #endregion

            #region Variables
            private Bitmask _minute;
            private Bitmask _hour;
            private Bitmask<DayOfWeek> _dayofWeek;
            private Bitmask _dayofWeekOrdinal;
            private Bitmask _day;
            private Bitmask _month;
            private Bitmask _year;
            #endregion Variables

            #region Parameters
            /// <summary>Selection of Minutes to run the Job</summary>
            public Schedule Minute(params int[] v) {
                if (v == null || v.Length == 0) {
                    _minute = null;
                    return this;
                }
                _minute = (_minute ?? new Bitmask()).Add(v);
                return this;
            }
            /// <summary>Selection of Hours to run the Job</summary>
            public Schedule Hour(params int[] v) {
                if (v == null || v.Length == 0) {
                    _hour = null;
                    return this;
                }
                _hour = (_hour ?? new Bitmask()).Add(v);
                return this;
            }
            /// <summary>Selection of Week Days to run the Job</summary>
            public Schedule DayOfWeek(params DayOfWeek[] v) {
                if (v == null || v.Length == 0) {
                    _dayofWeek = null;
                    return this;
                }
                _dayofWeek = (_dayofWeek ?? new Bitmask<DayOfWeek>()).Add(v);
                return this;
            }
            /// <summary>Selection of Week Day ordinals to run the Job (1st Monday Tuesday etc.)</summary>
            public Schedule DayOfWeekOrdinal(params int[] v) {
                if (v == null || v.Length == 0) {
                    _dayofWeekOrdinal = null;
                    return this;
                }
                _dayofWeekOrdinal = (_dayofWeekOrdinal ?? new Bitmask()).Add(v);
                return this;
            }
            /// <summary>Selection of Month Days to run the Job</summary>
            public Schedule Day(params int[] v) {
                if (v == null || v.Length == 0) {
                    _day = null;
                    return this;
                }
                _day = (_day ?? new Bitmask()).Add(v);
                return this;
            }
            /// <summary>Selection of Months to run the Job</summary>
            public Schedule Month(params int[] v) {
                if (v == null || v.Length == 0) {
                    _month = null;
                    return this;
                }
                _month = (_month ?? new Bitmask()).Add(v);
                return this;
            }
            /// <summary>Selection of Years to run the Job</summary>
            public Schedule Year(params int[] v) {
                if (v == null || v.Length == 0) {
                    _year = null;
                    return this;
                }
                if (_year == null) _year = new Bitmask();
                foreach (var y in v) _year.Add(y - 2000);
                return this;
            }
            #endregion Parameters

            #region NextRun
            /// <summary>Calculate the next run time</summary>
            /// <param name="searchFrom">Calculation starts from</param>
            public DateTime NextRun(DateTime searchFrom) {
                var from = searchFrom.ToUniversalTime();
                DateTime rval;
                if (_minute != null)
                    rval = new DateTime(from.AddMinutes(1).Ticks, DateTimeKind.Utc);
                else if (_hour != null)
                    rval = new DateTime(from.AddHours(1).Ticks, DateTimeKind.Utc);
                else if (_dayofWeek != null)
                    rval = new DateTime(from.AddDays(1).Ticks, DateTimeKind.Utc);
                else if (_dayofWeekOrdinal != null)
                    rval = new DateTime(from.AddDays(1).Ticks, DateTimeKind.Utc);
                else if (_day != null)
                    rval = new DateTime(from.AddDays(1).Ticks, DateTimeKind.Utc);
                else if (_month != null)
                    rval = new DateTime(from.AddMonths(1).Ticks, DateTimeKind.Utc);
                else if (_year != null)
                    rval = new DateTime(from.AddYears(1).Ticks, DateTimeKind.Utc);
                else
                    rval = new DateTime(from.AddDays(1).Ticks, DateTimeKind.Utc);

                var firstSearch = rval.Ticks;
                StartSearch:
                if (rval.Year > 2063) goto EndSearch;
                if (_minute != null)
                    for (var i = 0; i < 60; i++) {
                        var t = rval.AddMinutes(i);
                        if (_minute < t.Minute) {
                            rval = t;
                            //if (i != 0) goto StartSearch;  //infinite loop
                            break;
                        }
                    }
                if (_hour != null)
                    for (var i = 0; i < 24; i++) {
                        var t = rval.AddHours(i);
                        if (_hour < t.Hour) {
                            rval = t;
                            if (i != 0) {
                                rval = rval.AddMinutes(0 - rval.Minute);
                                goto StartSearch;
                            }
                            break;
                        }
                    }
                if (_dayofWeek != null)
                    for (var i = 0; i < 7; i++) {
                        var t = rval.AddDays(i);
                        if (_dayofWeek < t.DayOfWeek) {
                            //Day of week ordinal matching (1st monday)
                            if (_dayofWeekOrdinal != null) {
                                var dayChanged = false;
                                while (rval.Year < 2063) {
                                    if (_dayofWeekOrdinal < getWeekDayOrdinal(t)) {
                                        rval = t;
                                        if (dayChanged) {
                                            rval = rval.AddMinutes(0 - rval.Minute);
                                            rval = rval.AddHours(0 - rval.Hour);
                                            goto StartSearch;
                                        }
                                        break;
                                    }
                                    dayChanged = true;
                                    t = t.AddDays(7);
                                }
                            }
                            //End

                            rval = t;
                            if (i != 0) {
                                rval = rval.AddMinutes(0 - rval.Minute);
                                rval = rval.AddHours(0 - rval.Hour);
                                goto StartSearch;
                            }
                            break;
                        }
                    }

                if (_day != null)
                    for (var i = 0; i < 31; i++) {
                        var t = rval.AddDays(i);
                        if (_day < t.Day) {
                            rval = t;
                            if (i != 0) {
                                rval = rval.AddMinutes(0 - rval.Minute);
                                rval = rval.AddHours(0 - rval.Hour);
                                goto StartSearch;
                            }
                            break;
                        }
                    }
                if (_month != null)
                    for (var i = 0; i < 12; i++) {
                        var t = rval.AddMonths(i);
                        if (_month < t.Month) {
                            rval = t;
                            if (i != 0) {
                                rval = rval.AddDays(1 - rval.Day);
                                goto StartSearch;
                            }
                            break;
                        }
                    }
                if (_year != null)
                    for (var i = 0; i < 64 - (rval.Year - 2000); i++) {
                        var t = rval.AddYears(i);
                        if (_year < t.Year - 2000) {
                            rval = t;
                            if (i != 0) {
                                rval = rval.AddMonths(1 - rval.Month);
                                goto StartSearch;
                            }
                            break;
                        }
                    }
                EndSearch:
                if (firstSearch == rval.Ticks) return DateTime.MaxValue;
                return rval;
            }

            private int getWeekDayOrdinal(DateTime date) {
                var month = date.Month;
                var wd = date.DayOfWeek;
                var dt = date.AddDays(1 - date.Day);
                var rval = 0;
                while (dt <= date) {
                    if (wd == dt.DayOfWeek) rval++;
                    dt = dt.AddDays(1);
                }
                return rval;
            }

            #region Alternate code (Disabled) (Selahattin)
            /*
            public DateTime NextRun2(DateTime searchFrom)
            {
                var from = searchFrom.ToUniversalTime();
                DateTime rval = new DateTime(from.Year, from.Month, from.Day, from.Hour, 0, 0);
                DateTime startDate = rval;

                if (_hour == null)
                {
                    int[] tempHours = new int[1] { startDate.Hour };
                    Hour(tempHours);
                }

                if (_month == null)
                {
                    int[] tempMonths = new int[2] { startDate.Month, startDate.AddMonths(1).Month };
                    Month(tempMonths);
                }

                if (_dayofWeek != null)
                {
                    if (_day == null)
                    {
                        _day = new Bitmask();
                    }
                    _day.Add(getFirstDayOfWeek(rval, startDate));
                }

                if (_day == null)
                {
                    int[] tempDays = new int[2] { startDate.Day, startDate.AddDays(1).Day };
                    Day(tempDays);
                }

                if (calculate(ref rval, startDate))
                {
                    return rval;
                }

                return DateTime.MinValue;
            }

            private bool calculateHour(ref DateTime rval, DateTime startDate)
            {
                rval = new DateTime(rval.Year, rval.Month, rval.Day, 1, 0, 0);
                DateTime tempDate = rval;
                for (int i = 0; i <= 24; i++)
                {
                    if (_hour < rval.Hour)
                    {
                        if (rval > startDate)
                        {
                            return true;
                        }
                    }
                    rval = rval.AddHours(1);
                }
                rval = tempDate;
                return false;
            }

            private bool calculateDay(ref DateTime rval, DateTime startDate)
            {
                rval = new DateTime(rval.Year, rval.Month, 1, rval.Hour, 0, 0);
                DateTime tempDate = rval;
                for (int i = 1; i <= 31; i++)
                {
                    if (_day < rval.Day)
                    {
                        if (calculateHour(ref rval, startDate))
                        {
                            return true;
                        }
                    }
                    rval = rval.AddDays(1);
                }
                rval = tempDate;
                return false;
            }

            private int getFirstDayOfWeek(DateTime rval, DateTime startDate)
            {
                int startingMonth = rval.Month;
                for (int i = 0; i < 7; i++)
                {
                    if (_dayofWeek < rval.DayOfWeek)
                    {
                        if (calculateHour(ref rval, startDate))
                        {
                            return rval.Day;
                        }
                    }
                    rval = rval.AddDays(1);
                    if (startingMonth != rval.Month)
                    {
                        for (int j = 1; j <= 12; j++)
                        {
                            if (_month < rval.Month)
                            {
                                startingMonth = rval.Month;
                                i = 0;
                                break;
                            }
                            rval = rval.AddMonths(1);
                        }
                    }
                }
                return rval.Day;
            }

            private bool calculateMonth(ref DateTime rval, DateTime startDate)
            {
                rval = new DateTime(rval.Year, 1, rval.Day, rval.Hour, 0, 0);
                DateTime tempDate = rval;
                for (int i = 1; i <= 12; i++)
                {
                    if (_month < rval.Month)
                    {
                        if (calculateDay(ref rval, startDate))
                        {
                            return true;
                        }
                    }
                    rval = rval.AddMonths(1);
                }
                rval = tempDate;
                return false;
            }

            private bool calculate(ref DateTime rval, DateTime startDate)
            {
                rval = new DateTime(startDate.Year, rval.Month, rval.Day, rval.Hour, 0, 0);
                if (!calculateMonth(ref rval, startDate))
                {
                    rval = rval.AddYears(1);
                    calculateMonth(ref rval, startDate);
                }
                return true;
            } */
            #endregion Alternate code (Disabled) (Selahattin)
            #endregion NextRun

            #region ToXml/ParseXml
            /// <summary>Xml Dump of the schedule</summary>
            public string ToXml() {
                var xml = new XmlDocument();
                xml.CreateXmlDeclaration("1.0", null, null);
                var xroot = xml.CreateChildNode("Schedule");
                if (_minute != null) xroot.CreateChildNode("minute").InnerText = _minute.Mask.ToString();
                if (_hour != null) xroot.CreateChildNode("hour").InnerText = _hour.Mask.ToString();
                if (_dayofWeek != null) xroot.CreateChildNode("dayofWeek").InnerText = _dayofWeek.Mask.ToString();
                if (_dayofWeekOrdinal != null) xroot.CreateChildNode("dayofWeekOrdinal").InnerText = _dayofWeekOrdinal.Mask.ToString();
                if (_day != null) xroot.CreateChildNode("day").InnerText = _day.Mask.ToString();
                if (_month != null) xroot.CreateChildNode("month").InnerText = _month.Mask.ToString();
                if (_year != null) xroot.CreateChildNode("year").InnerText = _year.Mask.ToString();
                return xroot.OuterXml;
            }
            /// <summary>Recreate schedule from the Xml Dump</summary>
            /// <param name="xmlStr">Schedule XML dump string</param>
            public static Schedule ParseXml(string xmlStr) {
                var xml = new XmlDocument();
                try {
                    xml.LoadXml(xmlStr);
                } catch {
                    return null;
                }
                var sch = xml.SelectSingleNode("Schedule");
                if (sch == null) return null;
                var rval = new Schedule();
                Bitmask minute, hour, day, dayofWeekOrdinal, month, year;
                minute = new Bitmask(sch.GetInnerText("minute").To().Type<long>());
                hour = new Bitmask(sch.GetInnerText("hour").To().Type<long>());
                var dayofWeek = new Bitmask<DayOfWeek>(sch.GetInnerText("dayofWeek").To().Type<long>());
                dayofWeekOrdinal = new Bitmask(sch.GetInnerText("dayofWeekOrdinal").To().Type<long>());
                day = new Bitmask(sch.GetInnerText("day").To().Type<long>());
                month = new Bitmask(sch.GetInnerText("month").To().Type<long>());
                year = new Bitmask(sch.GetInnerText("year").To().Type<long>());

                if (minute.Mask != 0) rval._minute = minute;
                if (hour.Mask != 0) rval._hour = hour;
                if (dayofWeek.Mask != 0) rval._dayofWeek = dayofWeek;
                if (dayofWeekOrdinal.Mask != 0) rval._dayofWeekOrdinal = dayofWeekOrdinal;
                if (day.Mask != 0) rval._day = day;
                if (month.Mask != 0) rval._month = month;
                if (year.Mask != 0) rval._year = year;
                return rval;
            }
            #endregion ToXml/ParseXml

            #region Properties
            /// <summary>Selection of Minutes to run the Job</summary>
            public IEnumerable<short> Minutes => _minute?.FlagsEnabled ?? new short[0];
            /// <summary>Selection of Hours to run the Job</summary>
            public IEnumerable<short> Hours => _hour?.FlagsEnabled ?? new short[0];
            /// <summary>Selection of Week Days to run the Job</summary>
            public IEnumerable<DayOfWeek> DaysOfWeek => _dayofWeek?.FlagsEnabled ?? new DayOfWeek[0];
            /// <summary>Selection of Week Day Ordinals to run the Job (1st Monday, Tuesday) etc</summary>
            public IEnumerable<short> DayOfWeekOrdinals => _dayofWeekOrdinal?.FlagsEnabled ?? new short[0];
            /// <summary>Selection of Month Days to run the Job</summary>
            public IEnumerable<short> Days => _day?.FlagsEnabled ?? new short[0];
            /// <summary>Selection of Months to run the Job</summary>
            public IEnumerable<short> Months => _month?.FlagsEnabled ?? new short[0];
            /// <summary>Selection of Years to run the Job</summary>
            public IEnumerable<short> Years => _year?.FlagsEnabled ?? new short[0];
            #endregion Properties
        }
        #endregion

        #region AddJob
        /// <summary>Add Interval Job</summary>
        /// <param name="job">Job to Add</param>
        /// <param name="interval">Interval of the job execution</param>
        /// <param name="lastRun">Last Job run time</param>
        /// <param name="timeOutSecs">Job timeout in seconds</param>
        /// <param name="maxretries">Maximum number of retries upon failure</param>
        /// <param name="maxcount">Maximum number of sucessful job executions until job is no longer valid</param>
        public void AddIntervalJob(JobStart job, TimeSpan interval, DateTime lastRun, int timeOutSecs = 60, int maxretries = 5, int maxcount = -1) {
            var rval = new Job {
                Interval = interval,
                JobDelegate = job,
                TimeOut = timeOutSecs,
                MaxRetries = maxretries,
                MaxCount = maxcount,
                State = JobStates.Queued,
                Result = JobResults.None,
                Type = JobTypes.Interval,
                TimeLastRun = lastRun.ToUniversalTime()
            };
            rval.TimeToRun = rval.TimeLastRun.Add(rval.Interval);
            /*if (lastRun == DateTime.MinValue)
            {
                rval.TimeToRun = DateTime.UtcNow.Add(interval);
            }
            else
            {
                rval.TimeToRun = lastRun.Add(interval);
            }*/

            lock (this) {
                Add(rval);
            }
        }
        /// <summary>Add Scheduled Job</summary>
        /// <param name="job">Job to Add</param>
        /// <param name="schedule">Job execution schedule</param>
        /// <param name="timeOutSecs">Job timeout in seconds</param>
        /// <param name="maxretries">Maximum number of retries upon failure</param>
        /// <param name="maxcount">Maximum number of sucessful job executions until job is no longer valid</param>
        public void AddScheduledJob(JobStart job, Schedule schedule, int timeOutSecs = -1, int maxretries = 1, int maxcount = -1) {
            var rval = new Job {
                TimeToRun = schedule.NextRun(DateTime.UtcNow),
                Schedule = schedule,
                JobDelegate = job,
                TimeOut = timeOutSecs,
                MaxRetries = maxretries,
                MaxCount = maxcount,
                State = JobStates.Queued,
                Result = JobResults.None,
                Type = JobTypes.Scheduled
            };
            lock (this) {
                Add(rval);
            }
        }
        /// <summary>Add Fixed time Job</summary>
        /// <param name="job">Job to Add</param>
        /// <param name="timeToRun">When the job will start runing</param>
        /// <param name="timeOutSecs">Job timeout in seconds</param>
        /// <param name="maxretries">Maximum number of retries upon failure</param>
        /// <param name="maxcount">Maximum number of sucessful job executions until job is no longer valid</param>
        public void AddFixedJob(JobStart job, DateTime timeToRun, int timeOutSecs = -1, int maxretries = 1, int maxcount = -1) {
            var rval = new Job {
                TimeToRun = timeToRun.ToUniversalTime(),
                JobDelegate = job,
                TimeOut = timeOutSecs,
                MaxRetries = maxretries,
                MaxCount = maxcount,
                State = JobStates.Queued,
                Result = JobResults.None,
                Type = JobTypes.Fixed
            };
            lock (this) {
                Add(rval);
            }
        }
        /// <summary>New Schedule</summary>
        public static Schedule CreateSchedule() => new Schedule();
        #endregion AddJob

        #region Enums
        /// <summary>Job types by timing</summary>
        public enum JobTypes : short
        {
            /// <summary>Executes every x seconds</summary>
            Interval,
            /// <summary>Executes when a certain day month year etc. condition is met</summary>
            Scheduled,
            /// <summary>Executes at a set time</summary>
            Fixed
        }

        /// <summary>Result of the executed job</summary>
        public enum JobResults : short
        {
            /// <summary>N/A</summary>
            None,
            /// <summary>Failed</summary>
            Error,
            /// <summary>Successful</summary>
            Success,
            /// <summary>Completed but with errors</summary>
            SuccessWithErrors
        }

        /// <summary>State of the job</summary>
        public enum JobStates : short
        {
            /// <summary>Waiting to be executed</summary>
            Queued,
            /// <summary>Currently Running</summary>
            Running,
            /// <summary>Finished</summary>
            Done
        }
        #endregion Enums
    }
}