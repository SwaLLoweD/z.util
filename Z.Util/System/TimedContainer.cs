﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIS>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Z.Extensions;

namespace Z.Util;

/// <summary>Timer with an object container</summary>
public class TimedContainer<T> : TimedContainer
{
    /// <inheritdoc/>
    public new T Data { get { return (T)base.Data; } set { base.Data = value; } }
    /// <inheritdoc/>
    public new event EventHandler<TimedContainerElapsedEventArgs<T>> Elapsed;
    /// <inheritdoc/>
    public TimedContainer(T data) : base(data) { }
    /// <inheritdoc/>
    public TimedContainer(T data, double interval) : base(data, interval) { }
    /// <inheritdoc/>
    protected override void timer_Elapsed(object state) {
        Elapsed.Raise(this, new TimedContainerElapsedEventArgs<T>(e.SignalTime, Data));
    }
}

/// <summary>Timer with an object container</summary>
public class TimedContainer : Timer
{
    /// <summary>User Data</summary>
    public virtual object Data { get; set; }
    /// <inheritdoc/>
    public new event EventHandler<TimedContainerElapsedEventArgs> Elapsed;
    /// <inheritdoc/>
    public TimedContainer(object data) : base() { init(data); }
    /// <inheritdoc/>
    public TimedContainer(object data, double interval) : base(interval) { init(data); }
    /// <summary>Initialize</summary>
    protected void init(object data) {
        Data = data;
        base.Elapsed += timer_Elapsed;
        base.Disposed += timer_Disposed;
    }

    private void timer_Disposed(object sender, EventArgs e) {
        Disposed -= timer_Disposed;
        base.Elapsed -= timer_Elapsed;
    }

    /// <summary>Event trigger for encapsulated timer</summary>
    protected virtual void timer_Elapsed(object state) {
        Elapsed.Raise(this, new TimedContainerElapsedEventArgs(e.SignalTime, Data));
    }
}
/// <inheritdoc/>
public class TimedContainerElapsedEventArgs<T> : TimedContainerElapsedEventArgs
{
    /// <inheritdoc/>
    public new T Data { get { return (T)base.Data; } protected set { base.Data = value; } }
    /// <inheritdoc/>
    public TimedContainerElapsedEventArgs(DateTime signalTime, T data) : base(signalTime, data) { }
}

/// <summary>Timed Container Event Arguments</summary>
public class TimedContainerElapsedEventArgs : EventArgs
{
    /// <summary>User Data</summary>
    public virtual object Data { get; protected set; }
    /// <summary>Gets the time the System.Timers.Timer.Elapsed event was raised.</summary>
    public DateTime SignalTime { get; protected set; }
    /// <summary>Timed Container Event Arguments</summary>
    public TimedContainerElapsedEventArgs(DateTime signalTime, object data) {
        SignalTime = signalTime;
        Data = data;
    }

}