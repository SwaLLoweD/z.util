using System.Threading.Tasks;

namespace Z.Util;

///<summary>Async Event Handler</summary>
public delegate Task EventHandlerAsync<TEventArgs>(object sender, TEventArgs e);