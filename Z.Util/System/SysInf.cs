﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Z.Extensions;

#pragma warning disable IDE1006
namespace Z.Util;

/// <summary>System Information</summary>
public static class SysInf
{
    #region Constants / Static Fields
    private static readonly int[] linux = { (int)PlatformID.Unix, 128 /* Mono */};
    private static readonly int[] windows = { (int)PlatformID.Win32S, (int)PlatformID.Win32Windows, (int)PlatformID.Win32NT };

    private static StringBuilder? outDirs;

    private static bool? isMono;
    #endregion

    #region Fields / Properties
    /// <summary>Directory-separator for current system</summary>
    public static string bs => Path.DirectorySeparatorChar.ToString();

    /// <summary>Number of CPUs</summary>
    public static int CpuCount => Environment.ProcessorCount;
    /// <summary>Current Culture</summary>
    public static CultureInfo CurrentFormat => CultureInfo.CurrentCulture;

    //private static float perfMonCounter(string category, string counter, string instance = null)
    //{
    //    PerformanceCounter cnt = new PerformanceCounter(category, counter);
    //    if (instance != null) cnt.InstanceName = instance;
    //    cnt.NextValue();
    //    System.Threading.Thread.Sleep(1000); // 1 sec wait
    //    float rval = cnt.NextValue();
    //    cnt.Dispose();
    //    return rval;
    //}

    /// <summary>Current Application directory</summary>
    public static string DirApp => AppDomain.CurrentDomain.BaseDirectory; // return AppContext.BaseDirectory;
    /// <summary>Current Application "bin" directory</summary>
    public static string? DirAppBin {
        get {
            var codeBase = Assembly.GetExecutingAssembly().Location; //Assembly.GetEntryAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }
    }
    /// <summary>Application Data directory</summary>
    public static string DirAppData => Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
    /// <summary>Common Application Data directory</summary>
    public static string DirAppDataCommon => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
    /// <summary>Current Directory</summary>
    public static string DirCur => Environment.CurrentDirectory; //Directory.GetCurrentDirectory()
    /// <summary>Program Files directory (for 64-bit processes if applicable)</summary>
    public static string? DirProg => Os64 && Proc64 ? pf : Os64 ? pf64 : pf;
    /// <summary>Program Files directory for 32-bit Processes</summary>
    public static string DirProg32 => Os64 ? pfx86 : pf;
    /// <summary>Program Files directory for Current Process</summary>
    public static string DirProgForProc => pf;
    /// <summary>Current System Directory</summary>
    public static string DirSys => Environment.SystemDirectory;
    /// <summary>Invariant Culture</summary>
    public static CultureInfo InvariantFormat => CultureInfo.InvariantCulture;
    ///<summary>Framework Is Mono or not</summary>
    public static bool IsMono {
        get {
            if (isMono.HasValue) return isMono.Value;
            var t = Type.GetType("Mono.Runtime");
            isMono = t != null;
            return isMono.Value;
        }
    }
    /// <summary>Line-feed (NewLine) string for current system</summary>
    public static string lf => Environment.NewLine;
    /// <summary>Current Machine Name</summary>
    public static string MachineName => Environment.MachineName;
    ///<summary>Current Process Memory Usage Percent</summary>
    public static float MemProcPct {
        get {
            var currentProcess = Process.GetCurrentProcess();
            return currentProcess.WorkingSet64;
        }
    }
    /// <summary>Current Operating System</summary>
    public static OperatingSystem Os => Environment.OSVersion;
    /// <summary>If current Operating System is 64-bit</summary>
    public static bool Os64 => Environment.Is64BitOperatingSystem; //return RuntimeInformation.OSArchitecture == Architecture.Arm64 || RuntimeInformation.OSArchitecture == Architecture.X64; 
    /// <summary>If current Operating System is Linux</summary>
    public static bool OsWindows => windows.Contains((int)Environment.OSVersion.Platform);
    /// <summary>If current Operating System is Linux</summary>
    public static bool OsLinux => linux.Contains((int)Environment.OSVersion.Platform);
    /// <summary>If current Operating System is MacOs</summary>
    public static bool OsMacOs => PlatformID.MacOSX == Environment.OSVersion.Platform;
    /// <summary>If current Operating System is Unix-based</summary>
    public static bool OsUnix => OsLinux || OsMacOs;
    /// <summary>If current Operating System is MacOs</summary>
    public static bool OsXbox => PlatformID.Xbox == Environment.OSVersion.Platform;
    /// <summary>Current Operating System Version</summary>
    public static string OsVersion => Environment.OSVersion.VersionString;
    /// <summary>If current Process is 64-bit</summary>
    public static bool Proc64 => Environment.Is64BitProcess;
    /// <summary>Text output of SpecialfolderPaths on this System</summary>
    public static string SpecialFolderPaths {
        get {
            outDirs ??= new StringBuilder();
            foreach (var s in Fnc.EnumList<Environment.SpecialFolder>()) outDirs.AppendLine("{0}:{1}", s.ToString(), Environment.GetFolderPath(s));
            return outDirs.ToString();
        }
    }

    ///// <summary>System CPU usage percent</summary>
    //#if !__IOS__
    //        public static float CpuSysPct { get { return perfMonCounter("Processor", "% Processor Time", "_Total"); } }
    //        /// <summary>Process CPU usage percent</summary>
    //        public static float CpuProcPct { get { return perfMonCounter("Process", "% Processor Time", "_Total"); } }
    //        /// <summary>System Network (bytes Sent)</summary>
    //        public static float NetBytesSent { get { return perfMonCounter("Network Interface", "Bytes Sent/sec"); } }
    //        /// <summary>System Network (bytes Received)</summary>
    //        public static float NetBytesReceived { get { return perfMonCounter("Network Interface", "Bytes Received/sec"); } }
    //        /// <summary>System Network (bytes Total)</summary>
    //        public static float NetBytesTotal { get { return perfMonCounter("Network Interface", "Bytes Total/sec"); } }
    //#endif
    ///<summary>System-clock UTC Offset</summary>
    public static double UtcOffset => DateTime.UtcNow.Subtract(DateTime.UtcNow).TotalHours;

    private static string pf => Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
    private static string? pf64 => Environment.GetEnvironmentVariable("ProgramW6432");
    private static string pfx86 => Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
    #endregion

    /// <summary>Sets the priority of a Process system-wide (Requires administrator privileges)</summary>
    /// <param name="processName">Name of the Process.</param>
    /// <param name="priorityClass">Priorty to set.</param>
    /// <returns>true if operation succeeded false if not</returns>
    public static bool ProcessPrioritySet(string processName, ProcessPriorityClass priorityClass) {
        try {
            var sProcs = Process.GetProcessesByName(processName);
            if (sProcs == null || sProcs.Length == 0) return false;
            foreach (var sProc in sProcs) {
                if (sProc.PriorityClass == priorityClass) continue;
                sProc.PriorityClass = priorityClass;
            }
            return true;
        } catch { }
        return false;
    }
    /// <summary>Kills a process (Requires administrator privileges)</summary>
    /// <param name="processName">Name of the Process.</param>
    /// <returns>true if operation succeeded false if not</returns>
    public static bool ProcessKill(string processName) {
        try {
            var sProcs = Process.GetProcessesByName(processName);
            foreach (var sProc in sProcs) sProc.Kill();
            return true;
        } catch { }
        return false;
    }
}