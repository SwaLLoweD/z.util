﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Z.Util.Mathematics;

/// <summary>Roman Numeral Helper</summary>
public static class RomanNumeral
{
    #region Constants / Static Fields
    private const int NumberOfRomanNumeralMaps = 13;
    /// <summary>MinValue</summary>
    public const int MinValue = 1;
    /// <summary>MaxValue</summary>
    public const int MaxValue = 3999;

    private static readonly Dictionary<string, int> RomanNumerals =
        new(NumberOfRomanNumeralMaps) {
                {"M", 1000},
                {"CM", 900},
                {"D", 500},
                {"CD", 400},
                {"C", 100},
                {"XC", 90},
                {"L", 50},
                {"XL", 40},
                {"X", 10},
                {"IX", 9},
                {"V", 5},
                {"IV", 4},
                {"I", 1}
        };
    private static readonly Regex ValidRomanNumeral = new(
        "^(?i:(?=[MDCLXVI])((M{0,3})((C[DM])|(D?C{0,3}))"
        + "?((X[LC])|(L?XX{0,2})|L)?((I[VX])|(V?(II{0,2}))|V)?))$",
        RegexOptions.Compiled);
    #endregion

    /// <summary>IsValid</summary>
    public static bool IsValidRomanNumeral(string value) => ValidRomanNumeral.IsMatch(value);
    /// <summary>Parse</summary>
    public static int ParseRomanNumeral(string value) {
        if (value == null) throw new ArgumentNullException(nameof(value));
        value = value.ToUpperInvariant().Trim();
        var length = value.Length;
        if (length == 0 || !IsValidRomanNumeral(value)) throw new ArgumentException("Empty or invalid Roman numeral string.", nameof(value));
        var total = 0;
        var i = length;

        while (i > 0) {
            var digit = RomanNumerals[value[--i].ToString()];
            if (i > 0) {
                var previousDigit = RomanNumerals[value[i - 1].ToString()];
                if (previousDigit < digit) {
                    digit -= previousDigit;
                    i--;
                }
            }
            total += digit;
        }

        return total;
    }
    /// <summary>String</summary>
    public static string ToRomanNumeralString(int value) {
        if (value < MinValue || value > MaxValue) throw new ArgumentOutOfRangeException(nameof(value), value, "Argument out of Roman numeral range.");
        const int maxRomanNumeralLength = 15;
        var sb = new StringBuilder(maxRomanNumeralLength);

        foreach (var pair in RomanNumerals) {
            while (value / pair.Value > 0) {
                sb.Append(pair.Key);
                value -= pair.Value;
            }
        }

        return sb.ToString();
    }
}