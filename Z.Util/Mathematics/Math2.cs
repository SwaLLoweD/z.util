﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;

//using Utilities.DataTypes;
#endregion Usings

namespace Z.Util.Mathematics;

/// <summary>Extension methods that add basic math functions</summary>
public static class Math2
{
    #region Factorial
    /// <summary>Calculates the factorial for a number</summary>
    /// <param name="input">Input value (N!)</param>
    /// <returns>The factorial specified</returns>
    public static int Factorial(int input) {
        var Value1 = 1;
        for (var x = 2; x <= input; ++x) Value1 *= x;
        return Value1;
    }
    #endregion Factorial

    #region GreatestCommonDenominator
    /// <summary>Returns the greatest common denominator between value1 and value2</summary>
    /// <param name="value1">Value 1</param>
    /// <param name="value2">Value 2</param>
    /// <returns>The greatest common denominator if one exists</returns>
    public static long GreatestCommonDenominator(long value1, long value2) {
        value1 = Math.Abs(value1);
        value2 = Math.Abs(value2);
        while (value1 != 0 && value2 != 0) {
            if (value1 > value2)
                value1 %= value2;
            else
                value2 %= value1;
        }

        return value1 == 0 ? value2 : value1;
    }
    #endregion GreatestCommonDenominator

    #region Permute
    /// <summary>Finds all permutations of the items within the list</summary>
    /// <typeparam name="T">Object type in the list</typeparam>
    /// <param name="input">Input list</param>
    /// <returns>The list of permutations</returns>
    public static IDictionary<int, T> Permute<T>(IEnumerable<T> input) {
        if (input == null) throw new ArgumentNullException(nameof(input));
        var current = input.ToList();
        var cnt = current.Count;
        var returnValue = new Dictionary<int, T>();
        var max = Factorial(cnt - 1);
        var currentValue = 0;
        for (var x = 0; x < cnt; ++x) {
            var z = 0;
            while (z < max) {
                var y = cnt - 1;
                while (y > 1) {
                    (current[y], current[y - 1]) = (current[y - 1], current[y]);
                    --y;
                    foreach (var item in current) returnValue.Add(currentValue, item);
                    ++z;
                    ++currentValue;
                    if (z == max) break;
                }
            }
            if (x + 1 != cnt) {
                current.Clear();
                current.AddRange(input);
                (current[x + 1], current[0]) = (current[0], current[x + 1]);
            }
        }
        return returnValue;
    }
    #endregion Permute

    #region StandardDeviation
    /// <summary>Gets the standard deviation</summary>
    /// <param name="values">List of values</param>
    /// <returns>The standard deviation</returns>
    public static double StandardDeviation(IEnumerable<double> values) => Math.Sqrt(Variance(values));
    /// <summary>Gets the standard deviation</summary>
    /// <param name="values">List of values</param>
    /// <returns>The standard deviation</returns>
    public static double StandardDeviation(IEnumerable<int> values) => Math.Sqrt(Variance(values));
    /// <summary>Gets the standard deviation</summary>
    /// <param name="values">List of values</param>
    /// <returns>The standard deviation</returns>
    public static double StandardDeviation(IEnumerable<float> values) => Math.Sqrt(Variance(values));
    #endregion StandardDeviation

    #region Variance
    /// <summary>Calculates the variance of a list of values</summary>
    /// <param name="values">List of values</param>
    /// <returns>The variance</returns>
    public static double Variance(IEnumerable<double> values) {
        if (values?.Any() != true) return 0;
        var vArr = values.ToArray();
        var meanValue = vArr.Average();
        double sum = 0;
        foreach (var value in vArr) sum += Math.Pow(value - meanValue, 2);
        return sum / vArr.Length;
    }

    /// <summary>Calculates the variance of a list of values</summary>
    /// <param name="values">List of values</param>
    /// <returns>The variance</returns>
    public static double Variance(IEnumerable<int> values) {
        if (values?.Any() != true) return 0;
        var vArr = values.ToArray();
        var meanValue = vArr.Average();
        double sum = 0;
        foreach (var value in vArr) sum += Math.Pow(value - meanValue, 2);
        return sum / vArr.Length;
    }

    /// <summary>Calculates the variance of a list of values</summary>
    /// <param name="values">List of values</param>
    /// <returns>The variance</returns>
    public static double Variance(IEnumerable<float> values) {
        if (values?.Any() != true) return 0;
        var vArr = values.ToArray();
        double meanValue = vArr.Average();
        double sum = 0;
        foreach (int value in vArr) sum += Math.Pow(value - meanValue, 2);
        return sum / vArr.Length;
    }
    #endregion Variance

    #region Decimal Math
    /// <summary>Returns a specified number raised to the specified power.</summary>
    /// <param name="x">A decimal number to be raised to a power.</param>
    /// <param name="y">A decimal number that specifies a power.</param>
    /// <returns>The number x raised to the power y. Accurate up to 4 decimals</returns>
    public static decimal Pow(decimal x, decimal y) => (long)(Math.Pow((double)x, (double)y) * 10000.0) / 10000m;

    /// <summary>Returns the square root of a decimal using Babylonian Method.</summary>
    /// <param name="x">A decimal number</param>
    /// <param name="guess">Starting point for the search.</param>
    /// <returns>The square root of x</returns>
    public static decimal Sqrt(decimal x, decimal? guess = null) {
        var ourGuess = guess ?? x / 2m;
        var result = x / ourGuess;
        var average = (ourGuess + result) / 2m;
        var flag = average == ourGuess;
        var result2 = flag ? average : Sqrt(x, average);
        return result2;
    }
    #endregion
}