﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

using System;
using System.Xml.Serialization;
namespace Z.Util.Mathematics;

/// <summary>Vector class (holds three items)</summary>
[Serializable]
public class Vector3
{
    #region Constructors
    #region Constructor
    /// <summary>Constructor</summary>
    /// <param name="x">X direction</param>
    /// <param name="y">Y direction</param>
    /// <param name="z">Z direction</param>
    public Vector3(double x, double y, double z) {
        X = x;
        Y = y;
        Z = z;
    }
    #endregion Constructor
    #endregion

    #region Public Functions
    /// <summary>Normalizes the vector</summary>
    public virtual void Normalize() {
        var normal = Magnitude;
        if (normal > 0) {
            normal = 1 / normal;
            X *= normal;
            Y *= normal;
            Z *= normal;
        }
    }
    #endregion Public Functions

    #region Public Overridden Functions
    /// <summary>To string function</summary>
    /// <returns>String representation of the vector</returns>
    public override string ToString() => "(" + X + "," + Y + "," + Z + ")";

    /// <summary>Gets the hash code</summary>
    /// <returns>The hash code</returns>
    public override int GetHashCode() => (int)(X + Y + Z) % int.MaxValue;

    /// <summary>Determines if the items are equal</summary>
    /// <param name="obj">Object to compare</param>
    /// <returns>true if they are, false otherwise</returns>
    public override bool Equals(object? obj) {
        if (obj is Vector3 vector3) return this == vector3;
        return false;
    }
    #endregion Public Overridden Functions

    #region Public Properties
    /// <summary>Used for converting this to an array and back</summary>
    public virtual double[] Array {
        get => new[] { X, Y, Z };
        set {
            if (value.Length != 3) return;
            X = value[0];
            Y = value[1];
            Z = value[2];
        }
    }

    /// <summary>Returns the magnitude of the vector</summary>
    public virtual double Magnitude => Math.Sqrt((X * X) + (Y * Y) + (Z * Z));

    /// <summary>X value</summary>
    [XmlElement]
    public virtual double X { get; set; }

    /// <summary>Y Value</summary>
    [XmlElement]
    public virtual double Y { get; set; }

    /// <summary>Z value</summary>
    [XmlElement]
    public virtual double Z { get; set; }
    #endregion Public Properties

    #region Public Static Functions
    /// <summary>Addition</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static Vector3 operator +(Vector3 v1, Vector3 v2) => new(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);

    /// <summary>Subtraction</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static Vector3 operator -(Vector3 v1, Vector3 v2) => new(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);

    /// <summary>Negation</summary>
    /// <param name="v1">Item 1</param>
    /// <returns>The resulting vector</returns>
    public static Vector3 operator -(Vector3 v1) => new(-v1.X, -v1.Y, -v1.Z);

    /// <summary>Less than</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static bool operator <(Vector3 v1, Vector3 v2) => v1.Magnitude < v2.Magnitude;

    /// <summary>Less than or equal</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static bool operator <=(Vector3 v1, Vector3 v2) => v1.Magnitude <= v2.Magnitude;

    /// <summary>Greater than</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static bool operator >(Vector3 v1, Vector3 v2) => v1.Magnitude > v2.Magnitude;

    /// <summary>Greater than or equal</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static bool operator >=(Vector3 v1, Vector3 v2) => v1.Magnitude >= v2.Magnitude;

    /// <summary>Equals</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static bool operator ==(Vector3 v1, Vector3 v2) => v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z;

    /// <summary>Not equals</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static bool operator !=(Vector3 v1, Vector3 v2) => !(v1 == v2);

    /// <summary>Division</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="d">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static Vector3 operator /(Vector3 v1, double d) => new(v1.X / d, v1.Y / d, v1.Z / d);

    /// <summary>Multiplication</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="d">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static Vector3 operator *(Vector3 v1, double d) => new(v1.X * d, v1.Y * d, v1.Z * d);

    /// <summary>Multiplication</summary>
    /// <param name="d">Item 2</param>
    /// <param name="v1">Item 1</param>
    /// <returns>The resulting vector</returns>
    public static Vector3 operator *(double d, Vector3 v1) => new(v1.X * d, v1.Y * d, v1.Z * d);

    /// <summary>Does a cross product</summary>
    /// <param name="v1">Item 1</param>
    /// <param name="v2">Item 2</param>
    /// <returns>The resulting vector</returns>
    public static Vector3 operator *(Vector3 v1, Vector3 v2) {
        var TempVector = new Vector3(0.0, 0.0, 0.0) {
            X = (v1.Y * v2.Z) - (v1.Z * v2.Y),
            Y = (v1.Z * v2.X) - (v1.X * v2.Z),
            Z = (v1.X * v2.Y) - (v1.Y * v2.X)
        };
        return TempVector;
    }

    /// <summary>Does a dot product</summary>
    /// <param name="v1">Vector 1</param>
    /// <param name="V2">Vector 2</param>
    /// <returns>a dot product</returns>
    public static double DotProduct(Vector3 v1, Vector3 V2) => (v1.X * V2.X) + (v1.Y * V2.Y) + (v1.Z * V2.Z);

    /// <summary>Interpolates between the vectors</summary>
    /// <param name="V1">Vector 1</param>
    /// <param name="V2">Vector 2</param>
    /// <param name="Control">Percent to move between 1 and 2</param>
    /// <returns>The interpolated vector</returns>
    public static Vector3 Interpolate(Vector3 V1, Vector3 V2, double Control) {
        var TempVector = new Vector3(0.0, 0.0, 0.0) {
            X = (V1.X * (1 - Control)) + (V2.X * Control),
            Y = (V1.Y * (1 - Control)) + (V2.Y * Control),
            Z = (V1.Z * (1 - Control)) - (V2.Z * Control)
        };
        return TempVector;
    }

    /// <summary>The distance between two vectors</summary>
    /// <param name="V1">Vector 1</param>
    /// <param name="V2">Vector 2</param>
    /// <returns>Distance between the vectors</returns>
    public static double Distance(Vector3 V1, Vector3 V2) => Math.Sqrt(((V1.X - V2.X) * (V1.X - V2.X)) + ((V1.Y - V2.Y) * (V1.Y - V2.Y)) + ((V1.Z - V2.Z) * (V1.Z - V2.Z)));

    /// <summary>Determines the angle between the vectors</summary>
    /// <param name="V1">Vector 1</param>
    /// <param name="V2">Vector 2</param>
    /// <returns>Angle between the vectors</returns>
    public static double Angle(Vector3 V1, Vector3 V2) {
        V1.Normalize();
        V2.Normalize();
        return Math.Acos(DotProduct(V1, V2));
    }
    #endregion Public Static Functions
}