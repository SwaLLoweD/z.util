/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Z.Extensions;

namespace Z.Util;

#pragma warning disable RCS1158
/// <summary>Utility Class for easy-management of long/ulong bitmasks </summary>
[Serializable]
public class Bitmask: Bitmask<short> {
    #region Constructors
    /// <summary>Bitmask Empty constructor (Mask=0)</summary>
    public Bitmask() { }
    /// <summary>Construct Bitmask using specified mask</summary>
    /// <param name="mask">Bitmask as long</param>
    public Bitmask(long mask) : base(mask) { }
    /// <summary>Construct Bitmask using specifed flags or masks (Each one is included using OR)</summary>
    /// <param name="maskOrFlags">
    ///     if enum value is based on long, it is interpreted as mask, otherwise as flag (values are
    ///     included using OR)
    /// </param>
    public Bitmask(params object[] maskOrFlags) : this(GetMask(maskOrFlags)) { }
    #endregion

    /// <summary>Add specified flag or mask</summary>
    public new Bitmask Add(params object[] maskOrFlags) {
        Mask = MaskAdd(Mask, GetMask(maskOrFlags));
        return this;
    }
    /// <summary>Remove specified flag or mask</summary>
    public new Bitmask Remove(params object[] maskOrFlags) {
        Mask = MaskRemove(Mask, GetMask(maskOrFlags));
        return this;
    }

    /// <summary>Parse</summary>
    public new static Bitmask Parse(string s, NumberStyles style = NumberStyles.Any, IFormatProvider? provider = null) =>
        new(provider == null ? long.Parse(s, style) : long.Parse(s, style, provider));
    /// <summary>TryParse</summary>
    public static bool TryParse(string s, [MaybeNullWhen(false)] out Bitmask bitmask) {
        bitmask = null;
        if (!long.TryParse(s, out long mask)) return false;
        bitmask = new Bitmask(mask);
        return true;
    }
    /// <summary>TryParse</summary>
    public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, [MaybeNullWhen(false)] out Bitmask bitmask) {
        bitmask = null;
        if (!long.TryParse(s, style, provider, out long mask)) return false;
        bitmask = new Bitmask(mask);
        return true;
    }

    #region Bitmask implicit Conversions
    /// <summary>Long to Bitmask</summary>
    public static implicit operator Bitmask(long mask) => new(GetMask(mask));
    /// <summary>Bitmask to Long</summary>
    public static implicit operator long(Bitmask bmask) => bmask.Mask;
    /// <summary>Int to Bitmask</summary>
    public static implicit operator Bitmask(int mask) => new(GetMask(mask));
    #endregion Bitmask implicit Conversions
}

/// <summary>Utility Class for easy-management of long/ulong bitmasks (Generic version for enum usage)</summary>
[Serializable]
public class Bitmask<T> {
    #region Fields / Properties
    /// <summary>Real value of current Bitmask as long</summary>
    /// <returns>Current Bitmask</returns>
    public long Mask { get; set; }
    #endregion

    #region Constructors
    /// <summary>Bitmask Empty constructor (Mask=0)</summary>
    public Bitmask() => Mask = 0;

    /// <summary>Construct Bitmask using specifed flags or masks (Each one is included using OR)</summary>
    /// <param name="maskOrFlags">
    ///     if enum value is based on long, it is interpreted as mask, otherwise as flag (values are
    ///     included using OR)
    /// </param>
    public Bitmask(params T[] maskOrFlags) : this(GetMask(maskOrFlags)) { }

    /// <summary>Construct Bitmask using specified mask</summary>
    /// <param name="mask">Bitmask as long</param>
    public Bitmask(long mask) => Mask = mask;

    /// <summary>Construct Bitmask using specified mask</summary>
    /// <param name="mask">Bitmask as long</param>
    public Bitmask(IEnumerable<byte> mask) : this(GetMask(mask)) { }

    /// <summary>Construct Bitmask and set specified flags</summary>
    /// <param name="flag">Flags to set as 1</param>
    public Bitmask(params int[] flag) : this(GetMask(flag)) { }

    /// <summary>Construct Bitmask using specifed masks (Each one is included using OR)</summary>
    /// <param name="mask">Masks to set (as OR)</param>
    public Bitmask(params long[] mask) : this(GetMask(mask)) { }

    /// <summary>Construct Bitmask using specifed flags or masks (Each one is included using OR)</summary>
    /// <param name="maskOrFlags">
    ///     if enum value is based on long, it is interpreted as mask, otherwise as flag (values are
    ///     included using OR)
    /// </param>
    public Bitmask(params Enum[] maskOrFlags) : this(GetMask(maskOrFlags)) { }

    /// <summary>Construct Bitmask using specified mask</summary>
    /// <param name="mask">Bitmask to be included via OR</param>
    public Bitmask(params Bitmask[] mask) : this(GetMask(mask)) { }
    #endregion

    /// <summary>Clear current mask value</summary>
    public void Clear() => Mask = 0;
    /// <summary>String representation of current mask</summary>
    public string ToString(string? format = null, IFormatProvider? provider = null) {
        if (format == null && provider == null) return Mask.ToString();
        if (format == null) return Mask.ToString(provider);
        if (provider == null) return Mask.ToString(format);
        return Mask.ToString(format, provider);
    }
    /// <summary>Byte representation of current mask</summary>
    public byte[] GetBytes() => BitConverter.GetBytes(Mask);
    /// <summary>Parse</summary>
    public static Bitmask<T> Parse(string s, NumberStyles style = NumberStyles.Any, IFormatProvider? provider = null) =>
        new(provider == null ? long.Parse(s, style) : long.Parse(s, style, provider));
    /// <summary>TryParse</summary>
    public static bool TryParse(string s, [MaybeNullWhen(false)] out Bitmask<T> bitmask) {
        bitmask = null;
        if (!long.TryParse(s, out var mask)) return false;
        bitmask = new Bitmask<T>(mask);
        return true;
    }
    /// <summary>TryParse</summary>
    public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, [MaybeNullWhen(false)] out Bitmask<T> bitmask) {
        bitmask = null;
        if (!long.TryParse(s, style, provider, out var mask)) return false;
        bitmask = new Bitmask<T>(mask);
        return true;
    }
    /// <inheritdoc />
    public override int GetHashCode() => Mask.GetHashCode();
    /// <inheritdoc />
    public override bool Equals(object? obj) => obj != null && this == obj;

    #region Bitmask & Bitmask operators
    /// <summary>Bitwise OR between two bitmasks</summary>
    public static Bitmask<T> operator +(Bitmask<T> x, Bitmask<T> y) => new((long)Add(x.Mask, y.Mask));
    // if (x == null && y == null) return rval;
    // if (x == null) {
    //     rval.Mask = y.Mask;
    //     return rval;
    // }
    // if (y == null) {
    //     rval.Mask = x.Mask;
    //     return rval;
    // }
    /// <summary>Bitwise NAND between two bitmasks</summary>
    public static Bitmask<T> operator -(Bitmask<T> x, Bitmask<T> y) => new((long)Remove(x.Mask, y.Mask));
    // {
    //     var rval = new Bitmask<T>();
    //     if (x == null && y == null) return rval;
    //     if (x == null) return rval;
    //     if (y == null) {
    //         rval.Mask = x.Mask;
    //         return rval;
    //     }
    //     rval.Mask = remove(x.Mask, y.Mask);
    //     return rval;
    // }
    /// <summary>Checks if x has all flags set in y</summary>
    public static bool operator <(Bitmask<T> x, Bitmask<T> y) => Has(x.Mask, y.Mask);
    // {
    //     if (x == null && y == null) return true;
    //     if (x == null || y == null) return false;
    //     return has(x.Mask, y.Mask);
    // }

    /// <summary>Checks if x does not have any of the flags set in y</summary>
    public static bool operator >(Bitmask<T> x, Bitmask<T> y) => Has(x.Mask, y.Mask);
    // {
    //     if (x == null && y == null) return true;
    //     if (x == null || y == null) return false;
    //     return notHas(x.Mask, y.Mask);
    // }
    /// <summary>Bitwise AND</summary>
    public static Bitmask<T> operator &(Bitmask<T> x, Bitmask<T> y) => new((long)(x.Mask & y.Mask));
    // {
    //     var rval = new Bitmask<T>();
    //     if (x == null || y == null) return rval;
    //     rval.Mask = x.Mask & y.Mask;
    //     return rval;
    // }
    /// <summary>Shift left</summary>
    public static Bitmask<T> operator <<(Bitmask<T> x, int y) => new((long)(x.Mask <<= y));
    // {
    //     var rval = x ?? new Bitmask<T>(x == null ? 0 : x.Mask);
    //     rval.Mask <<= y;
    //     return x;
    // }
    /// <summary>Shift right</summary>
    public static Bitmask<T> operator >>(Bitmask<T> x, int y) => new((long)(x.Mask >> y));
    // {
    //     var rval = new Bitmask(x == null ? 0 : x.Mask);
    //     rval.Mask = rval.Mask >> y;
    //     return x;
    // }
    /// <summary>Equality</summary>
    public static bool operator ==(Bitmask<T> x, Bitmask<T> y) => x.Mask == y.Mask;
    // {
    //     if (ReferenceEquals(x, y)) return true;
    //     if (Equals(x, null) && Equals(y, null)) return true;
    //     if (Equals(x, null) || Equals(y, null)) return false;
    //     return x.Mask == y.Mask;
    // }
    /// <summary>Inequality</summary>
    public static bool operator !=(Bitmask<T> x, Bitmask<T> y) => !(x == y);
    #endregion Bitmask & Bitmask operators

    #region Bitmask & Object Mask operators
    /// <summary>Bitwise OR</summary>
    public static Bitmask<T> operator +(Bitmask<T> x, object maskOrFlag) {
        var rval = new Bitmask<T>();
        var mask = GetMask(maskOrFlag);
        // if (x == null) {
        //     rval.Mask = mask;
        //     return rval;
        // }
        rval.Mask = Add(x.Mask, mask);
        return rval;
    }
    /// <summary>Bitwise NAND</summary>
    public static Bitmask<T> operator -(Bitmask<T> x, object maskOrFlag) {
        var rval = new Bitmask<T>();
        var mask = GetMask(maskOrFlag);
        // if (x == null) return rval;
        rval.Mask = Remove(x.Mask, mask);
        return rval;
    }
    /// <summary>Checks if x has all flags set in y</summary>
    public static bool operator <(Bitmask<T> x, object maskOrFlag) {
        var mask = GetMask(maskOrFlag);
        // if (x == null) {
        //     if (mask == 0) return true;
        //     return false;
        // }
        return Has(x.Mask, mask);
    }
    /// <summary>Checks if x does not have any of the flags set in y</summary>
    public static bool operator >(Bitmask<T> x, object maskOrFlag) => NotHas(x.Mask, GetMask(maskOrFlag));
    // {
    //     var mask = GetMask(maskOrFlag);
    //     if (x == null) {
    //         if (mask == 0) return false;
    //         return true;
    //     }
    //     return notHas(x.Mask, mask);
    // }
    /// <summary>Bitwise AND</summary>
    public static Bitmask<T> operator &(Bitmask<T> x, object maskOrFlag) => new(x.Mask & GetMask(maskOrFlag));
    // {
    //     var mask = GetMask(maskOrFlag);
    //     var rval = new Bitmask<T>();
    //     // if (x == null) return rval;
    //     rval.Mask = x.Mask & mask;
    //     return rval;
    // }
    /// <summary>Equality</summary>
    public static bool operator ==(Bitmask<T> x, object maskOrFlag) {
        // if (x == null && maskOrFlag == null) return true;
        // if (x == null || maskOrFlag == null) return false;
        if (maskOrFlag.GetType().IsAssignableTo(typeof(Bitmask<T>))) return x.Mask == ((Bitmask<T>)maskOrFlag).Mask;
        return x.Mask == GetMask(maskOrFlag);
    }
    /// <summary>Inequality</summary>
    public static bool operator !=(Bitmask<T> x, object maskOrFlag) => !(x == maskOrFlag);
    #endregion Bitmask & Object Mask operators

    #region Bitmask implicit Conversions
    /// <summary>Long to Bitmask</summary>
    public static implicit operator Bitmask<T>(long mask) => new(GetMask(mask));
    /// <summary>Bitmask to Long</summary>
    public static implicit operator long(Bitmask<T> bmask) => bmask.Mask;
    /// <summary>Int to Bitmask</summary>
    public static implicit operator Bitmask<T>(int mask) => new(GetMask(mask));
    /// <summary>Enum to Bitmask</summary>
    public static implicit operator Bitmask<T>(Enum mask) => new(GetMask(mask));
    // /// <summary>Enum to Bitmask</summary>
    // public static implicit operator Bitmask(Bitmask<T> mask) => new Bitmask(mask == null ? 0 : mask.Mask);
    #endregion Bitmask implicit Conversions

    #region Add / Remove / Has / HasAny / NotHas
    private static long Add(long x, long y) => x | y;
    /// <summary>Add specified flag or mask</summary>
    public static long MaskAdd(long mask, params object[] maskOrFlags) => Add(mask, GetMask(maskOrFlags));
    /// <summary>Add specified flag or mask</summary>
    public virtual Bitmask<T> Add(params object[] maskOrFlags) {
        Mask = Add(Mask, GetMask(maskOrFlags));
        return this;
    }

    private static long Remove(long x, long y) => x & ~y;
    /// <summary>Remove specified flag or mask</summary>
    public static long MaskRemove(long mask, params object[] maskOrFlags) => Remove(mask, GetMask(maskOrFlags));
    /// <summary>Remove specified flag or mask</summary>
    public virtual Bitmask<T> Remove(params object[] maskOrFlags) {
        Mask = Remove(Mask, GetMask(maskOrFlags));
        return this;
    }

    private static bool Has(long x, long y) => (x & y) == y;
    /// <summary>Check if All specified flags or masks exist in the mask</summary>
    public static bool MaskHas(long mask, params object[] maskOrFlags) => Has(mask, GetMask(maskOrFlags));
    /// <summary>Check if All specified flags or masks exist in the mask</summary>
    public virtual bool Has(params object[] maskOrFlags) => Has(Mask, GetMask(maskOrFlags));

    private static bool HasAny(long x, long y) => (x & y) != 0;
    /// <summary>Check if Any specified flags or masks exist in the mask</summary>
    public static bool MaskHasAny(long mask, params object[] maskOrFlags) => HasAny(mask, GetMask(maskOrFlags));
    /// <summary>Check if Any specified flags or masks exist in the mask</summary>
    public virtual bool HasAny(params object[] maskOrFlags) => HasAny(Mask, GetMask(maskOrFlags));

    private static bool NotHas(long x, long y) => (x & y) == 0;
    /// <summary>Check if None of the specified flags or masks exist in the mask</summary>
    public static bool MaskNotHas(long mask, params object[] maskOrFlags) => NotHas(mask, GetMask(maskOrFlags));
    /// <summary>Check if None of the specified flags or masks exist in the mask</summary>
    public virtual bool NotHas(params object[] maskOrFlags) => NotHas(Mask, GetMask(maskOrFlags));
    #endregion Add / Remove / Has / HasAny / NotHas

    #region GetMask & GetBitmask
    private static long PGetMask(int flag) {
        if (flag > 64) throw new Exception("Value of the flag is outside the range of the bitmask");
        var y = (long)1 << flag;
        return y;
    }
    private static long GetMaskOfEnum(Enum maskOrFlag) {
        var t = Enum.GetUnderlyingType(maskOrFlag.GetType());
        if (t == typeof(long) || t == typeof(ulong)) return Convert.ToInt64(maskOrFlag);
        return GetMask(Convert.ToInt32(maskOrFlag));
    }
    private static long GetMaskArray(IEnumerable maskOrFlags) {
        long rval = 0;
        foreach (var maskOrFlag in maskOrFlags) rval |= GetMask(maskOrFlag);
        return rval;
    }
    //private static long GetMaskOfBArray(IEnumerable<byte> mask) => BitConverter.ToInt64(mask.To().Array(), 0);

    /// <summary>Get Mask for object</summary>
    /// <param name="maskOrFlag">if type is long/ulong/string value is evaluated as mask, if other evaluated as flag</param>
    public static long GetMask(object maskOrFlag) {
        if (maskOrFlag == null) return 0;
        var mfType = maskOrFlag.GetType();
        if (mfType.IsAssignableTo<IEnumerable>()) return GetMaskArray((IEnumerable)maskOrFlag);

        if (mfType.Equals<long>()) return (long)maskOrFlag;
        if (mfType.Equals<ulong>()) return Convert.ToInt64((ulong)maskOrFlag);
        if (mfType.Equals<int>()) return PGetMask((int)maskOrFlag);
        if (mfType.Equals<uint>()) return PGetMask(Convert.ToInt32((uint)maskOrFlag));
        if (mfType.Equals<short>()) return PGetMask((short)maskOrFlag);
        if (mfType.Equals<ushort>()) return PGetMask(Convert.ToInt16((ushort)maskOrFlag));
        if (mfType.Equals<byte>()) return PGetMask((byte)maskOrFlag);
        if (mfType.Equals<sbyte>()) return PGetMask(Convert.ToByte((sbyte)maskOrFlag));
        if (mfType.IsAssignableTo<Enum>()) return GetMaskOfEnum((Enum)maskOrFlag);
        //else if (mfType.Is().CastableTo<IEnumerable<byte>>()) return getMaskOfBArray((IEnumerable<byte>)maskOrFlag);
        if (mfType.IsAssignableTo<Bitmask>()) return ((Bitmask)maskOrFlag).Mask;
        if (mfType.IsAssignableTo<string>()) {
            if (long.TryParse((string)maskOrFlag, out long vl)) return vl;
        } else if (mfType.IsAssignableTo<long>()) {
            return (long)maskOrFlag;
        }
        throw new NotSupportedException("Specified parameter(s) is (are) not recognized as flag or mask");
    }
    #endregion GetMask & GetBitmask

    #region FlagsEnabled/Disabled
    /// <summary>Get currently set flags</summary>
    public virtual IEnumerable<T> FlagsEnabled {
        get {
            for (short i = 0; i < 64; i++) {
                var shift = (long)1 << i;
                if ((Mask & shift) == shift) yield return i.To().Type<T>()!;
            }
        }
    }
    /// <summary>Get currently unset flags</summary>
    public virtual IEnumerable<T> FlagsDisabled {
        get {
            for (short i = 0; i < 64; i++) {
                var shift = (long)1 << i;
                if ((Mask & shift) == 0) yield return i.To().Type<T>()!;
            }
        }
    }
    #endregion FlagsEnabled/Disabled
}