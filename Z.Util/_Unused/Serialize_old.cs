﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Securist.Util
{
    public static partial class Fnc
    {
        public static byte[] Serialize(object e)
        {
            if (e == null) return null;
            byte[] rval;
            using (var bs = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(bs, e);
                rval = bs.ToArray();
            }
            return rval;
        }
        public static object Deserialize(byte[] e)
        {
            if (e == null) return null;
            object rval;
            using (var bs = new MemoryStream(e))
            {
                var bf = new BinaryFormatter();
                bs.Position = 0;
                rval = bf.Deserialize(bs);
            }
            return rval;
        }
    }
}

