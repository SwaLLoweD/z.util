﻿/* Copyright (c) <2014> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Z.Extensions
{
    ///USE newer System.Net.Http.HttpClient Instead
    /// <summary>Extension methods for WebRequests</summary>
    public static partial class ExtWebRequest
    {
        /// <summary>
        /// Sending POST request.
        /// </summary>
        /// <param name="req">Request Url.</param>
        /// <param name="data">Data for request.</param>
        /// <param name="output">Response body.</param>
        /// <returns>Post successful or not.</returns>
        public static bool Post(this WebRequest req, string data, out string output)
        {
            var r = new System.Net.Http.HttpClient();
            output = string.Empty;
            if (req == null) return false;
            try {
                req.Method = "POST";
                req.Timeout = 100000;
                req.ContentType = "application/x-www-form-urlencoded";
                byte[] sentData = Encoding.UTF8.GetBytes(data);
                req.ContentLength = sentData.Length;
                using (System.IO.Stream sendStream = req.GetRequestStream()) {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Close();
                }
                System.Net.WebResponse res = req.GetResponse();
                System.IO.Stream ReceiveStream = res.GetResponseStream();
                using (System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8)) {
                    Char[] read = new Char[256];
                    int count = sr.Read(read, 0, 256);

                    while (count > 0) {
                        String str = new String(read, 0, count);
                        output += str;
                        count = sr.Read(read, 0, 256);
                    }
                }
            }
            catch (ArgumentException ex) {
                output = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
                return false;
            }
            catch (WebException ex) {
                output = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
                return false;
            }
            catch (Exception ex) {
                output = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sending GET request.
        /// </summary>
        /// <param name="req">Request Url.</param>
        /// <param name="output">Response body.</param>
        /// <returns>Post successful or not.</returns>
        public static bool Get(this WebRequest req, out string output)
        {
            output = String.Empty;
            if (req == null) return false;
            try {
                System.Net.WebResponse resp = req.GetResponse();
                using (System.IO.Stream stream = resp.GetResponseStream()) {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream)) {
                        output = sr.ReadToEnd();
                        sr.Close();
                    }
                }
            }
            catch (ArgumentException ex) {
                output = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
                return false;
            }
            catch (WebException ex) {
                output = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
                return false;
            }
            catch (Exception ex) {
                output = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
                return false;
            }

            return true;
        }
    }
}