/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Reflection;
using Z.Extensions;
using Z.Util;

namespace Z.Collections.Specialized
{
    

    /// <summary>ByteArray and packet generator</summary>
    [Serializable]
    public class ByteArray : List<byte>
    {
        //Current pointer position
        private int pos = 0;

        #region Constructors
        /// <summary>ByteArray constructor</summary>
        /// <param name="capacity">Initial capacity of the byte[]</param>
        public ByteArray(int capacity = 64) : base(capacity) { initLookupTable(); }
        /// <summary>ByteArray constructor</summary>
        /// <param name="initialArray">Initial byte[]</param>
        public ByteArray(byte[] initialArray) : this(initialArray, 0 /*initialArray.Length*/) { }

        /// <summary>ByteArray constructor</summary>
        /// <param name="initialArray">Initial byte[]</param>
        /// <param name="position">Set pointer position (Default=Array length)</param>
        public ByteArray(byte[] initialArray, int position) : base(initialArray) { pos = position; initLookupTable(); }
        #endregion

        #region Properties
        /// <summary>Current position of the array pointer</summary>
        public int CurrentIndex { get { return pos; } set { pos = value; } }

        /// <summary>Number of elements currently present in the array</summary>
        public int Length
        {
            get { return base.Count; }
            set
            {
                if (value < base.Count) RemoveRange(value, base.Count - value);
                else { while (value > base.Count) { base.Add((byte)0); } }
            }
        }
        #endregion

        #region Methods
        /// <summary>Clear the array</summary>
        public new void Clear()
        {
            pos = 0;
            base.Clear();
        }

        /// <summary>Reset array pointer</summary>
        public void ResetIndex() { pos = 0; }

        /// <summary>Move the pointer</summary>
        /// <param name="len">Amount to move the pointer from current position</param>
        public ByteArray Seek(int len)
        {
            pos += len;
            return this;
        }

        /// <summary>Get byte[] from current byteArray</summary>
        /// <param name="count">Amount of bytes to get from the start</param>
        /// <param name="startIndex">Index to start conversion from (0 = begining of the original array)</param>
        public byte[] ToArray(int count, int startIndex = 0) { return ToArray(startIndex, count); }
        #endregion

        #region Add
        //public ByteArray Insert(int index, byte arg) { return addDirect(ref index, arg); }
        /// <summary>Insert object into array at specified index</summary>
        /// <param name="index">Add to array starting from this index</param>
        /// <param name="args">Objects to add into the array</param>
        public ByteArray Insert(int index, params object[] args) { return add(ref index, args); }

        //public ByteArray Add(byte arg) { return addDirect(ref pos, arg); }
        /// <summary>Add objects to Array using auto pointer</summary>
        /// <param name="args">Objects to add into the array</param>
        public ByteArray Add(params object[] args) { return add(ref pos, args); }

        /// <summary>Add objects to Array using auto pointer (wihtout length)</summary>
        /// <param name="array">Objects to add into the array</param>
        /// <param name="sourceIndex">Starting point to read in the sourceArray</param>
        /// <param name="len">Number of elements to read</param>
        public ByteArray AddBytes(byte[] array, int sourceIndex, int len) { return addDirect(ref pos, array, sourceIndex, len); }

        /// <summary>Add objects to Array using auto pointer (wihtout length)</summary>
        /// <param name="array">Objects to add into the array</param>
        public ByteArray AddBytes(byte[] array) { return addDirect(ref pos, array, 0, array.Length); }
        #endregion Add

        #region Get
        /// <summary>Extract from array</summary>
        /// <param name="index">Staring from this index</param>
        /// <param name="count">Length of the byte[] to extract</param>
        public byte[] GetBytes(ref int index, int count) { return getBytes(ref index, count); }
        /// <summary>Extract from array</summary>
        /// <param name="index">Staring from this index</param>
        /// <typeparam name="T">Type of the object to extract</typeparam>
        public T Get<T>(ref int index) { return get<T>(ref index); }
        /// <summary>Extract from array</summary>
        /// <param name="index">Staring from this index</param>
        /// <param name="objectType">Type of the object to extract</param>
        public object Get(Type objectType, ref int index) { return get(objectType, ref index); }

        /// <summary>Extract from array</summary>
        /// <param name="index">Staring from this index</param>
        /// <param name="count">Length of the byte[] to extract</param>
        public byte[] GetBytes(int index, int count) { return getBytes(ref index, count); }
        /// <summary>Extract from array</summary>
        /// <param name="index">Staring from this index</param>
        /// <typeparam name="T">Type of the object to extract</typeparam>
        public T Get<T>(int index) { return get<T>(ref index); }
        /// <summary>Extract from array</summary>
        /// <param name="index">Staring from this index</param>
        /// <param name="objectType">Type of the object to extract</param>
        public object Get(Type objectType,int index) { return get(objectType, ref index); }

        /// <summary>Extract from array</summary>
        /// <param name="count">Length of the byte[] to extract</param>
        public byte[] GetBytes(int count) { return getBytes(ref pos, count); }
        /// <summary>Extract from array</summary>
        /// <typeparam name="T">Type of the object to extract</typeparam>
        public T Get<T>() { return get<T>(ref pos); }
        /// <summary>Extract from array</summary>
        /// <param name="objectType">Type of the object to extract</param>
        public object Get(Type objectType) { return get(objectType, ref pos); }
        #endregion Get

        

        #region Private Add
        private ByteArray addDirect(ref int index, params byte[] args)
        {
            if (args == null) args = new byte[0];
            if (Length < index + args.Length)
                Length = index + args.Length; // Resize if needed
            foreach (var b in args)
                this[index++] = b;
            return this;
        }

        private ByteArray addDirect(ref int index, byte[] args, int sourceIndex, int len)
        {
            if (args == null || len <= 0) return add(ref index, new byte[0]);
            if (sourceIndex < 0) sourceIndex = 0;
            if (sourceIndex + len > args.Length) len = args.Length - sourceIndex;
            if (Length < index + len)
                Length = index + len; // Resize if needed
            for (int i = 0; i < len; i++)
                this[index++] = args[sourceIndex + i];
            return this;
        }

        private ByteArray add(ref int index, byte[] args, int sourceIndex, int len)
        {
            if (args == null || len <= 0) return add(ref index, new byte[0]);
            if (sourceIndex < 0) sourceIndex = 0;
            if (sourceIndex + len > args.Length) len = args.Length - sourceIndex;
            if (Length < index + len + 4)
                Length = index + len + 4; // Resize if needed
            add(ref index, len);
            for(int i = 0; i<len ; i++)
                this[index++] = args[sourceIndex + i];
            return this;
        }
        private ByteArray add(ref int index, params object[] args)
        {
            if (args == null) { //only null is passed
                add(ref index, 0);
                return this;
            }
            foreach (var arg in args) {
                if (arg == null) {
                    add(ref index, 0); //data length set to 0 (Either serializable, string or byte[])
                    continue;
                }
                var type = arg.GetType();
                var c = GetConverter(type);
                if (c == null) throw new NotSupportedException("Object to be converted to byte[] is not recognized");
                var b = c.Writer(arg);
                if (c.Length == -1) add(ref index, b.Length);
                addDirect(ref index, b);
            }
            return this;
        }
        #endregion Private Add

        #region Private Get
        private byte[] getBytes(ref int index, int len)
        {
            byte[] rval = base.GetRange(index, len).ToArray();
            index += len;
            return rval;
        }
        private T get<T>(ref int index) {
            var rval = get(typeof(T), ref index);
            if (rval == null) return default(T);
            return (T)rval;
        }
        private object get(Type type, ref int index)
        {
            var rval = type.DefaultValue();
            var c = GetConverter(type);
            if (c == null) throw new NotSupportedException("Object to be converted from byte[] is not recognized");
            var len = c.Length;
            if (len == -1) {
                var bLen = getBytes(ref index, 4);
                len = BitConverter.ToInt32(bLen, 0);
            }
            if (len <= 0) return rval;
            var b = getBytes(ref index, len);
            rval = c.Reader(b, type);
            return rval;
        }


        #endregion Private Get

        #region Operators
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, byte y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, bool y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, ushort y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, short y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, uint y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, int y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, ulong y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, long y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, double y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, float y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, Guid y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, DateTime y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, TimeSpan y) { return x.Add(y); }
        /// <summary>Add to array</summary>
        public static ByteArray operator +(ByteArray x, string y) { return x.Add(y); }

        /// <summary>Extract from array</summary>
        public static byte operator -(ByteArray x, byte y) { return x.Get<byte>(); }
        /// <summary>Extract from array</summary>
        public static bool operator -(ByteArray x, bool y) { return x.Get<bool>(); }
        /// <summary>Extract from array</summary>
        public static ushort operator -(ByteArray x, ushort y) { return x.Get<ushort>(); }
        /// <summary>Extract from array</summary>
        public static short operator -(ByteArray x, short y) { return x.Get<short>(); }
        /// <summary>Extract from array</summary>
        public static uint operator -(ByteArray x, uint y) { return x.Get<uint>(); }
        /// <summary>Extract from array</summary>
        public static int operator -(ByteArray x, int y) { return x.Get<int>(); }
        /// <summary>Extract from array</summary>
        public static ulong operator -(ByteArray x, ulong y) { return x.Get<ulong>(); }
        /// <summary>Extract from array</summary>
        public static long operator -(ByteArray x, long y) { return x.Get<long>(); }
        /// <summary>Extract from array</summary>
        public static double operator -(ByteArray x, double y) { return x.Get<double>(); }
        /// <summary>Extract from array</summary>
        public static float operator -(ByteArray x, float y) { return x.Get<float>(); }
        /// <summary>Extract from array</summary>
        public static Guid operator -(ByteArray x, Guid y) { return x.Get<Guid>(); }
        /// <summary>Extract from array</summary>
        public static DateTime operator -(ByteArray x, DateTime y) { return x.Get<DateTime>(); }
        /// <summary>Extract from array</summary>
        public static TimeSpan operator -(ByteArray x, TimeSpan y) { return x.Get<TimeSpan>(); }
        /// <summary>Extract from array</summary>
        public static string operator -(ByteArray x, string y) { return x.Get<string>(); }
        #endregion Operators

        #region Depreciated
        /*public static void ToBytes(ulong a, byte[] b, ref int t)
        {
            b[t++] = (byte)(a & 0xff);
            b[t++] = (byte)((a >> 8) & 0xff);
            b[t++] = (byte)((a >> 0x10) & 0xff);
            b[t++] = (byte)((a >> 0x18) & 0xff);
            b[t++] = (byte)((a >> 0x20) & 0xff);
            b[t++] = (byte)((a >> 40) & 0xff);
            b[t++] = (byte)((a >> 0x30) & 0xff);
            b[t++] = (byte)((a >> 0x38) & 0xff);
        }*/
        #endregion Depreciated
    }
}