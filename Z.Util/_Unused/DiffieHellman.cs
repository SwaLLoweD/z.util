﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Numerics;
using Z.Collections.Specialized;
using Z.Extensions;

namespace Z.Cryptography
{
    /// <summary>Represents the Diffie-Hellman algorithm.</summary>
    public class DiffieHellman : IDisposable
    {
        #region Fields and properties
        /// <summary>The number of bits to generate.</summary>
        public int Bits {get;private set;}

        /// <summary>The shared prime.</summary>
        BigInteger prime;
        /// <summary>The shared base.</summary>
        BigInteger g;
        /// <summary>The private prime.</summary>
        BigInteger mine;

        /// <summary>The final key.</summary>
        byte[] key;
        ///// <summary>The string representation/packet.</summary>
        //string representation;

        /// <summary>Gets the final key to use for encryption.</summary>
        public byte[] Key { get { return key; } }
        #endregion

        #region - Ctor -
        /// <summary>Represents the Diffie-Hellman algorithm.</summary>
        public DiffieHellman(int bits = 256) { this.Bits = bits; }
        /// <summary>Represents the Diffie-Hellman algorithm.</summary>
        ~DiffieHellman() { Dispose(); }
        #endregion

        #region - Implementation Methods -

        /// <summary>Generates a request packet.</summary>
        /// <returns></returns>
        public byte[] GenerateRequest(out byte[] primeBytes, out byte[] gBytes)
        {
            // Generate the parameters.
            prime = BigInteger.Zero.RandomPrime(Bits/8, 30);
            mine = BigInteger.Zero.RandomPrime(Bits/8, 30);
            g = (BigInteger)5;

            // Gemerate the byteArray.
            primeBytes = prime.ToByteArray();
            gBytes = g.ToByteArray();

            // Generate the send BigInt.
            BigInteger send = g.ModPow(mine, prime);
            return send.ToByteArray();
        }
        /// <summary>Generates a request packet.</summary>
        /// <returns></returns>
        public byte[] GenerateCompactRequest()
        {
            byte[] p, g, req;
            req = GenerateRequest(out p, out g);
            var rval = new ByteArray(12 + p.Length + g.Length + req.Length);
            rval.Add(p, g, req);
            return rval.ToArray();
        }

        /// <summary>Agree on a key and generate a response packet.</summary>
        /// <returns></returns>
        public byte[] GenerateResponse(byte[] primeBytes, byte[] gBytes, byte[] givenBytes)
        {
            // Generate the would-be fields.
            BigInteger prime = new BigInteger(primeBytes);
            BigInteger g = new BigInteger(gBytes);
            BigInteger mine = BigInteger.Zero.RandomPrime(Bits / 8, 30);
            // Generate the key.
            BigInteger given = new BigInteger(givenBytes);
            BigInteger key = given.ModPow(mine, prime);
            this.key = key.ToByteArray();

            // Generate the response.
            BigInteger send = g.ModPow(mine, prime);
            return send.ToByteArray();
        }

        /// <summary>Agree on a key and generate a response packet.</summary>
        /// <returns></returns>
        public byte[] GenerateCompactResponse(byte[] request)
        {
            var req = new ByteArray(request);
            var p = req.Get<byte[]>();
            var g = req.Get<byte[]>();
            var r = req.Get<byte[]>();
            return GenerateResponse(p, g, r);
        }

        /// <summary>Generates the key after a response is received.</summary>
        /// <param name="response">The string representation of the response.</param>
        public byte[] HandleResponse(byte[] response)
        {
            // Get the response and modpow it with the stored prime.
            BigInteger given = new BigInteger(response);
            BigInteger key = given.ModPow(mine, prime);
            this.key = key.ToByteArray();
            return this.key;
        }
        #endregion

        #region IDisposable Members
        /// <summary>Ends the calculation. The key will still be available.</summary>
        public void Dispose()
        {
            //if (!Object.ReferenceEquals(prime, null))
            //    prime.Dispose();
            //if (!Object.ReferenceEquals(mine, null))
            //    mine.Dispose();
            //if (!Object.ReferenceEquals(g, null))
            //    g.Dispose();

            //prime = null;
            //mine = null;
            //g = null;

            //representation = null;
            GC.Collect();
            //GC.Collect();
        }

        #endregion
    }
}