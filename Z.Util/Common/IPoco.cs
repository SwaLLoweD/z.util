﻿namespace Z.Util.Common;

/// <summary>Poco Object</summary>
public interface IPoco<T> where T : class, IPoco<T> { }

/// <summary>Poco Database Object</summary>
public interface IPocoDb<T> : IPoco<T> where T : class, IPocoDb<T> { }

/// <summary>NHibernate Poco Object</summary>
public interface IPocoDbNH<T> : IPocoDb<T> where T : class, IPocoDbNH<T> { }

/// <summary>Entity Framework Poco Object</summary>
public interface IPocoDbEF<T> : IPocoDb<T> where T : class, IPocoDbEF<T> { }

/// <summary>MongoDb Poco Object</summary>
public interface IPocoDbMG<T> : IPocoDb<T> where T : class, IPocoDbMG<T> { }

/// <summary>Pure SQL Db Poco Object</summary>
public interface IPocoDbPSql<T> : IPocoDb<T> where T : class, IPocoDbPSql<T> { }
