﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;

namespace Z.Data.Common;

/// <summary>OnError Event Arguments</summary>
public class RepositoryEventArgs : EventArgs
{
    #region Fields / Properties
    /// <summary>Entity Itself</summary>
    public object Entity { get; set; }
    /// <summary>Entity Type</summary>
    public Type EntityType { get; set; }
    /// <summary>Exception causing the error</summary>
    public Exception Exception { get; set; }
    /// <summary>Error Message</summary>
    public string Message { get; set; }
    #endregion

    #region Constructors
    /// <summary>constructor</summary>
    public RepositoryEventArgs(string message, Type entityType, object entity, Exception exp) {
        Message = message;
        EntityType = entityType;
        Entity = entity;
        Exception = exp;
    }
    #endregion
}