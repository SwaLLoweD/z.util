﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;

namespace Z.Data.Common;

/// <summary>Transaction</summary>
public interface ITransactionDb : IDisposable
{
    #region Fields / Properties
    /// <summary>Returns if transaction is currently active</summary>
    bool IsActive { get; }
    /// <summary>Returns if transaction is committed</summary>
    bool WasCommited { get; }
    /// <summary>Returns if transaction is rolled back</summary>
    bool WasRolledBack { get; }
    #endregion

    /// <summary>Begin transaction</summary>
    void Begin(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
    /// <summary>Commit the transaction</summary>
    void Commit();
    /// <summary>Equals</summary>
    bool Equals(object? obj);
    /// <summary>GetHashcode</summary>
    int GetHashCode();
    /// <summary>Rollback the transaction</summary>
    void Rollback();
    /// <summary>ToString</summary>
    string ToString();
}

/// <summary>NHibernate Transaction</summary>
public interface ITransactionNH : ITransactionDb
{
    /// <summary>Enlist command to the transaction</summary>
    void Enlist(IDbCommand command);
}