﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIÞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Z.Util.Common;

namespace Z.Data.Common;

/// <summary>NHibernate Repository Object</summary>
public interface IRepoDbNH : IRepoDb { }

/// <summary>Entity Framework Repository Object</summary>
public interface IRepoDbEF : IRepoDb { }

/// <summary>MongoDb Repository Object</summary>
public interface IRepoDbMG : IRepoDb { }

/// <summary>Pure SQL Db Repository Object</summary>
public interface IRepoDbPSql : IRepoDb { }

/// <summary>Database Repository Object</summary>
public interface IRepoDb : IRepo
{
    #region Fields / Properties
    /// <summary>Databnase Connection</summary>
    IDbConnection ActiveConnection { get; }
    /// <summary>Returns the status of Db Session</summary>
    bool IsOpen { get; }

    /// <summary>Gets current transactional unit</summary>
    ITransactionDb Transaction { get; }
    #endregion

    /// <summary>Clear all entities and queries from session</summary>
    void Clear();
    /// <summary>Commit all changes in the session</summary>
    void Flush();
    /// <summary>Closes a db session</summary>
    void Close();

    /// <summary>Get Methods</summary>
    IRepoDbGetPart<T> Get<T>();

    /// <summary>Check if Entity is mapped</summary>
    bool IsMapped<T>(T entity);
    /// <summary>Check if Entity has different values than the one in db</summary>
    bool IsDirtyEntity<T>(T entity);
    /// <summary>Check if Entity property has different value than the one in db</summary>
    bool IsDirtyProperty<T>(T entity, string propertyName);
    /// <summary>Return db value of the entity property</summary>
    object GetOriginalEntityProperty<T>(T entity, string propertyName);

    /// <summary>Return LinQ Queryable for Entity type</summary>
    /// <typeparam name="T">Entity Type</typeparam>
    IQueryable<T> LinQ<T>(Expression<Func<T, bool>>? where = null);
    /// <summary>Exceute SQL Query (Scalar)</summary>
    /// <param name="SQLquery">SQL Query</param>
    object Execute(string SQLquery);
    /// <summary>Exceute SQL Command (Scalar)</summary>
    object Execute(IDbCommand cmd);
    /// <summary>Creates an IDbCommand</summary>
    IDbCommand CreateCommand();

    /// <summary>Reloads specified entities from db</summary>
    void Refresh<T>(params T[] entities);
    /// <summary>Deletes (or recycles) specified entities from db (and may commit changes without waiting for session flush)</summary>
    void DeleteMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null);
    /// <summary>Deletes (or recycles) specified entities from db (and may commit changes without waiting for session flush)</summary>
    void Delete<T>(T entity, bool commit = true);
    /// <summary>Saves specified new entities into db(and may commit changes without waiting for session flush)</summary>
    ICollection<object> SaveMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null);
    /// <summary>Saves specified new entities into db(and may commit changes without waiting for session flush)</summary>
    ICollection<object> Save<T>(T entity, bool commit = true);
    /// <summary>Updates existing specified entities in db (and may commit changes without waiting for session flush)</summary>
    void UpdateMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null);
    /// <summary>Updates existing specified entities in db (and may commit changes without waiting for session flush)</summary>
    void Update<T>(T entity, bool commit = true);
    /// <summary>
    ///     Saves specified new entities, and updates existing ones into db (and may commit changes without waiting for
    ///     session flush)
    /// </summary>
    void SaveOrUpdateMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null);
    /// <summary>
    ///     Saves specified new entities, and updates existing ones into db (and may commit changes without waiting for
    ///     session flush)
    /// </summary>
    void SaveOrUpdate<T>(T entity, bool commit = true);
    /// <summary>Starts a transaction</summary>
    ITransactionDb BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
}