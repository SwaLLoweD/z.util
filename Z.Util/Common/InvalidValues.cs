﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Z.Data.Common;

/// <summary>Invalid Entity Values</summary>
public class InvalidValue
{
    #region Fields / Properties
    /// <summary>Validated Entity</summary>
    public object Entity { get; set; }
    /// <summary>Validated Entity Type</summary>
    public Type EntityType { get; set; }
    /// <summary>Match Tags</summary>
    public ICollection<object> MatchTags { get; set; }
    /// <summary>Invalid Reason Message</summary>
    public string Message { get; set; }
    /// <summary>Invalid Property Name</summary>
    public string PropertyName { get; set; }
    /// <summary>Invalid Property Path</summary>
    public string PropertyPath { get; set; }
    /// <summary>Invalid Property Value</summary>
    public object Value { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    public InvalidValue(string message, object entity, Type entityType, PropertyInfo property, object value) {
        Message = message;
        Entity = entity;
        EntityType = entityType;
        PropertyName = property.Name;
        PropertyPath = property.DeclaringType?.Name + ": " + property.Name;
        Value = value;
        MatchTags = new List<object>();
    }
    #endregion

    //private void importNHInvalidValue(NHibernate.Validator.Engine.InvalidValue val)
    //{
    //    Entity = val.Entity;
    //    EntityType = val.EntityType;
    //    Message = val.Message;
    //    PropertyName = val.PropertyName;
    //    PropertyPath = val.PropertyPath;
    //    Value = val.Value;
    //    MatchTags = val.MatchTags;
    //}
    ///// <summary>Cast NHibernate InvalidValue to Securiskop InvalidVal</summary>
    //public static implicit operator InvalidVal(NHibernate.Validator.Engine.InvalidValue val)
    //{
    //    InvalidVal rval = new InvalidVal(val);
    //    return rval;
    //}
    ///// <summary>Convert NHibernate InvalidValues to Securiskop InvalidVal</summary>
    //public static List<InvalidVal> ImportInvalidValues(NHibernate.Validator.Engine.InvalidValue[] val)
    //{
    //    if (val == null) return null;
    //    var rval = new List<InvalidVal>();
    //    for (int i = 0; i < val.Length; i++) {
    //        rval.Add(val[i]);
    //    }
    //    return rval;
    //}
}