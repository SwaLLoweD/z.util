﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;

namespace Z.Data.Common;

/// <summary>Get Queries for Repo (Pure SQL)</summary>
public interface IRepoDbGetPartPSql<T> : IRepoDbGetPart<T> { }

/// <summary>Get Queries for Repo (NHibernate)</summary>
public interface IRepoDbGetPartNH<T> : IRepoDbGetPart<T> { }

/// <summary>Get Queries for Repo</summary>
public interface IRepoDbGetPart<T>
{
    #region Fields / Properties
    /// <summary>Repository the query originated from</summary>
    IRepoDb Repository { get; set; }
    #endregion

    /// <summary>Get All Entities recorded in db of specific type</summary>
    IList<T> All();
    /// <summary>Return SQL Query result</summary>
    /// <param name="SQLquery">SQL Query</param>
    /// <param name="parameters">Command Parameters</param>
    IList<T> All(string SQLquery, params object[] parameters);
    /// <summary>Return SQL Query result</summary>
    /// <param name="cmd">Command to execute</param>
    IList<T> All(IDbCommand cmd);

    /// <summary>Get Paged Entities recorded in db of specific type</summary>
    IList<T> Paged(int limit, int? offset = null);

    /// <summary>Return SQL Query result</summary>
    /// <param name="SQLquery">SQL Query</param>
    /// <param name="limit"></param>
    /// <param name="offset"></param>
    /// <param name="parameters">Command Parameters</param>
    IList<T> Paged(string SQLquery, int limit, int? offset = null, params object[] parameters);

    /// <summary>Return SQL Query result</summary>
    /// <param name="cmd">Command to execute</param>
    /// <param name="limit"></param>
    /// <param name="offset"></param>
    IList<T> Paged(IDbCommand cmd, int limit, int? offset = null);

    /// <summary>Get Entity by matching values from another instance</summary>
    /// <param name="exampleInstance">Example Instance</param>
    /// <param name="excludeNulls">Null values will not try to be matched</param>
    /// <param name="excludeZeros">Zero values will not try to be matched</param>
    /// <param name="propertiesToExclude">Specified properties will be skipped</param>
    IList<T> ByExample(T exampleInstance, bool excludeNulls = true, bool excludeZeros = false, params string[] propertiesToExclude);

    /// <summary>Get Entity by Id</summary>
    /// <param name="id">Entity Id</param>
    /// <param name="skipData">Do not load data</param>
    T ById<U>(U id, bool skipData = false);

    /// <summary>Return entity list matching specified property-value combination</summary>
    /// <param name="property">Entity property to be matched</param>
    /// <param name="values">Property value to be matched</param>
    IList<T> ByProperty<U>(PropertyInfo property, params U[] values);

    /// <summary>Get Entity by specific property values</summary>
    /// <param name="prop">Entity property to search</param>
    /// <param name="values">Entity property value to search</param>
    /// <typeparam name="U">Property Return Type</typeparam>
    IList<T> ByProperty<U>(Expression<Func<T, U>> prop, params U[] values);
}