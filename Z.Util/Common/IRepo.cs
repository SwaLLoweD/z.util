﻿using System;

namespace Z.Util.Common;

/// <summary>Repository Object</summary>
public interface IRepo : IDisposable { }
