﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Z.Extensions;
#pragma warning disable RCS1194
/// <summary>Generic exception for combining several other exceptions</summary>
[Serializable]
public class CombinedException: Exception {
    #region Fields / Properties
    /// <summary>Gets the inner exceptions.</summary>
    /// <value>The inner exceptions.</value>
    public ICollection<Exception> InnerExceptions { get; protected set; }
    #endregion

    #region Constructors
    /// <summary>Initializes a new instance of the <see cref="CombinedException" /> class.</summary>
    /// <param name="message">The message.</param>
    /// <param name="innerExceptions">The inner exceptions.</param>
    public CombinedException(string message, params Exception[] innerExceptions)
        : base(message) =>
        InnerExceptions = innerExceptions ?? Array.Empty<Exception>();
    #endregion
}