﻿namespace Z.Extensions
{
    /// <summary>Generic value holder for extendable extensions</summary>
    public class ExtendablePortion<T>
    {
        #region Fields / Properties
        /// <summary>Multi-method library value holder other extensions.</summary>
        public T Value { get; set; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ExtendablePortion(T value) => Value = value;
        #endregion
    }

    /// <summary>Conversion extendable</summary>
    public interface IConvertPortion<T>
    {
        #region Fields / Properties
        /// <summary>Multi-method library value holder other extensions.</summary>
        T Value { get; }
        #endregion
    }

    /// <summary>Is check extendable</summary>
    public interface IIsPortion<T>
    {
        #region Fields / Properties
        /// <summary>Multi-method library value holder other extensions.</summary>
        T Value { get; }
        #endregion
    }
}