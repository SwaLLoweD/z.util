﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for the FileInfo class</summary>
public static class ExtFileInfo
{
    /// <summary>Returns size in human readable format.</summary>
    /// <param name="file">The file.</param>
    /// <returns>size in human readable format</returns>
    public static string Size(this FileInfo file) => PrettySize(file.Length);

    /// <summary>Returns file size in human readable format.</summary>
    /// <param name="size">The size.</param>
    /// <returns>size in human readable format</returns>
    public static string PrettySize(long size) {
        var dblFileSize = size;
        try {
            if (dblFileSize < 1024) return string.Format("{0:N0} B", dblFileSize); //<1KB
            if (dblFileSize < 1048576) return string.Format("{0:N2} KB", dblFileSize / 1024); //1KB in bytes //<1MB
            if (dblFileSize < 1073741824) return string.Format("{0:N2} MB", dblFileSize / 1048576); //1MB in bytes //<1GB
            if (dblFileSize < 1099511627776) return string.Format("{0:N2} GB", dblFileSize / 1073741824); //1GB in bytes //<1TB
            return string.Format("{0:N2} TB", dblFileSize / 1099511627776); //1TB in bytes //<1TeB
        } catch {
            return "N/A";
        }
    }

    /// <summary>Renames a file.</summary>
    /// <param name="file">The file.</param>
    /// <param name="newName">The new name.</param>
    /// <returns>The renamed file</returns>
    /// <example>
    ///     <code>
    /// 		var file = new FileInfo(@"c:\test.txt");
    /// 		file.Rename("test2.txt");
    /// 	</code>
    /// </example>
    public static FileInfo Rename(this FileInfo file, string newName) {
        if (file?.Exists != true) throw new FileNotFoundException("File note found", file?.FullName);
        var filePath = Path.Combine(Path.GetDirectoryName(file.FullName)!, newName);
        file.MoveTo(filePath);
        return file;
    }

    /// <summary>Renames a without changing its extension.</summary>
    /// <param name="file">The file.</param>
    /// <param name="newName">The new name.</param>
    /// <returns>The renamed file</returns>
    /// <example>
    ///     <code>
    /// 		var file = new FileInfo(@"c:\test.txt");
    /// 		file.RenameFileWithoutExtension("test3");
    /// 	</code>
    /// </example>
    public static FileInfo RenameFileWithoutExtension(this FileInfo file, string newName) {
        if (file?.Exists != true) throw new FileNotFoundException("File note found", file?.FullName);
        var fileName = string.Concat(newName, file.Extension);
        file.Rename(fileName);
        return file;
    }

    /// <summary>Changes the files extension.</summary>
    /// <param name="file">The file.</param>
    /// <param name="newExtension">The new extension.</param>
    /// <returns>The renamed file</returns>
    /// <example>
    ///     <code>
    /// 		var file = new FileInfo(@"c:\test.txt");
    /// 		file.ChangeExtension("xml");
    /// 	</code>
    /// </example>
    public static FileInfo ChangeExtension(this FileInfo file, string newExtension) {
        if (file?.Exists != true) throw new FileNotFoundException("File note found", file?.FullName);
        newExtension = newExtension.EnsureStartsWith(".");
        var fileName = string.Concat(Path.GetFileNameWithoutExtension(file.FullName), newExtension);
        file.Rename(fileName);
        return file;
    }

    /// <summary>Changes the extensions of several files at once.</summary>
    /// <param name="files">The files.</param>
    /// <param name="newExtension">The new extension.</param>
    /// <returns>The renamed files</returns>
    /// <example>
    ///     <code>
    /// 		var files = directory.GetFiles("*.txt", "*.xml");
    /// 		files.ChangeExtensions("tmp");
    /// 	</code>
    /// </example>
    public static IEnumerable<FileInfo> ChangeExtensions(this IEnumerable<FileInfo> files, string newExtension) {
        files.ForEach(f => f.ChangeExtension(newExtension));
        return files;
    }

    /// <summary>Deletes several files at once and optionally consolidates any exceptions.</summary>
    /// <param name="files">The files.</param>
    /// <param name="consolidateExceptions">
    ///     if set to <c>true</c> exceptions are consolidated and the processing is not
    ///     interrupted.
    /// </param>
    /// <example>
    ///     <code>
    /// 		var files = directory.GetFiles("*.txt", "*.xml");
    /// 		files.Delete()
    /// 	</code>
    /// </example>
    public static void Delete(this IEnumerable<FileInfo> files, bool consolidateExceptions = true) {
        List<Exception>? exceptions = null;

        foreach (var file in files) {
            try {
                file.Delete();
            } catch (Exception e) when (consolidateExceptions) {
                (exceptions ??= new List<Exception>()).Add(e);
            }
        }

        if (exceptions?.Count > 0) throw new CombinedException("Error while deleting one or several files, see InnerExceptions array for details.", exceptions.ToArray());
    }

    /// <summary>Copies several files to a new folder at once and optionally consolidates any exceptions.</summary>
    /// <param name="files">The files.</param>
    /// <param name="targetPath">The target path.</param>
    /// <param name="consolidateExceptions">
    ///     if set to <c>true</c> exceptions are consolidated and the processing is not
    ///     interrupted.
    /// </param>
    /// <returns>The newly created file copies</returns>
    /// <example>
    ///     <code>
    /// 		var files = directory.GetFiles("*.txt", "*.xml");
    /// 		var copiedFiles = files.CopyTo(@"c:\temp\");
    /// 	</code>
    /// </example>
    public static IEnumerable<FileInfo> CopyTo(this IEnumerable<FileInfo> files, string targetPath, bool consolidateExceptions = true) {
        var copiedfiles = new List<FileInfo>();
        List<Exception>? exceptions = null;

        foreach (var file in files) {
            try {
                var fileName = Path.Combine(targetPath, file.Name);
                copiedfiles.Add(file.CopyTo(fileName));
            } catch (Exception e) when (consolidateExceptions) {
                (exceptions ??= new List<Exception>()).Add(e);
            }
        }

        if (exceptions?.Count > 0) throw new CombinedException("Error while copying one or several files, see InnerExceptions array for details.", exceptions.ToArray());

        return copiedfiles.ToArray();
    }

    /// <summary>Movies several files to a new folder at once and optionally consolidates any exceptions.</summary>
    /// <param name="files">The files.</param>
    /// <param name="targetPath">The target path.</param>
    /// <param name="consolidateExceptions">
    ///     if set to <c>true</c> exceptions are consolidated and the processing is not
    ///     interrupted.
    /// </param>
    /// <returns>The moved files</returns>
    /// <example>
    ///     <code>
    /// 		var files = directory.GetFiles("*.txt", "*.xml");
    /// 		files.MoveTo(@"c:\temp\");
    /// 	</code>
    /// </example>
    public static IEnumerable<FileInfo> MoveTo(this IEnumerable<FileInfo> files, string targetPath, bool consolidateExceptions = true) {
        List<Exception>? exceptions = null;

        foreach (var file in files) {
            try {
                var fileName = Path.Combine(targetPath, file.Name);
                file.MoveTo(fileName);
            } catch (Exception e) when (consolidateExceptions) {
                (exceptions ??= new List<Exception>()).Add(e);
            }
        }

        if (exceptions?.Count > 0) throw new CombinedException("Error while moving one or several files, see InnerExceptions array for details.", exceptions.ToArray());

        return files;
    }

    /// <summary>Sets file attributes for several files at once</summary>
    /// <param name="files">The files.</param>
    /// <param name="attributes">The attributes to be set.</param>
    /// <returns>The changed files</returns>
    /// <example>
    ///     <code>
    /// 		var files = directory.GetFiles("*.txt", "*.xml");
    /// 		files.SetAttributes(FileAttributes.Archive);
    /// 	</code>
    /// </example>
    public static IEnumerable<FileInfo> SetAttributes(this IEnumerable<FileInfo> files, FileAttributes attributes) {
        foreach (var file in files) file.Attributes = attributes;
        return files;
    }

    /// <summary>Appends file attributes for several files at once (additive to any existing attributes)</summary>
    /// <param name="files">The files.</param>
    /// <param name="attributes">The attributes to be set.</param>
    /// <returns>The changed files</returns>
    /// <example>
    ///     <code>
    /// 		var files = directory.GetFiles("*.txt", "*.xml");
    /// 		files.SetAttributesAdditive(FileAttributes.Archive);
    /// 	</code>
    /// </example>
    public static IEnumerable<FileInfo> SetAttributesAdditive(this IEnumerable<FileInfo> files, FileAttributes attributes) {
        foreach (var file in files) file.Attributes |= attributes;
        return files;
    }

    #region CompareTo
    /// <summary>Compares two files against one another</summary>
    /// <param name="File1">First file</param>
    /// <param name="File2">Second file</param>
    /// <returns>True if the content is the same, false otherwise</returns>
    public static bool CompareTo(this FileInfo File1, FileInfo File2) {
        if (File1?.Exists != true) throw new ArgumentNullException(nameof(File1));
        if (File2?.Exists != true) throw new ArgumentNullException(nameof(File2));
        if (File1.Length != File2.Length) return false;
        return File1.Read().Equals(File2.Read());
    }
    #endregion CompareTo

    #region DriveInfo
    /// <summary>Gets the drive information for a file</summary>
    /// <param name="File">The file to get the drive info of</param>
    /// <returns>The drive info connected to the file</returns>
    public static DriveInfo? DriveInfo(this FileInfo File) => File?.Directory?.DriveInfo();
    #endregion DriveInfo

    #region Read
    /// <summary>Reads a file to the end as a string</summary>
    /// <param name="File">File to read</param>
    /// <returns>A string containing the contents of the file</returns>
    public static string Read(this FileInfo File) {
        if (File?.Exists != true) throw new FileNotFoundException("File note found", File?.FullName);
        using var Reader = File.OpenText();
        var Contents = Reader.ReadToEnd();
        return Contents;
    }
    #endregion Read

    #region ReadBinary
    /// <summary>Reads a file to the end and returns a binary array</summary>
    /// <param name="File">File to open</param>
    /// <returns>A binary array containing the contents of the file</returns>
    public static Span<byte> ReadBinary(this FileInfo File) {
        if (File?.Exists != true) throw new FileNotFoundException("File note found", File?.FullName);
        using var Reader = File.OpenRead();
        var Output = Reader.ReadAllBytes();
        return Output;
    }
    #endregion ReadBinary

    #region SetAttributes
    /// <summary>Sets the attributes of a file</summary>
    /// <param name="file">File</param>
    /// <param name="Attributes">Attributes to set</param>
    /// <returns>The file info</returns>
    public static FileInfo SetAttributes(this FileInfo file, FileAttributes Attributes) {
        if (file?.Exists != true) throw new FileNotFoundException("File note found", file?.FullName);
        File.SetAttributes(file.FullName, Attributes);
        return file;
    }
    #endregion SetAttributes

    #region Execute
    /// <summary>Executes the file</summary>
    /// <param name="file">File to execute</param>
    /// <param name="arguments">Arguments sent to the executable</param>
    /// <param name="domain">Domain of the user</param>
    /// <param name="user">User to run the file as</param>
    /// <param name="password">Password of the user</param>
    /// <param name="windowStyle">Window style</param>
    /// <param name="workingDirectory">Working directory</param>
    /// <returns>The process object created when the executable is started</returns>
    public static Process? Execute(this FileInfo file, string arguments = "",
        string domain = "", string user = "", string password = "",
        ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal, string workingDirectory = "") {
        if (file?.Exists != true) throw new FileNotFoundException("File note found", file?.FullName);
        var Info = new ProcessStartInfo {
            Arguments = arguments
        };
        if (OperatingSystem.IsWindows()) {
            Info.Domain = domain;
            Info.Password = new SecureString();
            foreach (var Char in password) Info.Password.AppendChar(Char);
        }
        Info.UserName = user;
        Info.WindowStyle = windowStyle;
        Info.UseShellExecute = false;
        Info.WorkingDirectory = string.IsNullOrEmpty(workingDirectory) ? file.DirectoryName : workingDirectory;
        return file.Execute(Info);
    }

    /// <summary>Executes the file</summary>
    /// <param name="file">File to execute</param>
    /// <param name="Info">Info used to execute the file</param>
    /// <returns>The process object created when the executable is started</returns>
    public static Process? Execute(this FileInfo file, ProcessStartInfo Info) {
        if (file?.Exists != true) throw new FileNotFoundException("File note found", file?.FullName);
        //Info.ThrowIfNull("Info");
        Info.FileName = file.FullName;
        return Process.Start(Info);
    }
    #endregion Execute

    #region Save
    /// <summary>Saves a string to a file</summary>
    /// <param name="file">File to save to</param>
    /// <param name="Content">Content to save to the file</param>
    /// <param name="Mode">Mode for saving the file (defaults to Create)</param>
    /// <param name="EncodingUsing">Encoding that the content is using (defaults to ASCII)</param>
    /// <returns>The FileInfo object</returns>
    public static FileInfo Save(this FileInfo file, string Content, FileMode Mode = FileMode.Create, Encoding? EncodingUsing = null) =>
        file.Save((EncodingUsing ?? new ASCIIEncoding()).GetBytes(Content), Mode);

    /// <summary>Saves a byte array to a file</summary>
    /// <param name="file">File to save to</param>
    /// <param name="Content">Content to save to the file</param>
    /// <param name="Mode">Mode for saving the file (defaults to Create)</param>
    /// <returns>The FileInfo object</returns>
    public static FileInfo Save(this FileInfo file, byte[] Content, FileMode Mode = FileMode.Create) {
        //File.ThrowIfNull("File");
        if (file?.Exists != true) throw new FileNotFoundException("File note found", file?.FullName);
        new DirectoryInfo(file.DirectoryName ?? "").Create();
        using (var Writer = file.Open(Mode, FileAccess.Write)) {
            Writer.Write(Content, 0, Content.Length);
            Writer.Flush();
        }
        return file;
    }
    #endregion Save

    #region SaveAsync
    /// <summary>Saves a string to a file (asynchronously)</summary>
    /// <param name="file">File to save to</param>
    /// <param name="content">Content to save to the file</param>
    /// <param name="mode">Mode for saving the file (defaults to Create)</param>
    /// <param name="EncodingUsing">Encoding that the content is using (defaults to ASCII)</param>
    /// <returns>The FileInfo object</returns>
    public static async Task<FileInfo> SaveAsync(this FileInfo file, string content, FileMode mode = FileMode.Create, Encoding? EncodingUsing = null) =>
        await file.SaveAsync((EncodingUsing ?? new ASCIIEncoding()).GetBytes(content), mode);

    /// <summary>Saves a byte array to a file (asynchronously)</summary>
    /// <param name="File">File to save to</param>
    /// <param name="Content">Content to save to the file</param>
    /// <param name="Mode">Mode for saving the file (defaults to Create)</param>
    /// <returns>The FileInfo object</returns>
    public static async Task<FileInfo> SaveAsync(this FileInfo File, byte[] Content, FileMode Mode = FileMode.Create) {
        //File.ThrowIfNull("File");
        new DirectoryInfo(File.DirectoryName ?? "").Create();
        using (var Writer = File.Open(Mode, FileAccess.Write)) {
            await Writer.WriteAsync(Content);
            await Writer.FlushAsync();
        }
        return File;
    }
    #endregion SaveAsync
}