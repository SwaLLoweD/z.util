﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;

namespace Z.Extensions;

/// <summary>Extension methods for all comparable objects eg. string, DateTime, numeric values ...</summary>
public static class ExtIComparable
{
    /// <summary>
    ///     Determines whether the specified value is between the defined minimum and maximum range (including those
    ///     values).
    /// </summary>
    /// <param name="Value">Value</param>
    /// <param name="minValue">The minimum value.</param>
    /// <param name="maxValue">The maximum value.</param>
    /// <param name="comparer">An optional comparer to be used instead of the types default comparer.</param>
    /// <returns><c>true</c> if the specified value is between min and max; otherwise, <c>false</c>.</returns>
    /// <example>var value = 5; if(value.IsBetween(1, 10)) { // ... }</example>
    public static bool Between<T>(this T Value, T minValue, T maxValue, IComparer<T>? comparer = null) where T : IComparable {
        comparer ??= Comparer<T>.Default;
        var minMaxCompare = comparer.Compare(minValue, maxValue);
        if (minMaxCompare < 0) return comparer.Compare(Value, minValue) >= 0 && comparer.Compare(Value, maxValue) <= 0;
        if (minMaxCompare == 0) return comparer.Compare(Value, minValue) == 0;
        return comparer.Compare(Value, maxValue) >= 0 && comparer.Compare(Value, minValue) <= 0;
    }

    /// <summary>Clamps a value between two values</summary>
    /// <param name="Value">Value</param>
    /// <param name="Max">Max value it can be (inclusive)</param>
    /// <param name="Min">Min value it can be (inclusive)</param>
    /// <param name="Comparer">Comparer to use (defaults to GenericComparer)</param>
    /// <returns>The value set between Min and Max</returns>
    public static T Clamp<T>(this T Value, T Max, T Min, IComparer<T>? Comparer = null) where T : IComparable {
        Comparer ??= Comparer<T>.Default;
        if (Comparer.Compare(Max, Value) < 0) return Max;
        if (Comparer.Compare(Value, Min) < 0) return Min;
        return Value;
    }

    /// <summary>Returns the maximum value between the two</summary>
    /// <param name="Value">Value</param>
    /// <param name="InputB">Input B</param>
    /// <param name="Comparer">Comparer to use (defaults to GenericComparer)</param>
    /// <returns>The maximum value</returns>
    public static T Max<T>(this T Value, T InputB, IComparer<T>? Comparer = null) where T : IComparable {
        Comparer ??= Comparer<T>.Default;
        return Comparer.Compare(Value, InputB) < 0 ? InputB : Value;
    }

    /// <summary>Returns the minimum value between the two</summary>
    /// <param name="Value">Value</param>
    /// <param name="InputB">Input B</param>
    /// <param name="Comparer">Comparer to use (defaults to GenericComparer)</param>
    /// <returns>The minimum value</returns>
    public static T Min<T>(this T Value, T InputB, IComparer<T>? Comparer = null) where T : IComparable {
        Comparer ??= Comparer<T>.Default;
        return Comparer.Compare(Value, InputB) > 0 ? InputB : Value;
    }

    #region Disabled Extensions
    /*
    // todo: xml documentation is required
    public class DescendingComparer<T> : IComparer<T> where T : IComparable<T>
    {
        public int Compare(T x, T y)
        {
            return y.CompareTo(x);
        }
    }

    public class AscendingComparer<T> : IComparer<T> where T : IComparable<T>
    {
        public int Compare(T x, T y)
        {
            return x.CompareTo(y);
        }
    }*/
    #endregion Disabled Extensions
}