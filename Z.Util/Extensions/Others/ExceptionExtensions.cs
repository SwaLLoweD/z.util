﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Z.Extensions;

/// <summary>Extension methods for System.Exception class</summary>
public static class ExtException
{
    /// <summary>Gets the original exception which is most inner exception.</summary>
    /// <param name="exception">The exeption</param>
    /// <returns>The original exception</returns>
    /// <remarks>Contributed by Kenneth Scott</remarks>
    public static Exception GetOriginalException(this Exception exception) => exception.InnerException == null ? exception : exception.InnerException.GetOriginalException();

    /// <summary>Gets all the error messages</summary>
    /// <param name="exception">The exception</param>
    /// <returns>IEnumerable of message</returns>
    /// <remarks>Contributed by Michael T, http://about.me/MichaelTran</remarks>
    /// <note>The most inner exception message is first in the list, and the most outer exception message is last in the list</note>
    public static IEnumerable<string> Messages(this Exception exception) =>
        exception?.InnerException != null ? new List<string>(exception.InnerException.Messages()) { exception.Message } : Enumerable.Empty<string>();

    /// <summary>Gets all the errors</summary>
    /// <param name="exception">The exception</param>
    /// <returns>IEnumerable of message</returns>
    /// <remarks>Contributed by Michael T, http://about.me/MichaelTran</remarks>
    /// <note>The most inner exception is first in the list, and the most outer exception is last in the list</note>
    public static IEnumerable<Exception> Exceptions(this Exception exception) =>
        exception?.InnerException != null ? new List<Exception>(exception.InnerException.Exceptions()) { exception } : Enumerable.Empty<Exception>();
}