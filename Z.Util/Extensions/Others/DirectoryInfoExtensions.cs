﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for the DirectoryInfo class</summary>
public static class ExtDirectoryInfo
{
    /// <summary>Returns total size</summary>
    /// <param name="directory">Directory.</param>
    /// <returns>size of files and subdirectories</returns>
    public static long Length(this DirectoryInfo directory) {
        try {
            long dblDirSize = 0;
            var objDirInfo = directory;
            Array arrChildFiles = objDirInfo.GetFiles();
            Array arrSubFolders = objDirInfo.GetDirectories();
            foreach (FileInfo objChildFile in arrChildFiles) dblDirSize += objChildFile.Length;
            foreach (DirectoryInfo objSubFolder in arrSubFolders) dblDirSize += objSubFolder.Length();
            return dblDirSize;
        } catch {
            return 0;
        }
    }

    /// <summary>Returns total size in human readable format.</summary>
    /// <param name="directory">Directory.</param>
    /// <returns>size in human readable format</returns>
    public static string Size(this DirectoryInfo directory) => ExtFileInfo.PrettySize(directory.Length());

    /// <summary>
    ///     Gets all files in the directory matching one of the several (!) supplied patterns (instead of just one in the
    ///     regular implementation).
    /// </summary>
    /// <param name="directory">The directory.</param>
    /// <param name="patterns">The patterns.</param>
    /// <returns>The matching files</returns>
    /// <remarks>
    ///     This methods is quite perfect to be used in conjunction with the newly created FileInfo-Array extension
    ///     methods.
    /// </remarks>
    /// <example>
    ///     <code>
    /// 		var files = directory.GetFiles("*.txt", "*.xml");
    /// 	</code>
    /// </example>
    public static FileInfo[] GetFiles(this DirectoryInfo directory, params string[] patterns) {
        var files = new List<FileInfo>();
        foreach (var pattern in patterns) files.Add(directory.GetFiles(pattern));
        return files.ToArray();
    }

    /// <summary>Searches the provided directory recursively and returns the first file matching the provided pattern.</summary>
    /// <param name="directory">The directory.</param>
    /// <param name="pattern">The pattern.</param>
    /// <returns>The found file</returns>
    /// <example>
    ///     <code>
    /// 		var directory = new DirectoryInfo(@"c:\");
    /// 		var file = directory.FindFileRecursive("win.ini");
    /// 	</code>
    /// </example>
    public static FileInfo? FindFileRecursive(this DirectoryInfo directory, string pattern) {
        var files = directory.GetFiles(pattern);
        if (files.Length > 0) return files[0];

        foreach (var subDirectory in directory.GetDirectories()) {
            var foundFile = subDirectory.FindFileRecursive(pattern);
            if (foundFile != null) return foundFile;
        }
        return null;
    }

    /// <summary>Searches the provided directory recursively and returns the first file matching to the provided predicate.</summary>
    /// <param name="directory">The directory.</param>
    /// <param name="predicate">The predicate.</param>
    /// <returns>The found file</returns>
    /// <example>
    ///     <code>
    /// 		var directory = new DirectoryInfo(@"c:\");
    /// 		var file = directory.FindFileRecursive(f => f.Extension == ".ini");
    /// 	</code>
    /// </example>
    public static FileInfo? FindFileRecursive(this DirectoryInfo directory, Func<FileInfo, bool> predicate) {
        foreach (var file in directory.GetFiles()) {
            if (predicate(file))
                return file;
        }

        foreach (var subDirectory in directory.GetDirectories()) {
            var foundFile = subDirectory.FindFileRecursive(predicate);
            if (foundFile != null) return foundFile;
        }
        return null;
    }

    /// <summary>Searches the provided directory recursively and returns the all files matching the provided pattern.</summary>
    /// <param name="directory">The directory.</param>
    /// <param name="pattern">The pattern.</param>
    /// <remarks>
    ///     This methods is quite perfect to be used in conjunction with the newly created FileInfo-Array extension
    ///     methods.
    /// </remarks>
    /// <returns>The found files</returns>
    /// <example>
    ///     <code>
    /// 		var directory = new DirectoryInfo(@"c:\");
    /// 		var files = directory.FindFilesRecursive("*.ini");
    /// 	</code>
    /// </example>
    public static FileInfo[] FindFilesRecursive(this DirectoryInfo directory, string pattern) {
        var foundFiles = new List<FileInfo>();
        FindFilesRecursive(directory, pattern, foundFiles);
        return foundFiles.ToArray();
    }

    private static void FindFilesRecursive(DirectoryInfo directory, string pattern, List<FileInfo> foundFiles) {
        foundFiles.Add(directory.GetFiles(pattern));
        directory.GetDirectories().ForEach(d => FindFilesRecursive(d, pattern, foundFiles));
    }

    /// <summary>Searches the provided directory recursively and returns the all files matching to the provided predicate.</summary>
    /// <param name="directory">The directory.</param>
    /// <param name="predicate">The predicate.</param>
    /// <returns>The found files</returns>
    /// <remarks>
    ///     This methods is quite perfect to be used in conjunction with the newly created FileInfo-Array extension
    ///     methods.
    /// </remarks>
    /// <example>
    ///     <code>
    /// 		var directory = new DirectoryInfo(@"c:\");
    /// 		var files = directory.FindFilesRecursive(f => f.Extension == ".ini");
    /// 	</code>
    /// </example>
    public static FileInfo[] FindFilesRecursive(this DirectoryInfo directory, Func<FileInfo, bool> predicate) {
        var foundFiles = new List<FileInfo>();
        FindFilesRecursive(directory, predicate, foundFiles);
        return foundFiles.ToArray();
    }

    private static void FindFilesRecursive(DirectoryInfo directory, Func<FileInfo, bool> predicate, List<FileInfo> foundFiles) {
        foundFiles.Add(directory.GetFiles().Where(predicate));
        directory.GetDirectories().ForEach(d => FindFilesRecursive(d, predicate, foundFiles));
    }

    #region CopyTo
    /// <summary>Copies a directory to another location</summary>
    /// <param name="Source">Source directory</param>
    /// <param name="Destination">Destination directory</param>
    /// <param name="Recursive">Should the copy be recursive</param>
    /// <param name="Options">Options used in copying</param>
    /// <returns>The DirectoryInfo for the destination info</returns>
    public static DirectoryInfo CopyTo(this DirectoryInfo Source, string Destination, bool Recursive = true, OverwriteOptions Options = OverwriteOptions.Always) {
        //Source.ThrowIfNull("Source");
        //Source.ThrowIfNot(x => x.Exists, new DirectoryNotFoundException("Source directory " + Source.FullName + " not found."));
        //Destination.ThrowIfNullOrEmpty("Destination");
        var DestinationInfo = new DirectoryInfo(Destination);
        DestinationInfo.Create();
        foreach (var TempFile in Source.EnumerateFiles()) {
            switch (Options) {
                case OverwriteOptions.Always:
                default:
                    TempFile.CopyTo(Path.Combine(DestinationInfo.FullName, TempFile.Name), true);
                    break;

                case OverwriteOptions.IfNewer:
                    if (File.Exists(Path.Combine(DestinationInfo.FullName, TempFile.Name))) {
                        var FileInfo = new FileInfo(Path.Combine(DestinationInfo.FullName, TempFile.Name));
                        if (FileInfo.LastWriteTime.CompareTo(TempFile.LastWriteTime) < 0) TempFile.CopyTo(Path.Combine(DestinationInfo.FullName, TempFile.Name), true);
                    } else {
                        TempFile.CopyTo(Path.Combine(DestinationInfo.FullName, TempFile.Name), true);
                    }
                    break;

                case OverwriteOptions.Never:
                    TempFile.CopyTo(Path.Combine(DestinationInfo.FullName, TempFile.Name), false);
                    break;
            }
        }

        if (Recursive) {
            foreach (var SubDirectory in Source.EnumerateDirectories())
                SubDirectory.CopyTo(Path.Combine(DestinationInfo.FullName, SubDirectory.Name), Recursive, Options);
        }

        return new DirectoryInfo(Destination);
    }
    #endregion CopyTo

    #region DeleteAll
    /// <summary>Deletes directory and all content found within it</summary>
    /// <param name="Info">Directory info object</param>
    public static void DeleteAll(this DirectoryInfo Info) {
        //Info.ThrowIfNull("Info");
        if (!Info.Exists) return;
        Info.DeleteFiles();
        Info.EnumerateDirectories().ForEach(x => x.DeleteAll());
        Info.Delete(true);
    }
    #endregion DeleteAll

    #region DeleteDirectoriesNewerThan
    /// <summary>Deletes directories newer than the specified date</summary>
    /// <param name="Directory">Directory to look within</param>
    /// <param name="CompareDate">The date to compare to</param>
    /// <param name="Recursive">Is this a recursive call</param>
    /// <returns>Returns the directory object</returns>
    public static DirectoryInfo DeleteDirectoriesNewerThan(this DirectoryInfo Directory, DateTime CompareDate, bool Recursive = false) {
        //Directory.ThrowIfNull("Directory");
        //Directory.ThrowIfNot(x => x.Exists, new DirectoryNotFoundException("Directory"));
        Directory.EnumerateDirectories("*", Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
            .Where(x => x.LastWriteTime > CompareDate)
            .ForEach(x => x.DeleteAll());
        return Directory;
    }
    #endregion DeleteDirectoriesNewerThan

    #region DeleteDirectoriesOlderThan
    /// <summary>Deletes directories newer than the specified date</summary>
    /// <param name="Directory">Directory to look within</param>
    /// <param name="CompareDate">The date to compare to</param>
    /// <param name="Recursive">Is this a recursive call</param>
    /// <returns>Returns the directory object</returns>
    public static DirectoryInfo DeleteDirectoriesOlderThan(this DirectoryInfo Directory, DateTime CompareDate, bool Recursive = false) {
        //Directory.ThrowIfNull("Directory");
        //Directory.ThrowIfNot(x => x.Exists, new DirectoryNotFoundException("Directory"));
        Directory.EnumerateDirectories("*", Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
            .Where(x => x.LastWriteTime < CompareDate)
            .ForEach(x => x.DeleteAll());
        return Directory;
    }
    #endregion DeleteDirectoriesOlderThan

    #region DeleteFiles
    /// <summary>Deletes files from a directory</summary>
    /// <param name="Directory">Directory to delete the files from</param>
    /// <param name="Recursive">Should this be recursive?</param>
    /// <returns>The directory that is sent in</returns>
    public static DirectoryInfo DeleteFiles(this DirectoryInfo Directory, bool Recursive = false) {
        //Directory.ThrowIfNull("Directory");
        //Directory.ThrowIfNot(x => x.Exists, new DirectoryNotFoundException("Directory"));
        Directory.EnumerateFiles("*", Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
            .ForEach(x => x.Delete());
        return Directory;
    }
    #endregion DeleteFiles

    #region DeleteFilesNewerThan
    /// <summary>Deletes files newer than the specified date</summary>
    /// <param name="Directory">Directory to look within</param>
    /// <param name="CompareDate">The date to compare to</param>
    /// <param name="Recursive">Is this a recursive call</param>
    /// <returns>Returns the directory object</returns>
    public static DirectoryInfo DeleteFilesNewerThan(this DirectoryInfo Directory, DateTime CompareDate, bool Recursive = false) {
        //Directory.ThrowIfNull("Directory");
        //Directory.ThrowIfNot(x => x.Exists, new DirectoryNotFoundException("Directory"));
        Directory.EnumerateFiles("*", Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
            .Where(x => x.LastWriteTime > CompareDate)
            .ForEach(x => x.Delete());
        return Directory;
    }
    #endregion DeleteFilesNewerThan

    #region DeleteFilesOlderThan
    /// <summary>Deletes files older than the specified date</summary>
    /// <param name="Directory">Directory to look within</param>
    /// <param name="CompareDate">The date to compare to</param>
    /// <param name="Recursive">Is this a recursive call</param>
    /// <returns>Returns the directory object</returns>
    public static DirectoryInfo DeleteFilesOlderThan(this DirectoryInfo Directory, DateTime CompareDate, bool Recursive = false) {
        //Directory.ThrowIfNull("Directory");
        //Directory.ThrowIfNot(x => x.Exists, new DirectoryNotFoundException("Directory"));
        Directory.EnumerateFiles("*", Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
            .Where(x => x.LastWriteTime < CompareDate)
            .ForEach(x => x.Delete());
        return Directory;
    }
    #endregion DeleteFilesOlderThan

    #region DriveInfo
    /// <summary>Gets the drive information for a directory</summary>
    /// <param name="Directory">The directory to get the drive info of</param>
    /// <returns>The drive info connected to the directory</returns>
    public static DriveInfo DriveInfo(this DirectoryInfo Directory) => new(Directory.Root.FullName);
    #endregion DriveInfo

    #region Size
    /// <summary>Gets the size of all files within a directory</summary>
    /// <param name="Directory">Directory</param>
    /// <param name="SearchPattern">Search pattern used to tell what files to include (defaults to all)</param>
    /// <param name="Recursive">determines if this is a recursive call or not</param>
    /// <returns>The directory size</returns>
    public static long Size(this DirectoryInfo Directory, string SearchPattern = "*", bool Recursive = false) =>
        Directory.EnumerateFiles(SearchPattern, Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).Sum(x => x.Length);
    #endregion Size

    #region SetAttributes
    /// <summary>Sets a directory's attributes</summary>
    /// <param name="Directory">Directory</param>
    /// <param name="Attributes">Attributes to set</param>
    /// <param name="Recursive">Determines if this is a recursive call</param>
    /// <returns>The directory object</returns>
    public static DirectoryInfo SetAttributes(this DirectoryInfo Directory, FileAttributes Attributes, bool Recursive = false) {
        //Directory.ThrowIfNull("Directory");
        Directory.EnumerateFiles()
            .ForEach(x => x.SetAttributes(Attributes));
        if (Recursive) Directory.EnumerateDirectories().ForEach(x => x.SetAttributes(Attributes, true));
        return Directory;
    }
    #endregion SetAttributes
}