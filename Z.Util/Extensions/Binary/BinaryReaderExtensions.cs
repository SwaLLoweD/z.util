﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Buffers;
using System.IO;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for BinaryReaders</summary>
public static class ExtBinaryReader
{
    /// <summary>Reads Data in specified Type.</summary>
    /// <param name="reader">The stream.</param>
    /// <returns>Read object</returns>
    public static T? Read<T>(this BinaryReader reader) {
        var rval = Read(reader, typeof(T));
        if (rval == null) return default;
        return (T)rval;
    }

    /// <summary>Reads Data in specified Type.</summary>
    /// <param name="reader">The stream.</param>
    /// <param name="type">Typeof object to be read</param>
    /// <returns>Read object</returns>
    public static object? Read(this BinaryReader reader, Type type) {
        if (type.Is().Nullable(out var underLyingType)) { //Null check
            if (reader.ReadByte() == 1) return null;
            if (underLyingType != null)
                type = underLyingType;
        }
        var c = ZConverter.GetByteConverter(type);
        if (c == null) throw new NotSupportedException("Object to be converted from byte[] is not recognized");
        var len = c.Length;
        if (len == -1) len = reader.ReadInt32();
        if (len == 0) return null;
        var b = reader.ReadBytes(len);
        return c.Reader(b, type);
    }

    #region Nullable Readers
    /// <inheritdoc />
    public static sbyte? ReadNSByte(this BinaryReader reader) => reader.ReadBoolean() ? (sbyte?)reader.ReadSByte() : null;
    /// <inheritdoc />
    public static short? ReadNInt16(this BinaryReader reader) => reader.ReadBoolean() ? (short?)reader.ReadInt16() : null;
    /// <inheritdoc />
    public static int? ReadNInt32(this BinaryReader reader) => reader.ReadBoolean() ? (int?)reader.ReadInt32() : null;
    /// <inheritdoc />
    public static long? ReadNInt64(this BinaryReader reader) => reader.ReadBoolean() ? (long?)reader.ReadInt64() : null;

    /// <inheritdoc />
    public static float? ReadNSingle(this BinaryReader reader) => reader.ReadBoolean() ? (float?)reader.ReadSingle() : null;
    /// <inheritdoc />
    public static double? ReadNDouble(this BinaryReader reader) => reader.ReadBoolean() ? (double?)reader.ReadDouble() : null;
    /// <inheritdoc />
    public static decimal? ReadNDecimal(this BinaryReader reader) => reader.ReadBoolean() ? (decimal?)reader.ReadDecimal() : null;

    /// <inheritdoc />
    public static bool? ReadNBoolean(this BinaryReader reader) {
        var val = reader.ReadByte();
        if ((val & 0x80) != 0) return null;
        return val == 1;
    }
    /// <inheritdoc />
    public static char? ReadNChar(this BinaryReader reader) => reader.ReadBoolean() ? (char?)reader.ReadChar() : null;
    /// <inheritdoc />
    public static byte? ReadNByte(this BinaryReader reader) => reader.ReadBoolean() ? (byte?)reader.ReadByte() : null;
    /// <inheritdoc />
    public static ushort? ReadNUInt16(this BinaryReader reader) => reader.ReadBoolean() ? (ushort?)reader.ReadUInt16() : null;
    /// <inheritdoc />
    public static uint? ReadNUInt32(this BinaryReader reader) => reader.ReadBoolean() ? (uint?)reader.ReadUInt32() : null;
    /// <inheritdoc />
    public static ulong? ReadNUInt64(this BinaryReader reader) => reader.ReadBoolean() ? (ulong?)reader.ReadUInt64() : null;
    #endregion

    #region VariableInt Readers
    /// <inheritdoc />
    public static short ReadVarInt16(this BinaryReader reader) => (short)BitConverterVar.DecodeZigZag(ToTarget(reader, 16));
    /// <inheritdoc />
    public static int ReadVarInt32(this BinaryReader reader) => (int)BitConverterVar.DecodeZigZag(ToTarget(reader, 32));
    /// <inheritdoc />
    public static long ReadVarInt64(this BinaryReader reader) => BitConverterVar.DecodeZigZag(ToTarget(reader, 64));

    /// <inheritdoc />
    public static float ReadVarSingle(this BinaryReader reader) => (float)ToTargetFraction(reader, 32)!;
    /// <inheritdoc />
    public static double ReadVarDouble(this BinaryReader reader) => (double)ToTargetFraction(reader, 64)!;
    /// <inheritdoc />
    public static decimal ReadVarDecimal(this BinaryReader reader) => (decimal)ToTargetDecimal(reader)!;

    /// <inheritdoc />
    public static ushort ReadVarUInt16(this BinaryReader reader) => (ushort)ToTarget(reader, 16);
    /// <inheritdoc />
    public static uint ReadVarUInt32(this BinaryReader reader) => (uint)ToTarget(reader, 32);
    /// <inheritdoc />
    public static ulong ReadVarUInt64(this BinaryReader reader) => ToTarget(reader, 64);

    /// <inheritdoc />
    public static sbyte? ReadNVarSByte(this BinaryReader reader) => (sbyte?)BitConverterVar.DecodeZigZag(ToTargetN(reader, 8));
    /// <inheritdoc />
    public static short? ReadNVarInt16(this BinaryReader reader) => (short?)BitConverterVar.DecodeZigZag(ToTargetN(reader, 16));
    /// <inheritdoc />
    public static int? ReadNVarInt32(this BinaryReader reader) => (int?)BitConverterVar.DecodeZigZag(ToTargetN(reader, 32));
    /// <inheritdoc />
    public static long? ReadNVarInt64(this BinaryReader reader) => BitConverterVar.DecodeZigZag(ToTargetN(reader, 64));

    /// <inheritdoc />
    public static float? ReadNVarSingle(this BinaryReader reader) => (float?)ToTargetFraction(reader, 32, true);
    /// <inheritdoc />
    public static double? ReadNVarDouble(this BinaryReader reader) => ToTargetFraction(reader, 64, true);
    /// <inheritdoc />
    public static decimal? ReadNVarDecimal(this BinaryReader reader) => ToTargetDecimal(reader, true);

    /// <inheritdoc />
    public static bool? ReadNVarBoolean(this BinaryReader reader) {
        var val = reader.ReadByte();
        if ((val & 0x80) != 0) return null;
        return val == 1;
    }
    /// <inheritdoc />
    public static char? ReadNVarChar(this BinaryReader reader) => (char?)ToTargetN(reader, 8);
    /// <inheritdoc />
    public static byte? ReadNVarByte(this BinaryReader reader) => (byte?)ToTargetN(reader, 8);
    /// <inheritdoc />
    public static ushort? ReadNVarUInt16(this BinaryReader reader) => (ushort?)ToTargetN(reader, 16);
    /// <inheritdoc />
    public static uint? ReadNVarUInt32(this BinaryReader reader) => (uint?)ToTargetN(reader, 32);
    /// <inheritdoc />
    public static ulong? ReadNVarUInt64(this BinaryReader reader) => ToTargetN(reader, 64);

    #region Helpers
    private static ulong ToTarget(BinaryReader reader, int sizeBites) {
        var shift = 0;
        ulong result = 0;
        var i = 0;
        ulong byteValue;
        do {
            byteValue = reader.ReadByte();
            var tmp = byteValue & 0x7f;
            result |= tmp << shift;

            if (shift > sizeBites) throw new ArgumentOutOfRangeException(nameof(reader), "Byte array is too large.");
            if (i++ > 10) throw new Exception("Invalid integer long in the input stream.");

            shift += 7;
        } while ((byteValue & 0x80) != 0);

        return result;
    }
    private static ulong? ToTargetN(BinaryReader reader, int sizeBites) {
        var input = reader.ReadByte();
        if ((input & 0x80) != 0) //null check at first byte (only at first byte)
            return null;
        if ((input & 0x40) == 0) //No continuation flag (second flag only at first byte)
            return (ulong)(input & 0x3f);
        var shift = 6; //2 flags are used 8-2= 6 for next
        var result = (ulong)(input & 0x3f);
        var i = 1;
        ulong byteValue;
        do {
            byteValue = reader.ReadByte();
            var tmp = byteValue & 0x7f;
            result |= tmp << shift;

            if (shift > sizeBites) throw new ArgumentOutOfRangeException(nameof(reader), "Byte array is too large.");
            if (i++ > 10) throw new Exception("Invalid integer long in the input stream.");

            shift += 7;
        } while ((byteValue & 0x80) != 0);

        return result;
    }

    private static double? ToTargetFraction(BinaryReader reader, int sizeBites, bool isNullable = false) {
        var sizeBytes = sizeBites / 8;
        var input = reader.ReadByte();
        if (isNullable && (input & 0x80) != 0) return null;
        return ArrayPool<byte>.Shared.RentSpanDo(sizeBytes, (bytes) => {
            if (isNullable ? (input & 0x40) != 0 : (input & 0x80) != 0) {
                bytes[sizeBytes - 1] = (byte)(isNullable ? input & 0x3f : input & 0x7f);
            } else {
                for (var i = 0; i < input; i++)
                    bytes[sizeBytes - input + i] = reader.ReadByte();
            }
            return sizeBytes <= 4 ? BitConverter.ToSingle(bytes) : BitConverter.ToDouble(bytes);
        });
    }
    private static decimal? ToTargetDecimal(BinaryReader reader, bool isNullable = false) {
        var input = reader.ReadByte();
        if (isNullable && (input & 0x80) != 0) return null;
        return ArrayPool<int>.Shared.RentSpanDo(4, (decimalBits) => {
            if (isNullable ? (input & 0x40) != 0 : (input & 0x80) != 0) { //Read packed
                input &= (byte)(isNullable ? 0x3f : 0x7f);
                if (input == 0) return 0m;
                decimalBits[0] = input;
            } else {
                decimalBits.Clear();
                var shift = 0;
                for (var i = 0; i < input; i++) {
                    shift %= 32;
                    var di = i / 4;
                    decimalBits[di] |= reader.ReadByte() << shift;
                    shift = (shift + 8) % 32;
                }
            }
            return new decimal(decimalBits[..4]);
        });
    }
    #endregion
    #endregion
}