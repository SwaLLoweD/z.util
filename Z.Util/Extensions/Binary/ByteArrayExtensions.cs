﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Util;

//using System.IO.Compression;

namespace Z.Extensions;

/// <summary>Extension methods for byte[] values...</summary>
public static class ExtByteArray {
    /// <summary>Converts the specified byte[] to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion To(this byte[] value) => new(value);
    /// <summary>Converts the specified byte[] to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion To(this IEnumerable<byte> value) => new(value);
    /// <summary>Converts the specified byte[] to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion To(this IList<byte> value) => new(value);
    /// <summary>Transform the specified byteArray.</summary>
    /// <param name="value">The value to be transformed.</param>
    /// <returns>An universal transformer supplying additional transforming methods</returns>
    public static TransformPortion Transform(this IList<byte> value) => new(value);
    /// <summary>Deserialize ByteArray To Object.</summary>
    /// <param name="value">The value to be deserialized.</param>
    /// <returns>An universal transformer supplying additional transforming methods</returns>
    public static DeserializePortion Deserialize(this IEnumerable<byte> value) => new(value);

    #region Nested type: ConvertPortion
    /// <summary>Multi-method library for byte[] conversion related commands.</summary>
    public class ConvertPortion: ExtIEnumerable.ConvertPortion<byte> {
        #region Fields / Properties
        /// <summary>Converts the specified byte[] to a different types of string.</summary>
        /// <returns>An universal converter supplying additional target conversion methods</returns>
        public StringConverter String => new(Value);
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConvertPortion(IEnumerable<byte> value) : base(value) { }
        #endregion

        /// <summary>Converts the specified byte[] to a Binary File.</summary>
        /// <param name="fileName">File path to create/append</param>
        /// <param name="offset">offset x amount of bytes</param>
        /// <param name="len">write x amount of bytes</param>
        /// <param name="fm">FileMode to be used</param>
        public void File(string fileName, int offset = 0, int? len = null, FileMode fm = FileMode.Create) => Task.Run(() => FileAsync(fileName, offset, len, fm));
        /// <summary>Converts the specified byte[] to a Binary File.</summary>
        /// <param name="fileName">File path to create/append</param>
        /// <param name="offset">offset x amount of bytes</param>
        /// <param name="len">write x amount of bytes</param>
        /// <param name="fm">FileMode to be used</param>
        public async Task FileAsync(string fileName, int offset = 0, int? len = null, FileMode fm = FileMode.Create) {
            using var fs = new FileStream(fileName, fm, FileAccess.Write);
            var value = Value.ToArray();
            len ??= value.Length - offset;
            await fs.WriteAsync(value.AsMemory(offset, (int)len));
            fs.Close();
        }
        /// <summary>Converts the specified Binary file to a byte[].</summary>
        /// <param name="fileName">File path to read</param>
        /// <param name="offset">offset x amount of bytes</param>
        /// <param name="len">write x amount of bytes</param>
        /// <param name="fm">FileMode to be used</param>
        public Memory<byte> NewArrayFromFile(string fileName, int offset = 0, int? len = null, FileMode fm = FileMode.Open) => Task.Run(() => NewArrayFromFileAsync(fileName, offset, len, fm)).Result;
        /// <summary>Converts the specified Binary file to a byte[].</summary>
        /// <param name="fileName">File path to read</param>
        /// <param name="offset">offset x amount of bytes</param>
        /// <param name="len">write x amount of bytes</param>
        /// <param name="fm">FileMode to be used</param>
        public async Task<Memory<byte>> NewArrayFromFileAsync(string fileName, int offset = 0, int? len = null, FileMode fm = FileMode.Open) {
            var fs = new FileStream(fileName, fm, FileAccess.Read);
            Memory<byte> rval = Value.ToArray();
            if (len != null)
                await fs.ReadAsync(rval.Slice(offset, (int)len));
            else
                rval = await fs.ReadAllBytesAsync();
            fs.Close();
            await fs.DisposeAsync();
            Value = rval.ToArray();
            return rval;
        }

        #region Nested type: StringConverter
        /// <summary>Multi-method library for byte[] to string conversion related commands.</summary>
        public class StringConverter: ExtendablePortion<IEnumerable<byte>> {
            #region Fields / Properties
            /// <summary>Converts the specified byte[] to string using ASCII encoding.</summary>
            public string AsAscii {
                get {
                    var rval = new StringBuilder();
                    foreach (var by in Value) rval.Append(by.ToChar());
                    return rval.ToString();
                }
            }

            /// <summary>Converts the specified byte[] to string using Base32 encoding.</summary>
            public string AsBase32 => Base32Url.ToBase32String(Value.ToArray());

            /// <summary>Converts the specified byte[] to string using Base36 encoding.</summary>
            public string AsBase36 => BaseN.ToBaseN(Value, BaseN.Base36Alphabet);
            /// <summary>Converts the specified byte[] to string using Base64 encoding.</summary>
            public string AsBase64 => Convert.ToBase64String(Value.ToArray());

            /// <summary>Converts the specified byte[] to string using UTF8 encoding.</summary>
            public string AsUtf8 => Encoding.UTF8.GetString(Value.ToArray());
            #endregion

            #region Constructors
            /// <summary>Constructor</summary>
            public StringConverter(IEnumerable<byte> value) : base(value) { }
            #endregion

            /// <summary>Converts the specified byte[] to string using Hex encoding.</summary>
            public string AsHex(int offset = 0, int? len = null) {
                var value = Value.ToArray();
                len ??= value.Length;
                var rval = new StringBuilder();
                for (var i = offset; i < len; i++) rval.Append(value[i].ToString("X2"));
                return rval.ToString();
            }

            /// <summary>Converts the specified byte[] to string using specified alphabet.</summary>
            public string AsBaseN(string alphabet) => ThrowIfValueNull(BaseN.ToBaseN(Value, alphabet));
        }
        #endregion
    }
    #endregion

    #region Nested type: DeserializePortion
    /// <summary>Multi-method library for byte array deserialization related commands.</summary>
    public class DeserializePortion: ExtIEnumerable.TransformPortion<byte> {
        #region Constructors
        /// <summary>Constructor</summary>
        public DeserializePortion(IEnumerable<byte> value) : base(value) { }
        #endregion

        #region BoisBinary
        /// <summary>Deserialize a byte[] to an object using Salar Bois Binary Serializer.</summary>
        public T? BoisBinary<T>(ZTypeCacheFlags opts = ZTypeCacheFlags.Default) => (T?)BoisBinary(typeof(T), opts);
        /// <summary>Deserialize a byte[] to an object using Salar Bois Binary Serializer.</summary>
        public object? BoisBinary(Type objectType, ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            var valueArr = Value.ToArray();
            using var bs = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "Bois Deserialize", valueArr, 0, valueArr.Length);
            bs.Position = 0;
            return bs.Deserialize().BoisBinary(objectType, opts);
            //var b = Value.ToArray();
            //var bf = new Salar.Bois.BoisSerializer();
            //rval = bf.Deserialize(b, objectType, 0, b.Length);
        }
        #endregion

        #region DotNetBinary
        /// <summary>Deserialize a byte[] to an object using Binary Serializer.</summary>
        public T? DotNetBinary<T>() => (T?)DotNetBinary(typeof(T));
        /// <summary>Deserialize a byte[] to an object using Binary Serializer.</summary>
        public object? DotNetBinary(Type objectType) {
            var rval = objectType.GetDefault();
            if (Value == null) return rval;
            var valueArr = Value.ToArray();
            using (var bs = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "DotNetBinary Deserialize", valueArr, 0, valueArr.Length)) {
                bs.Position = 0;
                rval = bs.Deserialize().DotNetBinary(objectType);
            }
            return rval;
        }
        #endregion
    }
    #endregion

    #region Nested type: TransformPortion
    /// <summary>Multi-method library for byte array transformation related commands.</summary>
    public class TransformPortion: ExtIEnumerable.TransformPortion<byte> {
        #region Constructors
        /// <summary>Constructor</summary>
        public TransformPortion(IList<byte> value) : base(value) { }
        #endregion

        /// <summary>Fold higher end of the array onto itself via XOR</summary>
        public byte[] FoldRight(int targetLength) {
            var value = ThrowIfValueNull(Value);
            var array = value.ToArray();
            if (array.Length < targetLength) throw new Exception("TargetLength is greater than array length");
            if (array.Length > targetLength * 2) array = array.Transform().FoldRight(targetLength * 2);
            var rval = new byte[targetLength];
            Buffer.BlockCopy(array, 0, rval, 0, targetLength);

            for (var i = 0; i < array.Length - targetLength; i++) {
                var rvalInd = targetLength - 1 - i;
                var arrInd = targetLength + i;
                rval[rvalInd] ^= array[arrInd];
            }
            return rval;
        }

        /// <summary>Creates a same length array with randomized values</summary>
        public byte[] Randomize() {
            var value = ThrowIfValueNull(Value);
            return Encrypter.CreateRandomBytes(value.Count());
        }
    }
    #endregion

    private static T ThrowIfValueNull<T>(T? value) => value == null ? throw new NullReferenceException("Value is null") : value;
    //private static U ThrowIfValueNull<T,U>(T? value, Func<T,U> func) => value == null ? throw new NullReferenceException("Value is null") : func(value);
}