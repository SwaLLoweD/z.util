﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for BinaryWriters</summary>
public static class ExtBinaryWriter
{
    /// <summary>Writes Data in specified Type.</summary>
    /// <param name="writer">The stream.</param>
    /// <param name="arg">Arguments to be written</param>
    public static void Write<T>(this BinaryWriter writer, T arg) {
        if (arg == null) {
            writer.Write((byte)1); //data length set to 0 (Either serializable, string or byte[])
            return;
        }
        var type = typeof(T);
        if (type.Is().Nullable(out var underLyingType)) { //Null Check
            writer.Write((byte)0);
            if (underLyingType != null)
                type = underLyingType;
        }
        var c = ZConverter.GetByteConverter(type);
        if (c == null) throw new NotSupportedException("Object to be converted to byte[] is not recognized");
        var b = c.Writer(arg);
        if (c.Length == -1) writer.Write(b.Length);
        writer.Write(b);
    }

    #region Nullable Writers
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, sbyte? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, short? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, int? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, long? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }

    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, float? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, double? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, decimal? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }

    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, bool? value) => writer.Write((byte)(value.HasValue ? value.Value ? 1 : 0 : 0x80));
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, char? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, byte? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, ushort? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, uint? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    /// <inheritdoc />
    public static void Write(this BinaryWriter writer, ulong? value) {
        if (value.HasValue) {
            writer.Write(true);
            writer.Write(value.Value);
        } else {
            writer.Write(false);
        }
    }
    #endregion

    #region VariableInt Writers
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, byte value) => writer.Write(value);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, short value) => writer.WriteVar((ulong)BitConverterVar.EncodeZigZag(value, 16));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, int value) => writer.WriteVar((ulong)BitConverterVar.EncodeZigZag(value, 32));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, long value) => writer.WriteVar((ulong)BitConverterVar.EncodeZigZag(value, 64));

    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, float value) => WriteFraction(writer, BitConverter.GetBytes(value));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, double value) => WriteFraction(writer, BitConverter.GetBytes(value));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, decimal value) => WriteFraction(writer, value);

    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, ushort value) => writer.WriteVar((ulong)value);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, uint value) => writer.WriteVar((ulong)value);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, ulong value) {
        do {
            var byteVal = value & 0x7f;
            value >>= 7;

            if (value != 0) byteVal |= 0x80;

            writer.Write((byte)byteVal);
        } while (value != 0);
    }

    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, sbyte? value) => writer.WriteVar((ulong?)BitConverterVar.EncodeZigZag(value, 8));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, short? value) => writer.WriteVar((ulong?)BitConverterVar.EncodeZigZag(value, 16));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, int? value) => writer.WriteVar((ulong?)BitConverterVar.EncodeZigZag(value, 32));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, long? value) => writer.WriteVar((ulong?)BitConverterVar.EncodeZigZag(value, 64));

    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, float? value) => WriteFraction(writer, value.HasValue ? BitConverter.GetBytes(value.Value) : null, true);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, double? value) => WriteFraction(writer, value.HasValue ? BitConverter.GetBytes(value.Value) : null, true);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, decimal? value) => WriteFraction(writer, value, true);

    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, bool? value) => writer.Write((byte)(value.HasValue ? value.Value ? 1 : 0 : 0x80));
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, char? ch) => writer.WriteVar((ulong?)ch);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, byte? value) => writer.WriteVar((ulong?)value);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, ushort? value) => writer.WriteVar((ulong?)value);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, uint? value) => writer.WriteVar((ulong?)value);
    /// <inheritdoc />
    public static void WriteVar(this BinaryWriter writer, ulong? value) {
        if (value == null) {
            writer.Write((byte)0x80); //null flag
        } else if (value.Value < 0x40) {
            writer.Write((byte)value.Value); //with two flags gone only 6 bits are left yet this fits
        } else {
            var portion = value.Value & 0x3f;
            writer.Write((byte)(portion | 0x40)); //add has continuation flag
            var rest = value.Value >> 6; //this part is written;
            writer.WriteVar(rest); //do the rest normally
        }
    }

    #region Helpers
    private static void WriteFraction(BinaryWriter writer, byte[]? fractionBytes, bool isNullable = false) {
        // Float  &double numeric formats stores valuable bytes from right to left
        //var valueBuff = BitConverter.GetBytes(value);

        if (fractionBytes == null) {
            if (isNullable) {
                writer.Write((byte)0x80);
                return;
            } else { throw new NullReferenceException("Value is null when marked notNullable"); }
        }

        for (var i = 0; i <= fractionBytes.Length - 2; i++) {
            if (fractionBytes[i] == 0) continue;
            writer.Write((byte)(fractionBytes.Length - i));
            for (var j = i; j < fractionBytes.Length; j++) writer.Write(fractionBytes[j]);
            return;
        }
        var lastValue = fractionBytes[^1];
        //Will it fit with packed flag set
        if (isNullable ? lastValue < 0x40 : lastValue < 0x80) {
            // set the flag inside
            lastValue |= (byte)(isNullable ? 0x40 : 0x80);
            writer.Write(lastValue);
        } else {
            writer.Write((byte)1);
            writer.Write(lastValue);
        }
    }
    private static void WriteFraction(BinaryWriter writer, decimal? value, bool isNullable = false) {
        if (value == null) {
            if (isNullable) {
                writer.Write((byte)0x80);
                return;
            } else { throw new NullReferenceException("Value is null when marked notNullable"); }
        }

        var bits = decimal.GetBits(value.Value);
        var bitsArray = new byte[16];

        for (byte i = 0; i < bits.Length; i++) {
            var bytes = BitConverter.GetBytes(bits[i]);
            Array.Copy(bytes, 0, bitsArray, i * 4, 4);
        }
        // finding the empty characters
        for (var i = bitsArray.Length - 1; i > 0; i--) {
            if (bitsArray[i] == 0) continue;
            writer.Write((byte)(i + 1));
            for (var j = 0; j <= i; j++) writer.Write(bitsArray[j]);
            return;
        }
        var lastValue = bitsArray[0];
        //Will it fit with packed flag set
        if (isNullable ? lastValue < 0x40 : lastValue < 0x80) {
            // set the flag inside
            lastValue |= (byte)(isNullable ? 0x40 : 0x80);
            writer.Write(lastValue);
        } else {
            writer.Write((byte)1);
            writer.Write(lastValue);
        }
    }
    #endregion;
    #endregion
}