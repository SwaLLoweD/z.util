﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Z.Serialization;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for Streams</summary>
public static class ExtStream {
    /// <summary>Deserialize Stream to Object</summary>
    /// <param name="value">The stream to be Deserialized.</param>
    /// <returns>An universal deserialize helper supplying additional deserialization methods</returns>
    public static DeserializePortion Deserialize(this Stream value) => new(value);

    /// <summary>Opens a StreamReader using the specified encoding.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="encoding">The encoding.</param>
    /// <returns>The stream reader</returns>
    public static StreamReader GetStreamReader(this Stream stream, Encoding? encoding = null) {
        if (!stream.CanRead) throw new InvalidOperationException("Stream does not support reading.");

        encoding ??= Encoding.Default;
        return new StreamReader(stream, encoding);
    }

    /// <summary>Opens a StreamWriter using the specified encoding.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="encoding">The encoding.</param>
    /// <returns>The stream writer</returns>
    public static StreamWriter GetStreamWriter(this Stream stream, Encoding? encoding = null) {
        if (!stream.CanWrite) throw new InvalidOperationException("Stream does not support writing.");

        encoding ??= Encoding.Default;
        return new StreamWriter(stream, encoding);
    }

    /// <summary>Opens a BinaryReader using the specified encoding.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="encoding">The encoding.</param>
    /// <returns>The stream reader</returns>
    public static BinaryReader GetBinaryReader(this Stream stream, Encoding? encoding = null) {
        if (!stream.CanRead) throw new InvalidOperationException("Stream does not support reading.");

        encoding ??= Encoding.Default;
        return new BinaryReader(stream, encoding);
    }

    /// <summary>Opens a BinaryWriter using the specified encoding.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="encoding">The encoding.</param>
    /// <returns>The stream writer</returns>
    public static BinaryWriter GetBinaryWriter(this Stream stream, Encoding? encoding = null) {
        if (!stream.CanWrite) throw new InvalidOperationException("Stream does not support writing.");

        encoding ??= Encoding.Default;
        return new BinaryWriter(stream, encoding);
    }
    #region IndexOf
    /// <summary>
    ///     Searchs a list for a sub-sequence of items that match a particular pattern. A subsequence of
    ///     <paramref name="stream" /> matches pattern at index i if list[i] is equal to the first item in
    ///     <paramref name="pattern" />, list[i+1] is equal to the second item in <paramref name="pattern" />, and so forth for
    ///     all the items in <paramref name="pattern" />. The passed instance of IEqualityComparer&lt;T&gt; is used for
    ///     determining if two items are equal.
    /// </summary>
    /// <param name="stream">The list to search.</param>
    /// <param name="pattern">The sequence of items to search for.</param>
    /// <param name="equalityComparer">
    ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
    ///     method will be called.
    /// </param>
    /// <returns>The first index with <paramref name="stream" /> that matches the items in <paramref name="pattern" />.</returns>
    public static IList<int> IndexOf(this Stream stream, ReadOnlySpan<byte> pattern, IEqualityComparer<byte>? equalityComparer = null) {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        return IndexOf(stream, pattern, equalityComparer ?? EqualityComparer<byte>.Default);
    }
    /// <summary>
    ///     Searchs a list for a sub-sequence of items that match a particular pattern. A subsequence of
    ///     <paramref name="stream" /> matches pattern at index i if list[i] is "equal" to the first item in
    ///     <paramref name="pattern" />, list[i+1] is "equal" to the second item in <paramref name="pattern" />, and so forth
    ///     for all the items in <paramref name="pattern" />. The passed BinaryPredicate is used to determine if two items are
    ///     "equal".
    /// </summary>
    /// <remarks>
    ///     Since an arbitrary BinaryPredicate is passed to this function, what is being tested for in the pattern need
    ///     not be equality.
    /// </remarks>
    /// <param name="stream">The list to search.</param>
    /// <param name="pattern">The sequence of items to search for.</param>
    /// <param name="predicate">The BinaryPredicate used to compare items for "equality". </param>
    /// <returns>The first index with <paramref name="stream" /> that matches the items in <paramref name="pattern" />.</returns>
    public static IList<int> IndexOf(this Stream stream, ReadOnlySpan<byte> pattern, Func<byte, byte, bool> predicate) {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        if (!stream.CanSeek) throw new ArgumentException("IndexOf needs the stream to support seeking", nameof(stream));
        if (predicate == null) throw new ArgumentNullException(nameof(predicate));

        var startPos = stream.Position;
        var list = stream.ReadAllBytes(true);

        var rval = new List<int>();

        int listCount = list.Length, patternCount = pattern.Length;
        if (patternCount == 0) {
            rval.Add(0); // A zero-length pattern occurs anywhere.
            return rval;
        }
        if (listCount == 0) return rval; // no room for a pattern;

        var start = 0;
        while (start <= listCount - patternCount) {
            for (var count = 0; count < patternCount; ++count) {
                if (!predicate(list[start + count], pattern[count]))
                    goto NOMATCH;
            }
            // Got through the whole pattern. We have a match.
            //return start;
            rval.Add(start);

        NOMATCH:
            /* no match found at start. */
            ;
            ++start;
        }
        stream.Position = startPos;
        // no match found anywhere.
        return rval;
        //List<int> positions = new List<int>();
        //if (pattern.Is().Empty) return positions.ToArray();

        //var patternArray = pattern.ToArray();
        //int patternLength = patternArray.Length;
        //int totalLength = value.Count;
        //int firstMatch = value.IndexOf(patternArray[0]);
        //for (int i = 0; i < totalLength; i++) {
        //    if (firstMatch.Equals(value[i]) && totalLength - i >= patternLength) {
        //        var match = value.SubList(i, patternLength);
        //        if (match.SequenceEqual<T>(pattern)) {
        //            positions.Add(i);
        //            i += patternLength - 1;
        //        }
        //    }
        //}
        //return positions;
    }
    #endregion

    #region Read
    /// <summary>Reads the entire stream and returns a byte array.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="fromStart">Read from the start or read from current position</param>
    /// <returns>The byte array</returns>
    public static byte[] ReadAllBytes(this Stream stream, bool fromStart = true) {
        if (fromStart) {
            if (stream is MemoryStream stream1) return stream1.ToArray();
            using var memoryStream = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "ReadAllBytes", (int)stream.Length);
            stream.CopyTo(memoryStream);
            return memoryStream.ToArray();
        }
        var len = (int)(stream.Length - stream.Position);
        var rval = new byte[len];
        ReadBytes(stream, rval.AsSpan());
        return rval;
    }
    /// <summary>Reads the entire stream and returns a byte array.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="fromStart">Read from the start or read from current position</param>
    /// <returns>The byte array</returns>
    public static async Task<byte[]> ReadAllBytesAsync(this Stream stream, bool fromStart = true) {
        if (fromStart) {
            if (stream is MemoryStream stream1) return stream1.ToArray();
            using var memoryStream = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "ReadAllBytes", (int)stream.Length);
            await stream.CopyToAsync(memoryStream);
            return memoryStream.ToArray();
        }
        var len = (int)(stream.Length - stream.Position);
        var rval = new byte[len];
        await ReadBytesAsync(stream, rval.AsMemory());
        return rval;
    }
    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="target">Target span to be filled</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="acceptShorterOutput">Return up to the target amount</param>
    /// <returns>Read object</returns>
    public static int ReadBytes(this Stream stream, Span<byte> target, int timeout = -1, bool acceptShorterOutput = false) {
        if (target.IsEmpty) throw new ArgumentNullException(nameof(target));
        var read = 0;
        while (read < target.Length) {
            if (timeout > 0 && stream.CanTimeout) stream.ReadTimeout = timeout;
            var result = stream.Read(target[read..]);
            if (result <= 0) {
                if (acceptShorterOutput) return read;
                throw new IOException("Read zero bytes, stream might be closed");
            }
            read += result;
        }
        return read;
    }
    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="len">Length of the array to be read</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="acceptShorterOutput">Return up to the target amount</param>
    /// <returns>Read object</returns>
    public static Span<byte> ReadBytes(this Stream stream, int len, int timeout = -1, bool acceptShorterOutput = false) {
        var rval = new byte[len]; //Array will be returned can not be rented
        var read = ReadBytes(stream, rval, timeout, acceptShorterOutput);
        return rval.AsSpan(0, read);
    }
    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="len">Length of the array to be read</param>
    /// <param name="func">function to call</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <returns>Read object</returns>
    public static T ReadBytesDo<T>(this Stream stream, int len, SpanFunc<byte, T> func, int timeout = -1) {
        return ArrayPool<byte>.Shared.RentSpanDo(len, (target) => {
            ReadBytes(stream, target, timeout, false);
            return func(target);
        });
    }

    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="len">Length of the array to be read</param>
    /// <param name="func">function to call</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <returns>Read object</returns>
    public static T ReadBytesDo<T>(this Stream stream, int len, ReadBytesDoDelegate<T> func, int timeout = -1) {
        return ArrayPool<byte>.Shared.RentSpanDo(len, (target) => {
            var read = ReadBytes(stream, target, timeout, true);
            return func(target, read);
        });
    }
    /// <summary>Reads a byte array of specified length from current position.</summary>
    public delegate T ReadBytesDoDelegate<T>(ReadOnlySpan<byte> buffer, int len);

    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="target">Target span to be filled</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="acceptShorterOutput">Return up to the target amount</param>
    /// <returns>Read object</returns>
    public static async Task<int> ReadBytesAsync(this Stream stream, Memory<byte> target, int timeout = -1, bool acceptShorterOutput = false) {
        var read = 0;
        while (read < target.Length) {
            var result = await stream.ReadAsync(target[read..]).TimeoutAfter(timeout).ConfigureAwait(false);
            if (result <= 0) {
                if (acceptShorterOutput) return read;
                throw new IOException("Read zero bytes, stream might be closed");
            }
            read += result;
        }
        return read;
    }
    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="len">Length of the array to be read</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="acceptShorterOutput">Return up to the target amount</param>
    /// <returns>Read object</returns>
    public static async Task<Memory<byte>> ReadBytesAsync(this Stream stream, int len, int timeout = -1, bool acceptShorterOutput = false) {
        var rval = new byte[len]; //Array will be returned can not be rented
        var read = await ReadBytesAsync(stream, rval, timeout, acceptShorterOutput);
        return rval.AsMemory(0, read);
    }
    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="len">Length of the array to be read</param>
    /// <param name="func">function to call</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <returns>Read object</returns>
    public static Task<T> ReadBytesDoAsync<T>(this Stream stream, int len, Func<Memory<byte>, T> func, int timeout = -1) {
        return ArrayPool<byte>.Shared.RentMemDo(len, async (target) => {
            await ReadBytesAsync(stream, target, timeout, false);
            return func(target);
        });
    }
    /// <summary>Reads a byte array of specified length from current position.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="len">Length of the array to be read</param>
    /// <param name="func">function to call</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <returns>Read object</returns>
    public static Task<T> ReadBytesDoAsync<T>(this Stream stream, int len, Func<Memory<byte>, int, T> func, int timeout = -1) {
        return ArrayPool<byte>.Shared.RentMemDo(len, async (target) => {
            var read = await ReadBytesAsync(stream, target, timeout, true);
            return func(target, read);
        });
    }

    /// <summary>Reads Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="maxLength">Maximum length of bytes to be read for the type</param>
    /// <returns>Read object</returns>
    public static T? Readz<T>(this Stream stream, int timeout = -1, int maxLength = int.MaxValue) => (T?)(Readz(stream, typeof(T), timeout, maxLength) ?? default(T));

    /// <summary>Reads Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="type">Typeof object to be read</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="maxLength">Maximum length of bytes to be read for the type</param>
    /// <returns>Read object</returns>
    public static object? Readz(this Stream stream, Type type, int timeout = -1, int maxLength = int.MaxValue) {
        if (type.Is().Nullable(out var underLyingType)) { //Null check
            if (stream.ReadByte() == 1) return null;
            if (underLyingType != null)
                type = underLyingType;
        }
        var c = ZConverter.GetByteConverter(type);
        if (c == null) throw new NotSupportedException("Object to be converted from byte[] is not recognized");
        var len = c.Length;
        if (len == -1) {
            len = ReadBytesDo(stream, 4, (lenBytes) => BitConverter.ToInt32(lenBytes), timeout);
            if (len > maxLength) throw new InternalBufferOverflowException($"Length of the object ({len}) is greater than the set max limit ({maxLength})");
        }
        if (len <= 0) return null;
        return ReadBytesDo(stream, len, (b) => c.Reader(b, type), timeout);
    }

    /// <summary>Reads Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="maxLength">Maximum length of bytes to be read for the type</param>
    /// <returns>Read object</returns>
    public static async Task<T?> ReadzAsync<T>(this Stream stream, int timeout = -1, int maxLength = int.MaxValue) =>
        (T?)(await ReadzAsync(stream, typeof(T), timeout, maxLength).ConfigureAwait(false) ?? default);

    /// <summary>Reads Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="type">Typeof object to be read</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="maxLength">Maximum length of bytes to be read for the type</param>
    /// <returns>Read object</returns>
    public static async Task<object?> ReadzAsync(this Stream stream, Type type, int timeout = -1, int maxLength = int.MaxValue) {
        if (type.Is().Nullable(out var underLyingType)) { //Null check
            if (stream.ReadByte() == 1) return null;
            if (underLyingType != null)
                type = underLyingType;
        }
        var c = ZConverter.GetByteConverter(type);
        if (c == null) throw new NotSupportedException("Object to be converted from byte[] is not recognized");
        var len = c.Length;
        if (len == -1) {
            len = await ReadBytesDoAsync(stream, 4, (lenBytes) => BitConverter.ToInt32(lenBytes.Span), timeout).ConfigureAwait(false);
            if (len > maxLength) throw new InternalBufferOverflowException($"Length of the object ({len}) is greater than the set max limit ({maxLength})");
        }
        if (len <= 0) return null;
        return await ReadBytesDoAsync(stream, len, (b) => c.Reader(b.Span, type), timeout).ConfigureAwait(false);
    }
    #endregion

    #region Write
    /// <summary>Writes Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="arg">Arguments to be written</param>
    /// <param name="timeout">Timeout in msecs</param>
    public static void Writez<T>(this Stream stream, T arg, int timeout = -2) => Writez(stream, arg, typeof(T), timeout);

    /// <summary>Writes Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="arg">Arguments to be written</param>
    /// <param name="type">Type of the Argument to be written</param>
    /// <param name="timeout">Timeout in msecs</param>
    public static void Writez(this Stream stream, object? arg, Type type, int timeout = -2) {
        if (arg == null) {
            stream.WriteByte(1);
            return;
        }
        if (timeout > 0 && stream.CanTimeout) stream.WriteTimeout = timeout;
        if (type.Is().Nullable(out var underLyingType)) { //Null Check
            stream.WriteByte(0);
            if (underLyingType != null)
                type = underLyingType;
        }
        var c = ZConverter.GetByteConverter(type);
        if (c == null) throw new NotSupportedException("Object to be converted to byte[] is not recognized");
        var b = c.Writer(arg);
        //Set timeout
        //if (timeout != -2 && stream.CanTimeout) stream.WriteTimeout = timeout;
        if (c.Length < 0) stream.Write(BitConverter.GetBytes(b.Length), 0, 4);
        stream.Write(b);
    }

    /// <summary>Writes Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="arg">Arguments to be written</param>
    /// <param name="timeout">Timeout in msecs</param>
    public static async Task WritezAsync<T>(this Stream stream, T? arg, int timeout = -2) =>
        await WritezAsync(stream, arg, typeof(T), timeout).ConfigureAwait(false);

    /// <summary>Writes Data in specified Type.</summary>
    /// <param name="stream">The stream.</param>
    /// <param name="arg">Arguments to be written</param>
    /// <param name="type">Type of the argument</param>
    /// <param name="timeout">Timeout in msecs</param>
    public static async Task WritezAsync(this Stream stream, object? arg, Type type, int timeout = -2) {
        if (arg == null) {
            stream.WriteByte(1);
            return;
        }
        if (type.Is().Nullable(out var underLyingType)) { //Null Check
            stream.WriteByte(0);
            if (underLyingType != null)
                type = underLyingType;
        }
        var c = ZConverter.GetByteConverter(type);
        if (c == null) throw new NotSupportedException("Object to be converted to byte[] is not recognized");
        var b = c.Writer(arg).ToArray();
        //Set timeout
        //if (timeout != -2 && stream.CanTimeout) stream.WriteTimeout = timeout;
        if (c.Length < 0) await stream.WriteAsync(BitConverter.GetBytes(b.Length), 0, 4).TimeoutAfter(timeout).ConfigureAwait(false);
        await stream.WriteAsync(b).TimeoutAfter(timeout).ConfigureAwait(false);
    }

    #endregion

    #region CopyTo 
    /// <summary>Reads the bytes from the current stream and writes them to another stream, using a specified buffer size.</summary>
    /// <param name="source">Source Stream</param>
    /// <param name="destination">The stream to which the contents of the current stream will be copied.</param>
    /// <param name="length">
    ///     Length of contents to be copied (exception is thrown if source buffer does not have enough
    ///     elements, depending on accept param setting)
    /// </param>
    /// <param name="bufferSize">The size of the buffer. This value must be greater than zero. The default size is 4096</param>
    /// <param name="action">Action to be performed while copying for each buffer</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="acceptShorterOutput">if data read is less than the length parameter accept this as valid or not</param>
    public static long CopyTo(this Stream source, Stream destination, Func<ReadOnlyMemory<byte>, Stream, long>? action, long length = -1, int bufferSize = 81920, int timeout = -1, bool acceptShorterOutput = false) {
        if (timeout > 0) {
            if (source.CanTimeout) source.ReadTimeout = timeout;
            if (destination.CanTimeout) destination.WriteTimeout = timeout;
        }
        bufferSize = (int)(length >= 0 && length < bufferSize ? length : bufferSize);
        return ArrayPool<byte>.Shared.RentMemDo(bufferSize, (buffer) => {
            var totalRead = 0L;
            var totalWrite = 0L;
            var bufferRead = 0;
            int read = 1;

            while (read > 0) {
                var lenToRead = bufferSize - bufferRead;
                if (length >= 0 && lenToRead > length - totalRead) lenToRead = (int)(length - totalRead); //dont read more than len if len >= 0
                read = source.Read(buffer.Span.Slice(bufferRead, lenToRead));
                bufferRead += read;
                totalRead += read;
                if (bufferRead == bufferSize || (read == 0 && bufferRead != 0) || totalRead == length) {
                    if (action != null) {
                        var writtenLen = action(buffer[..bufferRead], destination);
                        totalWrite += writtenLen;
                    } else {
                        destination.Write(buffer.Span[..bufferRead]);
                        totalWrite += bufferRead;
                    }
                    bufferRead = 0;
                }
            }
            if (!acceptShorterOutput && totalRead < length) throw new ArgumentOutOfRangeException($"Source stream could not provide ({length}) bytes of data");
            return totalWrite;
        }, false);
    }

    /// <summary>Reads the bytes from the current stream and writes them to another stream, using a specified buffer size.</summary>
    /// <param name="source">Source Stream</param>
    /// <param name="destination">The stream to which the contents of the current stream will be copied.</param>
    /// <param name="length">
    ///     Length of contents to be copied (exception is thrown if source buffer does not have enough
    ///     elements)
    /// </param>
    /// <param name="bufferSize">The size of the buffer. This value must be greater than zero. The default size is 4096</param>
    /// <param name="action">Action to be performed while copying for each buffer</param>
    /// <param name="timeout">Timeout in msecs</param>
    /// <param name="acceptShorterOutput">if data read is less than the length parameter accept this as valid or not</param>
    public static async Task<long> CopyToAsync(this Stream source, Stream destination, Func<ReadOnlyMemory<byte>, Stream, Task<long>>? action, long length = -1, int bufferSize = 81920, int timeout = -1, bool acceptShorterOutput = false) {
        bufferSize = (int)(length >= 0 && length < bufferSize ? length : bufferSize);
        return await ArrayPool<byte>.Shared.RentMemDo(bufferSize, async (buffer) => {
            var totalRead = 0L;
            var totalWrite = 0L;
            var bufferRead = 0;
            int read = 1;

            while (read > 0) {
                var lenToRead = bufferSize - bufferRead;
                if (length >= 0 && lenToRead > length - totalRead) lenToRead = (int)(length - totalRead); //dont read more than len if len >= 0
                read = await source.ReadAsync(buffer.Slice(bufferRead, lenToRead)).TimeoutAfter(timeout).ConfigureAwait(false);
                bufferRead += read;
                totalRead += read;
                if (read == 0) break;
                if (bufferRead == bufferSize || (read == 0 && bufferRead != 0) || totalRead == length) {
                    if (action != null) {
                        var writtenLen = await action(buffer[..bufferRead], destination).TimeoutAfter(timeout).ConfigureAwait(false);
                        totalWrite += writtenLen;
                    } else {
                        await destination.WriteAsync(buffer[..bufferRead]).TimeoutAfter(timeout).ConfigureAwait(false);
                        totalWrite += bufferRead;
                    }

                    bufferRead = 0;
                }
            }
            if (!acceptShorterOutput && totalRead < length) throw new ArgumentOutOfRangeException($"Source stream could not provide ({length}) bytes of data");
            return totalWrite;
        }, false);
    }
    #endregion

    #region Nested type: DeserializePortion
    /// <summary>Multi-method library for deserialization.</summary>
    public class DeserializePortion: ExtendablePortion<Stream> {
        #region Constructors
        /// <summary>Constructor</summary>
        public DeserializePortion(Stream value) : base(value) { }
        #endregion

        #region BoisBinary
        /// <summary>Deserialize stream into an object using Salar Bois Binary Serializer.</summary>
        public object? BoisBinary(Type objectType, ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            if (objectType == null) return null;
            var rval = objectType.GetDefault();
            if (Value == null) return rval;
            var bf = new ZBoisSerializer(opts);
            return bf.Deserialize(Value, objectType);
        }
        /// <summary>Deserialize stream into an object using Salar Bois Binary Serializer.</summary>
        public T? BoisBinary<T>(ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            if (Value == null) return default;
            var bf = new ZBoisSerializer(opts);
            return bf.Deserialize<T>(Value);
        }
        /// <summary>Deserialize stream into an object using Salar Bois Binary Serializer.</summary>
        public object? BoisBinary(Type objectType, int len, ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            var rval = objectType.GetDefault();
            if (Value == null) return rval;
            var b = new byte[len];
            Value.Read(b, 0, len);
            var bf = new ZBoisSerializer(opts);
            return bf.Deserialize(b, objectType, 0, b.Length);
        }
        #endregion

        #region DotNetJson
        /// <summary>Deserialize stream into an object using .NET Xml Serializer.</summary>
        public T? DotNetJson<T>(bool includeFields = false) => (T?)DotNetJson(typeof(T), includeFields);
        /// <summary>Deserialize stream into an object using .NET Xml Serializer.</summary>
        public object? DotNetJson(Type objectType, bool includeFields = false) {
            if (Value == null) return null;
            return System.Text.Json.JsonSerializer.Deserialize(Value, objectType, new System.Text.Json.JsonSerializerOptions { IncludeFields = includeFields });
        }
        #endregion DotNetJson

        #region DotNetBinary
        /// <summary>Deserialize stream into an object using .NET Binary Serializer.</summary>
        public T? DotNetBinary<T>() => (T?)DotNetBinary(typeof(T));
#pragma warning disable SYSLIB0011
        /// <summary>Deserialize stream into an object using .NET Binary Serializer.</summary>
        public object? DotNetBinary(Type objectType) {
            var rval = objectType.GetDefault();
            if (Value == null) return rval;
            var bf = new BinaryFormatter();
            return bf.Deserialize(Value);
        }
#pragma warning restore
        #endregion DotNetBinary

        #region DotNetXml
        /// <summary>Deserialize stream into an object using .NET Xml Serializer.</summary>
        public T? DotNetXml<T>() => (T?)DotNetXml(typeof(T));
        /// <summary>Deserialize stream into an object using .NET Xml Serializer.</summary>
        public object? DotNetXml(Type objectType) {
            var rval = objectType.GetDefault();
            if (Value == null) return rval;
            //if (!objectType.IsSerializable) throw new ArgumentException($"Type is not Serializable: {objectType.Name}", nameof(objectType));
            var serializer = new XmlSerializer(objectType);
            return serializer.Deserialize(Value);
        }
        #endregion DotNetXml
    }
    #endregion

    #region Disabled Extensions
    ///// <summary>
    ///// 	Writes all passed bytes to the specified stream.
    ///// </summary>
    ///// <param name = "stream">The stream.</param>
    ///// <param name = "bytes">The byte array / buffer.</param>
    //public static void Write(this Stream stream, byte[] bytes)
    //{
    //    stream.Write(bytes, 0, bytes.Length);
    //}
    ///// <summary>
    ///// 	Reads all text from the stream using the default encoding.
    ///// </summary>
    ///// <param name = "stream">The stream.</param>
    ///// <returns>The result string.</returns>
    //public static string ReadToEnd(this Stream stream)
    //{
    //    return stream.ReadToEnd(null);
    //}

    ///// <summary>
    ///// 	Reads all text from the stream using a specified encoding.
    ///// </summary>
    ///// <param name = "stream">The stream.</param>
    ///// <param name = "encoding">The encoding.</param>
    ///// <returns>The result string.</returns>
    //public static string ReadToEnd(this Stream stream, Encoding encoding)
    //{
    //    using (var reader = stream.GetStreamReader(encoding))
    //        return reader.ReadToEnd();
    //}

    ///// <summary>
    ///// 	Sets the stream cursor to the beginning of the stream.
    ///// </summary>
    ///// <param name = "stream">The stream.</param>
    ///// <returns>The stream</returns>
    //public static Stream SeekToBegin(this Stream stream)
    //{
    //    if (stream.CanSeek == false)
    //        throw new InvalidOperationException("Stream does not support seeking.");

    //    stream.Seek(0, SeekOrigin.Begin);
    //    return stream;
    //}

    ///// <summary>
    ///// 	Sets the stream cursor to the end of the stream.
    ///// </summary>
    ///// <param name = "stream">The stream.</param>
    ///// <returns>The stream</returns>
    //public static Stream SeekToEnd(this Stream stream)
    //{
    //    if (stream.CanSeek == false)
    //        throw new InvalidOperationException("Stream does not support seeking.");

    //    stream.Seek(0, SeekOrigin.End);
    //    return stream;
    //}

    ///// <summary>
    ///// 	Copies one stream into a another one.
    ///// </summary>
    ///// <param name = "stream">The source stream.</param>
    ///// <param name = "targetStream">The target stream.</param>
    ///// <param name = "bufferSize">The buffer size used to read / write.</param>
    ///// <returns>The source stream.</returns>
    //public static Stream CopyTo(this Stream stream, Stream targetStream, int bufferSize = 4096)
    //{
    //    if (stream.CanRead == false)
    //        throw new InvalidOperationException("Source stream does not support reading.");
    //    if (targetStream.CanWrite == false)
    //        throw new InvalidOperationException("Target stream does not support writing.");

    //    var buffer = new byte[bufferSize];
    //    int bytesRead;

    //    while ((bytesRead = stream.Read(buffer, 0, bufferSize)) > 0)
    //        targetStream.Write(buffer, 0, bytesRead);
    //    return stream;
    //}

    ///// <summary>
    ///// 	Copies any stream into a local MemoryStream
    ///// </summary>
    ///// <param name = "stream">The source stream.</param>
    ///// <returns>The copied memory stream.</returns>
    //public static MemoryStream CopyToMemory(this Stream stream)
    //{
    //    var memoryStream = new MemoryStream((int)stream.Length);
    //    stream.CopyTo(memoryStream);
    //    return memoryStream;
    //}

    ///// <summary>
    ///// 	Reads a fixed number of bytes.
    ///// </summary>
    ///// <param name = "stream">The stream to read from</param>
    ///// <param name = "bufsize">The number of bytes to read.</param>
    ///// <returns>the read byte[]</returns>
    //public static byte[] ReadFixedBuffersize(this Stream stream, int bufsize)
    //{
    //    var buf = new byte[bufsize];
    //    int offset = 0, cnt;
    //    do {
    //            cnt = stream.Read(buf, offset, bufsize - offset);
    //        if (cnt == 0)
    //            return null;
    //        offset += cnt;
    //    } while (offset < bufsize);

    //    return buf;
    //}
    #endregion
}