﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
namespace Z.Extensions;
/// <summary>Extension methods for the TextReader class</summary>
public static class ExtTextReader
{
    /// <summary>The method provides an iterator through all lines of the text reader.</summary>
    /// <param name="reader">The text reader.</param>
    /// <returns>The iterator</returns>
    /// <example>
    ///     <code>
    /// 		using(var reader = fileInfo.OpenText()) {
    /// 		foreach(var line in reader.IterateLines()) {
    /// 		// ...
    /// 		}
    /// 		}
    /// 	</code>
    /// </example>
    /// <remarks>Contributed by OlivierJ</remarks>
    public static IEnumerable<string> IterateLines(this TextReader reader) {
        string? line;
        while ((line = reader.ReadLine()) != null) yield return line;
    }

    /// <summary>The method executes the passed delegate /lambda expression) for all lines of the text reader.</summary>
    /// <param name="reader">The text reader.</param>
    /// <param name="action">The action.</param>
    /// <example>
    ///     <code>
    /// 		using(var reader = fileInfo.OpenText()) {
    /// 		reader.IterateLines(l => Console.WriteLine(l));
    /// 		}
    /// 	</code>
    /// </example>
    /// <remarks>Contributed by OlivierJ</remarks>
    public static void IterateLines(this TextReader reader, Action<string> action) {
        foreach (var line in reader.IterateLines()) action(line);
    }
}