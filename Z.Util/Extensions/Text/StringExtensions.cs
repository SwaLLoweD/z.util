﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Z.Util;
using Z.Util.Inflector;

namespace Z.Extensions;

/// <summary>Extension methods for string values...</summary>
public static class ExtString {
    #region Constants / Static Fields
    private static readonly HashSet<string> webImageMIME = new() { "image/jpg", "image/jpeg", "image/pjpeg", "image/gif", "image/png" };
    #endregion

    /// <summary>Converts the specified string to a different type.</summary>
    /// <param name = "value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion To(this string value) => new(value);

    /// <summary>Checks the specified string for different conditions.</summary>
    /// <param name = "value">The value to be checked.</param>
    /// <returns>An universal checker supplying additional target checking methods</returns>
    public static IsPortion Is(this string value) => new(value);

    /// <summary>Performs specified file-system path commands on the provided string.</summary>
    /// <param name = "value">The file-system path value to be manipulated.</param>
    /// <returns>An universal method collection supplying additional path related methods</returns>
    public static StringPathPortion Path(this string value) => new(value);

    /// <summary>Transform the specified string.</summary>
    /// <param name = "value">The value to be transformed.</param>
    /// <returns>An universal transformer supplying additional transforming methods</returns>
    public static TransformPortion Transform(this string value) => new(value);

    /// <summary>Deserialize String into Object</summary>
    /// <param name = "value">The string to be Deserialized.</param>
    /// <returns>An universal deserialize helper supplying additional deserialization methods</returns>
    public static DeserializePortion Deserialize(this string value) => new(value);

    /// <summary>Performs specified selective string removal commands on the provided string.</summary>
    /// <param name = "value">The value to be manipulated.</param>
    /// <returns>An universal method collection supplying additional string removal methods</returns>
    public static RemovePortion Remove(this string value) => new(value);

    /// <summary>Convert null to empty string</summary>
    public static string Emptify(this string? value) => value ?? "";

    ///// <summary>Determines whether the comparison value strig is contained within the input value string</summary>
    ///// <param name = "value">The input value.</param>
    ///// <param name = "comparisonValue">The comparison value.</param>
    ///// <param name = "comparisonType">Type of the comparison to allow case sensitive or insensitive comparison.</param>
    ///// <returns>
    ///// 	<c>true</c> if input value contains the specified value, otherwise, <c>false</c>.
    ///// </returns>
    //public static bool Contains(this string value, string comparisonValue, StringComparison comparisonType) => (value.IndexOf(comparisonValue, comparisonType) != -1);

    #region Nested type: ConvertPortion
    /// <summary>Multi-method library for string conversion related commands.</summary>
    public class ConvertPortion: ExtIEnumerable.ConvertPortion<char>, IConvertPortion<IEnumerable<char>> {
        #region Fields / Properties
        /// <summary>Convert to byte[]</summary>
        public ByteArrayConverter ByteArray => new(ValueStr);
        private string ValueStr => (string)Value ?? throw new FormatException("Value is not a valid string");
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public ConvertPortion(string value) : base(value) { }
        #endregion

        /// <inheritdoc/>
        public new virtual U? Type<U>(U? defaultValue = default, IFormatProvider? format = null) {
            try {
                return (U?)Type(typeof(U?), defaultValue, format);
            } catch {
                return defaultValue;
            }
        }

        #region String to Enum
        /// <summary>Parse a string to a enum item if that string exists in the enum otherwise return the default enum item.</summary>
        /// <typeparam name="TEnum">The Enum type.</typeparam>
        /// <param name="value">String to be converted</param>
        /// <param name="defaultValue">Default value to set if conversion fails</param>
        /// <param name="ignoreCase">Whether the enum parser will ignore the given data's case or not.</param>
        /// <returns>Converted enum.</returns>
        /// <remarks>Contributed by Mohammad Rahman, http://mohammad-rahman.blogspot.com/ </remarks>
        public TEnum Enum<TEnum>(string value, TEnum defaultValue = default, bool ignoreCase = true) where TEnum : struct => Fnc.EnumContains<TEnum>(ValueStr) ? (TEnum)System.Enum.Parse(typeof(TEnum), value, ignoreCase) : defaultValue;
        #endregion String to Enum

        #region SecureString
        /// <summary>Converts a regular string into SecureString</summary>
        /// <param name="makeReadOnly">Makes the text value of this secure string read-only.</param>
        /// <returns>Returns a SecureString containing the value of a transformed object. </returns>
        public SecureString SecureString(bool makeReadOnly = false) {
            var s = new SecureString();
            foreach (var c in ValueStr)
                s.AppendChar(c);
            if (makeReadOnly) s.MakeReadOnly();
            return s;
        }
        #endregion

        #region List (Csv)
        /// <summary>Convert Csv to Collection (Add if existing collection is given)</summary>
        public IEnumerable<TList> Collection<TList>(string seperator = ",", bool trimValues = true, bool removeEmptyEntries = true) {
            if (ValueStr.Is().NotEmpty) {
                var lstValues = seperator.Split(new[] { seperator }, removeEmptyEntries ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
                foreach (var lstValue in lstValues) {
                    if (removeEmptyEntries && lstValue.Is().Empty) continue;
                    var item = (trimValues ? lstValue.Trim() : lstValue).To().Type<TList>();
                    if (item != null) yield return item;
                }
            }
        }
        #endregion List (Csv)

        #region Nested type: ByteArrayConverter
        /// <summary>Multi-method library for byte[] convertion related commands.</summary>
        public class ByteArrayConverter: ExtendablePortion<string> {
            #region Fields / Properties
            /// <summary>Convert to byte[] using ASCII encoding</summary>
            public byte[] AsAscii => Encoding.ASCII.GetBytes(Value);

            /// <summary>Convert to byte[] using Base32 encoding</summary>
            public byte[] AsBase32 => Base32Url.FromBase32String(Value);

            /// <summary>Convert to byte[] using Base36 encoding</summary>
            public byte[] AsBase36 => BaseN.FromBaseN(Value, BaseN.Base36Alphabet);

            /// <summary>Convert to byte[] using Base64 encoding</summary>
            public byte[] AsBase64 => Convert.FromBase64String(Value);

            /// <summary>Convert to byte[] using Hex encoding</summary>
            public byte[] AsHex {
                get {
                    var returnBytes = new byte[Value.Length / 2];
                    for (var i = 0; i < returnBytes.Length; i++)
                        returnBytes[i] = Convert.ToByte(Value.Substring(i * 2, 2), 16);
                    return returnBytes;
                }
            }

            /// <summary>Convert to byte[] using UTF-8 encoding</summary>
            public byte[] AsUtf8 => Encoding.UTF8.GetBytes(Value);
            #endregion

            #region Constructors
            /// <summary>Constructros</summary>
            public ByteArrayConverter(string value) : base(value) { }
            #endregion

            /// <summary>Convert to byte[] using specified alphabet</summary>
            public byte[] AsBaseN(string alphabet) => BaseN.FromBaseN(Value, alphabet);
        }
        #endregion

        #region XML / HTML
        /// <summary>Convert line-breaks and spaces to html format tags</summary>
        /// <returns>Html formatted string</returns>
        public string Html {
            get {
                return ValueStr.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;");
            }
        }

        /// <summary>Convert line-breaks and spaces to normal text format from html tags</summary>
        /// <returns>Text formatted string</returns>
        public string FromHtml => ValueStr.Replace("<br/>", "\r\n")
                    .Replace("<br>", "\r\n")
                    .Replace("</p>", "\r\n")
                    .Replace("<p>", "")
                    .Replace("&nbsp;&nbsp;", "  ");

        /// <summary>Loads the string into a XML DOM object (XmlDocument)</summary>
        /// <returns>The XML document object model (XmlDocument)</returns>
        public XmlDocument XmlDOM {
            get {
                var document = new XmlDocument();
                document.LoadXml(ValueStr);
                return document;
            }
        }

        /// <summary>Loads the string into a XML XPath DOM (XPathDocument)</summary>
        /// <returns>The XML XPath document object model (XPathNavigator)</returns>
        public XPathNavigator XPath => new XPathDocument(new StringReader(ValueStr)).CreateNavigator();

        /// <summary>Loads the string into a LINQ to XML XDocument</summary>
        /// <returns>The XML document object model (XDocument)</returns>
        public XDocument XDocument => XDocument.Parse(ValueStr);

        /// <summary>Loads the string into a LINQ to XML XElement</summary>
        /// <returns>The XML element object model (XElement)</returns>
        public XElement XElement => XElement.Parse(ValueStr);
        #endregion XML / HTML
    }
    #endregion

    #region Nested type: DeserializePortion
    /// <summary>Multi-method library for deserialization.</summary>
    public class DeserializePortion: ExtendablePortion<string?> {
        #region Constructors
        /// <summary>Constructor</summary>
        public DeserializePortion(string? value) : base(value) { }
        #endregion

        #region ZXml
        /// <summary>Custom Deserialize from XML</summary>
        /// <param name="o"></param>
        /// <param name="parsedProperties">Properties that have been found and filled</param>
        public T? ZXml<T>(T? o = default, ISet<ZMemberInfo>? parsedProperties = null) {
            if (Value == null) return default;
            var xml = new XmlDocument();
            xml.LoadXml(Value);
            return Fnc.DeserializeXml2(o, xml, parsedProperties);
        }
        #endregion ZXml

        #region DotNetJson
        /// <summary>Deserialize string into an object using .NET Xml Serializer.</summary>
        public T? DotNetJson<T>(bool includeFields = false) => (T?)DotNetJson(typeof(T), includeFields);
        /// <summary>Deserialize string into an object using .NET Xml Serializer.</summary>
        public object? DotNetJson(Type objectType, bool includeFields = false) {
            if (Value == null) return null;
            return System.Text.Json.JsonSerializer.Deserialize(Value, objectType, new System.Text.Json.JsonSerializerOptions { IncludeFields = includeFields });
        }
        #endregion DotNetJson

        #region DotNetXml
        /// <summary>Deserialize string into an object using .NET Xml Serializer.</summary>
        public T? DotNetXml<T>() => (T?)DotNetXml(typeof(T));
        /// <summary>Deserialize string into an object using .NET Xml Serializer.</summary>
        public object? DotNetXml(Type objectType) {
            if (Value == null) return default;
            var rval = objectType.GetDefault();
            if (Value.Is().Empty) return rval;
            using (var m = new MemoryStream(Value.To().ByteArray.AsUtf8 ?? Array.Empty<byte>())) {
                rval = m.Deserialize().DotNetXml(objectType);
            }
            return rval;
        }
        #endregion DotNetXml
    }
    #endregion

    #region Nested type: IsPortion
    /// <summary>Multi-method library for string condition check related commands.</summary>
    public class IsPortion: ExtIEnumerable.IsPortion<char>, IIsPortion<IEnumerable<char>> {
        #region Fields / Properties
        /// <summary>Determines whether the specified string is a valid IP Address</summary>
        public bool IpAddress {
            get {
                return IPAddress.TryParse(ValueStr, out _);
            }
        }

        /// <summary>Checks if mimetype is a web image type.</summary>
        public bool MIMEImageWeb => ValueStr != null && webImageMIME.Contains(ValueStr.Trim());
        /// <summary>Determines whether the specified string is a valid RomanNumeral</summary>
        public bool RomanNumeral => ValueStr != null && Util.Mathematics.RomanNumeral.IsValidRomanNumeral(ValueStr);
        /// <summary>Determines whether the specified string is a valid url</summary>
        public bool Url {
            get {
                if (ValueStr?.Is().Empty != false) return false;
                const string strRegex = "^(https?://)"
                               + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //user@
                               + @"(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP- 199.194.52.184
                               + "|" // allows either IP or domain
                               + @"([0-9a-z_!~*'()-]+\.)*" // tertiary domain(s)- www.
                               + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]" // second level domain
                               + @"(\.[a-z]{2,6})?)" // first level domain- .com or .museum is optional
                               + "(:[0-9]{1,5})?" // port number- :80
                               + "((/?)|" // a slash isn't required if there is no file name
                               + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
                return new Regex(strRegex).IsMatch(ValueStr);
            }
        }
        private string ValueStr => (string)Value ?? throw new FormatException("Value is not a valid string");
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public IsPortion(string value) : base(value) { }
        #endregion

        /// <summary>
        /// Indicates whether the current string matches the supplied wildcard pattern.  Behaves the same
        /// as VB's "Like" Operator.
        /// </summary>
        /// <param name="wildcardPattern">The wildcard pattern to match.  Syntax matches VB's Like operator.</param>
        /// <returns>true if the string matches the supplied pattern, false otherwise.</returns>
        /// <remarks>See http://msdn.microsoft.com/en-us/library/swf8kaxw(v=VS.100).aspx</remarks>
        public bool Like(string wildcardPattern) {
            if (ValueStr == null || string.IsNullOrEmpty(wildcardPattern)) return false;
            // turn into regex pattern, and match the whole string with ^$
            var regexPattern = "^" + Regex.Escape(wildcardPattern) + "$";

            // add support for ?, #, *, [], and [!]
            regexPattern = regexPattern.Replace(@"\[!", "[^")
                .Replace(@"\[", "[")
                .Replace(@"\]", "]")
                .Replace(@"\?", ".")
                .Replace(@"\*", ".*")
                .Replace(@"\#", @"\d");

            bool result;
            try {
                result = Regex.IsMatch(ValueStr, regexPattern);
            } catch (ArgumentException ex) {
                throw new ArgumentException(string.Format("Invalid pattern: {0}", wildcardPattern), ex);
            }
            return result;
        }

        /// <summary>Determines whether the specified string can be converted to numeric form</summary>
        public bool Number(bool floatpoint = false, IFormatProvider? format = null) {
            var withoutWhiteSpace = ValueStr.Remove().Spaces();
            if (floatpoint) {
                return double.TryParse(withoutWhiteSpace, NumberStyles.Any,
                    format ?? CultureInfo.CurrentUICulture, out _);
            }

            return int.TryParse(withoutWhiteSpace, out _);
        }

        /// <summary>Determines whether the specified string consists of only numeric characters</summary>
        public bool NumberOnly(bool floatpoint = false) {
            var value = ValueStr.Trim();
            if (value.Length == 0)
                return false;
            foreach (var c in value) {
                if (!char.IsDigit(c)) {
                    if (floatpoint && (c == '.' || c == ','))
                        continue;
                    return false;
                }
            }

            return true;
        }

        /// <summary>Determines whether the specified string is a valid e-mail</summary>
        public bool Email() => new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$").IsMatch(ValueStr);
        /// <summary>Determines whether the specified string is a valid TC Kimlik No</summary>
        public bool TCKN() {
            var odd = 0;
            var even = 0;
            var tenStep = 0;

            if (ValueStr?.Is().NumberOnly() != true) return false;

            var csnParse = ValueStr.Select(x => (int)x).ToArray();
            if (csnParse[0] == 0) return false;
            for (var i = 0; i < csnParse.Length; i++) {
                if (i <= 8) {
                    if (i % 2 == 0) even += csnParse[i];
                    else odd += csnParse[i];
                }
                if (i <= 9) tenStep += csnParse[i];
            }

            if (((even * 7) - odd) % 10 != csnParse[9]) return false;
            if (tenStep % 10 != csnParse[10]) return false;
            return true;
        }
        /// <summary>Uses regular expressions to determine if the string matches to a given regex pattern.</summary>
        /// <param name = "regexPattern">The regular expression pattern.</param>
        /// <param name = "options">The regular expression options.</param>
        /// <returns>
        /// 	<c>true</c> if the value is matching to the specified pattern; otherwise, <c>false</c>.
        /// </returns>
        /// <example>
        /// 	<code>
        /// 		var s = "12345";
        /// 		var isMatching = s.IsMatchingTo(@"^\d+$");
        /// 	</code>
        /// </example>
        public bool MatchingTo(string regexPattern, RegexOptions options = RegexOptions.None) => ValueStr != null && Regex.IsMatch(ValueStr, regexPattern, options);
    }
    #endregion

    #region Nested type: RemovePortion
    /// <summary>Multi-method library for string removal related commands.</summary>
    public class RemovePortion: ExtendablePortion<string> {
        #region Constructors
        /// <summary>Constructor</summary>
        public RemovePortion(string value) : base(value) { }
        #endregion

        /// <summary>Remove specific characters from string</summary>
        /// <param name="removeCharc">Characters to remove</param>
        public string Chars(params char[] removeCharc) {
            var result = Value;
            if (!string.IsNullOrEmpty(result) && removeCharc != null) {
                foreach (var c in removeCharc)
                    result = Strings(c.ToString());
            }

            return result;
        }

        /// <summary>Remove specific string pieces from string</summary>
        /// <param name="strings">String pieces to remove</param>
        public string Strings(params string[] strings) => strings.Aggregate(Value, (current, c) => current.Replace(c, string.Empty));
        //var result = value;
        //if (!string.IsNullOrEmpty(result) && removeStrings != null)
        //  Array.ForEach(removeStrings, s => result = result.Replace(s, string.Empty));
        //return result;
        /// <summary>Remove spaces from string</summary>
        public string Spaces() => Value.Replace(" ", "");

        /// <summary>Remove diacritics from string</summary>
        public string DiacriticsAndSpaces() {
            var normalized = Value.Normalize(NormalizationForm.FormD).Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark);
            var r = string.Join(string.Empty, normalized).Normalize(NormalizationForm.FormC);
            return r;
        }

        /// <summary>Remove everything except alphanumeric(english) characters from string</summary>
        public string SpecialCharacters() {
            var sb = new StringBuilder();
            foreach (var c in Value.Where(c => (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')))
                sb.Append(c);
            return sb.ToString();
        }

        /// <summary>Remove HTML tags from string</summary>
        /// <param name="acceptableTags">tags to keep</param>
        public string HtmlTags(params string[] acceptableTags) {
            var acceptable = acceptableTags.ToString(null, null, null, "|"); //"script|link|title";
            var stringPattern = "</?(?(?=" + acceptable + @")notag|[a-zA-Z0-9]+)(?:\s[a-zA-Z0-9\-]+=?(?:([""']?).*?\1?)?)*\s*/?>";
            return Regex.Replace(Value, stringPattern, "sausage");
        }
        /// <summary>Remove HTML tags from string using char array.</summary>
        public string HtmlTags() {
            var array = new char[Value.Length];
            var arrayIndex = 0;
            var inside = false;

            for (var i = 0; i < Value.Length; i++) {
                var let = Value[i];
                if (let == '<') {
                    inside = true;
                    continue;
                }
                if (let == '>') {
                    inside = false;
                    continue;
                }
                if (!inside) {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }
    #endregion

    #region Nested type: StringPathPortion
    /// <summary>Multi-method library for file-system path related commands.</summary>
    public class StringPathPortion: ExtendablePortion<string> {
        #region Fields / Properties
        /// <summary>Equivalent of Path.GetDirectoryName(Value)</summary>
        /// <returns>the directory information for the specified path</returns>
        public string? Directory => System.IO.Path.GetDirectoryName(Value);
        /// <summary>Equivalent of Path.GetExtension(Value)</summary>
        /// <returns>the file extension information for the specified path</returns>
        public string Extension => System.IO.Path.GetExtension(Value);
        /// <summary>Equivalent of Path.GetFileName(Value)</summary>
        /// <returns>the filename information for the specified path</returns>
        public string Filename => System.IO.Path.GetFileName(Value);
        /// <summary>Equivalent of Path.GetFullPath(value)</summary>
        /// <returns>The full filesystem path of current value</returns>
        public string FullPath => System.IO.Path.GetFullPath(Value);
        /// <summary>Get every part of the current path string</summary>
        /// <returns>every directory and file name contained in the path string in order</returns>
        public IList<string> PartArray => Value.Split(new[] { System.IO.Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries).ToList();
        #endregion

        #region Constructors
        /// <summary>Constructors</summary>
        public StringPathPortion(string value) : base(value) { }
        #endregion

        /// <summary>Implicit converter</summary>
        public static implicit operator string(StringPathPortion sp) => sp.ToString();
        /// <summary>Implicit converter</summary>
        public static explicit operator StringPathPortion(string s) => new(s);

        /// <summary>Combines current string with a rooted value.</summary>
        /// <param name = "root">Rooted path to combine to.</param>
        /// <returns>The combination result</returns>
        public StringPathPortion Combine(string root) {
            if (System.IO.Path.IsPathRooted(Value)) return this;
            Value = System.IO.Path.Combine(root, Value);
            return this;
        }

        /// <summary>Equivalent of FileSystem.Combine( [Union of path, parts] )</summary>
        /// <param name="parts"></param>
        /// <returns></returns>
        public StringPathPortion Append(params string[] parts) {
            var list = Value == null ? new List<string>() : new List<string> { Value };

            list.Add(parts);
            Value = System.IO.Path.Combine(list.ToArray());
            return this;
        }
        /// <summary>Gets full path of current virtual path relative to specified root</summary>
        /// <param name="root">Rooted path to start virtual path searching from</param>
        /// <returns>Full Path</returns>
        public StringPathPortion RelativeTo(string root) {
            var pathParts = Value.Path().PartArray;
            var rootParts = root.Path().PartArray;

            var length = pathParts.Count > rootParts.Count ? rootParts.Count : pathParts.Count;
            for (var i = 0; i < length; i++) {
                if (pathParts[0] == rootParts[0]) {
                    pathParts.RemoveAt(0);
                    rootParts.RemoveAt(0);
                } else {
                    break;
                }
            }

            for (var i = 0; i < rootParts.Count; i++)
                pathParts.Insert(0, "..");

            Value = pathParts.Count > 0 ? System.IO.Path.Combine(pathParts.ToArray()) : string.Empty;
            return this;
        }
        /// <summary>Replaces all invalid characters with _</summary>
        public StringPathPortion Sanitize() {
            var rval = Value;
            foreach (var c in System.IO.Path.GetInvalidFileNameChars()) rval = rval.Replace(c, '_');
            Value = rval;
            return this;
        }
        /// <summary>Get path as string</summary>
        public override string ToString() => Value;
    }
    #endregion

    #region Nested type: TransformPortion
    /// <summary>Multi-method library for string transformation related commands.</summary>
    public class TransformPortion: ExtIEnumerable.TransformPortion<char> {
        #region Fields / Properties
        /// <summary>Value as string</summary>
        public string ValueStr => (string)Value ?? throw new FormatException("Value is not a valid string");
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public TransformPortion(string value) : base(value) { }
        #endregion

        /// <summary>Implicit converter</summary>
        public static implicit operator string(TransformPortion sp) => sp.ToString();
        /// <summary>Implicit converter</summary>
        public static explicit operator TransformPortion(string s) => new(s);

        /// <summary>Titlecase a string using current culture.</summary>
        /// <returns>Capitalized string</returns>
        public TransformPortion Capitalize() {
            Value = SysInf.CurrentFormat.TextInfo.ToTitleCase(ValueStr);
            return this;
        }

        /// <summary>Inserts a space after every capital letter.</summary>
        /// <returns></returns>
        ///
        public TransformPortion SpaceOnUpper() {
            Value = Regex.Replace(ValueStr, "([A-Z])(?=[a-z])|(?<=[a-z])([A-Z]|[0-9]+)", " $1$2").TrimStart();
            return this;
        }

        /// <summary>Extracts all digits from a string.</summary>
        /// <returns>All digits contained within the input string</returns>
        /// <remarks>Contributed by Kenneth Scott</remarks>
        public TransformPortion ExtractDigits() {
            Value = string.Join(null, Regex.Split(ValueStr, "[^\\d]"));
            return this;
        }

        /// <summary>Encode base64.</summary>
        /// <returns>Base64 encoded string</returns>
        public TransformPortion EncodeBase64() {
            Value = ValueStr.To().ByteArray.AsAscii.To().String.AsBase64;
            return this;
        }

        /// <summary>Dencode base64.</summary>
        /// <returns>Base64 decoded string</returns>
        public TransformPortion DecodeBase64() {
            Value = ValueStr.To().ByteArray.AsBase64.To().String.AsAscii;
            return this;
        }

        /// <summary>Extracts a valid hostname from a url/other string.</summary>
        public string ExtractHost() {
            var rval = ValueStr;
            if (ValueStr.Contains("//")) rval = ValueStr.SubstringBetween("//");
            var firstSlash = rval.IndexOf('/');
            if (firstSlash > 0) rval = rval.Left(firstSlash);
            return rval;
        }
        /// <summary>Scramble the enumerable using specified key.</summary>
        public new string Scramble(int? key = null) => new(base.Scramble(key));

        /// <summary>Unscramble the enumerable using specified key.</summary>
        public new string Unscramble(int? key = null) => new(base.Unscramble(key));

        /// <summary>Convert to titlized style.</summary>
        /// <returns>titlized word</returns>
        /// <example>the man who sold the world => The Man Who Sold The World</example>
        public TransformPortion Titleize() {
            Value = Inflector.Titleize(ValueStr);
            return this;
        }

        /// <summary>Capitalizes the first word and turns underscores into spaces and strips a trailing “_id”, if any. Like titleize, this is meant for creating pretty output</summary>
        /// <example>employee_salary_id => Employee salary</example>
        public TransformPortion Humanize() {
            Value = Inflector.Humanize(ValueStr);
            return this;
        }

        /// <summary>Convert to pascalized form.</summary>
        public TransformPortion Pascalize() {
            Value = Inflector.Pascalize(ValueStr);
            return this;
        }

        /// <summary>Convert to camel-case form.</summary>
        /// <example>active_model => ActiveModel</example>
        public TransformPortion Camelize() {
            Value = Inflector.Camelize(ValueStr);
            return this;
        }

        /// <summary>Returns an underscore-syntaxed ($like_this_dear_reader) version of the CamelCasedWord.</summary>
        /// <example>ActiveModel => active_model</example>
        public TransformPortion Underscore() {
            Value = Inflector.Underscore(ValueStr);
            return this;
        }

        /// <summary>Uncapitalize</summary>
        public TransformPortion Uncapitalize() {
            Value = Inflector.Uncapitalize(ValueStr);
            return this;
        }

        /// <summary>Replaces underscores with dashes in the string.</summary>
        /// <example>active_model => active-model</example>
        public TransformPortion Dasherize() {
            Value = Inflector.Dasherize(ValueStr);
            return this;
        }
        /// <summary>Reverses / mirrors a string.</summary>
        public TransformPortion Reverse() {
            if (Value.Is().Empty || ValueStr.Length == 1)
                return this;

            var chars = Value.ToArray();
            Array.Reverse(chars);
            Value = new string(chars);
            return this;
        }

        /// <summary>Get string</summary>
        public override string ToString() => ValueStr;
    }
    #endregion

    #region String Manipulation
    /// <summary>Alternative System.string.Split() that uses RemoveEmptyEntries as default SplitOption</summary>
    /// <param name = "value">String value to be split</param>
    /// <param name = "seperator">The separator that appears between each element</param>
    /// <returns>The splitted string array.</returns>
    public static string[] Split(this string value, params string[] seperator) => value.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
    /// <summary>Trims the text to a provided maximum length and adds a suffix to the end if cut off</summary>
    /// <param name = "value">The input string.</param>
    /// <param name = "maxLength">Maximum length.</param>
    /// <param name = "suffix">The suffix.</param>
    /// <returns></returns>
    /// <remarks>Proposed by Rene Schulte</remarks>
    public static string Trim(this string value, int maxLength, string? suffix = null) {
        if (value.Length > maxLength) return string.Concat(value.AsSpan(0, maxLength), suffix);
        return value;
    }
    /// <summary>Ensures that a string starts with a given prefix.</summary>
    /// <param name = "value">The string value to check.</param>
    /// <param name = "prefix">The prefix value to check for.</param>
    /// <returns>The string value including the prefix</returns>
    /// <example>
    /// 	<code>
    /// 		var extension = "txt";
    /// 		var fileName = string.Concat(file.Name, extension.EnsureStartsWith("."));
    /// 	</code>
    /// </example>
    public static string EnsureStartsWith(this string value, string prefix) => value.StartsWith(prefix) ? value : string.Concat(prefix, value);

    /// <summary>Ensures that a string ends with a given suffix.</summary>
    /// <param name = "value">The string value to check.</param>
    /// <param name = "suffix">The suffix value to check for.</param>
    /// <returns>The string value including the suffix</returns>
    /// <example>
    /// 	<code>
    /// 		var url = "http://www.pgk.de";
    /// 		url = url.EnsureEndsWith("/"));
    /// 	</code>
    /// </example>
    public static string EnsureEndsWith(this string value, string suffix) => value.EndsWith(suffix) ? value : string.Concat(value, suffix);

    /// <summary>
    /// Centers a charters in this string, padding in both, left and right, by specified Unicode character,
    /// for a specified total lenght.
    /// </summary>
    /// <param name="value">Instance value.</param>
    /// <param name="width">The number of characters in the resulting string,
    /// equal to the number of original characters plus any additional padding characters.
    /// </param>
    /// <param name="padChar">A Unicode padding character.</param>
    /// <param name="truncate">Should get only the substring of specified width if string width is
    /// more than the specified width.</param>
    /// <returns>A new string that is equivalent to this instance,
    /// but center-aligned with as many paddingChar characters as needed to create a
    /// length of width paramether.</returns>
    public static string PadBoth(this string value, int width, char padChar, bool truncate = false) {
        var diff = width - value.Length;
        if (diff == 0 || (diff < 0 && !truncate))
            return value;
        if (diff < 0)
            return value[..width];
        return value.PadLeft(width - (diff / 2), padChar).PadRight(width, padChar);
    }

    /*
    /// <summary>A generic version of System.String.Join()</summary>
    /// <typeparam name = "T">The type of the array to join</typeparam>
    /// <param name = "separator">The separator to appear between each element</param>
    /// <param name = "value">An array of values</param>
    /// <returns>The join.</returns>
    /// <remarks>Contributed by Michael T, http://about.me/MichaelTran </remarks>
    public static string Join<T>(string separator, T[] value)
    {
        if (value == null || value.Length == 0)
            return string.Empty;
        if (separator == null)
            separator = string.Empty;
        Converter<T, string> converter = o => o.ToString();
        return string.Join(separator, Array.ConvertAll(value, converter));
    }
    /// <summary>
    /// Concatenates a string between each item in a sequence of strings
    /// </summary>
    /// <param name="values"></param>
    /// <param name="separator"></param>
    /// <returns></returns>
    public static string Join(this IEnumerable<string> values, string separator)
    {
        return Join(values.ToArray(), separator);
    }
    /// <summary>Repeats the specified string value as provided by the repeat count.</summary>
    /// <param name = "value">The original string.</param>
    /// <param name = "repeatCount">The repeat count.</param>
    /// <returns>The repeated string</returns>
    public static string Repeat(this string value, int repeatCount)
    {
        var sb = new StringBuilder();
        repeatCount.Times(() => sb.Append(value));
        return sb.ToString();
    }
    /// <summary>Formats the value with the parameters using string.Format.</summary>
    /// <param name = "value">The input string.</param>
    /// <param name = "args">The parameters.</param>
    /// <returns></returns>
    /// 
    public static string ToFormat(this string value, params object[] args) => String.Format(value, args); */
    #endregion String Manipulation

    #region Search / Substring Alternatives
    /// <summary>Gets the string before the given string parameter.</summary>
    /// <param name = "value">The default value.</param>
    /// <param name = "searchValue">The string to be searched.</param>
    /// <param name = "comparison">String comparison method.</param>
    /// <returns></returns>
    public static string SubstringBefore(this string value, string searchValue, StringComparison comparison = StringComparison.Ordinal) {
        var xPos = value.IndexOf(searchValue, comparison);
        return xPos == -1 ? string.Empty : value[..xPos];
    }

    /// <summary>Gets the string between the given string parameters.</summary>
    /// <param name = "value">The default value.</param>
    /// <param name = "after">The left-most string parameter.</param>
    /// <param name = "before">The right-most string parameter</param>
    /// <param name="incSearchedText">Include selected text in the result or not</param>
    /// <returns></returns>
    public static string SubstringBetween(this string value, string after, string? before = null, bool incSearchedText = false) {
        var xPos = value.IndexOf(after);
        var yPos = before == null ? value.Length : value.LastIndexOf(before);

        if (xPos == -1 || xPos == -1)
            return string.Empty;

        var startIndex = xPos + after.Length;
        var rval = startIndex >= yPos ? string.Empty : value[startIndex..yPos];
        if (incSearchedText) return $"{after}{rval.ToArray()}{before}";
        return rval;
    }

    /// <summary>Returns the left part of the string.</summary>
    /// <param name="value">The original string.</param>
    /// <param name="characterCount">The character count to be returned.</param>
    /// <param name="suffix">Suffix to add if the string is cut off</param>
    /// <returns>The left part</returns>
    public static string Left(this string value, int characterCount, string? suffix = null) {
        if (value.Length < characterCount) return value;
        return (!string.IsNullOrEmpty(suffix)) ? $"{value[..(characterCount - suffix.Length)]}{suffix}" : value[..characterCount];
    }

    /// <summary>Returns the Right part of the string.</summary>
    /// <param name="value">The original string.</param>
    /// <param name="characterCount">The character count to be returned.</param>
    /// <param name="prefix">Prefix to add if the string is cut off</param>
    /// <returns>The right part</returns>
    public static string Right(this string value, int characterCount, string? prefix = null) {
        if (value.Length < characterCount) return value;
        return (!string.IsNullOrEmpty(prefix)) ? $"{prefix}{value[(prefix.Length + value.Length - characterCount)..]}" : value[^characterCount..];
    }
    /// <summary>Checks if a string contains another string by removing diacricits from each</summary>
    public static bool ContainsLoose(this string searching, string search) {
        return searching.Remove().DiacriticsAndSpaces()
            .Contains(search.Remove().DiacriticsAndSpaces(), StringComparison.InvariantCultureIgnoreCase);
    }
    /// <summary>Splits a string into to two arrays, one containing all string between chosen seperator chars, and other strings split by whitespaces</summary>
    public static void SplitBetween(this string text, char seperator, out List<string> betweenStrings, out List<string> otherStrings, StringSplitOptions options) {
        betweenStrings = [];
        otherStrings = [];
        var inQuotes = false;
        var currentString = new StringBuilder();

        string? applyOptionsAndAdd(StringBuilder sb, List<string> list) {
            var str = sb.ToString().Trim(seperator);
            if (options.HasFlag(StringSplitOptions.TrimEntries))
                str = str.Trim();
            if (options.HasFlag(StringSplitOptions.RemoveEmptyEntries) && string.IsNullOrEmpty(str))
                return null;
            list.Add(str);
            sb.Clear();
            return str;
        }

        foreach (var character in text) {
            if (character == seperator && !inQuotes) {
                inQuotes = true;
            } else if (character == seperator && inQuotes) {
                inQuotes = false;
                applyOptionsAndAdd(currentString, betweenStrings);
            } else if (char.IsWhiteSpace(character) && !inQuotes) {
                if (currentString.Length > 0)
                    applyOptionsAndAdd(currentString, otherStrings);
            } else {
                currentString.Append(character);
            }
        }

        if (inQuotes)
            applyOptionsAndAdd(currentString, betweenStrings);
        else if (currentString.Length > 0)
            applyOptionsAndAdd(currentString, otherStrings);

    }
    #endregion Search / Substring Alternatives

    #region Regex based extension methods
    /// <summary>
    /// 	Uses regular expressions to replace parts of a string.
    /// </summary>
    /// <param name = "value">The input string.</param>
    /// <param name = "regexPattern">The regular expression pattern.</param>
    /// <param name = "replaceValue">The replacement value.</param>
    /// <param name = "options">The regular expression options.</param>
    /// <returns>The newly created string</returns>
    /// <example>
    /// 	<code>
    /// 		var s = "12345";
    /// 		var replaced = s.ReplaceWith(@"\d", m => string.Concat(" -", m.Value, "- "));
    /// 	</code>
    /// </example>
    public static string Replace(this string value, string regexPattern, string replaceValue, RegexOptions options = RegexOptions.None) => Regex.Replace(value, regexPattern, replaceValue, options);

    /// <summary>
    /// 	Uses regular expressions to replace parts of a string.
    /// </summary>
    /// <param name = "value">The input string.</param>
    /// <param name = "regexPattern">The regular expression pattern.</param>
    /// <param name = "evaluator">The replacement method / lambda expression.</param>
    /// <param name = "options">The regular expression options.</param>
    /// <returns>The newly created string</returns>
    /// <example>
    /// 	<code>
    /// 		var s = "12345";
    /// 		var replaced = s.ReplaceWith(@"\d", m => string.Concat(" -", m.Value, "- "));
    /// 	</code>
    /// </example>
    public static string Replace(this string value, string regexPattern, MatchEvaluator evaluator, RegexOptions options = RegexOptions.None) => Regex.Replace(value, regexPattern, evaluator, options);

    /// <summary>
    /// 	Uses regular expressions to determine all matches of a given regex pattern.
    /// </summary>
    /// <param name = "value">The input string.</param>
    /// <param name = "regexPattern">The regular expression pattern.</param>
    /// <param name = "options">The regular expression options.</param>
    /// <returns>A collection of all matches</returns>
    public static MatchCollection GetMatches(this string value, string regexPattern, RegexOptions options = RegexOptions.None) => Regex.Matches(value, regexPattern, options);

    /// <summary>
    /// 	Uses regular expressions to determine all matches of a given regex pattern and returns them as string enumeration.
    /// </summary>
    /// <param name = "value">The input string.</param>
    /// <param name = "regexPattern">The regular expression pattern.</param>
    /// <param name = "options">The regular expression options.</param>
    /// <returns>An enumeration of matching strings</returns>
    /// <example>
    /// 	<code>
    /// 		var s = "12345";
    /// 		foreach(var number in s.GetMatchingValues(@"\d")) {
    /// 		Console.WriteLine(number);
    /// 		}
    /// 	</code>
    /// </example>
    public static IEnumerable<string> GetMatchingValues(this string value, string regexPattern, RegexOptions options = RegexOptions.None) =>
        from Match match in GetMatches(value, regexPattern, options)
        where match.Success
        select match.Value;

    /// <summary>
    /// 	Uses regular expressions to split a string into parts.
    /// </summary>
    /// <param name = "value">The input string.</param>
    /// <param name = "regexPattern">The regular expression pattern.</param>
    /// <param name = "options">The regular expression options.</param>
    /// <returns>The splitted string array</returns>
    public static string[] Split(this string value, string regexPattern, RegexOptions options) => Regex.Split(value, regexPattern, options);

    /// <summary>
    /// 	Splits the given string into words and returns a string array.
    /// </summary>
    /// <param name = "value">The input string.</param>
    /// <returns>The splitted string array</returns>
    public static string[] GetWords(this string value) => value.Split(@"\W", RegexOptions.None);

    /// <summary>
    /// 	Gets the nth "word" of a given string, where "words" are substrings separated by a given separator
    /// </summary>
    /// <param name = "value">The string from which the word should be retrieved.</param>
    /// <param name = "index">Index of the word (0-based).</param>
    /// <returns>
    /// 	The word at position n of the string.
    /// 	Trying to retrieve a word at a position lower than 0 or at a position where no word exists results in an exception.
    /// </returns>
    /// <remarks>
    /// 	Originally contributed by MMathews
    /// </remarks>
    public static string GetWords(this string value, int index) {
        var words = value.GetWords();
        if (index < 0 || index > words.Length - 1)
            throw new IndexOutOfRangeException("The word number is out of range.");

        return words[index];
    }
    #endregion Regex based extension methods

    #region TextInfo TitleCase (Disabled)
    ///// <summary></summary>
    //public static string ToTitleCase(this TextInfo textInfo, string str)
    //{
    //    var tokens = str.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
    //    for (int i = 0; i < tokens.Length; i++) {
    //        var token = tokens[i];
    //        tokens[i] = token.Substring(0, 1).ToUpper() + token.Substring(1);
    //    }

    //    return string.Join(" ", tokens);
    //}
    #endregion
}