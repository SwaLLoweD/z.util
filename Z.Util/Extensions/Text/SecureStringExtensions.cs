﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Z.Extensions;

/// <summary>Extension methods for string values...</summary>
public static class ExtSecureString
{
    /// <summary>Converts the specified string to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion To(this SecureString value) => new(value);

    /// <summary>Allows a decrypted secure string to be used whilst minimising the exposure of the unencrypted string.</summary>
    /// <typeparam name="T">Generic type returned by Func delegate.</typeparam>
    /// <param name="secureString">The string to decrypt.</param>
    /// <param name="action">Func delegate which will receive the decrypted password as a string object</param>
    /// <returns>Result of Func delegate</returns>
    /// <remarks>
    ///     This method creates an empty managed string and pins it so that the garbage collector cannot move it around
    ///     and create copies. An unmanaged copy of the secure string is then created and copied into the managed string.
    ///     The action is then called using the managed string. Both the managed and unmanaged strings are then zeroed to erase
    ///     their contents. The managed string is unpinned so that the garbage collector can resume normal behaviour and the
    ///     unmanaged string is freed.
    /// </remarks>
    public static T Do<T>(this SecureString secureString, Func<string, T> action) {
        var length = secureString.Length;
        var sourceStringPointer = IntPtr.Zero;

        // Create an empty string of the correct size and pin it so that the GC can't move it around.
        var insecureString = new string('\0', length);
        var insecureStringHandler = GCHandle.Alloc(insecureString, GCHandleType.Pinned);

        var insecureStringPointer = insecureStringHandler.AddrOfPinnedObject();

        try {
            // Create an unmanaged copy of the secure string.
            sourceStringPointer = Marshal.SecureStringToBSTR(secureString);

            // Use the pointers to copy from the unmanaged to managed string.
            for (var i = 0; i < secureString.Length; i++) {
                var unicodeChar = Marshal.ReadInt16(sourceStringPointer, i * 2);
                Marshal.WriteInt16(insecureStringPointer, i * 2, unicodeChar);
            }

            return action(insecureString);
        } finally {
            // Zero the managed string so that the string is erased. Then unpin it to allow the
            // GC to take over.
            Marshal.Copy(new byte[length], 0, insecureStringPointer, length);
            insecureStringHandler.Free();

            // Zero and free the unmanaged string.
            Marshal.ZeroFreeBSTR(sourceStringPointer);
        }
    }

    /// <summary>Allows a decrypted secure string to be used whilst minimising the exposure of the unencrypted string.</summary>
    /// <param name="secureString">The string to decrypt.</param>
    /// <param name="action">Func delegate which will receive the decrypted password as a string object</param>
    /// <returns>Result of Func delegate</returns>
    /// <remarks>
    ///     This method creates an empty managed string and pins it so that the garbage collector cannot move it around
    ///     and create copies. An unmanaged copy of the secure string is then created and copied into the managed string.
    ///     The action is then called using the managed string. Both the managed and unmanaged strings are then zeroed to erase
    ///     their contents. The managed string is unpinned so that the garbage collector can resume normal behaviour and the
    ///     unmanaged string is freed.
    /// </remarks>
    public static void Do(this SecureString secureString, Action<string> action) =>
        Do(secureString, s => {
            action(s);
            return 0;
        });

    #region Nested type: ConvertPortion
    #region Converter
    /// <summary>Multi-method library for string conversion related commands.</summary>
    public class ConvertPortion : ExtObject.ConvertPortion<SecureString>
    {
        #region Fields / Properties
        #region String
        /// <summary>Coverts the SecureString to a regular string.</summary>
        /// <returns>Content of secured string.</returns>
        public string String {
            get {
                var unmanagedString = IntPtr.Zero;
                try {
                    unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(Value);
                    return Marshal.PtrToStringUni(unmanagedString) ?? throw new NullReferenceException();
                } finally {
                    Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
                }
            }
        }
        #endregion
        #endregion

        #region Constructors
        /// <summary>Convert</summary>
        public ConvertPortion(SecureString value) : base(value) { }
        #endregion

        /// <inheritdoc />
        public new virtual U? Type<U>(U? defaultValue = default, IFormatProvider? format = null) {
            try {
                return (U?)Type(typeof(U), defaultValue, format);
            } catch {
                return defaultValue;
            }
        }
    }
    #endregion Converter
    #endregion
}