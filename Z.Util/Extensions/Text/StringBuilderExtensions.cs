﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Text;

namespace Z.Extensions {
    /// <summary>Extension methods for the StringBuilder class</summary>
    public static class ExtStringBuilder {
        /// <summary>AppendLine version with format string parameters.</summary>
        public static StringBuilder AppendLine(this StringBuilder builder, string value, params object?[] parameters) => builder.AppendFormat(value, parameters).AppendLine();
        /// <summary>Append multiple parameters as string.</summary>
        public static StringBuilder AppendString(this StringBuilder builder, params object?[] values) {
            foreach (var value in values) {
                if (value == null) continue;
                var type = value.GetType();
                if (type.Equals<char>())
                    builder.Append((char)value);
                else if (type.Equals<char[]>())
                    builder.Append((char[])value);
                else if (type.Equals<int>())
                    builder.Append((int)value);
                else if (type.Equals<short>())
                    builder.Append((short)value);
                else if (type.Equals<long>())
                    builder.Append((long)value);
                else if (type.Equals<byte>())
                    builder.Append((byte)value);
                else if (type.Equals<bool>())
                    builder.Append((bool)value);
                else if (type.Equals<sbyte>())
                    builder.Append((sbyte)value);
                else if (type.Equals<decimal>())
                    builder.Append((decimal)value);
                else if (type.Equals<float>())
                    builder.Append((float)value);
                else if (type.Equals<double>())
                    builder.Append((double)value);
                else if (type.Equals<ushort>())
                    builder.Append((ushort)value);
                else if (type.Equals<uint>())
                    builder.Append((uint)value);
                else if (type.Equals<ulong>())
                    builder.Append((ulong)value);
                else if (type.IsAssignableTo<string>())
                    builder.Append((string)value);
                else
                    builder.Append(value);
            }
            return builder;
        }
    }
}