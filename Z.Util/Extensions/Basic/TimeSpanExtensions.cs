﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;

namespace Z.Extensions
{
    /// <summary>Extension methods for TimeSpan values ...</summary>
    public static class ExtTimeSpan
    {
        /// <summary>Calculates a specific time of day that came before the specified date</summary>
        /// <param name="time">Time of the day to be searched.</param>
        /// <param name="beforeDate">
        ///     Upper end of the period to be searched for previous time of the day (Dates are not converted
        ///     to UTC)
        /// </param>
        /// <example>05:00, 19.10.2011 03:00 -- 18.10.2011 05:00</example>
        public static DateTime PreviousTimeOfDay(this TimeSpan time, DateTime? beforeDate = null) {
            beforeDate ??= DateTime.UtcNow;
            var rval = beforeDate.Value.Date.Add(time);
            return rval > beforeDate ? PreviousTimeOfDay(time, rval.AddDays(-1)) : rval;
        }
    }
}