﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using Z.Util;

namespace Z.Extensions;

/// <summary> Extension methods for Int64 values ... </summary>
public static class ExtNumeric
{
    /// <summary>Checks the specified number for different conditions.</summary>
    public static NumericIsPortion Is(this long value) => new(value);
    /// <summary>Checks the specified number for different conditions.</summary>
    public static NumericIsPortion Is(this int value) => new(value);
    /// <summary>Checks the specified number for different conditions.</summary>
    public static NumericIsPortion Is(this short value) => new(value);
    /// <summary>Checks the specified number for different conditions.</summary>
    public static NumericIsPortion Is(this byte value) => new(value);
    /// <summary>Checks the specified number for different conditions.</summary>
    public static NumericIsFPortion Is(this double value) => new(value);
    /// <summary>Checks the specified number for different conditions.</summary>
    public static NumericIsFPortion Is(this float value) => new(value);

    /// <summary>Converts the specified number to a different type.</summary>
    public static ConvertPortion<T> To<T>(this long value) => new(value);
    /// <summary>Converts the specified number to a different type.</summary>
    public static ConvertPortion<T> To<T>(this int value) => new(value);
    /// <summary>Converts the specified number to a different type.</summary>
    public static ConvertPortion<T> To<T>(this short value) => new(value);
    /// <summary>Converts the specified number to a different type.</summary>
    public static ConvertPortion<T> To<T>(this byte value) => new(value);

    /// <summary>Converts the specified number to a different type.</summary>
    public static ConvertFPortion<T> To<T>(this double value) => new(value);
    /// <summary>Converts the specified number to a different type.</summary>
    public static ConvertFPortion<T> To<T>(this float value) => new(value);

    #region Nested type: ConvertFPortion
    #region Converter (Fraction)
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class ConvertFPortion<T> : ExtendablePortion<double>, IConvertPortion<double>
    {
        #region Constructors
        /// <summary>Convert</summary>
        public ConvertFPortion(double value) : base(value) { }
        #endregion

        /// <summary>Converts the value to Roman Numeral string.</summary>
        public string Currency(IFormatProvider? format = null) => string.Format(format ?? SysInf.CurrentFormat, "{0:C}", Value);
    }
    #endregion Converter (Fraction)
    #endregion

    #region Nested type: ConvertPortion
    #region Converter
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class ConvertPortion<T> : ExtendablePortion<long>, IConvertPortion<long>
    {
        #region Fields / Properties
        /// <summary>Converts the value to Roman Numeral string.</summary>
        public string RomanNumeral => Util.Mathematics.RomanNumeral.ToRomanNumeralString((int)Value);
        #endregion

        #region Constructors
        /// <summary>Convert</summary>
        public ConvertPortion(long value) : base(value) { }
        #endregion

        /// <summary>Converts the value to ordinal string. (English/Turkish)</summary>
        /// <returns>Returns string containing ordinal indicator adjacent to a numeral denoting. (English/Turkish)</returns>
        public string Ordinal() {
            string? suffix;
            if (SysInf.CurrentFormat.TwoLetterISOLanguageName == "tr") {
                suffix = (Value % 10) switch {
                    0 => "'ıncı",
                    2 or 7 => "'nci",
                    3 or 4 => "'üncü",
                    6 => "'ncı",
                    9 => "'uncu",
                    //case 1:
                    //case 5:
                    //case 8:
                    _ => "'inci",
                };
            } else {
                suffix = "th";
                switch (Value % 100) {
                    case 11:
                    case 12:
                    case 13:
                        break;

                    default:
                        switch (Value % 10) {
                            case 1:
                                suffix = "st";
                                break;

                            case 2:
                                suffix = "nd";
                                break;

                            case 3:
                                suffix = "rd";
                                break;
                        }
                        break;
                }
            }
            return string.Format("{0}{1}", Value, suffix);
        }
    }
    #endregion Converter
    #endregion

    #region Nested type: NumericIsFPortion
    #region Is Checker (Fraction)
    /// <summary>Multi-method library for string condition check related commands.</summary>
    public class NumericIsFPortion : ExtendablePortion<double>, IIsPortion<double>
    {
        #region Fields / Properties
        /// <summary>Determines whether the value is even</summary>
        /// <returns>true or false</returns>
        public bool Even => Value % 2 == 0;
        #endregion

        #region Constructors
        /// <summary>Is</summary>
        public NumericIsFPortion(double value) : base(value) { }
        #endregion

        /// <summary>Determines whether the value is between specified values</summary>
        public bool Between(double a, double b) => Value >= a && Value <= b;
        /// <summary>Checks if the number is almost zero.</summary>
        /// <param name="eps">epsilon chosen for calculation</param>
        /// <returns>true if is almost zero</returns>
        public bool AlmostZero(double eps = 1E-10) => Math.Abs(Value) <= eps;

        /// <summary>Compares the numbers.</summary>
        /// <param name="compareTo">Second number.</param>
        /// <param name="eps">epsilon chosen for calculation</param>
        /// <returns>true if almost equal</returns>
        public bool AlmostEqual(double compareTo, double eps = 1E-10) => Math.Abs(Value - compareTo) <= eps;

        ///// <summary>
        ///// Subtracts one series from another.
        ///// </summary>
        ///// <param name="src">Source series.</param>
        ///// <param name="dst">Subtracted series.</param>
        //public void Subtract(this decimal[] src, decimal[] dst)
        //{
        //    for (int i = 0; i < src.Length; i++) {
        //        src[i] -= dst[i];
        //    }
        //}

        ///// <summary>
        ///// Subtracts one series from another and creates new series.
        ///// </summary>
        ///// <param name="src">Source series.</param>
        ///// <param name="dst">Subtracted series.</param>
        ///// <returns>New series.</returns>
        //public static decimal[] CreateSubstract(this decimal[] src, decimal[] dst)
        //{
        //    var t = new decimal[src.Length];
        //    for (int i = 0; i < src.Length; i++) {
        //        t[i] = src[i] - dst[i];
        //    }

        //    return t;
        //}
    }
    #endregion Is Checker
    #endregion

    #region Nested type: NumericIsPortion
    #region Is Checker
    /// <summary>Multi-method library for string condition check related commands.</summary>
    public class NumericIsPortion : ExtendablePortion<long>, IIsPortion<long>
    {
        #region Fields / Properties
        /// <summary>Determines whether the value is even</summary>
        /// <returns>true or false</returns>
        public bool Even => Value % 2 == 0;

        /// <summary>
        ///     A prime number (or a prime) is a natural number that has exactly two distinct natural number divisors: 1 and
        ///     itself.
        /// </summary>
        /// <returns>Returns true if the value is a prime number.</returns>
        public bool Prime {
            get {
                if ((Value & 1) == 0) {
                    return Value == 2;
                }
                for (long i = 3; i * i <= Value; i += 2) {
                    if (Value % i == 0)
                        return false;
                }

                return Value != 1;
            }
        }
        #endregion

        #region Constructors
        /// <summary>Is</summary>
        public NumericIsPortion(long value) : base(value) { }
        #endregion

        /// <summary>Determines whether the value is between specified values</summary>
        public bool Between(long a, long b) => Value >= a && Value <= b;
    }
    #endregion Is Checker
    #endregion

    #region Round
    /// <summary>Shorten the fraction by rounding the decimal digits</summary>
    /// <param name="value">Fraction</param>
    /// <param name="decimals">Intented number of decimal digits</param>
    /// <param name="mode">Rounding method</param>
    /// <returns>Rounded Fraction</returns>
    public static double Round(this double value, int decimals, MidpointRounding mode = MidpointRounding.AwayFromZero) => Math.Round(value, decimals, mode);

    /// <summary>Shorten the fraction by rounding the decimal digits</summary>
    /// <param name="value">Fraction</param>
    /// <param name="decimals">Intented number of decimal digits</param>
    /// <param name="mode">Rounding method</param>
    /// <returns>Rounded Fraction</returns>
    public static float Round(this float value, int decimals, MidpointRounding mode = MidpointRounding.AwayFromZero) => (float)Math.Round(value, decimals, mode);

    /// <summary>Shorten the fraction by rounding the decimal digits</summary>
    /// <param name="value">Fraction</param>
    /// <param name="decimals">Intented number of decimal digits</param>
    /// <param name="mode">Rounding method</param>
    /// <returns>Rounded Fraction</returns>
    public static decimal Round(this decimal value, int decimals, MidpointRounding mode = MidpointRounding.AwayFromZero) => Math.Round(value, decimals, mode);
    #endregion Round

    #region Disabled Extensions
    ///// <summary>
    ///// Converts the value to ordinal string with specified format. (English)
    ///// </summary>
    ///// <param name="i">Object value</param>
    ///// <param name="format">A standard or custom format string that is supported by the object to be formatted.</param>
    ///// <returns>Returns string containing ordinal indicator adjacent to a numeral denoting. (English)</returns>
    //public static string ToOrdinal(this long i, string format)
    //{ return string.Format(format, i.ToOrdinal());  }
    #endregion Disabled Extensions
}