﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Numerics;
using System.Threading.Tasks;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for TimeSpan values ...</summary>
public static class ExtBigInteger
{
    #region Constants / Static Fields
    #region Small Primes
    internal static readonly uint[] smallPrimes = {
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
            73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151,
            157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233,
            239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317,
            331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419,
            421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503,
            509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607,
            613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701,
            709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811,
            821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911,
            919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997,

            1009, 1013, 1019, 1021, 1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069, 1087,
            1091, 1093, 1097, 1103, 1109, 1117, 1123, 1129, 1151, 1153, 1163, 1171, 1181,
            1187, 1193, 1201, 1213, 1217, 1223, 1229, 1231, 1237, 1249, 1259, 1277, 1279,
            1283, 1289, 1291, 1297, 1301, 1303, 1307, 1319, 1321, 1327, 1361, 1367, 1373,
            1381, 1399, 1409, 1423, 1427, 1429, 1433, 1439, 1447, 1451, 1453, 1459, 1471,
            1481, 1483, 1487, 1489, 1493, 1499, 1511, 1523, 1531, 1543, 1549, 1553, 1559,
            1567, 1571, 1579, 1583, 1597, 1601, 1607, 1609, 1613, 1619, 1621, 1627, 1637,
            1657, 1663, 1667, 1669, 1693, 1697, 1699, 1709, 1721, 1723, 1733, 1741, 1747,
            1753, 1759, 1777, 1783, 1787, 1789, 1801, 1811, 1823, 1831, 1847, 1861, 1867,
            1871, 1873, 1877, 1879, 1889, 1901, 1907, 1913, 1931, 1933, 1949, 1951, 1973,
            1979, 1987, 1993, 1997, 1999,

            2003, 2011, 2017, 2027, 2029, 2039, 2053, 2063, 2069, 2081, 2083, 2087, 2089,
            2099, 2111, 2113, 2129, 2131, 2137, 2141, 2143, 2153, 2161, 2179, 2203, 2207,
            2213, 2221, 2237, 2239, 2243, 2251, 2267, 2269, 2273, 2281, 2287, 2293, 2297,
            2309, 2311, 2333, 2339, 2341, 2347, 2351, 2357, 2371, 2377, 2381, 2383, 2389,
            2393, 2399, 2411, 2417, 2423, 2437, 2441, 2447, 2459, 2467, 2473, 2477, 2503,
            2521, 2531, 2539, 2543, 2549, 2551, 2557, 2579, 2591, 2593, 2609, 2617, 2621,
            2633, 2647, 2657, 2659, 2663, 2671, 2677, 2683, 2687, 2689, 2693, 2699, 2707,
            2711, 2713, 2719, 2729, 2731, 2741, 2749, 2753, 2767, 2777, 2789, 2791, 2797,
            2801, 2803, 2819, 2833, 2837, 2843, 2851, 2857, 2861, 2879, 2887, 2897, 2903,
            2909, 2917, 2927, 2939, 2953, 2957, 2963, 2969, 2971, 2999,

            3001, 3011, 3019, 3023, 3037, 3041, 3049, 3061, 3067, 3079, 3083, 3089, 3109,
            3119, 3121, 3137, 3163, 3167, 3169, 3181, 3187, 3191, 3203, 3209, 3217, 3221,
            3229, 3251, 3253, 3257, 3259, 3271, 3299, 3301, 3307, 3313, 3319, 3323, 3329,
            3331, 3343, 3347, 3359, 3361, 3371, 3373, 3389, 3391, 3407, 3413, 3433, 3449,
            3457, 3461, 3463, 3467, 3469, 3491, 3499, 3511, 3517, 3527, 3529, 3533, 3539,
            3541, 3547, 3557, 3559, 3571, 3581, 3583, 3593, 3607, 3613, 3617, 3623, 3631,
            3637, 3643, 3659, 3671, 3673, 3677, 3691, 3697, 3701, 3709, 3719, 3727, 3733,
            3739, 3761, 3767, 3769, 3779, 3793, 3797, 3803, 3821, 3823, 3833, 3847, 3851,
            3853, 3863, 3877, 3881, 3889, 3907, 3911, 3917, 3919, 3923, 3929, 3931, 3943,
            3947, 3967, 3989,

            4001, 4003, 4007, 4013, 4019, 4021, 4027, 4049, 4051, 4057, 4073, 4079, 4091,
            4093, 4099, 4111, 4127, 4129, 4133, 4139, 4153, 4157, 4159, 4177, 4201, 4211,
            4217, 4219, 4229, 4231, 4241, 4243, 4253, 4259, 4261, 4271, 4273, 4283, 4289,
            4297, 4327, 4337, 4339, 4349, 4357, 4363, 4373, 4391, 4397, 4409, 4421, 4423,
            4441, 4447, 4451, 4457, 4463, 4481, 4483, 4493, 4507, 4513, 4517, 4519, 4523,
            4547, 4549, 4561, 4567, 4583, 4591, 4597, 4603, 4621, 4637, 4639, 4643, 4649,
            4651, 4657, 4663, 4673, 4679, 4691, 4703, 4721, 4723, 4729, 4733, 4751, 4759,
            4783, 4787, 4789, 4793, 4799, 4801, 4813, 4817, 4831, 4861, 4871, 4877, 4889,
            4903, 4909, 4919, 4931, 4933, 4937, 4943, 4951, 4957, 4967, 4969, 4973, 4987,
            4993, 4999,

            5003, 5009, 5011, 5021, 5023, 5039, 5051, 5059, 5077, 5081, 5087, 5099, 5101,
            5107, 5113, 5119, 5147, 5153, 5167, 5171, 5179, 5189, 5197, 5209, 5227, 5231,
            5233, 5237, 5261, 5273, 5279, 5281, 5297, 5303, 5309, 5323, 5333, 5347, 5351,
            5381, 5387, 5393, 5399, 5407, 5413, 5417, 5419, 5431, 5437, 5441, 5443, 5449,
            5471, 5477, 5479, 5483, 5501, 5503, 5507, 5519, 5521, 5527, 5531, 5557, 5563,
            5569, 5573, 5581, 5591, 5623, 5639, 5641, 5647, 5651, 5653, 5657, 5659, 5669,
            5683, 5689, 5693, 5701, 5711, 5717, 5737, 5741, 5743, 5749, 5779, 5783, 5791,
            5801, 5807, 5813, 5821, 5827, 5839, 5843, 5849, 5851, 5857, 5861, 5867, 5869,
            5879, 5881, 5897, 5903, 5923, 5927, 5939, 5953, 5981, 5987
        };
    #endregion
    #endregion
    #pragma warning disable RCS1175, IDE0060
    /// <summary>Creates a random prime</summary>
    /// <param name="bint"></param>
    /// <param name="byteLength">Length of the BigInteger in bytes</param>
    /// <param name="certainty">Prime certainty using Miller-Rabin algorithm (certainty 0 is Auto confidence)</param>
    public static BigInteger RandomPrime(this BigInteger bint, int byteLength, int certainty = 0) {
        var rval = Random(byteLength, true);
        while (!rval.IsProbablePrime(certainty)) rval = Random(byteLength, true);
        //var s = rval.ToByteArray().To().ArrayString();
        return rval;
    }
    /// <summary>Creates a random number</summary>
    /// <param name="bint"></param>
    /// <param name="byteLength">Length of the BigInteger in bytes</param>
    /// <param name="onlyPositive">Only create positive bigIntegers</param>
    public static BigInteger Random(this BigInteger bint, int byteLength, bool onlyPositive = true) => Random(byteLength, onlyPositive);
    #pragma warning restore
    private static BigInteger Random(int byteLength, bool onlyPositive = false) {
        byte[] rndBytes = Encrypter.CreateRandomBytes(byteLength);
        var rval = new BigInteger(rndBytes);
        if (onlyPositive && rval.Sign != 1) rval = BigInteger.Negate(rval);
        //var sign = rval.Sign;
        //var len = rval.ToByteArray().Length;
        //var last = rndBytes[rndBytes.Length - 1];
        return rval;
    }
    /// <summary>Creates a random number bigger than the current one</summary>
    /// <param name="min"></param>
    /// <param name="max">Max value of the number created</param>
    public static BigInteger RandomBigger(this BigInteger min, BigInteger max) {
        var result = Random(max.ToByteArray().Length);
        // Generate the random number
        var randomBigInt = BigInteger.ModPow(result, 1, BigInteger.Add(max, min));
        return randomBigInt;
    }

    /// <summary>Miller-Rabin prime number test</summary>
    /// <param name="source"></param>
    /// <param name="certainty">
    ///     Prime certainty using Miller-Rabin algorithm (1024 RSA key requires 40 iterations, 2048
    ///     requires 56, 64 is about enough for anything)
    /// </param>
    public static bool IsProbablePrime(this BigInteger source, int certainty = 0) {
        //if (source == 2 || source == 3) return true;
        //if (source < 2 || source % 2 == 0)  return false;
        // can we use our small-prime table ?
        if (source <= smallPrimes[^1]) {
            for (var p = 0; p < smallPrimes.Length; p++) {
                if (source == smallPrimes[p])
                    return true;
            }
            // the list is complete, so it's not a prime
            return false;
        }

        // otherwise check if we can divide by one of the small primes
        for (var p = 0; p < smallPrimes.Length; p++) {
            if (source % smallPrimes[p] == 0)
                return false;
        }

        return MillerRabinTest(source, certainty);
    }

    /// <summary>Miller-Rabin prime number test</summary>
    /// <param name="source"></param>
    /// <param name="certainty">
    ///     Prime certainty using Miller-Rabin algorithm (1024 RSA key requires 40 iterations, 2048
    ///     requires 56, 64 is about enough for anything)
    /// </param>
    private static bool MillerRabinTest(BigInteger source, int certainty = 0) {
        var d = source - 1;
        var s = 0;

        while (d % 2 == 0) {
            d /= 2;
            s++;
        }

        var sign = source.Sign;
        var len = source.ToByteArray().Length;
        certainty = certainty == 0 ? GetSPPRounds(len) : certainty;
        var rval = false;
        Parallel.For(0, certainty, (_, ls) => {
            BigInteger a;
            do {
                a = Random(len, true);
            } while (a < 2 || a >= source - 2);

            var x = BigInteger.ModPow(a, d, source);
            if (x == 1 || x == source - 1) {
                rval = true;
                return;
            }

            for (var r = 1; r < s; r++) {
                x = BigInteger.ModPow(x, 2, source);
                if (x == 1) {
                    rval = false;
                    ls.Stop();
                }
                if (x == source - 1) {
                    rval = true;
                    ls.Stop();
                }
            }
            if (x != source - 1) {
                rval = false;
                ls.Stop();
            }
        });
        return rval;
        //BigInteger a;
        //for (int i = 0; i < certainty; i++) {
        //    do {
        //        a = random(len, true);
        //    }
        //    while (a < 2 || a >= source - 2);

        //    BigInteger x = BigInteger.ModPow(a, d, source);
        //    if (x == 1 || x == source - 1) continue;

        //    for (int r = 1; r < s; r++) {
        //        x = BigInteger.ModPow(x, 2, source);
        //        if (x == 1) return false;
        //        if (x == source - 1) break;
        //    }
        //    if (x != source - 1) return false;
        //}
        //return true;
    }
    private static int GetSPPRounds(int bc) {
        // Data from HAC, 4.49
        return bc switch {
            <= 12 => 27,
            <= 18 => 18,
            <= 25 => 15,
            <= 31 => 12,
            <= 37 => 9,
            <= 43 => 8,
            <= 50 => 7,
            <= 62 => 6,
            <= 75 => 5,
            <= 100 => 4,
            <= 156 => 3,
            _ => 2
        };

        //switch (confidence) {
        //    case ConfidenceFactor.ExtraLow:
        //        Rounds >>= 2;
        //        return Rounds != 0 ? Rounds : 1;
        //    case ConfidenceFactor.Low:
        //        Rounds >>= 1;
        //        return Rounds != 0 ? Rounds : 1;
        //    case ConfidenceFactor.Medium:
        //        return Rounds;
        //    case ConfidenceFactor.High:
        //        return Rounds << 1;
        //    case ConfidenceFactor.ExtraHigh:
        //        return Rounds << 2;
        //    case ConfidenceFactor.Provable:
        //        throw new Exception("The Rabin-Miller test can not be executed in a way such that its results are provable");
        //    default:
        //        throw new ArgumentOutOfRangeException("confidence");
        //}
    }

    /// <summary>Square Root via Newton Raphson method</summary>
    /// <param name="N"></param>
    public static BigInteger SqRtN(this BigInteger N) {
        /*++
         *  Using Newton Raphson method we calculate the
         *  square root (N/g + g)/2
         */
        var rootN = N;
        var count = 0;
        var bitLength = 1;
        while (rootN / 2 != 0) {
            rootN /= 2;
            bitLength++;
        }
        bitLength = (bitLength + 1) / 2;
        rootN = N >> bitLength;
        var lastRoot = BigInteger.Zero;
        do {
            if (lastRoot > rootN && count++ > 1000)
                return rootN;
            lastRoot = rootN;
            rootN = (BigInteger.Divide(N, rootN) + rootN) >> 1;
        } while ((rootN ^ lastRoot).ToString() != "0");
        return rootN;
    }
    /// <summary> Performs modulus division on a number raised to the power of another number. </summary>
    /// <param name="source">The number to raise to the exponent power.</param>
    /// <param name="exponent">The exponent to raise value by.</param>
    /// <param name="modulus">The number by which to divide value raised to the exponent power.</param>
    /// <returns>The remainder after dividing valueexponent by modulus.</returns>
    /// <exception cref="System.DivideByZeroException">modulus is zero.</exception>
    /// <exception cref="System.ArgumentOutOfRangeException">exponent is negative.</exception>
    public static BigInteger ModPow(this BigInteger source, BigInteger exponent, BigInteger modulus) => BigInteger.ModPow(source, exponent, modulus);
    /// <summary> This method returns a BigInteger object whose value is this-1 mod m. </summary>
    /// <param name="source">The number.</param>
    /// <param name="modulus">m.</param>
    /// <returns>The result of ModInverse.</returns>
    public static BigInteger ModInverse(this BigInteger source, BigInteger modulus) {
        var a = source;
        var n = modulus;
        BigInteger i = n, v = 0, d = 1;
        while (a > 0) {
            BigInteger t = i / a, x = a;
            a = i % x;
            i = x;
            x = d;
            d = v - (t * x);
            v = x;
        }
        v %= n;
        if (v < 0) v = (v + n) % n;
        return v;
    }
    /// <summary> Performs addition. </summary>
    /// <param name="source"></param>
    /// <param name="toAdd">Raise the biginteger by</param>
    public static BigInteger Add(this BigInteger source, BigInteger toAdd) {
        var rval = BigInteger.Add(source, toAdd);
        return rval;
    }
    /// <summary> Performs subtraction. </summary>
    /// <param name="source"></param>
    /// <param name="toSubtract">Lower the biginteger by</param>
    public static BigInteger Subtract(this BigInteger source, BigInteger toSubtract) {
        var rval = BigInteger.Subtract(source, toSubtract);
        return rval;
    }
    /// <summary> Performs Multiplication. </summary>
    /// <param name="source"></param>
    /// <param name="multiplier">Multiplier</param>
    public static BigInteger Multiply(this BigInteger source, BigInteger multiplier) => BigInteger.Multiply(source, multiplier);
    /// <summary> Performs Division. </summary>
    /// <param name="source"></param>
    /// <param name="divisor">Divisor</param>
    public static BigInteger Divide(this BigInteger source, BigInteger divisor) => BigInteger.Divide(source, divisor);
    /// <summary> Performs Division. </summary>
    /// <param name="source"></param>
    /// <param name="divisor">Divisor</param>
    /// <param name="remainder">Division remainder</param>
    public static BigInteger DivRem(this BigInteger source, BigInteger divisor, out BigInteger remainder) => BigInteger.DivRem(source, divisor, out remainder);
    /// <summary> Performs Modulus. </summary>
    /// <param name="source"></param>
    /// <param name="divisor">Divisor</param>
    public static BigInteger Modulus(this BigInteger source, BigInteger divisor) => BigInteger.Remainder(source, divisor);
    /// <summary> Performs absolute n </summary>
    /// <param name="source"></param>
    public static BigInteger Abs(this BigInteger source) => BigInteger.Abs(source);
    /// <summary> Performs Negation </summary>
    /// <param name="source"></param>
    public static BigInteger Negate(this BigInteger source) => BigInteger.Negate(source);
    /// <summary> Performs Logarithm </summary>
    /// <param name="source"></param>
    /// <param name="baseValue">Logarithm base</param>
    public static double Log(this BigInteger source, double baseValue) => BigInteger.Log(source, baseValue);
}