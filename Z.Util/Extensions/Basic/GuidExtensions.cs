﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Security.Cryptography;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for Guid values ...</summary>
public static class ExtGuid
{
    // /// <summary>Packs two long values into a Guid</summary>
    // /// <param name="g">Base Guid.</param>
    // /// <param name="lobit">Lower bits of the new Guid.</param>
    // /// <param name="hibit">Higher bits of the new Guid.</param>
    // /// <returns>New guid created from two long values</returns>
    // public static Guid Parse(this Guid g, long lobit, long hibit = 0) => g = Fnc.GuidParse(lobit, hibit);

    /// <summary>Exports two long values from a Guid</summary>
    /// <param name="g">Base Guid.</param>
    /// <param name="hiPart">False = Lower bits, True = Higher bits</param>
    /// <returns>Long value of the selected part</returns>
    public static long ToLong(this Guid g, bool hiPart = false) => BitConverter.ToInt64(g.ToByteArray(), hiPart ? 0 : 8);

    // /// <summary>MD5 hash as guid</summary>
    // /// <param name="g"></param>
    // /// <param name="s">string to generate hash for</param>
    // public static Guid Hash(this Guid g, string s) => s.Is().Empty ? Guid.Empty : new Guid(MD5.Create().ComputeHash(s.To().ByteArray.AsUtf8));

    // /// <summary>MD5 hash as guid</summary>
    // /// <param name="g"></param>
    // /// <param name="b">byte[] to generate hash for</param>
    // public static Guid Hash(this Guid g, byte[] b) => b == null ? Guid.Empty : new Guid(MD5.Create().ComputeHash(b));
}