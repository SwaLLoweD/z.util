﻿using System.Net.Sockets;
namespace Z.Extensions;

/// <summary>Extension methods for Network Sockets</summary>
public static class ExtSocket
{
    /// <summary>Returns the state of the socket</summary>
    public static bool IsConnected(this Socket socket) {
        try {
            return !((socket.Poll(1000, SelectMode.SelectRead) && (socket.Available == 0)) || !socket.Connected);
        } catch (SocketException) {
            return false;
        }
    }
    /// <summary>Tries to shutdown and then close socket</summary>
    public static bool Close(this Socket socket, bool shutdown, int timeout = 0, SocketShutdown shutdownType = SocketShutdown.Both) {
        if (socket == null) return false;
        if (shutdown) {
            try {
                socket.Shutdown(shutdownType);
            } catch { }
        }

        try {
            if (timeout > 0)
                socket.Close(timeout);
            else
                socket.Close();
        } catch {
            return false;
        }
        return true;
    }
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    public static void ConnectAsync(this Socket socket, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) =>
        SocketAsync(socket, socket.ConnectAsync, e, handler, skipEventBind);
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    public static void DisconnectAsync(this Socket socket, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) =>
        SocketAsync(socket, socket.DisconnectAsync, e, handler, skipEventBind);
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    public static void AcceptAsync(this Socket socket, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) =>
        SocketAsync(socket, socket.AcceptAsync, e, handler, skipEventBind);
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    public static void ReceiveAsync(this Socket socket, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) =>
        SocketAsync(socket, socket.ReceiveAsync, e, handler, skipEventBind);
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    public static void ReceiveFromAsync(this Socket socket, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) =>
        SocketAsync(socket, socket.ReceiveFromAsync, e, handler, skipEventBind);
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    public static void SendAsync(this Socket socket, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) =>
        SocketAsync(socket, socket.SendAsync, e, handler, skipEventBind);
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    public static void SendToAsync(this Socket socket, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) =>
        SocketAsync(socket, socket.SendToAsync, e, handler, skipEventBind);
    /// <summary>Shortcut methods to call handlers immediately if the operation will not be executed asynchronously</summary>
    private static void SocketAsync(Socket socket, Func<SocketAsyncEventArgs, bool> action, SocketAsyncEventArgs e, EventHandler<SocketAsyncEventArgs> handler, bool skipEventBind = false) {
        if (!skipEventBind) e.Completed += handler;
        if (!action(e)) handler(socket, e);
    }
}