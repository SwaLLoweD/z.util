﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Data;

namespace Z.Extensions
{
    /// <summary>Extension methods for all kind of ADO.NET DbCommand..</summary>
    public static class ExtIDbCommand
    {
        /// <summary>Adds a parameter to the command.</summary>
        /// <param name="command">The command.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">The parameter value.</param>
        /// <param name="dbType">The dbtype of the value</param>
        public static void ParameterAdd(this IDbCommand command, string parameterName, object parameterValue, DbType? dbType = null) {
            var parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.Value = parameterValue;
            if (dbType != null) parameter.DbType = dbType.Value;
            command.Parameters.Add(parameter);
        }
        /// <summary>Adds a parameter to the command.</summary>
        /// <param name="command">The command.</param>
        /// <param name="parameterValue">The parameter value.</param>
        public static void ParameterAdd(this IDbCommand command, object parameterValue) => command.Parameters.Add(parameterValue);
    }
}