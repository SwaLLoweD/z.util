﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for the XmlDocument class</summary>
public static class ExtXmlDocument {
    /// <summary>Deserialize xml into Object</summary>
    /// <param name="value">The xml to be Deserialized.</param>
    /// <returns>An universal deserialize helper supplying additional deserialization methods</returns>
    public static DeserializePortion Deserialize(this XmlDocument value) => new(value);

    #region Nested type: DeserializePortion
    #region Deserialize
    /// <summary>Multi-method library for deserialization.</summary>
    public class DeserializePortion: ExtendablePortion<XmlDocument> {
        #region Constructors
        /// <summary>Constructor</summary>
        public DeserializePortion(XmlDocument value) : base(value) { }
        #endregion

        #region ZXml
        /// <summary>Custom Deserialize from XML</summary>
        /// <param name="o"></param>
        /// <param name="parsedProperties">Properties that have been found and filled</param>
        public T? ZXml<T>(T? o = default, ISet<ZMemberInfo>? parsedProperties = null) => Fnc.DeserializeXml2(o, Value, parsedProperties);
        #endregion ZXml

        #region DotNetXml
        /// <summary>Deserialize document into an object using .NET Xml Serializer.</summary>
        public T? DotNetXml<T>() => (T?)DotNetXml(typeof(T));

        /// <summary>Deserialize document into an object using .NET Xml Serializer.</summary>
        public object? DotNetXml(Type objectType) {
            var rval = objectType.GetDefault();
            if (Value == null) return rval;
            using (var m = new MemoryStream(Value.InnerText.To().ByteArray.AsUtf8)) {
                rval = m.Deserialize().DotNetXml(objectType);
            }
            return rval;
        }
        #endregion DotNetXml
    }
    #endregion Deserialize
    #endregion
}