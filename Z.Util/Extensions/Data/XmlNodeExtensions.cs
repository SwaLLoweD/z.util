﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Xml;

namespace Z.Extensions;

/// <summary>Extension methods for the XmlNode class</summary>
public static class ExtXmlNode
{
    /// <summary>Appends a child to a XML node</summary>
    /// <param name="parentNode">The parent node.</param>
    /// <param name="name">The name of the child node.</param>
    /// <param name="namespaceUri">The node namespace.</param>
    /// <returns>The newly created XML node</returns>
    public static XmlNode CreateChildNode(this XmlNode parentNode, string name, string? namespaceUri = null) {
        var document = parentNode is not XmlDocument parentAsDoc ? parentNode.OwnerDocument : parentAsDoc;
        XmlNode? node = namespaceUri == null ? document?.CreateElement(name) : document?.CreateElement(name, namespaceUri);
        if (node == null) throw new ArgumentNullException(nameof(parentNode));
        parentNode.AppendChild(node);
        return node;
    }
    /// <summary>Creates an attribute for an XML node</summary>
    /// <param name="parentNode">The parent node.</param>
    /// <param name="name">The name of the attribute.</param>
    /// <param name="value">The value of the attribute.</param>
    /// <returns>The newly created XML node</returns>
    public static XmlNode CreateAttribute(this XmlNode parentNode, string name, string? value) {
        //var parentAsDoc = parentNode as XmlDocument;
        //var document = parentAsDoc == null ? parentNode.OwnerDocument : parentAsDoc;
        parentNode.SetAttribute(name, value);
        return parentNode;
    }

    /// <summary>Appends a CData section to a XML node and prefills the provided data</summary>
    /// <param name="parentNode">The parent node.</param>
    /// <param name="data">The CData section value.</param>
    /// <returns>The created CData Section</returns>
    public static XmlCDataSection CreateCDataSection(this XmlNode parentNode, string? data = null) {
        var document = parentNode is not XmlDocument parentAsDoc ? parentNode.OwnerDocument : parentAsDoc;
        var node = data == null ? document?.CreateCDataSection(string.Empty) : document?.CreateCDataSection(data);
        if (node == null) throw new ArgumentNullException(nameof(parentNode));
        parentNode.AppendChild(node);
        return node;
    }

    /// <summary>Returns the value of a nested CData section.</summary>
    /// <param name="parentNode">The parent node.</param>
    /// <param name="defaultValue">Value to use if result is null</param>
    /// <returns>The CData section content</returns>
    public static string? GetCDataSection(this XmlNode parentNode, string? defaultValue = null) {
        if (parentNode == null) return defaultValue;
        foreach (var node in parentNode.ChildNodes) {
            if (node is XmlCDataSection xmlCDataSections)
                return (xmlCDataSections).Value;
        }

        return defaultValue;
    }
    /// <summary>Gets the InnerText of a Child or return null if not found</summary>
    /// <param name="node">The node.</param>
    /// <param name="xPath">The Path of the child node.</param>
    /// <param name="defaultValue">Value to use if result is null</param>
    /// <returns>The innerText value</returns>
    public static string? GetInnerText(this XmlNode node, string? xPath = null, string? defaultValue = null) {
        if (node == null) return defaultValue;
        if (xPath == null) return node.InnerText ?? defaultValue;
        var realn = node.SelectSingleNode(xPath);
        if (realn == null) return defaultValue;
        return realn.InnerText ?? defaultValue;
    }
    /// <summary>Gets the InnerText of a Child or return null if not found</summary>
    /// <param name="node">The node.</param>
    /// <param name="xPath">The Path of the child node.</param>
    /// <param name="defaultValue">Value to use if result is null</param>
    /// <returns>The innerText value</returns>
    public static T? GetInnerText<T>(this XmlNode node, string? xPath = null, T? defaultValue = default) {
        var value = GetInnerText(node, xPath);
        return ConvertString(value, defaultValue);
    }
    /// <summary>Gets an attribute value </summary>
    /// <param name="node">The node.</param>
    /// <param name="attributeName">The Name of the attribute.</param>
    /// <param name="xPath">xPath</param>
    /// <param name="defaultValue">The default value to be returned if no matching attribute exists.</param>
    /// <returns>The attribute value</returns>
    public static string? GetAttribute(this XmlNode node, string attributeName, string? xPath = null, string? defaultValue = null) {
        XmlNode? realn;
        if (xPath == null)
            realn = node;
        else
            realn = node.SelectSingleNode(xPath);
        if (realn == null) return defaultValue;
        var attribute = realn.Attributes?[attributeName];
        if (attribute == null) return defaultValue;
        return attribute.InnerText ?? defaultValue;
    }

    /// <summary>Gets an attribute value converted to the specified data type</summary>
    /// <typeparam name="T">The desired return data type</typeparam>
    /// <param name="node">The node.</param>
    /// <param name="attributeName">The Name of the attribute.</param>
    /// <param name="xPath">xPath</param>
    /// <param name="defaultValue">The default value to be returned if no matching attribute exists.</param>
    /// <returns>The attribute value</returns>
    public static T? GetAttribute<T>(this XmlNode node, string attributeName, string? xPath = null, T? defaultValue = default) {
        var value = GetAttribute(node, attributeName, xPath);
        return ConvertString(value, defaultValue);
    }
    private static T? ConvertString<T>(string? value, T? defaultValue = default) {
        if (string.IsNullOrEmpty(value)) return defaultValue;
        if (typeof(T) == typeof(Type)) return (T?)(object?)Type.GetType(value, true);
        return value.To().Type(defaultValue);
    }

    /// <summary>Creates or updates an attribute with the passed value. </summary>
    /// <param name="node">The node.</param>
    /// <param name="name">The name.</param>
    /// <param name="value">The value.</param>
    public static void SetAttribute(this XmlNode node, string name, object? value) => SetAttribute(node, name, value?.ToString());

    /// <summary>Creates or updates an attribute with the passed value.</summary>
    /// <param name="node">The node.</param>
    /// <param name="name">The name.</param>
    /// <param name="value">The value.</param>
    public static void SetAttribute(this XmlNode node, string name, string? value) {
        if (value == null) return;
        var attribute = node.Attributes?[name, node.NamespaceURI] ?? node.OwnerDocument?.CreateAttribute(name, node.OwnerDocument.NamespaceURI);
        if (attribute == null) throw new ArgumentNullException(nameof(node), "Node attribute is null");
        node.Attributes?.Append(attribute);
        attribute.InnerText = value;
    }
}