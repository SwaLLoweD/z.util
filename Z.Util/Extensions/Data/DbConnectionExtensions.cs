﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Data;
using System.Linq;

namespace Z.Extensions;

/// <summary>Extension methods for all kind of ADO.NET DbConnections..</summary>
public static class ExtIDbConnection
{
    /// <summary>Returns true if the database connection is in one of the states received.</summary>
    public static bool IsInState(this IDbConnection connection, params ConnectionState[] states) =>
        connection != null && states?.Length > 0 && states.Any(x => (connection.State & x) == x);
    /// <summary>Open the Database connection if not already opened.</summary>
    public static void OpenIfNot(this IDbConnection connection) {
        if (!connection.IsInState(ConnectionState.Open)) connection.Open();
    }
}