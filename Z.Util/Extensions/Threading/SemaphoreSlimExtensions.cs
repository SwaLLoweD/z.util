﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Threading;

namespace Z.Extensions;
/// <summary>Extension methods for ReaderWriterSlim data type.</summary>
public static class ExtSemaphoreSlimExtensions
{
    /// <summary>Perform an action with locking</summary>
    public static void Lock(this SemaphoreSlim s, Action worker, int millisecondsTimeout = System.Threading.Timeout.Infinite, CancellationToken? cancellationToken = null) {
        s.Wait(millisecondsTimeout, cancellationToken ?? CancellationToken.None);
        try {
            worker();
        } finally {
            s.Release();
        }
    }

    /// <summary>Perform an action with locking</summary>
    public static T Lock<T>(this SemaphoreSlim s, Func<T> worker, int millisecondsTimeout = System.Threading.Timeout.Infinite, CancellationToken? cancellationToken = null) {
        s.Wait(millisecondsTimeout, cancellationToken ?? CancellationToken.None);
        try {
            return worker();
        } finally {
            s.Release();
        }
    }
    
    /// <summary>Perform an action with locking</summary>
    public static async Task LockAsync(this SemaphoreSlim s, Action worker, int millisecondsTimeout = System.Threading.Timeout.Infinite, CancellationToken? cancellationToken = null) {
        await s.WaitAsync(millisecondsTimeout, cancellationToken ?? CancellationToken.None);
        try {
            worker();
        } finally {
            s.Release();
        }
    }

    /// <summary>Perform an action with locking</summary>
    public static async Task<T> LockAsync<T>(this SemaphoreSlim s, Func<T> worker, int millisecondsTimeout = System.Threading.Timeout.Infinite, CancellationToken? cancellationToken = null) {
        await s.WaitAsync(millisecondsTimeout, cancellationToken ?? CancellationToken.None);
        try {
            return worker();
        } finally {
            s.Release();
        }
    }

    /// <summary>Perform an action with locking</summary>
    public static async Task LockAsync(this SemaphoreSlim s, Func<Task> worker, int millisecondsTimeout = System.Threading.Timeout.Infinite, CancellationToken? cancellationToken = null) {
        await s.WaitAsync(millisecondsTimeout, cancellationToken ?? CancellationToken.None);
        try {
            await worker();
        } finally {
            s.Release();
        }
    }
    
    /// <summary>Perform an action with locking</summary>
    public static async Task<T> LockAsync<T>(this SemaphoreSlim s, Func<Task<T>> worker, int millisecondsTimeout = System.Threading.Timeout.Infinite, CancellationToken? cancellationToken = null) {
        await s.WaitAsync(millisecondsTimeout, cancellationToken ?? CancellationToken.None);
        try {
            return await worker();
        } finally {
            s.Release();
        }
    }
}