﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Z.Extensions;

/// <summary>Extension methods for the FileInfo class</summary>
public static class ExtTask
{
    // /// <summary>
    // /// If you want to wait for an existing task and find out whether it completed or timed out, but don't want to cancel it if the timeout occurs:
    // /// </summary>
    // /// <param name="task"></param>
    // /// <param name="timeoutMilliseconds"></param>
    // /// <returns></returns>
    // /// <exception cref="ArgumentOutOfRangeException"></exception>
    // public static async Task<bool> TimedOutAsync(this Task task, int timeoutMilliseconds)
    // {
    //     if (timeoutMilliseconds < 0 || (timeoutMilliseconds > 0 && timeoutMilliseconds < 100)) { throw new ArgumentOutOfRangeException(); }
    //
    //     if (timeoutMilliseconds == 0) {
    //         return !task.IsCompleted; // timed out if not completed
    //     }
    //     var cts = new CancellationTokenSource();
    //     if (await Task.WhenAny( task, Task.Delay(timeoutMilliseconds, cts.Token)) == task) {
    //         cts.Cancel(); // task completed, get rid of timer
    //         await task; // test for exceptions or task cancellation
    //         return false; // did not timeout
    //     } else {
    //         return true; // did timeout
    //     }
    // }
    // /// <summary>
    // /// If you have a task already created that you want to cancel if a timeout occurs: 
    // /// </summary>
    // /// <param name="task"></param>
    // /// <param name="timeoutMilliseconds"></param>
    // /// <param name="taskCts"></param>
    // /// <typeparam name="T"></typeparam>
    // /// <returns></returns>
    // /// <exception cref="ArgumentOutOfRangeException"></exception>
    // public static async Task<T> CancelAfterAsync<T>(this Task<T> task, int timeoutMilliseconds, CancellationTokenSource taskCts)
    // {
    //     if (timeoutMilliseconds < 0 || (timeoutMilliseconds > 0 && timeoutMilliseconds < 100)) { throw new ArgumentOutOfRangeException(); }
    //
    //     var timerCts = new CancellationTokenSource();
    //     if (await Task.WhenAny(task, Task.Delay(timeoutMilliseconds, timerCts.Token)) == task) {
    //         timerCts.Cancel(); // task completed, get rid of timer
    //     } else {
    //         taskCts.Cancel(); // timer completed, get rid of task
    //     }
    //     return await task; // test for exceptions or task cancellation
    // }
    //
    // /// <summary>
    // /// If you want to start a work task and cancel the work if the timeout occurs: 
    // /// </summary>
    // /// <param name="actionAsync"></param>
    // /// <param name="timeoutMilliseconds"></param>
    // /// <typeparam name="T"></typeparam>
    // /// <returns></returns>
    // /// <exception cref="ArgumentOutOfRangeException"></exception>
    // public static async Task<T> CancelAfterAsync<T>( this Func<CancellationToken,Task<T>> actionAsync, int timeoutMilliseconds) {
    //     if (timeoutMilliseconds < 0 || (timeoutMilliseconds > 0 && timeoutMilliseconds < 100)) { throw new ArgumentOutOfRangeException(); }
    //
    //     var taskCts = new CancellationTokenSource();
    //     var timerCts = new CancellationTokenSource();
    //     Task<T> task = actionAsync(taskCts.Token);
    //     if (await Task.WhenAny(task, Task.Delay(timeoutMilliseconds, timerCts.Token)) == task) {
    //         timerCts.Cancel(); // task completed, get rid of timer
    //     } else {
    //         taskCts.Cancel(); // timer completed, get rid of task
    //     }
    //     return await task; // test for exceptions or task cancellation
    // }
    /// <summary>
    /// If you want to set a timeout limit for an existing task, and throw an exception if the timeout occurs:
    /// </summary>
    /// <param name="task"></param>
    /// <param name="timeoutMilliseconds"></param>
    /// <returns>Task result</returns>
    /// <exception cref="TimeoutException"></exception>
    public static async ValueTask TimeoutAfter(this ValueTask task, int timeoutMilliseconds) {
        if (timeoutMilliseconds < 0) {
            await task;
            return;
        }
        using var timeoutCancellationTokenSource = new CancellationTokenSource();
        var completedTask = await Task.WhenAny(task.AsTask(), Task.Delay(timeoutMilliseconds, timeoutCancellationTokenSource.Token));
        if (completedTask == task.AsTask()) {
            timeoutCancellationTokenSource.Cancel();
            await task; // Very important in order to propagate exceptions
        } else {
            throw new TimeoutException();
        }
    }
    // /// <summary>
    // /// If you want to wait for an existing task and find out whether it completed or timed out, but don't want to cancel it if the timeout occurs:
    // /// </summary>
    // /// <param name="task"></param>
    // /// <param name="timeoutMilliseconds"></param>
    // /// <returns></returns>
    // /// <exception cref="ArgumentOutOfRangeException"></exception>
    // public static async Task<bool> TimedOutAsync(this Task task, int timeoutMilliseconds)
    // {
    //     if (timeoutMilliseconds < 0 || (timeoutMilliseconds > 0 && timeoutMilliseconds < 100)) { throw new ArgumentOutOfRangeException(); }
    //
    //     if (timeoutMilliseconds == 0) {
    //         return !task.IsCompleted; // timed out if not completed
    //     }
    //     var cts = new CancellationTokenSource();
    //     if (await Task.WhenAny( task, Task.Delay(timeoutMilliseconds, cts.Token)) == task) {
    //         cts.Cancel(); // task completed, get rid of timer
    //         await task; // test for exceptions or task cancellation
    //         return false; // did not timeout
    //     } else {
    //         return true; // did timeout
    //     }
    // }
    // /// <summary>
    // /// If you have a task already created that you want to cancel if a timeout occurs: 
    // /// </summary>
    // /// <param name="task"></param>
    // /// <param name="timeoutMilliseconds"></param>
    // /// <param name="taskCts"></param>
    // /// <typeparam name="T"></typeparam>
    // /// <returns></returns>
    // /// <exception cref="ArgumentOutOfRangeException"></exception>
    // public static async Task<T> CancelAfterAsync<T>(this Task<T> task, int timeoutMilliseconds, CancellationTokenSource taskCts)
    // {
    //     if (timeoutMilliseconds < 0 || (timeoutMilliseconds > 0 && timeoutMilliseconds < 100)) { throw new ArgumentOutOfRangeException(); }
    //
    //     var timerCts = new CancellationTokenSource();
    //     if (await Task.WhenAny(task, Task.Delay(timeoutMilliseconds, timerCts.Token)) == task) {
    //         timerCts.Cancel(); // task completed, get rid of timer
    //     } else {
    //         taskCts.Cancel(); // timer completed, get rid of task
    //     }
    //     return await task; // test for exceptions or task cancellation
    // }
    //
    // /// <summary>
    // /// If you want to start a work task and cancel the work if the timeout occurs: 
    // /// </summary>
    // /// <param name="actionAsync"></param>
    // /// <param name="timeoutMilliseconds"></param>
    // /// <typeparam name="T"></typeparam>
    // /// <returns></returns>
    // /// <exception cref="ArgumentOutOfRangeException"></exception>
    // public static async Task<T> CancelAfterAsync<T>( this Func<CancellationToken,Task<T>> actionAsync, int timeoutMilliseconds) {
    //     if (timeoutMilliseconds < 0 || (timeoutMilliseconds > 0 && timeoutMilliseconds < 100)) { throw new ArgumentOutOfRangeException(); }
    //
    //     var taskCts = new CancellationTokenSource();
    //     var timerCts = new CancellationTokenSource();
    //     Task<T> task = actionAsync(taskCts.Token);
    //     if (await Task.WhenAny(task, Task.Delay(timeoutMilliseconds, timerCts.Token)) == task) {
    //         timerCts.Cancel(); // task completed, get rid of timer
    //     } else {
    //         taskCts.Cancel(); // timer completed, get rid of task
    //     }
    //     return await task; // test for exceptions or task cancellation
    // }
    /// <summary>
    /// If you want to set a timeout limit for an existing task, and throw an exception if the timeout occurs:
    /// </summary>
    /// <param name="task"></param>
    /// <param name="timeoutMilliseconds"></param>
    /// <returns>Task result</returns>
    /// <exception cref="TimeoutException"></exception>
    public static async Task TimeoutAfter(this Task task, int timeoutMilliseconds) {
        if (timeoutMilliseconds < 0) {
            await task;
            return;
        }
        using var timeoutCancellationTokenSource = new CancellationTokenSource();
        var completedTask = await Task.WhenAny(task, Task.Delay(timeoutMilliseconds, timeoutCancellationTokenSource.Token));
        if (completedTask == task) {
            timeoutCancellationTokenSource.Cancel();
            await task; // Very important in order to propagate exceptions
        } else {
            throw new TimeoutException();
        }
    }
    /// <summary>
    /// If you want to set a timeout limit for an existing task, and throw an exception if the timeout occurs:
    /// </summary>
    /// <param name="task"></param>
    /// <param name="timeoutMilliseconds"></param>
    /// <returns>Task result</returns>
    /// <exception cref="TimeoutException"></exception>
    public static async Task<TResult> TimeoutAfter<TResult>(this Task<TResult> task, int timeoutMilliseconds) {
        if (timeoutMilliseconds < 0) {
            return await task;
        }
        using var timeoutCancellationTokenSource = new CancellationTokenSource();
        var completedTask = await Task.WhenAny(task, Task.Delay(timeoutMilliseconds, timeoutCancellationTokenSource.Token));
        if (completedTask == task) {
            timeoutCancellationTokenSource.Cancel();
            return await task; // Very important in order to propagate exceptions
        } else {
            throw new TimeoutException();
        }
    }
    /// <summary>
    /// If you want to set a timeout limit for an existing task, and throw an exception if the timeout occurs:
    /// </summary>
    /// <param name="task"></param>
    /// <param name="timeoutMilliseconds"></param>
    /// <returns>Task result</returns>
    /// <exception cref="TimeoutException"></exception>
    public static async ValueTask<TResult> TimeoutAfter<TResult>(this ValueTask<TResult> task, int timeoutMilliseconds) {
        if (timeoutMilliseconds < 0) {
            return await task;
        }
        using var timeoutCancellationTokenSource = new CancellationTokenSource();
        var completedTask = await Task.WhenAny(task.AsTask(), Task.Delay(timeoutMilliseconds, timeoutCancellationTokenSource.Token));
        if (completedTask == task.AsTask()) {
            timeoutCancellationTokenSource.Cancel();
            return await task; // Very important in order to propagate exceptions
        } else {
            throw new TimeoutException();
        }
    }

    /// <summary>
    /// Executes an async TaskT method which has a void return value synchronously
    /// </summary>
    /// <param name="task">TaskT method to execute</param>
    /// <param name="state">State object</param>
    public static void RunSync(this Task task, object? state = null) {
        var oldContext = SynchronizationContext.Current;
        var synch = new ExclusiveSynchronizationContext();
        SynchronizationContext.SetSynchronizationContext(synch);
        synch.Post(async _ => {
            try {
                await task;
            } catch (Exception e) {
                synch.InnerException = e;
                throw;
            } finally {
                synch.EndMessageLoop();
            }
        }, state);
        synch.BeginMessageLoop();

        SynchronizationContext.SetSynchronizationContext(oldContext);
    }

    /// <summary>
    /// Executes an async TaskT method which has a T return type synchronously
    /// </summary>
    /// <typeparam name="T">Return Type</typeparam>
    /// <param name="task">TaskT method to execute</param>
    /// <param name="state">State object</param>
    /// <returns></returns>
    public static T? RunSync<T>(this Task<T> task, object? state = null) {
        var oldContext = SynchronizationContext.Current;
        var sync = new ExclusiveSynchronizationContext();
        SynchronizationContext.SetSynchronizationContext(sync);
        T? ret = default;
        sync.Post(async _ => {
            try {
                ret = await task;
            } catch (Exception e) {
                sync.InnerException = e;
                throw;
            } finally {
                sync.EndMessageLoop();
            }
        }, state);
        sync.BeginMessageLoop();
        SynchronizationContext.SetSynchronizationContext(oldContext);
        return ret;
    }

    /// <summary>Synch Context</summary>
    private class ExclusiveSynchronizationContext : SynchronizationContext
    {
        private bool done;
        /// <summary>Inner Exception</summary>
        public Exception? InnerException { get; set; }
        readonly AutoResetEvent workItemsWaiting = new(false);
        readonly Queue<Tuple<SendOrPostCallback, object?>> items = new();
        /// <summary>Send</summary>
        public override void Send(SendOrPostCallback d, object? state) { throw new NotSupportedException("We cannot send to our same thread"); }
        /// <summary>Post</summary>
        public override void Post(SendOrPostCallback d, object? state) {
            lock (items) {
                items.Enqueue(Tuple.Create(d, state));
            }
            workItemsWaiting.Set();
        }
        /// <summary>EndLoop</summary>
        public void EndMessageLoop() { Post(_ => done = true, null); }
        /// <summary>BeginLoop</summary>
        public void BeginMessageLoop() {
            while (!done) {
                Tuple<SendOrPostCallback, object?> ?task = null;
                lock (items) {
                    if (items.Count > 0) {
                        task = items.Dequeue();
                    }
                }
                if (task != null) {
                    task.Item1(task.Item2);
                    if (InnerException != null) // the method threw an exeption
                    {
                        throw new AggregateException("AsyncHelpers.Run method threw an exception.", InnerException);
                    }
                } else {
                    workItemsWaiting.WaitOne();
                }
            }
        }
        /// <summary>Copy</summary>
        public override SynchronizationContext CreateCopy() { return this; }

        //    private static TaskFactory _taskFactory = new
        //TaskFactory(CancellationToken.None,
        //            TaskCreationOptions.None,
        //            TaskContinuationOptions.None,
        //            TaskScheduler.Default);

        //    public static TResult RunSync<TResult>(this Func<Task<TResult>> func)
        //        => _taskFactory
        //            .StartNew(func)
        //            .Unwrap()
        //            .GetAwaiter()
        //            .GetResult();

        //    public static void RunSync(this Func<Task> func)
        //        => _taskFactory
        //            .StartNew(func)
        //            .Unwrap()
        //            .GetAwaiter()
        //            .GetResult();
    }
}