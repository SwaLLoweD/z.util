﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Threading;

namespace Z.Extensions;
/// <summary>Extension methods for ReaderWriterSlim data type.</summary>
public static class ExtReaderWriterLockSlim
{
    /// <summary>Perform a read action with locking</summary>
    public static void Read(this ReaderWriterLockSlim target, Action deleg) {
        target.EnterReadLock();
        try {
            deleg();
        } catch (Exception) {
            throw;
        } finally {
            target.ExitReadLock();
        }
    }
    /// <summary>Perform a read with locking</summary>
    public static T Read<T>(this ReaderWriterLockSlim target, Func<T> deleg) {
        target.EnterReadLock();
        T rval;
        try {
            rval = deleg();
        } catch (Exception) {
            throw;
        } finally {
            target.ExitReadLock();
        }
        return rval;
    }
    /// <summary>Perform a write with locking</summary>
    public static void Write(this ReaderWriterLockSlim target, Action deleg) {
        target.EnterWriteLock();
        try {
            deleg();
        } catch (Exception) {
            throw;
        } finally {
            target.ExitWriteLock();
        }
    }
    /// <summary>Perform a write with locking</summary>
    public static T Write<T>(this ReaderWriterLockSlim target, Func<T> deleg) {
        target.EnterWriteLock();
        T rval;
        try {
            rval = deleg();
        } catch (Exception) {
            throw;
        } finally {
            target.ExitWriteLock();
        }
        return rval;
    }
}