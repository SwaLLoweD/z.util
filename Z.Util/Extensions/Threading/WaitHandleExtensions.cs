﻿/* Copyright (c) <2020> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Threading;
using System.Threading.Tasks;

namespace Z.Extensions;

/// <summary>Extension methods for the WaitHandle class</summary>
public static class ExtWaitHandle
{
    /// <summary>WaitHandle class extension with cancellationToken</summary>
    public static bool WaitOne(this WaitHandle handle, int millisecondsTimeout = Timeout.Infinite, CancellationToken? cancellationToken = null) {
        cancellationToken ??= CancellationToken.None;
        int n = WaitHandle.WaitAny(new[] { handle, cancellationToken.Value.WaitHandle }, millisecondsTimeout);
        switch (n) {
            case WaitHandle.WaitTimeout:
                return false;
            case 0:
                return true;
            default:
                cancellationToken.Value.ThrowIfCancellationRequested();
                return false; // never reached
        }
    }
    /// <summary>WaitHandle class extension with cancellationToken</summary>
    public static bool WaitOne(this WaitHandle handle, TimeSpan? timeout = null, CancellationToken? cancellationToken = null) =>
        handle.WaitOne(timeout == null ? Timeout.Infinite : (int) timeout.Value.TotalMilliseconds, cancellationToken);

    /// <summary>async WaitHandle class extension with cancellationToken</summary>
    public static async Task<bool> WaitOneAsync(this WaitHandle handle, int millisecondsTimeout, CancellationToken? cancellationToken) {
        RegisteredWaitHandle? registeredHandle = null;
        CancellationTokenRegistration tokenRegistration = default;
        try {
            var tcs = new TaskCompletionSource<bool>();
            registeredHandle = ThreadPool.RegisterWaitForSingleObject(
                handle,
                (state, timedOut) => ((TaskCompletionSource<bool>)state!).TrySetResult(!timedOut),
                tcs,
                millisecondsTimeout,
                true);
            tokenRegistration = (cancellationToken ?? CancellationToken.None).Register(
                state => ((TaskCompletionSource<bool>)state!).TrySetCanceled(),
                tcs);
            return await tcs.Task;
        } finally {
            registeredHandle?.Unregister(null);
            tokenRegistration.Dispose();
        }
    }
    /// <summary>async WaitHandle class extension with cancellationToken</summary>
    public static async Task<bool> WaitOneAsync(this WaitHandle handle, TimeSpan? timeout = null, CancellationToken? cancellationToken = null) =>
        await handle.WaitOneAsync(timeout == null ? Timeout.Infinite : (int) timeout.Value.TotalMilliseconds, cancellationToken);
    /// <summary>async WaitHandle class extension with cancellationToken</summary>
    public static Task<bool> WaitOneAsync(this WaitHandle handle, CancellationToken? cancellationToken = null) => handle.WaitOneAsync(Timeout.Infinite, cancellationToken);
    /// <summary>async WaitHandle class extension</summary>
    public static Task<bool> WaitOneAsync(this WaitHandle handle, int millisecondsTimeout) => handle.WaitOneAsync(millisecondsTimeout, CancellationToken.None);
    /// <summary>async WaitHandle class extension</summary>
    public static Task<bool> WaitOneAsync(this WaitHandle handle, TimeSpan timeout) => handle.WaitOneAsync((int)timeout.TotalMilliseconds, CancellationToken.None);
    /// <summary>async WaitHandle class extension</summary>
    public static Task<bool> WaitOneAsync(this WaitHandle handle) => handle.WaitOneAsync(Timeout.Infinite, CancellationToken.None);
}