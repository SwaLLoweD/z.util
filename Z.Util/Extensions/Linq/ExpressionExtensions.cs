﻿//https://github.com/jbevain/mono.linq.expressions
//
// FluentExtensions.cs
//
// Author:
//   Jb Evain (jbevain@novell.com)
//
// (C) 2011 Novell, Inc. (http://www.novell.com)
// (C) 2012 Jb Evain
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// generated

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Z.Extensions;

#pragma warning disable RCS1138
/// <summary>Expressions</summary>
public static class ExtExpressions
{
    /// <summary>Is</summary>
    public static bool Is(this Expression self, ExpressionType type) {
        if (self == null) throw new ArgumentNullException(nameof(self));
        return self.NodeType == type;
    }
    /// <summary>Combine</summary>
    public static Expression<T> Combine<T>(this Expression<T> self, Func<Expression, Expression> combinator) where T : class {
        var parameters = ParametersFor(self);
        return Expression.Lambda<T>(combinator(RewriteBody(self, parameters)), parameters);
    }
    /// <summary>Combine</summary>
    public static Expression<T> Combine<T>(this Expression<T> self, Expression<T> expression, Func<Expression, Expression, Expression> combinator) where T : class {
        var parameters = ParametersFor(self);
        return Expression.Lambda<T>(combinator(RewriteBody(self, parameters), RewriteBody(expression, parameters)), parameters);
    }
    /// <summary>ParametersFor</summary>
    private static ParameterExpression[] ParametersFor(LambdaExpression lambda) => lambda.Parameters.Select(p => Expression.Parameter(p.Type, p.Name)).ToArray();
    /// <summary>RewriteBody</summary>
    private static Expression RewriteBody(LambdaExpression expression, IEnumerable<ParameterExpression> parameters) =>
        new ParameterRewriter(expression.Parameters, parameters).Visit(expression.Body);

    /// <summary>Assign</summary>
    public static BinaryExpression Assign(this Expression left, Expression right) => Expression.Assign(left, right);
    /// <summary>MakeBinary</summary>
    public static BinaryExpression MakeBinary(this ExpressionType binaryType, Expression left, Expression right) => Expression.MakeBinary(binaryType, left, right);
    /// <summary>MakeBinary</summary>
    public static BinaryExpression MakeBinary(this ExpressionType binaryType, Expression left, Expression right, bool liftToNull, MethodInfo method) =>
        Expression.MakeBinary(binaryType, left, right, liftToNull, method);
    /// <summary>MakeBinary</summary>
    public static BinaryExpression MakeBinary(this ExpressionType binaryType, Expression left, Expression right, bool liftToNull, MethodInfo method, LambdaExpression conversion) =>
        Expression.MakeBinary(binaryType, left, right, liftToNull, method, conversion);
    /// <summary>Equal</summary>
    public static BinaryExpression Equal(this Expression left, Expression right) => Expression.Equal(left, right);
    /// <summary>Equal</summary>
    public static BinaryExpression Equal(this Expression left, Expression right, bool liftToNull, MethodInfo method) => Expression.Equal(left, right, liftToNull, method);
    /// <summary>ReferenceEqual</summary>
    public static BinaryExpression ReferenceEqual(this Expression left, Expression right) => Expression.ReferenceEqual(left, right);
    /// <summary>NotEqual</summary>
    public static BinaryExpression NotEqual(this Expression left, Expression right) => Expression.NotEqual(left, right);
    /// <summary>NotEqual</summary>
    public static BinaryExpression NotEqual(this Expression left, Expression right, bool liftToNull, MethodInfo method) => Expression.NotEqual(left, right, liftToNull, method);
    /// <summary>ReferenceNotEqual</summary>
    public static BinaryExpression ReferenceNotEqual(this Expression left, Expression right) => Expression.ReferenceNotEqual(left, right);
    /// <summary>GreaterThan</summary>
    public static BinaryExpression GreaterThan(this Expression left, Expression right) => Expression.GreaterThan(left, right);
    /// <summary>GreaterThan</summary>
    public static BinaryExpression GreaterThan(this Expression left, Expression right, bool liftToNull, MethodInfo method) => Expression.GreaterThan(left, right, liftToNull, method);
    /// <summary>LessThan</summary>
    public static BinaryExpression LessThan(this Expression left, Expression right) => Expression.LessThan(left, right);
    /// <summary>LessThan</summary>
    public static BinaryExpression LessThan(this Expression left, Expression right, bool liftToNull, MethodInfo method) => Expression.LessThan(left, right, liftToNull, method);
    /// <summary>GreaterThanOrEqual</summary>
    public static BinaryExpression GreaterThanOrEqual(this Expression left, Expression right) => Expression.GreaterThanOrEqual(left, right);
    /// <summary>GreaterThanOrEqual</summary>
    public static BinaryExpression GreaterThanOrEqual(this Expression left, Expression right, bool liftToNull, MethodInfo method) => Expression.GreaterThanOrEqual(left, right, liftToNull, method);
    /// <summary>LessThanOrEqual</summary>
    public static BinaryExpression LessThanOrEqual(this Expression left, Expression right) => Expression.LessThanOrEqual(left, right);
    /// <summary>LessThanOrEqual</summary>
    public static BinaryExpression LessThanOrEqual(this Expression left, Expression right, bool liftToNull, MethodInfo method) => Expression.LessThanOrEqual(left, right, liftToNull, method);
    /// <summary>AndAlso</summary>
    public static BinaryExpression AndAlso(this Expression left, Expression right) => Expression.AndAlso(left, right);
    /// <summary>AndAlso</summary>
    public static BinaryExpression AndAlso(this Expression left, Expression right, MethodInfo method) => Expression.AndAlso(left, right, method);
    /// <summary>OrElse</summary>
    public static BinaryExpression OrElse(this Expression left, Expression right) => Expression.OrElse(left, right);
    /// <summary>OrElse</summary>
    public static BinaryExpression OrElse(this Expression left, Expression right, MethodInfo method) => Expression.OrElse(left, right, method);
    /// <summary>Coalesce</summary>
    public static BinaryExpression Coalesce(this Expression left, Expression right) => Expression.Coalesce(left, right);
    /// <summary>Coalesce</summary>
    public static BinaryExpression Coalesce(this Expression left, Expression right, LambdaExpression conversion) => Expression.Coalesce(left, right, conversion);
    /// <summary>Add</summary>
    public static BinaryExpression Add(this Expression left, Expression right) => Expression.Add(left, right);
    /// <summary>Add</summary>
    public static BinaryExpression Add(this Expression left, Expression right, MethodInfo method) => Expression.Add(left, right, method);
    /// <summary>AddAssign</summary>
    public static BinaryExpression AddAssign(this Expression left, Expression right) => Expression.AddAssign(left, right);
    /// <summary>AddAssign</summary>
    public static BinaryExpression AddAssign(this Expression left, Expression right, MethodInfo method) => Expression.AddAssign(left, right, method);
    /// <summary>AddAssign</summary>
    public static BinaryExpression AddAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) => Expression.AddAssign(left, right, method, conversion);
    /// <summary>AddAssignChecked</summary>
    public static BinaryExpression AddAssignChecked(this Expression left, Expression right) => Expression.AddAssignChecked(left, right);
    /// <summary>AddAssignChecked</summary>
    public static BinaryExpression AddAssignChecked(this Expression left, Expression right, MethodInfo method) => Expression.AddAssignChecked(left, right, method);
    /// <summary>AddAssignChecked</summary>
    public static BinaryExpression AddAssignChecked(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.AddAssignChecked(left, right, method, conversion);
    /// <summary>AddChecked</summary>
    public static BinaryExpression AddChecked(this Expression left, Expression right) => Expression.AddChecked(left, right);
    /// <summary>AddChecked</summary>
    public static BinaryExpression AddChecked(this Expression left, Expression right, MethodInfo method) => Expression.AddChecked(left, right, method);
    /// <summary>Subtract</summary>
    public static BinaryExpression Subtract(this Expression left, Expression right) => Expression.Subtract(left, right);
    /// <summary>Subtract</summary>
    public static BinaryExpression Subtract(this Expression left, Expression right, MethodInfo method) => Expression.Subtract(left, right, method);
    /// <summary>SubtractAssign</summary>
    public static BinaryExpression SubtractAssign(this Expression left, Expression right) => Expression.SubtractAssign(left, right);
    /// <summary>SubtractAssign</summary>
    public static BinaryExpression SubtractAssign(this Expression left, Expression right, MethodInfo method) => Expression.SubtractAssign(left, right, method);
    /// <summary>SubtractAssign</summary>
    public static BinaryExpression SubtractAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.SubtractAssign(left, right, method, conversion);
    /// <summary>SubtractAssignChecked</summary>
    public static BinaryExpression SubtractAssignChecked(this Expression left, Expression right) => Expression.SubtractAssignChecked(left, right);
    /// <summary>SubtractAssignChecked</summary>
    public static BinaryExpression SubtractAssignChecked(this Expression left, Expression right, MethodInfo method) => Expression.SubtractAssignChecked(left, right, method);
    /// <summary>SubtractAssignChecked</summary>
    public static BinaryExpression SubtractAssignChecked(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.SubtractAssignChecked(left, right, method, conversion);
    /// <summary>SubtractChecked</summary>
    public static BinaryExpression SubtractChecked(this Expression left, Expression right) => Expression.SubtractChecked(left, right);
    /// <summary>SubtractChecked</summary>
    public static BinaryExpression SubtractChecked(this Expression left, Expression right, MethodInfo method) => Expression.SubtractChecked(left, right, method);
    /// <summary>Divide</summary>
    public static BinaryExpression Divide(this Expression left, Expression right) => Expression.Divide(left, right);
    /// <summary>Divide</summary>
    public static BinaryExpression Divide(this Expression left, Expression right, MethodInfo method) => Expression.Divide(left, right, method);
    /// <summary>DivideAssign</summary>
    public static BinaryExpression DivideAssign(this Expression left, Expression right) => Expression.DivideAssign(left, right);
    /// <summary>DivideAssign</summary>
    public static BinaryExpression DivideAssign(this Expression left, Expression right, MethodInfo method) => Expression.DivideAssign(left, right, method);
    /// <summary>DivideAssign</summary>
    public static BinaryExpression DivideAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) => Expression.DivideAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression Modulo(this Expression left, Expression right) => Expression.Modulo(left, right);
    /// <summary></summary>
    public static BinaryExpression Modulo(this Expression left, Expression right, MethodInfo method) => Expression.Modulo(left, right, method);
    /// <summary></summary>
    public static BinaryExpression ModuloAssign(this Expression left, Expression right) => Expression.ModuloAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression ModuloAssign(this Expression left, Expression right, MethodInfo method) => Expression.ModuloAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression ModuloAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) => Expression.ModuloAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression Multiply(this Expression left, Expression right) => Expression.Multiply(left, right);
    /// <summary></summary>
    public static BinaryExpression Multiply(this Expression left, Expression right, MethodInfo method) => Expression.Multiply(left, right, method);
    /// <summary></summary>
    public static BinaryExpression MultiplyAssign(this Expression left, Expression right) => Expression.MultiplyAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression MultiplyAssign(this Expression left, Expression right, MethodInfo method) => Expression.MultiplyAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression MultiplyAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.MultiplyAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression MultiplyAssignChecked(this Expression left, Expression right) => Expression.MultiplyAssignChecked(left, right);
    /// <summary></summary>
    public static BinaryExpression MultiplyAssignChecked(this Expression left, Expression right, MethodInfo method) => Expression.MultiplyAssignChecked(left, right, method);
    /// <summary></summary>
    public static BinaryExpression MultiplyAssignChecked(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.MultiplyAssignChecked(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression MultiplyChecked(this Expression left, Expression right) => Expression.MultiplyChecked(left, right);
    /// <summary></summary>
    public static BinaryExpression MultiplyChecked(this Expression left, Expression right, MethodInfo method) => Expression.MultiplyChecked(left, right, method);
    /// <summary></summary>
    public static BinaryExpression LeftShift(this Expression left, Expression right) => Expression.LeftShift(left, right);
    /// <summary></summary>
    public static BinaryExpression LeftShift(this Expression left, Expression right, MethodInfo method) => Expression.LeftShift(left, right, method);
    /// <summary></summary>
    public static BinaryExpression LeftShiftAssign(this Expression left, Expression right) => Expression.LeftShiftAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression LeftShiftAssign(this Expression left, Expression right, MethodInfo method) => Expression.LeftShiftAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression LeftShiftAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.LeftShiftAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression RightShift(this Expression left, Expression right) => Expression.RightShift(left, right);
    /// <summary></summary>
    public static BinaryExpression RightShift(this Expression left, Expression right, MethodInfo method) => Expression.RightShift(left, right, method);
    /// <summary></summary>
    public static BinaryExpression RightShiftAssign(this Expression left, Expression right) => Expression.RightShiftAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression RightShiftAssign(this Expression left, Expression right, MethodInfo method) => Expression.RightShiftAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression RightShiftAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.RightShiftAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression And(this Expression left, Expression right) => Expression.And(left, right);
    /// <summary></summary>
    public static BinaryExpression And(this Expression left, Expression right, MethodInfo method) => Expression.And(left, right, method);
    /// <summary></summary>
    public static BinaryExpression AndAssign(this Expression left, Expression right) => Expression.AndAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression AndAssign(this Expression left, Expression right, MethodInfo method) => Expression.AndAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression AndAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) => Expression.AndAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression Or(this Expression left, Expression right) => Expression.Or(left, right);
    /// <summary></summary>
    public static BinaryExpression Or(this Expression left, Expression right, MethodInfo method) => Expression.Or(left, right, method);
    /// <summary></summary>
    public static BinaryExpression OrAssign(this Expression left, Expression right) => Expression.OrAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression OrAssign(this Expression left, Expression right, MethodInfo method) => Expression.OrAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression OrAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) => Expression.OrAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression ExclusiveOr(this Expression left, Expression right) => Expression.ExclusiveOr(left, right);
    /// <summary></summary>
    public static BinaryExpression ExclusiveOr(this Expression left, Expression right, MethodInfo method) => Expression.ExclusiveOr(left, right, method);
    /// <summary></summary>
    public static BinaryExpression ExclusiveOrAssign(this Expression left, Expression right) => Expression.ExclusiveOrAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression ExclusiveOrAssign(this Expression left, Expression right, MethodInfo method) => Expression.ExclusiveOrAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression ExclusiveOrAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) =>
        Expression.ExclusiveOrAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression Power(this Expression left, Expression right) => Expression.Power(left, right);
    /// <summary></summary>
    public static BinaryExpression Power(this Expression left, Expression right, MethodInfo method) => Expression.Power(left, right, method);
    /// <summary></summary>
    public static BinaryExpression PowerAssign(this Expression left, Expression right) => Expression.PowerAssign(left, right);
    /// <summary></summary>
    public static BinaryExpression PowerAssign(this Expression left, Expression right, MethodInfo method) => Expression.PowerAssign(left, right, method);
    /// <summary></summary>
    public static BinaryExpression PowerAssign(this Expression left, Expression right, MethodInfo method, LambdaExpression conversion) => Expression.PowerAssign(left, right, method, conversion);
    /// <summary></summary>
    public static BinaryExpression ArrayIndex(this Expression array, Expression index) => Expression.ArrayIndex(array, index);
    /// <summary></summary>
    public static BlockExpression Block(this Expression arg0, Expression arg1) => Expression.Block(arg0, arg1);
    /// <summary></summary>
    public static BlockExpression Block(this Expression arg0, Expression arg1, Expression arg2) => Expression.Block(arg0, arg1, arg2);
    /// <summary></summary>
    public static BlockExpression Block(this Expression arg0, Expression arg1, Expression arg2, Expression arg3) => Expression.Block(arg0, arg1, arg2, arg3);
    /// <summary></summary>
    public static BlockExpression Block(this Expression arg0, Expression arg1, Expression arg2, Expression arg3, Expression arg4) => Expression.Block(arg0, arg1, arg2, arg3, arg4);
    /// <summary></summary>
    public static BlockExpression Block(this IEnumerable<Expression> expressions) => Expression.Block(expressions);
    /// <summary></summary>
    public static BlockExpression Block(this Type type, params Expression[] expressions) => Expression.Block(type, expressions);
    /// <summary></summary>
    public static BlockExpression Block(this Type type, IEnumerable<Expression> expressions) => Expression.Block(type, expressions);
    /// <summary></summary>
    public static BlockExpression Block(this IEnumerable<ParameterExpression> variables, params Expression[] expressions) => Expression.Block(variables, expressions);
    /// <summary></summary>
    public static BlockExpression Block(this Type type, IEnumerable<ParameterExpression> variables, params Expression[] expressions) => Expression.Block(type, variables, expressions);
    /// <summary></summary>
    public static BlockExpression Block(this IEnumerable<ParameterExpression> variables, IEnumerable<Expression> expressions) => Expression.Block(variables, expressions);
    /// <summary></summary>
    public static BlockExpression Block(this Type type, IEnumerable<ParameterExpression> variables, IEnumerable<Expression> expressions) => Expression.Block(type, variables, expressions);
    /// <summary></summary>
    public static CatchBlock Catch(this Type type, Expression body) => Expression.Catch(type, body);
    /// <summary></summary>
    public static CatchBlock Catch(this ParameterExpression variable, Expression body) => Expression.Catch(variable, body);
    /// <summary></summary>
    public static CatchBlock Catch(this Type type, Expression body, Expression filter) => Expression.Catch(type, body, filter);
    /// <summary></summary>
    public static CatchBlock Catch(this ParameterExpression variable, Expression body, Expression filter) => Expression.Catch(variable, body, filter);
    /// <summary></summary>
    public static CatchBlock MakeCatchBlock(this Type type, ParameterExpression variable, Expression body, Expression filter) => Expression.MakeCatchBlock(type, variable, body, filter);
    /// <summary></summary>
    public static ConditionalExpression Condition(this Expression test, Expression ifTrue, Expression ifFalse) => Expression.Condition(test, ifTrue, ifFalse);
    /// <summary></summary>
    public static ConditionalExpression Condition(this Expression test, Expression ifTrue, Expression ifFalse, Type type) => Expression.Condition(test, ifTrue, ifFalse, type);
    /// <summary></summary>
    public static ConditionalExpression IfThen(this Expression test, Expression ifTrue) => Expression.IfThen(test, ifTrue);
    /// <summary></summary>
    public static ConditionalExpression IfThenElse(this Expression test, Expression ifTrue, Expression ifFalse) => Expression.IfThenElse(test, ifTrue, ifFalse);
    /// <summary></summary>
    public static ConstantExpression Constant(this object value) => Expression.Constant(value);
    /// <summary></summary>
    public static ConstantExpression Constant(this object value, Type type) => Expression.Constant(value, type);
    /// <summary></summary>
    public static DebugInfoExpression DebugInfo(this SymbolDocumentInfo document, int startLine, int startColumn, int endLine, int endColumn) =>
        Expression.DebugInfo(document, startLine, startColumn, endLine, endColumn);
    /// <summary></summary>
    public static DebugInfoExpression ClearDebugInfo(this SymbolDocumentInfo document) => Expression.ClearDebugInfo(document);
    /// <summary></summary>
    public static DefaultExpression Default(this Type type) => Expression.Default(type);
    /// <summary></summary>
    public static DynamicExpression MakeDynamic(this Type delegateType, CallSiteBinder binder, params Expression[] arguments) => Expression.MakeDynamic(delegateType, binder, arguments);
    /// <summary></summary>
    public static DynamicExpression MakeDynamic(this Type delegateType, CallSiteBinder binder, IEnumerable<Expression> arguments) => Expression.MakeDynamic(delegateType, binder, arguments);
    /// <summary></summary>
    public static DynamicExpression MakeDynamic(this Type delegateType, CallSiteBinder binder, Expression arg0) => Expression.MakeDynamic(delegateType, binder, arg0);
    /// <summary></summary>
    public static DynamicExpression MakeDynamic(this Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1) => Expression.MakeDynamic(delegateType, binder, arg0, arg1);
    /// <summary></summary>
    public static DynamicExpression MakeDynamic(this Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1, Expression arg2) =>
        Expression.MakeDynamic(delegateType, binder, arg0, arg1, arg2);
    /// <summary></summary>
    public static DynamicExpression MakeDynamic(this Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1, Expression arg2, Expression arg3) =>
        Expression.MakeDynamic(delegateType, binder, arg0, arg1, arg2, arg3);
    /// <summary></summary>
    public static DynamicExpression Dynamic(this CallSiteBinder binder, Type returnType, params Expression[] arguments) => Expression.Dynamic(binder, returnType, arguments);
    /// <summary></summary>
    public static DynamicExpression Dynamic(this CallSiteBinder binder, Type returnType, Expression arg0) => Expression.Dynamic(binder, returnType, arg0);
    /// <summary></summary>
    public static DynamicExpression Dynamic(this CallSiteBinder binder, Type returnType, Expression arg0, Expression arg1) => Expression.Dynamic(binder, returnType, arg0, arg1);
    /// <summary></summary>
    public static DynamicExpression Dynamic(this CallSiteBinder binder, Type returnType, Expression arg0, Expression arg1, Expression arg2) =>
        Expression.Dynamic(binder, returnType, arg0, arg1, arg2);
    /// <summary></summary>
    public static DynamicExpression Dynamic(this CallSiteBinder binder, Type returnType, Expression arg0, Expression arg1, Expression arg2, Expression arg3) =>
        Expression.Dynamic(binder, returnType, arg0, arg1, arg2, arg3);
    /// <summary></summary>
    public static DynamicExpression Dynamic(this CallSiteBinder binder, Type returnType, IEnumerable<Expression> arguments) => Expression.Dynamic(binder, returnType, arguments);
    /// <summary></summary>
    public static ElementInit ElementInit(this MethodInfo addMethod, params Expression[] arguments) => Expression.ElementInit(addMethod, arguments);
    /// <summary></summary>
    public static ElementInit ElementInit(this MethodInfo addMethod, IEnumerable<Expression> arguments) => Expression.ElementInit(addMethod, arguments);
    /// <summary></summary>
    public static GotoExpression Break(this LabelTarget target) => Expression.Break(target);
    /// <summary></summary>
    public static GotoExpression Break(this LabelTarget target, Expression value) => Expression.Break(target, value);
    /// <summary></summary>
    public static GotoExpression Break(this LabelTarget target, Type type) => Expression.Break(target, type);
    /// <summary></summary>
    public static GotoExpression Break(this LabelTarget target, Expression value, Type type) => Expression.Break(target, value, type);
    /// <summary></summary>
    public static GotoExpression Continue(this LabelTarget target) => Expression.Continue(target);
    /// <summary></summary>
    public static GotoExpression Continue(this LabelTarget target, Type type) => Expression.Continue(target, type);
    /// <summary></summary>
    public static GotoExpression Return(this LabelTarget target) => Expression.Return(target);
    /// <summary></summary>
    public static GotoExpression Return(this LabelTarget target, Type type) => Expression.Return(target, type);
    /// <summary></summary>
    public static GotoExpression Return(this LabelTarget target, Expression value) => Expression.Return(target, value);
    /// <summary></summary>
    public static GotoExpression Return(this LabelTarget target, Expression value, Type type) => Expression.Return(target, value, type);
    /// <summary></summary>
    public static GotoExpression Goto(this LabelTarget target) => Expression.Goto(target);
    /// <summary></summary>
    public static GotoExpression Goto(this LabelTarget target, Type type) => Expression.Goto(target, type);
    /// <summary></summary>
    public static GotoExpression Goto(this LabelTarget target, Expression value) => Expression.Goto(target, value);
    /// <summary></summary>
    public static GotoExpression Goto(this LabelTarget target, Expression value, Type type) => Expression.Goto(target, value, type);
    /// <summary></summary>
    public static GotoExpression MakeGoto(this GotoExpressionKind kind, LabelTarget target, Expression value, Type type) => Expression.MakeGoto(kind, target, value, type);
    /// <summary></summary>
    public static IndexExpression MakeIndex(this Expression instance, PropertyInfo indexer, IEnumerable<Expression> arguments) => Expression.MakeIndex(instance, indexer, arguments);
    /// <summary></summary>
    public static IndexExpression ArrayAccess(this Expression array, params Expression[] indexes) => Expression.ArrayAccess(array, indexes);
    /// <summary></summary>
    public static IndexExpression ArrayAccess(this Expression array, IEnumerable<Expression> indexes) => Expression.ArrayAccess(array, indexes);
    /// <summary></summary>
    public static IndexExpression Property(this Expression instance, string propertyName, params Expression[] arguments) => Expression.Property(instance, propertyName, arguments);
    /// <summary></summary>
    public static IndexExpression Property(this Expression instance, PropertyInfo indexer, params Expression[] arguments) => Expression.Property(instance, indexer, arguments);
    /// <summary></summary>
    public static IndexExpression Property(this Expression instance, PropertyInfo indexer, IEnumerable<Expression> arguments) => Expression.Property(instance, indexer, arguments);
    /// <summary></summary>
    public static InvocationExpression Invoke(this Expression expression, params Expression[] arguments) => Expression.Invoke(expression, arguments);
    /// <summary></summary>
    public static InvocationExpression Invoke(this Expression expression, IEnumerable<Expression> arguments) => Expression.Invoke(expression, arguments);
    /// <summary></summary>
    public static LabelExpression Label(this LabelTarget target) => Expression.Label(target);
    /// <summary></summary>
    public static LabelExpression Label(this LabelTarget target, Expression defaultValue) => Expression.Label(target, defaultValue);
    /// <summary></summary>
    public static LabelTarget Label(this string name) => Expression.Label(name);
    /// <summary></summary>
    public static LabelTarget Label(this Type type) => Expression.Label(type);
    /// <summary></summary>
    public static LabelTarget Label(this Type type, string name) => Expression.Label(type, name);
    /// <summary></summary>
    public static Expression<TDelegate> Lambda<TDelegate>(this Expression body, params ParameterExpression[] parameters) => Expression.Lambda<TDelegate>(body, parameters);
    /// <summary></summary>
    public static Expression<TDelegate> Lambda<TDelegate>(this Expression body, bool tailCall, params ParameterExpression[] parameters) => Expression.Lambda<TDelegate>(body, tailCall, parameters);
    /// <summary></summary>
    public static Expression<TDelegate> Lambda<TDelegate>(this Expression body, IEnumerable<ParameterExpression> parameters) => Expression.Lambda<TDelegate>(body, parameters);
    /// <summary></summary>
    public static Expression<TDelegate> Lambda<TDelegate>(this Expression body, bool tailCall, IEnumerable<ParameterExpression> parameters) =>
        Expression.Lambda<TDelegate>(body, tailCall, parameters);
    /// <summary></summary>
    public static Expression<TDelegate> Lambda<TDelegate>(this Expression body, string name, IEnumerable<ParameterExpression> parameters) => Expression.Lambda<TDelegate>(body, name, parameters);
    /// <summary></summary>
    public static Expression<TDelegate> Lambda<TDelegate>(this Expression body, string name, bool tailCall, IEnumerable<ParameterExpression> parameters) =>
        Expression.Lambda<TDelegate>(body, name, tailCall, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Expression body, params ParameterExpression[] parameters) => Expression.Lambda(body, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Expression body, bool tailCall, params ParameterExpression[] parameters) => Expression.Lambda(body, tailCall, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Expression body, IEnumerable<ParameterExpression> parameters) => Expression.Lambda(body, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Expression body, bool tailCall, IEnumerable<ParameterExpression> parameters) => Expression.Lambda(body, tailCall, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Type delegateType, Expression body, params ParameterExpression[] parameters) => Expression.Lambda(delegateType, body, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Type delegateType, Expression body, bool tailCall, params ParameterExpression[] parameters) =>
        Expression.Lambda(delegateType, body, tailCall, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Type delegateType, Expression body, IEnumerable<ParameterExpression> parameters) => Expression.Lambda(delegateType, body, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Type delegateType, Expression body, bool tailCall, IEnumerable<ParameterExpression> parameters) =>
        Expression.Lambda(delegateType, body, tailCall, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Expression body, string name, IEnumerable<ParameterExpression> parameters) => Expression.Lambda(body, name, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Expression body, string name, bool tailCall, IEnumerable<ParameterExpression> parameters) => Expression.Lambda(body, name, tailCall, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Type delegateType, Expression body, string name, IEnumerable<ParameterExpression> parameters) =>
        Expression.Lambda(delegateType, body, name, parameters);
    /// <summary></summary>
    public static LambdaExpression Lambda(this Type delegateType, Expression body, string name, bool tailCall, IEnumerable<ParameterExpression> parameters) =>
        Expression.Lambda(delegateType, body, name, tailCall, parameters);
    /// <summary></summary>
    public static ListInitExpression ListInit(this NewExpression newExpression, params Expression[] initializers) => Expression.ListInit(newExpression, initializers);
    /// <summary></summary>
    public static ListInitExpression ListInit(this NewExpression newExpression, IEnumerable<Expression> initializers) => Expression.ListInit(newExpression, initializers);
    /// <summary></summary>
    public static ListInitExpression ListInit(this NewExpression newExpression, MethodInfo addMethod, params Expression[] initializers) =>
        Expression.ListInit(newExpression, addMethod, initializers);
    /// <summary></summary>
    public static ListInitExpression ListInit(this NewExpression newExpression, MethodInfo addMethod, IEnumerable<Expression> initializers) =>
        Expression.ListInit(newExpression, addMethod, initializers);
    /// <summary></summary>
    public static ListInitExpression ListInit(this NewExpression newExpression, params ElementInit[] initializers) => Expression.ListInit(newExpression, initializers);
    /// <summary></summary>
    public static ListInitExpression ListInit(this NewExpression newExpression, IEnumerable<ElementInit> initializers) => Expression.ListInit(newExpression, initializers);
    /// <summary></summary>
    public static LoopExpression Loop(this Expression body) => Expression.Loop(body);
    /// <summary></summary>
    public static LoopExpression Loop(this Expression body, LabelTarget @break) => Expression.Loop(body, @break);
    /// <summary></summary>
    public static LoopExpression Loop(this Expression body, LabelTarget @break, LabelTarget @continue) => Expression.Loop(body, @break, @continue);
    /// <summary></summary>
    public static MemberAssignment Bind(this MemberInfo member, Expression expression) => Expression.Bind(member, expression);
    /// <summary></summary>
    public static MemberAssignment Bind(this MethodInfo propertyAccessor, Expression expression) => Expression.Bind(propertyAccessor, expression);
    /// <summary></summary>
    public static MemberExpression Field(this Expression expression, FieldInfo field) => Expression.Field(expression, field);
    /// <summary></summary>
    public static MemberExpression Field(this Expression expression, string fieldName) => Expression.Field(expression, fieldName);
    /// <summary></summary>
    public static MemberExpression Field(this Expression expression, Type type, string fieldName) => Expression.Field(expression, type, fieldName);
    /// <summary></summary>
    public static MemberExpression Property(this Expression expression, string propertyName) => Expression.Property(expression, propertyName);
    /// <summary></summary>
    public static MemberExpression Property(this Expression expression, Type type, string propertyName) => Expression.Property(expression, type, propertyName);
    /// <summary></summary>
    public static MemberExpression Property(this Expression expression, PropertyInfo property) => Expression.Property(expression, property);
    /// <summary></summary>
    public static MemberExpression Property(this Expression expression, MethodInfo propertyAccessor) => Expression.Property(expression, propertyAccessor);
    /// <summary></summary>
    public static MemberExpression PropertyOrField(this Expression expression, string propertyOrFieldName) => Expression.PropertyOrField(expression, propertyOrFieldName);
    /// <summary></summary>
    public static MemberExpression MakeMemberAccess(this Expression expression, MemberInfo member) => Expression.MakeMemberAccess(expression, member);
    /// <summary></summary>
    public static MemberInitExpression MemberInit(this NewExpression newExpression, params MemberBinding[] bindings) => Expression.MemberInit(newExpression, bindings);
    /// <summary></summary>
    public static MemberInitExpression MemberInit(this NewExpression newExpression, IEnumerable<MemberBinding> bindings) => Expression.MemberInit(newExpression, bindings);
    /// <summary></summary>
    public static MemberListBinding ListBind(this MemberInfo member, params ElementInit[] initializers) => Expression.ListBind(member, initializers);
    /// <summary></summary>
    public static MemberListBinding ListBind(this MemberInfo member, IEnumerable<ElementInit> initializers) => Expression.ListBind(member, initializers);
    /// <summary></summary>
    public static MemberListBinding ListBind(this MethodInfo propertyAccessor, params ElementInit[] initializers) => Expression.ListBind(propertyAccessor, initializers);
    /// <summary></summary>
    public static MemberListBinding ListBind(this MethodInfo propertyAccessor, IEnumerable<ElementInit> initializers) => Expression.ListBind(propertyAccessor, initializers);
    /// <summary></summary>
    public static MemberMemberBinding MemberBind(this MemberInfo member, params MemberBinding[] bindings) => Expression.MemberBind(member, bindings);
    /// <summary></summary>
    public static MemberMemberBinding MemberBind(this MemberInfo member, IEnumerable<MemberBinding> bindings) => Expression.MemberBind(member, bindings);
    /// <summary></summary>
    public static MemberMemberBinding MemberBind(this MethodInfo propertyAccessor, params MemberBinding[] bindings) => Expression.MemberBind(propertyAccessor, bindings);
    /// <summary></summary>
    public static MemberMemberBinding MemberBind(this MethodInfo propertyAccessor, IEnumerable<MemberBinding> bindings) => Expression.MemberBind(propertyAccessor, bindings);
    /// <summary></summary>
    public static MethodCallExpression Call(this MethodInfo method, Expression arg0) => Expression.Call(method, arg0);
    /// <summary></summary>
    public static MethodCallExpression Call(this MethodInfo method, Expression arg0, Expression arg1) => Expression.Call(method, arg0, arg1);
    /// <summary></summary>
    public static MethodCallExpression Call(this MethodInfo method, Expression arg0, Expression arg1, Expression arg2) => Expression.Call(method, arg0, arg1, arg2);
    /// <summary></summary>
    public static MethodCallExpression Call(this MethodInfo method, Expression arg0, Expression arg1, Expression arg2, Expression arg3) => Expression.Call(method, arg0, arg1, arg2, arg3);
    /// <summary></summary>
    public static MethodCallExpression Call(this MethodInfo method, Expression arg0, Expression arg1, Expression arg2, Expression arg3, Expression arg4) =>
        Expression.Call(method, arg0, arg1, arg2, arg3, arg4);
    /// <summary></summary>
    public static MethodCallExpression Call(this MethodInfo method, params Expression[] arguments) => Expression.Call(method, arguments);
    /// <summary></summary>
    public static MethodCallExpression Call(this MethodInfo method, IEnumerable<Expression> arguments) => Expression.Call(method, arguments);
    /// <summary></summary>
    public static MethodCallExpression Call(this Expression instance, MethodInfo method) => Expression.Call(instance, method);
    /// <summary></summary>
    public static MethodCallExpression Call(this Expression instance, MethodInfo method, params Expression[] arguments) => Expression.Call(instance, method, arguments);
    /// <summary></summary>
    public static MethodCallExpression Call(this Expression instance, MethodInfo method, Expression arg0, Expression arg1) => Expression.Call(instance, method, arg0, arg1);
    /// <summary></summary>
    public static MethodCallExpression Call(this Expression instance, MethodInfo method, Expression arg0, Expression arg1, Expression arg2) => Expression.Call(instance, method, arg0, arg1, arg2);
    /// <summary></summary>
    public static MethodCallExpression Call(this Expression instance, string methodName, Type[] typeArguments, params Expression[] arguments) =>
        Expression.Call(instance, methodName, typeArguments, arguments);
    /// <summary></summary>
    public static MethodCallExpression Call(this Type type, string methodName, Type[] typeArguments, params Expression[] arguments) => Expression.Call(type, methodName, typeArguments, arguments);
    /// <summary></summary>
    public static MethodCallExpression Call(this Expression instance, MethodInfo method, IEnumerable<Expression> arguments) => Expression.Call(instance, method, arguments);
    /// <summary></summary>
    public static MethodCallExpression ArrayIndex(this Expression array, params Expression[] indexes) => Expression.ArrayIndex(array, indexes);
    /// <summary></summary>
    public static MethodCallExpression ArrayIndex(this Expression array, IEnumerable<Expression> indexes) => Expression.ArrayIndex(array, indexes);
    /// <summary></summary>
    public static NewArrayExpression NewArrayInit(this Type type, params Expression[] initializers) => Expression.NewArrayInit(type, initializers);
    /// <summary></summary>
    public static NewArrayExpression NewArrayInit(this Type type, IEnumerable<Expression> initializers) => Expression.NewArrayInit(type, initializers);
    /// <summary></summary>
    public static NewArrayExpression NewArrayBounds(this Type type, params Expression[] bounds) => Expression.NewArrayBounds(type, bounds);
    /// <summary></summary>
    public static NewArrayExpression NewArrayBounds(this Type type, IEnumerable<Expression> bounds) => Expression.NewArrayBounds(type, bounds);
    /// <summary></summary>
    public static NewExpression New(this ConstructorInfo constructor) => Expression.New(constructor);
    /// <summary></summary>
    public static NewExpression New(this ConstructorInfo constructor, params Expression[] arguments) => Expression.New(constructor, arguments);
    /// <summary></summary>
    public static NewExpression New(this ConstructorInfo constructor, IEnumerable<Expression> arguments) => Expression.New(constructor, arguments);
    /// <summary></summary>
    public static NewExpression New(this ConstructorInfo constructor, IEnumerable<Expression> arguments, IEnumerable<MemberInfo> members) => Expression.New(constructor, arguments, members);
    /// <summary></summary>
    public static NewExpression New(this ConstructorInfo constructor, IEnumerable<Expression> arguments, params MemberInfo[] members) => Expression.New(constructor, arguments, members);
    /// <summary></summary>
    public static NewExpression New(this Type type) => Expression.New(type);
    /// <summary></summary>
    public static ParameterExpression Parameter(this Type type) => Expression.Parameter(type);
    /// <summary></summary>
    public static ParameterExpression Variable(this Type type) => Expression.Variable(type);
    /// <summary></summary>
    public static ParameterExpression Parameter(this Type type, string name) => Expression.Parameter(type, name);
    /// <summary></summary>
    public static ParameterExpression Variable(this Type type, string name) => Expression.Variable(type, name);
    /// <summary></summary>
    public static RuntimeVariablesExpression RuntimeVariables(this IEnumerable<ParameterExpression> variables) => Expression.RuntimeVariables(variables);
    /// <summary></summary>
    public static SwitchCase SwitchCase(this Expression body, params Expression[] testValues) => Expression.SwitchCase(body, testValues);
    /// <summary></summary>
    public static SwitchCase SwitchCase(this Expression body, IEnumerable<Expression> testValues) => Expression.SwitchCase(body, testValues);
    /// <summary></summary>
    public static SwitchExpression Switch(this Expression switchValue, params SwitchCase[] cases) => Expression.Switch(switchValue, cases);
    /// <summary></summary>
    public static SwitchExpression Switch(this Expression switchValue, Expression defaultBody, params SwitchCase[] cases) => Expression.Switch(switchValue, defaultBody, cases);
    /// <summary></summary>
    public static SwitchExpression Switch(this Expression switchValue, Expression defaultBody, MethodInfo comparison, params SwitchCase[] cases) =>
        Expression.Switch(switchValue, defaultBody, comparison, cases);
    /// <summary></summary>
    public static SwitchExpression Switch(this Type type, Expression switchValue, Expression defaultBody, MethodInfo comparison, params SwitchCase[] cases) =>
        Expression.Switch(type, switchValue, defaultBody, comparison, cases);
    /// <summary></summary>
    public static SwitchExpression Switch(this Expression switchValue, Expression defaultBody, MethodInfo comparison, IEnumerable<SwitchCase> cases) =>
        Expression.Switch(switchValue, defaultBody, comparison, cases);
    /// <summary></summary>
    public static SwitchExpression Switch(this Type type, Expression switchValue, Expression defaultBody, MethodInfo comparison, IEnumerable<SwitchCase> cases) =>
        Expression.Switch(type, switchValue, defaultBody, comparison, cases);
    /// <summary></summary>
    public static SymbolDocumentInfo SymbolDocument(this string fileName) => Expression.SymbolDocument(fileName);
    /// <summary></summary>
    public static SymbolDocumentInfo SymbolDocument(this string fileName, Guid language) => Expression.SymbolDocument(fileName, language);
    /// <summary></summary>
    public static SymbolDocumentInfo SymbolDocument(this string fileName, Guid language, Guid languageVendor) => Expression.SymbolDocument(fileName, language, languageVendor);
    /// <summary></summary>
    public static SymbolDocumentInfo SymbolDocument(this string fileName, Guid language, Guid languageVendor, Guid documentType) =>
        Expression.SymbolDocument(fileName, language, languageVendor, documentType);
    /// <summary></summary>
    public static TryExpression MakeTry(this Type type, Expression body, Expression @finally, Expression fault, IEnumerable<CatchBlock> handlers) =>
        Expression.MakeTry(type, body, @finally, fault, handlers);
    /// <summary></summary>
    public static TypeBinaryExpression TypeIs(this Expression expression, Type type) => Expression.TypeIs(expression, type);
    /// <summary></summary>
    public static TypeBinaryExpression TypeEqual(this Expression expression, Type type) => Expression.TypeEqual(expression, type);
    /// <summary></summary>
    public static UnaryExpression MakeUnary(this ExpressionType unaryType, Expression operand, Type type) => Expression.MakeUnary(unaryType, operand, type);
    /// <summary></summary>
    public static UnaryExpression MakeUnary(this ExpressionType unaryType, Expression operand, Type type, MethodInfo method) => Expression.MakeUnary(unaryType, operand, type, method);
    /// <summary></summary>
    public static UnaryExpression Negate(this Expression expression) => Expression.Negate(expression);
    /// <summary></summary>
    public static UnaryExpression Negate(this Expression expression, MethodInfo method) => Expression.Negate(expression, method);
    /// <summary></summary>
    public static UnaryExpression UnaryPlus(this Expression expression) => Expression.UnaryPlus(expression);
    /// <summary></summary>
    public static UnaryExpression UnaryPlus(this Expression expression, MethodInfo method) => Expression.UnaryPlus(expression, method);
    /// <summary></summary>
    public static UnaryExpression NegateChecked(this Expression expression) => Expression.NegateChecked(expression);
    /// <summary></summary>
    public static UnaryExpression NegateChecked(this Expression expression, MethodInfo method) => Expression.NegateChecked(expression, method);
    /// <summary></summary>
    public static UnaryExpression Not(this Expression expression) => Expression.Not(expression);
    /// <summary></summary>
    public static UnaryExpression Not(this Expression expression, MethodInfo method) => Expression.Not(expression, method);
    /// <summary></summary>
    public static UnaryExpression IsFalse(this Expression expression) => Expression.IsFalse(expression);
    /// <summary></summary>
    public static UnaryExpression IsFalse(this Expression expression, MethodInfo method) => Expression.IsFalse(expression, method);
    /// <summary></summary>
    public static UnaryExpression IsTrue(this Expression expression) => Expression.IsTrue(expression);
    /// <summary></summary>
    public static UnaryExpression IsTrue(this Expression expression, MethodInfo method) => Expression.IsTrue(expression, method);
    /// <summary></summary>
    public static UnaryExpression OnesComplement(this Expression expression) => Expression.OnesComplement(expression);
    /// <summary></summary>
    public static UnaryExpression OnesComplement(this Expression expression, MethodInfo method) => Expression.OnesComplement(expression, method);
    /// <summary></summary>
    public static UnaryExpression TypeAs(this Expression expression, Type type) => Expression.TypeAs(expression, type);
    /// <summary></summary>
    public static UnaryExpression Unbox(this Expression expression, Type type) => Expression.Unbox(expression, type);
    /// <summary></summary>
    public static UnaryExpression Convert(this Expression expression, Type type) => Expression.Convert(expression, type);
    /// <summary></summary>
    public static UnaryExpression Convert(this Expression expression, Type type, MethodInfo method) => Expression.Convert(expression, type, method);
    /// <summary></summary>
    public static UnaryExpression ConvertChecked(this Expression expression, Type type) => Expression.ConvertChecked(expression, type);
    /// <summary></summary>
    public static UnaryExpression ConvertChecked(this Expression expression, Type type, MethodInfo method) => Expression.ConvertChecked(expression, type, method);
    /// <summary></summary>
    public static UnaryExpression ArrayLength(this Expression array) => Expression.ArrayLength(array);
    /// <summary></summary>
    public static UnaryExpression Quote(this Expression expression) => Expression.Quote(expression);
    /// <summary></summary>
    public static UnaryExpression Rethrow(this Type type) => Expression.Rethrow(type);
    /// <summary></summary>
    public static UnaryExpression Throw(this Expression value) => Expression.Throw(value);
    /// <summary></summary>
    public static UnaryExpression Throw(this Expression value, Type type) => Expression.Throw(value, type);
    /// <summary></summary>
    public static UnaryExpression Increment(this Expression expression) => Expression.Increment(expression);
    /// <summary></summary>
    public static UnaryExpression Increment(this Expression expression, MethodInfo method) => Expression.Increment(expression, method);
    /// <summary></summary>
    public static UnaryExpression Decrement(this Expression expression) => Expression.Decrement(expression);
    /// <summary></summary>
    public static UnaryExpression Decrement(this Expression expression, MethodInfo method) => Expression.Decrement(expression, method);
    /// <summary></summary>
    public static UnaryExpression PreIncrementAssign(this Expression expression) => Expression.PreIncrementAssign(expression);
    /// <summary></summary>
    public static UnaryExpression PreIncrementAssign(this Expression expression, MethodInfo method) => Expression.PreIncrementAssign(expression, method);
    /// <summary></summary>
    public static UnaryExpression PreDecrementAssign(this Expression expression) => Expression.PreDecrementAssign(expression);
    /// <summary></summary>
    public static UnaryExpression PreDecrementAssign(this Expression expression, MethodInfo method) => Expression.PreDecrementAssign(expression, method);
    /// <summary></summary>
    public static UnaryExpression PostIncrementAssign(this Expression expression) => Expression.PostIncrementAssign(expression);
    /// <summary></summary>
    public static UnaryExpression PostIncrementAssign(this Expression expression, MethodInfo method) => Expression.PostIncrementAssign(expression, method);
    /// <summary></summary>
    public static UnaryExpression PostDecrementAssign(this Expression expression) => Expression.PostDecrementAssign(expression);
    /// <summary></summary>
    public static UnaryExpression PostDecrementAssign(this Expression expression, MethodInfo method) => Expression.PostDecrementAssign(expression, method);

    #region Nested type: ParameterRewriter
    /// <summary></summary>
    private class ParameterRewriter : ExpressionVisitor
    {
        #region Fields / Properties
        private readonly IDictionary<ParameterExpression, ParameterExpression> parameterMapping;
        #endregion

        #region Constructors
        public ParameterRewriter(IEnumerable<ParameterExpression> candidates, IEnumerable<ParameterExpression> replacements) => parameterMapping = ParametersMappingFor(candidates, replacements);
        #endregion

        private static IDictionary<ParameterExpression, ParameterExpression> ParametersMappingFor(IEnumerable<ParameterExpression> candidates, IEnumerable<ParameterExpression> replacements) =>
            candidates.Zip(replacements, (candidate, replacement) => new { candidate, replacement }).ToDictionary(t => t.candidate, t => t.replacement);

        protected override Expression VisitParameter(ParameterExpression expression) => parameterMapping.TryGetValue(expression, out var replacement) ? replacement : expression;
    }
    #endregion
}