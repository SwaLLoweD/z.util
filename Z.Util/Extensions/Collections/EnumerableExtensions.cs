﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Z.Collections.Concurrent;
using Z.Collections.Generic;
using Z.Collections.Helpers;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for all kind of Enumerables implementing the IEnumerable&lt;T&gt; interface</summary>
public static class ExtIEnumerable {
    /// <summary>Converts the specified Enumerable to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion<T> To<T>(this IEnumerable<T> value) => new(value);
    /// <summary>Checks the specified string for different conditions.</summary>
    /// <param name="value">The value to be checked.</param>
    /// <returns>An universal checker supplying additional target checking methods</returns>
    public static IsPortion<T> Is<T>(this IEnumerable<T> value) => new(value);
    /// <summary>Finds elements from the specified Enumerable.</summary>
    /// <param name="value">The value to be manipulated.</param>
    /// <returns>An universal find method supplying additional find methods</returns>
    public static FindPortion<T> Find<T>(this IEnumerable<T> value) => new(value);
    /// <summary>Removes elements from the specified Enumerable.</summary>
    /// <param name="value">The value to be manipulated.</param>
    /// <returns>An universal remove method supplying additional remove methods</returns>
    public static RemovePortion<T> Remove<T>(this IEnumerable<T> value) => new(value);
    /// <summary>Transform the specified Enumerable.</summary>
    /// <param name="value">The value to be transformed.</param>
    /// <returns>An universal transformer supplying additional transforming methods</returns>
    public static TransformPortion<T> Transform<T>(this IEnumerable<T> value) => new(value);

    #region Nested type: ConvertPortion
    #region Converter
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class ConvertPortion<T>: ExtObject.ConvertPortion<IEnumerable<T>> {
        #region Constructors
        /// <summary>Constructor</summary>
        public ConvertPortion(IEnumerable<T> value) : base(value) { }
        #endregion

        private IEnumerable<T> ValueEnum => Value as IEnumerable<T> ?? System.Array.Empty<T>();

        /// <summary>Convert To Array</summary>
        public T[] Array() => ValueEnum.ToArray();

        /// <summary>Convert To List</summary>
        public List<T> List() => ValueEnum.ToList();

        /// <summary>Convert To LinkedList</summary>
        public LinkedList<T> LinkedList() => new(ValueEnum);

        ///<summary>Convert To HashSet (duplicates omitted)</summary>
        public HashSet<T> HashSet() => new(ValueEnum);

        ///<summary>Convert to SortedSet</summary>
        public SortedSet<T> SortedSet() => new(ValueEnum);

        ///<summary>Convert to Queue</summary>
        public Queue<T> Queue() => new(ValueEnum);

        ///<summary>Convert to Stack</summary>
        public Stack<T> Stack() => new(ValueEnum);

        ///<summary>Convert to thread-safe Bag</summary>
        public ConcurrentBag<T> ConcurrentBag() => new(ValueEnum);

        ///<summary>Convert to thread-safe Queue</summary>
        public ConcurrentQueue<T> ConcurrentQueue() => new(ValueEnum);

        ///<summary>Convert to thread-sage Stack</summary>
        public ConcurrentStack<T> ConcurrentStack() => new(ValueEnum);

        /// <summary>
        ///     Given a non-generic IEnumerable interface, wrap a generic IEnumerable&lt;T&gt; interface around it. The
        ///     generic interface will enumerate the same objects as the underlying non-generic collection, but can be used in
        ///     places that require a generic interface. The underlying non-generic collection must contain only items that are of
        ///     type T/> or a type derived from it. This method is useful when interfacing older, non-generic collections to newer
        ///     code that uses generic interfaces.
        /// </summary>
        /// <remarks>
        ///     Some collections implement both generic and non-generic interfaces. For efficiency, this method will first
        ///     attempt to cast untypedCollection to IEnumerable&lt;T&gt;. If that succeeds, it is returned; otherwise, a wrapper
        ///     object is created.
        /// </remarks>
        /// <returns>
        ///     A generic IEnumerable&lt;T&gt; wrapper around untypedCollection/>. If untypedCollection is null, then null is
        ///     returned.
        /// </returns>
        public virtual IEnumerable<T> TypedEnumerable() {
            if (ValueEnum.GetType() == typeof(IEnumerable<T?>)) return ValueEnum;
            return Wrappers.TypedAs<T>(ValueEnum);
        }

        /// <summary>Convert To HashList</summary>
        public HashList<T> HashList() => new(ValueEnum);

        /// <summary>Convert To HashList</summary>
        public HashLinkedList<T> HashLinkedList() => new(ValueEnum);

        ///<summary>Convert to Bag</summary>
        public Bag<T> Bag() => new(ValueEnum);

        ///<summary>Convert to OrderedBag</summary>
        public OrderedBag<T> OrderedBag() => new(ValueEnum);

        ///<summary>Convert to PriorityQueue</summary>
        public PriorityQueue<T> PriorityQueue() => new(ValueEnum);

        ///<summary>Convert to thread-safe LinkedList</summary>
        public SyncLinkedList<T> ConcurrentLinkedList() => new(ValueEnum);

        ///<summary>Convert to thread-safe HashSet</summary>
        public SyncSet<T> ConcurrentSet() => new(ValueEnum);

        /// <summary>Turn the list of objects to a string of Common Seperated Value</summary>
        /// <param name="separator"></param>
        /// <returns></returns>
        /// <example>
        ///     <code>
        ///  		var values = new[] { 1, 2, 3, 4, 5 };
        /// 			string csv = values.ToCSV(';');
        ///  	</code>
        /// </example>
        /// <remarks>Contributed by Moses, http://mosesofegypt.net </remarks>
        public string CSV(string separator = ",") => ArrayString(false, null, separator, null);

        /// <summary>
        ///     Gets a string representation of the elements in the collection. The string to used at the beginning and end,
        ///     and to separate items, and supplied by parameters. Each item in the collection is converted to a string by calling
        ///     its ToString method (null is represented by "null").
        /// </summary>
        /// <param name="recursive">
        ///     If true, contained collections (except strings) are converted to strings by a recursive call to
        ///     this method, instead of by calling ToString.
        /// </param>
        /// <param name="start">The string to appear at the beginning of the output string.</param>
        /// <param name="separator">The string to appear between each item in the string.</param>
        /// <param name="end">The string to appear at the end of the output string.</param>
        /// <returns>The string representation of the collection. If collection is null, then the string "null" is returned.</returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="start" />, <paramref name="separator" />, or
        ///     <paramref name="end" /> is null.
        /// </exception>
        public virtual string ArrayString(bool recursive = true, string? start = "{", string separator = ",", string? end = "}") {
            var firstItem = true;

            var builder = new StringBuilder();

            if (start != null) builder.Append(start);

            // Call ToString on each item and put it in.
            foreach (var item in ValueEnum) {
                if (!firstItem) builder.Append(separator);

                if (item == null) {
                    builder.Append("null");
                } else if (recursive && item is IEnumerable itemEnum && item is not string) {
                    var lst = new List<object>();
                    foreach (var obj in itemEnum) lst.Add(obj);
                    builder.Append(lst.To().ArrayString(recursive, start, separator, end));
                } else {
                    builder.Append(item);
                }

                firstItem = false;
            }

            if (end != null) builder.Append(end);
            return builder.ToString();
        }

        ///<summary>Convert values into a key value pair to use in a dictionary using specified keyselector function</summary>
        ///<param name="keySelector">Key selection lambda</param>
        public virtual ExtKeyValuePair.ConvertPortion<TKey, T> KeyValue<TKey>(Func<T?, TKey> keySelector) where TKey : notnull => new(KeyValuePair(keySelector));
        /// <summary>
        ///     Convert values into a key value pair to use in a dictionary using specified keyselector and valueselector
        ///     function
        /// </summary>
        /// <param name="keySelector">Key selection lambda</param>
        /// <param name="valueSelector">Value selection lambda</param>
        public virtual ExtKeyValuePair.ConvertPortion<TKey, TValue> KeyValue<TKey, TValue>(Func<T?, TKey> keySelector, Func<T?, TValue> valueSelector) where TKey : notnull =>
            new(KeyValuePair(keySelector, valueSelector));
        ///<summary>Convert values into a key value pair to use in a dictionary using specified keyselector function</summary>
        ///<param name="keySelector">Key selection lambda</param>
        protected virtual IEnumerable<KeyValuePair<TKey, T>> KeyValuePair<TKey>(Func<T?, TKey> keySelector) {
            foreach (var t in ValueEnum) if (t != null) yield return new KeyValuePair<TKey, T>(keySelector(t), t);
        }
        /// <summary>
        ///     Convert values into a key value pair to use in a dictionary using specified keyselector and valueselector
        ///     function
        /// </summary>
        /// <param name="keySelector">Key selection lambda</param>
        /// <param name="valueSelector">Value selection lambda</param>
        protected virtual IEnumerable<KeyValuePair<TKey, TValue>> KeyValuePair<TKey, TValue>(Func<T?, TKey> keySelector, Func<T?, TValue> valueSelector) {
            foreach (var t in ValueEnum) yield return new KeyValuePair<TKey, TValue>(keySelector(t), valueSelector(t));
        }

        /// <summary>Convert the items in the enumerable to a specified type using default converters</summary>
        /// <typeparam name="U"></typeparam>
        /// <returns>An universal converter supplying additional target conversion methods</returns>
        public virtual IEnumerable<U> GenericType<U>() {
            if (Value == null) yield break;
            foreach (var sourceItem in Value) {
                var convItem = ZConverter.To(sourceItem, typeof(U));
                if (convItem != null) yield return (U)convItem;
            }
        }

        /// <summary>Convert the items in the enumerable to a specified type using default converters</summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="defaultValue">Default value to set if conversion fails</param>
        /// <returns>An universal converter supplying additional target conversion methods</returns>
        public virtual IEnumerable<U> GenericType<U>(U defaultValue) {
            if (Value == null) yield break;
            foreach (var sourceItem in Value) {
                var convItem = ZConverter.To(sourceItem, typeof(U), defaultValue);
                if (convItem == null) yield return defaultValue;
                else yield return (U)convItem;
            }
        }

        /// <summary>Convert the items in the enumerable to a specified type using a specified converter.</summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="converter">Converter to use</param>
        /// <returns>An universal converter supplying additional target conversion methods</returns>
        public virtual IEnumerable<U> GenericType<U>(Converter<T?, U> converter) {
            if (Value == null) yield break;
            foreach (var sourceItem in Value)
                yield return converter(sourceItem);
        }
    }
    #endregion Converter
    #endregion

    #region Nested type: IsPortion
    #region Is Checker
    /// <summary>Multi-method library for string condition check related commands.</summary>
    public class IsPortion<T>: ExtendablePortion<IEnumerable<T>>, IIsPortion<IEnumerable<T>> {
        #region Fields / Properties
        /// <summary>Determines whether the specified enumerable is null or empty (if string, only whitespace)</summary>
        public bool Empty {
            get {
                if (Value == null) return true;
                if (typeof(T).Equals<char>()) return (Value as string ?? string.Empty).Trim().Length == 0;
                return !Value.Any();
            }
        }
        /// <summary>Determines whether the specified enumerable is not null or empty.</summary>
        public bool NotEmpty => !Empty;
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public IsPortion(IEnumerable<T> value) : base(value) { }
        #endregion

        /// <summary>
        ///     Determines if the two collections contain equal items in the same order. The passed instance of
        ///     IEqualityComparer&lt;T&gt; is used for determining if two items are equal.
        /// </summary>
        /// <param name="collection2">The second collection to compare.</param>
        /// <param name="equalityComparer">
        ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
        ///     member function of this interface is called.
        /// </param>
        /// <returns>True if the collections have equal items in the same order. If both collections are empty, true is returned.</returns>
        public virtual bool EqualTo(IEnumerable<T> collection2, IEqualityComparer<T>? equalityComparer = null) {
            if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
            return EqualTo(collection2, (a, b) => equalityComparer.Equals(a, b));
        }
        /// <summary>
        ///     Determines if the two collections contain "equal" items in the same order. The passed BinaryPredicate is used
        ///     to determine if two items are "equal".
        /// </summary>
        /// <remarks>
        ///     Since an arbitrary BinaryPredicate is passed to this function, what is being tested for need not be equality. For
        ///     example, the following code determines if each integer in list1 is less than or equal to the corresponding integer
        ///     in list2.
        ///     <code>
        /// List&lt;int&gt; list1, list2;
        /// if (EqualCollections(list1, list2, delegate(int x, int y) { return x &lt;= y; }) {
        ///     // the check is true...
        /// }
        /// </code>
        /// </remarks>
        /// <param name="collection2">The second collection to compare.</param>
        /// <param name="predicate">
        ///     The BinaryPredicate used to compare items for "equality". This predicate can compute any
        ///     relation between two items; it need not represent equality or an equivalence relation.
        /// </param>
        /// <returns>
        ///     True if <paramref name="predicate" />returns true for each corresponding pair of items in the two collections.
        ///     If both collections are empty, true is returned.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     collection1, <paramref name="collection2" />, or <paramref name="predicate" />
        ///     is null.
        /// </exception>
        public virtual bool EqualTo(IEnumerable<T> collection2, Func<T, T, bool> predicate) {
            if (Value == null) {
                return collection2 == null;
            }
            if (collection2 == null) throw new ArgumentNullException(nameof(collection2));
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));

            using IEnumerator<T> enum1 = Value.GetEnumerator(), enum2 = collection2.GetEnumerator();
            bool continue1, continue2;

            for (; ; ) {
                continue1 = enum1.MoveNext();
                continue2 = enum2.MoveNext();
                if (!continue1 || !continue2) break;

                if (!predicate(enum1.Current, enum2.Current)) return false; // the two items are not equal.
            }

            // If both continue1 and continue2 are false, we reached the end of both sequences at the same
            // time and found success. If one is true and one is false, the sequences were of difference lengths -- failure.
            return continue1 == continue2;
        }
    }
    #endregion Is Checker
    #endregion

    #region Nested type: RemovePortion
    #region Remove
    /// <summary>Multi-method library for IEnumerable item removal related commands.</summary>
    public class RemovePortion<T>: ExtendablePortion<IEnumerable<T>> {
        #region Constructors
        /// <summary>Constructor</summary>
        public RemovePortion(IEnumerable<T> value) : base(value) { }
        #endregion

        /// <summary>
        ///     Remove consecutive equal items from a collection, yielding another collection. In each run of consecutive
        ///     equal items in the collection, all items after the first item in the run are removed. A passed IEqualityComparer is
        ///     used to determine equality.
        /// </summary>
        /// <param name="equalityComparer">
        ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
        ///     method will be called.
        /// </param>
        /// <returns>An new collection with the items from collection, in the same order, with consecutive duplicates removed.</returns>
        /// <exception cref="ArgumentNullException">collection or <paramref name="equalityComparer" /> is null.</exception>
        public virtual IEnumerable<T> Duplicates(IEqualityComparer<T>? equalityComparer = null) {
            if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
            return Duplicates(equalityComparer.Equals);
        }

        /// <summary>
        ///     Remove consecutive "equal" items from a collection, yielding another collection. In each run of consecutive
        ///     equal items in the collection, all items after the first item in the run are removed. The passed BinaryPredicate is
        ///     used to determine if two items are "equal".
        /// </summary>
        /// <remarks>
        ///     Since an arbitrary BinaryPredicate is passed to this function, what is being removed need not be true
        ///     equality.
        /// </remarks>
        /// <param name="predicate">
        ///     The BinaryPredicate used to compare items for "equality". An item <c>current</c> is removed if
        ///     <c>predicate(first, current)==true</c>, where <c>first</c> is the first item in the group of "duplicate" items.
        /// </param>
        /// <returns>An new collection with the items from collection, in the same order, with consecutive "duplicates" removed.</returns>
        public virtual IEnumerable<T> Duplicates(Func<T?, T, bool> predicate) {
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));

            return Duplicates2();

            IEnumerable<T> Duplicates2() {
                if (Value == null) yield break;

                var current = default(T);
                var atBeginning = true;

                foreach (var item in Value) {
                    // Is the new item different from the current item?
                    if (atBeginning || !predicate(current, item)) {
                        current = item;
                        yield return item;
                    }

                    atBeginning = false;
                }
            }
        }

        /// <summary>Remove an item from the collection with predicate</summary>
        /// <param name="predicate"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <remarks>Contributed by Michael T, http://about.me/MichaelTran </remarks>
        public virtual IEnumerable<T> Where(Predicate<T> predicate) {
            if (Value == null) yield break;
            var deleteList = Value.Where(child => predicate(child)).ToList();
            foreach (var item in deleteList) {
                if (!predicate(item))
                    yield return item;
            }
        }
    }
    #endregion Remove
    #endregion

    #region Nested type: TransformPortion
    #region Transform
    /// <summary>Multi-method library for IEnumerable transformation related commands.</summary>
    public class TransformPortion<T>: ExtendablePortion<IEnumerable<T>> {
        #region Constructors
        /// <summary>Constructors</summary>
        public TransformPortion(IEnumerable<T> value) : base(value) { }
        #endregion

        /// <summary>
        ///     Copies at most <paramref name="count" /> items from the collection source to the list <paramref name="dest" />
        ///     , starting at the index <paramref name="destIndex" />. If necessary, the size of the destination list is expanded.
        ///     The source collection must not be the destination list or part thereof.
        /// </summary>
        /// <param name="dest">The list to store the items into.</param>
        /// <param name="destIndex">The index to begin copying items to.</param>
        /// <param name="count">The maximum number of items to copy.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="destIndex" /> is negative or greater than
        ///     <paramref name="dest" />.Count
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="count" /> is negative.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="dest" /> is null.</exception>
        public void Copy(IList<T> dest, int destIndex = 0, int count = 0) {
            var source = Value;
            if (source == null) return;
            if (dest == null) throw new ArgumentNullException(nameof(dest));
            if (dest.IsReadOnly) throw new ArgumentException("List is read-only", nameof(dest));

            var destCount = dest.Count;

            if (destIndex < 0) destIndex = 0;
            if (destIndex > destCount) throw new ArgumentOutOfRangeException(nameof(destIndex));
            if (count <= 0) count = int.MaxValue;

            using var sourceEnum = source.GetEnumerator();
            // First, overwrite items to the end of the destination list.
            while (destIndex < destCount && count > 0 && sourceEnum.MoveNext()) {
                dest[destIndex++] = sourceEnum.Current;
                --count;
            }

            // Second, insert items until done.
            while (count > 0 && sourceEnum.MoveNext()) {
                dest.Insert(destCount++, sourceEnum.Current);
                --count;
            }
        }

        /// <summary>
        ///     Copies at most <paramref name="count" /> items from the collection source to the array
        ///     <paramref name="dest" />, starting at the index <paramref name="destIndex" />. The source collection must not be
        ///     the destination array or part thereof.
        /// </summary>
        /// <param name="dest">The array to store the items into.</param>
        /// <param name="destIndex">The index to begin copying items to.</param>
        /// <param name="count">The maximum number of items to copy. The array must be large enought to fit this number of items.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="destIndex" /> is negative or greater than
        ///     <paramref name="dest" />.Length.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="count" /> is negative or <paramref name="destIndex" /> +
        ///     <paramref name="count" /> is greater than <paramref name="dest" />.Length.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="dest" /> is null.</exception>
        public void Copy(T[] dest, int destIndex = 0, int count = 0) {
            var source = Value;
            if (source == null) return;
            if (dest == null) throw new ArgumentNullException(nameof(dest));

            var destCount = dest.Length;

            if (destIndex < 0) destIndex = 0;
            if (destIndex > destCount) throw new ArgumentOutOfRangeException(nameof(destIndex));
            if (count <= 0) count = destCount - destIndex; //Max possible
            if (count < 0 || destIndex + count > destCount) throw new ArgumentOutOfRangeException(nameof(count));

            using var sourceEnum = source.GetEnumerator();
            // First, overwrite items to the end of the destination array.
            while (destIndex < destCount && count > 0 && sourceEnum.MoveNext()) {
                dest[destIndex++] = sourceEnum.Current;
                --count;
            }
        }

        /// <summary>Scramble the enumerable using specified key.</summary>
        public virtual T[] Scramble(int? key = null) => Value == null ? Array.Empty<T>() : Scrambler.Scramble(Value, key);

        /// <summary>Unscramble the enumerable using specified key.</summary>
        public virtual T[] Unscramble(int? key = null) => Value == null ? Array.Empty<T>() : Scrambler.Unscramble(Value, key);

        /// <summary>Randomly shuffles the items in a collection, yielding a new collection.</summary>
        /// <param name="randomGenerator">The random number generator to use to select the random order.</param>
        /// <returns>An array with the same size and items as collection, but the items in a randomly chosen order.</returns>
        public virtual IList<T> RandomShuffle(Random? randomGenerator = null) {
            // We have to copy all items anyway, and there isn't a way to produce the items
            // on the fly that is linear. So copying to an array and shuffling it is an efficient as we can get.
            randomGenerator ??= ZUtilCfg.Random;

            if (Value == null) return Array.Empty<T>();
            var array = Value.ToArray();

            var count = array.Length;
            for (var i = count - 1; i >= 1; --i) {
                // Pick an random number 0 through i inclusive.
                var j = randomGenerator.Next(i + 1);

                // Swap array[i] and array[j]
                (array[j], array[i]) = (array[i], array[j]);
            }

            return array;
        }

        /// <summary>
        ///     Picks a random subset of <paramref name="count" /> items from collection, and places those items into a random
        ///     order. No item is selected more than once.
        /// </summary>
        /// <remarks>
        ///     If the collection implements IList&lt;T&gt;, then this method takes time O(<paramref name="count" />).
        ///     Otherwise, this method takes time O(N), where N is the number of items in the collection.
        /// </remarks>
        /// <param name="count">The number of items in the subset to choose.</param>
        /// <param name="randomGenerator">The random number generates used to make the selection.</param>
        /// <returns>An array of <paramref name="count" /> items, selected at random from collection.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="count" /> is negative or greater than list/>.Count.</exception>
        public virtual T[] RandomSubset(int count, Random? randomGenerator = null) {
            randomGenerator ??= ZUtilCfg.Random;
            if (Value == null) return Array.Empty<T>();
            // We need random access to the items in the collection. If it's not already an
            // IList<T>, copy to a temporary list.
            var list = Value as IList<T> ?? new List<T>(Value);

            var listCount = list.Count;
            if (count < 0 || count > listCount) throw new ArgumentOutOfRangeException(nameof(count));

            var result = new T[count]; // the result array.
            var swappedValues = new Dictionary<int, T>(count); // holds swapped values from the list.

            for (var i = 0; i < count; ++i) {
                // Set j to the index of the item to swap with, and value to the value to swap with.
                var j = randomGenerator.Next(listCount - i) + i;

                // Swap values of i and j in the list. The list isn't actually changed; instead,
                // swapped values are stored in the dictionary swappedValues.
                if (!swappedValues.TryGetValue(j, out var value)) value = list[j];

                result[i] = value;
                if (i != j) {
                    if (swappedValues.TryGetValue(i, out value))
                        swappedValues[j] = value;
                    else
                        swappedValues[j] = list[i];
                }
            }

            return result;
        }

        /// <summary>
        ///     Generates all the possible permutations of the items in collection. If collection has N items, then N
        ///     factorial permutations will be generated. This method does not compare the items to determine if any of them are
        ///     equal. If some items are equal, the same permutation may be generated more than once. For example, if the
        ///     collections contains the three items A, A, and B, then this method will generate the six permutations, AAB, AAB,
        ///     ABA, ABA, BAA, BAA (not necessarily in that order). To take equal items into account, use the
        ///     GenerateSortedPermutations method.
        /// </summary>
        /// <returns>
        ///     An IEnumerable&lt;T[]&gt; that enumerations all the possible permutations of the items in collection. Each
        ///     permutations is returned as an array. The items in the array should be copied if they need to be used after the
        ///     next permutation is generated; each permutation may reuse the same array instance.
        /// </returns>
        public IEnumerable<T[]> GeneratePermutations() {
            if (Value == null) yield break;
            var array = Value.ToArray();

            if (array.Length == 0) yield break;

            var state = new int[array.Length - 1];
            var maxLength = state.Length;

            yield return array;

            if (array.Length == 1) yield break;

            // The following algorithm makes two swaps for each
            // permutation generated.
            // This is not optimal in terms of number of swaps, but
            // is still O(1), and shorter and clearer to understand.
            var i = 0;
            T temp;
            for (; ; ) {
                if (state[i] < i + 1) {
                    if (state[i] > 0) {
                        temp = array[i + 1];
                        array[i + 1] = array[state[i] - 1];
                        array[state[i] - 1] = temp;
                    }

                    temp = array[i + 1];
                    array[i + 1] = array[state[i]];
                    array[state[i]] = temp;

                    yield return array;

                    ++state[i];
                    i = 0;
                } else {
                    temp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = temp;

                    state[i] = 0;
                    ++i;
                    if (i >= maxLength) yield break;
                }
            }
        }

        /// <summary>
        ///     Generates all the possible permutations of the items in collection, in lexicographical order. A supplied
        ///     IComparer&lt;T&gt; instance is used to compare the items. Even if some items are equal, the same permutation will
        ///     not be generated more than once. For example, if the collections contains the three items A, A, and B, then this
        ///     method will generate only the three permutations, AAB, ABA, BAA.
        /// </summary>
        /// <param name="comparer">The IComparer&lt;T&gt; used to compare the items.</param>
        /// <returns>
        ///     An IEnumerable&lt;T[]&gt; that enumerations all the possible permutations of the items in ollection. Each
        ///     permutations is returned as an array. The items in the array should be copied if they need to be used after the
        ///     next permutation is generated; each permutation may reuse the same array instance.
        /// </returns>
        public IEnumerable<T[]> GenerateSortedPermutations(IComparer<T>? comparer = null) {
            if (comparer == null) comparer = Comparer<T>.Default;
            if (Value == null) yield break;
            var array = Value.ToArray();
            var length = array.Length;
            if (length == 0) yield break;

            Array.Sort(array, comparer);

            yield return array;
            if (length == 1) yield break;

            // Keep generating the next permutation until we're done. Algorithm is
            // due to Jeffrey A. Johnson ("SEPA - a Simple Efficient Permutation Algorithm")
            int key, swap, i, j;
            T temp;
            for (; ; ) {
                // Find the key point -- where array[key]<array[key+1]. Everything after the
                // key is the tail.
                key = length - 2;
                while (comparer.Compare(array[key], array[key + 1]) >= 0) {
                    --key;
                    if (key < 0) yield break;
                }

                // Find the last item in the tail less than key.
                swap = length - 1;
                while (comparer.Compare(array[swap], array[key]) <= 0) --swap;

                // Swap it with the key.
                temp = array[key];
                array[key] = array[swap];
                array[swap] = temp;

                // Reverse the tail.
                i = key + 1;
                j = length - 1;
                while (i < j) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    ++i;
                    --j;
                }

                yield return array;
            }
        }
        /// <summary>
        ///     Computes the cartestian product of two collections: all possible pairs of items, with the first item taken
        ///     from the first collection and the second item taken from the second collection. If the first collection has N
        ///     items, and the second collection has M items, the cartesian product will have N * M pairs.
        /// </summary>
        /// <typeparam name="TSecond">The type of items in the second collection.</typeparam>
        /// <param name="second">The second collection.</param>
        /// <returns>
        ///     An IEnumerable&lt;Pair&lt;TFirst, TSecond&gt;&gt; that enumerates the cartesian product of the two
        ///     collections.
        /// </returns>
        public IEnumerable<KeyValuePair<T, TSecond>> CartesianProduct<TSecond>(IEnumerable<TSecond> second) {
            if (second == null) throw new ArgumentNullException(nameof(second));

            return CartesianProduct2();

            IEnumerable<KeyValuePair<T, TSecond>> CartesianProduct2() {
                if (Value == null) yield break;
                foreach (var itemFirst in Value) {
                    foreach (var itemSecond in second) {
                        yield return new KeyValuePair<T, TSecond>(itemFirst, itemSecond);
                    }
                }
            }
        }

        /// <summary>
        ///     Concatenates all the items from several collections. The collections need not be of the same type, but must
        ///     have the same item type.
        /// </summary>
        /// <param name="collections">
        ///     The set of collections to concatenate. In many languages, this parameter can be specified as
        ///     several individual parameters.
        /// </param>
        /// <returns>An IEnumerable that enumerates all the items in each of the collections, in order.</returns>
        public IEnumerable<T> Join(params IEnumerable<T>[] collections) => Join(null, collections);

        /// <summary>
        ///     Concatenates all the items from several collections. The collections need not be of the same type, but must
        ///     have the same item type.
        /// </summary>
        /// <param name="condition">Condition under with the item will be added</param>
        /// <param name="collections">
        ///     The set of collections to concatenate. In many languages, this parameter can be specified as
        ///     several individual parameters.
        /// </param>
        /// <returns>An IEnumerable that enumerates all the items in each of the collections, in order.</returns>
        public IEnumerable<T> Join(Func<T, bool>? condition, params IEnumerable<T>[] collections) {
            if (collections == null) throw new ArgumentNullException(nameof(collections));

            return Join2();

            IEnumerable<T> Join2() {
                if (Value != null) {
                    foreach (var item in Value) {
                        if (condition != null && !condition(item)) continue;
                        yield return item;
                    }
                }

                foreach (var coll in collections) {
                    foreach (var item in coll) {
                        if (condition != null && !condition(item)) continue;
                        yield return item;
                    }
                }
            }
        }

        /// <summary>
        ///     Replace all items in a collection equal to a particular value with another values, yielding another
        ///     collection. A passed IEqualityComparer is used to determine equality.
        /// </summary>
        /// <param name="itemFind">The value to find and replace within collection.</param>
        /// <param name="replaceWith">The new value to replace with.</param>
        /// <param name="equalityComparer">
        ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
        ///     method will be called.
        /// </param>
        /// <returns>An new collection with the items from collection, in the same order, with the appropriate replacements made.</returns>
        public virtual IEnumerable<T> Replace(T itemFind, T replaceWith, IEqualityComparer<T>? equalityComparer = null) {
            if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
            if (Value == null) yield break;
            foreach (var item in Value) {
                if (equalityComparer.Equals(item, itemFind))
                    yield return replaceWith;
                else
                    yield return item;
            }
        }

        /// <summary>
        ///     Replace all items in a collection that a predicate evalues at true with a value, yielding another collection.
        ///     .
        /// </summary>
        /// <param name="predicate">
        ///     The predicate used to evaluate items with the collection. If the predicate returns true for a
        ///     particular item, the item is replaces with <paramref name="replaceWith" />.
        /// </param>
        /// <param name="replaceWith">The new value to replace with.</param>
        /// <returns>An new collection with the items from collection, in the same order, with the appropriate replacements made.</returns>
        public virtual IEnumerable<T> Replace(Predicate<T> predicate, T replaceWith) {
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));

            return Replace2();

            IEnumerable<T> Replace2() {
                if (Value == null) yield break;
                foreach (var item in Value) {
                    if (predicate(item))
                        yield return replaceWith;
                    else
                        yield return item;
                }
            }
        }

        #region MoreLinq
        #region Batch
        /// <summary>Batches the source sequence into sized buckets.</summary>
        /// <param name="size">Size of buckets.</param>
        /// <returns>A sequence of equally sized buckets containing elements of the source collection.</returns>
        /// <remarks>This operator uses deferred execution and streams its results (buckets and bucket content).</remarks>
        public IEnumerable<IEnumerable<T>> Batch(int size) => Batch(size, x => x);

        /// <summary>Batches the source sequence into sized buckets and applies a projection to each bucket.</summary>
        /// <typeparam name="TResult">Type of result returned by <paramref name="resultSelector" />.</typeparam>
        /// <param name="size">Size of buckets.</param>
        /// <param name="resultSelector">The projection to apply to each bucket.</param>
        /// <returns>A sequence of projections on equally sized buckets containing elements of the source collection.</returns>
        /// <remarks>This operator uses deferred execution and streams its results (buckets and bucket content).</remarks>
        public IEnumerable<TResult> Batch<TResult>(int size, Func<IEnumerable<T>, TResult> resultSelector) {
            if (size <= 0) throw new ArgumentOutOfRangeException(nameof(size));
            if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

            return _();
            IEnumerable<TResult> _() {
                T[]? bucket = null;
                var count = 0;

                if (Value != null) {
                    foreach (var item in Value) {
                        if (bucket == null) bucket = new T[size];

                        bucket[count++] = item;

                        // The bucket is fully buffered before it's yielded
                        if (count != size) continue;

                        // Select is necessary so bucket contents are streamed too
                        yield return resultSelector(bucket);

                        bucket = null;
                        count = 0;
                    }
                }

                // Return the last bucket with all remaining elements
                if (bucket != null && count > 0) {
                    Array.Resize(ref bucket, count);
                    yield return resultSelector(bucket);
                }
            }
        }
        #endregion

        #region Cartesian
        /// <summary>
        ///     Returns the Cartesian product of two sequences by combining each element of the first set with each in the
        ///     second and applying the user=define projection to the pair.
        /// </summary>
        /// <typeparam name="TSecond">The type of the elements of <paramref name="second" /></typeparam>
        /// <typeparam name="TResult">The type of the elements of the result sequence</typeparam>
        /// <param name="second">The second sequence of elements</param>
        /// <param name="resultSelector">A projection function that combines elements from both sequences</param>
        /// <returns>A sequence representing the Cartesian product of the two source sequences</returns>
        public IEnumerable<TResult> Cartesian<TSecond, TResult>(IEnumerable<TSecond> second, Func<T, TSecond, TResult> resultSelector) {
            if (second == null) throw new ArgumentNullException(nameof(second));
            if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

            return from item1 in Value ?? Array.Empty<T>()
                   from item2 in second // TODO buffer to avoid multiple enumerations
                   select resultSelector(item1, item2);
        }
        #endregion

        #region EquiZip
        /// <summary>
        ///     Returns a projection of tuples, where each tuple contains the N-th element from each of the argument
        ///     sequences.
        /// </summary>
        /// <example>
        ///     <code>
        /// int[] numbers = { 1, 2, 3, 4 };
        /// string[] letters = { "A", "B", "C", "D" };
        /// var zipped = numbers.EquiZip(letters, (n, l) => n + l);
        /// </code>
        ///     The <c>zipped</c> variable, when iterated over, will yield "1A", "2B", "3C", "4D" in turn.
        /// </example>
        /// <typeparam name="T2">Type of elements in second sequence</typeparam>
        /// <typeparam name="TResult">Type of elements in result sequence</typeparam>
        /// <param name="second">Second sequence</param>
        /// <param name="resultSelector">Function to apply to each pair of elements</param>
        /// <returns>A sequence that contains elements of the two input sequences, combined by <paramref name="resultSelector" />.</returns>
        /// <remarks>
        ///     If the two input sequences are of different lengths then <see cref="InvalidOperationException" /> is thrown.
        ///     This operator uses deferred execution and streams its results.
        /// </remarks>
        public IEnumerable<TResult> EquiZip<T2, TResult>(IEnumerable<T2> second, Func<T, T2?, TResult> resultSelector) {
            if (second == null) throw new ArgumentNullException(nameof(second));
            if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

            return EquiZipImpl<T, T2, object, object, TResult>(Value, second, null, null, (a, b, _, _) => resultSelector(a, b));
        }

        /// <summary>
        ///     Returns a projection of tuples, where each tuple contains the N-th element from each of the argument
        ///     sequences.
        /// </summary>
        /// <remarks>
        ///     If the three input sequences are of different lengths then <see cref="InvalidOperationException" /> is thrown.
        ///     This operator uses deferred execution and streams its results.
        /// </remarks>
        /// <example>
        ///     <code>
        /// var numbers = { 1, 2, 3, 4 };
        /// var letters = { "A", "B", "C", "D" };
        /// var chars    = { 'a', 'b', 'c', 'd' };
        /// var zipped = numbers.EquiZip(letters, chars, (n, l, c) => n + l + c);
        /// </code>
        ///     The <c>zipped</c> variable, when iterated over, will yield "1Aa", "2Bb", "3Cc", "4Dd" in turn.
        /// </example>
        /// <typeparam name="T2">Type of elements in second sequence</typeparam>
        /// <typeparam name="T3">Type of elements in third sequence</typeparam>
        /// <typeparam name="TResult">Type of elements in result sequence</typeparam>
        /// <param name="second">Second sequence</param>
        /// <param name="third">Third sequence</param>
        /// <param name="resultSelector">Function to apply to each triplet of elements</param>
        /// <returns>
        ///     A sequence that contains elements of the three input sequences, combined by <paramref name="resultSelector" />
        ///     .
        /// </returns>
        public IEnumerable<TResult> EquiZip<T2, T3, TResult>(IEnumerable<T2> second, IEnumerable<T3> third, Func<T, T2?, T3?, TResult> resultSelector) {
            if (second == null) throw new ArgumentNullException(nameof(second));
            if (third == null) throw new ArgumentNullException(nameof(third));
            if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

            return EquiZipImpl<T, T2, T3, object, TResult>(Value, second, third, null, (a, b, c, _) => resultSelector(a, b, c));
        }

        /// <summary>
        ///     Returns a projection of tuples, where each tuple contains the N-th element from each of the argument
        ///     sequences.
        /// </summary>
        /// <remarks>
        ///     If the three input sequences are of different lengths then <see cref="InvalidOperationException" /> is thrown.
        ///     This operator uses deferred execution and streams its results.
        /// </remarks>
        /// <example>
        ///     <code>
        /// var numbers = { 1, 2, 3, 4 };
        /// var letters = { "A", "B", "C", "D" };
        /// var chars   = { 'a', 'b', 'c', 'd' };
        /// var flags   = { true, false, true, false };
        /// var zipped = numbers.EquiZip(letters, chars, flags, (n, l, c, f) => n + l + c + f);
        /// </code>
        ///     The <c>zipped</c> variable, when iterated over, will yield "1AaTrue", "2BbFalse", "3CcTrue", "4DdFalse" in turn.
        /// </example>
        /// <typeparam name="T2">Type of elements in second sequence</typeparam>
        /// <typeparam name="T3">Type of elements in third sequence</typeparam>
        /// <typeparam name="T4">Type of elements in fourth sequence</typeparam>
        /// <typeparam name="TResult">Type of elements in result sequence</typeparam>
        /// <param name="second">Second sequence</param>
        /// <param name="third">Third sequence</param>
        /// <param name="fourth">Fourth sequence</param>
        /// <param name="resultSelector">Function to apply to each quadruplet of elements</param>
        /// <returns>A sequence that contains elements of the four input sequences, combined by <paramref name="resultSelector" />.</returns>
        public IEnumerable<TResult> EquiZip<T2, T3, T4, TResult>(IEnumerable<T2> second, IEnumerable<T3> third, IEnumerable<T4> fourth,
            Func<T, T2?, T3?, T4?, TResult> resultSelector) {
            if (second == null) throw new ArgumentNullException(nameof(second));
            if (third == null) throw new ArgumentNullException(nameof(third));
            if (fourth == null) throw new ArgumentNullException(nameof(fourth));
            if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

            return EquiZipImpl(Value, second, third, fourth, resultSelector);
        }

        private static IEnumerable<TResult> EquiZipImpl<T1, T2, T3, T4, TResult>(
            IEnumerable<T1>? first,
            IEnumerable<T2>? second,
            IEnumerable<T3>? third,
            IEnumerable<T4>? fourth,
            Func<T1, T2?, T3?, T4?, TResult> resultSelector) {
            if (first == null) yield break;
            using var e1 = first.GetEnumerator();
            using var e2 = second?.GetEnumerator();
            using var e3 = third?.GetEnumerator();
            using var e4 = fourth?.GetEnumerator();
            while (e1.MoveNext()) {
                bool m2, m3 = false;
                if ((m2 = e2?.MoveNext() != false) && (m3 = e3?.MoveNext() != false) && (e4?.MoveNext() != false)) {
                    yield return resultSelector(e1.Current,
                        e2 != null ? e2.Current : default,
                        e3 != null ? e3.Current : default,
                        e4 != null ? e4.Current : default);
                } else {
                    var message = string.Format("{0} sequence too short.", !m2 ? "Second" : !m3 ? "Third" : "Fourth");
                    throw new InvalidOperationException(message);
                }
            }
            if (e2?.MoveNext() == true || e3?.MoveNext() == true || e4?.MoveNext() == true)
                throw new InvalidOperationException("First sequence too short.");
        }
        #endregion

        #region Interleave
        /// <summary>
        ///     Interleaves the elements of two or more sequences into a single sequence, skipping sequences as they are
        ///     consumed
        /// </summary>
        /// <remarks>
        ///     Interleave combines sequences by visiting each in turn, and returning the first element of each, followed by the
        ///     second, then the third, and so on. So, for example:<br />
        ///     <code>
        /// {1,1,1}.Interleave( {2,2,2}, {3,3,3} ) => { 1,2,3,1,2,3,1,2,3 }
        /// </code>
        ///     This operator behaves in a deferred and streaming manner.<br /> When sequences are of unequal length, this method
        ///     will skip those sequences that have been fully consumed and continue interleaving the remaining sequences.<br />
        ///     The sequences are interleaved in the order that they appear in the <paramref name="otherSequences" /> collection,
        ///     with sequence as the first sequence.
        /// </remarks>
        /// <param name="otherSequences">The other sequences in the interleave group</param>
        /// <returns>A sequence of interleaved elements from all of the source sequences</returns>
        public IEnumerable<T> Interleave(params IEnumerable<T>[] otherSequences) => Interleave(ImbalancedInterleaveStrategy.Skip, otherSequences);

        /// <summary>
        ///     Interleaves the elements of two or more sequences into a single sequence, applying the specified strategy when
        ///     sequences are of unequal length
        /// </summary>
        /// <remarks>
        ///     Interleave combines sequences by visiting each in turn, and returning the first element of each, followed by the
        ///     second, then the third, and so on. So, for example:<br />
        ///     <code>
        /// {1,1,1}.Interleave( {2,2,2}, {3,3,3} ) => { 1,2,3,1,2,3,1,2,3 }
        /// </code>
        ///     This operator behaves in a deferred and streaming manner.<br /> When sequences are of unequal length, this method
        ///     will use the imbalance strategy specified to decide how to continue interleaving the remaining sequences. See
        ///     <see cref="ImbalancedInterleaveStrategy" /> for more information.<br /> The sequences are interleaved in the order
        ///     that they appear in the <paramref name="otherSequences" /> collection, with sequence as the first sequence.
        /// </remarks>
        /// <param name="imbalanceStrategy">Defines the behavior of the operator when sequences are of unequal length</param>
        /// <param name="otherSequences">The other sequences in the interleave group</param>
        /// <returns>A sequence of interleaved elements from all of the source sequences</returns>
        private IEnumerable<T> Interleave(ImbalancedInterleaveStrategy imbalanceStrategy, params IEnumerable<T>[] otherSequences) {
            if (Value == null) throw new NullReferenceException("IEnumerable is null");
            if (otherSequences == null) throw new ArgumentNullException(nameof(otherSequences));
            if (otherSequences.Any(s => s == null)) throw new ArgumentNullException(nameof(otherSequences), "One or more sequences passed to Interleave was null.");

            return _();
            IEnumerable<T> _() {
                var sequences = new[] { Value }.Concat(otherSequences);

                // produce an iterator collection for all IEnumerable<T> instancess passed to us
                var iterators = sequences.Select(e => e.GetEnumerator());
                List<IEnumerator<T>>? iteratorList = null;

                try {
                    iteratorList = new List<IEnumerator<T>>(iterators);
                    //iterators = null;
                    var shouldContinue = true;
                    var consumedIterators = 0;
                    var iterCount = iteratorList.Count;

                    while (shouldContinue) {
                        // advance every iterator and verify a value exists to be yielded
                        for (var index = 0; index < iterCount; index++) {
                            if (!iteratorList[index].MoveNext()) {
                                // check if all iterators have been consumed and we can terminate
                                // or if the imbalance strategy informs us that we MUST terminate
                                if (++consumedIterators == iterCount || imbalanceStrategy == ImbalancedInterleaveStrategy.Stop) {
                                    shouldContinue = false;
                                    break;
                                }

                                iteratorList[index].Dispose(); // dispose the iterator sice we no longer need it

                                // otherwise, apply the imbalance strategy
                                switch (imbalanceStrategy) {
                                    case ImbalancedInterleaveStrategy.Pad:
                                        var newIter = iteratorList[index] = Enumerable.Empty<T>().GetEnumerator();
                                        newIter.MoveNext();
                                        break;

                                    case ImbalancedInterleaveStrategy.Skip:
                                        iteratorList.RemoveAt(index); // no longer visit this particular iterator
                                        --iterCount; // reduce the expected number of iterators to visit
                                        --index; // decrement iterator index to compensate for index shifting
                                        --consumedIterators; // decrement consumer iterator count to stay in balance
                                        break;
                                }
                            }
                        }

                        if (shouldContinue) // only if all iterators could be advanced
                        {
                            // yield the values of each iterator's current position
                            for (var index = 0; index < iterCount; index++)
                                yield return iteratorList[index].Current;
                        }
                    }
                } finally {
                    Debug.Assert(iteratorList != null || iterators != null);
                    foreach (var iter in iteratorList ?? iterators.ToList()) iter.Dispose();
                }
            }
        }

        /// <summary>Defines the strategies available when Interleave is passed sequences of unequal length</summary>
        private enum ImbalancedInterleaveStrategy {
            /// <summary>Extends a sequence by padding its tail with default(T)</summary>
            Pad,
            /// <summary>Removes the sequence from the interleave set, and continues interleaving remaining sequences.</summary>
            Skip,
            /// <summary>Stops the interleave operation.</summary>
            Stop
        }
        #endregion
        #endregion
    }
    #endregion Transform
    #endregion

    #region ForEach<T>
    /// <summary>Performs an action with a counter for each item in a sequence and provides</summary>
    /// <typeparam name="T">The type of the items in the sequence</typeparam>
    /// <param name="values">The sequence to iterate</param>
    /// <param name="eachAction">The action to performa on each item</param>
    /// <returns></returns>
    public static IEnumerable<T> ForEach<T>(this IEnumerable<T> values, Action<T, int> eachAction) {
        var index = 0;
        foreach (var item in values) eachAction(item, index++);
        return values;
    }
    /// <summary>Performs an action with a counter for each item in a sequence and provides</summary>
    /// <typeparam name="T">The type of the items in the sequence</typeparam>
    /// <param name="values">The sequence to iterate</param>
    /// <param name="eachAction">The action to performa on each item</param>
    /// <returns></returns>
    [DebuggerStepThrough]
    public static IEnumerable<T> ForEach<T>(this IEnumerable<T> values, Action<T> eachAction) {
        foreach (var item in values) eachAction(item);
        return values;
    }
    /// <summary>Performs an action with a counter for each item in a sequence and provides</summary>
    /// <param name="values">The sequence to iterate</param>
    /// <param name="eachAction">The action to performa on each item</param>
    /// <returns></returns>
    [DebuggerStepThrough]
    public static IEnumerable ForEach(this IEnumerable values, Action<object> eachAction) {
        foreach (var item in values) eachAction(item);

        return values;
    }
    #endregion ForEach<T>

    #region Find
    /// <summary>Multi-method library for IEnumerable item search related commands.</summary>
    public class FindPortion<T>: ExtendablePortion<IEnumerable<T>> {
        #region Constructors
        /// <summary>Constructor</summary>
        public FindPortion(IEnumerable<T> value) : base(value) { }
        #endregion

        /// <summary>Gets the median from the list</summary>
        /// <returns>The median value</returns>
        public T? Median() {
            if (Value == null) return default;
            if (!Value.Any()) return default;
            var value = Value.OrderBy(x => x);
            return value.ElementAt(Value.Count() / 2);
        }

        /// <summary>Gets the mode (item that occurs the most) from the list</summary>
        /// <returns>The mode value</returns>
        public T? Mode() {
            if (Value == null) return default;
            var modesList = Value
                .GroupBy(values => values)
                .Select(valueCluster =>
                    new {
                        Value = valueCluster.Key,
                        Occurrence = valueCluster.Count()
                    })
                .ToList();

            var maxOccurrence = modesList
                .Max(g => g.Occurrence);

            return modesList
                .Where(x => x.Occurrence == maxOccurrence && maxOccurrence > 1) // Thanks Rui!
                .Select(x => x.Value).FirstOrDefault();
        }
    }

    #region FindPeaks (Numeric Only)
    /// <summary>Finds peaks and valleys in a numeric iEnumerable</summary>
    /// <param name="fp">FindPortion</param>
    /// <param name="windowSize">Window size to search</param>
    /// <returns>Index, value, bool to indicate true=peak false=valley, of the found peak or valley</returns>
    public static IEnumerable<Tuple<int, T, bool>> Peaks<T>(this FindPortion<T> fp, int windowSize = 3) where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable {
        var source = fp.Value;
        // Round up to nearest odd value
        windowSize = windowSize - (windowSize % 2) + 1;
        var halfWindow = windowSize / 2;

        var minValue = (T)(typeof(T).InvokeMember("MinValue", BindingFlags.GetField, null, default(T), null) ?? throw new Exception("MinValue can not be null"));

        var index = 0;
        var before = new Queue<T>(Enumerable.Repeat(minValue, halfWindow));
        var after = new Queue<T>(source.Take(halfWindow + 1));

        foreach (var d in source.Skip(halfWindow + 1).Concat(Enumerable.Repeat(minValue, halfWindow + 1))) {
            var curVal = after.Dequeue();
            if (before.All(x => curVal.CompareTo(x) > 0) && after.All(x => curVal.CompareTo(x) >= 0)) yield return Tuple.Create(index, curVal, true);
            if (before.All(x => curVal.CompareTo(x) < 0) && after.All(x => curVal.CompareTo(x) <= 0)) yield return Tuple.Create(index, curVal, false);

            before.Dequeue();
            before.Enqueue(curVal);
            after.Enqueue(d);
            index++;
        }
    }
    /// <summary>Finds peaks and valleys in a numeric iEnumerable</summary>
    /// <param name="fp">FindPortion</param>
    /// <param name="findPeaks">If false finds valleys, if true finds peaks</param>
    /// <param name="windowSize">Window size to search</param>
    /// <returns>Index and value of the found peak or valley</returns>
    public static IEnumerable<Tuple<int, T>> Peaks<T>(this FindPortion<T> fp, bool findPeaks, int windowSize = 3)
        where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable {
        var source = fp.Value;
        // Round up to nearest odd value
        windowSize = windowSize - (windowSize % 2) + 1;
        var halfWindow = windowSize / 2;

        var minValue = (T)(typeof(T).InvokeMember("MinValue", BindingFlags.GetField, null, default(T), null) ?? throw new Exception("MinValue can not be null"));

        var index = 0;
        var before = new Queue<T>(Enumerable.Repeat(minValue, halfWindow));
        var after = new Queue<T>(source.Take(halfWindow + 1));

        foreach (var d in source.Skip(halfWindow + 1).Concat(Enumerable.Repeat(minValue, halfWindow + 1))) {
            var curVal = after.Dequeue();
            if (findPeaks
                ? before.All(x => curVal.CompareTo(x) > 0) && after.All(x => curVal.CompareTo(x) >= 0)
                : before.All(x => curVal.CompareTo(x) < 0) && after.All(x => curVal.CompareTo(x) <= 0)) {
                yield return Tuple.Create(index, curVal);
            }

            before.Dequeue();
            before.Enqueue(curVal);
            after.Enqueue(d);
            index++;
        }
    }
    #endregion
    #endregion Find
}