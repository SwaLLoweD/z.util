﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Z.Collections;
using Z.Collections.Helpers;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for all kind of Lists implementing the IList&lt;T&gt; interface</summary>
public static class ExtIList
{
    /// <summary>Converts the specified Enumerable to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion<T> To<T>(this IList<T> value) => new(value);

    /// <summary>Finds elements from the specified Enumerable.</summary>
    /// <param name="value">The value to be manipulated.</param>
    /// <returns>An universal remove method supplying additional remove methods</returns>
    public static FindPortion<T> Find<T>(this IList<T> value) => new(value);

    /// <summary>Removes elements from the specified Enumerable.</summary>
    /// <param name="value">The value to be manipulated.</param>
    /// <returns>An universal remove method supplying additional remove methods</returns>
    public static RemovePortion<T> Remove<T>(this IList<T> value) => new(value);

    /// <summary>Transform the specified List.</summary>
    /// <param name="value">The value to be transformed.</param>
    /// <returns>An universal transformer supplying additional transforming methods</returns>
    public static TransformPortion<T> Transform<T>(this IList<T> value) => new(value);

    /// <summary>Inserts an item uniquely to a list and returns a value whether the item was inserted or not.</summary>
    /// <typeparam name="T">The generic list item type.</typeparam>
    /// <param name="value">The list to be inserted into.</param>
    /// <param name="index">The index to insert the item at.</param>
    /// <param name="item">The item to be added.</param>
    /// <returns>Indicates whether the item was inserted or not</returns>
    public static bool InsertUnique<T>(this IList<T> value, int index, T item) {
        if (!value.Contains(item)) {
            value.Insert(index, item);
            return true;
        }
        return false;
    }

    /// <summary>
    ///     Inserts a range of items uniquely to a list starting at a given index and returns the amount of items
    ///     inserted.
    /// </summary>
    /// <typeparam name="T">The generic list item type.</typeparam>
    /// <param name="value">The list to be inserted into.</param>
    /// <param name="startIndex">The start index.</param>
    /// <param name="items">The items to be inserted.</param>
    /// <returns>The amount if items that were inserted.</returns>
    public static int InsertUnique<T>(this IList<T> value, int startIndex, IEnumerable<T> items) {
        var index = startIndex + items.Reverse().Count(item => value.InsertUnique(startIndex, item));
        return index - startIndex;
    }

    /// <summary>Return the indexes of the matching items</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value">The list.</param>
    /// <param name="item">The item to search for.</param>
    /// <param name="comparer">The comparison.</param>
    /// <returns>The item index</returns>
    public static IList<int> IndexOf<T>(this IList<T> value, T item, IEqualityComparer<T> comparer) => IndexOf(value, item, comparer.Equals);

    /// <summary>Return the indexes of the matching items</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value">The list.</param>
    /// <param name="item">The item to search for.</param>
    /// <param name="comparison">The comparison.</param>
    /// <returns>The item index</returns>
    public static IList<int> IndexOf<T>(this IList<T> value, T item, Func<T, T, bool> comparison) {
        if (value == null) throw new ArgumentNullException(nameof(value));
        if (comparison == null) throw new ArgumentNullException(nameof(comparison));

        var rval = new List<int>();
        var buf1 = value.ToArray();
        for (var i = 0; i < value.Count; i++) {
            if (comparison(buf1[i], item))
                rval.Add(i);
        }

        return rval;
    }

    /// <summary>
    ///     Searchs a list for a sub-sequence of items that match a particular pattern. A subsequence of
    ///     <paramref name="list" /> matches pattern at index i if list[i] is equal to the first item in
    ///     <paramref name="pattern" />, list[i+1] is equal to the second item in <paramref name="pattern" />, and so forth for
    ///     all the items in <paramref name="pattern" />. The passed instance of IEqualityComparer&lt;T&gt; is used for
    ///     determining if two items are equal.
    /// </summary>
    /// <typeparam name="T">The type of items in the list.</typeparam>
    /// <param name="list">The list to search.</param>
    /// <param name="pattern">The sequence of items to search for.</param>
    /// <param name="equalityComparer">
    ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
    ///     method will be called.
    /// </param>
    /// <returns>The first index with <paramref name="list" /> that matches the items in <paramref name="pattern" />.</returns>
    public static IList<int> IndexOf<T>(this IList<T> list, IEnumerable<T> pattern, IEqualityComparer<T>? equalityComparer = null) {
        if (list == null) throw new ArgumentNullException(nameof(list));
        if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
        return IndexOf(list, pattern, equalityComparer.Equals);
    }
    /// <summary>
    ///     Searchs a list for a sub-sequence of items that match a particular pattern. A subsequence of
    ///     <paramref name="list" /> matches pattern at index i if list[i] is "equal" to the first item in
    ///     <paramref name="pattern" />, list[i+1] is "equal" to the second item in <paramref name="pattern" />, and so forth
    ///     for all the items in <paramref name="pattern" />. The passed BinaryPredicate is used to determine if two items are
    ///     "equal".
    /// </summary>
    /// <remarks>
    ///     Since an arbitrary BinaryPredicate is passed to this function, what is being tested for in the pattern need
    ///     not be equality.
    /// </remarks>
    /// <typeparam name="T">The type of items in the list.</typeparam>
    /// <param name="list">The list to search.</param>
    /// <param name="pattern">The sequence of items to search for.</param>
    /// <param name="predicate">The BinaryPredicate used to compare items for "equality". </param>
    /// <returns>The first index with <paramref name="list" /> that matches the items in <paramref name="pattern" />.</returns>
    public static IList<int> IndexOf<T>(this IList<T> list, IEnumerable<T> pattern, Func<T, T, bool> predicate) {
        if (list == null) throw new ArgumentNullException(nameof(list));
        if (pattern == null) throw new ArgumentNullException(nameof(pattern));
        if (predicate == null) throw new ArgumentNullException(nameof(predicate));

        var rval = new List<int>();
        // Put the pattern into an array for performance (don't keep allocating enumerators).
        var patternArray = pattern.ToArray();

        int listCount = list.Count, patternCount = patternArray.Length;
        if (patternCount == 0) {
            rval.Add(0); // A zero-length pattern occurs anywhere.
            return rval;
        }
        if (listCount == 0) return rval; // no room for a pattern;

        var start = 0;
        while (start <= listCount - patternCount) {
            for (var count = 0; count < patternCount; ++count) {
                if (!predicate(list[start + count], patternArray[count]))
                    goto NOMATCH;
            }
            // Got through the whole pattern. We have a match.
            //return start;
            rval.Add(start);

        NOMATCH:
            /* no match found at start. */
            ;
            ++start;
        }

        // no match found anywhere.
        return rval;
        //List<int> positions = new List<int>();
        //if (pattern.Is().Empty) return positions.ToArray();

        //var patternArray = pattern.ToArray();
        //int patternLength = patternArray.Length;
        //int totalLength = value.Count;
        //int firstMatch = value.IndexOf(patternArray[0]);
        //for (int i = 0; i < totalLength; i++) {
        //    if (firstMatch.Equals(value[i]) && totalLength - i >= patternLength) {
        //        var match = value.SubList(i, patternLength);
        //        if (match.SequenceEqual<T>(pattern)) {
        //            positions.Add(i);
        //            i += patternLength - 1;
        //        }
        //    }
        //}
        //return positions;
    }

    /// <summary>
    ///     Finds the index of the last item in a list equal to a given item. A passed IEqualityComparer is used to
    ///     determine equality.
    /// </summary>
    /// <param name="list">The list to search.</param>
    /// <param name="item">The item to search for.</param>
    /// <param name="equalityComparer">
    ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
    ///     method will be called.
    /// </param>
    /// <returns>The index of the last item equal to <paramref name="item" />. -1 if no such item exists in the list.</returns>
    public static int LastIndexOf<T>(this IList<T> list, T item, IEqualityComparer<T>? equalityComparer = null) {
        if (list == null) throw new ArgumentNullException(nameof(list));
        if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
        return LastIndexOf(list, item, equalityComparer.Equals);
    }

    /// <summary>
    ///     Finds the index of the last item in a list equal to a given item. A passed IEqualityComparer is used to
    ///     determine equality.
    /// </summary>
    /// <param name="list">The list to search.</param>
    /// <param name="item">The item to search for.</param>
    /// <param name="comparison">The function to compare items for equality. Only the Equals method will be called.</param>
    /// <returns>The index of the last item equal to <paramref name="item" />. -1 if no such item exists in the list.</returns>
    public static int LastIndexOf<T>(this IList<T> list, T item, Func<T, T, bool> comparison) {
        if (list == null) throw new ArgumentNullException(nameof(list));
        if (comparison == null) throw new ArgumentNullException(nameof(comparison));
        for (var index = list.Count - 1; index >= 0; --index) {
            if (comparison(item, list[index]))
                return index;
        }

        // didn't find any item that matches.
        return -1;
    }
    /// <summary>
    ///     Creates a sublist of items or wraps existing list in a format starting at specified offset for specified
    ///     length
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value">The list.</param>
    /// <param name="offset">Starting index of the original list.</param>
    /// <param name="length">Number of items to add starting from the specified offset</param>
    /// <param name="newInstance">Create a new list or reuse existing list with a wrapper</param>
    /// <returns>The pattern starting index</returns>
    public static IList<T> SubList<T>(this IList<T> value, int offset = 0, int? length = null, bool newInstance = false) {
        if (value == null) throw new ArgumentOutOfRangeException(nameof(value));
        var srclen = value.Count;
        var len = srclen - offset;
        if (length.HasValue && length.Value + offset < srclen)
            len = length.Value;
        if (newInstance) {
            var rval = new List<T>(len);
            for (var i = offset; i < offset + len && i < srclen; i++) rval.Add(value[i]);
            return rval;
        }
        return Wrappers.Range(value, offset, len);
    }

    #region Nested type: ConvertPortion
    #region Converter
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class ConvertPortion<T> : ExtIEnumerable.ConvertPortion<T>
    {
        #region Constructors
        /// <summary>Constructor</summary>
        public ConvertPortion(IList<T> value) : base(value) { }
        #endregion
    }
    #endregion Converter
    #endregion

    #region Nested type: FindPortion
    #region Find
    /// <summary>Multi-method library for IEnumerable item removal related commands.</summary>
    public class FindPortion<T> : ExtIEnumerable.FindPortion<T>
    {
        #region Fields / Properties
        /// <summary>Constructor</summary>
        public virtual IList<T> ValueList => Value.ToList();
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public FindPortion(IList<T> value) : base(value) { }
        #endregion

        /// <summary>
        ///     Finds the first occurence of <paramref name="count" /> consecutive equal items in the list. A passed
        ///     IEqualityComparer is used to determine equality.
        /// </summary>
        /// <param name="count">The number of consecutive equal items to look for. The count must be at least 1.</param>
        /// <param name="equalityComparer">
        ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
        ///     method will be called.
        /// </param>
        /// <returns>
        ///     The index of the first item in the first run of <paramref name="count" /> consecutive equal items, or -1 if no
        ///     such run exists.
        /// </returns>
        public int FirstConsecutiveEqual(int count, IEqualityComparer<T>? equalityComparer = null) {
            if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
            return FirstConsecutiveEqual(count, equalityComparer.Equals);
        }

        /// <summary>
        ///     Finds the first occurence of <paramref name="count" /> consecutive "equal" items in the list. The passed
        ///     BinaryPredicate is used to determine if two items are "equal".
        /// </summary>
        /// <remarks>
        ///     Since an arbitrary BinaryPredicate is passed to this function, what is being tested for need not be true
        ///     equality.
        /// </remarks>
        /// <param name="count">The number of consecutive equal items to look for. The count must be at least 1.</param>
        /// <param name="predicate">The BinaryPredicate used to compare items for "equality". </param>
        /// <returns>
        ///     The index of the first item in the first run of <paramref name="count" /> consecutive equal items, or -1 if no
        ///     such run exists.
        /// </returns>
        public int FirstConsecutiveEqual(int count, Func<T?, T, bool> predicate) {
            var list = ValueList;
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));
            if (count < 1) throw new ArgumentOutOfRangeException(nameof(count));

            var listCount = list.Count;
            if (listCount < count) return -1; // Can't find run longer than the list itself.
            if (count == 1) return 0; // Run of 1 must be the first item in the list.

            int start = 0, index = 0;
            var current = default(T);
            var runLength = 0;

            // Go through the list, looking for a run of the given length.
            foreach (var item in list) {
                if (index > 0 && predicate(current, item)) {
                    ++runLength;
                    if (runLength >= count) return start;
                } else {
                    current = item;
                    start = index;
                    runLength = 1;
                }

                ++index;
            }

            return -1;
        }

        /// <summary>
        ///     Finds the first occurence of <paramref name="count" /> consecutive items in the list for which a given
        ///     predicate returns true.
        /// </summary>
        /// <param name="count">The number of consecutive items to look for. The count must be at least 1.</param>
        /// <param name="predicate">The predicate used to test each item.</param>
        /// <returns>
        ///     The index of the first item in the first run of <paramref name="count" /> items where
        ///     <paramref name="predicate" /> returns true for all items in the run, or -1 if no such run exists.
        /// </returns>
        public int FirstConsecutiveWhere(int count, Predicate<T> predicate) {
            var list = ValueList;
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));
            if (count < 1) throw new ArgumentOutOfRangeException(nameof(count));

            var listCount = list.Count;
            if (count > listCount) return -1; // Can't find run longer than the list itself.

            int index = 0, start = -1;
            var runLength = 0;

            // Scan the list in order, looking for the number of consecutive true items.
            foreach (var item in list) {
                if (predicate(item)) {
                    if (start < 0) start = index;
                    ++runLength;
                    if (runLength >= count) return start;
                } else {
                    runLength = 0;
                    start = -1;
                }

                ++index;
            }

            return -1;
        }
    }
    #endregion Find
    #endregion

    #region Nested type: RemovePortion
    #region Remove
    /// <summary>Multi-method library for IEnumerable item removal related commands.</summary>
    public class RemovePortion<T> : ExtICollection.RemovePortion<T>
    {
        #region Fields / Properties
        /// <summary>Constructor</summary>
        public virtual IList<T> ValueList => Value.ToList();
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public RemovePortion(IList<T> value) : base(value) { }
        #endregion

        /// <summary>Remove an item from the collection with predicate</summary>
        /// <param name="predicate"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <remarks>Contributed by Michael T, http://about.me/MichaelTran </remarks>
        public override void Where(Predicate<T> predicate) {
            for (var i = ValueList.Count - 1; i >= 0; i--) {
                if (predicate(ValueList[i]))
                    ValueList.RemoveAt(i);
            }
        }

        /// <summary>
        ///     Remove subsequent consecutive equal items from a list or array. In each run of consecutive equal items in the
        ///     list, all items after the first item in the run are removed. The replacement is done in-place, changing the list. A
        ///     passed IEqualityComparer is used to determine equality.
        /// </summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <param name="equalityComparer">
        ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
        ///     method will be called.
        /// </param>
        ///// <param name="remover"></param>
        public void DuplicatesInPlace(IEqualityComparer<T>? equalityComparer = null) {
            if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
            DuplicatesInPlace(equalityComparer.Equals);
        }
        /// <summary>
        ///     Remove consecutive "equal" items from a list or array. In each run of consecutive equal items in the list, all
        ///     items after the first item in the run are removed. The replacement is done in-place, changing the list. The passed
        ///     BinaryPredicate is used to determine if two items are "equal".
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Since an arbitrary BinaryPredicate is passed to this function, what is being tested for need not be true
        ///         equality.
        ///     </para>
        ///     <para>
        ///         Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///         array passed as list.
        ///     </para>
        /// </remarks>
        /// <param name="predicate">The BinaryPredicate used to compare items for "equality". </param>
        ///// <param name="remover"></param>
        public void DuplicatesInPlace(Func<T?, T, bool> predicate) {
            var list = ValueList;
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));

            if (list is T[] v) list = Wrappers.List(v);

            var current = default(T);
            T item;
            int i = -1, j = 0;
            var listCount = list.Count;

            // Remove duplicates, compressing items to lower in the list.
            while (j < listCount) {
                item = list[j];
                if (i < 0 || !predicate(current, item)) {
                    current = item;
                    ++i;
                    if (i != j) list[i] = current;
                }
                ++j;
            }

            ++i;
            if (i < listCount) {
                // remove items from the end.
                if (list is IList lists && (lists).IsFixedSize) // An array or similar. Null out the last elements.
                {
                    while (i < listCount) {
                        lists[i++] = default;
                    }
                } else // Normal list.
                {
                    while (i < listCount) {
                        list.RemoveAt(listCount - 1);
                        --listCount;
                    }
                }
            }
        }
    }
    #endregion Remove
    #endregion

    #region Nested type: TransformPortion
    #region Transform
    /// <summary>Multi-method library for IList transformation related commands.</summary>
    public class TransformPortion<T> : ExtICollection.TransformPortion<T>
    {
        #region Fields / Properties
        /// <summary>Constructor</summary>
        public virtual IList<T> ValueList => Value.ToList();
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public TransformPortion(IList<T> value) : base(value) { }
        #endregion

        /// <summary>Replaces each item in a part of a list with a given value.</summary>
        /// <param name="value">The value to fill with.</param>
        /// <param name="start">The index at which to start filling. The first index in the list has index 0.</param>
        /// <param name="count">The number of items to fill.</param>
        /// <exception cref="ArgumentException">list is a read-only list.</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="start" /> or <paramref name="count" /> is negative, or
        ///     <paramref name="start" /> + <paramref name="count" /> is greater than list.Count.
        /// </exception>
        /// <exception cref="ArgumentNullException">list is null.</exception>
        public void Fill(T value, int start = 0, int count = 0) {
            var list = ValueList;

            if (count <= 0) count = list.Count;
            if (start < 0) start = 0;
            if (start >= list.Count) throw new ArgumentOutOfRangeException(nameof(start));
            if (count < 0 || count > list.Count || start > list.Count - count) throw new ArgumentOutOfRangeException(nameof(count));

            for (var i = start; i < count + start; ++i) list[i] = value;
        }

        /// <summary>
        ///     Copies <paramref name="count" /> items from the list source, starting at the index
        ///     <paramref name="sourceIndex" />, to the list <paramref name="dest" />, starting at the index
        ///     <paramref name="destIndex" />. If necessary, the size of the destination list is expanded. The source and
        ///     destination lists may be the same.
        /// </summary>
        /// <param name="dest">The list to store the items into.</param>
        /// <param name="sourceIndex">The index within source to begin copying items from.</param>
        /// <param name="destIndex">The index within <paramref name="dest" />to begin copying items to.</param>
        /// <param name="count">The maximum number of items to copy.</param>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="sourceIndex" /> is negative or greater than source.Count</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="destIndex" /> is negative or greater than
        ///     <paramref name="dest" />.Count
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="count" /> is negative or too large.</exception>
        /// <exception cref="ArgumentNullException">source or <paramref name="dest" /> is null.</exception>
        public void Copy(IList<T> dest, int sourceIndex = 0, int destIndex = 0, int count = 0) {
            var source = ValueList;
            if (dest == null) throw new ArgumentNullException(nameof(dest));
            if (dest.IsReadOnly) throw new ArgumentException("List is read-only: {0}", nameof(dest));

            var sourceCount = source.Count;
            var destCount = dest.Count;

            if (sourceIndex < 0) sourceIndex = 0;
            if (sourceIndex >= sourceCount) throw new ArgumentOutOfRangeException(nameof(sourceIndex));
            if (destIndex <= 0) destIndex = 0;
            if (destIndex > destCount) throw new ArgumentOutOfRangeException(nameof(destIndex));
            if (count <= 0) count = sourceCount;
            if (count > sourceCount - sourceIndex) count = sourceCount - sourceIndex;

            if (source == dest && sourceIndex > destIndex) {
                while (count > 0) {
                    dest[destIndex++] = source[sourceIndex++];
                    --count;
                }
            } else {
                int si, di;

                // First, insert any items needed at the end
                if (destIndex + count > destCount) {
                    var numberToInsert = destIndex + count - destCount;
                    si = sourceIndex + (count - numberToInsert);
                    di = destCount;
                    count -= numberToInsert;
                    while (numberToInsert > 0) {
                        dest.Insert(di++, source[si++]);
                        --numberToInsert;
                    }
                }

                // Do the copy, from end to beginning in case of overlap.
                si = sourceIndex + count - 1;
                di = destIndex + count - 1;
                while (count > 0) {
                    dest[di--] = source[si--];
                    --count;
                }
            }
        }

        /// <summary>
        ///     Copies <paramref name="count" /> items from the list source, starting at the index
        ///     <paramref name="sourceIndex" />, to the list <paramref name="dest" />, starting at the index
        ///     <paramref name="destIndex" />. If necessary, the size of the destination list is expanded. The source and
        ///     destination lists may be the same.
        /// </summary>
        /// <param name="dest">The list to store the items into.</param>
        /// <param name="sourceIndex">The index within source to begin copying items from.</param>
        /// <param name="destIndex">The index within <paramref name="dest" />to begin copying items to.</param>
        /// <param name="count">The maximum number of items to copy.</param>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="sourceIndex" /> is negative or greater than source.Count</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="destIndex" /> is negative or greater than
        ///     <paramref name="dest" />.Count
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="count" /> is negative or too large.</exception>
        /// <exception cref="ArgumentNullException">source or <paramref name="dest" /> is null.</exception>
        public void Copy(T[] dest, int sourceIndex, int destIndex, int count) {
            var source = ValueList;
            if (dest == null) throw new ArgumentNullException(nameof(dest));

            var sourceCount = source.Count;
            var destCount = dest.Length;

            if (sourceIndex < 0) sourceIndex = 0;
            if (sourceIndex >= sourceCount) throw new ArgumentOutOfRangeException(nameof(sourceIndex));
            if (destIndex <= 0) destIndex = 0;
            if (destIndex > destCount) throw new ArgumentOutOfRangeException(nameof(destIndex));
            if (count <= 0) count = sourceCount;
            if (destIndex + count > destCount) throw new ArgumentOutOfRangeException(nameof(count));

            if (count > sourceCount - sourceIndex) count = sourceCount - sourceIndex;

            if (source is T[] t) {
                // Array.Copy is probably faster, and also handles any overlapping issues.
                Array.Copy(t, sourceIndex, dest, destIndex, count);
            } else {
                var si = sourceIndex;
                var di = destIndex;
                while (count > 0) {
                    dest[di++] = source[si++];
                    --count;
                }
            }
        }

        /// <summary>Reverses a list or array and returns the reversed array, without changing the source list.</summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <exception cref="ArgumentNullException">list is null.</exception>
        /// <exception cref="ArgumentException">list is read only.</exception>
        public IEnumerable<T> Reverse() {
            var list = ValueList;
            if (list != null) {
                for (var i = list.Count - 1; i >= 0; --i)
                    yield return list[i];
            }
        }

        /// <summary>Rotates a list and returns the rotated list, without changing the source list.</summary>
        /// <param name="amountToRotate">
        ///     The number of elements to rotate. This value can be positive or negative. For example,
        ///     rotating by positive 3 means that source[3] is the first item in the returned collection. Rotating by negative 3
        ///     means that source[source.Count - 3] is the first item in the returned collection.
        /// </param>
        /// <returns>A collection that contains the items from source in rotated order.</returns>
        /// <exception cref="ArgumentNullException">source is null.</exception>
        public IEnumerable<T> Rotate(int amountToRotate) {
            var list = ValueList;
            if (list != null) {
                var count = list.Count;
                if (count != 0) {
                    amountToRotate %= count;
                    if (amountToRotate < 0) amountToRotate += count;

                    // Do it in two parts.
                    for (var i = amountToRotate; i < count; ++i) yield return list[i];
                    for (var i = 0; i < amountToRotate; ++i) yield return list[i];
                }
            }
        }
        /// <summary>
        ///     Replace all items in a list or array equal to a particular value with another values. The replacement is done
        ///     in-place, changing the list. A passed IEqualityComparer is used to determine equality.
        /// </summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <param name="itemFind">The value to find and replace within collection/>.</param>
        /// <param name="replaceWith">The new value to replace with.</param>
        /// <param name="equalityComparer">
        ///     The IEqualityComparer&lt;T&gt; used to compare items for equality. Only the Equals
        ///     method will be called.
        /// </param>
        ///// <param name="transformer"></param>
        public void ReplaceInPlace(T itemFind, T replaceWith, IEqualityComparer<T>? equalityComparer = null) {
            if (equalityComparer == null) equalityComparer = EqualityComparer<T>.Default;
            var list = ValueList;
            if (list is T[] v) list = Wrappers.List(v);

            var listCount = list.Count;
            for (var index = 0; index < listCount; ++index) {
                if (equalityComparer.Equals(list[index], itemFind))
                    list[index] = replaceWith;
            }
        }

        /// <summary>
        ///     Replace all items in a list or array that a predicate evaluates at true with a value. The replacement is done
        ///     in-place, changing the list.
        /// </summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <param name="predicate">
        ///     The predicate used to evaluate items with the collection. If the predicate returns true for a
        ///     particular item, the item is replaces with <paramref name="replaceWith" />.
        /// </param>
        /// <param name="replaceWith">The new value to replace with.</param>
        ///// <param name="transformer"></param>
        public void ReplaceInPlace(Predicate<T> predicate, T replaceWith) {
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));
            var list = ValueList;
            if (list is T[] v) list = Wrappers.List(v);

            var listCount = list.Count;
            for (var index = 0; index < listCount; ++index) {
                if (predicate(list[index]))
                    list[index] = replaceWith;
            }
        }

        /// <summary>Reverses a list or array in place.</summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <exception cref="ArgumentNullException">list is null.</exception>
        /// <exception cref="ArgumentException">list is read only.</exception>
        public void ReverseInPlace() {
            var list = ValueList;
            if (list == null) return;
            if (list is T[] v) list = Wrappers.List(v);

            int i, j;
            i = 0;
            j = list.Count - 1;
            while (i < j) {
                (list[j], list[i]) = (list[i], list[j]);
                i++;
                j--;
            }
        }

        /// <summary>Rotates a list or array in place.</summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <param name="amountToRotate">
        ///     The number of elements to rotate. This value can be positive or negative. For example,
        ///     rotating by positive 3 means that list[3] is the first item in the resulting list. Rotating by negative 3 means
        ///     that list[list.Count - 3] is the first item in the resulting list.
        /// </param>
        /// <exception cref="ArgumentNullException">list is null.</exception>
        public void RotateInPlace(int amountToRotate) {
            var list = ValueList;
            if (list == null) return;
            if (list is T[] v) list = Wrappers.List(v);

            var count = list.Count;
            if (count != 0) {
                amountToRotate %= count;
                if (amountToRotate < 0) amountToRotate += count;

                var itemsLeft = count;
                var indexStart = 0;
                while (itemsLeft > 0) {
                    // Rotate an orbit of items through the list. If itemsLeft is relatively prime
                    // to count, this will rotate everything. If not, we need to do this several times until
                    // all items have been moved.
                    var index = indexStart;
                    var itemStart = list[indexStart];
                    for (; ; ) {
                        --itemsLeft;
                        var nextIndex = index + amountToRotate;
                        if (nextIndex >= count) nextIndex -= count;
                        if (nextIndex == indexStart) {
                            list[index] = itemStart;
                            break;
                        }
                        list[index] = list[nextIndex];
                        index = nextIndex;
                    }

                    // Move to the next orbit.
                    ++indexStart;
                }
            }
        }

        /// <summary>Randomly shuffles the items in a list or array, in place.</summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <param name="randomGenerator">The random number generator to use to select the random order.</param>
        ///// <param name="transformer"></param>
        public override IList<T> RandomShuffle(Random? randomGenerator = null) {
            var list = ValueList;
            if (randomGenerator == null) randomGenerator = ZUtilCfg.Random;
            if (list is T[] v) list = Wrappers.List(v);

            var count = list.Count;
            for (var i = count - 1; i >= 1; --i) {
                // Pick an random number 0 through i inclusive.
                var j = randomGenerator.Next(i + 1);

                // Swap list[i] and list[j]
                (list[j], list[i]) = (list[i], list[j]);
            }
            return list;
        }

        /// <summary>
        ///     Partition a list or array based on a predicate. After partitioning, all items for which the predicate returned
        ///     true precede all items for which the predicate returned false. The partition is stable, which means that if items X
        ///     and Y have the same result from the predicate, and X precedes Y in the original list, X will precede Y in the
        ///     partitioned list.
        /// </summary>
        /// <remarks>
        ///     Although arrays cast to IList&lt;T&gt; are normally read-only, this method will work correctly and modify an
        ///     array passed as list.
        /// </remarks>
        /// <param name="predicate">A delegate that defines the partitioning condition.</param>
        /// <returns>
        ///     The index of the first item in the second half of the partition; i.e., the first item for which
        ///     <paramref name="predicate" /> returned false. If the predicate was true for all items in the list, list.Count is
        ///     returned.
        /// </returns>
        public int Partition(Predicate<T> predicate) {
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));
            var list = ValueList;
            if (list is T[] v) list = Wrappers.List(v);

            var listCount = list.Count;
            if (listCount == 0) return 0;
            var temp = new T[listCount];

            // Copy from list to temp buffer, true items at fron, false item (in reverse order) at back.
            int i = 0, j = listCount - 1;
            foreach (var item in list) {
                if (predicate(item))
                    temp[i++] = item;
                else
                    temp[j--] = item;
            }

            // Copy back to the original list.
            var index = 0;
            while (index < i) {
                list[index] = temp[index];
                index++;
            }
            j = listCount - 1;
            while (index < listCount) list[index++] = temp[j--];

            return i;
        }

        /// <summary>Convert collection to a readonly one.</summary>
        public new virtual IList<T> Readonly() => Wrappers.ReadOnly(ValueList);
    }
    #endregion Transform
    #endregion

    #region Disabled Extensions
    /*
    /// <summary>
    /// 	Join all the elements in the list and create a string seperated by the specified char.
    /// </summary>
    /// <param name = "list">
    /// 	The list.
    /// </param>
    /// <param name = "joinChar">
    /// 	The join char.
    /// </param>
    /// <typeparam name = "T">
    /// </typeparam>
    /// <returns>
    /// 	The resulting string of the elements in the list.
    /// </returns>
    /// <remarks>
    /// 	Contributed by Michael T, http://about.me/MichaelTran
    /// </remarks>
    public static string Join<T>(this IList<T> list, char joinChar)
    { return list.Join(joinChar.ToString()); }

    /// <summary>
    /// 	Join all the elements in the list and create a string seperated by the specified string.
    /// </summary>
    /// <param name = "list">
    /// 	The list.
    /// </param>
    /// <param name = "joinString">
    /// 	The join string.
    /// </param>
    /// <typeparam name = "T">
    /// </typeparam>
    /// <returns>
    /// 	The resulting string of the elements in the list.
    /// </returns>
    /// <remarks>
    /// 	Contributed by Michael T, http://about.me/MichaelTran
    /// 	Optimised by Mario Majcica
    /// </remarks>
    public static string Join<T>(this IList<T> list, string joinString)
    {
        StringBuilder result = new StringBuilder();

        int listCount = list.Count;
        int listCountMinusOne = listCount - 1;

        if (list != null && listCount > 0)
        {
            if (listCount > 1)
            {
                for (var i = 0; i < listCount; i++)
                {
                    if (i != listCountMinusOne)
                    {
                        result.Append(list[i]);
                        result.Append(joinString);
                    }
                    else
                        result.Append(list[i]);
                }
            }
            else
                result.Append(list[0]);
        }

        return result.ToString();
    }

    /// <summary>
    /// 	Using Relugar Expression, find the top matches for each item in the source specified by the arguments to search.
    /// </summary>
    /// <param name = "list">
    /// 	The source.
    /// </param>
    /// <param name = "searchString">
    /// 	The search string.
    /// </param>
    /// <param name = "top">
    /// 	The top.
    /// </param>
    /// <param name = "args">
    /// 	The args.
    /// </param>
    /// <typeparam name = "T">
    /// </typeparam>
    /// <returns>
    /// 	A List of top matches.
    /// </returns>
    /// <remarks>
    /// 	Contributed by Michael T, http://about.me/MichaelTran
    /// </remarks>
    public static List<T> Match<T>(this IList<T> list, string searchString, int top, params Expression<Func<T, object>>[] args)
    {
        // Create a new list of results and matches;
        var results = new List<T>();
        var matches = new Dictionary<T, int>();
        var maxMatch = 0;
        // For each item in the source
        list.ForEach(s =>
        {
            // Generate the expression string from the argument.
            var regExp = string.Empty;
            if (args != null)
            {
                // For each argument
                Array.ForEach(args,
                    a =>
                    {
                        // Compile the expression
                        var property = a.Compile();
                        // Attach the new property to the expression string
                        regExp += (string.IsNullOrEmpty(regExp) ? "(?:" : "|(?:") + property(s) + ")+?";
                    });
            }
            // Get the matches
            var match = Regex.Matches(searchString, regExp, RegexOptions.IgnoreCase);
            // If there are more than one match
            if (match.Count > 0)
            {
                // Add it to the match dictionary, including the match count.
                matches.Add(s, match.Count);
            }
            // Get the highest max matching
            maxMatch = match.Count > maxMatch ? match.Count : maxMatch;
        });
        // Convert the match dictionary into a list
        var matchList = matches.ToList();

        // Sort the list by decending match counts
        // matchList.Sort((s1, s2) => s2.Value.CompareTo(s1.Value));

        // Remove all matches that is less than the best match.
        matchList.RemoveAll(s => s.Value < maxMatch);

        // If the top value is set and is less than the number of matches
        var getTop = top > 0 && top < matchList.Count ? top : matchList.Count;

        // Add the maches into the result list.
        for (var i = 0; i < getTop; i++)
            results.Add(matchList[i].Key);

        return results;
    }
    public static ArrayList ToArrayList(this IList source)
    {
        var rval = new ArrayList();
        rval.Add(source);
        return rval;
    }
     */
    #endregion Disabled Extensions
}