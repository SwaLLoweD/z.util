﻿/* Copyright (c) <2022> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Z.Collections.Concurrent;
using Z.Collections.Generic;
using Z.Collections.Helpers;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for all kind of Enumerables implementing the IEnumerable&lt;T&gt; interface</summary>
public static class ExtMemory
{
    #region SliceOverloads
    /// <summary>Forms a slice out of the current read-only span starting at a specified index for a specified length.</summary>
    /// <param name="value">Memory area to be sliced</param>
    /// <param name="offset">The index at which to begin this slice. Will be increased by length at the end</param>
    /// <param name="length">The desired length for the slice.</param>
    /// <returns>A read-only span that consists of length elements from the current span starting at start.</returns>
    public static ReadOnlySpan<T> Slicez<T>(this ReadOnlySpan<T> value, ref int offset, int length) { var rval = value.Slice(offset, length); offset += length; return rval; }
    /// <summary>Forms a slice out of the current read-only span starting at a specified index for a specified length.</summary>
    /// <param name="value">Memory area to be sliced</param>
    /// <param name="offset">The index at which to begin this slice. Will be increased by length at the end</param>
    /// <param name="length">The desired length for the slice.</param>
    /// <returns>A read-only span that consists of length elements from the current span starting at start.</returns>
    public static Span<T> Slicez<T>(this Span<T> value, ref int offset, int length) { var rval = value.Slice(offset, length); offset += length; return rval; }
    /// <summary>Forms a slice out of the current read-only span starting at a specified index for a specified length.</summary>
    /// <param name="value">Memory area to be sliced</param>
    /// <param name="offset">The index at which to begin this slice. Will be increased by length at the end</param>
    /// <param name="length">The desired length for the slice.</param>
    /// <returns>A read-only span that consists of length elements from the current span starting at start.</returns>
    public static ReadOnlyMemory<T> Slicez<T>(this ReadOnlyMemory<T> value, ref int offset, int length) { var rval = value.Slice(offset, length); offset += length; return rval; }
    /// <summary>Forms a slice out of the current read-only span starting at a specified index for a specified length.</summary>
    /// <param name="value">Memory area to be sliced</param>
    /// <param name="offset">The index at which to begin this slice. Will be increased by length at the end</param>
    /// <param name="length">The desired length for the slice.</param>
    /// <returns>A read-only span that consists of length elements from the current span starting at start.</returns>
    public static Memory<T> Slicez<T>(this Memory<T> value, ref int offset, int length) { var rval = value.Slice(offset, length); offset += length; return rval; }
    #endregion
}
/// <summary>Action for span</summary>
public delegate void SpanAction<T>(Span<T> span);
/// <summary>Function for span</summary>
public delegate U SpanFunc<T, U>(Span<T> span);

/// <summary>Action for span</summary>
public delegate void ReadOnlySpanAction<T>(ReadOnlySpan<T> span);
/// <summary>Function for span</summary>
public delegate U ReadOnlySpanFunc<T, U>(ReadOnlySpan<T> span);