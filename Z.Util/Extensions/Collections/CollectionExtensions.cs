﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using Z.Collections.Helpers;

namespace Z.Extensions;

/// <summary>Extension methods for all kind of Collections implementing the ICollection&lt;T&gt; interface</summary>
public static class ExtICollection {
    /// <summary>Transform the specified Enumerable.</summary>
    /// <param name="value">The value to be transformed.</param>
    /// <returns>An universal transformer supplying additional transforming methods</returns>
    public static TransformPortion<T> Transform<T>(this ICollection<T> value) => new(value);

    /// <summary>Removes elements from the specified Enumerable.</summary>
    /// <param name="value">The value to be manipulated.</param>
    /// <returns>An universal remove method supplying additional remove methods</returns>
    public static RemovePortion<T> Remove<T>(this ICollection<T> value) => new(value);

    #region Nested type: RemovePortion
    #region Remove
    /// <summary>Multi-method library for IEnumerable item removal related commands.</summary>
    public class RemovePortion<T>: ExtIEnumerable.RemovePortion<T> {
        #region Fields / Properties
        /// <summary>Value as Collection</summary>
        public virtual ICollection<T> ValueCol => (ICollection<T>)Value;
        #endregion

        #region Constructors
        /// <summary>Remove</summary>
        public RemovePortion(ICollection<T> value) : base(value) { }
        #endregion

        /// <summary>Remove an item from the collection with predicate</summary>
        /// <param name="predicate"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <remarks>Contributed by Michael T, http://about.me/MichaelTran </remarks>
        public new virtual void Where(Predicate<T> predicate) {
            var collection = ValueCol;
            if (collection == null) return;
            var deleteList = collection.Where(child => predicate(child)).ToList();
            deleteList.ForEach(t => collection.Remove(t));
        }
    }
    #endregion Remove
    #endregion

    #region Nested type: TransformPortion
    #region Transform
    /// <summary>Multi-method library for IEnumerable transformation related commands.</summary>
    public class TransformPortion<T>: ExtIEnumerable.TransformPortion<T> {
        #region Fields / Properties
        /// <summary>ValueCollection</summary>
        public virtual ICollection<T> ValueCollection => (ICollection<T>)Value;
        #endregion

        #region Constructors
        /// <summary>Transform</summary>
        public TransformPortion(ICollection<T> value) : base(value) { }
        #endregion

        /// <summary>Convert collection to a readonly one.</summary>
        public virtual ICollection<T> Readonly() => Wrappers.ReadOnly(ValueCollection);
    }
    #endregion Transform
    #endregion

    #region Add Methods
    /// <summary>Adds a value uniquely to a collection and returns a value whether the value was added or not.</summary>
    /// <typeparam name="T">The generic collection value type</typeparam>
    /// <param name="collection">The collection.</param>
    /// <param name="value">The value to be added.</param>
    /// <returns>Indicates whether the value was added or not</returns>
    /// <example>
    ///     <code>
    /// 		list.AddUnique(1); // returns true;
    /// 		list.AddUnique(1); // returns false the second time;
    /// 	</code>
    /// </example>
    public static bool AddUnique<T>(this ICollection<T> collection, T value) {
        if (collection.Contains(value)) return false;
        collection.Add(value);
        return true;
    }

    /// <summary>Adds a range of value uniquely to a collection and returns the amount of values added.</summary>
    /// <typeparam name="T">The generic collection value type.</typeparam>
    /// <param name="collection">The collection.</param>
    /// <param name="values">The values to be added.</param>
    /// <returns>The amount if values that were added.</returns>
    public static int AddUnique<T>(this ICollection<T> collection, IEnumerable<T> values) {
        if (collection == null) throw new ArgumentNullException(nameof(collection));
        if (values == null) return 0;
        var count = 0;
        var itemArray = values.ToArray(); //No change during add allowed.
        foreach (var value in itemArray) {
            if (collection.AddUnique(value))
                count++;
        }

        return count;
    }

    /// <summary>Appends a sequence of items to an existing list</summary>
    /// <typeparam name="T">The type of the items in the list</typeparam>
    /// <param name="collection">The list to modify</param>
    /// <param name="items">The sequence of items to add to the list</param>
    /// <returns>The amount if values that were added.</returns>
    public static int Add<T>(this ICollection<T> collection, IEnumerable<T> items) {
        if (collection == null) throw new ArgumentNullException(nameof(collection));
        if (items == null) return 0;
        var count = 0;
        var colType = collection.GetType();
        var itemArray = items.ToArray(); //No change during add allowed.
        if (colType.IsAssignableTo<List<T>>()) {
            var list = (List<T>)collection;
            list.AddRange(itemArray);
            count = itemArray.Length;
        } else if (colType.IsAssignableTo<ISet<T>>()) {
            foreach (var item in itemArray) {
                if (collection.AddUnique(item))
                    count++;
            }
        } else {
            itemArray.ForEach(collection.Add);
            count = itemArray.Length;
        }
        return count;
    }
    #endregion
}