﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;

namespace Z.Extensions;

/// <summary>Extension methods for all kinds of Arrays </summary>
public static class ExtArray {
    /// <summary>Converts the specified Enumerable to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion<T> To<T>(this T[] value) => new(value);

    /// <summary>Clears the array completely</summary>
    /// <param name="array">Array to clear</param>
    /// <typeparam name="T">Array type</typeparam>
    /// <returns>The final array</returns>
    /// <example>
    ///     <code>
    ///  int[] TestObject = new int[] { 1, 2, 3, 4, 5, 6 };
    ///  TestObject.Clear();
    /// </code>
    /// </example>
    public static T[]? Clear<T>(this T[] array) {
        if (array == null) return null;
        Array.Clear(array, 0, array.Length);
        return array;
    }

    /// <summary>Replaces each item in a part of a array with a given value.</summary>
    /// <param name="array">The array to modify.</param>
    /// <param name="value">The value to fill with.</param>
    /// <param name="start">The index at which to start filling. The first index in the array has index 0.</param>
    /// <param name="count">The number of items to fill.</param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="start" /> or <paramref name="count" /> is negative, or
    ///     <paramref name="start" /> + <paramref name="count" /> is greater than <paramref name="array" />.Length.
    /// </exception>
    /// <exception cref="ArgumentNullException"><paramref name="array" /> is null.</exception>
    public static void Fill<T>(this T[] array, T value, int start = 0, int count = 0) {
        if (array == null) throw new ArgumentNullException(nameof(array));

        if (count <= 0) count = array.Length;
        if (start < 0) start = 0;
        if (start >= array.Length) throw new ArgumentOutOfRangeException(nameof(start));
        if (count < 0 || count > array.Length || start > array.Length - count) throw new ArgumentOutOfRangeException(nameof(count));

        for (var i = start; i < count + start; ++i) array[i] = value;
    }

    /// <summary>Returns a block of items from an array</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="array"></param>
    /// <param name="index"></param>
    /// <param name="length"></param>
    /// <param name="padToLength"></param>
    /// <returns></returns>
    /// <remarks>
    ///     Test results prove that Array.Copy is many times faster than Skip/Take and LINQ Item count: 1,000,000
    ///     Array.Copy:     15 ms Skip/Take:  42,464 ms - 42.5 seconds LINQ:          881 ms Contributed by Chris Gessler
    /// </remarks>
    public static T[] BlockCopy<T>(this T[] array, int index, int length, bool padToLength = false) {
        var n = length;
        T[]? b = null;

        if (array.Length < index + length) {
            n = array.Length - index;
            if (padToLength) b = new T[length];
        }

        if (b == null) b = new T[n];
        Array.Copy(array, index, b, 0, n);
        return b;
    }
    /// <summary>Check if the index is within the array</summary>
    /// <param name="source"></param>
    /// <param name="index"></param>
    /// <param name="dimension"></param>
    /// <returns></returns>
    /// <remarks>Contributed by Michael T, http://about.me/MichaelTran</remarks>
    public static bool HasIndex(this Array source, int index, int dimension = 0) => source != null && index >= source.GetLowerBound(dimension) && index <= source.GetUpperBound(dimension);
    /// <summary>Find the first occurence of an T[] in another T[]</summary>
    /// <param name="source">the T[] to search in</param>
    /// <param name="toSearch">the T[] to find</param>
    /// <returns>the first position of the found T[] or -1 if not found</returns>
    /// <remarks>Contributed by blaumeister, http://www.codeplex.com/site/users/view/blaumeiser </remarks>
    public static int IndexOf<T>(this T[] source, T[] toSearch) {
        if (toSearch == null || source == null) return -1;
        if (toSearch.Length == 0) return 0; // by definition empty sets match immediately

        var j = -1;
        var end = source.Length - toSearch.Length;
        while ((j = Array.IndexOf(source, toSearch[0], j + 1)) <= end && j != -1) {
            var i = 1;
            while (Equals(source[j + i], toSearch[i])) {
                if (++i == toSearch.Length)
                    return j;
            }
        }
        return -1;
    }

    #region Nested type: ConvertPortion
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class ConvertPortion<T>: ExtIEnumerable.ConvertPortion<T> {
        #region Constructors
        /// <summary>Convert</summary>
        public ConvertPortion(T[] value) : base(value) { }
        #endregion

        /// <summary>Converts the specified value to a different type.</summary>
        /// <returns>An universal converter supplying additional target conversion methods</returns>
        public IEnumerable<T> Enumerable() => Value;
    }
    #endregion
}