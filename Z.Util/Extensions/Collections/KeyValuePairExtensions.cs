﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Z.Collections.Concurrent;
using Z.Collections.Generic;
using Z.Collections.Helpers;

namespace Z.Extensions;

/// <summary>Extension methods for all kind of KeyValuePairs</summary>
public static class ExtKeyValuePair
{
    /// <summary>Converts the specified Enumerable to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion<TKey, TValue> To<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> value) where TKey : notnull => new(value);

    #region Nested type: ConvertPortion
    /// <summary>Multi-method library for KeyValuePair conversion related commands.</summary>
    public class ConvertPortion<TKey, TValue> : ExtIEnumerable.ConvertPortion<KeyValuePair<TKey, TValue>> where TKey : notnull
    {
        #region Fields / Properties
        /// <summary>Value as Dictionary</summary>
        public IEnumerable<KeyValuePair<TKey, TValue>> ValueDictionary { get; set; }
        #endregion

        #region Constructors
        /// <summary>Convert</summary>
        public ConvertPortion(IEnumerable<KeyValuePair<TKey, TValue>> value) : base(value) => ValueDictionary = value;
        #endregion

        ///<summary>Convert KeyValue items into a thread-safe Sorted Dictionary</summary>
        public SyncSortedDictionary<TKey, TValue> ConcurrentSortedDictionary() => new(ValueDictionary);

        ///<summary>Convert KeyValue items into a Sorted Dictionary</summary>
        public SortedDictionary<TKey, TValue> SortedDictionary() => new(Wrappers.Dictionary(ValueDictionary));

        ///<summary>Convert KeyValue items into a Sorted Hashtable using specified sorting field selector</summary>
        public SortedDictionary<TSort, TKey, TValue> SortedTable<TSort>(Func<TValue, TSort> sortSelector) where TSort : notnull {
            var rval = new SortedDictionary<TSort, TKey, TValue>();
            foreach (var t in ValueDictionary) rval[sortSelector(t.Value), t.Key] = t.Value;
            return rval;
        }
        ///<summary>Convert KeyValue items into a SortedList</summary>
        public SortedList<TKey, TValue> SortedList() => new(Wrappers.Dictionary(ValueDictionary));

        ///<summary>Convert KeyValue items into a Dictionary</summary>
        public Dictionary<TKey, TValue> Dictionary() => new(Wrappers.Dictionary(ValueDictionary));

        ///<summary>Convert KeyValue items into a thread-safe Dictionary</summary>
        public ConcurrentDictionary<TKey, TValue> ConcurrentDictionary() => new(ValueDictionary);

        ///<summary>Get a KeyValue Enumerable of items contained in the collection</summary>
        public IEnumerable<KeyValuePair<TKey, TValue>> KeyValuePair() {
            using var e = ValueDictionary.GetEnumerator();
            while (e.MoveNext()) yield return e.Current;
        }
    }
    #endregion
}