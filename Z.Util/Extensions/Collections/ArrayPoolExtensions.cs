/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System.Buffers;

namespace Z.Extensions;

/// <summary>Extension methods for all kinds of ArrayPools </summary>
public static class ExtArrayPool
{
    /// <summary>Rents an array and performans actions on it then returns the array to the pool.</summary>
    /// <param name="pool">The ArrayPool</param>
    /// <param name="minLength">Minimum length of the array to be rented</param>
    /// <param name="action">Action to be performed with the array rented</param>
    /// <param name="clearFirst">Clear the array relevant portion before use</param>
    /// <returns>The value of the processed function</returns>
    public static void RentDo<T>(this ArrayPool<T> pool, int minLength, Action<T[]> action, bool clearFirst = true) {
        var arr = pool.Rent(minLength);
        if (clearFirst) Array.Clear(arr, 0, minLength);
        try {
            action(arr);
        } finally {
            pool.Return(arr);
        }
    }
    /// <summary>Rents an array and performans actions on it then returns the array to the pool.</summary>
    /// <param name="pool">The ArrayPool</param>
    /// <param name="minLength">Minimum length of the array to be rented</param>
    /// <param name="func">Function to be performed with the array rented</param>
    /// <param name="clearFirst">Clear the array relevant portion before use</param>
    /// <returns>The value of the processed function</returns>
    public static U RentDo<T, U>(this ArrayPool<T> pool, int minLength, Func<T[], U> func, bool clearFirst = true) {
        var arr = pool.Rent(minLength);
        if (clearFirst) Array.Clear(arr, 0, minLength);
        U rval;
        try {
            rval = func(arr);
        } finally {
            pool.Return(arr);
        }
        return rval;
    }
    /// <summary>Rents an array and performans actions on it then returns the array to the pool.</summary>
    /// <param name="pool">The ArrayPool</param>
    /// <param name="length">Minimum length of the array to be rented</param>
    /// <param name="action">Action to be performed with the array rented</param>
    /// <param name="clearFirst">Clear the array relevant portion before use</param>
    /// <returns>The value of the processed function</returns>
    public static void RentSpanDo<T>(this ArrayPool<T> pool, int length, SpanAction<T> action, bool clearFirst = true)
        => RentDo<T>(pool, length, (arr) => action(arr.AsSpan(0, length)), clearFirst);
    /// <summary>Rents an array and performans actions on it then returns the array to the pool.</summary>
    /// <param name="pool">The ArrayPool</param>
    /// <param name="length">Minimum length of the array to be rented</param>
    /// <param name="func">Function to be performed with the array rented</param>
    /// <param name="clearFirst">Clear the array relevant portion before use</param>
    /// <returns>The value of the processed function</returns>
    public static U RentSpanDo<T, U>(this ArrayPool<T> pool, int length, SpanFunc<T,U> func, bool clearFirst = true)
        => RentDo<T, U>(pool, length, (arr) => func(arr.AsSpan(0, length)), clearFirst);
    /// <summary>Rents an array and performans actions on it then returns the array to the pool.</summary>
    /// <param name="pool">The ArrayPool</param>
    /// <param name="length">Minimum length of the array to be rented</param>
    /// <param name="action">Action to be performed with the array rented</param>
    /// <param name="clearFirst">Clear the array relevant portion before use</param>
    /// <returns>The value of the processed function</returns>
    public static void RentMemDo<T>(this ArrayPool<T> pool, int length, Action<Memory<T>> action, bool clearFirst = true)
        => RentDo<T>(pool, length, (arr) => action(arr.AsMemory(0, length)), clearFirst);
    /// <summary>Rents an array and performans actions on it then returns the array to the pool.</summary>
    /// <param name="pool">The ArrayPool</param>
    /// <param name="length">Minimum length of the array to be rented</param>
    /// <param name="func">Function to be performed with the array rented</param>
    /// <param name="clearFirst">Clear the array relevant portion before use</param>
    /// <returns>The value of the processed function</returns>
    public static U RentMemDo<T, U>(this ArrayPool<T> pool, int length, Func<Memory<T>, U> func, bool clearFirst = true) 
        => RentDo<T, U>(pool, length, (arr) => func(arr.AsMemory(0, length)), clearFirst);
}