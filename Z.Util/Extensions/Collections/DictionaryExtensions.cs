﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Z.Extensions;

/// <summary>Extension methods for all kind of Dictionaries implementing the IDictionary&lt;T&gt; interface</summary>
public static class ExtIDictionary
{
    /// <summary>Converts the specified Enumerable to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion<T, U> To<T, U>(this IDictionary<T, U> value) where T:notnull => new(value);

    /// <summary>Tries to get value from a dictionary, if key does not exist returns default value instead</summary>
    public static U? TryGetValue<T, U>(this IDictionary<T, U> dict, T key, U? notFoundValue = default) {
        if (!dict.TryGetValue(key, out U? rval)) return notFoundValue;
        return rval;
    }
    /// <summary>Removes a dictionary item by value (Expensive)</summary>
    /// <param name="dict"></param>
    /// <param name="value">Value of the item to be removed</param>
    /// <param name="removeAll">Remove all instances or the first encountered one</param>
    public static bool RemoveByValue<T, U>(this IDictionary<T, U> dict, U value, bool removeAll = true) {
        var success = true;
        foreach (var item in dict.Where(kvp => Equals(kvp.Value, value)).ToList()) {
            success &= dict.Remove(item.Key);
            if (!removeAll) break;
        }
        return success;
    }

    #region Nested type: ConvertPortion
    #region Converter
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class ConvertPortion<T, U> : ExtKeyValuePair.ConvertPortion<T, U> where T:notnull
    {
        #region Fields / Properties
        /// <summary>Value</summary>
        public new IDictionary<T, U> ValueDictionary { get; set; }
        #endregion

        #region Constructors
        /// <summary>Cosntructor</summary>
        public ConvertPortion(IDictionary<T, U> value) : base(value) => ValueDictionary = value;
        #endregion
    }
    #endregion Converter
    #endregion

    #region Concurrent Methods
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value) {
        if (dict == null) return false;
        lock (dict) {
            if (dict.ContainsKey(key)) return false;
            dict.Add(key, value);
            return true;
        }
    }
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static bool TryRemove<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, [System.Diagnostics.CodeAnalysis.MaybeNullWhen(false)] out TValue value) {
        value = default;
        if (dict == null) return false;
        lock (dict) {
            var rval = false;
            if (dict.ContainsKey(key)) {
                value = dict[key];
                rval = dict.Remove(key);
            }
            return rval;
        }
    }
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static bool TryUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue newValue, TValue comparisonValue) {
        if (dict == null) return false;
        lock (dict) {
            if (!dict.ContainsKey(key)) return false;
            var oldValue = dict[key];
            if (!Equals(oldValue, comparisonValue)) return false;
            dict[key] = newValue;
            return true;
        }
    }
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static bool TryUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, Func<TKey, TValue, TValue> updateValueFactory) {
        if (dict == null) return false;
        lock (dict) {
            if (!dict.ContainsKey(key)) return false;
            var oldValue = dict[key];
            var newValue = updateValueFactory(key, oldValue);
            dict[key] = newValue;
            return true;
        }
    }
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, Func<TKey, TValue> valueFactory) {
        if (dict == null) throw new ArgumentNullException(nameof(dict));
        lock (dict) {
            TValue value;
            if (dict.ContainsKey(key)) return dict[key];
            value = valueFactory(key);
            dict.Add(key, value);
            return value;
        }
    }
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue value) => dict.GetOrAdd(key, _ => value);
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static TValue AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory) {
        if (dict == null) throw new ArgumentNullException(nameof(dict));
        lock (dict) {
            TValue value;
            if (dict.ContainsKey(key)) {
                var oldValue = dict[key];
                value = updateValueFactory(key, oldValue);
            } else {
                value = addValueFactory(key);
            }
            dict.Add(key, value);
            return value;
        }
    }
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static TValue AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory) =>
        dict.AddOrUpdate(key, _ => addValue, updateValueFactory);
    /// <summary>Concurrent collection mimicing methods. These extension methods are thread-safe only between themselves.</summary>
    public static TValue AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue addValue, TValue updateValue) =>
        dict.AddOrUpdate(key, _ => addValue, (_, _) => updateValue);
    #endregion Concurrent Methods
}