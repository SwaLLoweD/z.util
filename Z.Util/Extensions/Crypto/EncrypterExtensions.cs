﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
using Z.Cryptography;

namespace Z.Extensions;
#pragma warning disable CS1634, IDE0060

/// <summary>Extension methods for Encrypter</summary>
public static class ExtCEncrypter
{
    /// <summary>.NET Native AES Encryption.(Key sizes: 128, 192, 256, Block sizes: 128) D:128</summary>
    public static IEncryptor Aes(this Encrypter enc, ReadOnlyMemory<byte> key, ReadOnlyMemory<byte> salt = default, CipherPrepend prepend = CipherPrepend.IV, Action<Aes>? encOpts = null)
        => enc.CreateCryptoEngine<Aes>(key, salt, prepend, encOpts);
    // /// <summary>.NET Native Rijndael Encryption. (Key sizes: 128, 192, 256, Block sizes: 128, 192, 256) D:256/128</summary>
    // public static IEncryptor Rijndael(this Encrypter enc, byte[] key, byte[] salt = null, CipherPrepend prepend = CipherPrepend.None, Action<Rijndael> encOpts = null) =>
    //     enc.CreateCryptoEngine<Rijndael>(key, salt, prepend, encOpts);
    /// <summary>.NET Native RC2 Encryption. (Key sizes: 8-1024, Block sizes: 64) D:128</summary>
    public static IEncryptor RC2(this Encrypter enc, ReadOnlyMemory<byte> key, ReadOnlyMemory<byte> salt = default, CipherPrepend prepend = CipherPrepend.IV, Action<RC2>? encOpts = null)
        => enc.CreateCryptoEngine<RC2>(key, salt, prepend, encOpts);
    /// <summary>.NET Native 3DES Encryption. (Key sizes: 128, 192, Block sizes: 64) D:192</summary>
    public static IEncryptor TDES(this Encrypter enc, ReadOnlyMemory<byte> key, ReadOnlyMemory<byte> salt = default, CipherPrepend prepend = CipherPrepend.IV, Action<TripleDES>? encOpts = null) 
        => enc.CreateCryptoEngine<TripleDES>(key, salt, prepend, encOpts);
    /// <summary>Custom Aes-Gcm Encryption.(Key sizes: 256, Block sizes: 96) D:128 Tag:128</summary>
    public static IEncryptor AesGcmz(this Encrypter enc, ReadOnlyMemory<byte> key, ReadOnlyMemory<byte> salt = default, CipherPrepend prepend = CipherPrepend.IV) => new AesGcmz(key, enc, salt, prepend);
    /// <summary>Create Rsa</summary>
    public static RSA Rsa(this Encrypter enc, ReadOnlySpan<byte> spkiPubkey, ReadOnlySpan<byte> pkcs8Privkey) {
        var rsa = RSA.Create();
        if (!spkiPubkey.IsEmpty) rsa.ImportSubjectPublicKeyInfo(spkiPubkey, out _);
        if (!pkcs8Privkey.IsEmpty) rsa.ImportPkcs8PrivateKey(pkcs8Privkey, out _);
        return rsa;
    }
    /// <summary>Custom Rsa with any symmetric algorithm for bigger packets</summary>
    public static IEncryptor Rsaz<T>(this Encrypter enc, ReadOnlySpan<byte> spkiPubkey, ReadOnlySpan<byte> pkcs8Privkey, RSAEncryptionPadding? padding = default, bool prependIv = true) where T : SymmetricAlgorithm 
        => Rsa(enc, spkiPubkey, pkcs8Privkey).Crypto<T>(padding, prependIv, enc);
    /// <summary>Custom Rsa with Aesgcm for bigger packets</summary>
    public static IEncryptor RsaAesGcm(this Encrypter enc, ReadOnlySpan<byte> spkiPubkey, ReadOnlySpan<byte> pkcs8Privkey, RSAEncryptionPadding? padding = default, bool prependIv = true)
        => Rsa(enc, spkiPubkey, pkcs8Privkey).CryptoAesGcm(padding, prependIv, enc);
    #pragma warning disable
    /// <summary>X509 Certificate tools.</summary>
    public static X509Util X509(this Encrypter enc) => new();
}
