using System;
using System.Security.Cryptography;
using System.Text;

namespace Z.Extensions;

/// <summary>Hash algorithm overloads</summary>
public static class ExtHashAlgorithm
{
    /// <summary>Hash algorithm string overload</summary>
    /// <param name="alg">Hash Algorithm</param>
    /// <param name="input">string to hash as UTF-8</param>
    /// <returns>output hash as Base64 string</returns>
    public static string ComputeHash(this HashAlgorithm alg, string input) => Convert.ToBase64String(alg.ComputeHash(Encoding.UTF8.GetBytes(input)));

    /// <summary>KeyedHashAlgorithm overload</summary>
    /// <param name="alg">Hash Algorithm</param>
    /// <param name="input">bytes[] to hash</param>
    /// <param name="key">byte[] key to perform hash</param>
    /// <returns>Hashed bytes[]</returns>
    public static byte[] ComputeHash(this KeyedHashAlgorithm alg, byte[] input, byte[] key) {
        alg.Key = key;
        return alg.ComputeHash(input);
    }

    /// <summary>KeyedHashAlgorithm string overload</summary>
    /// <param name="alg">Hash Algorithm</param>
    /// <param name="input">string to hash</param>
    /// <param name="key">byte[] key to perform hash</param>
    /// <returns>Base64 encoded Hash string</returns>
    public static string ComputeHash(this KeyedHashAlgorithm alg, string input, string key) => Convert.ToBase64String(alg.ComputeHash(Encoding.UTF8.GetBytes(input), Encoding.UTF8.GetBytes(key)));
}