using System;
using System.Security.Cryptography;

namespace Z.Extensions;

/// <summary>Extensions Simplifying the keyExchange for multi-platform usage</summary>
public static class ExtECDiffieHellman
{
    /// <summary>Create other party ECDiffieHellmanPublicKey from byte[]</summary>
    /// <param name="ecdh">EC Diffie-Hellman algorithm</param>
    /// <param name="otherPartyPublicKey">The public key of the party with which to derive a mutual secret.</param>
    public static ECDiffieHellmanPublicKey CreateOtherPartyPublicKey(this ECDiffieHellman ecdh, ReadOnlySpan<byte> otherPartyPublicKey) {
        var p = ecdh.ExportParameters(false);
        var remoteEcdh = ECDiffieHellman.Create(p);
        remoteEcdh.ImportSubjectPublicKeyInfo(otherPartyPublicKey, out int _);
        return remoteEcdh.PublicKey;
    }
}