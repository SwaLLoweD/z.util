﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using System.Security.Cryptography;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for ICryptoTransform</summary>
public static class ExtICryptoTransform
{
    /// <summary>Do Encryption or Decryption using the ICryptoTransform object</summary>
    /// <param name="enc">Cyrpto Algorithm</param>
    /// <param name="data">Data to be encrypted</param>
    /// <returns>Encrypted bytes</returns>
    public static byte[] Encrypt(this ICryptoTransform enc, byte[] data) {
        byte[] rval;
        using (var stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "ExtEncrypt ", data.Length))
        using (var cStream = new CryptoStream(stream, enc, CryptoStreamMode.Write)) {
            cStream.Write(data, 0, data.Length);
            cStream.Flush();
            cStream.Close();
            rval = stream.ToArray();
            stream.Flush();
            stream.Close();
        }
        return rval;
    }
}