﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
using Z.Cryptography;
using Z.Util;

namespace Z.Extensions;

/// <summary>Extension methods for SymmetricAlgorithms</summary>
public static class ExtSymmetricAlgorithm
{
    /// <summary>Create Encryptor alternative</summary>
    public static ICryptoTransform CreateEncryptor(this SymmetricAlgorithm enc, string pwd, int keySize = 128, int blockSize = 128, byte[]? salt = null) =>
        CreateEncryptorInner(enc, true, pwd, keySize, blockSize, salt);
    /// <summary>Create Encryptor alternative</summary>
    public static ICryptoTransform CreateEncryptor(this SymmetricAlgorithm enc, byte[] pwd, int keySize = 128, int blockSize = 128, byte[]? salt = null) =>
        CreateEncryptorInner(enc, true, pwd, keySize, blockSize, salt);

    /// <summary>Create Decryptor alternative</summary>
    public static ICryptoTransform CreateDecryptor(this SymmetricAlgorithm enc, string pwd, int keySize = 128, int blockSize = 128, byte[]? salt = null) =>
        CreateEncryptorInner(enc, false, pwd, keySize, blockSize, salt);
    /// <summary>Create Decryptor alternative</summary>
    public static ICryptoTransform CreateDecryptor(this SymmetricAlgorithm enc, byte[] pwd, int keySize = 128, int blockSize = 128, byte[]? salt = null) =>
        CreateEncryptorInner(enc, false, pwd, keySize, blockSize, salt);

    private static ICryptoTransform CreateEncryptorInner(SymmetricAlgorithm enc, bool encrypt, string pwd, int keySize = 128, int blockSize = 128, byte[]? salt = null) {
        enc.KeySize = keySize;
        enc.BlockSize = blockSize;
        var p = Encrypter.DeriveBytes(pwd, keySize / 8, salt);
        var iv = Encrypter.DeriveBytes(pwd, blockSize / 8, salt);
        return encrypt ? enc.CreateEncryptor(p, iv) : enc.CreateDecryptor(p, iv);
    }
    private static ICryptoTransform CreateEncryptorInner(SymmetricAlgorithm enc, bool encrypt, byte[] pwd, int keySize = 128, int blockSize = 128, byte[]? salt = null) {
        enc.KeySize = keySize;
        enc.BlockSize = blockSize;
        var p = Encrypter.DeriveBytes(pwd, keySize / 8, salt);
        var iv = Encrypter.DeriveBytes(pwd, blockSize / 8, salt);
        return encrypt ? enc.CreateEncryptor(p, iv) : enc.CreateDecryptor(p, iv);
    }
}