﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Buffers;
using Z.Cryptography;

namespace Z.Extensions;

/// <summary>Extension methods for IEncryptor</summary>
public static class ExtIEncryptorExtensions
{
    #region Encrypt
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Data to Encrypt</param>
    /// <param name="iv">Vector</param>
    /// <returns>Encrypted data</returns>
    public static byte[] Encrypt(this IEncryptor enc, ReadOnlySpan<byte> source, ReadOnlySpan<byte> iv = default) {
        using var stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "BEncrypter Encrypt", source.Length);
        enc.Encrypt(source, stream, iv);
        return stream.ToArray();
    }
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be encrypt will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="iv">Vector</param>
    public static byte[] Encrypt(this IEncryptor enc, Stream source, int len, ReadOnlyMemory<byte> iv = default) {
        return ArrayPool<byte>.Shared.RentSpanDo(len, (sourceMem) => {
            source.ReadBytes(sourceMem);
            return Encrypt(enc, sourceMem, iv.Span);
        });
    }
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be encrypt will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="destination">Destination stream the encryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams</param>
    /// <param name="iv">Vector</param>
    public static long Encrypt(this IEncryptor enc, Stream source, long len, Stream destination, int? bufferSize = null, ReadOnlySpan<byte> iv = default) {
        var initialPos = destination.Position;
        enc.HeaderWrite(destination, iv);
        using var c = enc.CreateCryptoStream(destination, true);
        source.CopyTo(c, null, len, bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize);
        //written += source.CopyTo(destination, (b, d) => enc.Encrypt(b.Span, d, iv.Span, true), len, (bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize) - 1); // -1 buffer to allow paddingOverhead
        return destination.Position - initialPos;
    }
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be encrypt will be read from</param>
    /// <param name="destination">Destination stream the encryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams</param>
    /// <param name="iv">Vector</param>
    public static long Encrypt(this IEncryptor enc, Stream source, Stream destination, int? bufferSize = null, ReadOnlySpan<byte> iv = default)
        => Encrypt(enc, source, -1, destination, bufferSize, iv);
    #endregion
    
    #region EncryptAsync
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Data to Encrypt</param>
    /// <param name="iv">Vector</param>
    /// <returns>Encrypted data</returns>
    public static async Task<byte[]> EncryptAsync(this IEncryptor enc, ReadOnlyMemory<byte> source, ReadOnlyMemory<byte> iv = default) {
        using var stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "Encrypter Encrypt", source.Length);
        await enc.EncryptAsync(source, stream, iv);
        return stream.ToArray();
    }
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be encrypt will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="iv">Vector</param>
    public static async Task<byte[]> EncryptAsync(this IEncryptor enc, Stream source, int len, ReadOnlyMemory<byte> iv = default) {
        return await ArrayPool<byte>.Shared.RentMemDo(len, async (sourceMem) => {
            source.ReadBytes(sourceMem.Span);
            return await EncryptAsync(enc, sourceMem, iv);
        });
    }
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be encrypt will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="destination">Destination stream the encryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams</param>
    /// <param name="iv">Vector</param>
    public static async Task<long> EncryptAsync(this IEncryptor enc, Stream source, long len, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> iv = default) {
        var initialPos = destination.Position;
        await enc.HeaderWriteAsync(destination, iv);
        using var c = enc.CreateCryptoStream(destination, true);
        await source.CopyToAsync(c, null, len, bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize);
        //written += source.CopyTo(destination, (b, d) => enc.Encrypt(b.Span, d, iv.Span, true), len, (bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize) - 1); // -1 buffer to allow paddingOverhead
        return destination.Position - initialPos;
    }
    /// <summary>Encrypt data</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be encrypt will be read from</param>
    /// <param name="destination">Destination stream the encryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams</param>
    /// <param name="iv">Vector</param>
    public static Task<long> EncryptAsync(this IEncryptor enc, Stream source, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> iv = default)
    => EncryptAsync(enc, source, -1, destination, bufferSize, iv);
    #endregion

    #region Decrypt
    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Data to Decrypt</param>
    /// <param name="iv">Vector</param>
    /// <returns>Decrypted data</returns>
    public static byte[] Decrypt(this IEncryptor enc, ReadOnlySpan<byte> source, ReadOnlySpan<byte> iv = default) {
        using var stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "Encrypter Decrypt", source.Length);
        enc.Decrypt(source, stream, iv);
        return stream.ToArray();
    }
    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be decrypted will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="iv">Vector</param>
    public static byte[] Decrypt(this IEncryptor enc, Stream source, int len, ReadOnlyMemory<byte> iv = default) {
        return ArrayPool<byte>.Shared.RentSpanDo(len, (sourceMem) => {
            source.ReadBytes(sourceMem);
            return Decrypt(enc, sourceMem, iv.Span);
        });
    }
    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be decrypted will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="destination">Destination stream the decryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams</param>
    /// <param name="iv">Vector</param>
    public static long Decrypt(this IEncryptor enc, Stream source, long len, Stream destination, int? bufferSize = null, ReadOnlySpan<byte> iv = default) {
        var initialPos = destination.Position;
        enc.HeaderRead(source, iv);
        using var c = enc.CreateCryptoStream(destination, false);
        source.CopyTo(c, null, len, bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize);
        //source.CopyTo(destination, (b, d) => enc.Decrypt(b.Span, d, iv.Span, true), len, (bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize) + enc.TagSize);
        return destination.Position - initialPos;
    }

    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be decrypted will be read from</param>
    /// <param name="destination">Destination stream the decryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams (TagLength will be added)</param>
    /// <param name="iv">Vector</param>
    public static long Decrypt(this IEncryptor enc, Stream source, Stream destination, int? bufferSize = null, ReadOnlySpan<byte> iv = default)
    => Decrypt(enc, source, -1, destination, bufferSize, iv);
    #endregion

    #region DecryptAsync
    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Data to Decrypt</param>
    /// <param name="iv">Vector</param>
    /// <returns>Decrypted data</returns>
    public static async Task<byte[]> DecryptAsync(this IEncryptor enc, ReadOnlyMemory<byte> source, ReadOnlyMemory<byte> iv = default) {
        using var stream = ZUtilCfg.GlobalMemoryStreamManager.GetStream(Guid.NewGuid(), "Encrypter Decrypt", source.Length);
        await enc.DecryptAsync(source, stream, iv);
        return stream.ToArray();
    }
    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be decrypted will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="iv">Vector</param>
    public static async Task<byte[]> DecryptAsync(this IEncryptor enc, Stream source, int len, ReadOnlyMemory<byte> iv = default) {
        return await ArrayPool<byte>.Shared.RentMemDo(len, async (sourceMem) => {
            await source.ReadBytesAsync(sourceMem);
            return await DecryptAsync(enc, sourceMem, iv);
        });
    }
    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be decrypted will be read from</param>
    /// <param name="len">Length of items to be read from the source</param>
    /// <param name="destination">Destination stream the decryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams</param>
    /// <param name="iv">Vector</param>
    public static async Task<long> DecryptAsync(this IEncryptor enc, Stream source, long len, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> iv = default) {
        var initialPos = destination.Position;
        await enc.HeaderReadAsync(source, iv);
        using var c = enc.CreateCryptoStream(destination, false);
        await source.CopyToAsync(c, null, len, bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize);
        //source.CopyTo(destination, (b, d) => enc.Decrypt(b.Span, d, iv.Span, true), len, (bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize) + enc.TagSize);
        return destination.Position - initialPos;
    }
    /// <summary>Decryption</summary>
    /// <param name="enc">Encryptor</param>
    /// <param name="source">Source stream that the data to be decrypted will be read from</param>
    /// <param name="destination">Destination stream the decryption result will be written into</param>
    /// <param name="bufferSize">Buffer size to copy between streams</param>
    /// <param name="iv">Vector</param>
    public static Task<long> DecryptAsync(this IEncryptor enc, Stream source, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> iv = default)
        => DecryptAsync(enc, source, -1, destination, bufferSize, iv);
    #endregion

    #region CreateStream (disabled)
    // /// <summary>Create an encryption stream in write mode</summary>
    // public static CryptoSafeStream<MemoryStream> CreateEncryptionStream(this IEncryptor enc, ReadOnlySpan<byte> iv = default) 
    //     => enc.CreateEncryptionStream(ZUtilCfg.GlobalMemoryStreamManager.GetStream("CreateEncryptionStream"), iv);
    // /// <summary>Create an encryption stream in write mode</summary>
    // public static async Task<CryptoSafeStream<MemoryStream>> CreateEncryptionStreamAsync(this IEncryptor enc, ReadOnlyMemory<byte> iv = default)
    //     => await enc.CreateEncryptionStreamAsync(ZUtilCfg.GlobalMemoryStreamManager.GetStream("CreateEncryptionStream"), iv);
    // /// <summary>Create a decryption stream in write mode</summary>
    // public static CryptoSafeStream<MemoryStream> CreateDecryptionStream(this IEncryptor enc, ReadOnlySpan<byte> iv = default) 
    //     => enc.CreateDecryptionStream(ZUtilCfg.GlobalMemoryStreamManager.GetStream("CreateDecryptionStream"), iv);

    #endregion
}