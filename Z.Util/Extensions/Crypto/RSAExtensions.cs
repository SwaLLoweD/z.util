﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
using Z.Cryptography;
namespace Z.Extensions;

/// <summary>Extension methods for Encrypter</summary>
public static class ExtRSA
{
    /// <summary>
    ///     Does RSA Decryption if data fits the container, otherwise decrypts data with random key Symmetric Algorithm
    ///     which is gathered by RSA decryption first
    /// </summary>
    /// <param name="rsa">RSA Encryption Manager</param>
    /// <param name="padding">RSA encryption padding</param>
    /// <param name="prependIv">If the encrypted bytes contain a prepended IV or not</param>
    /// <param name="e">Crypto engine</param>
    public static Rsaz Crypto<T>(this RSA rsa, RSAEncryptionPadding? padding = null, bool prependIv = true, Encrypter? e = null) where T : SymmetricAlgorithm
        => new(rsa, ()=>(e ?? Encrypter.Default).CreateCryptoEngine<T>(), padding, e, null, prependIv);

    /// <summary>
    ///     Does RSA Decryption if data fits the container, otherwise decrypts data with random key Symmetric Algorithm
    ///     which is gathered by RSA decryption first
    /// </summary>
    /// <param name="rsa">RSA Encryption Manager</param>
    /// <param name="padding">RSA encryption padding</param>
    /// <param name="prependIv">If the encrypted bytes contain a prepended IV or not</param>
    /// <param name="e">Crypto engine</param>
    public static Rsaz CryptoAesGcm(this RSA rsa, RSAEncryptionPadding? padding = null, bool prependIv = true, Encrypter? e = null)
        => new(rsa, () => new AesGcmz(default, e, default, prependIv ? CipherPrepend.IV : CipherPrepend.None), padding, e, null, prependIv);
}