﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Z.Extensions;

/// <summary>Extension methods for X509Certificate2</summary>
public static class ExtX509Certificate2
{
    /// <summary>Sign with X509 certificate</summary>
    /// <param name="cert">X509 certificate that has its private key set</param>
    /// <param name="b">Data to sign</param>
    /// <remarks>Private key of the used X509Ceritificate must be set</remarks>
    public static byte[] Sign(this X509Certificate2 cert, byte[] b) {
        var dsaKey = cert.GetECDsaPrivateKey();
        var rsaKey = cert.GetRSAPrivateKey();
        if (dsaKey != null) return dsaKey.SignData(b, HashAlgorithmName.SHA256);
        if (rsaKey != null) return rsaKey.SignData(b, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
        //CryptoConfig.MapNameToOID("SHA256")
        throw new KeyNotFoundException();
        //Oid contentOid = new Oid("1.2.840.113549.1.7.1", "PKCS 7 Data");
        //SignedCms signedMessage = new SignedCms(new ContentInfo(contentOid, b), detached);
        //signedMessage.ComputeSignature(new CmsSigner(cert));
        //byte[] signedBytes = signedMessage.Encode();
        //return signedBytes;
    }
    /// <summary>Verify signature with X509 certificate</summary>
    /// <param name="cert">X509 certificate</param>
    /// <param name="b">Signed Data</param>
    /// <param name="signature">Signature to verify</param>
    /// <returns>True if signature is authentic, false if not</returns>
    public static bool SignVerify(this X509Certificate2 cert, ReadOnlySpan<byte> b, ReadOnlySpan<byte> signature) {
        var dsaKey = cert.GetECDsaPrivateKey();
        var rsaKey = cert.GetRSAPrivateKey();
        if (dsaKey != null) return dsaKey.VerifyData(b, signature, HashAlgorithmName.SHA256);
        if (rsaKey != null) return rsaKey.VerifyData(b, signature, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
        //CryptoConfig.MapNameToOID("SHA256")
        return false;
    }

    /// <summary>Sign with X509 certificate</summary>
    /// <param name="cert">X509 certificate that has its private key set</param>
    /// <param name="s">Data to sign</param>
    /// <remarks>Private key of the used X509Ceritificate must be set</remarks>
    public static string Sign(this X509Certificate2 cert, string s) => Sign(cert, s.To().ByteArray.AsUtf8).To().String.AsBase64;

    /// <summary>Verify signature with X509 certificate</summary>
    /// <param name="cert">X509 certificate</param>
    /// <param name="s">Signed Data</param>
    /// <param name="signature">Signature to verify</param>
    /// <returns>True if signature is authentic, false if not</returns>
    public static bool SignVerify(this X509Certificate2 cert, string s, string signature) => SignVerify(cert, s.To().ByteArray.AsUtf8, signature.To().ByteArray.AsBase64);

    /// <summary>Verify the certificate is a valid one signed by this CA</summary>
    /// <param name="authority">CA Certificate</param>
    /// <param name="certificateToValidate">certificate to be validated by this CA</param>
    /// <param name="validateChain">Check the whole chain using slightly relaxed default checks</param>
    public static bool VerifyChild(this X509Certificate2 authority, X509Certificate2 certificateToValidate, bool validateChain = false) =>
        VerifyChild(authority, certificateToValidate, out _, validateChain);
    /// <summary>Verify the certificate is a valid one signed by this CA</summary>
    /// <param name="authority">CA Certificate</param>
    /// <param name="certificateToValidate">certificate to be validated by this CA</param>
    /// <param name="errors">Found chain validation errors</param>
    /// <param name="validateChain">Check the whole chain using slightly relaxed default checks</param>
    public static bool VerifyChild(this X509Certificate2 authority, X509Certificate2 certificateToValidate, out IEnumerable<X509ChainStatus> errors, bool validateChain = true) {
        errors = Array.Empty<X509ChainStatus>();
        var chain = new X509Chain();
        chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
        chain.ChainPolicy.RevocationFlag = X509RevocationFlag.ExcludeRoot;
        chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority;
        chain.ChainPolicy.VerificationTime = DateTime.Now;
        chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 0, 0);

        // This part is very important. You're adding your known root here.
        // It doesn't have to be in the computer store at all. Neither certificates do.
        chain.ChainPolicy.ExtraStore.Add(authority);

        var isChainValid = chain.Build(certificateToValidate);

        if (validateChain && !isChainValid) {
            errors = chain.ChainStatus;
            return false;
        }

        // This piece makes sure it actually matches your known root
        return chain.ChainElements
            .Cast<X509ChainElement>()
            .Any(x => x.Certificate.Thumbprint == authority.Thumbprint);
    }

    #region Encrypt / Decrypy
    /// <summary>
    ///     X509 Certificate Encryption. (Only-RSA).
    /// </summary>
    /// <param name="cert"></param>
    public static RSA GetEncrypter(this X509Certificate2 cert) {
        var rsaKey = cert.GetRSAPublicKey();
        if (rsaKey == null) throw new ArgumentException("Non-RSA certificates can not encrypt data");
        return rsaKey;
    }
    /// <summary>
    ///     X509 Certificate Encryption. (Only-RSA).
    /// </summary>
    /// <param name="cert"></param>
    public static RSA GetDecrypter(this X509Certificate2 cert) {
        var rsaKey = cert.GetRSAPrivateKey();
        if (rsaKey == null) throw new ArgumentException("Non-RSA certificates can not encrypt data");
        return rsaKey;
    }

    #endregion
}