﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
using Z.Cryptography;

namespace Z.Extensions;

/// <summary>Extension methods for AesGnc</summary>
public static class ExtAesGcm
{
    #region Encrypt
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static long Encrypt(this AesGcm aesGcm, ReadOnlySpan<byte> data, Stream destination, Span<byte> tag = default, ReadOnlySpan<byte> nonce = default, ReadOnlySpan<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) {
        var len = data.Length;
        var cipherBytes = new byte[len];
        if (tag.IsEmpty) tag = new byte[16];
        nonce = Encrypter.DeriveBytes(nonce, 12);

        aesGcm.Encrypt(nonce, data, cipherBytes, tag, associatedData);
        var initialPos = destination.Position;
        if (prepend.HasFlag(CipherPrepend.IV))
            destination.Write(nonce);
        destination.Write(tag);

        // destination.Writez(associatedData.Length);
        // if (associatedData != null && associatedData.Length > 0) {
        //     destination.Write(associatedData);
        // };
        destination.Write(cipherBytes);
        return destination.Position - initialPos;
    }
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static byte[] Encrypt(this AesGcm aesGcm, ReadOnlySpan<byte> data, Span<byte> tag = default, ReadOnlySpan<byte> nonce = default, ReadOnlySpan<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) {
        using var destination = ZUtilCfg.GlobalMemoryStreamManager.GetStream();
        Encrypt(aesGcm, data, destination, tag, nonce, associatedData, prepend);
        return destination.ToArray();
    }
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static long Encrypt(this AesGcm aesGcm, Stream source, long len, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV)
        => source.CopyTo(destination, (b, d) => Encrypt(aesGcm, b.Span, d, default, nonce.Span, associatedData.Span, prepend), len, bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize);
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static long Encrypt(this AesGcm aesGcm, Stream source, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) 
        => Encrypt(aesGcm, source, -1, destination, bufferSize, nonce, associatedData, prepend);

    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static async Task<long> EncryptAsync(this AesGcm aesGcm, ReadOnlyMemory<byte> data, Stream destination, Memory<byte> tag = default, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) {
        var len = data.Length;
        var cipherBytes = new byte[len];
        if (tag.IsEmpty) tag = new byte[16];
        nonce = Encrypter.DeriveBytes(nonce.Span, 12);

        aesGcm.Encrypt(nonce.Span, data.Span, cipherBytes, tag.Span, associatedData.Span);
        var initialPos = destination.Position;
        if (prepend.HasFlag(CipherPrepend.IV))
            await destination.WriteAsync(nonce);
        await destination.WriteAsync(tag);

        // destination.Writez(associatedData.Length);
        // if (associatedData != null && associatedData.Length > 0) {
        //     destination.Write(associatedData);
        // };
        await destination.WriteAsync(cipherBytes);
        return destination.Position - initialPos;
    }
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static async Task<byte[]> EncryptAsync(this AesGcm aesGcm, ReadOnlyMemory<byte> data, Memory<byte> tag, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) {
        using var destination = ZUtilCfg.GlobalMemoryStreamManager.GetStream();
        await EncryptAsync(aesGcm, data, destination, tag, nonce, associatedData, prepend);
        return destination.ToArray();
    }
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static async Task<long> EncryptAsync(this AesGcm aesGcm, Stream source, long len, Stream destination, int? bufferSize, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV)
        => await source.CopyToAsync(destination, async (b, d) => await EncryptAsync(aesGcm, b, d, default, nonce, associatedData, prepend), len, bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize);
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static Task<long> EncryptAsync(this AesGcm aesGcm, Stream source, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) 
        => EncryptAsync(aesGcm, source, -1, destination, bufferSize, nonce, associatedData, prepend);
    #endregion

    #region Decrypt
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static long Decrypt(this AesGcm aesGcm, ReadOnlySpan<byte> cipherBytes, Stream destination, ReadOnlySpan<byte> tag = default, ReadOnlySpan<byte> nonce = default, ReadOnlySpan<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) {
        var b = Decrypt(aesGcm, cipherBytes, tag, nonce, associatedData, prepend);
        destination.Write(b);
        return b.Length;
    }
    
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static ReadOnlySpan<byte> Decrypt(this AesGcm aesGcm, ReadOnlySpan<byte> cipherBytes, ReadOnlySpan<byte> tag = default, ReadOnlySpan<byte> nonce = default, ReadOnlySpan<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) {
        int read = 0;
        if (prepend.HasFlag(CipherPrepend.IV)) {
            nonce = cipherBytes.Slice(read, 12);
            read += nonce.Length;
        } else if (nonce.Length != 12) nonce = Encrypter.DeriveBytes(nonce, 12);
        if (tag.Length != 16) {
            tag = cipherBytes.Slice(read, 16);
            read += tag.Length;
        }
        // cipherBytes.Readz(associatedData.Length);
        // if (associatedData != null && associatedData.Length > 0) {
        //     cipherBytes.CopyTo(associatedData);
        // };
        var rval = new byte[cipherBytes.Length - read];
        aesGcm.Decrypt(nonce, cipherBytes[read..], tag, rval, associatedData);
        return rval;
    }
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static long Decrypt(this AesGcm aesGcm, Stream source, long len, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV)
        => source.CopyTo(destination, (b, d) => Decrypt(aesGcm, b.Span, d, default, nonce.Span, associatedData.Span, prepend), len, (bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize) + (prepend.HasFlag(CipherPrepend.IV) ? 12 : 0) + 16);
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static long Decrypt(this AesGcm aesGcm, Stream source, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV)
        => Decrypt(aesGcm, source, -1, destination, bufferSize, nonce, associatedData, prepend);


    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static Task<byte[]> DecryptAsync(this AesGcm aesGcm, ReadOnlyMemory<byte> cipherBytes, ReadOnlyMemory<byte> tag = default, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV)
        => Task.FromResult(Decrypt(aesGcm, cipherBytes.Span, tag.Span, nonce.Span, associatedData.Span, prepend).ToArray());
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static async Task<long> DecryptAsync(this AesGcm aesGcm, ReadOnlyMemory<byte> cipherBytes, Stream destination, ReadOnlyMemory<byte> tag = default, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV) {
        var b = await DecryptAsync(aesGcm, cipherBytes, tag, nonce, associatedData, prepend);
        await destination.WriteAsync(b);
        return b.Length;
    }
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static async Task<long> DecryptAsync(this AesGcm aesGcm, Stream source, long len, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV)
        => await source.CopyToAsync(destination, async (b, d) => await DecryptAsync(aesGcm, b, d, default, nonce, associatedData, prepend), len, (bufferSize ?? ZUtilCfg.EncrypterStreamBufferSize) + (prepend.HasFlag(CipherPrepend.IV) ? 12 : 0) + 16 );
    /// <summary>.NET Native AESGCM Encryption.(Key: 256, Block: 128, nonce:96+(default 96), tag:128+ )</summary>
    public static Task<long> DecryptAsync(this AesGcm aesGcm, Stream source, Stream destination, int? bufferSize = null, ReadOnlyMemory<byte> nonce = default, ReadOnlyMemory<byte> associatedData = default, CipherPrepend prepend = CipherPrepend.IV)
        => DecryptAsync(aesGcm, source, -1, destination, bufferSize, nonce, associatedData, prepend);
    #endregion
}