﻿/* Copyright (c) <2024> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Z.Extensions;

/// <summary>Extension methods for the PropertyInfo class</summary>
public static class ExtPropertyInfo {
    /// <summary>Gets a lambda expression for the specified property</summary>
    public static Expression<Func<T, TProp>> GetLambda<T, TProp>(this PropertyInfo prop) {
        var parameter = Expression.Parameter(typeof(T));
        var property = Expression.Property(parameter, prop);
        var conversion = Expression.Convert(property, typeof(TProp));
        var lambda = Expression.Lambda<Func<T, TProp>>(conversion, parameter);
        return lambda;
    }
    /// <summary>Checks if the property is nullable</summary>
    public static bool IsNullable(this PropertyInfo property) {
        var nullContext = new NullabilityInfoContext();
        return nullContext.Create(property).WriteState == NullabilityState.Nullable;
    }
}