﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Xml;
using System.Xml.Serialization;
using Z.Serialization;
using Z.Util;

namespace Z.Extensions;

#region CloneMethods
/// <summary>Methods to be used for object cloning</summary>
public enum CloneMethods {
    /// <summary>Shallow cloning via Reflection</summary>
    ReflectionShallow,
    /// <summary>Deep cloning via Reflection</summary>
    ReflectionDeep,
    /// <summary>Deep cloning via Serialization</summary>
    SerializationDeep,
    /// <summary>Deep cloning via IL Assembly</summary>
    ILDeep
}
#endregion

/// <summary>Extension methods for all objects...</summary>
public static class ExtObject {
    /// <summary>Serialize Object To Various formats.</summary>
    /// <param name="value">The object to be Serialized.</param>
    /// <returns>An universal serialize helper supplying additional serialization methods</returns>
    public static SerializePortion<T> Serialize<T>(this T value) => new(value);

    /// <summary>Converts the specified object to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static ConvertPortion<object> To(this object value) => new(value);

    /// <summary>Returns value of the object or specified value.</summary>
    public static T? Coalesce<T>(this T obj, T? defaultValue) where T : class => obj ?? defaultValue;
    /// <summary>Returns value of the object or specified value.</summary>
    public static T? Coalesce<T>(this T obj, Func<T?, T> defaultValue) where T : class => obj ?? defaultValue(obj);

    #region Nested type: ConvertPortion
    #region Convert
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class ConvertPortion<T>: ExtendablePortion<T>, IConvertPortion<T> {
        #region Constructors
        /// <summary>Constructor</summary>
        public ConvertPortion(T value) : base(value) { }
        #endregion

        /// <summary>Converts the specified value to a different type.</summary>
        /// <typeparam name="U">Type to convert to</typeparam>
        /// <param name="defaultValue">Default value to set if conversion fails</param>
        /// <param name="format">Formatting to use (If applicable)</param>
        /// <returns>An universal converter supplying additional target conversion methods</returns>
        /// <example>
        ///     <code>
        /// 		var value = "123";
        /// 		var numeric = value.Convert(int)();
        /// 	</code>
        /// </example>
        public virtual U? Type<U>(U? defaultValue = default, IFormatProvider? format = null) {
            try {
                return (U?)Type(typeof(U?), defaultValue, format);
            } catch {
                return defaultValue;
            }
        }
        /// <summary>Converts the specified value to a different type.</summary>
        /// <param name="type">Type to convert to</param>
        /// <param name="defaultValue">Default value to set if conversion fails</param>
        /// <param name="format">Formatting to use (If applicable)</param>
        /// <returns>An universal converter supplying additional target conversion methods</returns>
        /// <example>
        ///     <code>
        /// 		var value = "123";
        /// 		var numeric = value.Convert(int)();
        /// 	</code>
        /// </example>
        public virtual object? Type(Type type, object? defaultValue = null, IFormatProvider? format = null) => ZConverter.To(Value, type, defaultValue, format);

        /// <summary>Creates an IEnumerator that enumerates a given item <paramref name="repeatCount" /> times.</summary>
        /// <example>
        ///     The following creates a list consisting of 1000 copies of the double 1.0.
        ///     <code>
        /// List&lt;double&gt; list = new List&lt;double&gt;(Algorithms.NCopiesOf(1000, 1.0));
        /// </code>
        /// </example>
        /// <param name="repeatCount">The number of times to enumerate the item.</param>
        /// <returns>An IEnumerable&lt;T&gt; that yields <paramref name="repeatCount" /> copies of item.</returns>
        /// <exception cref="ArgumentOutOfRangeException">The argument <paramref name="repeatCount" /> is less than zero.</exception>
        public IEnumerable<T> Enumerable(int repeatCount) {
            if (repeatCount <= 0) throw new ArgumentOutOfRangeException(nameof(repeatCount), repeatCount, "Argument must not be negative");

            return Enumerable2();

            IEnumerable<T> Enumerable2() {
                while (repeatCount-- > 0) yield return Value;
            }
        }
    }
    #endregion Convert
    #endregion

    #region Nested type: SerializePortion
    #region Serialize
    /// <summary>Multi-method library for serialization.</summary>
    public class SerializePortion<T>: ExtendablePortion<T> {
        #region Constructors
        /// <summary>Constructor</summary>
        public SerializePortion(T value) : base(value) { }
        #endregion

        #region ZXml
        /// <summary>Custom Serialize as XML</summary>
        /// <param name="info">Other serialization information (ex. which properties to skip etc.)</param>
        public XmlDocument ZXml(ZSerializationInfo? info = null) => Fnc.SerializeXml2(Value, info);
        #endregion ZXml

        #region BoisBinary
        /// <summary>Serialize an object into byte[] using .NET Binary Serializer.</summary>
        public byte[] BoisBinary(ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            byte[] rval;
            using (var ms = new MemoryStream()) {
                BoisBinary(ms, opts);
                rval = ms.ToArray();
            }
            return rval;
        }
        /// <summary>Serialize an object into a stream using .NET Binary Serializer.</summary>
        public Stream BoisBinary(Stream s, ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            if (Value == null) return s;
            //if (!Value.GetType().IsSerializable) throw new ArgumentException("Type Is Not Serializable: {0}".ToFormat(Value.GetType().Name));
            var bs = new ZBoisSerializer(opts);
            bs.Serialize(Value, typeof(T) ?? Value.GetType(), s);
            return s;
        }
        /// <summary>Serialize an object into a file using .NET Binary Serializer.</summary>
        public void BoisBinary(string file, ZTypeCacheFlags opts = ZTypeCacheFlags.Default) {
            using var bs = new FileStream(file, FileMode.OpenOrCreate);
            BoisBinary(bs, opts);
        }
        #endregion BoisBinary

        #region DotNetJson
        /// <summary>Serialize an object into string using .NET Json Serializer.</summary>
        public string DotNetJson(bool includeFields = false) {
            if (Value == null) throw new NullReferenceException("Value is null");
            //if (!Value.GetType().IsSerializable) throw new ArgumentException($"Type Is Not Serializable: {Value.GetType().Name}");
            return JsonSerializer.Serialize(Value, new JsonSerializerOptions { IncludeFields = includeFields });
        }
        /// <summary>Serialize an object into a stream using .NET Json Serializer.</summary>
        public Stream DotNetJson(Stream s, bool includeFields = false) {
            if (Value == null) return s;
            //if (!Value.GetType().IsSerializable) throw new ArgumentException($"Type Is Not Serializable: {Value.GetType().Name}");
            JsonSerializer.Serialize(s, Value, new JsonSerializerOptions { IncludeFields = includeFields });
            return s;
        }
        /// <summary>Serialize an object into a file using .NET Json Serializer.</summary>
        public void DotNetJson(string file, bool includeFields = false) {
            using var bs = new FileStream(file, FileMode.OpenOrCreate);
            DotNetJson(bs, includeFields);
        }
        #endregion DotNetJson

        #region DotNetBinary
        /// <summary>Serialize an object into byte[] using .NET Binary Serializer.</summary>
        public byte[] DotNetBinary() {
            byte[] rval;
            using (var bs = new MemoryStream()) {
                DotNetBinary(bs);
                rval = bs.ToArray();
            }
            return rval;
        }
#pragma warning disable SYSLIB0011
        /// <summary>Serialize an object into a stream using .NET Binary Serializer.</summary>
        public Stream DotNetBinary(Stream s) {
            if (Value == null) return s;
            //if (!Value.GetType().IsSerializable) throw new ArgumentException($"Type Is Not Serializable: {Value.GetType().Name}");
            var bf = new BinaryFormatter();
            bf.Serialize(s, Value);
            return s;
        }
#pragma warning restore
        /// <summary>Serialize an object into a file using .NET Binary Serializer.</summary>
        public void DotNetBinary(string file) {
            using var bs = new FileStream(file, FileMode.OpenOrCreate);
            DotNetBinary(bs);
        }
        #endregion DotNetBinary

        #region DotNetXml
        /// <summary>Serialize an object into string using .NET Xml Serializer.</summary>
        public string DotNetXml() {
            if (Value == null) throw new NullReferenceException("Value is null");
            using var m = ZUtilCfg.GlobalMemoryStreamManager.GetStream("DotnetXmlSerializer");
            DotNetXml(m);
            return m.ToArray().To().String.AsUtf8;
        }
        /// <summary>Serialize an object into a stream using .NET Xml Serializer.</summary>
        public Stream DotNetXml(Stream s) {
            if (Value == null) return s;
            //if (!Value.GetType().IsSerializable) throw new ArgumentException(typeNotSerializable(Value.GetType()));

            var serializer = new XmlSerializer(Value.GetType());
            serializer.Serialize(s, Value);
            return s;
        }
        /// <summary>Serialize an object into a file using .NET Xml Serializer.</summary>
        public void DotNetXml(string file) {
            using var s = new FileStream(file, FileMode.OpenOrCreate);
            DotNetXml(s);
        }
        #endregion DotNetXml
    }
    #endregion Serialize
    #endregion

    #region Disabled Extensions
    ///// <summary>
    ///// 	Converts the specified value to a different type.
    ///// </summary>
    ///// <typeparam name = "T"></typeparam>
    ///// <param name = "value">The value.</param>
    ///// <param name = "defaultValue">Default value to set if conversion fails</param>
    ///// <param name = "ignoreException">Raise or skip exceptions</param>
    ///// <returns>An universal converter suppliying additional target conversion methods</returns>
    ///// <example>
    ///// 	<code>
    ///// 		var value = "123";
    ///// 		var numeric = value.ConvertTo(0);
    ///// 	</code>
    ///// </example>
    //public static T ConvertTo<T>(this object value, T defaultValue = default(T), bool ignoreException = true)
    //{
    //    if (!ignoreException) return value.convertTo<T>(defaultValue);
    //    try { return value.convertTo<T>(defaultValue); } catch { return defaultValue; }
    //}
    //private static T convertTo<T>(this object value, T defaultValue)
    //{
    //    var rval = defaultValue;
    //    if (value != null) {
    //        var targetType = typeof(T);

    //        if (value.GetType() == targetType) return (T) value;

    //        var converter = TypeDescriptor.GetConverter(value);
    //        if (converter != null) {
    //            if (converter.CanConvertTo(targetType)) rval = (T) converter.ConvertTo(value, targetType);
    //        } else {
    //            converter = TypeDescriptor.GetConverter(targetType);
    //            if (converter != null) {
    //                if (converter.CanConvertFrom(value.GetType())) rval = (T) converter.ConvertFrom(value);
    //            }
    //        }
    //    }
    //    return rval;
    //}

    //public static void DisposeSafe(this IDisposable disposable)
    //{
    //try { disposable.Dispose(); }
    //catch (Exception) { }
    //}
    #endregion Disabled Extensions

    #region Reflection
    /// <summary>Gets all matching attribute defined on the data type.</summary>
    /// <typeparam name="T">The attribute type to look for.</typeparam>
    /// <param name="obj">The object to look on.</param>
    /// <param name="includeInherited">if set to <c>true</c> includes inherited attributes.</param>
    /// <returns>The found attributes</returns>
    public static IEnumerable<T> GetAttributes<T>(this object obj, bool includeInherited = true) where T : Attribute =>
        (obj as Type ?? obj.GetType()).GetTypeInfo().GetCustomAttributes(typeof(T), includeInherited).OfType<T>().Select(attribute => attribute);

    #region Disabled Extensions
    /*
    /// <summary>
    /// Finds a type instance using a recursive call. The method is useful to find specific parents for example.
    /// </summary>
    /// <typeparam name="T">The source type to perform on.</typeparam>
    /// <typeparam name="K">The targte type to be returned</typeparam>
    /// <param name="item">The item to start performing on.</param>
    /// <param name="function">The function to be executed.</param>
    /// <returns>An target type instance or null.</returns>
    /// <example><code>
    /// var tree = ...
    /// var node = tree.FindNodeByValue("");
    /// var parentByType = node.FindTypeByRecursion%lt;TheType&gt;(n => n.Parent);
    /// </code></example>
    public static K FindTypeByRecursion<T, K>(this T item, Func<T, T> function)
        where T : class
        where K : class, T
    {
        do
        {
            if (item is K) return (K)item;
        }
        while ((item = function(item)) != null);
        return null;
    }
    /// <summary>
    /// 	Determines whether the object is excactly of the passed generic type.
    /// </summary>
    /// <typeparam name = "T">The target type.</typeparam>
    /// <param name = "obj">The object to check.</param>
    /// <param name = "type">The target type.</param>
    /// <returns>
    /// 	<c>true</c> if the object is of the specified type; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsOfType<T>(this object obj, Type type = null)
    {
        return type == null ? typeof(T).Equals(type) : obj.GetType().Equals(type);
    }

    /// <summary>
    /// 	Determines whether the object is of the passed generic type or inherits from it.
    /// </summary>
    /// <typeparam name = "T">The target type.</typeparam>
    /// <param name = "obj">The object to check.</param>
    /// <param name = "type">The target type.</param>
    /// <returns>
    /// 	<c>true</c> if the object is of the specified type; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsOfTypeOrInherits<T>(this object obj, Type type = null)
    {
        type = type ?? typeof(T);
        var objectType = obj.GetType();
        do
        {
            if (objectType.Equals(type))
                return true;
            if ((objectType == objectType.BaseType) || (objectType.BaseType == null))
                return false;
            objectType = objectType.BaseType;
        } while (true);
    }

    /// <summary>
    /// 	Cast an object to the given type. Usefull especially for anonymous types.
    /// </summary>
    /// <param name="obj">The object to be cast</param>
    /// <param name="targetType">The type to cast to</param>
    /// <returns>
    /// 	the casted type or null if casting is not possible.
    /// </returns>
    /// <remarks>
    /// 	Contributed by Michael T, http://about.me/MichaelTran
    /// </remarks>
    public static object DynamicCast(this object obj, Type targetType)
    {
        // First, it might be just a simple situation
        if (targetType.IsAssignableFrom(obj.GetType()))
            return obj;

        // If not, we need to find a cast operator. The operator
        // may be explicit or implicit and may be included in
        // either of the two types...
        const BindingFlags pubStatBinding = BindingFlags.Public | BindingFlags.Static;
        var originType = obj.GetType();
        String[] names = { "op_Implicit", "op_Explicit" };

        var castMethod =
                targetType.GetMethods(pubStatBinding).Union(originType.GetMethods(pubStatBinding)).FirstOrDefault(
                        itm => itm.ReturnType.Equals(targetType) && itm.GetParameters().Length == 1 && itm.GetParameters()[0].ParameterType.IsAssignableFrom(originType) && names.Contains(itm.Name));
        if (null != castMethod)
            return castMethod.Invoke(null, new[] { obj });
        throw new InvalidOperationException(
                String.Format(
                        "No matching cast operator found from {0} to {1}.",
                        originType.Name,
                        targetType.Name));
    }

    /// <summary>
    /// 	Cast an object to the given type. Usefull especially for anonymous types.
    /// </summary>
    /// <typeparam name = "T">The type to cast to</typeparam>
    /// <param name = "value">The object to case</param>
    /// <returns>
    /// 	the casted type or null if casting is not possible.
    /// </returns>
    /// <remarks>
    /// 	Contributed by blaumeister, http://www.codeplex.com/site/users/view/blaumeiser
    /// </remarks>
    public static T CastTo<T>(this object value)
    {
        if (value == null || !(value is T))
            return default(T);

        return (T)value;
    }
    */
    #endregion Disabled Extensions

#pragma warning disable SYSLIB0011
    /// <summary>Perform a deep or shallow Copy of the object.</summary>
    /// <typeparam name="T">The type of object being copied.</typeparam>
    /// <param name="source">The object instance to copy.</param>
    /// <param name="method">Method to be used for copying.</param>
    /// <returns>The copied object.</returns>
    public static T Clone<T>(this T source, CloneMethods method = CloneMethods.ReflectionShallow) {
        T? rval = default;
        switch (method) {
            //case CloneMethods.ILDeep:
            //    throw new NotSupportedException();

            case CloneMethods.SerializationDeep:
                //if (!typeof(T).GetTypeInfo().IsSerializable) throw new ArgumentException("The type must be serializable.", nameof(source));

                // Don't serialize a null object, simply return the default for that object
                if (source == null) throw new ArgumentNullException(nameof(source));

                var formatter = new BinaryFormatter();
                Stream stream = new MemoryStream();
                using (stream) {
                    formatter.Serialize(stream, source);
                    stream.Seek(0, SeekOrigin.Begin);
                    rval = (T)formatter.Deserialize(stream);
                }
                break;
            case CloneMethods.ReflectionDeep:
                rval = DeepCopyByExpressionTrees.DeepClone(source);
                //rval = Force.DeepCloner.Helpers.DeepClonerGenerator.CloneObject(source);
                //rval = Activator.CreateInstance<T>();
                //rval = source.Populate(rval, PopulateOptions.DeepCopy);
                break;

            default:
                rval = DeepCopyByExpressionTrees.ShallowClone(source);
                //rval = Force.DeepCloner.Helpers.ShallowClonerGenerator.CloneObject(source);
                //rval = Activator.CreateInstance<T>();
                //rval = source.Populate(rval);
                break;
        }
        return rval;
    }
#pragma warning restore
    ///// <summary>Push all property and field values to another object</summary>
    ///// <typeparam name="TInput"></typeparam>
    ///// <typeparam name="TTarget">Object to push all data into (new instance is created)</typeparam>
    ///// <param name="deepClone">Deep or Shallow Clone</param>
    ///// <param name="source">Object containing values</param>
    //public static TTarget Clone<TInput, TTarget>(this TInput source, bool deepClone = false) => DeepCopyByExpressionTrees.CloneTo<TInput, TTarget>(source, deepClone);

    ///// <summary>Push all property and field values to another object</summary>
    ///// <param name="target">Object to push all data into (if left null, new instance is created)</param>
    ///// <param name="deepClone">Deep or Shallow Clone</param>
    ///// <param name="source">Object containing values</param>
    //public static TTarget Clone<TInput, TTarget>(this TInput source, TTarget target, bool deepClone = false)
    ////where T : class
    ////where TTarget : class
    //{
    //    var copy = Clone<TInput, TTarget>(source, deepClone);
    //    if (copy == null) return default(TTarget);
    //    foreach (var prop in typeof(TTarget).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(prop => prop.CanWrite && prop.CanRead)) {
    //        prop.SetValue(target, prop.GetValue(copy, null), null);
    //    }
    //    return target;
    //}

    /// <summary>Push all property and field values to another object (Slower version with more control)</summary>
    /// <param name="source">Object containing values</param>
    /// <param name="target">Object to push all data into (if left null, new instance is created)</param>
    /// <param name="options">Populate Options</param>
    /// <param name="skipProps">Skip specified properties</param>
    public static TTarget Populate<TSource, TTarget>(this TSource source, TTarget? target = default, PopulateOptions options = PopulateOptions.None, IEnumerable<string>? skipProps = null)
    //where T : class
    //where TTarget : class
    {
        var skipFields = (options & PopulateOptions.SkipFields) == PopulateOptions.SkipFields;
        var clone = (options & PopulateOptions.DeepCopy) == PopulateOptions.DeepCopy;

        if (target == null) target = Activator.CreateInstance<TTarget>();
        if (source == null || target == null) return target;
        //if (skipProps == null) skipProps = new string[] { /*"DateAdded", "DateModified", "Id", "Parent", "Children", "Nodes"*/};

        var sourceInfo = (ZTypeInfo)ZTypeCache.GetTypeInfo<TSource>();
        var targetInfo = (ZTypeInfo)ZTypeCache.GetTypeInfo<TTarget>();

        //IEnumerable<MemberInfo> members = skipFields ? typeof(TTarget).GetTypeInfo().GetProperties() : typeof(TTarget).GetTypeInfo().GetMembers();
        foreach (var member in targetInfo.Members.Values) {
            if (skipProps?.Contains(member.Member.Name) == true) continue;
            if (skipFields && !member.IsProperty) continue;

            if (!sourceInfo.Members.TryGetValue(member.Member.Name, out var srcMember)) continue; //typeof(TSource).GetTypeInfo().GetMember(member.Name).FirstOrDefault();
            if (srcMember == null || (skipFields && !srcMember.IsProperty)) continue;

            var sourceType = srcMember.Member.GetMemberType(); //member.GetMemberType();

            if (sourceType == null) continue;
            if (member.Member.GetMemberType()?.GetTypeInfo().IsAssignableFrom(sourceType) == false) continue;

            try {
                //var originalValue = pi.GetValue(entity, null);
                var sourceValue = srcMember.Getter(source); //.GetValue(source);
                if (NeedRecursion(sourceType, target) && clone) sourceValue = sourceValue.Populate(Activator.CreateInstance(sourceType), options);
                //if (!originalValue.Equals(updateValue))
                member.Setter(target, sourceValue); //member.SetValue(target, sourceValue);
            } catch { }
        }
        return target;
    }

    /// <summary>Populate Options</summary>
    [Flags]
    public enum PopulateOptions {
        /// <summary>None</summary>
        None = 0,
        /// <summary>Perform deepcopy of referenced objects (Clone them)</summary>
        DeepCopy = 0x1,
        /// <summary>Copy only properties</summary>
        SkipFields = 0x2
    }

    private static bool NeedRecursion(Type type, object o) =>
        o != null && !type.GetTypeInfo().IsPrimitive && !(o is string || o is DateTime || o is DateTimeOffset || o is TimeSpan || o is Delegate || o is Enum || o is decimal || o is Guid);

    /// <summary>Detaches event from the invocation list using reflection</summary>
    public static void DetachEvent(this object obj, string eventName) {
        if (obj == null) return;

        var t = obj.GetType().GetTypeInfo();
        const BindingFlags AllBindings = BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

        // Type.GetEvent(s) gets all Events for the type AND it's ancestors
        // Type.GetField(s) gets only Fields for the exact type.
        //  (BindingFlags.FlattenHierarchy only works on PROTECTED & PUBLIC
        //   doesn't work because Fieds are PRIVATE)

        // NEW version of this routine uses .GetEvents and then uses .DeclaringType
        // to get the correct ancestor type so that we can get the FieldInfo.
        var ei = t.GetEvent(eventName, AllBindings);
        var dt = ei?.DeclaringType;
        var fi = ei != null ? dt?.GetTypeInfo().GetField(ei.Name, AllBindings) : null;
        if (fi == null) return;

        // After hours and hours of research and trial and error, it turns out that
        // STATIC Events have to be treated differently from INSTANCE Events...
        if (fi.IsStatic) {
            // STATIC EVENT
            var mi = t.GetMethod("get_Events", AllBindings);
            var static_event_handlers = (EventHandlerList)(mi?.Invoke(obj, Array.Empty<object>()) ?? new EventHandlerList());

            var idx = fi.GetValue(obj);
            var eh = idx != null ? static_event_handlers[idx] : null;
            if (eh == null) return;

            var dels = eh.GetInvocationList();
            if (dels == null) return;

            //EventInfo ei = t.GetEvent(fi.Name, AllBindings);
            if (ei != null) {
                foreach (var del in dels) ei.RemoveEventHandler(obj, del);
            }
        } else {
            // INSTANCE EVENT
            //EventInfo ei = t.GetEvent(fi.Name, AllBindings);
            var val = fi.GetValue(obj);
            if (val is Delegate mdel && ei != null) {
                foreach (var del in mdel.GetInvocationList())
                    ei.RemoveEventHandler(obj, del);
            }
        }
    }

    #region ClassCopy (Old Disabled)
    /*
    /// <summary>
    /// Perform a deep or shallow of the current object into target object.
    /// </summary>
    /// <typeparam name="T">The type of object being copied.</typeparam>
    /// <param name="source">The object instance to gather values to copy.</param>
    /// <param name="target">The object instance to copy values into.</param>
    /// <param name="deepCopy">Method to be used for copying.</param>
    /// <returns>Copied target object.</returns>
    public static T ClassCopy<T>(this T source, T target, bool deepCopy = false)
    {
        Type type = typeof(T);
        while (type != null)
        {
            updateForType<T>(source, target, deepCopy);
            type = type.BaseType;
        }
        return target;
    }
    private static void updateForType<T>(T source, T destination, bool deepCopy = false)
    {
        var cloneMethod = deepCopy ? CloneMethods.ReflectionDeep : CloneMethods.ReflectionShallow;
        Type type = typeof(T);
        PropertyInfo[] myObjectProps = type.GetProperties(
        BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

        foreach (PropertyInfo pi in myObjectProps)
        {
            if (!pi.CanWrite) continue;
            var o = pi.GetValue(source, null);
            if (deepCopy && NeedRecursion(pi.PropertyType, o)) o = o.Clone(cloneMethod);
            pi.SetValue(destination, o, null);
        }

        FieldInfo[] myObjectFields = type.GetFields(
            BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

        foreach (FieldInfo fi in myObjectFields)
        {
            var o = fi.GetValue(source);
            if (deepCopy && NeedRecursion(fi.GetType(), o)) o = o.Clone(cloneMethod);
            fi.SetValue(destination, o);
        }
    }*/
    #endregion

    #region Reflection based cloning example (disabled)
    /*
    /// <summary>
    /// Clone the object, and returning a reference to a cloned object.
    /// </summary>
    /// <returns>Reference to the new cloned
    /// object.</returns>
    public object Clone()
    {
        //First we create an instance of this specific type.
        object newObject = Activator.CreateInstance(this.GetType());

        //We get the array of fields for the new type instance.
        FieldInfo[] fields = newObject.GetType().GetFields();

        int i = 0;

        foreach (FieldInfo fi in this.GetType().GetFields()) {
            //We query if the fiels support the ICloneable interface.
            Type ICloneType = fi.FieldType.
                        GetInterface("ICloneable", true);

            if (ICloneType != null) {
                //Getting the ICloneable interface from the object.
                ICloneable IClone = (ICloneable) fi.GetValue(this);

                //We use the clone method to set the new value to the field.
                fields[i].SetValue(newObject, IClone.Clone());
            } else {
                // If the field doesn't support the ICloneable
                // interface then just set it.
                fields[i].SetValue(newObject, fi.GetValue(this));
            }

            //Now we check if the object support the
            //IEnumerable interface, so if it does
            //we need to enumerate all its items and check if
            //they support the ICloneable interface.
            Type IEnumerableType = fi.FieldType.GetInterface
                            ("IEnumerable", true);
            if (IEnumerableType != null) {
                //Get the IEnumerable interface from the field.
                IEnumerable IEnum = (IEnumerable) fi.GetValue(this);

                //This version support the IList and the
                //IDictionary interfaces to iterate on collections.
                Type IListType = fields[i].FieldType.GetInterface
                                    ("IList", true);
                Type IDicType = fields[i].FieldType.GetInterface
                                    ("IDictionary", true);

                int j = 0;
                if (IListType != null) {
                    //Getting the IList interface.
                    IList list = (IList) fields[i].GetValue(newObject);

                    foreach (object obj in IEnum) {
                        //Checking to see if the current item
                        //support the ICloneable interface.
                        ICloneType = obj.GetType().
                            GetInterface("ICloneable", true);

                        if (ICloneType != null) {
                            //If it does support the ICloneable interface,
                            //we use it to set the clone of
                            //the object in the list.
                            ICloneable clone = (ICloneable) obj;

                            list[j] = clone.Clone();
                        }

                        //NOTE: If the item in the list is not
                        //support the ICloneable interface then in the
                        //cloned list this item will be the same
                        //item as in the original list
                        //(as long as this type is a reference type).

                        j++;
                    }
                } else if (IDicType != null) {
                    //Getting the dictionary interface.
                    IDictionary dic = (IDictionary) fields[i].
                                        GetValue(newObject);
                    j = 0;

                    foreach (DictionaryEntry de in IEnum) {
                        //Checking to see if the item
                        //support the ICloneable interface.
                        ICloneType = de.Value.GetType().
                            GetInterface("ICloneable", true);

                        if (ICloneType != null) {
                            ICloneable clone = (ICloneable) de.Value;

                            dic[de.Key] = clone.Clone();
                        }
                        j++;
                    }
                }
            }
            i++;
        }
        return newObject;
    } */
    #endregion Reflection based cloning example
    #endregion Reflection
}