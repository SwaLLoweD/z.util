﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Z.Extensions;

/// <summary>Extension methods for Type values...</summary>
public static class ExtType {
    /// <summary>Checks the specified Type for different conditions.</summary>
    /// <param name="value">The value to be checked.</param>
    /// <returns>An universal checker supplying additional target checking methods</returns>
    public static TypeIsPortion Is(this Type value) => new(value);

    /// <summary>Type.Equals overload</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="theType"></param>
    public static bool Equals<T>(this Type theType) => theType.Equals(typeof(T));

    /// <summary>Type.GetInterface strongly-typed overloads</summary>
    /// <typeparam name="T">Interface Type</typeparam>
    /// <param name="theType"></param>
    public static Type? GetInterface<T>(this TypeInfo theType) => theType.GetInterface(typeof(T));

    /// <summary>Type.GetInterface strongly-typed overloads</summary>
    /// <param name="theType"></param>
    /// <param name="intface">Interface Type</param>
    public static Type? GetInterface(this TypeInfo theType, Type intface) => theType.GetInterface(intface.GetTypeInfo().FullName ?? "");

    /// <summary>Find interface that closes an open type</summary>
    public static Type? FindInterfaceThatCloses(this TypeInfo type, Type openType) {
        if (type.IsInterface && type.IsGenericType && type.GetGenericTypeDefinition() == openType) return type.DeclaringType;

        foreach (var interfaceType in type.GetInterfaces()) {
            if (interfaceType.GetTypeInfo().IsGenericType && interfaceType.GetGenericTypeDefinition() == openType)
                return interfaceType;
        }

        if (!type.Is().Concrete) return null;

        return type.BaseType == typeof(object)
            ? null
            : type.GetTypeInfo().BaseType?.GetTypeInfo().FindInterfaceThatCloses(openType);
    }
    /// <summary>Find parameter type for</summary>
    public static Type? FindParameterTypeTo(this TypeInfo type, Type openType) {
        var interfaceType = type.FindInterfaceThatCloses(openType);
        return interfaceType?.GetGenericArguments().FirstOrDefault();
    }

    /// <summary>Displays type names using CSharp syntax style. Supports funky generic types.</summary>
    /// <param name="type">Type to be pretty printed</param>
    /// <returns></returns>
    public static string PrettyPrint(this Type type) => type.PrettyPrint(t => t.Name);

    /// <summary>Displays type names using CSharp syntax style. Supports funky generic types.</summary>
    /// <param name="type">Type to be pretty printed</param>
    /// <param name="selector">
    ///     Function determining the name of the type to be displayed. Useful if you want a fully qualified
    ///     name.
    /// </param>
    /// <returns></returns>
    public static string PrettyPrint(this Type type, Func<Type, string> selector) {
        var typeName = selector(type) ?? string.Empty;
        if (!type.IsGenericType) return typeName;

        var genericParamSelector = type.IsGenericTypeDefinition ? t => t.Name : selector;
        var genericTypeList = string.Join(",", type.GetGenericArguments().Select(genericParamSelector).ToArray());
        var tickLocation = typeName.IndexOf('`');
        if (tickLocation >= 0) typeName = typeName[..tickLocation];
        return string.Format("{0}<{1}>", typeName, genericTypeList);
    }
    /*
    public static T CloseAndBuildAs<T>(this Type openType, params Type[] parameterTypes)
    {
        var closedType = openType.MakeGenericType(parameterTypes);
        return (T)Activator.CreateInstance(closedType);
    }

    public static T CloseAndBuildAs<T>(this Type openType, object ctorArgument, params Type[] parameterTypes)
    {
        var closedType = openType.MakeGenericType(parameterTypes);
        return (T)Activator.CreateInstance(closedType, ctorArgument);
    }

    public static bool PropertyMatches(this PropertyInfo prop1, PropertyInfo prop2)
    {
        return prop1.DeclaringType == prop2.DeclaringType && prop1.Name == prop2.Name;
    }*/
    /// <summary>Creates and returns an instance of the desired type</summary>
    /// <param name="type">The type to be instanciated.</param>
    /// <param name="constructorParameters">Optional constructor parameters</param>
    /// <returns>The instanciated object</returns>
    /// <example>
    ///     <code>
    /// 		var type = Type.GetType(".NET full qualified class Type")
    /// 		var instance = type.CreateInstance();
    /// 	</code>
    /// </example>
    public static object? CreateInstance(this Type type, params object[] constructorParameters) => CreateInstance<object>(type, constructorParameters);

    /// <summary>Creates and returns an instance of the desired type casted to the generic parameter type T</summary>
    /// <typeparam name="T">The data type the instance is casted to.</typeparam>
    /// <param name="type">The type to be instanciated.</param>
    /// <param name="constructorParameters">Optional constructor parameters</param>
    /// <returns>The instanciated object</returns>
    /// <example>
    ///     <code>
    /// 		var type = Type.GetType(".NET full qualified class Type")
    /// 		var instance = type.CreateInstance&lt;IDataType&gt;();
    /// 	</code>
    /// </example>
    public static T? CreateInstance<T>(this Type type, params object[] constructorParameters) {
        return (T?)Activator.CreateInstance(type, constructorParameters);
    }
    /// <summary>
    ///     Closes the passed generic type with the provided type arguments and returns an instance of the newly
    ///     constructed type.
    /// </summary>
    /// <typeparam name="T">The typed type to be returned.</typeparam>
    /// <param name="genericType">The open generic type.</param>
    /// <param name="typeArguments">The type arguments to close the generic type.</param>
    /// <returns>An instance of the constructed type casted to T.</returns>
    public static T? CreateGenericTypeInstance<T>(this Type genericType, params Type[] typeArguments) where T : class {
        var constructedType = genericType.MakeGenericType(typeArguments);
        var instance = Activator.CreateInstance(constructedType);
        return instance as T;
    }
    /// <summary>Non-generic default T</summary>
    /// <param name="t">Type</param>
    /// <returns>Default value of the Type</returns>
    public static object? GetDefault(this Type t) =>
        typeof(ExtType).GetMethod("GetDefaultGeneric")?.MakeGenericMethod(t).Invoke(null, null);
    public static T? GetDefaultGeneric<T>() => default;

    #region Nested type: TypeIsPortion
    /// <summary>Multi-method library for string condition check related commands.</summary>
    public class TypeIsPortion: ExtendablePortion<Type>, IIsPortion<Type> {
        #region Constants / Static Fields
        private static readonly HashSet<Type> integerTypes = [
            typeof(byte),
            typeof(short),
            typeof(int),
            typeof(long),
            typeof(sbyte),
            typeof(ushort),
            typeof(uint),
            typeof(ulong),
            typeof(byte?),
            typeof(short?),
            typeof(int?),
            typeof(long?),
            typeof(sbyte?),
            typeof(ushort?),
            typeof(uint?),
            typeof(ulong?)
        ];
        #endregion

        #region Fields / Properties
        /// <summary>Checks if type is Boolean</summary>
        public bool Boolean => Value == typeof(bool) || Value == typeof(bool?);
        /// <summary>Checks if type is Concrete-type</summary>
        public bool Concrete => !Value.IsAbstract && !Value.IsInterface;

        /// <summary>Checks if type is concrete and has () constructor</summary>
        public bool ConcreteWithDefaultCtor => Concrete && Value.GetConstructor(Array.Empty<Type>()) != null;
        /// <summary>Returns true if the type is a DateTime or nullable DateTime</summary>
        /// <returns></returns>
        public bool DateTime => Value == typeof(DateTime) || Value == typeof(DateTime?);

        /// <summary>Returns a boolean value indicating whether or not the type is: Enum</summary>
        /// <returns>Bool indicating whether the type is enum</returns>
        public bool Enum => Value.GetTypeInfo().IsEnum;

        /// <summary>Returns a boolean value indicating whether or not the type is: decimal, float or double</summary>
        /// <returns>Bool indicating whether the type is floating point</returns>
        public bool FloatingPoint => Value == typeof(decimal) || Value == typeof(float) || Value == typeof(double);
        /// <summary>Checks if type is generic-enumerable</summary>
        public bool GenericEnumerable {
            get {
                var genericArgs = Value.GetGenericArguments();
                return genericArgs.Length == 1 && typeof(IEnumerable<>).MakeGenericType(genericArgs).IsAssignableFrom(Value);
            }
        }

        /// <summary>Returns a boolean value indicating whether or not the type is: int, long or short</summary>
        /// <returns>Bool indicating whether the type is integer based</returns>
        public bool IntegerBased => integerTypes.Contains(Value);
        /// <summary>Returns a boolean value indicating whether or not the type is: int, long, decimal, short, float, or double</summary>
        /// <returns>Bool indicating whether the type is numeric</returns>
        public bool Numeric => FloatingPoint || IntegerBased;
        /// <summary>Checks if type is open-generic</summary>
        public bool OpenGeneric => Value.IsGenericTypeDefinition || Value.ContainsGenericParameters;
        /// <summary>Checks if type is Primitive-type</summary>
        public bool Primitive => Value.IsPrimitive && String && Value != typeof(IntPtr);
        /// <summary>Checks if type is Simple-type</summary>
        public bool Simple => Value.IsPrimitive || String || Value.IsEnum;
        /// <summary>Checks if type is string</summary>
        public bool String => Value.Equals(typeof(string));
        #endregion

        #region Constructors
        /// <summary>Is</summary>
        public TypeIsPortion(Type value) : base(value) { }
        #endregion

        /// <summary>Checks if type is Nullable</summary>
        public bool Nullable() => Z.Util.ReflectionHelper.IsNullable(Value);
        /// <summary>Checks if type is Nullable</summary>
        public bool Nullable(out Type? underLyingType) => Z.Util.ReflectionHelper.IsNullable(Value, out underLyingType);
        /// <summary>Checks if type is Nullable Of T</summary>
        public bool NullableOf<T>() => NullableOf(typeof(T));
        /// <summary>Checks if type is Nullable Of T</summary>
        public bool NullableOf(Type otherType) => Nullable() && Value.GetGenericArguments()[0].Equals(otherType);

        /// <summary>Checks if type is Nullable Of T or Equal to T</summary>
        public bool TypeOrNullableOf<T>() {
            var otherType = typeof(T);
            return Value == otherType ||
                   (Nullable() && Value.GetGenericArguments()[0].Equals(otherType));
        }
        /// <summary>Checks if type is inside namespace</summary>
        public bool InNamespace(string nameSpace) => Value.Namespace?.StartsWith(nameSpace) == true;
        /// <summary>Checks if type is concrete type of T</summary>
        public bool ConcreteTypeOf<T>() => ConcreteTypeOf(typeof(T));
        /// <summary>Checks if type is concrete type of T</summary>
        public bool ConcreteTypeOf(Type otherType) => Concrete && otherType.IsAssignableFrom(Value);

        /// <summary>Checks if type has interface T</summary>
        /// <typeparam name="T">Interface Type</typeparam>
        public bool HasInterface<T>() => HasInterface(typeof(T));

        /// <summary>Checks if type has interface T</summary>
        /// <param name="interfaceType">interface type</param>
        public bool HasInterface(Type interfaceType) => Value.GetTypeInfo().GetInterface(interfaceType) != null;

        /// <summary>Check if this is a base type</summary>
        /// <param name="checkingType"></param>
        /// <returns></returns>
        /// <remarks>Contributed by Michael T, http://about.me/MichaelTran</remarks>
        public bool BaseType(Type checkingType) {
            while (Value != typeof(object)) {
                if (Value == null) continue;
                if (Value == checkingType) return true;
                if (Value.BaseType == null) break;
                Value = Value.BaseType;
            }
            return false;
        }

        /// <summary>Check if this is a sub class generic type</summary>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        /// <remarks>Contributed by Michael T, http://about.me/MichaelTran</remarks>
        public bool SubclassOfRawGeneric(Type toCheck) {
            while (toCheck != typeof(object)) {
                if (toCheck == null) continue;
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (Value == cur) return true;
                if (toCheck.BaseType == null) break;
                toCheck = toCheck.BaseType;
            }
            return false;
        }

        /// <summary>Checks type is a collection</summary>
        public bool Collection() => Value.GetInterfaces().Any(s =>
            s.Namespace == "System.Collections.Generic" &&
            (s.Name == "IEnumerable" || s.Name.StartsWith("IEnumerable`")));
    }
    #endregion

    /// <summary>Checks if T can be cast to current Type</summary>
    public static bool IsAssignableFrom<T>(this Type type) => type?.IsAssignableFrom(typeof(T)) ?? false;
    /// <summary>Checks if type can be cast to T</summary>
    public static bool IsAssignableTo<T>(this Type type) => type?.IsAssignableTo(typeof(T)) ?? false;

    #region Disabled Extensions
    /*
        public static bool Closes(this Type type, Type openType)
        {
            if (type == null) return false;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == openType) return true;

            foreach (var @interface in type.GetInterfaces())
            {
                if (@interface.Closes(openType)) return true;
            }

            Type baseType = type.BaseType;
            if (baseType == null) return false;

            bool closes = baseType.IsGenericType && baseType.GetGenericTypeDefinition() == openType;
            if (closes) return true;

            return type.BaseType == null ? false : type.BaseType.Closes(openType);
        }

        public static Type GetInnerTypeFromNullable(this Type nullableType)
        {
            return nullableType.GetGenericArguments()[0];
        }

        public static string GetName(this Type type)
        {
            if (type.IsGenericType)
            {
                string[] parameters = Array.ConvertAll(type.GetGenericArguments(), t => t.GetName());
                string parameterList = String.Join(", ", parameters);
                return "{0}<{1}>".ToFormat(type.Name, parameterList);
            }

            return type.Name;
        }

        public static string GetFullName(this Type type)
        {
            if (type.IsGenericType)
            {
                string[] parameters = Array.ConvertAll(type.GetGenericArguments(), t => t.GetName());
                string parameterList = String.Join(", ", parameters);
                return "{0}<{1}>".ToFormat(type.Name, parameterList);
            }

            return type.FullName;
        }*/
    #endregion Disabled Extensions
}