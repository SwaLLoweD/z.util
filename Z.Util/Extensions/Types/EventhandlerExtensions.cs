﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System;
using System.Threading.Tasks;

namespace Z.Extensions
{
    //Outdated as of C# 6.0 since null check invoke for eventhandler fixes all threadsafety   Ex: event?.Invoke(sender, EventArg) is thread-safe
    /// <summary>Extension methods for Eventhandlers</summary>
    public static partial class ExtEventHandler
    {

        ///// <summary>Raises the Event</summary>
        //public static Exception Raise(this EventHandler handler, object sender, EventArgs args, object lockObj)
        //{
        //    if (lockObj == null) {
        //        return Raise(handler, sender, args);
        //    }
        //    else {
        //        lock (lockObj) {
        //            return Raise(handler, sender, args);
        //        }
        //    }
        //}
        ///// <summary>Raises the Event</summary>
        //public static Exception Raise(this EventHandler handler, object sender, EventArgs args)
        //{
        //    try {
        //        handler?.Invoke(sender, args);
        //    }
        //    catch (Exception e) {
        //        return e;
        //    }
        //    return null;
        //}
        ///// <summary>Raises the Event</summary>
        //public static Exception Raise<T>(this EventHandler<T> handler, object sender, T args, object lockObj) where T : EventArgs
        //{
        //    if (lockObj == null) {
        //        return Raise(handler, sender, args);
        //    }
        //    else {
        //        lock (lockObj) {
        //            return Raise(handler, sender, args);
        //        }
        //    }
        //}
        ///// <summary>Raises the Event</summary>
        //public static Exception Raise<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs
        //{
        //    try {
        //        handler?.Invoke(sender, args);
        //    }
        //    catch (Exception e) {
        //        return e;
        //    }
        //    return null;
        //}

        ///// <summary>Detaches event from the invocation list</summary>
        //public static void Detach<T>(this EventHandler<T> handler, object lockObject) where T : EventArgs
        //{
        //    lock (lockObject) {
        //        Detach(handler);
        //    }
        //}
        ///// <summary>Detaches event from the invocation list</summary>
        //public static void Detach<T>(this EventHandler<T> handler) where T : EventArgs
        //{
        //    if (handler == null) return;
        //    var l = handler.GetInvocationList();
        //    foreach (Delegate d in l) {
        //        try { handler -= (EventHandler<T>)d; }
        //        catch {
        //        }
        //    }
        //}
        ///// <summary>Detaches event from the invocation list</summary>
        //public static void Detach(this EventHandler handler, object lockObject)
        //{
        //    lock (lockObject) {
        //        Detach(handler);
        //    }
        //}
        ///// <summary>Detaches event from the invocation list</summary>
        //public static void Detach(this EventHandler handler)
        //{
        //    if (handler == null) return;
        //    var l = handler.GetInvocationList();
        //    foreach (Delegate d in l) {
        //        try { handler -= (EventHandler)d; }
        //        catch { }
        //    }
        //}
    }
}