﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Text;

namespace Z.Extensions;

/// <summary>Extension methods for structs</summary>
public static class ExtStruct
{
    /// <summary>Converts byte value to char value</summary>
    /// <param name="value">The value to be converted.</param>
    public static char ToChar(this byte value) => Convert.ToChar(value);

    /// <summary>Converts char value to byte value</summary>
    /// <param name="value">The value to be converted.</param>
    public static byte ToByte(this char value) => Encoding.ASCII.GetBytes(value.ToString())[0];

    #region Disabled Extensions
    /*/// <summary>
    /// 	Converts the specified value to a corresponding nullable type
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    /// <param name = "value">The value.</param>
    /// <returns>The nullable type</returns>
    public static T? ToNullable<T>(this T value) where T : struct
    {
        return (value.Is().Empty ? null : (T?)value);
    }*/
    #endregion Disabled Extensions
}