﻿/* Copyright (c) <2018> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Z.Extensions
{
    /// <summary>Extension methods for Assembly values...</summary>
    public static class ExtAssembly
    {
        /// <summary>Checks the specified Assembly for types that implement the specified interface.</summary>
        /// <param name="value">The value to be checked.</param>
        /// <typeparam name="T">The base type to search for</typeparam>
        /// <returns>An array of Types that implement the speicified type</returns>
        public static IEnumerable<Type> FindDerivedFrom<T>(this Assembly value) => FindDerivedFrom(value, typeof(T));
        /// <summary>Checks the specified Assembly for types that implement the specified interface.</summary>
        /// <param name="value">The value to be checked.</param>
        /// <param name="type">The base type to search for</param>
        /// <returns>An array of Types that implement the speicified type</returns>
        public static IEnumerable<Type> FindDerivedFrom(this Assembly value, Type type) => value.GetTypes().Where(x => type.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract);
        /// <summary>Checks the specified AppDomain for types that implement the specified interface.</summary>
        /// <param name="value">The value to be checked.</param>
        /// <typeparam name="T">The base type to search for</typeparam>
        /// <returns>An array of Types that implement the speicified type</returns>
        public static IEnumerable<Type> FindDerivedFrom<T>(this AppDomain value) => FindDerivedFrom(value, typeof(T));
        /// <summary>Checks the specified AppDomain for types that implement the specified interface.</summary>
        /// <param name="value">The value to be checked.</param>
        /// <param name="type">The base type to search for</param>
        /// <returns>An array of Types that implement the speicified type</returns>
        public static IEnumerable<Type> FindDerivedFrom(this AppDomain value, Type type) =>
            value.GetAssemblies().SelectMany(x => x.GetTypes()).Where(x => type.IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract);
    }
}