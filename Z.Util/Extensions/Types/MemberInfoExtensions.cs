﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Reflection;

namespace Z.Extensions;

/// <summary>Extension methods for the MemberInfo class</summary>
public static class ExtMemberInfo {
    /// <summary>Get current value of properties or fields</summary>
    /// <param name="member">Member</param>
    /// <param name="instance">Instance</param>
    /// <returns>Current value</returns>
    public static object? GetValue(this MemberInfo member, object? instance) {
        if (member == null || instance == null) return null;
        var pi = member as PropertyInfo;
        var fi = member as FieldInfo;
        if (pi?.CanRead == true) return pi.GetValue(instance, null);
        if (fi != null) return fi.GetValue(instance);
        return null;
    }
    /// <summary>Set current value of properties or fields</summary>
    /// <param name="member">Member</param>
    /// <param name="instance">Instance</param>
    /// <param name="value">Value</param>
    /// <returns>true if successful</returns>
    public static bool SetValue(this MemberInfo member, object instance, object? value) {
        if (member == null || instance == null) return false;
        var pi = member as PropertyInfo;
        var fi = member as FieldInfo;
        if (pi?.CanWrite == true) {
            pi.SetValue(instance, value, null);
            return true;
        }
        if (fi != null) {
            fi.SetValue(instance, value);
            return true;
        }
        return false;
    }

    /// <summary>Gets the type of contained data in property or field</summary>
    /// <param name="member">Member</param>
    /// <returns>true if successful</returns>
    public static Type? GetMemberType(this MemberInfo member) {
        if (member == null) return null;
        var pi = member as PropertyInfo;
        var fi = member as FieldInfo;
        if (pi != null) return pi.PropertyType;
        if (fi != null) return fi.FieldType;
        return null;
    }
    /// <summary>Check if member has the attribute</summary>
    public static bool HasAttribute<T, TAttr>(this MemberInfo member) where TAttr : Attribute {
        return HasAttribute<TAttr>(member, typeof(T));
    }
    /// <summary>Gets the type of contained data in property or field</summary>
    public static bool HasAttribute<TAttr>(this MemberInfo member, Type type, bool inherit = false)
        where TAttr : Attribute {
        var all = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
            .Where(x => x.Name == member.Name).ToArray();
        var prop = all.FirstOrDefault(x => x.DeclaringType == type) ?? all.FirstOrDefault();
        if (prop == null) return false;
        return Attribute.IsDefined(prop, typeof(TAttr));
    }
}