﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIÞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Z.Collections.Bases;
using Z.Extensions;

namespace Z.Collections.Concurrent;

/// <summary>Circular FIFO queue that is thread-safe with a fixed capacity</summary>
/// <typeparam name="T">Type of elements to be stored</typeparam>
public class SyncCircularQueue<T> : CollectionBase<T>, IDisposable
{
    #region Fields / Properties
    private readonly ReaderWriterLockSlim control = new(LockRecursionPolicy.SupportsRecursion);
    private bool _disposed;
    private volatile int count;
    private volatile int head;
    private T[] queue;
    private volatile int tail;

    /// <summary>Total capacity of the queue</summary>
    public int Capacity => control.Read(() => queue.Length);
    /// <summary>Number of elements in the queue</summary>
    public override int Count => control.Read(() => count);
    /// <summary>When capacity is exceeded, overwrite old elements or throw an exception</summary>
    public bool Overwrite { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    /// <param name="initialArray">Initial Elements</param>
    public SyncCircularQueue(IEnumerable<T> initialArray) : this(initialArray.Count()) {
        Enqueue(initialArray);
    }
    /// <summary>Constructor</summary>
    /// <param name="capacity">Total capacity of the queue</param>
    /// <param name="overwrite">When capacity exceeded overwrite old elements or throw an exception</param>
    public SyncCircularQueue(int capacity = 50, bool overwrite = true) {
        head = tail = 0;
        queue = new T[capacity];
        Overwrite = overwrite;
    }
    #endregion

    /// <summary>Add an element to the queue</summary>
    /// <param name="value">Value to be added</param>
    public void Enqueue(T value) =>
        control.Write(() => {
            if (count + 1 > queue.Length && !Overwrite)
                throw new OverflowException("Queue is full!");

            if (tail == head) head = (head + 1) % queue.Length;
            queue[tail++] = value;
            if (tail >= queue.Length) tail = 0;
            if (count < queue.Length) count++;
        });
    /// <summary>Pull an element from the queue (If none exists, default value is returned)</summary>
    public T? Dequeue() =>
        control.Write(() => {
            var rval = default(T);
            if (count <= 0) return rval;
            rval = queue[head++];
            if (head >= queue.Length) head = 0;
            count--;
            return rval;
        });
    /// <summary>
    ///     Pull a number of elements from the queue (If more than the existing ones are requested, only existing ones
    ///     returned)
    /// </summary>
    public T?[] Dequeue(int len) =>
        control.Write(() => {
            if (len > count) len = count;
            var rval = new T?[len];
            for (var i = 0; i < len; i++) rval[i] = Dequeue();
            return rval;
        });
    /// <summary>Add a number of elements to the queue</summary>
    public void Enqueue(IEnumerable<T> values) {
        if (values == null) return;
        control.Write(() => {
            foreach (var val in values) Enqueue(val);
        });
    }

    #region CollectionBase
    /// <inheritdoc />
    public override IEnumerator<T> GetEnumerator() {
        for (var i = 0; i < count; i++) yield return queue[(head + i) % queue.Length];
    }
    /// <inheritdoc />
    public override bool Remove(T item) =>
        control.Write(() => {
            var indexes = queue.IndexOf(item, (a, b) => object.Equals(a, b));
            var cntFound = indexes.Count;
            if (cntFound <= 0) return false;
            var newQueue = new T[queue.Length];
            var newIndex = 0;
            for (var i = 0; i < queue.Length; i++) {
                if (indexes.Contains(i)) continue;
                newQueue[newIndex++] = queue[i];
            }
            tail -= cntFound;
            if (tail < 0) tail += queue.Length;
            count -= cntFound;
            return true;
        });
    /// <inheritdoc />
    public override void Clear() => control.Write(() => queue = new T[queue.Length]);
    #endregion

    #region Dispose
    /// <inheritdoc />
    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing) {
        if (!_disposed) {
            if (disposing) {
                //DetachEvents();
                queue?.Clear();
                control.Dispose();
            }
            // Call the appropriate methods to clean up
            // unmanaged resources here.
            _disposed = true;
        }
    }
    #endregion Dispose
}