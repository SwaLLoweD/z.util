/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using Z.Collections.Generic;

namespace Z.Collections.Concurrent;

/// <summary>IDictionary implementation that returns default(U) if a key does not exist instead of throwing an exception</summary>
/// <typeparam name="TKey">Key</typeparam>
/// <typeparam name="TValue">Value</typeparam>
[Serializable]
public class SyncSortedTable<TKey, TValue> : SyncSortedDictionary<TKey, TValue>
{
    #region Fields / Properties
    /// <inheritdoc />
    public new virtual TValue this[TKey key] {
        get {
            if (!base.TryGetValue(key, out var rval)) rval = default;
            return rval;
        }
        set => base[key] = value;
    }
    #endregion

    #region Constructors
    /// <inheritdoc />
    public SyncSortedTable() { }
    /// <inheritdoc />
    public SyncSortedTable(IComparer<TKey> comparer) : base(comparer) { }
    /// <inheritdoc />
    public SyncSortedTable(IEnumerable<KeyValuePair<TKey, TValue>> collection, IComparer<TKey> comparer) : base(collection, comparer) { }
    /// <inheritdoc />
    public SyncSortedTable(IEnumerable<KeyValuePair<TKey, TValue>> collection) : base(collection) { }
    #endregion

    /// <inheritdoc />
    public new virtual void Add(TKey key, TValue value) => TryAdd(key, value);

    /// <inheritdoc />
    public new virtual void Remove(TKey key) => TryRemove(key, out var val);
}

/// <summary>IDictionary implementation that returns default(U) if a key does not exist instead of throwing an exception</summary>
/// <typeparam name="TSort">Type used for sorting</typeparam>
/// <typeparam name="TKey">Key</typeparam>
/// <typeparam name="TValue">Value</typeparam>
[Serializable]
public class SyncSortedTable<TSort, TKey, TValue> : SyncHashtable<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>
{
    #region Fields / Properties
    /// <inheritdoc />
    public virtual TKey this[TSort key] => SortTable.TryGetValue(key, out var rval) ? rval : default;
    /// <inheritdoc />
    public new virtual TValue this[TKey key] => base.TryGetValue(key, out var rval) ? rval : default;

    /// <inheritdoc />
    public new virtual ICollection<TKey> Keys => base.Keys;
    /// <inheritdoc />
    public virtual ICollection<TSort> SortKeys => SortTable.Keys;
    /// <inheritdoc />
    public new virtual ICollection<TValue> Values => base.Values;
    /// <inheritdoc />
    public virtual TValue this[TSort sort, TKey key] {
        get => base.TryGetValue(key, out var rval) ? rval : default;
        set {
            base[key] = value;
            SortTable[sort] = key;
        }
    }
    /// <inheritdoc />
    public SyncSortedDictionary<TSort, TKey> SortTable { get; private set; }
    #endregion

    #region Constructors
    /// <inheritdoc />
    public SyncSortedTable(IComparer<TSort> comparer = null) => SortTable = new SyncSortedDictionary<TSort, TKey>(comparer);
    #endregion

    #region ICollection<KeyValuePair<TKey,TValue>> Members
    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() => GetEnumerator();
    void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item) => throw new NotSupportedException();
    #endregion

    /// <inheritdoc />
    public virtual void Add(TSort sort, TKey key, TValue value) => TryAdd(sort, key, value);

    private void Add(TKey key, TValue value) => throw new NotSupportedException();

    /// <inheritdoc />
    public virtual void Remove(TSort sort, TKey key) => TryRemove(sort, key, out var s);
    /// <inheritdoc />
    public virtual void Remove(TSort sort) => TryRemove(sort, out var s);
    /// <inheritdoc />
    public virtual void Remove(TKey key) => TryRemove(key, out var s);

    /// <inheritdoc />
    public virtual bool ContainsKey(TSort sort, TKey key) => SortTable.ContainsKey(sort) && base.ContainsKey(key);
    /// <inheritdoc />
    public virtual bool ContainsKey(TSort sort) => SortTable.ContainsKey(sort);
    /// <inheritdoc />
    public new virtual bool ContainsKey(TKey key) => base.ContainsKey(key);

    /// <inheritdoc />
    public new virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
        var e = SortTable.GetEnumerator();
        while (e.MoveNext()) {
            var key = e.Current.Value;
            if (TryGetValue(key, out var value)) yield return new KeyValuePair<TKey, TValue>(key, value);
        }
    }
    /// <inheritdoc />
    public override int GetHashCode() => SortTable.GetHashCode() ^ base.GetHashCode();
    /// <inheritdoc />
    public override bool Equals(object obj) {
        var ct = obj as SortedTable<TSort, TKey, TValue>;
        if (obj == null) return false;
        return GetHashCode() == ct.GetHashCode();
    }

    /// <inheritdoc />
    public virtual bool TryAdd(TSort sort, TKey key, TValue value) => SortTable.TryAdd(sort, key) ? base.TryAdd(key, value) : false;

    /// <inheritdoc />
    public virtual bool TryRemove(TSort sort, out TValue value) {
        value = default;
        if (!SortTable.ContainsKey(sort)) return false;
        var key = SortTable[sort];
        if (!base.ContainsKey(key)) return false;
        SortTable.Remove(sort);
        if (key != null) base.TryRemove(key, out value);
        return true;
    }
    /// <inheritdoc />
    public new virtual bool TryRemove(TKey key, out TValue value) {
        value = default;
        if (!base.ContainsKey(key)) return false;
        var v = value = base[key];
        var sort = SortTable.Where(x => Equals(x.Value, key)).Select(x => x.Key).FirstOrDefault();
        base.TryRemove(key, out v);
        if (key != null) SortTable.Remove(sort);
        return true;
    }
    /// <inheritdoc />
    public virtual bool TryRemove(TSort sort, TKey key, out TValue value) {
        value = default;
        if (SortTable.TryRemove(sort, out var k)) return base.TryRemove(key, out value);
        return false;
    }
    /// <inheritdoc />
    public new virtual bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue) => base.TryUpdate(key, newValue, comparisonValue);
    /// <inheritdoc />
    public new virtual bool TryGetValue(TKey key, out TValue value) => base.TryGetValue(key, out value);
    /// <inheritdoc />
    protected new virtual TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory) => throw new NotSupportedException();
    /// <inheritdoc />
    protected new virtual TValue GetOrAdd(TKey key, TValue value) => throw new NotSupportedException();
    /// <inheritdoc />
    public virtual TValue GetOrAdd(TSort sort, TKey key, TValue value) {
        if (ContainsKey(key)) return base[key];
        TryAdd(sort, key, value);
        return value;
    }
    /// <inheritdoc />
    protected new virtual TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory) => throw new NotSupportedException();
    /// <inheritdoc />
    protected new virtual TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory) => throw new NotSupportedException();

    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TSort sort, TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory) {
        if (ContainsKey(key)) return base[key] = updateValueFactory(key, base[key]);
        TryAdd(sort, key, addValue);
        return addValue;
    }
}