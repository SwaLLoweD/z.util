﻿/*
Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>
Copyright (c) 2003-2006 Niels Kokholm and Peter Sestoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Collections.Generic;
using System.Threading;
using Z.Collections.Generic;
using Z.Collections.Helpers;
using Z.Extensions;

namespace Z.Collections.Concurrent;

/// <summary>
///     A list collection based on a plain dynamic array data structure. Expansion of the internal array is performed by
///     doubling on demand. The internal array is only shrinked by the Clear method.
///     <i>
///         When the FIFO property is set to false this class works fine as a stack of T. When the FIFO property is set to
///         true the class will function as a (FIFO) queue but very inefficiently, use a LinkedList (
///         <see cref="T:C5.LinkedList`1" />) instead.
///     </i>
/// </summary>
public class SyncPriorityQueue<T> : PriorityQueue<T>
{
    #region Fields / Properties
    private readonly ReaderWriterLockSlim control = new(LockRecursionPolicy.SupportsRecursion);

    /// <inheritdoc />
    public override int Count => control.Read(() => base.Count);
    /// <inheritdoc />
    public override T this[IPriorityQueueHandle<T> handle] { get => control.Read(() => base[handle]); set => control.Write(() => base[handle] = value); }
    #endregion

    #region Constructors
    /// <summary>Create an array list with default item equalityComparer and initial capacity 8 items.</summary>
    #pragma warning disable
    public SyncPriorityQueue(IEnumerable<T> collection, IComparer<T>? comparer = null, IEqualityComparer<T>? itemequalityComparer = null) : base(collection, comparer, itemequalityComparer) { }
    #pragma warning enable
    /// <summary>Create an interval heap with external item comparer and default initial capacity (16)</summary>
    /// <param name="comparer">The external comparer</param>
    public SyncPriorityQueue(IComparer<T> comparer) : this(16, comparer) { }

    //TODO: maybe remove
    /// <summary>Create an interval heap with natural item comparer and prescribed initial capacity</summary>
    /// <param name="capacity">The initial capacity</param>
    public SyncPriorityQueue(int capacity = 16) : base(capacity, Comparer<T>.Default, EqualityComparer<T>.Default) { }

    /// <summary>Create an interval heap with external item comparer and prescribed initial capacity</summary>
    /// <param name="capacity">The initial capacity</param>
    /// <param name="comparer">The external comparer</param>
    public SyncPriorityQueue(int capacity, IComparer<T> comparer) : base(capacity, comparer, Comparers.EqualityComparerZeroHashCode(comparer)) { }
    #endregion

    /// <inheritdoc />
    public override void Add(T item) => control.Write(() => base.Add(item));

    /// <inheritdoc />
    public override bool Remove(T item) => control.Write(() => base.Remove(item));

    /// <inheritdoc />
    public override void Clear() => control.Write(() => base.Clear());

    /// <inheritdoc />
    public override IEnumerator<T> GetEnumerator() => control.Read(() => base.GetEnumerator());

    /// <inheritdoc />
    public override bool Find(IPriorityQueueHandle<T> handle, out T item) {
        control.EnterReadLock();
        var rval = base.Find(handle, out item);
        control.ExitReadLock();
        return rval;
    }

    /// <inheritdoc />
    public override bool Add(ref IPriorityQueueHandle<T> handle, T item) {
        control.EnterWriteLock();
        var rval = base.Add(ref handle, item);
        control.ExitWriteLock();
        return rval;
    }

    /// <inheritdoc />
    public override T Delete(IPriorityQueueHandle<T> handle) => control.Write(() => base.Delete(handle));

    /// <inheritdoc />
    public override T Replace(IPriorityQueueHandle<T> handle, T item) => control.Write(() => base.Replace(handle, item));

    /// <inheritdoc />
    public override T FindMin(out IPriorityQueueHandle<T> handle) {
        control.EnterReadLock();
        var rval = base.FindMin(out handle);
        control.ExitReadLock();
        return rval;
    }

    /// <inheritdoc />
    public override T FindMax(out IPriorityQueueHandle<T> handle) {
        control.EnterReadLock();
        var rval = base.FindMax(out handle);
        control.ExitReadLock();
        return rval;
    }

    /// <inheritdoc />
    public override T DeleteMin(out IPriorityQueueHandle<T> handle) {
        control.EnterWriteLock();
        var rval = base.DeleteMin(out handle);
        control.ExitWriteLock();
        return rval;
    }

    /// <inheritdoc />
    public override T DeleteMax(out IPriorityQueueHandle<T> handle) {
        control.EnterWriteLock();
        var rval = base.DeleteMax(out handle);
        control.ExitWriteLock();
        return rval;
    }

    ///// <inheritdoc/>
    //public override T FindMin() => (size == 0) ? throw new ArgumentOutOfRangeException("Heap is empty.") : heap[0].first;
    ///// <inheritdoc/>
    //public override T DeleteMin() => DeleteMin(out var handle);
    ///// <inheritdoc/>
    //public override T FindMax()
    ///// <inheritdoc/>
    //public override T DeleteMax() => DeleteMax(out var handle);
}