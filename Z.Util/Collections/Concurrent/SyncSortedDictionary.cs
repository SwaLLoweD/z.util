/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Z.Collections.Generic;
using Z.Collections.Helpers;
using Z.Extensions;

namespace Z.Collections.Concurrent;

/// <summary>Thread-safe SortedDictionary implementation</summary>
/// <typeparam name="TKey">Key</typeparam>
/// <typeparam name="TValue">Value</typeparam>
[Serializable]
public class SyncSortedDictionary<TKey, TValue> : SortedDictionary<TKey, TValue>, ISyncDictionary<TKey, TValue>, IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary where TKey : notnull
{
    #region Fields / Properties
    /// <summary>Thread-safety locker</summary>
    protected readonly ReaderWriterLockSlim control = new();
    #endregion

    #region Constructors
    /// <inheritdoc />
    public SyncSortedDictionary(IComparer<TKey>? comparer = null) : base(comparer) { } //Init();
    /// <inheritdoc />
    public SyncSortedDictionary(IEnumerable<KeyValuePair<TKey, TValue>> collection, IComparer<TKey>? comparer = null) : base(Wrappers.Dictionary(collection), comparer) { } //Init();"
#endregion

#region ISyncDictionary<TKey,TValue> Members
/// <inheritdoc />
public new virtual TValue this[TKey key] {
        get =>
            control.Read(() => {
                if (base.TryGetValue(key, out var val)) return val;
                throw new NullReferenceException("Tkey can not be found in SyncSortedDictionary");
            });
        set => control.Write(() => base[key] = value);
    }
    #endregion

    // private void Init() {
    //     /* control.WriteEnd(); */
    // }

    #region SortedDictionary Wrap Methods
    /// <inheritdoc />
    public new virtual int Count => control.Read(() => base.Count);
    /// <inheritdoc />
    public new virtual ICollection<TKey> Keys => control.Read(() => base.Keys);
    /// <inheritdoc />
    public new virtual ICollection<TValue> Values => control.Read(() => base.Values);

    /// <inheritdoc />
    public new virtual void Add(TKey key, TValue value) => control.Write(() => base.Add(key, value));
    /// <inheritdoc />
    public new virtual void Clear() => control.Write(() => base.Clear());

    /// <inheritdoc />
    public new virtual bool ContainsKey(TKey key) => control.Read(() => base.ContainsKey(key));
    /// <inheritdoc />
    public new virtual bool ContainsValue(TValue value) => control.Read(() => base.ContainsValue(value));
    /// <inheritdoc />
    public new virtual void CopyTo(KeyValuePair<TKey, TValue>[] array, int index) => control.Read(() => base.CopyTo(array, index));

    /// <inheritdoc />
    public new virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
        control.EnterReadLock();
        var en = base.GetEnumerator();
        //var lst = new List<KeyValuePair<TKey, TValue>>(base.Count);
        while (en.MoveNext()) yield return en.Current;
        control.ExitReadLock();
    }
    /// <inheritdoc />
    public new virtual bool Remove(TKey key) => control.Write(() => base.Remove(key));
    /// <inheritdoc />
    public new virtual bool TryGetValue(TKey key, [System.Diagnostics.CodeAnalysis.MaybeNullWhen(false)] out TValue value) {
        control.EnterReadLock();
        var rval = base.TryGetValue(key, out value);
        control.ExitReadLock();
        return rval;
    }
    #endregion SortedDictionary Wrap Methods

    #region Explicit Interfaces
    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() => GetEnumerator();
    //Not Thread-safe
    //IDictionaryEnumerator IDictionary.GetEnumerator() { return base.GetEnumerator() as IDictionaryEnumerator; }
    void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> keyValuePair) => TryAdd(keyValuePair.Key, keyValuePair.Value);
    bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> keyValuePair) =>
        TryGetValue(keyValuePair.Key, out var x) && EqualityComparer<TValue>.Default.Equals(x, keyValuePair.Value);
    bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> keyValuePair) => TryRemove(keyValuePair.Key);
    //Not Thread-safe
    //void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int index);
    void IDictionary<TKey, TValue>.Add(TKey key, TValue value) => TryAdd(key, value);
    //Not Thread-safe
    //void ICollection.CopyTo(Array array, int index)
    void IDictionary.Add(object key, object? value) => TryAdd((TKey)key, (TValue)(value ?? throw new NullReferenceException("TValue can not be null")));
    bool IDictionary.Contains(object key) => ContainsKey((TKey)key);
    void IDictionary.Remove(object key) => TryRemove((TKey)key);
    #endregion Explicit Interfaces

    #region ISyncDictionary<TKey,TValue> Methods
    /// <inheritdoc />
    public virtual bool TryGetValue(TKey key, Action<TValue> valueAction) =>
        control.Read(() => {
            if (!base.TryGetValue(key, out var value)) return false;
            valueAction(value);
            return true;
        });
    /// <inheritdoc />
    public virtual bool TryAdd(TKey key, TValue value) =>
        control.Write(() => {
            if (base.ContainsKey(key)) return false;
            base.Add(key, value);
            return true;
        });
    /// <inheritdoc />
    public virtual bool TryAdd(TKey key, Func<TKey, TValue> valueFactory) {
        if (key == null || valueFactory == null) return false;
        return control.Write(() => {
            var value = valueFactory(key);
            if (base.ContainsKey(key)) return false;
            base.Add(key, value);
            return true;
        });
    }
    /// <inheritdoc />
    public virtual bool TryRemove(TKey key) => TryRemove(key, out _);
    /// <inheritdoc />
    public virtual bool TryRemove(TKey key, [System.Diagnostics.CodeAnalysis.MaybeNullWhen(false)] out TValue value) {
        control.EnterWriteLock();
        var rval = false;
        try {
            if (!base.ContainsKey(key)) {
                value = default;
            } else {
                value = base[key];
                rval = base.Remove(key);
            }
        } catch (Exception) {
            throw;
        } finally {
            control.ExitWriteLock();
        }
        return rval;
    }
    /// <inheritdoc />
    public virtual bool TryUpdate(TKey key, Func<TKey, TValue, TValue> updateValueFactory) =>
        control.Write(() => {
            if (!base.ContainsKey(key)) return false;
            var oldValue = base[key];
            var newValue = updateValueFactory(key, oldValue);
                //if (!object.Equals(oldValue, newValue)) return false;
                base[key] = newValue;
            return true;
        });
    /// <inheritdoc />
    public virtual bool TryUpdate(TKey key, TValue newValue, Func<TKey, TValue, TValue, bool> compareValueFactory) =>
        control.Write(() => {
            if (!base.ContainsKey(key)) return false;
            var oldValue = base[key];
            if (!compareValueFactory(key, newValue, oldValue)) return false;
                //if (!object.Equals(oldValue, newValue)) return false;
                base[key] = newValue;
            return true;
        });
    /// <inheritdoc />
    public virtual bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue) =>
        control.Write(() => {
            if (!base.ContainsKey(key)) return false;
            var oldValue = base[key];
            if (!Equals(oldValue, comparisonValue)) return false;
            base[key] = newValue;
            return true;
        });
    /// <inheritdoc />
    public virtual TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory) =>
        control.Write(() => {
            if (base.ContainsKey(key)) return this[key];
            var value = valueFactory(key);
            base.Add(key, value);
            return value;
        });
    /// <inheritdoc />
    public virtual TValue GetOrAdd(TKey key, TValue value) => GetOrAdd(key, _ => value);
    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory) =>
        control.Write(() => {
            TValue value;
            if (base.ContainsKey(key)) {
                var oldValue = base[key];
                value = updateValueFactory(key, oldValue);
            } else {
                value = addValueFactory(key);
            }
            base.Add(key, value);
            return value;
        });
    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory) => AddOrUpdate(key, _ => addValue, updateValueFactory);
    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TKey key, TValue addValue, TValue updateValue) => AddOrUpdate(key, _ => addValue, (_, _) => addValue);
    #endregion ISyncDictionary<TKey,TValue> Methods
}

/// <summary>IDictionary implementation that returns default(U) if a key does not exist instead of throwing an exception</summary>
/// <typeparam name="TSort">Type used for sorting</typeparam>
/// <typeparam name="TKey">Key</typeparam>
/// <typeparam name="TValue">Value</typeparam>
[Serializable]
public class SyncSortedDictionary<TSort, TKey, TValue> : ConcurrentDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>> where TSort: notnull where TKey: notnull
{
    #region Fields / Properties
    /// <inheritdoc />
    public virtual TKey this[TSort key] => SortTable.TryGetValue(key, out var rval) ? rval : throw new NullReferenceException("TSort key not found");
    /// <inheritdoc />
    public new virtual TValue this[TKey key] => base.TryGetValue(key, out var rval) ? rval : throw new NullReferenceException("TKey key not found");

    /// <inheritdoc />
    public new virtual ICollection<TKey> Keys => base.Keys;
    /// <inheritdoc />
    public virtual ICollection<TSort> SortKeys => SortTable.Keys;
    /// <inheritdoc />
    public new virtual ICollection<TValue> Values => base.Values;
    /// <inheritdoc />
    public virtual TValue this[TSort sort, TKey key] {
        get => base.TryGetValue(key, out var rval) ? rval : throw new NullReferenceException("TKey key not found");
        set {
            base[key] = value;
            SortTable[sort] = key;
        }
    }
    /// <inheritdoc />
    public SyncSortedDictionary<TSort, TKey> SortTable { get; }
    #endregion

    #region Constructors
    /// <inheritdoc />
    public SyncSortedDictionary(IComparer<TSort>? comparer = null) => SortTable = new SyncSortedDictionary<TSort, TKey>(comparer);
    #endregion

    #region ICollection<KeyValuePair<TKey,TValue>> Members
    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() => GetEnumerator();
    void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item) => throw new NotSupportedException();
    #endregion

    /// <inheritdoc />
    public virtual void Add(TSort sort, TKey key, TValue value) => TryAdd(sort, key, value);

    //private void Add(TKey key, TValue value) => throw new NotSupportedException();

    /// <inheritdoc />
    public virtual void Remove(TSort sort, TKey key) => TryRemove(sort, key, out _);
    /// <inheritdoc />
    public virtual void Remove(TSort sort) => TryRemove(sort, out _);
    /// <inheritdoc />
    public virtual void Remove(TKey key) => TryRemove(key, out _);

    /// <inheritdoc />
    public virtual bool ContainsKey(TSort sort, TKey key) => SortTable.ContainsKey(sort) && base.ContainsKey(key);
    /// <inheritdoc />
    public virtual bool ContainsKey(TSort sort) => SortTable.ContainsKey(sort);
    /// <inheritdoc />
    public new virtual bool ContainsKey(TKey key) => base.ContainsKey(key);

    /// <inheritdoc />
    public new virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
        var e = SortTable.GetEnumerator();
        while (e.MoveNext()) {
            var key = e.Current.Value;
            if (TryGetValue(key, out var value)) yield return new KeyValuePair<TKey, TValue>(key, value!);
        }
    }
    /// <inheritdoc />
    public override int GetHashCode() => SortTable.GetHashCode() ^ base.GetHashCode();
    /// <inheritdoc />
    public override bool Equals(object? obj) {
        if (obj is not SortedDictionary<TSort, TKey, TValue> ct) return false;
        return GetHashCode() == ct.GetHashCode();
    }

    /// <inheritdoc />
    public virtual bool TryAdd(TSort sort, TKey key, TValue value) => SortTable.TryAdd(sort, key) && base.TryAdd(key, value);

    /// <inheritdoc />
    public virtual bool TryRemove(TSort sort, out TValue? value) {
        value = default;
        if (!SortTable.ContainsKey(sort)) return false;
        var key = SortTable[sort];
        if (!base.ContainsKey(key)) return false;
        SortTable.Remove(sort);
        base.TryRemove(key, out value);
        return true;
    }
    /// <inheritdoc />
    public new virtual bool TryRemove(TKey key, out TValue? value) {
        value = default;
        if (!base.ContainsKey(key)) return false;
        var v = value = base[key];
        var sort = SortTable.Where(x => Equals(x.Value, key)).Select(x => x.Key).First();
        base.TryRemove(key, out v);
        if (key != null) SortTable.Remove(sort);
        return true;
    }
    /// <inheritdoc />
    public virtual bool TryRemove(TSort sort, TKey key, out TValue? value) {
        value = default;
        if (SortTable.TryRemove(sort, out _)) return base.TryRemove(key, out value);
        return false;
    }
    /// <inheritdoc />
    public new virtual bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue) => base.TryUpdate(key, newValue, comparisonValue);
    /// <inheritdoc />
    public new virtual bool TryGetValue(TKey key, out TValue? value) => base.TryGetValue(key, out value);
    /// <inheritdoc />
    protected new virtual TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory) => throw new NotSupportedException();
    /// <inheritdoc />
    protected new virtual TValue GetOrAdd(TKey key, TValue value) => throw new NotSupportedException();
    /// <inheritdoc />
    public virtual TValue GetOrAdd(TSort sort, TKey key, TValue value) {
        if (ContainsKey(key)) return base[key];
        TryAdd(sort, key, value);
        return value;
    }
    /// <inheritdoc />
    protected new virtual TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory) => throw new NotSupportedException();
    /// <inheritdoc />
    protected new virtual TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory) => throw new NotSupportedException();

    /// <inheritdoc />
    public virtual TValue AddOrUpdate(TSort sort, TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory) {
        if (ContainsKey(key)) return base[key] = updateValueFactory(key, base[key]);
        TryAdd(sort, key, addValue);
        return addValue;
    }
}