/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Threading;
using Z.Extensions;

namespace Z.Collections.Concurrent;

/// <summary>Thread-safe LinkedList implementation</summary>
/// <typeparam name="T">Value Type</typeparam>
[Serializable]
public class SyncLinkedList<T>: LinkedList<T>, IEnumerable<T> {
    #region Fields / Properties
    private readonly ReaderWriterLockSlim control = new(LockRecursionPolicy.SupportsRecursion);
    #endregion

    #region Constructors
    /// <inheritdoc />
    public SyncLinkedList() { }
    /// <inheritdoc />
    public SyncLinkedList(IEnumerable<T> collection) : base(collection) { }
    #endregion

    #region LinkedList Wrap Methods
    /// <inheritdoc />
    public new virtual int Count => control.Read(() => base.Count);
    /// <inheritdoc />
    public new virtual LinkedListNode<T>? First => control.Read(() => base.First);
    /// <inheritdoc />
    public new virtual LinkedListNode<T>? Last => control.Read(() => base.Last);

    /// <inheritdoc />
    public new virtual void AddAfter(LinkedListNode<T> node, LinkedListNode<T> newNode) => control.Write(() => base.AddAfter(node, newNode));
    /// <inheritdoc />
    public new virtual LinkedListNode<T> AddAfter(LinkedListNode<T> node, T value) => control.Write(() => base.AddAfter(node, value));
    /// <inheritdoc />
    public new virtual void AddBefore(LinkedListNode<T> node, LinkedListNode<T> newNode) => control.Write(() => base.AddBefore(node, newNode));
    /// <inheritdoc />
    public new virtual LinkedListNode<T> AddBefore(LinkedListNode<T> node, T value) => control.Write(() => base.AddBefore(node, value));
    /// <inheritdoc />
    public new virtual void AddFirst(LinkedListNode<T> node) => control.Write(() => base.AddFirst(node));
    /// <inheritdoc />
    public new virtual LinkedListNode<T> AddFirst(T value) => control.Write(() => base.AddFirst(value));
    /// <inheritdoc />
    public new virtual void AddLast(LinkedListNode<T> node) => control.Write(() => base.AddLast(node));
    /// <inheritdoc />
    public new virtual LinkedListNode<T> AddLast(T value) => control.Write(() => base.AddLast(value));

    /// <inheritdoc />
    public new virtual void Clear() => control.Write(() => base.Clear());
    /// <inheritdoc />
    public new virtual bool Contains(T value) => control.Read(() => base.Contains(value));
    /// <inheritdoc />
    public new virtual void CopyTo(T[] array, int index) => control.Read(() => base.CopyTo(array, index));

    /// <inheritdoc />
    public new virtual LinkedListNode<T>? Find(T value) => control.Read(() => base.Find(value));
    /// <inheritdoc />
    public new virtual LinkedListNode<T>? FindLast(T value) => control.Read(() => base.FindLast(value));

    /// <inheritdoc />
    public new virtual IEnumerator<T> GetEnumerator() =>
        control.Read(() => {
            var en = base.GetEnumerator();
            var lst = new List<T>(base.Count);
            while (en.MoveNext()) lst.Add(en.Current);
            return lst.GetEnumerator();
        });

    IEnumerator<T> IEnumerable<T>.GetEnumerator() => GetEnumerator();

    /// <inheritdoc />
    public override void OnDeserialization(object? sender) => control.Write(() => base.OnDeserialization(sender));

    /// <inheritdoc />
    public new virtual void Remove(LinkedListNode<T> node) => control.Write(() => base.Remove(node));
    /// <inheritdoc />
    public new virtual bool Remove(T value) => control.Write(() => base.Remove(value));
    /// <inheritdoc />
    public new virtual void RemoveFirst() => control.Write(() => base.RemoveFirst());
    /// <inheritdoc />
    public new virtual void RemoveLast() => control.Write(() => base.RemoveLast());
    #endregion LinkedList Wrap Methods
}
