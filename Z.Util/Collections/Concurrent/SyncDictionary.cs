/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Z.Collections;
using Z.Collections.Helpers;
using Z.Extensions;

namespace Z.Collections.Concurrent
{
    /// <summary>Thread-safe Dictionary implementation that uses atomic concurrent methods</summary>
    /// <typeparam name="TKey">Key</typeparam>
    /// <typeparam name="TValue">Value</typeparam>
    [Serializable]
    public class SyncDictionary<TKey, TValue> : ConcurrentDictionary<TKey, TValue>, ISyncDictionary<TKey, TValue>, IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary, ICollection
    {
        /// <summary>Thread-safety locker</summary>
        protected readonly ReaderWriterLockSlim control = new ReaderWriterLockSlim();

        #region Constructors
        /// <inheritdoc/>
        public SyncDictionary() : base() { }
        /// <inheritdoc/>
        public SyncDictionary(int concurrencyLevel, int capacity) : base(concurrencyLevel, capacity) { }
        /// <inheritdoc/>
        public SyncDictionary(IEnumerable<KeyValuePair<TKey, TValue>> collection) : base(collection) { }
        /// <inheritdoc/>
        public SyncDictionary(IEqualityComparer<TKey> comparer) : base(comparer) { }
        /// <inheritdoc/>
        public SyncDictionary(IEnumerable<KeyValuePair<TKey, TValue>> collection, IEqualityComparer<TKey> comparer) : base(collection, comparer) { }
        /// <inheritdoc/>
        public SyncDictionary(int concurrencyLevel, int capacity, IEqualityComparer<TKey> comparer) : base(concurrencyLevel, capacity, comparer) { }
        /// <inheritdoc/>
        public SyncDictionary(int concurrencyLevel, IEnumerable<KeyValuePair<TKey, TValue>> collection, IEqualityComparer<TKey> comparer) : base(concurrencyLevel, collection, comparer) { }
        #endregion Constructors

        #region ConcurrentDictionary Methods
        /// <inheritdoc/>
        public new virtual TValue this[TKey key]
        {
            get { return control.Read(() => { return base[key]; }); }
            set { control.Write(() => base[key] = value); }
        }
        /// <inheritdoc/>
        public virtual new int Count { get { return control.Read(() => base.Count); } }
        /// <inheritdoc/>
        public new bool IsEmpty { get { return control.Read(() => base.IsEmpty); } }
        /// <inheritdoc/>
        public virtual new ICollection<TKey> Keys { get { return control.Read(() => base.Keys); } }
        /// <inheritdoc/>
        public virtual new ICollection<TValue> Values { get { return control.Read(() => base.Values); } }
        /// <inheritdoc/>
        public virtual new void Clear() { control.Write(() => base.Clear()); }
        /// <inheritdoc/>
        public virtual new bool ContainsKey(TKey key) { return control.Read(() => base.ContainsKey(key)); }
        /// <inheritdoc/>
        public virtual new IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return control.Read(() => {
                var en = base.GetEnumerator();
                var lst = new List<KeyValuePair<TKey, TValue>>(base.Count);
                while (en.MoveNext())
                    lst.Add(en.Current);
                return lst.GetEnumerator();
            });
        }
        #endregion ConcurrentDictionary Methods

        #region Explicit interfaces
        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() { return this.GetEnumerator(); }
        //Not Thread-safe
        //IDictionaryEnumerator IDictionary.GetEnumerator() { return base.GetEnumerator() as IDictionaryEnumerator; }
        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> keyValuePair) { TryAdd(keyValuePair.Key, keyValuePair.Value); }
        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> keyValuePair)
        {
            TValue x;
            return this.TryGetValue(keyValuePair.Key, out x) && EqualityComparer<TValue>.Default.Equals(x, keyValuePair.Value);
        }
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> keyValuePair) { return TryRemove(keyValuePair.Key); }
        //Not Thread-safe
        //void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int index);
        void IDictionary<TKey, TValue>.Add(TKey key, TValue value) { TryAdd(key, value); }
        //Not Thread-safe
        //void ICollection.CopyTo(Array array, int index)
        void IDictionary.Add(object key, object value) { TryAdd((TKey)key, (TValue)value); }
        bool IDictionary.Contains(object key) { return ContainsKey((TKey)key); }
        void IDictionary.Remove(object key) { TryRemove((TKey)key); }
        #endregion Explicit interfaces

        #region ISyncDictionary<TKey,TValue> Methods
        /// <inheritdoc/>
        public virtual bool TryGetValue(TKey key, Action<TValue> valueAction)
        {
            return control.Read(() => {
                TValue value;
                if (!base.TryGetValue(key, out value)) return false;
                valueAction(value);
                return true;
            });
        }
        /// <inheritdoc/>
        public virtual new bool TryGetValue(TKey key, out TValue value)
        {
            //control.EnterReadLock();
            TValue val = default(TValue);
            var rval = TryGetValue(key, (v) => { val = v; });
            value = val;
            //control.ExitReadLock();
            return rval;
        }
        /// <inheritdoc/>
        public virtual bool TryAdd(TKey key, Func<TKey, TValue> valueFactory)
        {
            if (key == null || valueFactory == null) return false;
            return control.Write(() => {
                var value = valueFactory(key);
                return base.TryAdd(key, value);
            });
        }
        /// <inheritdoc/>
        public new virtual bool TryAdd(TKey key, TValue value) { return TryAdd(key, k => { return value; }); }

        /// <inheritdoc/>
        public virtual bool TryRemove(TKey key)
        {
            TValue value;
            return TryRemove(key, out value);
        }
        /// <inheritdoc/>
        public new virtual bool TryRemove(TKey key, out TValue value)
        {
            control.EnterWriteLock();
            try {
                return base.TryRemove(key, out value);
            }
            catch (Exception e) {
                value = default(TValue);
                throw e;
            }
            finally {
                control.ExitWriteLock();
            }
        }
        /// <inheritdoc/>
        public virtual bool TryUpdate(TKey key, Func<TKey, TValue, TValue> updateValueFactory)
        {
            return control.Write(() => {
                if (!base.ContainsKey(key)) return false;
                var oldValue = base[key];
                var newValue = updateValueFactory(key, oldValue);
                //if (!object.Equals(oldValue, newValue)) return false;
                base[key] = newValue;
                return true;
            });
        }
        /// <inheritdoc/>
        public virtual bool TryUpdate(TKey key, TValue newValue, Func<TKey, TValue, TValue, bool> compareValueFactory)
        {
            return control.Write(() => {
                if (!base.ContainsKey(key)) return false;
                var oldValue = base[key];
                if (!compareValueFactory(key, newValue, oldValue)) return false;
                //if (!object.Equals(oldValue, newValue)) return false;
                base[key] = newValue;
                return true;
            });
        }
        /// <inheritdoc/>
        public new virtual bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue)
        {
            return control.Write(() => {
                if (!base.ContainsKey(key)) return false;
                var oldValue = base[key];
                if (!object.Equals(oldValue, newValue)) return false;
                base[key] = newValue;
                return true;
            });
        }
        /// <inheritdoc/>
        public new virtual TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory) { return control.Write(() => base.GetOrAdd(key, valueFactory)); }
        /// <inheritdoc/>
        public new virtual TValue GetOrAdd(TKey key, TValue value) { return GetOrAdd(key, new Func<TKey, TValue>(x => { return value; })); }
        /// <inheritdoc/>
        public new virtual TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory)
        { return control.Write(() => base.AddOrUpdate(key, addValueFactory, updateValueFactory)); }
        /// <inheritdoc/>
        public new virtual TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory)
        { return control.Write(() => base.AddOrUpdate(key, addValue, updateValueFactory)); }
        /// <inheritdoc/>
        public virtual TValue AddOrUpdate(TKey key, TValue addValue, TValue updateValue)
        { return AddOrUpdate(key, x => { return addValue; }, (x, y) => { return addValue; }); }
        #endregion ISyncDictionary<TKey,TValue> Methods
    }
}