/* Copyright (c) <2008> <PETAR_K_MARCHEV>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Z.Collections.Concurrent;

/// <summary>Thread-Safe HashSet</summary>
/// <typeparam name="T"></typeparam>
[Serializable]
public class SyncSet<T> : IEnumerable<T>, ISet<T>, ICollection<T>
{
    #region Fields / Properties
    private readonly HashSet<T> hashSet;
    private readonly object mutex = new();
    #endregion

    #region Constructors
    //public object Mutex { get { return this.mutex; } } //??

    /// <summary>Thread-Safe HashSet</summary>
    public SyncSet() => hashSet = new HashSet<T>();

    /// <summary>Thread-Safe HashSet</summary>
    /// <param name="collection">Initial values</param>
    public SyncSet(IEnumerable<T> collection) => hashSet = new HashSet<T>(collection);
    #endregion

    /// <summary>Thread-safe remove where</summary>
    /// <param name="predicate">Predicate to select values for removal</param>
    public int RemoveWhere(Predicate<T> predicate) {
        var toBeRemoved = new HashSet<T>();
        // GetEnumerator() is thread safe
        foreach (var item in this) {
            if (predicate(item))
                toBeRemoved.Add(item);
        }

        var foundAndRemovedCount = 0;
        lock (mutex) {
            foreach (var item in toBeRemoved) {
                var foundAndRemoved = hashSet.Remove(item);
                if (foundAndRemoved) foundAndRemovedCount++;
            }
        }
        return foundAndRemovedCount;
    }

    #region IEnumerable<T>
    /// <inheritdoc />
    public IEnumerator<T> GetEnumerator() {
        List<T> clone;

        lock (mutex) {
            clone = new List<T>(hashSet);
        }

        return clone.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion IEnumerable<T>

    #region ISet<T>
    /// <inheritdoc />
    public bool Add(T item) {
        bool elementWasAdded;

        lock (mutex) {
            elementWasAdded = hashSet.Add(item);
        }

        return elementWasAdded;
    }
    /// <inheritdoc />
    public bool Remove(T item) {
        bool foundAndRemoved;

        lock (mutex) {
            foundAndRemoved = hashSet.Remove(item);
        }

        return foundAndRemoved;
    }
    /// <inheritdoc />
    public void ExceptWith(IEnumerable<T> other) {
        lock (mutex) {
            hashSet.ExceptWith(other);
        }
    }
    /// <inheritdoc />
    public void IntersectWith(IEnumerable<T> other) {
        lock (mutex) {
            hashSet.IntersectWith(other);
        }
    }
    /// <inheritdoc />
    public bool IsProperSubsetOf(IEnumerable<T> other) {
        var isProperSubsetOf = hashSet.IsProperSubsetOf(other);
        return isProperSubsetOf;
    }
    /// <inheritdoc />
    public bool IsProperSupersetOf(IEnumerable<T> other) {
        var isProperSupersetOf = hashSet.IsProperSupersetOf(other);
        return isProperSupersetOf;
    }
    /// <inheritdoc />
    public bool IsSubsetOf(IEnumerable<T> other) {
        var isSubsetOf = hashSet.IsSubsetOf(other);
        return isSubsetOf;
    }
    /// <inheritdoc />
    public bool IsSupersetOf(IEnumerable<T> other) {
        var isSupersetOf = hashSet.IsSupersetOf(other);
        return isSupersetOf;
    }
    /// <inheritdoc />
    public bool Overlaps(IEnumerable<T> other) {
        var overlaps = hashSet.Overlaps(other);
        return overlaps;
    }
    /// <inheritdoc />
    public bool SetEquals(IEnumerable<T> other) {
        var setsAreEqual = hashSet.Overlaps(other);
        return setsAreEqual;
    }
    /// <inheritdoc />
    public void SymmetricExceptWith(IEnumerable<T> other) {
        lock (mutex) {
            hashSet.SymmetricExceptWith(other);
        }
    }
    /// <inheritdoc />
    public void UnionWith(IEnumerable<T> other) {
        lock (mutex) {
            hashSet.UnionWith(other);
        }
    }

    void ICollection<T>.Add(T item) => Add(item);
    /// <inheritdoc />
    public void Clear() {
        lock (mutex) {
            hashSet.Clear();
        }
    }
    /// <inheritdoc />
    public bool Contains(T item) {
        var contains = hashSet.Contains(item);
        return contains;
    }
    /// <inheritdoc />
    public void CopyTo(T[] array, int arrayIndex) {
        var i = 0;
        foreach (var item in this) {
            array[arrayIndex + i] = item;
            i++;
        }
    }
    /// <inheritdoc />
    public int Count => hashSet.Count;
    /// <inheritdoc />
    public bool IsReadOnly => ((ISet<T>)hashSet).IsReadOnly;
    #endregion ISet<T>
}