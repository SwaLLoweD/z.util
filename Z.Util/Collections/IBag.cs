﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Z.Collections.Generic;

/// <summary>
///     Bag&lt;T&gt; is a collection that contains items of type T. Unlike a Set, duplicate items (items that compare
///     equal to each other) are allowed in an Bag.
/// </summary>
/// <remarks>
///     <p>
///         The items are compared in one of two ways. If T implements IComparable&lt;T&gt; then the Equals method of that
///         interface will be used to compare items, otherwise the Equals method from Object will be used. Alternatively,
///         an instance of IComparer&lt;T&gt; can be passed to the constructor to use to compare items.
///     </p>
///     <p>
///         Bag is implemented as a hash table. Inserting, deleting, and looking up an element all are done in
///         approximately constant time, regardless of the number of items in the bag.
///     </p>
///     <p>
///         When multiple equal items are stored in the bag, they are stored as a representative item and a count. If equal
///         items can be distinguished, this may be noticable. For example, if a case-insensitive comparer is used with a
///         Bag&lt;string&gt;, and both "hello", and "HELLO" are added to the bag, then the bag will appear to contain two
///         copies of "hello" (the representative item).
///     </p>
///     <p>
///         <see cref="OrderedBag&lt;T&gt;" /> is similar, but uses comparison instead of hashing, maintain the items in
///         sorted order, and stores distinct copies of items that compare equal.
///     </p>
/// </remarks>
/// <seealso cref="OrderedBag&lt;T&gt;" />
public interface IBag<T> : ICollection<T>, IEnumerable<T>, IEnumerable
{
    /// <summary>
    ///     Removes all the items in <paramref name="other" /> from the bag. Items that are not present in the bag are
    ///     ignored.
    /// </summary>
    /// <remarks>
    ///     <para>Equality between items is determined by the comparer instance used to create the bag.</para>
    ///     <para>Removing the collection takes time O(M), where M is the number of items in <paramref name="other" />.</para>
    /// </remarks>
    /// <param name="other">A collection of items to remove from the bag.</param>
    /// <exception cref="ArgumentNullException"><paramref name="other" /> is null.</exception>
    void ExceptWith(IEnumerable<T> other);

    /// <summary>
    ///     Computes the intersection of this bag with another bag. The intersection of two bags is all items that appear
    ///     in both of the bags. If an item appears X times in one bag, and Y times in the other bag, the sum contains the item
    ///     Minimum(X,Y) times. This bag receives the intersection of the two bags, the other bag is unchanged.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         When equal items appear in both bags, the intersection will include an arbitrary choice of one of the two
    ///         equal items.
    ///     </para>
    ///     <para>The intersection of two bags is computed in time O(N), where N is the size of the smaller bag.</para>
    /// </remarks>
    /// <param name="other">Bag to intersection with.</param>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    void IntersectWith(IEnumerable<T> other);

    /// <summary>
    ///     Determines if this bag is a proper subset of another bag. Neither bag is modified. This bag is a subset of
    ///     <paramref name="other" /> if every element in this bag is also in <paramref name="other" />, at least the same
    ///     number of times. Additional, this bag must have strictly fewer items than <paramref name="other" />.
    /// </summary>
    /// <remarks>IsProperSubsetOf is computed in time O(N), where N is the number of unique items in this bag.</remarks>
    /// <param name="other">Bag to compare to.</param>
    /// <returns>True if this is a proper subset of <paramref name="other" />.</returns>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    bool IsProperSubsetOf(IEnumerable<T> other);

    /// <summary>
    ///     Determines if this bag is a proper superset of another bag. Neither bag is modified. This bag is a proper
    ///     superset of <paramref name="other" /> if every element in <paramref name="other" /> is also in this bag, at least
    ///     the same number of times. Additional, this bag must have strictly more items than <paramref name="other" />.
    /// </summary>
    /// <remarks>
    ///     IsProperSupersetOf is computed in time O(M), where M is the number of unique items in
    ///     <paramref name="other" />.
    /// </remarks>
    /// <param name="other">Set to compare to.</param>
    /// <returns>True if this is a proper superset of <paramref name="other" />.</returns>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    bool IsProperSupersetOf(IEnumerable<T> other);

    /// <summary>Determines if this bag is a subset of another ba11 items in this bag.</summary>
    /// <param name="other">Bag to compare to.</param>
    /// <returns>True if this is a subset of <paramref name="other" />.</returns>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    bool IsSubsetOf(IEnumerable<T> other);

    /// <summary>
    ///     Determines if this bag is a superset of another bag. Neither bag is modified. This bag is a superset of
    ///     <paramref name="other" /> if every element in <paramref name="other" /> is also in this bag, at least the same
    ///     number of times.
    /// </summary>
    /// <remarks>IsSupersetOf is computed in time O(M), where M is the number of unique items in <paramref name="other" />.</remarks>
    /// <param name="other">Bag to compare to.</param>
    /// <returns>True if this is a superset of <paramref name="other" />.</returns>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    bool IsSupersetOf(IEnumerable<T> other);

    /// <summary>Determines if this bag has at least one element common with another bag.</summary>
    /// <remarks>
    ///     <para>The answer is computed in time O(N), where N is the size of the smaller set.</para>
    /// </remarks>
    /// <param name="other">Bag to check overlap with.</param>
    /// <returns>True if the two bags overlap, false otherwise.</returns>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    bool Overlaps(IEnumerable<T> other);

    /// <summary>
    ///     Determines if this bag is disjoint from another bag. Two bags are disjoint if no item from one set is equal to
    ///     any item in the other bag.
    /// </summary>
    /// <remarks>
    ///     <para>The answer is computed in time O(N), where N is the size of the smaller set.</para>
    /// </remarks>
    /// <param name="other">Bag to check disjointness with.</param>
    /// <returns>True if the two bags are disjoint, false otherwise.</returns>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    bool IsDisjointFrom(IEnumerable<T> other);

    /// <summary>
    ///     Determines if this bag is equal to another bag. This bag is equal to <paramref name="other" /> if they contain
    ///     the same number of copies of equal elements.
    /// </summary>
    /// <remarks>IsSupersetOf is computed in time O(N), where N is the number of unique items in this bag.</remarks>
    /// <param name="other">Bag to compare to</param>
    /// <returns>True if this bag is equal to <paramref name="other" />, false otherwise.</returns>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    bool SetEquals(IEnumerable<T> other);

    /// <summary>
    ///     Computes the symmetric difference of this bag with another bag. The symmetric difference of two bags is all
    ///     items that appear in either of the bags, but not both. If an item appears X times in one bag, and Y times in the
    ///     other bag, the symmetric difference contains the item AbsoluteValue(X - Y) times. This bag receives the symmetric
    ///     difference of the two bags; the other bag is unchanged.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         The symmetric difference of two bags is computed in time O(M + N), where M is the size of the larger bag, and
    ///         N is the size of the smaller bag.
    ///     </para>
    /// </remarks>
    /// <param name="other">Bag to symmetric difference with.</param>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    void SymmetricExceptWith(IEnumerable<T> other);

    /// <summary>
    ///     Computes the union of this bag with another bag. The union of two bags is all items from both of the bags. If
    ///     an item appears X times in one bag, and Y times in the other bag, the union contains the item Maximum(X,Y) times.
    ///     This bag receives the union of the two bags, the other bag is unchanged.
    /// </summary>
    /// <remarks>
    ///     <para>The union of two bags is computed in time O(M+N), where M and N are the size of the two bags.</para>
    /// </remarks>
    /// <param name="other">Bag to union with.</param>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    void UnionWith(IEnumerable<T> other);

    /// <summary>
    ///     Computes the sum of this bag with another bag. The sum of two bags is all items from both of the bags. If an
    ///     item appears X times in one bag, and Y times in the other bag, the sum contains the item (X+Y) times. This bag
    ///     receives the sum of the two bags, the other bag is unchanged.
    /// </summary>
    /// <remarks>
    ///     <para>The sum of two bags is computed in time O(M), where M is the size of the other bag..</para>
    /// </remarks>
    /// <param name="other">Bag to sum with.</param>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    void SumWith(IEnumerable<T> other);

    /// <summary>
    ///     Computes the difference of this bag with another bag. The difference of these two bags is all items that
    ///     appear in this bag, but not in <paramref name="other" />. If an item appears X times in this bag, and Y times in
    ///     the other bag, the difference contains the item X - Y times (zero times if Y >= X). This bag receives the
    ///     difference of the two bags; the other bag is unchanged.
    /// </summary>
    /// <remarks>
    ///     <para>The difference of two bags is computed in time O(M), where M is the size of the other bag.</para>
    /// </remarks>
    /// <param name="other">Bag to difference with.</param>
    /// <exception cref="InvalidOperationException">
    ///     This bag and <paramref name="other" /> don't use the same method for
    ///     comparing items.
    /// </exception>
    void DifferenceWith(IEnumerable<T> other);
}