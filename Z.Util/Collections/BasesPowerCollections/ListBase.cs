﻿//******************************
// Written by Peter Golde
// Copyright (c) 2004-2005, Wintellect
//
// Use and restribution of this code is subject to the license agreement
// contained in the file "License.txt" accompanying this file.
//******************************

using System;
using System.Collections;
using System.Collections.Generic;
using Z.Extensions;

namespace Z.Collections.Bases;
/// <summary>
///     ListBase is an abstract class that can be used as a base class for a read-write collection that needs to
///     implement the generic IList&lt;T&gt; and non-generic IList collections. The derived class needs to override the
///     following methods: Count, Clear, Insert, RemoveAt, and the indexer. The implementation of all the other methods in
///     IList&lt;T&gt; and IList are handled by ListBase.
/// </summary>
/// <typeparam name="T"></typeparam>
[Serializable]
public abstract class ListBase<T> : CollectionBase<T>, IList<T>, IList
{
    #region IList<T> Members
    #region IEnumerable<T>
    /// <summary>
    ///     Enumerates all of the items in the list, in order. The item at index 0 is enumerated first, then the item at
    ///     index 1, and so on.
    /// </summary>
    /// <remarks>
    ///     The enumerator does not check for changes made to the structure of the list. Thus, changes to the list during
    ///     enumeration may cause incorrect enumeration or out of range exceptions. Consider overriding this method and adding
    ///     checks for structural changes.
    /// </remarks>
    /// <returns>An IEnumerator&lt;T&gt; that enumerates all the items in the list.</returns>
    public override IEnumerator<T> GetEnumerator() {
        var count = Count;
        for (var i = 0; i < count; ++i) yield return this[i];
    }
    #endregion IEnumerable<T>
    #endregion

    /// <summary>Copies all the items in the list, in order, to <paramref name="array" />, starting at index 0.</summary>
    /// <param name="array">The array to copy to. This array must have a size that is greater than or equal to Count.</param>
    public virtual void CopyTo(T[] array) => CopyTo(array, 0);

    /// <summary>
    ///     Copies a range of elements from the list to <paramref name="array" />, starting at
    ///     <paramref name="arrayIndex" />.
    /// </summary>
    /// <param name="index">The starting index in the source list of the range to copy.</param>
    /// <param name="array">
    ///     The array to copy to. This array must have a size that is greater than or equal to Count +
    ///     arrayIndex.
    /// </param>
    /// <param name="arrayIndex">The starting index in <paramref name="array" /> to copy to.</param>
    /// <param name="count">The number of items to copy.</param>
    public virtual void CopyTo(int index, T[] array, int arrayIndex, int count) => this.SubList(index, count).CopyTo(array, arrayIndex);

    /// <summary>Convert the given parameter to T. Throw an ArgumentException if it isn't.</summary>
    /// <param name="name">parameter name</param>
    /// <param name="value">parameter value</param>
    private static T? ConvertToItemType(string name, object? value) {
        try {
            return (T?)value;
        } catch (InvalidCastException) {
            throw new ArgumentException(string.Format(Strings.WrongType, value, typeof(T)), name);
        }
    }

    #region ICollection<T>
    /// <summary>The property must be overridden by the derived class to return the number of items in the list.</summary>
    /// <value>The number of items in the list.</value>
    public abstract override int Count { get; }

    /// <summary>
    ///     Adds an item to the end of the list. This method is equivalent to calling: <c>Insert(Count, item)</c>
    /// </summary>
    /// <param name="item">The item to add to the list.</param>
    public override void Add(T item) => Insert(Count, item);
    /// <summary>This method must be overridden by the derived class to empty the list of all items.</summary>
    public abstract override void Clear();

    /// <summary>
    ///     Determines if the list contains any item that compares equal to <paramref name="item" />. The implementation
    ///     simply checks whether IndexOf(item) returns a non-negative value.
    /// </summary>
    /// <remarks>
    ///     Equality in the list is determined by the default sense of equality for T. If T implements IComparable&lt;T
    ///     &gt;, the Equals method of that interface is used to determine equality. Otherwise, Object.Equals is used to
    ///     determine equality.
    /// </remarks>
    /// <param name="item">The item to search for.</param>
    /// <returns>True if the list contains an item that compares equal to <paramref name="item" />.</returns>
    public override bool Contains(T? item) => IndexOf(item) >= 0;

    /// <summary>
    ///     Copies all the items in the list, in order, to <paramref name="array" />, starting at
    ///     <paramref name="arrayIndex" />.
    /// </summary>
    /// <param name="array">
    ///     The array to copy to. This array must have a size that is greater than or equal to Count +
    ///     arrayIndex.
    /// </param>
    /// <param name="arrayIndex">The starting index in <paramref name="array" /> to copy to.</param>
    public override void CopyTo(T[] array, int arrayIndex) => base.CopyTo(array, arrayIndex);

    /// <summary>
    ///     Searches the list for the first item that compares equal to <paramref name="item" />. If one is found, it is
    ///     removed. Otherwise, the list is unchanged.
    /// </summary>
    /// <remarks>
    ///     Equality in the list is determined by the default sense of equality for T. If T implements IComparable&lt;T
    ///     &gt;, the Equals method of that interface is used to determine equality. Otherwise, Object.Equals is used to
    ///     determine equality.
    /// </remarks>
    /// <param name="item">The item to remove from the list.</param>
    /// <returns>
    ///     True if an item was found and removed that compared equal to <paramref name="item" />. False if no such item
    ///     was in the list.
    /// </returns>
    public override bool Remove(T item) {
        var index = IndexOf(item);
        if (index >= 0) {
            RemoveAt(index);
            return true;
        }
        return false;
    }
    #endregion ICollection<T>

    #region IList<T>
    /// <summary>The indexer must be overridden by the derived class to get and set values of the list at a particular index.</summary>
    /// <param name="index">
    ///     The index in the list to get or set an item at. The first item in the list has index 0, and the
    ///     last has index Count-1.
    /// </param>
    /// <returns>The item at the given index.</returns>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="index" /> is less than zero or greater than or equal to
    ///     Count.
    /// </exception>
    public abstract T this[int index] { get; set; }
    /// <summary>Finds the index of the first item, that is equal to <paramref name="item" />.</summary>
    /// <remarks>
    ///     The default implementation of equality for type T is used in the search. This is the equality defined by
    ///     IComparable&lt;T&gt; or object.Equals.
    /// </remarks>
    /// <param name="item">The item to search fror.</param>
    /// <returns>
    ///     The index of the first item in the given range that is equal to <paramref name="item" />.  If no item is
    ///     equal to <paramref name="item" />, -1 is returned.
    /// </returns>
    public virtual int IndexOf(T? item) {
        var index = 0;
        foreach (var x in this) {
            if (EqualityComparer<T>.Default.Equals(x, item)) return index;
            ++index;
        }

        // didn't find any item that matches.
        return -1;
    }

    /// <summary>This method must be overridden by the derived class to insert a new item at the given index.</summary>
    /// <param name="index">
    ///     The index in the list to insert the item at. After the insertion, the inserted item is located at
    ///     this index. The first item in the list has index 0.
    /// </param>
    /// <param name="item">The item to insert at the given index.</param>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="index" /> is less than zero or greater than Count.</exception>
    public abstract void Insert(int index, T? item);

    /// <summary>This method must be overridden by the derived class to remove the item at the given index.</summary>
    /// <param name="index">The index in the list to remove the item at. The first item in the list has index 0.</param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="index" /> is less than zero or greater than or equal to
    ///     Count.
    /// </exception>
    public abstract void RemoveAt(int index);
    #endregion IList<T>

    #region IList
    /// <summary>
    ///     Adds an item to the end of the list. This method is equivalent to calling: <c>Insert(Count, item)</c>
    /// </summary>
    /// <param name="value">The item to add to the list.</param>
    /// <exception cref="ArgumentException"><paramref name="value" /> cannot be converted to T.</exception>
    int IList.Add(object? value) {
        var count = Count;
        Insert(count, ListBase<T>.ConvertToItemType("value", value));
        return count;
    }

    /// <summary>Removes all the items from the list, resulting in an empty list.</summary>
    void IList.Clear() => Clear();

    /// <summary>Determines if the list contains any item that compares equal to <paramref name="value" />.</summary>
    /// <remarks>
    ///     Equality in the list is determined by the default sense of equality for T. If T implements IComparable&lt;T
    ///     &gt;, the Equals method of that interface is used to determine equality. Otherwise, Object.Equals is used to
    ///     determine equality.
    /// </remarks>
    /// <param name="value">The item to search for.</param>
    bool IList.Contains(object? value) => (value is T || value == null) && Contains((T?)value);

    /// <summary>
    ///     Find the first occurrence of an item equal to <paramref name="value" /> in the list, and returns the index of
    ///     that item.
    /// </summary>
    /// <remarks>
    ///     Equality in the list is determined by the default sense of equality for T. If T implements IComparable&lt;T
    ///     &gt;, the Equals method of that interface is used to determine equality. Otherwise, Object.Equals is used to
    ///     determine equality.
    /// </remarks>
    /// <param name="value">The item to search for.</param>
    /// <returns>
    ///     The index of <paramref name="value" />, or -1 if no item in the list compares equal to
    ///     <paramref name="value" />.
    /// </returns>
    int IList.IndexOf(object? value) => value is T || value == null ? IndexOf((T?)value) : -1;

    /// <summary>Insert a new item at the given index.</summary>
    /// <param name="index">
    ///     The index in the list to insert the item at. After the insertion, the inserted item is located at
    ///     this index. The first item in the list has index 0.
    /// </param>
    /// <param name="value">The item to insert at the given index.</param>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="index" /> is less than zero or greater than Count.</exception>
    /// <exception cref="ArgumentException"><paramref name="value" /> cannot be converted to T.</exception>
    void IList.Insert(int index, object? value) => Insert(index, ListBase<T>.ConvertToItemType("value", value));

    /// <summary>Returns whether the list is a fixed size. This implementation always returns false.</summary>
    /// <value>Alway false, indicating that the list is not fixed size.</value>
    bool IList.IsFixedSize => false;

    /// <summary>
    ///     Returns whether the list is read only. This implementation returns the value from ICollection&lt;T&gt;
    ///     .IsReadOnly, which is by default, false.
    /// </summary>
    /// <value>By default, false, indicating that the list is not read only.</value>
    bool IList.IsReadOnly => ((ICollection<T>)this).IsReadOnly;

    /// <summary>
    ///     Searches the list for the first item that compares equal to <paramref name="value" />. If one is found, it is
    ///     removed. Otherwise, the list is unchanged.
    /// </summary>
    /// <remarks>
    ///     Equality in the list is determined by the default sense of equality for T. If T implements IComparable&lt;T
    ///     &gt;, the Equals method of that interface is used to determine equality. Otherwise, Object.Equals is used to
    ///     determine equality.
    /// </remarks>
    /// <param name="value">The item to remove from the list.</param>
    /// <exception cref="ArgumentException"><paramref name="value" /> cannot be converted to T.</exception>
    void IList.Remove(object? value) {
        if (value is T value2) Remove(value2);
    }

    /// <summary>Removes the item at the given index.</summary>
    /// <param name="index">The index in the list to remove the item at. The first item in the list has index 0.</param>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="index" /> is less than zero or greater than or equal to
    ///     Count.
    /// </exception>
    void IList.RemoveAt(int index) => RemoveAt(index);

    /// <summary>Gets or sets the value at a particular index in the list.</summary>
    /// <param name="index">
    ///     The index in the list to get or set an item at. The first item in the list has index 0, and the
    ///     last has index Count-1.
    /// </param>
    /// <value>The item at the given index.</value>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     <paramref name="index" /> is less than zero or greater than or equal to
    ///     Count.
    /// </exception>
    /// <exception cref="ArgumentException"><paramref name="value" /> cannot be converted to T.</exception>
    object? IList.this[int index] { get => this[index]; set => Insert(index, ListBase<T>.ConvertToItemType("value", value)); }
    #endregion IList
}