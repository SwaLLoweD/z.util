﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Z.Extensions;

namespace Z.Collections.Specialized;

/// <summary>IPRanges collection</summary>
[Serializable]
public class IPRangeCollection : List<IPRange>
{
    /// <summary>Checks if any of the ranges contain specified IP</summary>
    /// <param name="ip">Ip Address to be checked</param>
    /// <returns>true if any of the ranges contain specified IP</returns>
    public bool Contains(IPAddress ip) {
        if (ip == null) return false;
        foreach (var target in this) {
            if (target == null) continue;
            if (!target.InRange(ip)) continue;
            return !target.Reverse;
        }
        return false;
    }
    /// <summary>Reconstruct IPRange collection from its string dump</summary>
    /// <param name="s">String dump of IPRange collection</param>
    public static IPRangeCollection Parse(string s) {
        var rval = new IPRangeCollection();
        if (s.Is().Empty) return rval;
        var ss = s.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
        foreach (var rangeString in ss) {
            var range = IPRange.Parse(rangeString);
            if (range != null) rval.Add(range);
        }
        return rval;
    }
    /// <summary>Dump current IPRange collection to string</summary>
    public override string ToString() {
        if (Count == 0) return "";
        var sb = new StringBuilder();
        var values = ToArray();
        foreach (var v in values) {
            sb.Append(v);
            sb.Append('|');
        }
        return sb.ToString();
    }
    /// <summary>Get all possible IPAddresses in current collection</summary>
    public HashSet<IPAddress> ListIpsInRange() {
        var rval = new HashSet<IPAddress>();
        foreach (var ipr in this) {
            var ipsInRange = ipr.ListIpsInRange();
            if (ipr.Reverse)
                rval.RemoveWhere(x => ipsInRange.Contains(x));
            else
                rval.AddUnique(ipsInRange);
        }
        return rval;
    }
}

#region IPRange
/// <summary>IPRange</summary>
[Serializable]
public class IPRange
{
    #region Fields / Properties
    /// <summary>IPRanges end address</summary>
    public IPAddress End { get; set; }
    /// <summary>IPRange is exclusive or inclusive(true exclusive, false inclusive)</summary>
    public bool Reverse { get; set; }
    /// <summary>IPRange start address</summary>
    public IPAddress Start { get; set; }
    #endregion

    #pragma warning disable CS8618
    #region Constructors
    /// <summary>IPRange</summary>
    /// <param name="start">IPRange start address</param>
    /// <param name="cidr">Cidr (Mask)</param>
    /// <param name="reverse">IPRange is exclusive or inclusive(true exclusive, false inclusive)</param>
    public IPRange(IPAddress start, int cidr, bool reverse = false) {
        if (start == null) return;
        var startBytes = start.GetAddressBytes();
        var subnet = new byte[startBytes.Length];

        var arrayBitLength = startBytes.Length * 8;
        if (cidr > arrayBitLength) cidr = arrayBitLength;
        for (var i = 0; i < cidr; i++) {
            var byteIndex = i / 8;
            subnet[byteIndex] |= (byte)(1 << (7 - (i % 8)));
        }
        SetInitialValues(start, new IPAddress(subnet), reverse, true);
    }
    /// <summary>IPRange</summary>
    /// <param name="start">IPRange start address</param>
    /// <param name="endOrSubnet">IPRange end address or SubnetMask</param>
    /// <param name="reverse">IPRange is exclusive or inclusive(true exclusive, false inclusive)</param>
    /// <param name="isSubnet">true, if subnetmask is entered. false, if end address is entered</param>
    public IPRange(IPAddress start, IPAddress? endOrSubnet = null, bool reverse = false, bool isSubnet = false) => SetInitialValues(start, endOrSubnet, reverse, isSubnet);
    #endregion
    #pragma warning restore

    private static byte[] ExcludeNetworkAddresses(byte[] b) {
        if (b[^1] == 0) b[^1] = 1;
        if (b[^1] == 0xFF) b[^1] = 0xFE;
        return b;
    }
    /// <summary>Reconstruct IPRange from its string dump</summary>
    /// <param name="ipRangeString">String dump of IPRange</param>
    public static IPRange? Parse(string ipRangeString) {
        if (ipRangeString.Is().Empty) return null;
        var reverse = false;
        if (ipRangeString.StartsWith("(-)")) reverse = true;
        ipRangeString = ipRangeString.Replace("(-)", "").Replace("(+)", "");
        var vals = ipRangeString.Split('-');
        if (vals.Length < 2) return null;
        if (!IPAddress.TryParse(vals[0], out IPAddress? ipstart)) return null;
        if (!IPAddress.TryParse(vals[1], out IPAddress? ipend)) return new IPRange(ipstart, ipstart, reverse);
        return new IPRange(ipstart, ipend, reverse);
    }
    /// <summary>Get all possible IPAddresses in current range</summary>
    public IList<IPAddress> ListIpsInRange() {
        //var rval = new HashSet<IPAddress>();
        var startBytes = Start.GetAddressBytes();
        var endBytes = End.GetAddressBytes();
        var ipBytes = new byte[endBytes.Length];
        Array.Copy(startBytes, ipBytes, startBytes.Length);

        return GenerateHigherBytes(startBytes, endBytes, ipBytes, -1);
    }
    private static byte CalcMaxForDigit(byte[] startBytes, byte[] endBytes, byte[] curBytes, int index) {
        for (var i = 0; i < index; i++) {
            if (endBytes[i] > startBytes[i] && endBytes[i] > curBytes[i])
                return 255;
        }

        return endBytes[index];
    }
    private static byte CalcMinForDigit(byte[] startBytes, byte[] endBytes, byte[] curBytes, int index) {
        for (var i = 0; i < index; i++) {
            if (endBytes[i] > startBytes[i] && startBytes[i] < curBytes[i])
                return 0;
        }

        return startBytes[index];
    }
    // private void resetToMinimum(byte[] startBytes, byte[] endBytes, ref byte[] curBytes, int index) {
    //     for (var i = curBytes.Length - 1; i >= index; i--) curBytes[i] = calcMinForDigit(startBytes, endBytes, curBytes, i);
    // }
    private IList<IPAddress> GenerateHigherBytes(byte[] startBytes, byte[] endBytes, byte[] curBytes, int index) {
        var rval = new List<IPAddress>();
        for (var i = index + 1; i < curBytes.Length; i++) {
            var maxValue = CalcMaxForDigit(startBytes, endBytes, curBytes, i);
            var minValue = CalcMinForDigit(startBytes, endBytes, curBytes, i);
            for (int j = minValue; j <= maxValue; j++) {
                curBytes[i] = (byte)j;
                if (curBytes[^1] != 0 && curBytes[^1] != 0xFF) rval.Add(new IPAddress(curBytes));
                rval.AddUnique(GenerateHigherBytes(startBytes, endBytes, curBytes, i));

                curBytes[i] = minValue; //Reset to MinValue
            }
            //resetToMinimum(startBytes, endBytes, ref curBytes, i);
        }
        return rval;
    }
    /// <summary>Dump current IPRange collection to string</summary>
    public override string ToString() {
        if (Start == null) return "";
        return (Reverse ? "(-)" : "(+)") + Start + "-" + End;
    }
    /// <summary>Checks if an ip is on a subnet</summary>
    /// <param name="ip">Ip Address to check</param>
    /// <param name="net">Network Address</param>
    /// <param name="mask">Subnet Mask</param>
    /// <returns>true, if ip is in subnet. false, if not.</returns>
    public static bool IsIpOnSubnet(IPAddress ip, IPAddress net, IPAddress mask) {
        if (ip == null || net == null || mask == null) return false;

        var s = net.GetAddressBytes();
        var t = ip.GetAddressBytes();
        var m = mask.GetAddressBytes();

        return OnSubnet(t, s, m);
    }

    /// <summary>Checks if an ip is on a subnet</summary>
    /// <param name="ip">Ip Address to check</param>
    /// <param name="net">Network Address</param>
    /// <param name="mask">Subnet Mask</param>
    /// <returns>true, if ip is in subnet. false, if not.</returns>
    public static bool OnSubnet(byte[] ip, byte[] net, byte[] mask) {
        if (ip == null || net == null || mask == null) return false;
        var ms = new byte[net.Length]; //Masked Source
        var mt = new byte[ip.Length]; //Masked Target
        for (var i = 0; i < net.Length; i++) {
            ms[i] = (byte)(net[i] & mask[i]); //Masked Source
            mt[i] = (byte)(ip[i] & mask[i]); //Masked Target
        }
        var msa = new IPAddress(ms);
        var mta = new IPAddress(mt);
        return msa.Equals(mta);
    }
    /// <summary>Checks if an ip is in an IPRange</summary>
    /// <param name="ip">Ip Address to check</param>
    /// <returns>true, if ip is in range. false, if not.</returns>
    public bool InRange(IPAddress ip) => InRange(ip, Start, End);

    /// <summary>Checks if an ip is in an IP range</summary>
    /// <param name="ip">Ip Address to check</param>
    /// <param name="start">Start IP Address</param>
    /// <param name="end">End IP Address</param>
    /// <returns>true, if ip is in range. false, if not.</returns>
    public static bool InRange(IPAddress ip, IPAddress start, IPAddress end) {
        if (ip == null || start == null || end == null) return false;
        return InRange(ip.GetAddressBytes(), start.GetAddressBytes(), end.GetAddressBytes());
    }
    /// <summary>Checks if an ip is in an IP range</summary>
    /// <param name="ip">Ip Address to check</param>
    /// <param name="start">Start IP Address</param>
    /// <param name="end">End IP Address</param>
    /// <returns>true, if ip is in range. false, if not.</returns>
    public static bool InRange(byte[] ip, byte[] start, byte[] end) {
        if (ip == null || start == null || end == null) return false;
        if (ip.Length != start.Length || ip.Length != end.Length) return false; //ipv4 != ipv6

        bool lowerBoundary = true, upperBoundary = true;

        for (var i = 0; i < start.Length && (lowerBoundary || upperBoundary); i++) {
            if ((lowerBoundary && ip[i] < start[i]) || (upperBoundary && ip[i] > end[i])) return false;

            lowerBoundary &= ip[i] == start[i];
            upperBoundary &= ip[i] == end[i];
        }
        return true;
    }

    private void SetInitialValues(IPAddress start, IPAddress? endOrSubnet = null, bool reverse = false, bool isSubnet = false) {
        Reverse = reverse;
        endOrSubnet ??= (isSubnet ? IPAddress.Parse("255.255.255.0") : start);
        var sb = start.GetAddressBytes();
        var eb = endOrSubnet.GetAddressBytes();

        if (isSubnet) {
            var startb = new byte[sb.Length];
            var endb = new byte[sb.Length];
            for (var i = 0; i < eb.Length; i++) {
                if (sb.Length <= i) return;
                startb[i] = (byte)(eb[i] & sb[i]);
                endb[i] = (byte)((~eb[i] & 0xFF) | startb[i]);
            }
            sb = startb;
            eb = endb;
            //startb[sb.Length-1] += 1;
            //endb[sb.Length-1] -= 1;
        }
        sb = ExcludeNetworkAddresses(sb);
        eb = ExcludeNetworkAddresses(eb);
        Start = new IPAddress(sb);
        End = new IPAddress(eb);
    }
}
    #endregion IPRange