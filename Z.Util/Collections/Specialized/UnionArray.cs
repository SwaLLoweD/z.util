﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Runtime.InteropServices;

namespace Z.Collections.Specialized;

/// <summary>Array struct with direct conversions to different types</summary>
[StructLayout(LayoutKind.Explicit)]
public struct UnionArray
{
    /// <summary>Array values as bytes</summary>
    [FieldOffset(0)]
    public Byte[] Bytes;

    /// <summary>Array values as Shorts</summary>
    [FieldOffset(0)]
    public short[] Shorts;

    /// <summary>Array values as Ints</summary>
    [FieldOffset(0)]
    public int[] Ints;

    /// <summary>Array values as Sbytes</summary>
    [FieldOffset(0)]
    public SByte[] SBytes;

    /// <summary>Array values as Longs</summary>
    [FieldOffset(0)]
    public long[] Longs;

    /// <summary>Array values as Ushorts</summary>
    [FieldOffset(0)]
    public ushort[] UShorts;

    /// <summary>Array values as UInts</summary>
    [FieldOffset(0)]
    public uint[] UInts;

    /// <summary>Array values as ULongs</summary>
    [FieldOffset(0)]
    public ulong[] ULongs;
}