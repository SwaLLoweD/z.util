//******************************
// Written by Peter Golde
// Copyright (c) 2004-2005, Wintellect
//
// Use and restribution of this code is subject to the license agreement
// contained in the file "License.txt" accompanying this file.
//******************************

using System;
using System.Collections.Generic;

namespace Z.Collections.Helpers;

/// <summary>A holder class for various internal utility functions that need to be shared.</summary>
internal static class CollectionHelper
{
    /// <summary>Determine if a type is cloneable: either a value type or implementing ICloneable.</summary>
    /// <param name="type">Type to check.</param>
    /// <param name="isValue">Returns if the type is a value type, and does not implement ICloneable.</param>
    /// <returns>True if the type is cloneable.</returns>
    public static bool IsCloneableType(Type type, out bool isValue) {
        isValue = false;

        if (typeof(ICloneable).IsAssignableFrom(type)) return true;
        if (type.IsValueType) {
            isValue = true;
            return true;
        }
        return false;
    }

    /// <summary>Returns the simple name of the class, for use in exception messages.</summary>
    /// <returns>The simple name of this class.</returns>
    public static string SimpleClassName(Type type) {
        var name = type.Name;

        // Just use the simple name.
        var index = name.IndexOfAny(new[] { '<', '{', '`' });
        if (index >= 0) name = name[..index];

        return name;
    }

    /// <summary>Gets the hash code for an object using a comparer. Correctly handles null.</summary>
    /// <param name="item">Item to get hash code for. Can be null.</param>
    /// <param name="equalityComparer">The comparer to use.</param>
    /// <returns>The hash code for the item.</returns>
    public static int GetHashCode<T>(T item, IEqualityComparer<T> equalityComparer) {
        if (item == null) return 0x1786E23C;
        return equalityComparer.GetHashCode(item);
    }
}