﻿//******************************
// Written by Peter Golde
// Copyright (c) 2004-2005, Wintellect
//
// Use and restribution of this code is subject to the license agreement
// contained in the file "License.txt" accompanying this file.
//******************************

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Z.Collections.Helpers;
/// <summary>A collection of methods to create IComparer and IEqualityComparer instances in various ways.</summary>
internal static class Comparers
{
    /// <summary>
    ///     Reverses the order of comparison of an IComparer&lt;T&gt;. The resulting comparer can be used, for example, to
    ///     sort a collection in descending order. Equality and hash codes are unchanged.
    /// </summary>
    /// <typeparam name="T">The type of items thta are being compared.</typeparam>
    /// <param name="comparer">The comparer to reverse.</param>
    /// <returns>An IComparer&lt;T&gt; that compares items in the reverse order of <paramref name="comparer" />.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="comparer" /> is null.</exception>
    public static IComparer<T> GetReverseComparer<T>(IComparer<T> comparer) {
        if (comparer == null) throw new ArgumentNullException(nameof(comparer));

        return new ReverseComparerClass<T>(comparer);
    }

    /// <summary>
    ///     Gets an IEqualityComparer&lt;T&gt; instance that can be used to compare objects of type T for object identity
    ///     only. Two objects compare equal only if they are references to the same object.
    /// </summary>
    /// <returns>An IEqualityComparer&lt;T&gt; instance for identity comparison.</returns>
    public static IEqualityComparer<T> GetIdentityComparer<T>()
        where T : class =>
        new IdentityComparer<T>();

    /// <summary>
    ///     Reverses the order of comparison of an Comparison&lt;T&gt;. The resulting comparison can be used, for example,
    ///     to sort a collection in descending order.
    /// </summary>
    /// <typeparam name="T">The type of items that are being compared.</typeparam>
    /// <param name="comparison">The comparison to reverse.</param>
    /// <returns>A Comparison&lt;T&gt; that compares items in the reverse order of <paramref name="comparison" />.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="comparison" /> is null.</exception>
    public static Comparison<T> GetReverseComparison<T>(Comparison<T> comparison) {
        if (comparison == null) throw new ArgumentNullException(nameof(comparison));

        return (T x, T y) => -comparison(x, y);
    }

    /// <summary>
    ///     Given a comparison delegate that compares two items of type T, gets an IComparer&lt;T&gt; instance that
    ///     performs the same comparison.
    /// </summary>
    /// <param name="comparison">The comparison delegate to use.</param>
    /// <returns>An IComparer&lt;T&gt; that performs the same comparing operation as <paramref name="comparison" />.</returns>
    public static IComparer<T> GetComparerFromComparison<T>(Comparison<T?> comparison) {
        if (comparison == null) throw new ArgumentNullException(nameof(comparison));

        return ComparerFromComparison(comparison);
    }

    /// <summary>
    ///     Given in IComparer&lt;T&gt; instenace that comparers two items from type T, gets a Comparison delegate that
    ///     performs the same comparison.
    /// </summary>
    /// <param name="comparer">The IComparer&lt;T&gt; instance to use.</param>
    /// <returns>A Comparison&lt;T&gt; delegate that performans the same comparing operation as <paramref name="comparer" />.</returns>
    public static Comparison<T> GetComparisonFromComparer<T>(IComparer<T> comparer) {
        if (comparer == null) throw new ArgumentNullException(nameof(comparer));

        return comparer.Compare;
    }

    /// <summary>Given an Comparison on a type, returns an IComparer on that type.</summary>
    /// <typeparam name="T">T to compare.</typeparam>
    /// <param name="comparison">Comparison delegate on T</param>
    /// <returns>IComparer that uses the comparison.</returns>
    public static IComparer<T> ComparerFromComparison<T>(Comparison<T?> comparison) {
        if (comparison == null) throw new ArgumentNullException(nameof(comparison));

        return new ComparisonComparer<T>(comparison);
    }

    /// <summary>Given an IComparer on TKey, returns an IComparer on key-value Pairs.</summary>
    /// <typeparam name="TKey">TKey of the pairs</typeparam>
    /// <typeparam name="TValue">TValue of the apris</typeparam>
    /// <param name="keyComparer">IComparer on TKey</param>
    /// <returns>IComparer for comparing key-value pairs.</returns>
    public static IComparer<KeyValuePair<TKey, TValue>> ComparerKeyValueFromComparerKey<TKey, TValue>(IComparer<TKey> keyComparer) {
        if (keyComparer == null) throw new ArgumentNullException(nameof(keyComparer));

        return new KeyValueComparer<TKey, TValue>(keyComparer);
    }

    /// <summary>Given an IEqualityComparer on TKey, returns an IEqualityComparer on key-value Pairs.</summary>
    /// <typeparam name="TKey">TKey of the pairs</typeparam>
    /// <typeparam name="TValue">TValue of the apris</typeparam>
    /// <param name="keyEqualityComparer">IComparer on TKey</param>
    /// <returns>IEqualityComparer for comparing key-value pairs.</returns>
    public static IEqualityComparer<KeyValuePair<TKey, TValue>> EqualityComparerKeyValueFromComparerKey<TKey, TValue>(IEqualityComparer<TKey> keyEqualityComparer) {
        if (keyEqualityComparer == null) throw new ArgumentNullException(nameof(keyEqualityComparer));

        return new KeyValueEqualityComparer<TKey, TValue>(keyEqualityComparer);
    }

    /// <summary>
    ///     Given an IComparer on TKey and TValue, returns an IComparer on key-value Pairs of TKey and TValue, comparing
    ///     first keys, then values.
    /// </summary>
    /// <typeparam name="TKey">TKey of the pairs</typeparam>
    /// <typeparam name="TValue">TValue of the apris</typeparam>
    /// <param name="keyComparer">IComparer on TKey</param>
    /// <param name="valueComparer">IComparer on TValue</param>
    /// <returns>IComparer for comparing key-value pairs.</returns>
    public static IComparer<KeyValuePair<TKey, TValue>> ComparerPairFromKeyValueComparers<TKey, TValue>(IComparer<TKey> keyComparer, IComparer<TValue> valueComparer) {
        if (keyComparer == null) throw new ArgumentNullException(nameof(keyComparer));
        if (valueComparer == null) throw new ArgumentNullException(nameof(valueComparer));

        return new PairComparer<TKey, TValue>(keyComparer, valueComparer);
    }

    /// <summary>Given an Comparison on TKey, returns an IComparer on key-value Pairs.</summary>
    /// <typeparam name="TKey">TKey of the pairs</typeparam>
    /// <typeparam name="TValue">TValue of the apris</typeparam>
    /// <param name="keyComparison">Comparison delegate on TKey</param>
    /// <returns>IComparer for comparing key-value pairs.</returns>
    public static IComparer<KeyValuePair<TKey, TValue>> ComparerKeyValueFromComparisonKey<TKey, TValue>(Comparison<TKey> keyComparison) {
        if (keyComparison == null) throw new ArgumentNullException(nameof(keyComparison));

        return new ComparisonKeyValueComparer<TKey, TValue>(keyComparison);
    }

    /// <summary>
    ///     Given an element type, check that it implements IComparable&lt;T&gt; or IComparable, then returns a IComparer
    ///     that can be used to compare elements of that type.
    /// </summary>
    /// <returns>The IComparer&lt;T&gt; instance.</returns>
    /// <exception cref="InvalidOperationException">T does not implement IComparable&lt;T&gt;.</exception>
    public static IComparer<T> DefaultComparer<T>() {
        if (typeof(IComparable<T>).IsAssignableFrom(typeof(T)) ||
            typeof(IComparable).IsAssignableFrom(typeof(T))) {
            return Comparer<T>.Default;
        }

        throw new InvalidOperationException(string.Format(Strings.UncomparableType, typeof(T).FullName));
    }

    /// <summary>
    ///     Given an key and value type, check that TKey implements IComparable&lt;T&gt; or IComparable, then returns a
    ///     IComparer that can be used to compare KeyValuePairs of those types.
    /// </summary>
    /// <returns>The IComparer&lt;KeyValuePair&lt;TKey, TValue&gt;&gt; instance.</returns>
    /// <exception cref="InvalidOperationException">TKey does not implement IComparable&lt;T&gt;.</exception>
    public static IComparer<KeyValuePair<TKey, TValue>> DefaultKeyValueComparer<TKey, TValue>() {
        var keyComparer = DefaultComparer<TKey>();
        return ComparerKeyValueFromComparerKey<TKey, TValue>(keyComparer);
    }

    /// <summary>Given an IComparer on T, returns an IComparer on Zero Hash equality</summary>
    /// <typeparam name="T">T of the items</typeparam>
    /// <param name="comp">IComparer on T</param>
    /// <returns>IComparer for comparing items.</returns>
    public static IEqualityComparer<T> EqualityComparerZeroHashCode<T>(IComparer<T> comp) {
        if (comp == null) throw new ArgumentNullException(nameof(comp));

        return new ZeroHashCodeEqualityComparer<T>(comp);
    }

    #region Nested type: ComparisonComparer
    /// <summary>Class to change an Comparison&lt;T&gt; to an IComparer&lt;T&gt;.</summary>
    [Serializable]
    private class ComparisonComparer<T> : IComparer<T>
    {
        #region Fields / Properties
        private readonly Comparison<T?> comparison;
        #endregion

        #region Constructors
        public ComparisonComparer(Comparison<T?> comparison) => this.comparison = comparison;
        #endregion

        #region IComparer<T> Members
        public int Compare(T? x, T? y) => comparison(x, y);
        #endregion

        public override bool Equals(object? obj) {
            if (obj is ComparisonComparer<T> comparisonComparer) return comparison.Equals((comparisonComparer).comparison);
            return false;
        }

        public override int GetHashCode() => comparison.GetHashCode();
    }
    #endregion

    #region Nested type: ComparisonKeyValueComparer
    /// <summary>
    ///     Class to change an Comparison&lt;TKey&gt; to an IComparer&lt;KeyValuePair&lt;TKey, TValue&gt;&gt;. GetHashCode
    ///     cannot be used on this class.
    /// </summary>
    [Serializable]
    private class ComparisonKeyValueComparer<TKey, TValue> : IComparer<KeyValuePair<TKey, TValue>>
    {
        #region Fields / Properties
        private readonly Comparison<TKey> comparison;
        #endregion

        #region Constructors
        public ComparisonKeyValueComparer(Comparison<TKey> comparison) => this.comparison = comparison;
        #endregion

        #region IComparer<KeyValuePair<TKey,TValue>> Members
        public int Compare(KeyValuePair<TKey, TValue> x, KeyValuePair<TKey, TValue> y) => comparison(x.Key, y.Key);
        #endregion

        public override bool Equals(object? obj) {
            if (obj is ComparisonKeyValueComparer<TKey, TValue> comparisonKeyValueComparer) return comparison.Equals((comparisonKeyValueComparer).comparison);
            return false;
        }

        public override int GetHashCode() => comparison.GetHashCode();
    }
    #endregion

    #region Nested type: IdentityComparer
    /// <summary>
    ///     A class, implementing IEqualityComparer&lt;T&gt;, that compares objects for object identity only. Only Equals
    ///     and GetHashCode can be used; this implementation is not appropriate for ordering.
    /// </summary>
    [Serializable]
    private class IdentityComparer<T> : IEqualityComparer<T>
        where T : class
    {
        #region IEqualityComparer<T> Members
        public bool Equals(T? x, T? y) => x == y;

        public int GetHashCode(T obj) => RuntimeHelpers.GetHashCode(obj);
        #endregion

        // For comparing two IComparers to see if they compare the same thing.
        public override bool Equals(object? obj) => obj != null && obj is IdentityComparer<T>;

        // For comparing two IComparers to see if they compare the same thing.
        public override int GetHashCode() => 0x7143DDEF;
    }
    #endregion

    #region Nested type: KeyValueComparer
    /// <summary>
    ///     Class to change an IComparer&lt;TKey&gt; to an IComparer&lt;KeyValuePair&lt;TKey, TValue&gt;&gt; Only the keys
    ///     are compared.
    /// </summary>
    [Serializable]
    private class KeyValueComparer<TKey, TValue> : IComparer<KeyValuePair<TKey, TValue>>
    {
        #region Fields / Properties
        private readonly IComparer<TKey> keyComparer;
        #endregion

        #region Constructors
        public KeyValueComparer(IComparer<TKey> keyComparer) => this.keyComparer = keyComparer;
        #endregion

        #region IComparer<KeyValuePair<TKey,TValue>> Members
        public int Compare(KeyValuePair<TKey, TValue> x, KeyValuePair<TKey, TValue> y) => keyComparer.Compare(x.Key, y.Key);
        #endregion

        public override bool Equals(object? obj) {
            if (obj is KeyValueComparer<TKey, TValue> keyValueComparer) return Equals(keyComparer, (keyValueComparer).keyComparer);
            return false;
        }

        public override int GetHashCode() => keyComparer.GetHashCode();
    }
    #endregion

    #region Nested type: KeyValueEqualityComparer
    /// <summary>
    ///     Class to change an IEqualityComparer&lt;TKey&gt; to an IEqualityComparer&lt;KeyValuePair&lt;TKey, TValue&gt;
    ///     &gt; Only the keys are compared.
    /// </summary>
    [Serializable]
    private class KeyValueEqualityComparer<TKey, TValue> : IEqualityComparer<KeyValuePair<TKey, TValue>>
    {
        #region Fields / Properties
        private readonly IEqualityComparer<TKey> keyEqualityComparer;
        #endregion

        #region Constructors
        public KeyValueEqualityComparer(IEqualityComparer<TKey> keyEqualityComparer) => this.keyEqualityComparer = keyEqualityComparer;
        #endregion

        #region IEqualityComparer<KeyValuePair<TKey,TValue>> Members
        public bool Equals(KeyValuePair<TKey, TValue> x, KeyValuePair<TKey, TValue> y) => keyEqualityComparer.Equals(x.Key, y.Key);

        public int GetHashCode(KeyValuePair<TKey, TValue> obj) => CollectionHelper.GetHashCode(obj.Key, keyEqualityComparer);
        #endregion

        public override bool Equals(object? obj) {
            if (obj is KeyValueEqualityComparer<TKey, TValue> keyValueEqualityComparer) return Equals(keyEqualityComparer, (keyValueEqualityComparer).keyEqualityComparer);
            return false;
        }

        public override int GetHashCode() => keyEqualityComparer.GetHashCode();
    }
    #endregion

    #region Nested type: PairComparer
    /// <summary>
    ///     Class to change an IComparer&lt;TKey&gt; and IComparer&lt;TValue&gt; to an IComparer&lt;KeyValuePair&lt;TKey,
    ///     TValue&gt;&gt; Keys are compared, followed by values.
    /// </summary>
    [Serializable]
    private class PairComparer<TKey, TValue> : IComparer<KeyValuePair<TKey, TValue>>
    {
        #region Fields / Properties
        private readonly IComparer<TKey> keyComparer;
        private readonly IComparer<TValue> valueComparer;
        #endregion

        #region Constructors
        public PairComparer(IComparer<TKey> keyComparer, IComparer<TValue> valueComparer) {
            this.keyComparer = keyComparer;
            this.valueComparer = valueComparer;
        }
        #endregion

        #region IComparer<KeyValuePair<TKey,TValue>> Members
        public int Compare(KeyValuePair<TKey, TValue> x, KeyValuePair<TKey, TValue> y) {
            var keyCompare = keyComparer.Compare(x.Key, y.Key);

            if (keyCompare == 0) return valueComparer.Compare(x.Value, y.Value);
            return keyCompare;
        }
        #endregion

        public override bool Equals(object? obj) {
            if (obj is PairComparer<TKey, TValue> pairComparer) {
                return Equals(keyComparer, (pairComparer).keyComparer) &&
                       Equals(valueComparer, (pairComparer).valueComparer);
            }

            return false;
        }

        public override int GetHashCode() => keyComparer.GetHashCode() ^ valueComparer.GetHashCode();
    }
    #endregion

    #region Nested type: ReverseComparerClass
    /// <summary>An IComparer instance that can be used to reverse the sense of a wrapped IComparer instance.</summary>
    [Serializable]
    private class ReverseComparerClass<T> : IComparer<T>
    {
        #region Fields / Properties
        private readonly IComparer<T> comparer;
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        /// <param name="comparer">The comparer to reverse.</param>
        public ReverseComparerClass(IComparer<T> comparer) => this.comparer = comparer;
        #endregion

        #region IComparer<T> Members
        public int Compare(T? x, T? y) => -comparer.Compare(x, y);
        #endregion

        // For comparing this comparer to others.

        public override bool Equals(object? obj) {
            if (obj is ReverseComparerClass<T> reverseComparerClass) return comparer.Equals((reverseComparerClass).comparer);
            return false;
        }

        public override int GetHashCode() => comparer.GetHashCode();
    }
    #endregion

    #region Nested type: ZeroHashCodeEqualityComparer
    /// <summary>
    ///     An equalityComparer compatible with a given comparer. All hash codes are 0, meaning that anything based on hash
    ///     codes will be quite inefficient.
    ///     <para>
    ///         <b>Note: this will give a new EqualityComparer each time created!</b>
    ///     </para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class ZeroHashCodeEqualityComparer<T> : IEqualityComparer<T>
    {
        #region Fields / Properties
        private readonly IComparer<T> comparer;
        #endregion

        #region Constructors
        /// <summary>
        ///     Create a trivial <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> compatible with the
        ///     <see cref="T:System.Collections.Generic.IComparer`1" /> <c>comparer</c>
        /// </summary>
        /// <param name="comparer"></param>
        public ZeroHashCodeEqualityComparer(IComparer<T> comparer) {
            this.comparer = comparer ?? throw new ArgumentNullException(nameof(comparer));
        }
        #endregion

        #region IEqualityComparer<T> Members
        /// <summary>A trivial, inefficient hash fuction. Compatible with any equality relation.</summary>
        /// <param name="item"></param>
        /// <returns>0</returns>
        public int GetHashCode(T item) => 0;
        /// <summary>Equality of two items as defined by the comparer.</summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <returns></returns>
        public bool Equals(T? item1, T? item2) => comparer.Compare(item1, item2) == 0;
        #endregion
    }
    #endregion
}