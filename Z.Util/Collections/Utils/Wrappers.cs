﻿//******************************
// Written by Peter Golde
// Copyright (c) 2004-2005, Wintellect
//
// Use and restribution of this code is subject to the license agreement
// contained in the file "License.txt" accompanying this file.
// Sep 9, 2007
//******************************

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Z.Collections.Bases;
using Z.Extensions;
#nullable disable
#pragma warning disable
// Everything should be CLS compliant.
// [assembly: CLSCompliant(true)]
namespace Z.Collections.Helpers
{
    /// <summary>
    ///     Algorithms contains a number of static methods that implement algorithms that work on collections. Most of the
    ///     methods deal with the standard generic collection interfaces such as IEnumerable&lt;T&gt;, ICollection&lt;T&gt; and
    ///     IList&lt;T&gt;.
    /// </summary>
    public static class Wrappers
    {
        #region Collection wrappers
        /// <summary>
        ///     The class that is used to implement IList&lt;T&gt; to view an array in a read-write way. Insertions cause the
        ///     last item in the array to fall off, deletions replace the last item with the default value.
        /// </summary>
        [Serializable]
        private class ArrayWrapper<T> : ListBase<T>, IList
        {
            #region Fields / Properties
            private T[] wrappedArray;
            /// <inheritdoc />
            public override T this[int index] {
                get {
                    if (index < 0 || index >= wrappedArray.Length) throw new ArgumentOutOfRangeException("index");

                    return wrappedArray[index];
                }
                set {
                    if (index < 0 || index >= wrappedArray.Length) throw new ArgumentOutOfRangeException("index");

                    wrappedArray[index] = value;
                }
            }
            #endregion

            #region Constructors
            /// <summary>Create a list wrapper object on an array.</summary>
            /// <param name="wrappedArray">Array to wrap.</param>
            public ArrayWrapper(T[] wrappedArray) => this.wrappedArray = wrappedArray;
            #endregion

            #region IList Members
            /// <inheritdoc />
            public override int Count => wrappedArray.Length;

            /// <summary>Return true, to indicate that the list is fixed size.</summary>
            bool IList.IsFixedSize => true;
            /// <inheritdoc />
            public override void Clear() {
                var count = wrappedArray.Length;
                for (var i = 0; i < count; ++i) wrappedArray[i] = default;
            }
            /// <inheritdoc />
            public override void RemoveAt(int index) {
                if (index < 0 || index >= wrappedArray.Length) throw new ArgumentOutOfRangeException("index");

                if (index < wrappedArray.Length - 1) Array.Copy(wrappedArray, index + 1, wrappedArray, index, wrappedArray.Length - index - 1);
                wrappedArray[wrappedArray.Length - 1] = default;
            }

            IEnumerator IEnumerable.GetEnumerator() => ((IList) wrappedArray).GetEnumerator();
            #endregion

            /// <inheritdoc />
            public override void Insert(int index, T item) {
                if (index < 0 || index > wrappedArray.Length) throw new ArgumentOutOfRangeException("index");

                if (index + 1 < wrappedArray.Length) Array.Copy(wrappedArray, index, wrappedArray, index + 1, wrappedArray.Length - index - 1);
                if (index < wrappedArray.Length) wrappedArray[index] = item;
            }
            /// <inheritdoc />
            public override void CopyTo(T[] array, int arrayIndex) {
                if (array == null) throw new ArgumentNullException("array");
                if (array.Length < wrappedArray.Length) throw new ArgumentException("array is too short", "array");
                if (arrayIndex < 0 || arrayIndex >= array.Length) throw new ArgumentOutOfRangeException("arrayIndex");
                if (array.Length + arrayIndex < wrappedArray.Length) throw new ArgumentOutOfRangeException("arrayIndex");

                Array.Copy(wrappedArray, 0, array, arrayIndex, wrappedArray.Length);
            }
            /// <inheritdoc />
            public override IEnumerator<T> GetEnumerator() => ((IList<T>) wrappedArray).GetEnumerator();
        }

        /// <summary>
        ///     Returns class that is used to implement IList&lt;T&gt; to view an array in a read-write way. Insertions cause
        ///     the last item in the array to fall off, deletions replace the last item with the default value.
        /// </summary>
        public static IList<T> List<T>(T[] array) => array == null ? null : new ArrayWrapper<T>(array);

        /// <summary>Dictionary Wrapper</summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        private class DictionaryWrapper<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary<TKey, TValue>
        {
            #region Fields / Properties
            private readonly IEnumerable<KeyValuePair<TKey, TValue>> value;
            private IDictionary<TKey, TValue> dict;

            private IDictionary<TKey, TValue> Dict {
                get {
                    if (dict != null) return dict;
                    if (value == null) return null;
                    dict = new Dictionary<TKey, TValue>(value.Count());
                    foreach (var item in value) dict[item.Key] = item.Value;
                    return dict;
                }
                set => dict = value;
            }
            #endregion

            #region Constructors
            /// <inheritdoc />
            public DictionaryWrapper(IEnumerable<KeyValuePair<TKey, TValue>> dictionary) {
                value = dictionary;
                if (dictionary is IDictionary<TKey, TValue>) Dict = dictionary as IDictionary<TKey, TValue>;
            }
            #endregion

            #region IDictionary<TKey,TValue> Members
            /// <inheritdoc />
            public ICollection<TKey> Keys => Dict.Keys;
            /// <inheritdoc />
            public ICollection<TValue> Values => Dict.Values;
            /// <inheritdoc />
            public int Count => value.Count();
            /// <inheritdoc />
            public bool IsReadOnly => Dict.IsReadOnly;
            /// <inheritdoc />
            public TValue this[TKey key] { get => Dict[key]; set => Dict[key] = value; }
            /// <inheritdoc />
            public void Add(TKey key, TValue value) => Dict.Add(key, value);
            /// <inheritdoc />
            public bool ContainsKey(TKey key) => Dict.ContainsKey(key);
            /// <inheritdoc />
            public bool Remove(TKey key) => Dict.Remove(key);
            /// <inheritdoc />
            public bool TryGetValue(TKey key, out TValue value) => Dict.TryGetValue(key, out value);
            /// <inheritdoc />
            public void Add(KeyValuePair<TKey, TValue> item) => Dict.Add(item);
            /// <inheritdoc />
            public void Clear() => Dict.Clear();
            /// <inheritdoc />
            public bool Contains(KeyValuePair<TKey, TValue> item) => Dict.Contains(item);
            /// <inheritdoc />
            public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => Dict.CopyTo(array, arrayIndex);
            /// <inheritdoc />
            public bool Remove(KeyValuePair<TKey, TValue> item) => Dict.Remove(item);
            #endregion

            #region IEnumerable<KeyValuePair<TKey,TValue>> Members
            /// <inheritdoc />
            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => Dict.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => value.GetEnumerator();
            #endregion
        }

        /// <summary>
        ///     Returns class that is used to implement IDictionary&lt;T&gt; to view an array of KeyValuePairs in a read-write
        ///     way.
        /// </summary>
        public static IDictionary<TKey, TValue> Dictionary<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> array) => array == null ? null : new DictionaryWrapper<TKey, TValue>(array);

        /// <summary>
        ///     The class that is used to implement IList&lt;T&gt; to view a sub-range of a list. The object stores a wrapped
        ///     list, and a start/count indicating a sub-range of the list. Insertion/deletions through the sub-range view cause
        ///     the count to change also; insertions and deletions directly on the wrapped list do not.
        /// </summary>
        [Serializable]
        private class ListRange<T> : ListBase<T>, ICollection<T>
        {
            #region Fields / Properties
            private int count;
            private int start;
            private IList<T> wrappedList;

            public override T this[int index] {
                get {
                    if (index < 0 || index >= count) throw new ArgumentOutOfRangeException("index");

                    return wrappedList[start + index];
                }
                set {
                    if (index < 0 || index >= count) throw new ArgumentOutOfRangeException("index");

                    wrappedList[start + index] = value;
                }
            }
            #endregion

            #region Constructors
            /// <summary>Create a sub-range view object on the indicate part of the list.</summary>
            /// <param name="wrappedList">List to wrap.</param>
            /// <param name="start">The start index of the view in the wrapped list.</param>
            /// <param name="count">The number of items in the view.</param>
            public ListRange(IList<T> wrappedList, int start, int count) {
                this.wrappedList = wrappedList;
                this.start = start;
                this.count = count;
            }
            #endregion

            #region ICollection<T> Members
            public override int Count => Math.Min(count, wrappedList.Count - start);

            bool ICollection<T>.IsReadOnly => wrappedList.IsReadOnly;

            public override void Clear() {
                if (wrappedList.Count - start < count) count = wrappedList.Count - start;

                while (count > 0) {
                    wrappedList.RemoveAt(start + count - 1);
                    --count;
                }
            }

            public override bool Remove(T item) {
                if (wrappedList.IsReadOnly) throw new NotSupportedException(string.Format(Strings.CannotModifyCollection, "Range"));
                return base.Remove(item);
            }
            #endregion

            public override void Insert(int index, T item) {
                if (index < 0 || index > count) throw new ArgumentOutOfRangeException("index");

                wrappedList.Insert(start + index, item);
                ++count;
            }

            public override void RemoveAt(int index) {
                if (index < 0 || index >= count) throw new ArgumentOutOfRangeException("index");

                wrappedList.RemoveAt(start + index);
                --count;
            }
        }

        /// <summary>
        ///     Returns a view onto a sub-range of a list. Items from <paramref name="list" /> are not copied; the returned
        ///     IList&lt;T&gt; is simply a different view onto the same underlying items. Changes to <paramref name="list" /> are
        ///     reflected in the view, and vice versa. Insertions and deletions in the view change the size of the view, but
        ///     insertions and deletions in the underlying list do not.
        /// </summary>
        /// <remarks>
        ///     This method can be used to apply an algorithm to a portion of a list. For example:
        ///     <code>Algorithms.ReverseInPlace(Algorithms.Range(list, 3, 6))</code> will reverse the 6 items beginning at index 3.
        /// </remarks>
        /// <typeparam name="T">The type of the items in the list.</typeparam>
        /// <param name="list">The list to view.</param>
        /// <param name="start">The starting index of the view.</param>
        /// <param name="count">The number of items in the view.</param>
        /// <returns>A list that is a view onto the given sub-list. </returns>
        /// <exception cref="ArgumentNullException"><paramref name="list" /> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="start" /> or <paramref name="count" /> is negative.</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="start" /> + <paramref name="count" /> is greater than the
        ///     size of <paramref name="list" />.
        /// </exception>
        public static IList<T> Range<T>(IList<T> list, int start, int count) {
            if (list == null) throw new ArgumentOutOfRangeException("list");
            if (start < 0 || start > list.Count || start == list.Count && count != 0) throw new ArgumentOutOfRangeException("start");
            if (count < 0 || count > list.Count || count + start > list.Count) throw new ArgumentOutOfRangeException("count");

            return new ListRange<T>(list, start, count);
        }

        /// <summary>
        ///     The class that is used to implement IList&lt;T&gt; to view a sub-range of an array. The object stores a
        ///     wrapped array, and a start/count indicating a sub-range of the array. Insertion/deletions through the sub-range
        ///     view cause the count to change up to the size of the underlying array. Elements fall off the end of the underlying
        ///     array.
        /// </summary>
        [Serializable]
        private class ArrayRange<T> : ListBase<T>
        {
            #region Fields / Properties
            private int count;
            private int start;
            private T[] wrappedArray;

            public override int Count => count;

            public override T this[int index] {
                get {
                    if (index < 0 || index >= count) throw new ArgumentOutOfRangeException("index");

                    return wrappedArray[start + index];
                }
                set {
                    if (index < 0 || index >= count) throw new ArgumentOutOfRangeException("index");

                    wrappedArray[start + index] = value;
                }
            }
            #endregion

            #region Constructors
            /// <summary>Create a sub-range view object on the indicate part of the array.</summary>
            /// <param name="wrappedArray">Array to wrap.</param>
            /// <param name="start">The start index of the view in the wrapped list.</param>
            /// <param name="count">The number of items in the view.</param>
            public ArrayRange(T[] wrappedArray, int start, int count) {
                this.wrappedArray = wrappedArray;
                this.start = start;
                this.count = count;
            }
            #endregion

            public override void Clear() {
                Array.Copy(wrappedArray, start + count, wrappedArray, start, wrappedArray.Length - (start + count));
                wrappedArray.Fill(default, wrappedArray.Length - count, count);
                count = 0;
            }

            public override void Insert(int index, T item) {
                if (index < 0 || index > count) throw new ArgumentOutOfRangeException("index");

                var i = start + index;

                if (i + 1 < wrappedArray.Length) Array.Copy(wrappedArray, i, wrappedArray, i + 1, wrappedArray.Length - i - 1);
                if (i < wrappedArray.Length) wrappedArray[i] = item;

                if (start + count < wrappedArray.Length) ++count;
            }

            public override void RemoveAt(int index) {
                if (index < 0 || index >= count) throw new ArgumentOutOfRangeException("index");

                var i = start + index;

                if (i < wrappedArray.Length - 1) Array.Copy(wrappedArray, i + 1, wrappedArray, i, wrappedArray.Length - i - 1);
                wrappedArray[wrappedArray.Length - 1] = default;

                --count;
            }
        }

        /// <summary>
        ///     Returns a view onto a sub-range of an array. Items from <paramref name="array" /> are not copied; the returned
        ///     IList&lt;T&gt; is simply a different view onto the same underlying items. Changes to <paramref name="array" /> are
        ///     reflected in the view, and vice versa. Insertions and deletions in the view change the size of the view. After an
        ///     insertion, the last item in <paramref name="array" /> "falls off the end". After a deletion, the last item in array
        ///     becomes the default value (0 or null).
        /// </summary>
        /// <remarks>
        ///     This method can be used to apply an algorithm to a portion of a array. For example:
        ///     <code>Algorithms.ReverseInPlace(Algorithms.Range(array, 3, 6))</code> will reverse the 6 items beginning at index
        ///     3.
        /// </remarks>
        /// <param name="array">The array to view.</param>
        /// <param name="start">The starting index of the view.</param>
        /// <param name="count">The number of items in the view.</param>
        /// <returns>A list that is a view onto the given sub-array. </returns>
        /// <exception cref="ArgumentNullException"><paramref name="array" /> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="start" /> or <paramref name="count" /> is negative.</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     <paramref name="start" /> + <paramref name="count" /> is greater than the
        ///     size of <paramref name="array" />.
        /// </exception>
        public static IList<T> Range<T>(T[] array, int start, int count) {
            if (array == null) throw new ArgumentOutOfRangeException("array");
            if (start < 0 || start > array.Length || start == array.Length && count != 0) throw new ArgumentOutOfRangeException("start");
            if (count < 0 || count > array.Length || count + start > array.Length) throw new ArgumentOutOfRangeException("count");

            return new ArrayRange<T>(array, start, count);
        }

        /// <summary>
        ///     The read-only ICollection&lt;T&gt; implementation that is used by the ReadOnly method. Methods that modify the
        ///     collection throw a NotSupportedException, methods that don't modify are fowarded through to the wrapped collection.
        /// </summary>
        [Serializable]
        private class ReadOnlyCollection<T> : ICollection<T>
        {
            #region Fields / Properties
            private ICollection<T> wrappedCollection; // The collection we are wrapping (never null).
            #endregion

            #region Constructors
            /// <summary>Create a ReadOnlyCollection wrapped around the given collection.</summary>
            /// <param name="wrappedCollection">Collection to wrap.</param>
            public ReadOnlyCollection(ICollection<T> wrappedCollection) => this.wrappedCollection = wrappedCollection;
            #endregion

            #region ICollection<T> Members
            public int Count => wrappedCollection.Count;

            public bool IsReadOnly => true;
            public IEnumerator<T> GetEnumerator() => wrappedCollection.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) wrappedCollection).GetEnumerator();

            public bool Contains(T item) => wrappedCollection.Contains(item);

            public void CopyTo(T[] array, int arrayIndex) => wrappedCollection.CopyTo(array, arrayIndex);

            public void Add(T item) => MethodModifiesCollection();

            public void Clear() => MethodModifiesCollection();

            public bool Remove(T item) {
                MethodModifiesCollection();
                return false;
            }
            #endregion

            /// <summary>Throws an NotSupportedException stating that this collection cannot be modified.</summary>
            private void MethodModifiesCollection() => throw new NotSupportedException(string.Format(Strings.CannotModifyCollection, "read-only collection"));
        }

        /// <summary>
        ///     Returns a read-only view onto a collection. The returned ICollection&lt;T&gt; interface only allows operations
        ///     that do not change the collection: GetEnumerator, Contains, CopyTo, Count. The ReadOnly property returns false,
        ///     indicating that the collection is read-only. All other methods on the interface throw a NotSupportedException.
        /// </summary>
        /// <remarks>
        ///     The data in the underlying collection is not copied. If the underlying collection is changed, then the
        ///     read-only view also changes accordingly.
        /// </remarks>
        /// <typeparam name="T">The type of items in the collection.</typeparam>
        /// <param name="collection">The collection to wrap.</param>
        /// <returns>
        ///     A read-only view onto <paramref name="collection" />. If <paramref name="collection" /> is null, then null is
        ///     returned.
        /// </returns>
        public static ICollection<T> ReadOnly<T>(ICollection<T> collection) {
            if (collection == null) return null;
            return new ReadOnlyCollection<T>(collection);
        }

        /// <summary>
        ///     The read-only IList&lt;T&gt; implementation that is used by the ReadOnly method. Methods that modify the list
        ///     throw a NotSupportedException, methods that don't modify are fowarded through to the wrapped list.
        /// </summary>
        [Serializable]
        private class ReadOnlyList<T> : IList<T>
        {
            #region Fields / Properties
            private IList<T> wrappedList; // The list we are wrapping (never null).
            #endregion

            #region Constructors
            /// <summary>Create a ReadOnlyList wrapped around the given list.</summary>
            /// <param name="wrappedList">List to wrap.</param>
            public ReadOnlyList(IList<T> wrappedList) => this.wrappedList = wrappedList;
            #endregion

            #region IList<T> Members
            public int Count => wrappedList.Count;

            public bool IsReadOnly => true;

            public T this[int index] { get => wrappedList[index]; set => MethodModifiesCollection(); }
            public IEnumerator<T> GetEnumerator() => wrappedList.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) wrappedList).GetEnumerator();

            public int IndexOf(T item) => wrappedList.IndexOf(item);

            public bool Contains(T item) => wrappedList.Contains(item);

            public void CopyTo(T[] array, int arrayIndex) => wrappedList.CopyTo(array, arrayIndex);

            public void Add(T item) => MethodModifiesCollection();

            public void Clear() => MethodModifiesCollection();

            public void Insert(int index, T item) => MethodModifiesCollection();

            public void RemoveAt(int index) => MethodModifiesCollection();

            public bool Remove(T item) {
                MethodModifiesCollection();
                return false;
            }
            #endregion

            /// <summary>Throws an NotSupportedException stating that this collection cannot be modified.</summary>
            private void MethodModifiesCollection() => throw new NotSupportedException(string.Format(Strings.CannotModifyCollection, "read-only list"));
        }

        /// <summary>
        ///     Returns a read-only view onto a list. The returned IList&lt;T&gt; interface only allows operations that do not
        ///     change the list: GetEnumerator, Contains, CopyTo, Count, IndexOf, and the get accessor of the indexer. The
        ///     IsReadOnly property returns true, indicating that the list is read-only. All other methods on the interface throw a
        ///     NotSupportedException.
        /// </summary>
        /// <remarks>
        ///     The data in the underlying list is not copied. If the underlying list is changed, then the read-only view also
        ///     changes accordingly.
        /// </remarks>
        /// <typeparam name="T">The type of items in the list.</typeparam>
        /// <param name="list">The list to wrap.</param>
        /// <returns>
        ///     A read-only view onto <paramref name="list" />. Returns null if <paramref name="list" /> is null. If
        ///     <paramref name="list" /> is already read-only, returns <paramref name="list" />.
        /// </returns>
        public static IList<T> ReadOnly<T>(IList<T> list) {
            if (list == null) return null;
            if (list.IsReadOnly) return list;
            return new ReadOnlyList<T>(list);
        }

        /// <summary>The private class that implements a read-only wrapped for IDictionary &lt;TKey,TValue&gt;.</summary>
        [Serializable]
        private class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
        {
            #region Fields / Properties
            // The dictionary that is wrapped
            private IDictionary<TKey, TValue> wrappedDictionary;
            #endregion

            #region Constructors
            /// <summary>Create a read-only dictionary wrapped around the given dictionary.</summary>
            /// <param name="wrappedDictionary">The IDictionary&lt;TKey,TValue&gt; to wrap.</param>
            public ReadOnlyDictionary(IDictionary<TKey, TValue> wrappedDictionary) => this.wrappedDictionary = wrappedDictionary;
            #endregion

            #region IDictionary<TKey,TValue> Members
            public ICollection<TKey> Keys => ReadOnly(wrappedDictionary.Keys);

            public ICollection<TValue> Values => ReadOnly(wrappedDictionary.Values);

            public int Count => wrappedDictionary.Count;

            public bool IsReadOnly => true;

            public TValue this[TKey key] { get => wrappedDictionary[key]; set => MethodModifiesCollection(); }
            public void Add(TKey key, TValue value) => MethodModifiesCollection();

            public bool ContainsKey(TKey key) => wrappedDictionary.ContainsKey(key);

            public bool Remove(TKey key) {
                MethodModifiesCollection();
                return false; // never reached
            }

            public bool TryGetValue(TKey key, out TValue value) => wrappedDictionary.TryGetValue(key, out value);

            public void Add(KeyValuePair<TKey, TValue> item) => MethodModifiesCollection();

            public void Clear() => MethodModifiesCollection();

            public bool Contains(KeyValuePair<TKey, TValue> item) => wrappedDictionary.Contains(item);

            public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) => wrappedDictionary.CopyTo(array, arrayIndex);

            public bool Remove(KeyValuePair<TKey, TValue> item) {
                MethodModifiesCollection();
                return false; // never reached
            }

            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => wrappedDictionary.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) wrappedDictionary).GetEnumerator();
            #endregion

            /// <summary>Throws an NotSupportedException stating that this collection cannot be modified.</summary>
            private void MethodModifiesCollection() => throw new NotSupportedException(string.Format(Strings.CannotModifyCollection, "read-only dictionary"));
        }

        /// <summary>
        ///     Returns a read-only view onto a dictionary. The returned IDictionary&lt;TKey,TValue&gt; interface only allows
        ///     operations that do not change the dictionary. The IsReadOnly property returns true, indicating that the dictionary
        ///     is read-only. All other methods on the interface throw a NotSupportedException.
        /// </summary>
        /// <remarks>
        ///     The data in the underlying dictionary is not copied. If the underlying dictionary is changed, then the
        ///     read-only view also changes accordingly.
        /// </remarks>
        /// <param name="dictionary">The dictionary to wrap.</param>
        /// <returns>
        ///     A read-only view onto <paramref name="dictionary" />. Returns null if <paramref name="dictionary" /> is null.
        ///     If <paramref name="dictionary" /> is already read-only, returns <paramref name="dictionary" />.
        /// </returns>
        public static IDictionary<TKey, TValue> ReadOnly<TKey, TValue>(IDictionary<TKey, TValue> dictionary) {
            if (dictionary == null) return null;
            if (dictionary.IsReadOnly) return dictionary;
            return new ReadOnlyDictionary<TKey, TValue>(dictionary);
        }

        /// <summary>The class that provides a typed IEnumerator&lt;T&gt; view onto an untyped IEnumerator interface.</summary>
        [Serializable]
        private class TypedEnumerator<T> : IEnumerator<T>
        {
            #region Fields / Properties
            private IEnumerator wrappedEnumerator;
            #endregion

            #region Constructors
            /// <summary>Create a typed IEnumerator&lt;T&gt; view onto an untyped IEnumerator interface</summary>
            /// <param name="wrappedEnumerator">IEnumerator to wrap.</param>
            public TypedEnumerator(IEnumerator wrappedEnumerator) => this.wrappedEnumerator = wrappedEnumerator;
            #endregion

            #region IEnumerator<T> Members
            T IEnumerator<T>.Current => (T) wrappedEnumerator.Current;

            object IEnumerator.Current => wrappedEnumerator.Current;

            void IDisposable.Dispose() {
                if (wrappedEnumerator is IDisposable) ((IDisposable) wrappedEnumerator).Dispose();
            }

            bool IEnumerator.MoveNext() => wrappedEnumerator.MoveNext();

            void IEnumerator.Reset() => wrappedEnumerator.Reset();
            #endregion
        }

        /// <summary>Returns a typed IEnumerator&lt;T&gt; from untyped IEnumerator interface.</summary>
        public static IEnumerator<T> TypedAs<T>(IEnumerator enumerator) => enumerator == null ? null : new TypedEnumerator<T>(enumerator);

        /// <summary>The class that provides a typed IEnumerable&lt;T&gt; view onto an untyped IEnumerable interface.</summary>
        [Serializable]
        private class TypedEnumerable<T> : IEnumerable<T>
        {
            #region Fields / Properties
            private IEnumerable wrappedEnumerable;
            #endregion

            #region Constructors
            /// <summary>Create a typed IEnumerable&lt;T&gt; view onto an untyped IEnumerable interface.</summary>
            /// <param name="wrappedEnumerable">IEnumerable interface to wrap.</param>
            public TypedEnumerable(IEnumerable wrappedEnumerable) => this.wrappedEnumerable = wrappedEnumerable;
            #endregion

            #region IEnumerable<T> Members
            /// <inheritdoc />
            public IEnumerator<T> GetEnumerator() => new TypedEnumerator<T>(wrappedEnumerable.GetEnumerator());

            IEnumerator IEnumerable.GetEnumerator() => wrappedEnumerable.GetEnumerator();
            #endregion
        }

        /// <summary>Returns a typed IEnumerable&lt;T&gt; from untyped IEnumerable interface.</summary>
        public static IEnumerable<T> TypedAs<T>(IEnumerable enumerator) => enumerator == null ? null : new TypedEnumerable<T>(enumerator);

        /// <summary>
        ///     The class that provides a typed ICollection&lt;T&gt; view onto an untyped ICollection interface. The
        ///     ICollection&lt;T&gt; is read-only.
        /// </summary>
        [Serializable]
        private class TypedCollection<T> : ICollection<T>
        {
            #region Fields / Properties
            private ICollection wrappedCollection;
            #endregion

            #region Constructors
            /// <summary>Create a typed ICollection&lt;T&gt; view onto an untyped ICollection interface.</summary>
            /// <param name="wrappedCollection">ICollection interface to wrap.</param>
            public TypedCollection(ICollection wrappedCollection) => this.wrappedCollection = wrappedCollection;
            #endregion

            #region ICollection<T> Members
            public int Count => wrappedCollection.Count;

            public bool IsReadOnly => true;
            public void Add(T item) => MethodModifiesCollection();

            public void Clear() => MethodModifiesCollection();

            public bool Remove(T item) {
                MethodModifiesCollection();
                return false;
            }

            public bool Contains(T item) {
                IEqualityComparer<T> equalityComparer = EqualityComparer<T>.Default;
                foreach (var obj in wrappedCollection)
                    if (obj is T && equalityComparer.Equals(item, (T) obj))
                        return true;
                return false;
            }

            public void CopyTo(T[] array, int arrayIndex) => wrappedCollection.CopyTo(array, arrayIndex);

            public IEnumerator<T> GetEnumerator() => new TypedEnumerator<T>(wrappedCollection.GetEnumerator());

            IEnumerator IEnumerable.GetEnumerator() => wrappedCollection.GetEnumerator();
            #endregion

            /// <summary>Throws an NotSupportedException stating that this collection cannot be modified.</summary>
            private void MethodModifiesCollection() => throw new NotSupportedException(string.Format(Strings.CannotModifyCollection, "strongly-typed Collection"));
        }

        /// <summary>
        ///     Given a non-generic ICollection interface, wrap a generic ICollection&lt;T&gt; interface around it. The
        ///     generic interface will enumerate the same objects as the underlying non-generic collection, but can be used in
        ///     places that require a generic interface. The underlying non-generic collection must contain only items that are of
        ///     type T or a type derived from it. This method is useful when interfacing older, non-generic collections to newer
        ///     code that uses generic interfaces.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Some collections implement both generic and non-generic interfaces. For efficiency, this method will first
        ///         attempt to cast <paramref name="untypedCollection" /> to ICollection&lt;T&gt;. If that succeeds, it is
        ///         returned; otherwise, a wrapper object is created.
        ///     </para>
        ///     <para>
        ///         Unlike the generic interface, the non-generic ICollection interfaces does not contain methods for adding or
        ///         removing items from the collection. For this reason, the returned ICollection&lt;T&gt; will be read-only.
        ///     </para>
        /// </remarks>
        /// <typeparam name="T">The item type of the wrapper collection.</typeparam>
        /// <param name="untypedCollection">
        ///     An untyped collection. This collection should only contain items of type T/> or a type
        ///     derived from it.
        /// </param>
        /// <returns>
        ///     A generic ICollection&lt;T&gt; wrapper around <paramref name="untypedCollection" />. If
        ///     <paramref name="untypedCollection" /> is null, then null is returned.
        /// </returns>
        public static ICollection<T> TypedAs<T>(ICollection untypedCollection) {
            if (untypedCollection == null) return null;
            if (untypedCollection is ICollection<T>) return (ICollection<T>) untypedCollection;
            return new TypedCollection<T>(untypedCollection);
        }

        /// <summary>The class used to create a typed IList&lt;T&gt; view onto an untype IList interface.</summary>
        [Serializable]
        private class TypedList<T> : IList<T>
        {
            #region Fields / Properties
            private IList wrappedList;
            #endregion

            #region Constructors
            /// <summary>Create a typed IList&lt;T&gt; view onto an untype IList interface.</summary>
            /// <param name="wrappedList">The IList to wrap.</param>
            public TypedList(IList wrappedList) => this.wrappedList = wrappedList;
            #endregion

            #region IList<T> Members
            public int Count => wrappedList.Count;

            public bool IsReadOnly => wrappedList.IsReadOnly;

            public T this[int index] { get => (T) wrappedList[index]; set => wrappedList[index] = value; }
            public IEnumerator<T> GetEnumerator() => new TypedEnumerator<T>(wrappedList.GetEnumerator());

            IEnumerator IEnumerable.GetEnumerator() => wrappedList.GetEnumerator();

            public int IndexOf(T item) => wrappedList.IndexOf(item);

            public void Insert(int index, T item) => wrappedList.Insert(index, item);

            public void RemoveAt(int index) => wrappedList.RemoveAt(index);

            public void Add(T item) => wrappedList.Add(item);

            public void Clear() => wrappedList.Clear();

            public bool Contains(T item) => wrappedList.Contains(item);

            public void CopyTo(T[] array, int arrayIndex) => wrappedList.CopyTo(array, arrayIndex);

            public bool Remove(T item) {
                if (wrappedList.Contains(item)) {
                    wrappedList.Remove(item);
                    return true;
                }
                return false;
            }
            #endregion
        }

        /// <summary>
        ///     Given a non-generic IList interface, wrap a generic IList&lt;T&gt; interface around it. The generic interface
        ///     will enumerate the same objects as the underlying non-generic list, but can be used in places that require a
        ///     generic interface. The underlying non-generic list must contain only items that are of type T/> or a type derived
        ///     from it. This method is useful when interfacing older, non-generic lists to newer code that uses generic
        ///     interfaces.
        /// </summary>
        /// <remarks>
        ///     Some collections implement both generic and non-generic interfaces. For efficiency, this method will first
        ///     attempt to cast <paramref name="untypedList" /> to IList&lt;T&gt;. If that succeeds, it is returned; otherwise, a
        ///     wrapper object is created.
        /// </remarks>
        /// <typeparam name="T">The item type of the wrapper list.</typeparam>
        /// <param name="untypedList">An untyped list. This list should only contain items of type T or a type derived from it.</param>
        /// <returns>A generic IList&lt;T&gt; wrapper around untypedlist/>. If untypedlist is null, then null is returned.</returns>
        public static IList<T> TypedAs<T>(IList untypedList) {
            if (untypedList == null) return null;
            if (untypedList is IList<T>) return (IList<T>) untypedList;
            return new TypedList<T>(untypedList);
        }

        /// <summary>The class that is used to provide an untyped ICollection view onto a typed ICollection&lt;T&gt; interface.</summary>
        [Serializable]
        private class UntypedCollection<T> : ICollection
        {
            #region Fields / Properties
            private ICollection<T> wrappedCollection;
            #endregion

            #region Constructors
            /// <summary>Create an untyped ICollection view onto a typed ICollection&lt;T&gt; interface.</summary>
            /// <param name="wrappedCollection">The ICollection&lt;T&gt; to wrap.</param>
            public UntypedCollection(ICollection<T> wrappedCollection) => this.wrappedCollection = wrappedCollection;
            #endregion

            #region ICollection Members
            public int Count => wrappedCollection.Count;

            public bool IsSynchronized => false;

            public object SyncRoot => this;
            public void CopyTo(Array array, int index) {
                if (array == null) throw new ArgumentNullException("array");

                var i = 0;
                var count = wrappedCollection.Count;

                if (index < 0) throw new ArgumentOutOfRangeException("index", index, Strings.ArgMustNotBeNegative);
                if (index >= array.Length || count > array.Length - index) throw new ArgumentException("index", Strings.ArrayTooSmall);

                foreach (var item in wrappedCollection) {
                    if (i >= count) break;

                    array.SetValue(item, index);
                    ++index;
                    ++i;
                }
            }

            public IEnumerator GetEnumerator() => ((IEnumerable) wrappedCollection).GetEnumerator();
            #endregion
        }

        /// <summary>
        ///     Given a generic ICollection&lt;T&gt; interface, wrap a non-generic (untyped) ICollection interface around it.
        ///     The non-generic interface will contain the same objects as the underlying generic collection, but can be used in
        ///     places that require a non-generic interface. This method is useful when interfacing generic interfaces with older
        ///     code that uses non-generic interfaces.
        /// </summary>
        /// <remarks>
        ///     Many generic collections already implement the non-generic interfaces directly. This method will first attempt
        ///     to simply cast <paramref name="typedCollection" /> to ICollection. If that succeeds, it is returned; if it fails,
        ///     then a wrapper object is created.
        /// </remarks>
        /// <typeparam name="T">The item type of the underlying collection.</typeparam>
        /// <param name="typedCollection">A typed collection to wrap.</param>
        /// <returns>
        ///     A non-generic ICollection wrapper around <paramref name="typedCollection" />. If
        ///     <paramref name="typedCollection" /> is null, then null is returned.
        /// </returns>
        public static ICollection Untyped<T>(ICollection<T> typedCollection) {
            if (typedCollection == null) return null;
            if (typedCollection is ICollection) return (ICollection) typedCollection;
            return new UntypedCollection<T>(typedCollection);
        }

        /// <summary>The class that implements a non-generic IList wrapper around a generic IList&lt;T&gt; interface.</summary>
        [Serializable]
        private class UntypedList<T> : IList
        {
            #region Fields / Properties
            private IList<T> wrappedList;
            #endregion

            #region Constructors
            /// <summary>Create a non-generic IList wrapper around a generic IList&lt;T&gt; interface.</summary>
            /// <param name="wrappedList">The IList&lt;T&gt; interface to wrap.</param>
            public UntypedList(IList<T> wrappedList) => this.wrappedList = wrappedList;
            #endregion

            #region IList Members
            public bool IsFixedSize => false;

            public bool IsReadOnly => wrappedList.IsReadOnly;

            public int Count => wrappedList.Count;

            public bool IsSynchronized => false;

            public object SyncRoot => this;

            public object this[int index] { get => wrappedList[index]; set => wrappedList[index] = ConvertToItemType("value", value); }
            public int Add(object value) {
                // We assume that Add always adds to the end. Is this true?
                wrappedList.Add(ConvertToItemType("value", value));
                return wrappedList.Count - 1;
            }

            public void Clear() => wrappedList.Clear();

            public bool Contains(object value) {
                if (value is T) return wrappedList.Contains((T) value);
                return false;
            }

            public int IndexOf(object value) {
                if (value is T) return wrappedList.IndexOf((T) value);
                return -1;
            }

            public void Insert(int index, object value) => wrappedList.Insert(index, ConvertToItemType("value", value));

            public void Remove(object value) {
                if (value is T) wrappedList.Remove((T) value);
            }

            public void RemoveAt(int index) => wrappedList.RemoveAt(index);

            public void CopyTo(Array array, int index) {
                if (array == null) throw new ArgumentNullException("array");

                var i = 0;
                var count = wrappedList.Count;

                if (index < 0) throw new ArgumentOutOfRangeException("index", index, Strings.ArgMustNotBeNegative);
                if (index >= array.Length || count > array.Length - index) throw new ArgumentException("index", Strings.ArrayTooSmall);

                foreach (var item in wrappedList) {
                    if (i >= count) break;

                    array.SetValue(item, index);
                    ++index;
                    ++i;
                }
            }

            public IEnumerator GetEnumerator() => ((IEnumerable) wrappedList).GetEnumerator();
            #endregion

            /// <summary>Convert the given parameter to T. Throw an ArgumentException if it isn't.</summary>
            /// <param name="name">parameter name</param>
            /// <param name="value">parameter value</param>
            private T ConvertToItemType(string name, object value) {
                try {
                    return (T) value;
                } catch (InvalidCastException) {
                    throw new ArgumentException(string.Format(Strings.WrongType, value, typeof(T)), name);
                }
            }
        }

        /// <summary>
        ///     Given a generic IList&lt;T&gt; interface, wrap a non-generic (untyped) IList interface around it. The
        ///     non-generic interface will contain the same objects as the underlying generic list, but can be used in places that
        ///     require a non-generic interface. This method is useful when interfacing generic interfaces with older code that
        ///     uses non-generic interfaces.
        /// </summary>
        /// <remarks>
        ///     Many generic collections already implement the non-generic interfaces directly. This method will first attempt
        ///     to simply cast <paramref name="typedList" /> to IList. If that succeeds, it is returned; if it fails, then a
        ///     wrapper object is created.
        /// </remarks>
        /// <typeparam name="T">The item type of the underlying list.</typeparam>
        /// <param name="typedList">A typed list to wrap.</param>
        /// <returns>
        ///     A non-generic IList wrapper around <paramref name="typedList" />. If <paramref name="typedList" /> is null,
        ///     then null is returned.
        /// </returns>
        public static IList Untyped<T>(IList<T> typedList) {
            if (typedList == null) return null;
            if (typedList is IList) return (IList) typedList;
            return new UntypedList<T>(typedList);
        }

        /// <summary>
        ///     <para>
        ///         Creates a read-write IList&lt;T&gt; wrapper around an array. When an array is implicitely converted to an
        ///         IList&lt;T&gt;, changes to the items in the array cannot be made through the interface. This method creates a
        ///         read-write IList&lt;T&gt; wrapper on an array that can be used to make changes to the array.
        ///     </para>
        ///     <para>
        ///         Use this method when you need to pass an array to an algorithms that takes an IList&lt;T&gt; and that tries
        ///         to modify items in the list. Algorithms in this class generally do not need this method, since they have been
        ///         design to operate on arrays even when they are passed as an IList&lt;T&gt;.
        ///     </para>
        /// </summary>
        /// <remarks>
        ///     Since arrays cannot be resized, inserting an item causes the last item in the array to be automatically
        ///     removed. Removing an item causes the last item in the array to be replaced with a default value (0 or null).
        ///     Clearing the list causes all the items to be replaced with a default value.
        /// </remarks>
        /// <param name="array">The array to wrap.</param>
        /// <returns>An IList&lt;T&gt; wrapper onto <paramref name="array" />.</returns>
        public static IList<T> ReadWriteList<T>(T[] array) {
            if (array == null) throw new ArgumentNullException("array");

            return new ArrayWrapper<T>(array);
        }

        /// <summary>Wrap an enumerable so that clients can't get to the underlying implementation via a down-cast.</summary>
        [Serializable]
        private class WrapEnumerable<T> : IEnumerable<T>
        {
            #region Fields / Properties
            private IEnumerable<T> wrapped;
            #endregion

            #region Constructors
            /// <summary>Create the wrapper around an enumerable.</summary>
            /// <param name="wrapped">IEnumerable to wrap.</param>
            public WrapEnumerable(IEnumerable<T> wrapped) => this.wrapped = wrapped;
            #endregion

            #region IEnumerable<T> Members
            public IEnumerator<T> GetEnumerator() => wrapped.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) wrapped).GetEnumerator();
            #endregion
        }

        /// <summary>Wrap an enumerable so that clients can't get to the underlying implementation via a down-case</summary>
        /// <param name="wrapped">Enumerable to wrap.</param>
        /// <returns>A wrapper around the enumerable.</returns>
        public static IEnumerable<T> CreateEnumerableWrapper<T>(IEnumerable<T> wrapped) => new WrapEnumerable<T>(wrapped);
        #endregion Collection wrappers
    }
}