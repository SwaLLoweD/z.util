﻿/*
Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>
Copyright (c) 2003-2006 Niels Kokholm and Peter Sestoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using Z.Collections.Bases;

#nullable disable
#pragma warning disable
namespace Z.Collections.Generic;

/// <summary>
///     A list collection based on a plain dynamic array data structure. Expansion of the internal array is performed by
///     doubling on demand. The internal array is only shrinked by the Clear method.
///     <i>
///         When the FIFO property is set to false this class works fine as a stack of T. When the FIFO property is set to
///         true the class will function as a (FIFO) queue but very inefficiently, use a LinkedList (
///         <see cref="T:C5.LinkedList`1" />) instead.
///     </i>
/// </summary>
public class HashLinkedList<T> : ListBase<T>, IList<T>
{
    #region Fields / Properties
    /// <summary></summary>
    /// <value>Offset for this list view or 0 for a underlying list.</value>
    public virtual int Offset => (int)offset;
    /// <summary>
    ///     Since <code>Add(T item)</code> always add at the end of the list, this describes if list has FIFO or LIFO
    ///     semantics.
    /// </summary>
    /// <value>
    ///     True if the <code>Remove()</code> operation removes from the start of the list, false if it removes from the
    ///     end. The default for a new array list is false.
    /// </value>
    public virtual bool FIFO {
        get => fIFO;
        set {
            updatecheck();
            fIFO = value;
        }
    }
    #endregion

    #region Constructors
    /// <summary>Create an array list with default item equalityComparer and initial capacity 8 items.</summary>
    public HashLinkedList(IEnumerable<T> collection, IEqualityComparer<T> itemequalityComparer = null)
        : this(itemequalityComparer) {
        if (collection == null) throw new ArgumentNullException("collection");
        foreach (var item in collection) Add(item);
    }

    /// <summary>Create a linked list with en external item equalityComparer</summary>
    /// <param name="itemequalityComparer">The external equalitySCG.Comparer</param>
    public HashLinkedList(IEqualityComparer<T> itemequalityComparer = null) {
        this.itemequalityComparer = itemequalityComparer ?? EqualityComparer<T>.Default;
        offset = 0;
        size = stamp = 0;
        startsentinel = new Node(default);
        endsentinel = new Node(default);
        startsentinel.next = endsentinel;
        endsentinel.prev = startsentinel;
        dict = new Dictionary<T, Node>(this.itemequalityComparer);
    }
    #endregion

    #region IList<T> Members
    /// <summary></summary>
    /// <value>The number of items in this collection</value>
    public override int Count => size;
    /// <summary>
    ///     On this list, this indexer is read/write. <exception cref="IndexOutOfRangeException" /> if i is negative or
    ///     &gt;= the size of the collection.
    /// </summary>
    /// <value>The i'th item of this list.</value>
    /// <param name="index">The index of the item to fetch or store.</param>
    public override T this[int index] {
        get => get(index).item;
        set {
            updatecheck();
            var n = get(index);
            //
            var item = n.item;

            if (itemequalityComparer.Equals(value, item)) {
                n.item = value;
                dict[value] = n; //.Update(value, n);
            } else if (!dict.ContainsKey(value)) {
                dict.Remove(item);
                n.item = value;
            } else {
                throw new ArgumentException("Item already in indexed list");
            }
        }
    }

    /// <summary>Searches for an item in the list going forwrds from the start.</summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>Index of item from start.</returns>
    public override int IndexOf(T item) {
        Node node;
        if (!dict.ContainsKey(item)) return ~size;
        node = startsentinel.next;
        var index = 0;
        if (find(item, ref node, ref index)) return index;
        return ~size;
    }

    /// <summary>
    ///     Insert an item at a specific index location in this list. <exception cref="IndexOutOfRangeException" /> if i
    ///     is negative or &gt; the size of the collection.
    /// </summary>
    /// <param name="i">The index at which to insert.</param>
    /// <param name="item">The item to insert.</param>
    public override void Insert(int i, T item) {
        updatecheck();
        insert(i, i == size ? endsentinel : get(i), item);
    }

    /// <summary>
    ///     Remove the item at a specific position of the list. <exception cref="IndexOutOfRangeException" /> if i is
    ///     negative or &gt;= the size of the collection.
    /// </summary>
    /// <param name="i">The index of the item to remove.</param>
    /// <returns>The removed item.</returns>
    public override void RemoveAt(int i) {
        updatecheck();
        var retval = remove(get(i), i);
        dict.Remove(retval);
    }

    /// <summary>
    ///     Remove a particular item from this collection. Since the collection has bag semantics only one copy equivalent
    ///     to the supplied item is removed.
    /// </summary>
    /// <param name="item">The value to remove.</param>
    /// <returns>True if the item was found (and removed).</returns>
    public override bool Remove(T item) {
        updatecheck();
        var i = 0;
        if (!dict.ContainsKey(item)) return false;
        var node = dict[item];
        if (!dict.Remove(item)) return false;
        var removeditem = remove(node, i);
        return true;
    }

    /// <summary>Remove all items from this collection.</summary>
    public override void Clear() {
        updatecheck();
        if (size == 0) return;
        var oldsize = size;
        foreach (var item in this) dict.Remove(item);
        clear();
    }

    /// <summary>Create an enumerator for the collection</summary>
    /// <returns>The enumerator</returns>
    public override IEnumerator<T> GetEnumerator() {
        var cursor = startsentinel.next;
        var enumeratorstamp = stamp;

        while (cursor != endsentinel) {
            modifycheck(enumeratorstamp);
            yield return cursor.item;
            cursor = cursor.next;
        }
    }
    #endregion

    /// <summary>
    ///     Check if this collection contains an item equivalent according to the itemequalityComparer to a particular
    ///     value. If so, update the item in the collection to with a binary copy of the supplied value. This will only update
    ///     the first mathching item.
    /// </summary>
    /// <param name="item">Value to update.</param>
    /// <returns>True if the item was found and hence updated.</returns>
    public virtual bool Update(T item) => Update(item, out var olditem);

    /// <summary></summary>
    /// <param name="item"></param>
    /// <param name="olditem"></param>
    /// <returns></returns>
    public virtual bool Update(T item, out T olditem) {
        updatecheck();

        if (contains(item, out var node)) {
            olditem = node.item;
            node.item = item;
            //Avoid clinging onto a reference to olditem via dict!
            dict[item] = node;
            return true;
        }

        olditem = default;
        return false;
    }

    /// <summary>
    ///     Remove a particular item from this collection if found (only one copy). If an item was removed, report a
    ///     binary copy of the actual item removed in the argument.
    /// </summary>
    /// <param name="item">The value to remove on input.</param>
    /// <param name="removeditem">The value removed.</param>
    /// <returns>True if the item was found (and removed).</returns>
    public virtual bool Remove(T item, out T removeditem) {
        updatecheck();
        var i = 0;
        removeditem = default;

        if (!dict.ContainsKey(item)) return false;
        var node = dict[item];

        removeditem = node.item;
        remove(node, i);
        return true;
    }

    private void clear() {
        if (size == 0) return;
        endsentinel.prev = startsentinel;
        startsentinel.next = endsentinel;
        size = 0;
    }

    #region Nested type: Node
    #region Node nested class
    /// <summary>An individual cell in the linked list</summary>
    private class Node
    {
        #region Fields / Properties
        public T item;

        public Node next;
        public Node prev;
        #endregion

        #region Constructors
        internal Node(T item) => this.item = item;

        internal Node(T item, Node prev, Node next) {
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
        #endregion

        public override string ToString() => string.Format("Node: (item={0})", item);
    }
    #endregion Node nested class
    #endregion

    #region Fields
    /// <summary>The actual internal array container. Will be extended on demand.</summary>
    protected T[] array;

    /// <summary>The current stamp value</summary>
    protected int stamp;

    /// <summary>The number of items in the collection</summary>
    protected int size;

    /// <summary>The item equalityComparer of the collection</summary>
    protected readonly IEqualityComparer<T> itemequalityComparer;

    /// <summary>The underlying field of the FIFO property</summary>
    private bool fIFO;

    //Invariant:  startsentinel != null && endsentinel != null
    //If size==0: startsentinel.next == endsentinel && endsentinel.prev == startsentinel
    //Else:      startsentinel.next == First && endsentinel.prev == Last)
    /// <summary>Node to the left of first node</summary>
    private readonly Node startsentinel;
    /// <summary>Node to the right of last node</summary>
    private readonly Node endsentinel;
    /// <summary>Offset of this view in underlying list</summary>
    private readonly int? offset;

    private readonly Dictionary<T, Node> dict;
    #endregion Fields

    #region Util
    private bool equals(T i1, T i2) => itemequalityComparer.Equals(i1, i2);

    /// <summary>Increment or decrement the private size fields</summary>
    /// <param name="delta">Increment (with sign)</param>
    private void addtosize(int delta) => size += delta;

    #region Checks
    /// <summary>Check if it is valid to perform updates and increment stamp if so.</summary>
    protected virtual void updatecheck() => stamp++;

    /// <summary>Check that the list has not been updated since a particular time.
    ///     <para>To be used by enumerators and range </para>
    /// </summary>
    /// <param name="stamp">The stamp indicating the time.</param>
    protected virtual void modifycheck(int stamp) {
        if (this.stamp != stamp) throw new Exception("Collection has been modified.");
    }
    #endregion Checks

    #region Searching
    private bool contains(T item, out Node node) {
        node = null;
        if (!dict.ContainsKey(item)) return false;
        node = dict[item];
        return true;
    }
    /// <summary>Search forwards from a node for a node with a particular item.</summary>
    /// <param name="item">The item to look for</param>
    /// <param name="node">On input, the node to start at. If item was found, the node found on output.</param>
    /// <param name="index">
    ///     If node was found, the value will be the number of links followed higher than the value on input.
    ///     If item was not found, the value on output is undefined.
    /// </param>
    /// <returns>True if node was found.</returns>
    private bool find(T item, ref Node node, ref int index) {
        while (node != endsentinel) {
            //if (item.Equals(node.item))
            if (itemequalityComparer.Equals(item, node.item)) return true;

            index++;
            node = node.next;
        }

        return false;
    }

    private bool dnif(T item, ref Node node, ref int index) {
        while (node != startsentinel) {
            //if (item.Equals(node.item))
            if (itemequalityComparer.Equals(item, node.item)) return true;

            index--;
            node = node.prev;
        }

        return false;
    }
    #endregion Searching

    #region Inserting
    private void insert(int index, Node succ, T item) {
        var newnode = new Node(item);
        if (dict.ContainsKey(item)) throw new ArgumentException("Item already in indexed list: " + item);
        dict[item] = newnode;
        insertNode(true, succ, newnode);
    }

    /// <summary>Insert a Node before another one. Unchecked version.</summary>
    /// <param name="succ">The successor to be</param>
    /// <param name="newnode">Node to insert</param>
    /// <param name="updateViews">update overlapping view in this call</param>
    private void insertNode(bool updateViews, Node succ, Node newnode) {
        newnode.next = succ;
        var pred = newnode.prev = succ.prev;
        succ.prev.next = newnode;
        succ.prev = newnode;
        size++;
    }
    #endregion Inserting

    #region Removing
    private T remove(Node node, int index) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
        size--;
        return node.item;
    }
    #endregion Removing

    #region Indexing
    /// <summary>Return the node at position pos</summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    private Node get(int pos) {
        if (pos < 0 || pos >= size) throw new IndexOutOfRangeException();
        if (pos < size / 2) { // Closer to front
            var node = startsentinel;

            for (var i = 0; i <= pos; i++) node = node.next;

            return node;
        } else { // Closer to end
            var node = endsentinel;

            for (var i = size; i > pos; i--) node = node.prev;

            return node;
        }
    }

    /// <summary>
    ///     Find the distance from pos to the set given by positions. Return the signed distance as return value and as an
    ///     out parameter, the array index of the nearest position. This is used for up to length 5 of positions, and we do not
    ///     assume it is sorted.
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="positions"></param>
    /// <param name="nearest"></param>
    /// <returns></returns>
    private int dist(int pos, out int nearest, int[] positions) {
        nearest = -1;
        var bestdist = int.MaxValue;
        var signeddist = bestdist;
        for (var i = 0; i < positions.Length; i++) {
            var thisdist = positions[i] - pos;
            if (thisdist >= 0 && thisdist < bestdist) {
                nearest = i;
                bestdist = thisdist;
                signeddist = thisdist;
            }
            if (thisdist < 0 && -thisdist < bestdist) {
                nearest = i;
                bestdist = -thisdist;
                signeddist = thisdist;
            }
        }
        return signeddist;
    }

    /// <summary>Find the node at position pos, given known positions of several nodes.</summary>
    /// <param name="pos"></param>
    /// <param name="positions"></param>
    /// <param name="nodes"></param>
    /// <returns></returns>
    private Node get(int pos, int[] positions, Node[] nodes) {
        int nearest;
        var delta = dist(pos, out nearest, positions);
        var node = nodes[nearest];
        if (delta > 0)
            for (var i = 0; i < delta; i++)
                node = node.prev;
        else
            for (var i = 0; i > delta; i--)
                node = node.next;
        return node;
    }

    /// <summary>Get nodes at positions p1 and p2, given nodes at several positions.</summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <param name="n1"></param>
    /// <param name="n2"></param>
    /// <param name="positions"></param>
    /// <param name="nodes"></param>
    private void getPair(int p1, int p2, out Node n1, out Node n2, int[] positions, Node[] nodes) {
        int delta1 = dist(p1, out var nearest1, positions), d1 = delta1 < 0 ? -delta1 : delta1;
        int delta2 = dist(p2, out var nearest2, positions), d2 = delta2 < 0 ? -delta2 : delta2;

        if (d1 < d2) {
            n1 = get(p1, positions, nodes);
            n2 = get(p2, new[] { positions[nearest2], p1 }, new[] { nodes[nearest2], n1 });
        } else {
            n2 = get(p2, positions, nodes);
            n1 = get(p1, new[] { positions[nearest1], p2 }, new[] { nodes[nearest1], n2 });
        }
    }
    #endregion Indexing
    #endregion Util
}