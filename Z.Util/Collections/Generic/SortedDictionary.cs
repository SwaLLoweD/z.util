using System;
using System.Collections.Generic;
using System.Linq;

namespace Z.Collections.Generic;

/// <summary>IDictionary implementation that returns default(U) if a key does not exist instead of throwing an exception</summary>
/// <typeparam name="TSort">Type used for sorting</typeparam>
/// <typeparam name="TKey">Key</typeparam>
/// <typeparam name="TValue">Value</typeparam>
[Serializable]
public class SortedDictionary<TSort, TKey, TValue> : SortedDictionary<TSort, TValue> where TSort : notnull where TKey : notnull
{
    #region Fields / Properties
    /// <inheritdoc />
    public new virtual TValue this[TSort key] => base[key];
    /// <inheritdoc />
    public virtual TValue this[TKey key] => KeyTable[key];

    /// <inheritdoc />
    public new virtual Dictionary<TKey, TValue>.KeyCollection Keys => KeyTable.Keys;

    /// <inheritdoc />
    public virtual KeyCollection SortKeys => base.Keys;
    /// <inheritdoc />
    public virtual TValue this[TSort sort, TKey key] {
        get => KeyTable[key];
        set {
            KeyTable[key] = value;
            base[sort] = value;
        }
    }
    /// <inheritdoc />
    public Dictionary<TKey, TValue> KeyTable { get; }
    #endregion

    #region Constructors
    /// <inheritdoc />
    public SortedDictionary(IComparer<TSort>? comparer = null) : base(comparer) => KeyTable = new Dictionary<TKey, TValue>();
    #endregion

    /// <inheritdoc />
    public virtual void Add(TSort sort, TKey key, TValue value) {
        base[sort] = value;
        KeyTable[key] = value;
    }

    /// <inheritdoc />
    public virtual void Remove(TSort sort, TKey key) {
        if (KeyTable.ContainsKey(key)) KeyTable.Remove(key);
        if (base.ContainsKey(sort)) base.Remove(sort);
    }
    /// <inheritdoc />
    public new virtual void Remove(TSort sort) {
        if (!base.ContainsKey(sort)) return;
        var value = base[sort];
        var key = KeyTable.Where(x => Equals(x.Value, value)).Select(x => x.Key).FirstOrDefault();
        base.Remove(sort);
        if (key != null) KeyTable.Remove(key);
    }
    /// <inheritdoc />
    public virtual void Remove(TKey key) {
        if (!KeyTable.ContainsKey(key)) return;
        var value = KeyTable[key];
        var sort = this.Where(x => Equals(x.Value, value)).Select(x => x.Key).FirstOrDefault();
        KeyTable.Remove(key);
        if (sort != null) base.Remove(sort);
    }

    /// <inheritdoc />
    public virtual bool ContainsKey(TSort sort, TKey key) => base.ContainsKey(sort) && KeyTable.ContainsKey(key);
    /// <inheritdoc />
    public virtual bool ContainsKey(TKey key) => KeyTable.ContainsKey(key);

    /// <inheritdoc />
    public override int GetHashCode() => KeyTable.GetHashCode() ^ base.GetHashCode();
    /// <inheritdoc />
    public override bool Equals(object? obj) {
        var ct = obj as SortedDictionary<TSort, TKey, TValue>;
        if (obj == null) return false;
        return GetHashCode() == ct?.GetHashCode();
    }
}
