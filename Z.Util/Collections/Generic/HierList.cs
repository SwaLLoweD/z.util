﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Z.Collections.Generic;

/// <summary>Hierarchic List (One parent, many children)</summary>
/// <typeparam name="T">Type of items to be stored</typeparam>
[Serializable]
public class HierList<T> : List<HierListItem<T>>
{
    /// <summary>Get the root items (Parents = null)</summary>
    /// <returns>root items list</returns>
    public IEnumerable<HierListItem<T>> RootList() => ChildrenOf(null);
    /// <summary>Get the children of specifed parent</summary>
    /// <param name="parent">Parent item</param>
    /// <returns>children of specified parent</returns>
    public IEnumerable<HierListItem<T>> ChildrenOf(HierListItem<T>? parent) => this.Where(x => x.Parent == parent);
}

/// <summary>Hierarchic List Item</summary>
/// <typeparam name="T">Type of items to be stored</typeparam>
[Serializable]
public class HierListItem<T>
{
    #region Fields / Properties
    /// <summary>Children of this item</summary>
    public IEnumerable<HierListItem<T>> Children => List == null ? new List<HierListItem<T>>() : List.ChildrenOf(this);
    /// <summary>The list that this item belongs to</summary>
    public HierList<T> List { get; set; }
    /// <summary>Parent of this item</summary>
    public HierListItem<T>? Parent { get; set; }
    /// <summary>Text attached to this item</summary>
    public string? Text { get; set; }
    /// <summary>Real value of the item</summary>
    public T Value { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    /// <param name="list">The list that this item belongs to</param>
    /// <param name="value">Real value of the item</param>
    /// <param name="parent">Parent of this item</param>
    public HierListItem(HierList<T> list, T value, HierListItem<T>? parent = null) {
        List = list;
        Parent = parent;
        Value = value;
    }
    #endregion
}