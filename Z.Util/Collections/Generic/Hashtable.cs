/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Z.Collections.Helpers;

namespace Z.Collections.Generic
{
    /// <summary>IDictionary implementation that returns default(U) if a key does not exist instead of throwing an exception</summary>
    /// <typeparam name="TKey">Key</typeparam>
    /// <typeparam name="TValue">Value</typeparam>
    [Serializable]
    public class Hashtable<TKey, TValue> : Dictionary<TKey, TValue>
    {
        #region Fields / Properties
        /// <inheritdoc />
        public new virtual TValue this[TKey key] { get => TryGetValue(key, out var rval) ? rval : default; set => base[key] = value; }
        #endregion

        #region Constructors
        /// <inheritdoc />
        public Hashtable() { }
        /// <inheritdoc />
        public Hashtable(IEnumerable<KeyValuePair<TKey, TValue>> dictionary) : base(Wrappers.Dictionary(dictionary)) { }
        /// <inheritdoc />
        public Hashtable(IEqualityComparer<TKey> comparer) : base(comparer) { }
        /// <inheritdoc />
        public Hashtable(int capacity) : base(capacity) { }
        /// <inheritdoc />
        public Hashtable(IEnumerable<KeyValuePair<TKey, TValue>> dictionary, IEqualityComparer<TKey> comparer) : base(Wrappers.Dictionary(dictionary), comparer) { }
        /// <inheritdoc />
        public Hashtable(int capacity, IEqualityComparer<TKey> comparer) : base(capacity, comparer) { }
        /// <inheritdoc />
        protected Hashtable(SerializationInfo info, StreamingContext context) : base(info, context) { }
        #endregion

        /// <inheritdoc />
        public new virtual void Add(TKey key, TValue value) => this[key] = value;

        /// <inheritdoc />
        public new virtual void Remove(TKey key) {
            if (ContainsKey(key)) base.Remove(key);
        }
    }
}