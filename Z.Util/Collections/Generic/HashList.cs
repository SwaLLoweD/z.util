﻿/*
Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>
Copyright (c) 2003-2006 Niels Kokholm and Peter Sestoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Z.Collections.Bases;
using Z.Collections.Helpers;

namespace Z.Collections.Generic;

/// <summary>
///     A list collection based on a plain dynamic array data structure. Expansion of the internal array is performed by
///     doubling on demand. The internal array is only shrinked by the Clear method.
///     <i>
///         When the FIFO property is set to false this class works fine as a stack of T. When the FIFO property is set to
///         true the class will function as a (FIFO) queue but very inefficiently, use a LinkedList (
///         <see cref="T:C5.LinkedList`1" />) instead.
///     </i>
/// </summary>
public class HashList<T> : ListBase<T>, IList<T>
{
    #region Fields / Properties
    /// <summary>Constructor</summary>
    /// <value>Offset for this list view or 0 for an underlying list.</value>
    public virtual int Offset => offset;
    /// <summary>
    ///     Since <c>Add(T item)</c> always add at the end of the list, this describes if list has FIFO or LIFO
    ///     semantics.
    /// </summary>
    /// <value>
    ///     True if the <c>Remove()</c> operation removes from the start of the list, false if it removes from the
    ///     end. The default for a new array list is false.
    /// </value>
    public virtual bool FIFO { get; set; }
    #endregion

    #region Constructors
    /// <summary>Create an array list with default item equalityComparer and initial capacity 8 items.</summary>
    public HashList() : this(8) { }

    /// <summary>Create an array list with default item equalityComparer and initial capacity 8 items.</summary>
    public HashList(IEnumerable<T> collection, IEqualityComparer<T>? itemequalityComparer = null)
        : this(8) {
        if (collection == null) throw new ArgumentNullException(nameof(collection));
        this.itemequalityComparer = itemequalityComparer ?? EqualityComparer<T>.Default;
        itemIndex = new Hash<KeyValuePair<T, int>>(Comparers.EqualityComparerKeyValueFromComparerKey<T, int>(this.itemequalityComparer));
        var newlength = 8;
        while (newlength < collection.Count()) newlength *= 2;
        array = new T[newlength];

        foreach (var item in collection) Add(item);
    }

    /// <summary>Create an array list with external item equalityComparer and initial capacity 8 items.</summary>
    /// <param name="itemequalityComparer">The external item equalitySCG.Comparer</param>
    public HashList(IEqualityComparer<T> itemequalityComparer) : this(8, itemequalityComparer) { }

    /// <summary>Create an array list with default item equalityComparer and prescribed initial capacity.</summary>
    /// <param name="capacity">The prescribed capacity</param>
    public HashList(int capacity) : this(capacity, EqualityComparer<T>.Default) { }

    /// <summary>Create an array list with external item equalityComparer and prescribed initial capacity.</summary>
    /// <param name="capacity">The prescribed capacity</param>
    /// <param name="itemequalityComparer">The external item equalitySCG.Comparer</param>
    public HashList(int capacity, IEqualityComparer<T> itemequalityComparer) {
        this.itemequalityComparer = itemequalityComparer ?? throw new NullReferenceException("Item EqualityComparer cannot be null.");
        itemIndex = new Hash<KeyValuePair<T, int>>(Comparers.EqualityComparerKeyValueFromComparerKey<T, int>(itemequalityComparer));
        var newlength = 8;
        while (newlength < capacity) newlength *= 2;
        array = new T[newlength];
    }
    #endregion

    #region IList<T> Members
    #region IEnumerable<T>
    /// <summary>Create an enumerator for the collection</summary>
    /// <returns>The enumerator</returns>
    public override IEnumerator<T> GetEnumerator() {
        int thestamp = stamp, theend = size + offset, thestart = offset;

        for (var i = thestart; i < theend; i++) {
            Modifycheck(thestamp);
            yield return array[i];
        }
    }
    #endregion IEnumerable<T>
    #endregion

    /// <summary>
    ///     Check if this collection contains an item equivalent according to the itemequalityComparer to a particular
    ///     value. If so, update the item in the collection to with a binary copy of the supplied value. This will only update
    ///     the first mathching item.
    /// </summary>
    /// <param name="item">Value to update.</param>
    /// <returns>True if the item was found and hence updated.</returns>
    public virtual bool Update(T item) => TryUpdate(item, out _);

    /// <summary>TryUpdate</summary>
    /// <param name="item"></param>
    /// <param name="olditem"></param>
    /// <returns></returns>
    public virtual bool TryUpdate(T item, out T? olditem) {
        Updatecheck();
        int i;

        if ((i = IndexOfInner(item)) >= 0) {
            olditem = array[offset + i];
            array[offset + i] = item;
            itemIndex.Insert(new KeyValuePair<T, int>(item, offset + i), true, out _);
            return true;
        }

        olditem = default;
        return false;
    }

    #region Diagnostics
    /// <summary>Check the integrity of the internal data structures of this array list.</summary>
    /// <returns>True if check does not fail.</returns>
    public virtual bool Check() {
        var retval = true;

        if (size > array.Length) //Logger.Log(string.Format("underlyingsize ({0}) > array.Length ({1})", size, array.Length));
            return false;

        if (offset + size > size) //Logger.Log(string.Format("offset({0})+size({1}) > underlyingsize ({2})", offset, size, underlyingsize));
            return false;

        if (offset < 0) //Logger.Log(string.Format("offset({0}) < 0", offset));
            return false;

        for (var i = 0; i < size; i++) {
            if (array[i] == null) //Logger.Log(string.Format("Bad element: null at (base)index {0}", i));
                retval = false;
        }

        for (int i = size, length = array.Length; i < length; i++) {
            if (!EqualsInner(array[i], default)) //Logger.Log(string.Format("Bad element: != default(T) at (base)index {0}", i));
                retval = false;
        }

        if (size != itemIndex.Count()) //Logger.Log(string.Format("size ({0})!= index.Count ({1})", size, itemIndex.Count));
            retval = false;

        for (var i = 0; i < size; i++) {
            var p = new KeyValuePair<T, int>(array[i], 0);

            if (!itemIndex.Find(p, false, out _)) //Logger.Log(string.Format("Item {1} at {0} not in hashindex", i, array[i]));
                retval = false;
            else if (p.Value != i) //Logger.Log(string.Format("Item {1} at {0} has hashindex {2}", i, array[i], p.Value));
                retval = false;
        }
        return retval;
    }
    #endregion Diagnostics

    #region Fields
    /// <summary>The actual internal array container. Will be extended on demand.</summary>
    protected T[] array;

    /// <summary>
    ///     The offset into the internal array container of the first item. The offset is 0 for a base dynamic array and
    ///     may be positive for an updatable view into a base dynamic array.
    /// </summary>
    protected int offset;

    /// <summary>The current stamp value</summary>
    protected int stamp;

    /// <summary>The number of items in the collection</summary>
    protected int size;

    /// <summary>The item equalityComparer of the collection</summary>
    protected readonly IEqualityComparer<T> itemequalityComparer;

    private Hash<KeyValuePair<T, int>> itemIndex;
    #endregion Fields

    #region Util
    private bool EqualsInner(T? i1, T? i2) => itemequalityComparer.Equals(i1, i2);

    /// <summary>Increment or decrement the private size fields</summary>
    /// <param name="delta">Increment (with sign)</param>
    private void AddtoSizeInner(int delta) => size += delta;

    #region Array handling
    /// <summary>Double the size of the internal array.</summary>
    protected virtual void Expand() => Expand(2 * array.Length, size);

    /// <summary>Expand the internal array, resetting the index of the first unused element.</summary>
    /// <param name="newcapacity">The new capacity (will be rouded upwards to a power of 2).</param>
    /// <param name="newsize">The new count of </param>
    protected virtual void Expand(int newcapacity, int newsize) {
        Debug.Assert(newcapacity >= newsize);

        var newlength = array.Length;

        while (newlength < newcapacity) newlength *= 2;

        var newarray = new T[newlength];

        Array.Copy(array, newarray, newsize);
        array = newarray;
    }
    #endregion Array handling

    #region Checks
    /// <summary>Check if it is valid to perform updates and increment stamp if so.</summary>
    protected virtual void Updatecheck() => stamp++;

    /// <summary>Check that the list has not been updated since a particular time.
    ///     <para>To be used by enumerators and range </para>
    /// </summary>
    /// <param name="stamp">The stamp indicating the time.</param>
    protected virtual void Modifycheck(int stamp) {
        if (this.stamp != stamp) throw new Exception("Collection has been modified.");
    }
    #endregion Checks

    #region Searching
    /// <summary>Internal version of IndexOf without modification checks.</summary>
    /// <param name="item">Item to look for</param>
    /// <returns>The index of first occurrence</returns>
    private int IndexOfInner(T item) {
        var p = new KeyValuePair<T, int>(item, 0);
        if (!itemIndex.Find(p, false, out var fnd)) return ~size;
        if (fnd.Value >= offset && fnd.Value < offset + size) return fnd.Value - offset;

        return ~size;
    }

    /// <summary>Internal version of LastIndexOf without modification checks.</summary>
    /// <param name="item">Item to look for</param>
    /// <returns>The index of last occurrence</returns>
    private int LastIndexOfInner(T item) => IndexOfInner(item);
    #endregion Searching

    #region Inserting
    /// <summary>Internal version of Insert with no modification checks.</summary>
    /// <param name="i">Index to insert at</param>
    /// <param name="item">Item to insert</param>
    protected virtual void InsertInner(int i, T item) {
        var p = new KeyValuePair<T, int>(item, offset + i);
        if (Contains(item)) throw new ArgumentException("Item already in indexed list: " + item);
        itemIndex.Insert(p, true, out _);

        BaseInserInner(i, item);
        Reindex(i + offset + 1);
    }

    private void BaseInserInner(int i, T item) {
        if (size == array.Length) Expand();
        i += offset;
        if (i < size) Array.Copy(array, i, array, i + 1, size - i);
        array[i] = item;
        AddtoSizeInner(1);
    }
    #endregion Inserting

    #region Removing
    /// <summary>Internal version of RemoveAt with no modification checks.</summary>
    /// <param name="i">Index to remove at</param>
    /// <returns>The removed item</returns>
    private T RemoveAtInner(int i) {
        i += offset;
        var retval = array[i];
        AddtoSizeInner(-1);
        if (size > i) Array.Copy(array, i + 1, array, i, size - i);
        //array[size] = default;
        itemIndex.Delete(new KeyValuePair<T, int>(retval, 0), out _);
        Reindex(i);
        return retval;
    }
    #endregion Removing

    #region Indexing
    private void Reindex(int start) => Reindex(start, size);

    private void Reindex(int start, int end) {
        for (var j = start; j < end; j++) itemIndex.Insert(new KeyValuePair<T, int>(array[j], j), true, out _);
    }
    #endregion Indexing
    #endregion Util

    #region IList<T>
    /// <summary>On this list, this indexer is read/write.</summary>
    /// <exception cref="IndexOutOfRangeException">if index is negative or &gt;= the size of the collection.</exception>
    /// <value>The index'th item of this list.</value>
    /// <param name="index">The index of the item to fetch or store.</param>
    public override T this[int index] {
        get {
            if (index < 0 || index >= size) throw new IndexOutOfRangeException();
            return array[offset + index];
        }
        set {
            Updatecheck();
            if (index < 0 || index >= size) throw new IndexOutOfRangeException();
            index += offset;
            var item = array[index];

            var p = new KeyValuePair<T, int>(value, index);
            if (itemequalityComparer.Equals(value, item)) {
                array[index] = value;
                itemIndex.Insert(p, true, out _);
            } else if (!itemIndex.Find(p, false, out _)) {
                itemIndex.Delete(new KeyValuePair<T, int>(item, 0), out _);
                array[index] = value;
            } else {
                throw new ArgumentException("Item already in indexed list");
            }
        }
    }

    /// <summary>Search for an item in the list going forwrds from the start.</summary>
    /// <param name="item">Item to search for.</param>
    /// <returns>Index of item from start.</returns>
    public override int IndexOf(T? item) => item == null ? -1 : IndexOfInner(item);

    /// <summary>Insert an item at a specific index location in this list.</summary>
    /// <exception cref="IndexOutOfRangeException">if index is negative or &gt; the size of the collection.</exception>
    /// <param name="index">The index at which to insert.</param>
    /// <param name="item">The item to insert.</param>
    public override void Insert(int index, T? item) {
        Updatecheck();
        if (index < 0 || index > size) throw new IndexOutOfRangeException();
        if (item == null) throw new ArgumentNullException(nameof(item));

        InsertInner(index, item);
    }

    /// <summary>Remove the item at a specific position of the list.</summary>
    /// <exception cref="IndexOutOfRangeException">if index is negative or &gt;= the size of the collection.</exception>
    /// <param name="index">The index of the item to remove.</param>
    /// <returns>The removed item.</returns>
    public override void RemoveAt(int index) {
        Updatecheck();
        if (index < 0 || index >= size) throw new IndexOutOfRangeException("Index out of range for sequenced collection");

        _ = RemoveAtInner(index);
    }
    #endregion IList<T>

    #region ICollection<T>
    /// <inheritdoc />
    public override int Count => size;

    /// <summary>Remove all items from this collection, resetting internal array size.</summary>
    public override void Clear() {
        Updatecheck();
        if (size == 0) return;
        itemIndex.StopEnumerations(); // Invalidate any enumerations.
                                      // The simplest and fastest way is simply to throw away the old hash and create a new one.
        itemIndex = new Hash<KeyValuePair<T, int>>(Comparers.EqualityComparerKeyValueFromComparerKey<T, int>(itemequalityComparer));
        array = new T[8];
        size = 0;
    }

    /// <summary>
    ///     Remove a particular item from this list. The item will be searched for from the end of the list if
    ///     <c>FIFO == false</c> (the default), else from the start.
    /// </summary>
    /// <param name="item">The value to remove.</param>
    /// <returns>True if the item was found (and removed).</returns>
    public override bool Remove(T item) {
        Updatecheck();

        var i = FIFO ? IndexOfInner(item) : LastIndexOfInner(item);

        if (i < 0) return false;

        _ = RemoveAtInner(i);
        return true;
    }

    /// <summary>
    ///     Remove the first copy of a particular item from this collection if found. If an item was removed, report a
    ///     binary copy of the actual item removed in the argument. The item will be searched for from the end of the list if
    ///     <c>FIFO == false</c> (the default), else from the start.
    /// </summary>
    /// <param name="item">The value to remove.</param>
    /// <param name="removeditem">The removed value.</param>
    /// <returns>True if the item was found (and removed).</returns>
    public virtual bool TryRemove(T item, out T? removeditem) {
        Updatecheck();

        var i = FIFO ? IndexOfInner(item) : LastIndexOfInner(item);

        if (i < 0) {
            removeditem = default;
            return false;
        }

        removeditem = RemoveAtInner(i);
        return true;
    }
    #endregion ICollection<T>
}