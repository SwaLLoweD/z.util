/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using Z.Collections.Helpers;

namespace Z.Collections.Generic
{
    /// <summary>IDictionary implementation that returns default(U) if a key does not exist instead of throwing an exception</summary>
    /// <typeparam name="TKey">Key</typeparam>
    /// <typeparam name="TValue">Value</typeparam>
    [Serializable]
    public class SortedTable<TKey, TValue> : SortedDictionary<TKey, TValue>
    {
        #region Fields / Properties
        /// <inheritdoc />
        public new virtual TValue this[TKey key] { get => ContainsKey(key) ? base[key] : default; set => base[key] = value; }
        #endregion

        #region Constructors
        /// <inheritdoc />
        public SortedTable() { }
        /// <inheritdoc />
        public SortedTable(IComparer<TKey> comparer) : base(comparer) { }
        /// <inheritdoc />
        public SortedTable(IEnumerable<KeyValuePair<TKey, TValue>> source) : base(Wrappers.Dictionary(source)) { }
        /// <inheritdoc />
        public SortedTable(IEnumerable<KeyValuePair<TKey, TValue>> source, IComparer<TKey> comparer) : base(Wrappers.Dictionary(source), comparer) { }
        #endregion

        /// <inheritdoc />
        public new virtual void Add(TKey key, TValue value) => this[key] = value;

        /// <inheritdoc />
        public new virtual void Remove(TKey key) {
            if (ContainsKey(key)) base.Remove(key);
        }
    }

    /// <summary>IDictionary implementation that returns default(U) if a key does not exist instead of throwing an exception</summary>
    /// <typeparam name="TSort">Type used for sorting</typeparam>
    /// <typeparam name="TKey">Key</typeparam>
    /// <typeparam name="TValue">Value</typeparam>
    [Serializable]
    public class SortedTable<TSort, TKey, TValue> : SortedDictionary<TSort, TValue>
    {
        #region Fields / Properties
        /// <inheritdoc />
        public new virtual TValue this[TSort key] => ContainsKey(key) ? base[key] : default;
        /// <inheritdoc />
        public virtual TValue this[TKey key] => KeyTable.ContainsKey(key) ? KeyTable[key] : default;

        /// <inheritdoc />
        public new virtual Dictionary<TKey, TValue>.KeyCollection Keys => KeyTable.Keys;

        /// <inheritdoc />
        public virtual KeyCollection SortKeys => base.Keys;
        /// <inheritdoc />
        public virtual TValue this[TSort sort, TKey key] {
            get => KeyTable.ContainsKey(key) ? KeyTable[key] : default;
            set {
                KeyTable[key] = value;
                base[sort] = value;
            }
        }
        /// <inheritdoc />
        public Dictionary<TKey, TValue> KeyTable { get; private set; }
        #endregion

        #region Constructors
        /// <inheritdoc />
        public SortedTable(IComparer<TSort> comparer = null) : base(comparer) => KeyTable = new Dictionary<TKey, TValue>();
        #endregion

        /// <inheritdoc />
        public virtual void Add(TSort sort, TKey key, TValue value) {
            base[sort] = value;
            KeyTable[key] = value;
        }

        /// <inheritdoc />
        public virtual void Remove(TSort sort, TKey key) {
            if (KeyTable.ContainsKey(key)) KeyTable.Remove(key);
            if (base.ContainsKey(sort)) base.Remove(sort);
        }
        /// <inheritdoc />
        public new virtual void Remove(TSort sort) {
            if (!base.ContainsKey(sort)) return;
            var value = base[sort];
            var key = KeyTable.Where(x => Equals(x.Value, value)).Select(x => x.Key).FirstOrDefault();
            base.Remove(sort);
            if (key != null) KeyTable.Remove(key);
        }
        /// <inheritdoc />
        public virtual void Remove(TKey key) {
            if (!KeyTable.ContainsKey(key)) return;
            var value = KeyTable[key];
            var sort = this.Where(x => Equals(x.Value, value)).Select(x => x.Key).FirstOrDefault();
            KeyTable.Remove(key);
            if (sort != null) base.Remove(sort);
        }

        /// <inheritdoc />
        public virtual bool ContainsKey(TSort sort, TKey key) => base.ContainsKey(sort) && KeyTable.ContainsKey(key);
        /// <inheritdoc />
        public virtual bool ContainsKey(TKey key) => KeyTable.ContainsKey(key);

        /// <inheritdoc />
        public override int GetHashCode() => KeyTable.GetHashCode() ^ base.GetHashCode();
        /// <inheritdoc />
        public override bool Equals(object obj) {
            var ct = obj as SortedTable<TSort, TKey, TValue>;
            if (obj == null) return false;
            return GetHashCode() == ct.GetHashCode();
        }
    }
}