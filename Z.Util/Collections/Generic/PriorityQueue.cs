﻿/*
Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>
Copyright (c) 2003-2006 Niels Kokholm and Peter Sestoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using Z.Collections.Bases;
using Z.Collections.Helpers;

#nullable disable
namespace Z.Collections.Generic;

/// <summary>
///     A list collection based on a plain dynamic array data structure. Expansion of the internal array is performed by
///     doubling on demand. The internal array is only shrinked by the Clear method.
///     <i>
///         When the FIFO property is set to false this class works fine as a stack of T. When the FIFO property is set to
///         true the class will function as a (FIFO) queue but very inefficiently, use a LinkedList (
///         <see cref="T:C5.LinkedList`1" />) instead.
///     </i>
/// </summary>
public class PriorityQueue<T> : CollectionBase<T>
{
    #region Fields / Properties
    /// <summary>The comparer object supplied at creation time for this collection</summary>
    /// <value>The comparer</value>
    public virtual IComparer<T> Comparer => comparer;
    /// <summary>Count</summary>
    /// <value>The size of this collection</value>
    public override int Count => size;

    /// <summary>Get or set the item corresponding to a handle.</summary>
    /// <param name="handle">The reference into the heap</param>
    /// <returns></returns>
    public virtual T this[IPriorityQueueHandle<T> handle] {
        get {
            CheckHandle(handle, out var cell, out var isfirst);
            return isfirst ? heap[cell].first : heap[cell].last;
        }
        set => Replace(handle, value);
    }
    #endregion

    #region Constructors
    /// <summary>Create an array list with default item equalityComparer and initial capacity 8 items.</summary>
    public PriorityQueue(IEnumerable<T> collection, IComparer<T> comparer = null, IEqualityComparer<T> itemequalityComparer = null) {
        if (collection == null) throw new ArgumentNullException(nameof(collection));

        var capacity = collection.Count();
        this.comparer = comparer ?? Comparer<T>.Default;
        this.itemequalityComparer = itemequalityComparer ?? EqualityComparer<T>.Default;
        var length = 1;
        while (length < capacity) length <<= 1;
        heap = new Interval[length];

        foreach (var item in collection) Add(item);
    }

    /// <summary>Create an interval heap with external item comparer and default initial capacity (16)</summary>
    /// <param name="comparer">The external comparer</param>
    public PriorityQueue(IComparer<T> comparer) : this(16, comparer) { }

    //TODO: maybe remove
    /// <summary>Create an interval heap with natural item comparer and prescribed initial capacity</summary>
    /// <param name="capacity">The initial capacity</param>
    public PriorityQueue(int capacity = 16) : this(capacity, Comparer<T>.Default, EqualityComparer<T>.Default) { }

    /// <summary>Create an interval heap with external item comparer and prescribed initial capacity</summary>
    /// <param name="capacity">The initial capacity</param>
    /// <param name="comparer">The external comparer</param>
    public PriorityQueue(int capacity, IComparer<T> comparer) : this(capacity, comparer, Comparers.EqualityComparerZeroHashCode(comparer)) { }

    /// <summary>Create an interval heap with external item comparer and prescribed initial capacity</summary>
    /// <param name="capacity">The initial capacity</param>
    /// <param name="comparer">The external comparer</param>
    /// <param name="itemequalityComparer"></param>
    protected PriorityQueue(int capacity, IComparer<T> comparer, IEqualityComparer<T> itemequalityComparer) {
        this.comparer = comparer ?? throw new NullReferenceException("Item comparer cannot be null");
        this.itemequalityComparer = itemequalityComparer ?? throw new NullReferenceException("Item equality comparer cannot be null");
        var length = 1;
        while (length < capacity) length <<= 1;
        heap = new Interval[length];
    }
    #endregion

    /// <summary>Add an item to this priority queue.</summary>
    /// <param name="item">The item to add.</param>
    /// <returns>True</returns>
    public override void Add(T item) {
        stamp++;
        Add(null, item);
        //if (add(null, item)) {
        //    return true;
        //}
        //return false;
    }

    private bool Add(Handle itemhandle, T item) {
        if (size == 0) {
            size = 1;
            UpdateFirst(0, item, itemhandle);
            return true;
        }

        if (size == 2 * heap.Length) {
            var newheap = new Interval[2 * heap.Length];

            Array.Copy(heap, newheap, heap.Length);
            heap = newheap;
        }

        if (size % 2 == 0) {
            int i = size / 2, p = ((i + 1) / 2) - 1;
            var tmp = heap[p].last;

            if (comparer.Compare(item, tmp) > 0) {
                UpdateFirst(i, tmp, heap[p].lasthandle);
                UpdateLast(p, item, itemhandle);
                BubbleUpMax(p);
            } else {
                UpdateFirst(i, item, itemhandle);

                if (comparer.Compare(item, heap[p].first) < 0) BubbleUpMin(i);
            }
        } else {
            var i = size / 2;
            var other = heap[i].first;

            if (comparer.Compare(item, other) < 0) {
                UpdateLast(i, other, heap[i].firsthandle);
                UpdateFirst(i, item, itemhandle);
                BubbleUpMin(i);
            } else {
                UpdateLast(i, item, itemhandle);
                BubbleUpMax(i);
            }
        }
        size++;

        return true;
    }

    private void UpdateLast(int cell, T item, Handle handle) {
        heap[cell].last = item;
        if (handle != null) handle.index = (2 * cell) + 1;
        heap[cell].lasthandle = handle;
    }

    private void UpdateFirst(int cell, T item, Handle handle) {
        heap[cell].first = item;
        if (handle != null) handle.index = 2 * cell;
        heap[cell].firsthandle = handle;
    }

    /// <summary>Remove a particular item from this collection. Very resource extensive on heaps.</summary>
    /// <param name="item">The value to remove.</param>
    /// <returns>True if the item was found (and removed).</returns>
    public override bool Remove(T item) {
        var set = new HashSet<T>(this);
        var rval = set.Remove(item);
        var newHeap = new PriorityQueue<T>(set);
        heap = newHeap.heap;
        stamp = newHeap.stamp;
        size = newHeap.size;
        return rval;
    }

    /// <summary>Remove all items from this collection.</summary>
    public override void Clear() {
        if (size == 0) return;
        var length = 1;
        while (length < 16) length <<= 1;
        heap = new Interval[length];
    }

    /// <summary>Create an enumerator for the collection
    ///     <para>Note: the enumerator does *not* enumerate the items in sorted order, but in the internal table order.</para>
    /// </summary>
    /// <returns>The enumerator(SIC)</returns>
    public override IEnumerator<T> GetEnumerator() {
        var mystamp = stamp;
        for (var i = 0; i < size; i++) {
            if (mystamp != stamp) throw new Exception("Collection has been modified.");
            yield return i % 2 == 0 ? heap[i >> 1].first : heap[i >> 1].last;
        }
    }
    /// <summary>Find the current least item of this priority queue.</summary>
    /// <returns>The least item.</returns>
    public virtual T FindMin() => FindMin(out _);

    /// <summary>Remove the least item from this  priority queue.</summary>
    /// <returns>The removed item.</returns>
    public virtual T DeleteMin() => DeleteMin(out _);

    /// <summary>Find the current largest item of this priority queue.</summary>
    /// <returns>The largest item.</returns>
    public virtual T FindMax() => FindMax(out _);

    /// <summary>Remove the largest item from this  priority queue.</summary>
    /// <returns>The removed item.</returns>
    public virtual T DeleteMax() => DeleteMax(out _);

    /// <summary>Check safely if a handle is valid for this queue and if so, report the corresponding queue item.</summary>
    /// <param name="handle">The handle to check</param>
    /// <param name="item">If the handle is valid this will contain the corresponding item on output.</param>
    /// <returns>True if the handle is valid.</returns>
    public virtual bool Find(IPriorityQueueHandle<T> handle, out T item) {
        if (handle is not Handle myhandle) {
            item = default;
            return false;
        }
        var toremove = myhandle.index;
        var cell = toremove / 2;
        var isfirst = toremove % 2 == 0;
        {
            if (toremove == -1 || toremove >= size) {
                item = default;
                return false;
            }
            var actualhandle = isfirst ? heap[cell].firsthandle : heap[cell].lasthandle;
            if (actualhandle != myhandle) {
                item = default;
                return false;
            }
        }
        item = isfirst ? heap[cell].first : heap[cell].last;
        return true;
    }

    /// <summary>
    ///     Add an item to the priority queue, receiving a handle for the item in the queue, or reusing an already
    ///     existing handle.
    /// </summary>
    /// <param name="handle">
    ///     On output: a handle for the added item. On input: null for allocating a new handle, an invalid
    ///     handle for reuse. A handle for reuse must be compatible with this priority queue, by being created by a priority
    ///     queue of the same runtime type, but not necessarily the same priority queue object.
    /// </param>
    /// <param name="item">The item to add.</param>
    /// <returns>True since item will always be added unless the call throws an exception.</returns>
    public virtual bool Add(ref IPriorityQueueHandle<T> handle, T item) {
        stamp++;
        var myhandle = (Handle)handle;
        if (myhandle == null)
            handle = myhandle = new Handle();
        else if (myhandle.index != -1) throw new ArgumentException("Handle not valid for reuse", nameof(handle));
        return Add(myhandle, item);
    }

    /// <summary>Delete an item with a handle from a priority queue.</summary>
    /// <param name="handle">The handle for the item. The handle will be invalidated, but reusable.</param>
    /// <returns>The deleted item</returns>
    public virtual T Delete(IPriorityQueueHandle<T> handle) {
        stamp++;
        var myhandle = CheckHandle(handle, out var cell, out var isfirst);

        T retval;
        myhandle.index = -1;
        var lastcell = (size - 1) / 2;

        if (cell == lastcell) {
            if (isfirst) {
                retval = heap[cell].first;
                if (size % 2 == 0) {
                    UpdateFirst(cell, heap[cell].last, heap[cell].lasthandle);
                    heap[cell].last = default;
                    heap[cell].lasthandle = null;
                } else {
                    heap[cell].first = default;
                    heap[cell].firsthandle = null;
                }
            } else {
                retval = heap[cell].last;
                heap[cell].last = default;
                heap[cell].lasthandle = null;
            }
            size--;
        } else if (isfirst) {
            retval = heap[cell].first;

            if (size % 2 == 0) {
                UpdateFirst(cell, heap[lastcell].last, heap[lastcell].lasthandle);
                heap[lastcell].last = default;
                heap[lastcell].lasthandle = null;
            } else {
                UpdateFirst(cell, heap[lastcell].first, heap[lastcell].firsthandle);
                heap[lastcell].first = default;
                heap[lastcell].firsthandle = null;
            }

            size--;
            if (HeapifyMin(cell))
                BubbleUpMax(cell);
            else
                BubbleUpMin(cell);
        } else {
            retval = heap[cell].last;

            if (size % 2 == 0) {
                UpdateLast(cell, heap[lastcell].last, heap[lastcell].lasthandle);
                heap[lastcell].last = default;
                heap[lastcell].lasthandle = null;
            } else {
                UpdateLast(cell, heap[lastcell].first, heap[lastcell].firsthandle);
                heap[lastcell].first = default;
                heap[lastcell].firsthandle = null;
            }

            size--;
            if (HeapifyMax(cell))
                BubbleUpMin(cell);
            else
                BubbleUpMax(cell);
        }

        return retval;
    }

    private Handle CheckHandle(IPriorityQueueHandle<T> handle, out int cell, out bool isfirst) {
        var myhandle = (Handle)handle;
        var toremove = myhandle.index;
        cell = toremove / 2;
        isfirst = toremove % 2 == 0;
        {
            if (toremove == -1 || toremove >= size) throw new ArgumentOutOfRangeException(nameof(handle), "Invalid handle, index out of range");
            var actualhandle = isfirst ? heap[cell].firsthandle : heap[cell].lasthandle;
            if (actualhandle != myhandle) throw new ArgumentException("Invalid handle, doesn't match queue", nameof(handle));
        }
        return myhandle;
    }

    /// <summary>
    ///     Replace an item with a handle in a priority queue with a new item. Typically used for changing the priority of
    ///     some queued object.
    /// </summary>
    /// <param name="handle">The handle for the old item</param>
    /// <param name="item">The new item</param>
    /// <returns>The old item</returns>
    public virtual T Replace(IPriorityQueueHandle<T> handle, T item) {
        stamp++;
        CheckHandle(handle, out var cell, out var isfirst);
        if (size == 0) throw new NullReferenceException("Heap is empty.");

        T retval;

        if (isfirst) {
            retval = heap[cell].first;
            heap[cell].first = item;
            if (size == 1) { } else if (size == (2 * cell) + 1) {
                // cell == lastcell
                var p = ((cell + 1) / 2) - 1;
                if (comparer.Compare(item, heap[p].last) > 0) {
                    var thehandle = heap[cell].firsthandle;
                    UpdateFirst(cell, heap[p].last, heap[p].lasthandle);
                    UpdateLast(p, item, thehandle);
                    BubbleUpMax(p);
                } else {
                    BubbleUpMin(cell);
                }
            } else if (HeapifyMin(cell)) {
                BubbleUpMax(cell);
            } else {
                BubbleUpMin(cell);
            }
        } else {
            retval = heap[cell].last;
            heap[cell].last = item;
            if (HeapifyMax(cell))
                BubbleUpMin(cell);
            else
                BubbleUpMax(cell);
        }
        return retval;
    }

    /// <summary>Find the current least item of this priority queue.</summary>
    /// <param name="handle">On return: the handle of the item.</param>
    /// <returns>The least item.</returns>
    public virtual T FindMin(out IPriorityQueueHandle<T> handle) {
        if (size == 0) throw new Exception("Heap is empty.");

        handle = heap[0].firsthandle;

        return heap[0].first;
    }

    /// <summary>Find the current largest item of this priority queue.</summary>
    /// <param name="handle">On return: the handle of the item.</param>
    /// <returns>The largest item.</returns>
    public virtual T FindMax(out IPriorityQueueHandle<T> handle) {
        if (size == 0) throw new Exception("Heap is empty.");
        if (size == 1) {
            handle = heap[0].firsthandle;
            return heap[0].first;
        }
        handle = heap[0].lasthandle;
        return heap[0].last;
    }

    /// <summary>Remove the least item from this priority queue.</summary>
    /// <param name="handle">On return: the handle of the removed item.</param>
    /// <returns>The removed item.</returns>
    public virtual T DeleteMin(out IPriorityQueueHandle<T> handle) {
        stamp++;
        if (size == 0) throw new Exception("Heap is empty.");

        var retval = heap[0].first;
        var myhandle = heap[0].firsthandle;
        handle = myhandle;
        if (myhandle != null) myhandle.index = -1;

        if (size == 1) {
            size = 0;
            heap[0].first = default;
            heap[0].firsthandle = null;
        } else {
            var lastcell = (size - 1) / 2;

            if (size % 2 == 0) {
                UpdateFirst(0, heap[lastcell].last, heap[lastcell].lasthandle);
                heap[lastcell].last = default;
                heap[lastcell].lasthandle = null;
            } else {
                UpdateFirst(0, heap[lastcell].first, heap[lastcell].firsthandle);
                heap[lastcell].first = default;
                heap[lastcell].firsthandle = null;
            }

            size--;
            HeapifyMin(0);
        }

        return retval;
    }

    /// <summary>Remove the largest item from this priority queue.</summary>
    /// <param name="handle">On return: the handle of the removed item.</param>
    /// <returns>The removed item.</returns>
    public virtual T DeleteMax(out IPriorityQueueHandle<T> handle) {
        stamp++;
        if (size == 0) throw new Exception("Heap is empty.");

        T retval;
        Handle myhandle;

        if (size == 1) {
            size = 0;
            retval = heap[0].first;
            myhandle = heap[0].firsthandle;
            if (myhandle != null) myhandle.index = -1;
            heap[0].first = default;
            heap[0].firsthandle = null;
        } else {
            retval = heap[0].last;
            myhandle = heap[0].lasthandle;
            if (myhandle != null) myhandle.index = -1;

            var lastcell = (size - 1) / 2;

            if (size % 2 == 0) {
                UpdateLast(0, heap[lastcell].last, heap[lastcell].lasthandle);
                heap[lastcell].last = default;
                heap[lastcell].lasthandle = null;
            } else {
                UpdateLast(0, heap[lastcell].first, heap[lastcell].firsthandle);
                heap[lastcell].first = default;
                heap[lastcell].firsthandle = null;
            }

            size--;
            HeapifyMax(0);
        }
        handle = myhandle;
        return retval;
    }

    #region Nested type: Handle
    private class Handle : IPriorityQueueHandle<T>
    {
        #region Fields / Properties
        /// <summary>To save space, the index is 2*cell for heap[cell].first, and 2*cell+1 for heap[cell].last</summary>
        internal int index = -1;
        #endregion

        public override string ToString() => string.Format("[{0}]", index);
    }
    #endregion

    #region Nested type: IPriorityQueueHandle
    /// <summary>The base type of a priority queue handle</summary>
    /// <typeparam name="U"></typeparam>
    public interface IPriorityQueueHandle<U>
    {
        //TODO: make abstract and prepare for double dispatch:
        //public virtual bool Delete(IPriorityQueue<T> q) { throw new InvalidFooException();}
        //bool Replace(T item);
    }
    #endregion

    #region Fields
    /// <summary>The current stamp value</summary>
    protected int stamp;

    /// <summary>The number of items in the collection</summary>
    protected int size;

    /// <summary>The item equalityComparer of the collection</summary>
    protected readonly IEqualityComparer<T> itemequalityComparer;

    /// <summary>The item comparer of the collection</summary>
    protected readonly IComparer<T> comparer;

    private Interval[] heap;

    private struct Interval
    {
        internal T first, last;
        internal Handle firsthandle, lasthandle;

        public override string ToString() => string.Format("[{0}; {1}]", first, last);
    }
    #endregion Fields

    #region Util
    // heapifyMin and heapifyMax and their auxiliaries

    private void SwapFirstWithLast(int cell1, int cell2) {
        var first = heap[cell1].first;
        var firsthandle = heap[cell1].firsthandle;
        UpdateFirst(cell1, heap[cell2].last, heap[cell2].lasthandle);
        UpdateLast(cell2, first, firsthandle);
    }

    private void SwapLastWithLast(int cell1, int cell2) {
        var last = heap[cell2].last;
        var lasthandle = heap[cell2].lasthandle;
        UpdateLast(cell2, heap[cell1].last, heap[cell1].lasthandle);
        UpdateLast(cell1, last, lasthandle);
    }

    private void SwapFirstWithFirst(int cell1, int cell2) {
        var first = heap[cell2].first;
        var firsthandle = heap[cell2].firsthandle;
        UpdateFirst(cell2, heap[cell1].first, heap[cell1].firsthandle);
        UpdateFirst(cell1, first, firsthandle);
    }

    private bool HeapifyMin(int cell) {
        var swappedroot = false;
        // If first > last, swap them
        if ((2 * cell) + 1 < size && comparer.Compare(heap[cell].first, heap[cell].last) > 0) {
            swappedroot = true;
            SwapFirstWithLast(cell, cell);
        }

        int currentmin = cell, l = (2 * cell) + 1, r = l + 1;
        if (2 * l < size && comparer.Compare(heap[l].first, heap[currentmin].first) < 0) currentmin = l;
        if (2 * r < size && comparer.Compare(heap[r].first, heap[currentmin].first) < 0) currentmin = r;

        if (currentmin != cell) {
            // cell has at least one daughter, and it contains the min
            SwapFirstWithFirst(currentmin, cell);
            HeapifyMin(currentmin);
        }
        return swappedroot;
    }

    private bool HeapifyMax(int cell) {
        var swappedroot = false;
        if ((2 * cell) + 1 < size && comparer.Compare(heap[cell].last, heap[cell].first) < 0) {
            swappedroot = true;
            SwapFirstWithLast(cell, cell);
        }

        int currentmax = cell, l = (2 * cell) + 1, r = l + 1;
        var firstmax = false; // currentmax's first field holds max
        if ((2 * l) + 1 < size) { // both l.first and l.last exist
            if (comparer.Compare(heap[l].last, heap[currentmax].last) > 0) currentmax = l;
        } else if ((2 * l) + 1 == size) { // only l.first exists
            if (comparer.Compare(heap[l].first, heap[currentmax].last) > 0) {
                currentmax = l;
                firstmax = true;
            }
        }

        if ((2 * r) + 1 < size) { // both r.first and r.last exist
            if (comparer.Compare(heap[r].last, heap[currentmax].last) > 0) currentmax = r;
        } else if ((2 * r) + 1 == size) { // only r.first exists
            if (comparer.Compare(heap[r].first, heap[currentmax].last) > 0) {
                currentmax = r;
                firstmax = true;
            }
        }

        if (currentmax != cell) {
            // The cell has at least one daughter, and it contains the max
            if (firstmax)
                SwapFirstWithLast(currentmax, cell);
            else
                SwapLastWithLast(currentmax, cell);
            HeapifyMax(currentmax);
        }
        return swappedroot;
    }

    private void BubbleUpMin(int i) {
        if (i > 0) {
            T min = heap[i].first, iv = min;
            var minhandle = heap[i].firsthandle;
            var p = ((i + 1) / 2) - 1;

            while (i > 0) {
                if (comparer.Compare(iv, min = heap[p].first) < 0) {
                    UpdateFirst(i, min, heap[p].firsthandle);
                    i = p;
                } else {
                    break;
                }
            }

            UpdateFirst(i, iv, minhandle);
        }
    }

    private void BubbleUpMax(int i) {
        if (i > 0) {
            T max = heap[i].last, iv = max;
            var maxhandle = heap[i].lasthandle;
            int p; //= (i + 1) / 2 - 1;
            while (i > 0)
                if (comparer.Compare(iv, max = heap[p = (i + 1) / 2 - 1].last) > 0) {
                    UpdateLast(i, max, heap[p].lasthandle);
                    //max = iv;
                    i = p;
                } else {
                    break;
                }
            UpdateLast(i, iv, maxhandle);
        }
    }
    #endregion Util
}