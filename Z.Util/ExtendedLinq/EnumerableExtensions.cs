﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Z.Extensions;

namespace Z.Linq;

/// <summary>Extension methods for all kind of Enumerables implementing the IEnumerable&lt;T&gt; interface</summary>
public static class ExtLinqIEnumerable {
    // ///<summary>Returns the first element of a sequence, or a default value if the sequence contains no elements.</summary>
    // ///<param name="source">The System.Collections.Generic.IEnumerableT to return the first element of.</param>
    // ///<param name="defaultValue">value to set if nothing is found.</param>
    // ///<typeparam name="TSource">The type of the elements of source.</typeparam>
    // ///<returns>default(TSource) if source is empty; otherwise, the first element in source.</returns>
    // public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, TSource defaultValue) => source == null || source.Count() == 0 ? defaultValue : source.FirstOrDefault();

    #region Compare
    /// <summary>Compares with another collection of same type, and returns same or different objects in those collections.</summary>
    /// <param name="source">The source collection to compare.</param>
    /// <param name="target">The collection to compare with.</param>
    /// <param name="inBoth">Items existing in both collections.</param>
    /// <param name="onlyInSource">Items only exist in source collection</param>
    /// <param name="onlyInTarget">Items only exist in target collection</param>
    /// <param name="match">The match.</param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static bool Compare<T>(this IEnumerable<T> source, ICollection<T> target, ICollection<T>? inBoth = null, ICollection<T>? onlyInSource = null, ICollection<T>? onlyInTarget = null,
        Func<T, T, bool>? match = null) {
        if (source == null) return target == null;
        if (target == null) return false;
        if (match == null) match = (x, y) => x!.Equals(y);
        var rval = true;
        var onlyInSourceUnique = UniqueCollection(onlyInSource);
        var onlyInTargetUnique = UniqueCollection(onlyInTarget);
        var inBothUnique = UniqueCollection(inBoth);
        var t = target.To().HashSet();
        Parallel.ForEach(source, x => {
            if (!t.Contains(x)) {
                rval = false;
                if (onlyInSource != null) {
                    if (onlyInSourceUnique)
                        onlyInSource.AddUnique(x);
                    else
                        onlyInSource.Add(x);
                }
            } else {
                if (inBoth != null) {
                    if (inBothUnique)
                        inBoth.AddUnique(x);
                    else
                        inBoth.Add(x);
                }
            }
            t.Remove(x);
        });
        if (onlyInTarget != null) {
            if (onlyInSourceUnique)
                onlyInSource?.AddUnique(t);
            else
                onlyInSource?.Add(t);
        }
        return rval;
    }
    #endregion Compare

    internal static bool UniqueCollection<T>(ICollection<T>? collection) {
        if (collection == null) return false;
        var valueType = collection.GetType();
        var unique = valueType.IsAssignableTo<ISet<T>>() || valueType.IsAssignableTo<IDictionary>();
        return unique;
    }

    #region MoreLinq
    #region CountBy
    /// <summary>
    ///     Applies a key-generating function to each element of a sequence and returns a sequence of unique keys and
    ///     their number of occurrences in the original sequence. An additional argument specifies a comparer to use for
    ///     testing equivalence of keys.
    /// </summary>
    /// <typeparam name="TSource">Type of the elements of the source sequence.</typeparam>
    /// <typeparam name="TKey">Type of the projected element.</typeparam>
    /// <param name="source">Source sequence.</param>
    /// <param name="keySelector">
    ///     Function that transforms each item of source sequence into a key to be compared against the
    ///     others.
    /// </param>
    /// <param name="comparer">
    ///     The equality comparer to use to determine whether or not keys are equal. If null, the default
    ///     equality comparer for <typeparamref name="TSource" /> is used.
    /// </param>
    /// <returns>A sequence of unique keys and their number of occurrences in the original sequence.</returns>
    public static IEnumerable<KeyValuePair<TKey, int>> CountBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey>? comparer = null) where TKey : notnull {
        if (source == null) throw new ArgumentNullException(nameof(source));

        return _();
        IEnumerable<KeyValuePair<TKey, int>> _() {
            List<TKey> keys;
            List<int> counts;

            // Avoid the temptation to inline the Loop method, which
            // exists solely to separate the scope & lifetimes of the
            // locals needed for the actual looping of the source &
            // production of the results (that happens once at the start
            // of iteration) from those needed to simply yield the
            // results. It is harder to reason about the lifetimes (if the
            // code is inlined) with respect to how the compiler will
            // rewrite the iterator code as a state machine. For
            // background, see:
            // http://blog.stephencleary.com/2010/02/q-should-i-set-variables-to-null-to.html

            Loop(comparer ?? EqualityComparer<TKey>.Default);

            for (var i = 0; i < keys.Count; i++) yield return new KeyValuePair<TKey, int>(keys[i], counts[i]);

            void Loop(IEqualityComparer<TKey> cmp) {
                var dic = new Dictionary<TKey, int>(cmp);
                keys = new List<TKey>();
                counts = new List<int>();
                var havePrevKey = false;
                var prevKey = default(TKey);
                var index = 0;

                foreach (var item in source) {
                    var key = keySelector(item);

                    if ( // key same as the previous? then re-use the index
                        (havePrevKey && cmp.GetHashCode(prevKey!) == cmp.GetHashCode(key) && cmp.Equals(prevKey, key))
                        // otherwise try & find index of the key
                        || dic.TryGetValue(key, out index)) {
                        counts[index]++;
                    } else {
                        dic[key] = keys.Count;
                        index = keys.Count;
                        keys.Add(key);
                        counts.Add(1);
                    }

                    prevKey = key;
                    havePrevKey = true;
                }
            }
        }
    }
    #endregion

    #region DistinctBy
    /// <summary>
    ///     Returns all distinct elements of the given source, where "distinctness" is determined via a projection and the
    ///     specified comparer for the projected type.
    /// </summary>
    /// <remarks>
    ///     This operator uses deferred execution and streams the results, although a set of already-seen keys is
    ///     retained. If a key is seen multiple times, only the first element with that key is returned.
    /// </remarks>
    /// <typeparam name="TSource">Type of the source sequence</typeparam>
    /// <typeparam name="TKey">Type of the projected element</typeparam>
    /// <param name="source">Source sequence</param>
    /// <param name="keySelector">Projection for determining "distinctness"</param>
    /// <param name="comparer">
    ///     The equality comparer to use to determine whether or not keys are equal. If null, the default
    ///     equality comparer for <c>TSource</c> is used.
    /// </param>
    /// <returns>
    ///     A sequence consisting of distinct elements from the source sequence, comparing them by the specified key
    ///     projection.
    /// </returns>
    public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey>? comparer = null) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        return _();
        IEnumerable<TSource> _() {
            var knownKeys = new HashSet<TKey>(comparer);
            foreach (var element in source) {
                if (knownKeys.Add(keySelector(element)))
                    yield return element;
            }
        }
    }
    #endregion

    #region EndsWith
    /// <summary>
    ///     Determines whether the end of the first sequence is equivalent to the second sequence, using the specified
    ///     element equality comparer.
    /// </summary>
    /// <typeparam name="T">Type of elements.</typeparam>
    /// <param name="first">The sequence to check.</param>
    /// <param name="second">The sequence to compare to.</param>
    /// <param name="comparer">Equality comparer to use.</param>
    /// <returns><c>true</c> if <paramref name="first" /> ends with elements equivalent to <paramref name="second" />.</returns>
    /// <remarks>
    ///     This is the <see cref="IEnumerable{T}" /> equivalent of <see cref="string.EndsWith(string)" /> and it calls
    ///     <see cref="IEqualityComparer{T}.Equals(T,T)" /> on pairs of elements at the same index.
    /// </remarks>
    public static bool EndsWith<T>(this IEnumerable<T> first, IEnumerable<T> second, IEqualityComparer<T>? comparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));

        comparer ??= EqualityComparer<T>.Default;

        List<T> secondList;
        return second is ICollection<T> collection
            ? Impl(second, collection.Count)
            : Impl(secondList = second.ToList(), secondList.Count);

        bool Impl(IEnumerable<T> snd, int count) {
            using var firstIter = first.TakeLast(count).GetEnumerator();
            return snd.All(item => firstIter.MoveNext() && comparer.Equals(firstIter.Current, item));
        }
    }
    #endregion

    #region ExceptBy
    /// <summary>
    ///     Returns the set of elements in the first sequence which aren't in the second sequence, according to a given
    ///     key selector.
    /// </summary>
    /// <remarks>
    ///     This is a set operation; if multiple elements in <paramref name="first" /> have equal keys, only the first
    ///     such element is returned. This operator uses deferred execution and streams the results, although a set of keys
    ///     from <paramref name="second" /> is immediately selected and retained.
    /// </remarks>
    /// <typeparam name="TSource">The type of the elements in the input sequences.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
    /// <param name="first">The sequence of potentially included elements.</param>
    /// <param name="second">
    ///     The sequence of elements whose keys may prevent elements in <paramref name="first" /> from being
    ///     returned.
    /// </param>
    /// <param name="keySelector">The mapping from source element to key.</param>
    /// <param name="keyComparer">
    ///     The equality comparer to use to determine whether or not keys are equal. If null, the default
    ///     equality comparer for <c>TSource</c> is used.
    /// </param>
    /// <returns>
    ///     A sequence of elements from <paramref name="first" /> whose key was not also a key for any element in
    ///     <paramref name="second" />.
    /// </returns>
    public static IEnumerable<TSource> ExceptBy<TSource, TKey>(this IEnumerable<TSource> first,
        IEnumerable<TSource> second,
        Func<TSource, TKey> keySelector,
        IEqualityComparer<TKey>? keyComparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));

        return _();
        IEnumerable<TSource> _() {
            // TODO Use ToHashSet
            var keys = new HashSet<TKey>(second.Select(keySelector), keyComparer);
            foreach (var element in first) {
                var key = keySelector(element);
                if (keys.Contains(key)) continue;
                yield return element;
                keys.Add(key);
            }
        }
    }
    #endregion

    #region Exclude
    /// <summary>Excludes a contiguous number of elements from a sequence starting at a given index.</summary>
    /// <typeparam name="T">The type of the elements of the sequence</typeparam>
    /// <param name="sequence">The sequence to exclude elements from</param>
    /// <param name="startIndex">The zero-based index at which to begin excluding elements</param>
    /// <param name="count">The number of elements to exclude</param>
    /// <returns>A sequence that excludes the specified portion of elements</returns>
    public static IEnumerable<T> Exclude<T>(this IEnumerable<T> sequence, int startIndex, int count) {
        if (sequence == null) throw new ArgumentNullException(nameof(sequence));
        if (startIndex < 0) throw new ArgumentOutOfRangeException(nameof(startIndex));
        if (count < 0) throw new ArgumentOutOfRangeException(nameof(count));

        if (count == 0) return sequence;

        return _();
        IEnumerable<T> _() {
            var index = -1;
            var endIndex = startIndex + count;
            using var iter = sequence.GetEnumerator();
            // yield the first part of the sequence
            while (iter.MoveNext() && ++index < startIndex) yield return iter.Current;
            // skip the next part (up to count items)
            while (++index < endIndex && iter.MoveNext()) continue;
            // yield the remainder of the sequence
            while (iter.MoveNext()) yield return iter.Current;
        }
    }
    #endregion

    #region Flatten
    /// <summary>Flattens a sequence containing arbitrarily-nested sequences.</summary>
    /// <param name="source">The sequence that will be flattened.</param>
    /// <returns>A sequence that contains the elements of <paramref name="source" /> and all nested sequences (except strings).</returns>
    /// <exception cref="ArgumentNullException"><paramref name="source" /> is null.</exception>
    public static IEnumerable<object> Flatten(this IEnumerable source) => Flatten(source, obj => obj is not string);

    /// <summary>
    ///     Flattens a sequence containing arbitrarily-nested sequences. An additional parameter specifies a predicate
    ///     function used to determine whether a nested <see cref="IEnumerable" /> should be flattened or not.
    /// </summary>
    /// <param name="source">The sequence that will be flattened.</param>
    /// <param name="predicate">
    ///     A function that receives each element that implements <see cref="IEnumerable" /> and indicates
    ///     if its elements should be recursively flattened into the resulting sequence.
    /// </param>
    /// <returns>
    ///     A sequence that contains the elements of <paramref name="source" /> and all nested sequences for which the
    ///     predicate function returned <c>true</c>.
    /// </returns>
    /// <exception cref="ArgumentNullException"><paramref name="source" /> is null.</exception>
    /// <exception cref="ArgumentNullException"><paramref name="predicate" /> is null.</exception>
    public static IEnumerable<object> Flatten(this IEnumerable source, Func<IEnumerable, bool> predicate) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (predicate == null) throw new ArgumentNullException(nameof(predicate));

        return _();
        IEnumerable<object> _() {
            var e = source.GetEnumerator();
            var stack = new Stack<IEnumerator>();

            stack.Push(e);

            try {
                while (stack.Count > 0) {
                    e = stack.Pop();

                reloop:

                    bool next;
                    while (next = e.MoveNext()) {
                        if (e.Current is IEnumerable inner && predicate(inner)) {
                            stack.Push(e);
                            e = inner.GetEnumerator();
                            goto reloop;
                        } else {
                            yield return e.Current;
                        }
                    }

                    if (!next) (e as IDisposable)?.Dispose();
                }
            } finally {
                (e as IDisposable)?.Dispose();
                foreach (var se in stack) (se as IDisposable)?.Dispose();
            }
        }
    }
    #endregion

    #region FullGroupJoin
    /// <summary>Performs a Full Group Join between the <paramref name="first" /> and <paramref name="second" /> sequences.</summary>
    /// <remarks>
    ///     This operator uses deferred execution and streams the results. The results are yielded in the order of the
    ///     elements found in the first sequence followed by those found only in the second. In addition, the callback
    ///     responsible for projecting the results is supplied with sequences which preserve their source order.
    /// </remarks>
    /// <typeparam name="TFirst">The type of the elements in the first input sequence</typeparam>
    /// <typeparam name="TSecond">The type of the elements in the second input sequence</typeparam>
    /// <typeparam name="TKey">The type of the key to use to join</typeparam>
    /// <param name="first">First sequence</param>
    /// <param name="second">Second sequence</param>
    /// <param name="firstKeySelector">The mapping from first sequence to key</param>
    /// <param name="secondKeySelector">The mapping from second sequence to key</param>
    /// <param name="comparer">
    ///     The equality comparer to use to determine whether or not keys are equal. If null, the default
    ///     equality comparer for <c>TKey</c> is used.
    /// </param>
    /// <returns>A sequence of elements joined from <paramref name="first" /> and <paramref name="second" />.</returns>
    public static IEnumerable<(TKey Key, IEnumerable<TFirst> First, IEnumerable<TSecond> Second)> FullGroupJoin<TFirst, TSecond, TKey>(this IEnumerable<TFirst> first,
        IEnumerable<TSecond> second,
        Func<TFirst, TKey> firstKeySelector,
        Func<TSecond, TKey> secondKeySelector,
        IEqualityComparer<TKey>? comparer = null) =>
        FullGroupJoin(first, second, firstKeySelector, secondKeySelector, ValueTuple.Create, comparer);

    /// <summary>Performs a full group-join between two sequences.</summary>
    /// <remarks>
    ///     This operator uses deferred execution and streams the results. The results are yielded in the order of the
    ///     elements found in the first sequence followed by those found only in the second. In addition, the callback
    ///     responsible for projecting the results is supplied with sequences which preserve their source order.
    /// </remarks>
    /// <typeparam name="TFirst">The type of the elements in the first input sequence</typeparam>
    /// <typeparam name="TSecond">The type of the elements in the second input sequence</typeparam>
    /// <typeparam name="TKey">The type of the key to use to join</typeparam>
    /// <typeparam name="TResult">The type of the elements of the resulting sequence</typeparam>
    /// <param name="first">First sequence</param>
    /// <param name="second">Second sequence</param>
    /// <param name="firstKeySelector">The mapping from first sequence to key</param>
    /// <param name="secondKeySelector">The mapping from second sequence to key</param>
    /// <param name="resultSelector">Function to apply to each pair of elements plus the key</param>
    /// <param name="comparer">
    ///     The equality comparer to use to determine whether or not keys are equal. If null, the default
    ///     equality comparer for <c>TKey</c> is used.
    /// </param>
    /// <returns>A sequence of elements joined from <paramref name="first" /> and <paramref name="second" />.</returns>
    public static IEnumerable<TResult> FullGroupJoin<TFirst, TSecond, TKey, TResult>(this IEnumerable<TFirst> first,
        IEnumerable<TSecond> second,
        Func<TFirst, TKey> firstKeySelector,
        Func<TSecond, TKey> secondKeySelector,
        Func<TKey, IEnumerable<TFirst>, IEnumerable<TSecond>, TResult>? resultSelector = null,
        IEqualityComparer<TKey>? comparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));
        if (firstKeySelector == null) throw new ArgumentNullException(nameof(firstKeySelector));
        if (secondKeySelector == null) throw new ArgumentNullException(nameof(secondKeySelector));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        return _();
        IEnumerable<TResult> _() {
            comparer ??= EqualityComparer<TKey>.Default;

            var alookup = Lookup<TKey, TFirst>.CreateForJoin(first, firstKeySelector, comparer);
            var blookup = Lookup<TKey, TSecond>.CreateForJoin(second, secondKeySelector, comparer);

            foreach (var a in alookup) yield return resultSelector(a.Key, a, blookup[a.Key]);

            foreach (var b in blookup) {
                if (alookup.Contains(b.Key)) continue;
                // We can skip the lookup because we are iterating over keys not found in the first sequence
                yield return resultSelector(b.Key, Enumerable.Empty<TFirst>(), b);
            }
        }
    }
    #endregion

    #region FullJoin
    /// <summary>
    ///     Performs a full outer join on two homogeneous sequences. Additional arguments specify key selection functions,
    ///     result projection functions and a key comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in the source sequence.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by the key selector function.</typeparam>
    /// <typeparam name="TResult">The type of the result elements.</typeparam>
    /// <param name="first">The first sequence to join fully.</param>
    /// <param name="second">The second sequence to join fully.</param>
    /// <param name="keySelector">Function that projects the key given an element of one of the sequences to join.</param>
    /// <param name="firstSelector">
    ///     Function that projects the result given just an element from <paramref name="first" />
    ///     where there is no corresponding element in <paramref name="second" />.
    /// </param>
    /// <param name="secondSelector">
    ///     Function that projects the result given just an element from <paramref name="second" />
    ///     where there is no corresponding element in <paramref name="first" />.
    /// </param>
    /// <param name="bothSelector">
    ///     Function that projects the result given an element from <paramref name="first" /> and an
    ///     element from <paramref name="second" /> that match on a common key.
    /// </param>
    /// <param name="comparer">The <see cref="IEqualityComparer{T}" /> instance used to compare keys for equality.</param>
    /// <returns>A sequence containing results projected from a full outer join of the two input sequences.</returns>
    public static IEnumerable<TResult> FullJoin<TSource, TKey, TResult>(
        this IEnumerable<TSource> first,
        IEnumerable<TSource> second,
        Func<TSource, TKey> keySelector,
        Func<TSource, TResult> firstSelector,
        Func<TSource, TResult> secondSelector,
        Func<TSource, TSource, TResult> bothSelector,
        IEqualityComparer<TKey>? comparer = null) {
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
        return first.FullJoin(second,
            keySelector, keySelector,
            firstSelector, secondSelector, bothSelector,
            comparer);
    }

    /// <summary>
    ///     Performs a full outer join on two heterogeneous sequences. Additional arguments specify key selection
    ///     functions, result projection functions and a key comparer.
    /// </summary>
    /// <typeparam name="TFirst">The type of elements in the first sequence.</typeparam>
    /// <typeparam name="TSecond">The type of elements in the second sequence.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by the key selector functions.</typeparam>
    /// <typeparam name="TResult">The type of the result elements.</typeparam>
    /// <param name="first">The first sequence to join fully.</param>
    /// <param name="second">The second sequence to join fully.</param>
    /// <param name="firstKeySelector">Function that projects the key given an element from <paramref name="first" />.</param>
    /// <param name="secondKeySelector">Function that projects the key given an element from <paramref name="second" />.</param>
    /// <param name="firstSelector">
    ///     Function that projects the result given just an element from <paramref name="first" />
    ///     where there is no corresponding element in <paramref name="second" />.
    /// </param>
    /// <param name="secondSelector">
    ///     Function that projects the result given just an element from <paramref name="second" />
    ///     where there is no corresponding element in <paramref name="first" />.
    /// </param>
    /// <param name="bothSelector">
    ///     Function that projects the result given an element from <paramref name="first" /> and an
    ///     element from <paramref name="second" /> that match on a common key.
    /// </param>
    /// <param name="comparer">The <see cref="IEqualityComparer{T}" /> instance used to compare keys for equality.</param>
    /// <returns>A sequence containing results projected from a full outer join of the two input sequences.</returns>
    public static IEnumerable<TResult> FullJoin<TFirst, TSecond, TKey, TResult>(
        this IEnumerable<TFirst> first,
        IEnumerable<TSecond> second,
        Func<TFirst, TKey> firstKeySelector,
        Func<TSecond, TKey> secondKeySelector,
        Func<TFirst, TResult> firstSelector,
        Func<TSecond, TResult> secondSelector,
        Func<TFirst, TSecond, TResult> bothSelector,
        IEqualityComparer<TKey>? comparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));
        if (firstKeySelector == null) throw new ArgumentNullException(nameof(firstKeySelector));
        if (secondKeySelector == null) throw new ArgumentNullException(nameof(secondKeySelector));
        if (firstSelector == null) throw new ArgumentNullException(nameof(firstSelector));
        if (secondSelector == null) throw new ArgumentNullException(nameof(secondSelector));
        if (bothSelector == null) throw new ArgumentNullException(nameof(bothSelector));

        return _();
        IEnumerable<TResult> _() {
            var seconds = second.Select(e => new KeyValuePair<TKey, TSecond>(secondKeySelector(e), e)).ToArray();
            var secondLookup = seconds.ToLookup(e => e.Key, e => e.Value, comparer);
            var firstKeys = new HashSet<TKey>(comparer);

            foreach (var fe in first) {
                var key = firstKeySelector(fe);
                firstKeys.Add(key);

                using var se = secondLookup[key].GetEnumerator();
                if (se.MoveNext()) {
                    do {
                        yield return bothSelector(fe, se.Current);
                    } while (se.MoveNext());
                } else {
                    se.Dispose();
                    yield return firstSelector(fe);
                }
            }

            foreach (var se in seconds) {
                if (!firstKeys.Contains(se.Key))
                    yield return secondSelector(se.Value);
            }
        }
    }
    #endregion

    #region GroupAdjacent
    /// <summary>
    ///     Groups the adjacent elements of a sequence according to a specified key selector function and compares the
    ///     keys by using a specified comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
    /// <param name="source">A sequence whose elements to group.</param>
    /// <param name="keySelector">A function to extract the key for each element.</param>
    /// <param name="comparer">An <see cref="IEqualityComparer{T}" /> to compare keys.</param>
    /// <returns>
    ///     A sequence of groupings where each grouping (<see cref="IGrouping{TKey,TElement}" />) contains the key and the
    ///     adjacent elements in the same order as found in the source sequence.
    /// </returns>
    /// <remarks>
    ///     This method is implemented by using deferred execution and streams the groupings. The grouping elements,
    ///     however, are buffered. Each grouping is therefore yielded as soon as it is complete and before the next grouping
    ///     occurs.
    /// </remarks>
    public static IEnumerable<IGrouping<TKey, TSource>> GroupAdjacent<TSource, TKey>(
        this IEnumerable<TSource> source,
        Func<TSource, TKey> keySelector,
        IEqualityComparer<TKey>? comparer = null) where TKey : notnull {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));

        return GroupAdjacent(source, keySelector, e => e, comparer);
    }

    /// <summary>
    ///     Groups the adjacent elements of a sequence according to a specified key selector function. The keys are
    ///     compared by using a comparer and each group's elements are projected by using a specified function.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
    /// <typeparam name="TElement">The type of the elements in the resulting groupings.</typeparam>
    /// <param name="source">A sequence whose elements to group.</param>
    /// <param name="keySelector">A function to extract the key for each element.</param>
    /// <param name="elementSelector">A function to map each source element to an element in the resulting grouping.</param>
    /// <param name="comparer">An <see cref="IEqualityComparer{T}" /> to compare keys.</param>
    /// <returns>
    ///     A sequence of groupings where each grouping (<see cref="IGrouping{TKey,TElement}" />) contains the key and the
    ///     adjacent elements (of type <typeparamref name="TElement" />) in the same order as found in the source sequence.
    /// </returns>
    /// <remarks>
    ///     This method is implemented by using deferred execution and streams the groupings. The grouping elements,
    ///     however, are buffered. Each grouping is therefore yielded as soon as it is complete and before the next grouping
    ///     occurs.
    /// </remarks>
    public static IEnumerable<IGrouping<TKey, TElement>> GroupAdjacent<TSource, TKey, TElement>(
        this IEnumerable<TSource> source,
        Func<TSource, TKey> keySelector,
        Func<TSource, TElement> elementSelector,
        IEqualityComparer<TKey>? comparer = null) where TKey : notnull {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
        if (elementSelector == null) throw new ArgumentNullException(nameof(elementSelector));

        return GroupAdjacentImpl(source, keySelector, elementSelector, CreateGroupAdjacentGrouping,
            comparer ?? EqualityComparer<TKey>.Default);
    }

    /// <summary>
    ///     Groups the adjacent elements of a sequence according to a specified key selector function. The keys are
    ///     compared by using a comparer and each group's elements are projected by using a specified function.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
    /// <typeparam name="TResult">The type of the elements in the resulting sequence.</typeparam>
    /// <param name="source">A sequence whose elements to group.</param>
    /// <param name="keySelector">A function to extract the key for each element.</param>
    /// <param name="resultSelector">A function to map each key and associated source elements to a result object.</param>
    /// <param name="comparer">An <see cref="IEqualityComparer{TKey}" /> to compare keys.</param>
    /// <returns>
    ///     A collection of elements of type <typeparamref name="TResult" /> where each element represents a projection
    ///     over a group and its key.
    /// </returns>
    /// <remarks>
    ///     This method is implemented by using deferred execution and streams the groupings. The grouping elements,
    ///     however, are buffered. Each grouping is therefore yielded as soon as it is complete and before the next grouping
    ///     occurs.
    /// </remarks>
    public static IEnumerable<TResult> GroupAdjacent<TSource, TKey, TResult>(
        this IEnumerable<TSource> source,
        Func<TSource, TKey> keySelector,
        Func<TKey, IEnumerable<TSource>, TResult> resultSelector,
        IEqualityComparer<TKey>? comparer = null) where TKey : notnull {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        // This should be removed once the target framework is bumped to something that supports covariance
        TResult ResultSelectorWrapper(TKey key, IList<TSource> group) => resultSelector(key, group);
        return GroupAdjacentImpl(source, keySelector, i => i, ResultSelectorWrapper,
            comparer ?? EqualityComparer<TKey>.Default);
    }

    private static IEnumerable<TResult> GroupAdjacentImpl<TSource, TKey, TElement, TResult>(
        this IEnumerable<TSource> source,
        Func<TSource, TKey> keySelector,
        Func<TSource, TElement> elementSelector,
        Func<TKey, IList<TElement>, TResult> resultSelector,
        IEqualityComparer<TKey>? comparer = null) where TKey : notnull {
        Debug.Assert(source != null);
        Debug.Assert(keySelector != null);
        Debug.Assert(elementSelector != null);
        Debug.Assert(resultSelector != null);
        Debug.Assert(comparer != null);

        using var iterator = source.GetEnumerator();
        var group = default(TKey);
        List<TElement>? members = null;

        while (iterator.MoveNext()) {
            var key = keySelector(iterator.Current);
            var element = elementSelector(iterator.Current);
            if (members != null && comparer.Equals(group, key)) {
                members.Add(element);
            } else {
                if (members != null) yield return resultSelector(group!, members);
                group = key;
                members = new List<TElement> { element };
            }
        }

        if (members != null) yield return resultSelector(group!, members);
    }

    private static IGrouping<TKey, TElement> CreateGroupAdjacentGrouping<TKey, TElement>(TKey key, IList<TElement> members) {
        Debug.Assert(members != null);
        return GroupingCreate(key, members.IsReadOnly ? members : new ReadOnlyCollection<TElement>(members));
    }

    private static Grouping<TKey, TElement> GroupingCreate<TKey, TElement>(TKey key, IEnumerable<TElement> members) => new(key, members);

    [Serializable]
    private sealed class Grouping<TKey, TElement>: IGrouping<TKey, TElement> {
        #region Fields / Properties
        private readonly IEnumerable<TElement> _members;
        #endregion

        #region Constructors
        public Grouping(TKey key, IEnumerable<TElement> members) {
            Debug.Assert(members != null);
            Key = key;
            _members = members;
        }
        #endregion

        #region IGrouping<TKey,TElement> Members
        public TKey Key { get; }

        public IEnumerator<TElement> GetEnumerator() => _members.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        #endregion
    }
    #endregion

    #region Insert
    /// <summary>Inserts the elements of a sequence into another sequence at a specified index.</summary>
    /// <typeparam name="T">Type of the elements of the source sequence.</typeparam>
    /// <param name="first">The source sequence.</param>
    /// <param name="second">The sequence that will be inserted.</param>
    /// <param name="index">The zero-based index at which to insert elements from <paramref name="second" />.</param>
    /// <returns>
    ///     A sequence that contains the elements of <paramref name="first" /> plus the elements of
    ///     <paramref name="second" /> inserted at the given index.
    /// </returns>
    /// <exception cref="ArgumentNullException"><paramref name="first" /> is null.</exception>
    /// <exception cref="ArgumentNullException"><paramref name="second" /> is null.</exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="index" /> is negative.</exception>
    /// <exception cref="ArgumentOutOfRangeException">
    ///     Thrown lazily if <paramref name="index" /> is greater than the length of
    ///     <paramref name="first" />. The validation occurs when yielding the next element after having iterated
    ///     <paramref name="first" /> entirely.
    /// </exception>
    public static IEnumerable<T> Insert<T>(this IEnumerable<T> first, IEnumerable<T> second, int index) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));
        if (index < 0) throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative.");

        return _();
        IEnumerable<T> _() {
            var i = -1;

            using var iter = first.GetEnumerator();
            while (++i < index && iter.MoveNext()) yield return iter.Current;

            if (i < index) throw new ArgumentOutOfRangeException(nameof(index), "Insertion index is greater than the length of the first sequence.");

            foreach (var item in second) yield return item;

            while (iter.MoveNext()) yield return iter.Current;
        }
    }
    #endregion

    #region LeftJoin
    /// <summary>
    ///     Performs a left outer join on two homogeneous sequences. Additional arguments specify key selection functions,
    ///     result projection functions and a key comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in the source sequence.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by the key selector function.</typeparam>
    /// <typeparam name="TResult">The type of the result elements.</typeparam>
    /// <param name="first">The first sequence of the join operation.</param>
    /// <param name="second">The second sequence of the join operation.</param>
    /// <param name="keySelector">Function that projects the key given an element of one of the sequences to join.</param>
    /// <param name="firstSelector">
    ///     Function that projects the result given just an element from <paramref name="first" />
    ///     where there is no corresponding element in <paramref name="second" />.
    /// </param>
    /// <param name="bothSelector">
    ///     Function that projects the result given an element from <paramref name="first" /> and an
    ///     element from <paramref name="second" /> that match on a common key.
    /// </param>
    /// <param name="comparer">The <see cref="IEqualityComparer{T}" /> instance used to compare keys for equality.</param>
    /// <returns>A sequence containing results projected from a left outer join of the two input sequences.</returns>
    public static IEnumerable<TResult> LeftJoin<TSource, TKey, TResult>(
        this IEnumerable<TSource> first,
        IEnumerable<TSource> second,
        Func<TSource, TKey> keySelector,
        Func<TSource, TResult> firstSelector,
        Func<TSource, TSource, TResult> bothSelector,
        IEqualityComparer<TKey>? comparer = null) {
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
        return first.LeftJoin(second,
            keySelector, keySelector,
            firstSelector, bothSelector,
            comparer);
    }

    /// <summary>
    ///     Performs a left outer join on two heterogeneous sequences. Additional arguments specify key selection
    ///     functions, result projection functions and a key comparer.
    /// </summary>
    /// <typeparam name="TFirst">The type of elements in the first sequence.</typeparam>
    /// <typeparam name="TSecond">The type of elements in the second sequence.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by the key selector functions.</typeparam>
    /// <typeparam name="TResult">The type of the result elements.</typeparam>
    /// <param name="first">The first sequence of the join operation.</param>
    /// <param name="second">The second sequence of the join operation.</param>
    /// <param name="firstKeySelector">Function that projects the key given an element from <paramref name="first" />.</param>
    /// <param name="secondKeySelector">Function that projects the key given an element from <paramref name="second" />.</param>
    /// <param name="firstSelector">
    ///     Function that projects the result given just an element from <paramref name="first" />
    ///     where there is no corresponding element in <paramref name="second" />.
    /// </param>
    /// <param name="bothSelector">
    ///     Function that projects the result given an element from <paramref name="first" /> and an
    ///     element from <paramref name="second" /> that match on a common key.
    /// </param>
    /// <param name="comparer">The <see cref="IEqualityComparer{T}" /> instance used to compare keys for equality.</param>
    /// <returns>A sequence containing results projected from a left outer join of the two input sequences.</returns>
    public static IEnumerable<TResult> LeftJoin<TFirst, TSecond, TKey, TResult>(
        this IEnumerable<TFirst> first,
        IEnumerable<TSecond> second,
        Func<TFirst, TKey> firstKeySelector,
        Func<TSecond, TKey> secondKeySelector,
        Func<TFirst, TResult> firstSelector,
        Func<TFirst, TSecond, TResult> bothSelector,
        IEqualityComparer<TKey>? comparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));
        if (firstKeySelector == null) throw new ArgumentNullException(nameof(firstKeySelector));
        if (secondKeySelector == null) throw new ArgumentNullException(nameof(secondKeySelector));
        if (firstSelector == null) throw new ArgumentNullException(nameof(firstSelector));
        if (bothSelector == null) throw new ArgumentNullException(nameof(bothSelector));

        static KeyValuePair<TK, TV> Pair<TK, TV>(TK k, TV v) => new(k, v);

        return // TODO replace KeyValuePair<,> with (,) for clarity
            from j in first.GroupJoin(second, firstKeySelector, secondKeySelector,
                (f, ss) => Pair(f, from s in ss select Pair(true, s)),
                comparer)
            from s in j.Value.DefaultIfEmpty()
            select s.Key ? bothSelector(j.Key, s.Value) : firstSelector(j.Key);
    }
    #endregion

    #region MinBy
    /// <summary>
    ///     Returns the minimal element of the given sequence, based on the given projection and the specified comparer
    ///     for projected values.
    /// </summary>
    /// <remarks>
    ///     If more than one element has the minimal projected value, the first one encountered will be returned. This
    ///     operator uses immediate execution, but only buffers a single result (the current minimal element).
    /// </remarks>
    /// <typeparam name="TSource">Type of the source sequence</typeparam>
    /// <typeparam name="TKey">Type of the projected element</typeparam>
    /// <param name="source">Source sequence</param>
    /// <param name="selector">Selector to use to pick the results to compare</param>
    /// <param name="comparer">Comparer to use to compare projected values</param>
    /// <returns>The minimal element, according to the projection.</returns>
    /// <exception cref="ArgumentNullException">
    ///     <paramref name="source" />, <paramref name="selector" /> or
    ///     <paramref name="comparer" /> is null
    /// </exception>
    /// <exception cref="InvalidOperationException"><paramref name="source" /> is empty</exception>
    public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source,
        Func<TSource, TKey> selector, IComparer<TKey>? comparer = null) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (selector == null) throw new ArgumentNullException(nameof(selector));

        comparer ??= Comparer<TKey>.Default;
        return ExtremumBy(source, selector, (x, y) => -Math.Sign(comparer.Compare(x, y)));
    }
    #endregion

    #region MaxBy
    /// <summary>
    ///     Returns the maximal element of the given sequence, based on the given projection and the specified comparer
    ///     for projected values.
    /// </summary>
    /// <remarks>
    ///     If more than one element has the maximal projected value, the first one encountered will be returned. This
    ///     operator uses immediate execution, but only buffers a single result (the current maximal element).
    /// </remarks>
    /// <typeparam name="TSource">Type of the source sequence</typeparam>
    /// <typeparam name="TKey">Type of the projected element</typeparam>
    /// <param name="source">Source sequence</param>
    /// <param name="selector">Selector to use to pick the results to compare</param>
    /// <param name="comparer">Comparer to use to compare projected values</param>
    /// <returns>The maximal element, according to the projection.</returns>
    /// <exception cref="ArgumentNullException">
    ///     <paramref name="source" />, <paramref name="selector" /> or
    ///     <paramref name="comparer" /> is null
    /// </exception>
    /// <exception cref="InvalidOperationException"><paramref name="source" /> is empty</exception>
    public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source,
        Func<TSource, TKey> selector, IComparer<TKey>? comparer = null) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (selector == null) throw new ArgumentNullException(nameof(selector));

        comparer ??= Comparer<TKey>.Default;
        return ExtremumBy(source, selector, (x, y) => comparer.Compare(x, y));
    }

    // > In mathematical analysis, the maxima and minima (the respective
    // > plurals of maximum and minimum) of a function, known collectively
    // > as extrema (the plural of extremum), ...
    // >
    // > - https://en.wikipedia.org/wiki/Maxima_and_minima

    private static TSource ExtremumBy<TSource, TKey>(IEnumerable<TSource> source,
        Func<TSource, TKey> selector, Func<TKey, TKey, int> comparer) {
        using var sourceIterator = source.GetEnumerator();
        if (!sourceIterator.MoveNext()) throw new InvalidOperationException("Sequence contains no elements");

        var extremum = sourceIterator.Current;
        var key = selector(extremum);
        while (sourceIterator.MoveNext()) {
            var candidate = sourceIterator.Current;
            var candidateProjected = selector(candidate);
            if (comparer(candidateProjected, key) > 0) {
                extremum = candidate;
                key = candidateProjected;
            }
        }

        return extremum;
    }
    #endregion

    #region Move
    /// <summary>Returns a sequence with a range of elements in the source sequence moved to a new offset.</summary>
    /// <typeparam name="T">Type of the source sequence.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="fromIndex">The zero-based index identifying the first element in the range of elements to move.</param>
    /// <param name="count">The count of items to move.</param>
    /// <param name="toIndex">The index where the specified range will be moved.</param>
    /// <returns>A sequence with the specified range moved to the new position.</returns>
    /// <remarks>This operator uses deferred execution and streams its results.</remarks>
    /// <example>
    ///     <code>
    /// var result = Enumerable.Range(0, 6).Move(3, 2, 0);
    /// </code>
    ///     The <c>result</c> variable will contain <c>{ 3, 4, 0, 1, 2, 5 }</c>.
    /// </example>
    public static IEnumerable<T> Move<T>(this IEnumerable<T> source, int fromIndex, int count, int toIndex) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (fromIndex < 0) throw new ArgumentOutOfRangeException(nameof(fromIndex), "The source index cannot be negative.");
        if (count < 0) throw new ArgumentOutOfRangeException(nameof(count), "Count cannot be negative.");
        if (toIndex < 0) throw new ArgumentOutOfRangeException(nameof(toIndex), "Target index of range to move cannot be negative.");

        if (toIndex == fromIndex || count == 0) return source;

        return toIndex < fromIndex
            ? _(toIndex, fromIndex - toIndex, count)
            : _(fromIndex, count, toIndex - fromIndex);

        IEnumerable<T> _(int bufferStartIndex, int bufferSize, int bufferYieldIndex) {
            using var e = source.GetEnumerator();
            for (var i = 0; i < bufferStartIndex && e.MoveNext(); i++) yield return e.Current;

            var buffer = new T[bufferSize];
            var length = 0;

            for (; length < bufferSize && e.MoveNext(); length++) buffer[length] = e.Current;

            for (var i = 0; i < bufferYieldIndex && e.MoveNext(); i++) yield return e.Current;

            for (var i = 0; i < length; i++) yield return buffer[i];

            while (e.MoveNext()) yield return e.Current;
        }
    }
    #endregion

    #region OrderedMerge
    /// <summary>
    ///     Merges two ordered sequences into one with an additional parameter specifying how to compare the elements of
    ///     the sequences. Where the elements equal in both sequences, the element from the first sequence is returned in the
    ///     resulting sequence.
    /// </summary>
    /// <typeparam name="T">Type of elements in input and output sequences.</typeparam>
    /// <param name="first">The first input sequence.</param>
    /// <param name="second">The second input sequence.</param>
    /// <param name="comparer">An <see cref="IComparer{T}" /> to compare elements.</param>
    /// <returns>A sequence with elements from the two input sequences merged, as in a full outer join.</returns>
    /// <remarks>This method uses deferred execution. The behavior is undefined if the sequences are unordered as inputs.</remarks>
    public static IEnumerable<T> OrderedMerge<T>(
        this IEnumerable<T> first,
        IEnumerable<T> second,
        IComparer<T>? comparer = null) =>
        OrderedMerge(first, second, e => e, f => f, s => s, (a, _) => a, comparer);

    /// <summary>
    ///     Merges two ordered sequences into one. Additional parameters specify the element key by which the sequences
    ///     are ordered, the result when element is found in first sequence but not in the second, the result when element is
    ///     found in second sequence but not in the first, the result when elements are found in both sequences and a method
    ///     for comparing keys.
    /// </summary>
    /// <typeparam name="T">Type of elements in source sequences.</typeparam>
    /// <typeparam name="TKey">Type of keys used for merging.</typeparam>
    /// <typeparam name="TResult">Type of elements in the returned sequence.</typeparam>
    /// <param name="first">The first input sequence.</param>
    /// <param name="second">The second input sequence.</param>
    /// <param name="keySelector">Function to extract a key given an element.</param>
    /// <param name="firstSelector">
    ///     Function to project the result element when only the first sequence yields a source
    ///     element.
    /// </param>
    /// <param name="secondSelector">
    ///     Function to project the result element when only the second sequence yields a source
    ///     element.
    /// </param>
    /// <param name="bothSelector">
    ///     Function to project the result element when only both sequences yield a source element whose
    ///     keys are equal.
    /// </param>
    /// <param name="comparer">An <see cref="IComparer{T}" /> to compare keys.</param>
    /// <returns>A sequence with projections from the two input sequences merged according to a key, as in a full outer join.</returns>
    /// <remarks>
    ///     This method uses deferred execution. The behavior is undefined if the sequences are unordered (by key) as
    ///     inputs.
    /// </remarks>
    public static IEnumerable<TResult> OrderedMerge<T, TKey, TResult>(
        this IEnumerable<T> first,
        IEnumerable<T> second,
        Func<T, TKey> keySelector,
        Func<T, TResult> firstSelector,
        Func<T, TResult> secondSelector,
        Func<T, T, TResult> bothSelector,
        IComparer<TKey>? comparer = null) {
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector)); // Argument name changes to 'firstKeySelector'
        return OrderedMerge(first, second, keySelector, keySelector, firstSelector, secondSelector, bothSelector, comparer);
    }

    /// <summary>
    ///     Merges two heterogeneous sequences ordered by a common key type into a homogeneous one. Additional parameters
    ///     specify the element key by which the sequences are ordered, the result when element is found in first sequence but
    ///     not in the second, the result when element is found in second sequence but not in the first, the result when
    ///     elements are found in both sequences and a method for comparing keys.
    /// </summary>
    /// <typeparam name="TFirst">Type of elements in the first sequence.</typeparam>
    /// <typeparam name="TSecond">Type of elements in the second sequence.</typeparam>
    /// <typeparam name="TKey">Type of keys used for merging.</typeparam>
    /// <typeparam name="TResult">Type of elements in the returned sequence.</typeparam>
    /// <param name="first">The first input sequence.</param>
    /// <param name="second">The second input sequence.</param>
    /// <param name="firstKeySelector">Function to extract a key given an element from the first sequence.</param>
    /// <param name="secondKeySelector">Function to extract a key given an element from the second sequence.</param>
    /// <param name="firstSelector">
    ///     Function to project the result element when only the first sequence yields a source
    ///     element.
    /// </param>
    /// <param name="secondSelector">
    ///     Function to project the result element when only the second sequence yields a source
    ///     element.
    /// </param>
    /// <param name="bothSelector">
    ///     Function to project the result element when only both sequences yield a source element whose
    ///     keys are equal.
    /// </param>
    /// <param name="comparer">An <see cref="IComparer{T}" /> to compare keys.</param>
    /// <returns>A sequence with projections from the two input sequences merged according to a key, as in a full outer join.</returns>
    /// <remarks>
    ///     This method uses deferred execution. The behavior is undefined if the sequences are unordered (by key) as
    ///     inputs.
    /// </remarks>
    public static IEnumerable<TResult> OrderedMerge<TFirst, TSecond, TKey, TResult>(
        this IEnumerable<TFirst> first,
        IEnumerable<TSecond> second,
        Func<TFirst, TKey> firstKeySelector,
        Func<TSecond, TKey> secondKeySelector,
        Func<TFirst, TResult> firstSelector,
        Func<TSecond, TResult> secondSelector,
        Func<TFirst, TSecond, TResult> bothSelector,
        IComparer<TKey>? comparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));
        if (firstKeySelector == null) throw new ArgumentNullException(nameof(firstKeySelector));
        if (secondKeySelector == null) throw new ArgumentNullException(nameof(secondKeySelector));
        if (firstSelector == null) throw new ArgumentNullException(nameof(firstSelector));
        if (bothSelector == null) throw new ArgumentNullException(nameof(bothSelector));
        if (secondSelector == null) throw new ArgumentNullException(nameof(secondSelector));

        comparer ??= Comparer<TKey>.Default;
        return _();
        IEnumerable<TResult> _() {
            using var e1 = first.GetEnumerator();
            using var e2 = second.GetEnumerator();
            var gotFirst = e1.MoveNext();
            var gotSecond = e2.MoveNext();

            while (gotFirst || gotSecond) {
                if (gotFirst && gotSecond) {
                    var element1 = e1.Current;
                    var key1 = firstKeySelector(element1);
                    var element2 = e2.Current;
                    var key2 = secondKeySelector(element2);
                    var comparison = comparer.Compare(key1, key2);

                    if (comparison < 0) {
                        yield return firstSelector(element1);
                        gotFirst = e1.MoveNext();
                    } else if (comparison > 0) {
                        yield return secondSelector(element2);
                        gotSecond = e2.MoveNext();
                    } else {
                        yield return bothSelector(element1, element2);
                        gotFirst = e1.MoveNext();
                        gotSecond = e2.MoveNext();
                    }
                } else if (gotSecond) {
                    yield return secondSelector(e2.Current);
                    gotSecond = e2.MoveNext();
                } else // (gotFirst)
                {
                    yield return firstSelector(e1.Current);
                    gotFirst = e1.MoveNext();
                }
            }
        }
    }
    #endregion

    #region Pad
    /// <summary>Pads a sequence with a given filler value if it is narrower (shorter in length) than a given width.</summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
    /// <param name="source">The sequence to pad.</param>
    /// <param name="width">The width/length below which to pad.</param>
    /// <param name="padding">The value to use for padding.</param>
    /// <returns>
    ///     Returns a sequence that is at least as wide/long as the width/length specified by the
    ///     <paramref name="width" /> parameter.
    /// </returns>
    /// <remarks>This operator uses deferred execution and streams its results.</remarks>
    /// <example>
    ///     <code>
    /// int[] numbers = { 123, 456, 789 };
    /// IEnumerable&lt;int&gt; result = numbers.Pad(5, -1);
    /// </code>
    ///     The <c>result</c> variable, when iterated over, will yield 123, 456, and 789 followed by two occurrences of -1, in
    ///     turn.
    /// </example>
    public static IEnumerable<TSource?> Pad<TSource>(this IEnumerable<TSource> source, int width, TSource? padding = default) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (width < 0) throw new ArgumentException(null, nameof(width));
        return PadImpl(source, width, padding, null);
    }

    /// <summary>Pads a sequence with a dynamic filler value if it is narrower (shorter in length) than a given width.</summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
    /// <param name="source">The sequence to pad.</param>
    /// <param name="width">The width/length below which to pad.</param>
    /// <param name="paddingSelector">Function to calculate padding.</param>
    /// <returns>
    ///     Returns a sequence that is at least as wide/long as the width/length specified by the
    ///     <paramref name="width" /> parameter.
    /// </returns>
    /// <remarks>This operator uses deferred execution and streams its results.</remarks>
    /// <example>
    ///     <code>
    /// int[] numbers = { 0, 1, 2 };
    /// IEnumerable&lt;int&gt; result = numbers.Pad(5, i => -i);
    /// </code>
    ///     The <c>result</c> variable, when iterated over, will yield 0, 1, 2, -3 and -4, in turn.
    /// </example>
    public static IEnumerable<TSource?> Pad<TSource>(this IEnumerable<TSource> source, int width, Func<int, TSource>? paddingSelector = null) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (paddingSelector == null) throw new ArgumentNullException(nameof(paddingSelector));
        if (width < 0) throw new ArgumentException(null, nameof(width));
        return PadImpl(source, width, default, paddingSelector);
    }

    private static IEnumerable<T?> PadImpl<T>(IEnumerable<T> source,
        int width, T? padding, Func<int, T>? paddingSelector = null) {
        Debug.Assert(source != null);
        Debug.Assert(width >= 0);

        var count = 0;
        foreach (var item in source) {
            yield return item;
            count++;
        }
        while (count < width) {
            yield return paddingSelector != null ? paddingSelector(count) : padding;
            count++;
        }
    }
    #endregion

    #region PadStart
    /// <summary>
    ///     Pads a sequence with a given filler value in the beginning if it is narrower (shorter in length) than a given
    ///     width. An additional parameter specifies the value to use for padding.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
    /// <param name="source">The sequence to pad.</param>
    /// <param name="width">The width/length below which to pad.</param>
    /// <param name="padding">The value to use for padding.</param>
    /// <returns>
    ///     Returns a sequence that is at least as wide/long as the width/length specified by the
    ///     <paramref name="width" /> parameter.
    /// </returns>
    /// <remarks>This operator uses deferred execution and streams its results.</remarks>
    /// <example>
    ///     <code>
    /// int[] numbers = { 123, 456, 789 };
    /// var result = numbers.PadLeft(5, -1);
    /// </code>
    ///     The <c>result</c> variable will contain <c>{ -1, -1, 123, 456, 789 }</c>.
    /// </example>
    public static IEnumerable<TSource?> PadStart<TSource>(this IEnumerable<TSource> source, int width, TSource? padding = default) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (width < 0) throw new ArgumentException(null, nameof(width));
        return PadLeftImpl(source, width, padding, null);
    }

    /// <summary>
    ///     Pads a sequence with a dynamic filler value in the beginning if it is narrower (shorter in length) than a
    ///     given width. An additional parameter specifies the function to calculate padding.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
    /// <param name="source">The sequence to pad.</param>
    /// <param name="width">The width/length below which to pad.</param>
    /// <param name="paddingSelector">Function to calculate padding.</param>
    /// <returns>
    ///     Returns a sequence that is at least as wide/long as the width/length specified by the
    ///     <paramref name="width" /> parameter.
    /// </returns>
    /// <remarks>This operator uses deferred execution and streams its results.</remarks>
    /// <example>
    ///     <code>
    /// int[] numbers = { 123, 456, 789 };
    /// var result = numbers.PadLeft(6, i => -i);
    /// </code>
    ///     The <c>result</c> variable will contain <c>{ 0, -1, -2, 123, 456, 789 }</c>.
    /// </example>
    public static IEnumerable<TSource?> PadStart<TSource>(this IEnumerable<TSource> source, int width, Func<int, TSource>? paddingSelector = null) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (paddingSelector == null) throw new ArgumentNullException(nameof(paddingSelector));
        if (width < 0) throw new ArgumentException(null, nameof(width));
        return PadLeftImpl(source, width, default, paddingSelector);
    }

    private static IEnumerable<T?> PadLeftImpl<T>(IEnumerable<T> source,
        int width, T? padding, Func<int, T>? paddingSelector) {
        return
            source is ICollection<T?> collection
                ? collection.Count >= width
                    ? collection
                    : Enumerable.Range(0, width - collection.Count)
                        .Select(i => paddingSelector != null ? paddingSelector(i) : padding)
                        .Concat(collection)
                : _();
        IEnumerable<T?> _() {
            var array = new T[width];
            var count = 0;

            using (var e = source.GetEnumerator()) {
                for (; count < width && e.MoveNext(); count++) array[count] = e.Current;

                if (count == width) {
                    for (var i = 0; i < count; i++) yield return array[i];

                    while (e.MoveNext()) yield return e.Current;

                    yield break;
                }
            }

            var len = width - count;

            for (var i = 0; i < len; i++) yield return paddingSelector != null ? paddingSelector(i) : padding;

            for (var i = 0; i < count; i++) yield return array[i];
        }
    }
    #endregion

    #region Partition
    /// <summary>Partitions or splits a sequence in two using a predicate.</summary>
    /// <param name="source">The source sequence.</param>
    /// <param name="predicate">The predicate function.</param>
    /// <typeparam name="T">Type of source elements.</typeparam>
    /// <returns>A tuple of elements staisfying the predicate and those that do not, respectively.</returns>
    /// <example>
    ///     <code>
    /// var (evens, odds) =
    ///     Enumerable.Range(0, 10).Partition(x => x % 2 == 0);
    /// </code>
    ///     The <c>evens</c> variable, when iterated over, will yield 0, 2, 4, 6 and then 8. The <c>odds</c> variable, when
    ///     iterated over, will yield 1, 3, 5, 7 and then 9.
    /// </example>
    public static (IEnumerable<T> True, IEnumerable<T> False)
        Partition<T>(this IEnumerable<T> source, Func<T, bool> predicate) =>
        source.Partition(predicate, ValueTuple.Create);

    /// <summary>Partitions or splits a sequence in two using a predicate and then projects a result from the two.</summary>
    /// <param name="source">The source sequence.</param>
    /// <param name="predicate">The predicate function.</param>
    /// <param name="resultSelector">
    ///     Function that projects the result from sequences of elements that satisfy the predicate
    ///     and those that do not, respectively, passed as arguments.
    /// </param>
    /// <typeparam name="T">Type of source elements.</typeparam>
    /// <typeparam name="TResult">Type of the result.</typeparam>
    /// <returns>The return value from <paramref name="resultSelector" />.</returns>
    /// <example>
    ///     <code>
    /// var (evens, odds) =
    ///     Enumerable.Range(0, 10)
    ///               .Partition(x => x % 2 == 0, ValueTuple.Create);
    /// </code>
    ///     The <c>evens</c> variable, when iterated over, will yield 0, 2, 4, 6 and then 8. The <c>odds</c> variable, when
    ///     iterated over, will yield 1, 3, 5, 7 and then 9.
    /// </example>
    public static TResult Partition<T, TResult>(this IEnumerable<T> source,
        Func<T, bool> predicate, Func<IEnumerable<T>, IEnumerable<T>, TResult> resultSelector) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (predicate == null) throw new ArgumentNullException(nameof(predicate));
        return source.GroupBy(predicate).Partition(resultSelector);
    }

    /// <summary>Partitions a grouping by Boolean keys into a projection of true elements and false elements, respectively.</summary>
    /// <typeparam name="T">Type of elements in source groupings.</typeparam>
    /// <typeparam name="TResult">Type of the result.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="resultSelector">
    ///     Function that projects the result from sequences of true elements and false elements,
    ///     respectively, passed as arguments.
    /// </param>
    /// <returns>The return value from <paramref name="resultSelector" />.</returns>
    public static TResult Partition<T, TResult>(this IEnumerable<IGrouping<bool, T>> source,
        Func<IEnumerable<T>, IEnumerable<T>, TResult> resultSelector) {
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));
        return source.Partition(true, false, null, (t, f, _) => resultSelector(t, f));
    }

    /// <summary>
    ///     Partitions a grouping by nullable Boolean keys into a projection of true elements, false elements and null
    ///     elements, respectively.
    /// </summary>
    /// <typeparam name="T">Type of elements in source groupings.</typeparam>
    /// <typeparam name="TResult">Type of the result.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="resultSelector">
    ///     Function that projects the result from sequences of true elements, false elements and null
    ///     elements, respectively, passed as arguments.
    /// </param>
    /// <returns>The return value from <paramref name="resultSelector" />.</returns>
    public static TResult Partition<T, TResult>(this IEnumerable<IGrouping<bool?, T>> source,
        Func<IEnumerable<T>, IEnumerable<T>, IEnumerable<T>, TResult> resultSelector) {
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));
        return source.Partition(true, false, null, (t, f, n, _) => resultSelector(t, f, n));
    }

    /// <summary>
    ///     Partitions a grouping and projects a result from group elements matching a key and those groups that do not.
    ///     An additional parameter specifies how to compare keys for equality.
    /// </summary>
    /// <typeparam name="TKey">Type of keys in source groupings.</typeparam>
    /// <typeparam name="TElement">Type of elements in source groupings.</typeparam>
    /// <typeparam name="TResult">Type of the result.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="key">The key to partition on.</param>
    /// <param name="comparer">The comparer for keys.</param>
    /// <param name="resultSelector">
    ///     Function that projects the result from elements of the group matching
    ///     <paramref name="key" /> and those groups that do not (in the order in which they appear in
    ///     <paramref name="source" />), passed as arguments.
    /// </param>
    /// <returns>The return value from <paramref name="resultSelector" />.</returns>
    public static TResult Partition<TKey, TElement, TResult>(this IEnumerable<IGrouping<TKey, TElement>> source,
        TKey key, IEqualityComparer<TKey>? comparer,
        Func<IEnumerable<TElement>, IEnumerable<IGrouping<TKey, TElement>>, TResult> resultSelector) {
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));
        return PartitionImpl(source, 1, key, default, default, comparer,
            (a, _, _, rest) => resultSelector(a, rest));
    }
    /// <summary>
    ///     Partitions a grouping and projects a result from elements of groups matching a set of two keys and those
    ///     groups that do not. An additional parameter specifies how to compare keys for equality.
    /// </summary>
    /// <typeparam name="TKey">Type of keys in source groupings.</typeparam>
    /// <typeparam name="TElement">Type of elements in source groupings.</typeparam>
    /// <typeparam name="TResult">Type of the result.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="key1">The first key to partition on.</param>
    /// <param name="key2">The second key to partition on.</param>
    /// <param name="comparer">The comparer for keys.</param>
    /// <param name="resultSelector">
    ///     Function that projects the result from elements of the group matching
    ///     <paramref name="key1" />, elements of the group matching <paramref name="key2" /> and those groups that do not (in
    ///     the order in which they appear in <paramref name="source" />), passed as arguments.
    /// </param>
    /// <returns>The return value from <paramref name="resultSelector" />.</returns>
    public static TResult Partition<TKey, TElement, TResult>(this IEnumerable<IGrouping<TKey, TElement>> source,
        TKey key1, TKey key2, IEqualityComparer<TKey>? comparer,
        Func<IEnumerable<TElement>, IEnumerable<TElement>, IEnumerable<IGrouping<TKey, TElement>>, TResult> resultSelector) {
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));
        return PartitionImpl<TKey, TElement, TResult>(source, 2, key1, key2, default, comparer,
            (a, b, _, rest) => resultSelector(a, b, rest));
    }

    /// <summary>
    ///     Partitions a grouping and projects a result from elements groups matching a set of three keys and those groups
    ///     that do not.
    /// </summary>
    /// <typeparam name="TKey">Type of keys in source groupings.</typeparam>
    /// <typeparam name="TElement">Type of elements in source groupings.</typeparam>
    /// <typeparam name="TResult">Type of the result.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="key1">The first key to partition on.</param>
    /// <param name="key2">The second key to partition on.</param>
    /// <param name="key3">The third key to partition on.</param>
    /// <param name="resultSelector">
    ///     Function that projects the result from elements of groups matching
    ///     <paramref name="key1" />, <paramref name="key2" /> and <paramref name="key3" /> and those groups that do not (in
    ///     the order in which they appear in <paramref name="source" />), passed as arguments.
    /// </param>
    /// <returns>The return value from <paramref name="resultSelector" />.</returns>
    public static TResult Partition<TKey, TElement, TResult>(this IEnumerable<IGrouping<TKey, TElement>> source,
        TKey key1, TKey key2, TKey key3,
        Func<IEnumerable<TElement>, IEnumerable<TElement>, IEnumerable<TElement>, IEnumerable<IGrouping<TKey, TElement>>, TResult> resultSelector) =>
        Partition(source, key1, key2, key3, null, resultSelector);

    /// <summary>
    ///     Partitions a grouping and projects a result from elements groups matching a set of three keys and those groups
    ///     that do not. An additional parameter specifies how to compare keys for equality.
    /// </summary>
    /// <typeparam name="TKey">Type of keys in source groupings.</typeparam>
    /// <typeparam name="TElement">Type of elements in source groupings.</typeparam>
    /// <typeparam name="TResult">Type of the result.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="key1">The first key to partition on.</param>
    /// <param name="key2">The second key to partition on.</param>
    /// <param name="key3">The third key to partition on.</param>
    /// <param name="comparer">The comparer for keys.</param>
    /// <param name="resultSelector">
    ///     Function that projects the result from elements of groups matching
    ///     <paramref name="key1" />, <paramref name="key2" /> and <paramref name="key3" /> and those groups that do not (in
    ///     the order in which they appear in <paramref name="source" />), passed as arguments.
    /// </param>
    /// <returns>The return value from <paramref name="resultSelector" />.</returns>
    public static TResult Partition<TKey, TElement, TResult>(this IEnumerable<IGrouping<TKey, TElement>> source,
        TKey key1, TKey key2, TKey key3, IEqualityComparer<TKey>? comparer,
        Func<IEnumerable<TElement>, IEnumerable<TElement>, IEnumerable<TElement>, IEnumerable<IGrouping<TKey, TElement>>, TResult> resultSelector) =>
        PartitionImpl(source, 3, key1, key2, key3, comparer, resultSelector);

    private static TResult PartitionImpl<TKey, TElement, TResult>(IEnumerable<IGrouping<TKey, TElement>> source,
        int count, TKey key1, TKey? key2, TKey? key3, IEqualityComparer<TKey>? comparer,
        Func<IEnumerable<TElement>, IEnumerable<TElement>, IEnumerable<TElement>, IEnumerable<IGrouping<TKey, TElement>>, TResult> resultSelector) {
        Debug.Assert(count > 0 && count <= 3);

        if (source == null) throw new ArgumentNullException(nameof(source));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        comparer ??= EqualityComparer<TKey>.Default;

        List<IGrouping<TKey, TElement>>? etc = null;

        var groups = new[] {
                Enumerable.Empty<TElement>(),
                Enumerable.Empty<TElement>(),
                Enumerable.Empty<TElement>()
            };

        foreach (var e in source) {
            var i = count > 0 && comparer.Equals(e.Key, key1) ? 0
                : count > 1 && comparer.Equals(e.Key, key2) ? 1
                : count > 2 && comparer.Equals(e.Key, key3) ? 2
                : -1;

            if (i < 0) {
                etc ??= new List<IGrouping<TKey, TElement>>();
                etc.Add(e);
            } else {
                groups[i] = e;
            }
        }

        return resultSelector(groups[0], groups[1], groups[2], etc ?? Enumerable.Empty<IGrouping<TKey, TElement>>());
    }
    #endregion

    #region RightJoin
    /// <summary>
    ///     Performs a right outer join on two homogeneous sequences. Additional arguments specify key selection
    ///     functions, result projection functions and a key comparer.
    /// </summary>
    /// <typeparam name="TSource">The type of elements in the source sequence.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by the key selector function.</typeparam>
    /// <typeparam name="TResult">The type of the result elements.</typeparam>
    /// <param name="first">The first sequence of the join operation.</param>
    /// <param name="second">The second sequence of the join operation.</param>
    /// <param name="keySelector">Function that projects the key given an element of one of the sequences to join.</param>
    /// <param name="secondSelector">
    ///     Function that projects the result given just an element from <paramref name="second" />
    ///     where there is no corresponding element in <paramref name="first" />.
    /// </param>
    /// <param name="bothSelector">
    ///     Function that projects the result given an element from <paramref name="first" /> and an
    ///     element from <paramref name="second" /> that match on a common key.
    /// </param>
    /// <param name="comparer">The <see cref="IEqualityComparer{T}" /> instance used to compare keys for equality.</param>
    /// <returns>A sequence containing results projected from a right outer join of the two input sequences.</returns>
    public static IEnumerable<TResult> RightJoin<TSource, TKey, TResult>(
        this IEnumerable<TSource> first,
        IEnumerable<TSource> second,
        Func<TSource, TKey> keySelector,
        Func<TSource, TResult> secondSelector,
        Func<TSource, TSource, TResult> bothSelector,
        IEqualityComparer<TKey>? comparer = null) {
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
        return first.RightJoin(second,
            keySelector, keySelector,
            secondSelector, bothSelector,
            comparer);
    }

    /// <summary>
    ///     Performs a right outer join on two heterogeneous sequences. Additional arguments specify key selection
    ///     functions, result projection functions and a key comparer.
    /// </summary>
    /// <typeparam name="TFirst">The type of elements in the first sequence.</typeparam>
    /// <typeparam name="TSecond">The type of elements in the second sequence.</typeparam>
    /// <typeparam name="TKey">The type of the key returned by the key selector functions.</typeparam>
    /// <typeparam name="TResult">The type of the result elements.</typeparam>
    /// <param name="first">The first sequence of the join operation.</param>
    /// <param name="second">The second sequence of the join operation.</param>
    /// <param name="firstKeySelector">Function that projects the key given an element from <paramref name="first" />.</param>
    /// <param name="secondKeySelector">Function that projects the key given an element from <paramref name="second" />.</param>
    /// <param name="secondSelector">
    ///     Function that projects the result given just an element from <paramref name="second" />
    ///     where there is no corresponding element in <paramref name="first" />.
    /// </param>
    /// <param name="bothSelector">
    ///     Function that projects the result given an element from <paramref name="first" /> and an
    ///     element from <paramref name="second" /> that match on a common key.
    /// </param>
    /// <param name="comparer">The <see cref="IEqualityComparer{T}" /> instance used to compare keys for equality.</param>
    /// <returns>A sequence containing results projected from a right outer join of the two input sequences.</returns>
    public static IEnumerable<TResult> RightJoin<TFirst, TSecond, TKey, TResult>(
        this IEnumerable<TFirst> first,
        IEnumerable<TSecond> second,
        Func<TFirst, TKey> firstKeySelector,
        Func<TSecond, TKey> secondKeySelector,
        Func<TSecond, TResult> secondSelector,
        Func<TFirst, TSecond, TResult> bothSelector,
        IEqualityComparer<TKey>? comparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));
        if (firstKeySelector == null) throw new ArgumentNullException(nameof(firstKeySelector));
        if (secondKeySelector == null) throw new ArgumentNullException(nameof(secondKeySelector));
        if (secondSelector == null) throw new ArgumentNullException(nameof(secondSelector));
        if (bothSelector == null) throw new ArgumentNullException(nameof(bothSelector));

        return second.LeftJoin(first,
            secondKeySelector, firstKeySelector,
            secondSelector, (x, y) => bothSelector(y, x),
            comparer);
    }
    #endregion

    #region Segment
    /// <summary>Divides a sequence into multiple sequences by using a segment detector based on the original sequence</summary>
    /// <typeparam name="T">The type of the elements in the sequence</typeparam>
    /// <param name="source">The sequence to segment</param>
    /// <param name="newSegmentPredicate">
    ///     A function, which returns <c>true</c> if the given element begins a new segment, and
    ///     <c>false</c> otherwise
    /// </param>
    /// <returns>A sequence of segment, each of which is a portion of the original sequence</returns>
    /// <exception cref="ArgumentNullException">
    ///     Thrown if either <paramref name="source" /> or
    ///     <paramref name="newSegmentPredicate" /> are <see langword="null" />.
    /// </exception>
    public static IEnumerable<IEnumerable<T>> Segment<T>(this IEnumerable<T> source, Func<T, bool> newSegmentPredicate) {
        if (newSegmentPredicate == null) throw new ArgumentNullException(nameof(newSegmentPredicate));

        return Segment(source, (curr, _, _) => newSegmentPredicate(curr));
    }

    /// <summary>Divides a sequence into multiple sequences by using a segment detector based on the original sequence</summary>
    /// <typeparam name="T">The type of the elements in the sequence</typeparam>
    /// <param name="source">The sequence to segment</param>
    /// <param name="newSegmentPredicate">
    ///     A function, which returns <c>true</c> if the given element or index indicate a new
    ///     segment, and <c>false</c> otherwise
    /// </param>
    /// <returns>A sequence of segment, each of which is a portion of the original sequence</returns>
    /// <exception cref="ArgumentNullException">
    ///     Thrown if either <paramref name="source" /> or
    ///     <paramref name="newSegmentPredicate" /> are <see langword="null" />.
    /// </exception>
    public static IEnumerable<IEnumerable<T>> Segment<T>(this IEnumerable<T> source, Func<T, int, bool> newSegmentPredicate) {
        if (newSegmentPredicate == null) throw new ArgumentNullException(nameof(newSegmentPredicate));

        return Segment(source, (curr, _, index) => newSegmentPredicate(curr, index));
    }

    /// <summary>Divides a sequence into multiple sequences by using a segment detector based on the original sequence</summary>
    /// <typeparam name="T">The type of the elements in the sequence</typeparam>
    /// <param name="source">The sequence to segment</param>
    /// <param name="newSegmentPredicate">
    ///     A function, which returns <c>true</c> if the given current element, previous element
    ///     or index indicate a new segment, and <c>false</c> otherwise
    /// </param>
    /// <returns>A sequence of segment, each of which is a portion of the original sequence</returns>
    /// <exception cref="ArgumentNullException">
    ///     Thrown if either <paramref name="source" /> or
    ///     <paramref name="newSegmentPredicate" /> are <see langword="null" />.
    /// </exception>
    public static IEnumerable<IEnumerable<T>> Segment<T>(this IEnumerable<T> source, Func<T, T?, int, bool> newSegmentPredicate) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (newSegmentPredicate == null) throw new ArgumentNullException(nameof(newSegmentPredicate));

        return _();
        IEnumerable<IEnumerable<T>> _() {
            var index = -1;
            using var iter = source.GetEnumerator();
            var segment = new List<T>();
            var prevItem = default(T);

            // ensure that the first item is always part
            // of the first segment. This is an intentional
            // behavior. Segmentation always begins with
            // the second element in the sequence.
            if (iter.MoveNext()) {
                ++index;
                segment.Add(iter.Current);
                prevItem = iter.Current;
            }

            while (iter.MoveNext()) {
                ++index;
                // check if the item represents the start of a new segment
                var isNewSegment = newSegmentPredicate(iter.Current, prevItem, index);
                prevItem = iter.Current;

                if (!isNewSegment) {
                    // if not a new segment, append and continue
                    segment.Add(iter.Current);
                    continue;
                }
                yield return segment; // yield the completed segment

                // start a new segment...
                segment = new List<T> { iter.Current };
            }
            // handle the case of the sequence ending before new segment is detected
            if (segment.Count > 0) yield return segment;
        }
    }
    #endregion

    #region SkipLast
    /// <summary>Bypasses a specified number of elements at the end of the sequence.</summary>
    /// <typeparam name="T">Type of the source sequence</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="count">The number of elements to bypass at the end of the source sequence.</param>
    /// <returns>
    ///     An <see cref="IEnumerable{T}" /> containing the source sequence elements except for the bypassed ones at the
    ///     end.
    /// </returns>
    public static IEnumerable<T> SkipLast<T>(this IEnumerable<T> source, int count) {
        if (source == null) throw new ArgumentNullException(nameof(source));

        if (count < 1) return source;

        return
            source is ICollection<T> col ? col.Take(col.Count - count) : _();
        IEnumerable<T> _() {
            var queue = new Queue<T>(count);

            foreach (var item in source) {
                if (queue.Count < count) {
                    queue.Enqueue(item);
                    continue;
                }

                yield return queue.Dequeue();
                queue.Enqueue(item);
            }
        }
    }
    #endregion

    #region SkipUntil
    /// <summary>
    ///     Skips items from the input sequence until the given predicate returns true when applied to the current source
    ///     item; that item will be the last skipped.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         SkipUntil differs from Enumerable.SkipWhile in two respects. Firstly, the sense of the predicate is reversed:
    ///         it is expected that the predicate will return false to start with, and then return true - for example, when
    ///         trying to find a matching item in a sequence.
    ///     </para>
    ///     <para>
    ///         Secondly, SkipUntil skips the element which causes the predicate to return true. For example, in a sequence
    ///         <c>{ 1, 2, 3, 4, 5 }</c> and with a predicate of <c>x => x == 3</c>, the result would be
    ///         <c>{ 4, 5 }</c>.
    ///     </para>
    ///     <para>
    ///         SkipUntil is as lazy as possible: it will not iterate over the source sequence until it has to, it won't
    ///         iterate further than it has to, and it won't evaluate the predicate until it has to. (This means that an item
    ///         may be returned which would actually cause the predicate to throw an exception if it were evaluated, so long as
    ///         it comes after the first item causing the predicate to return true.)
    ///     </para>
    /// </remarks>
    /// <typeparam name="TSource">Type of the source sequence</typeparam>
    /// <param name="source">Source sequence</param>
    /// <param name="predicate">Predicate used to determine when to stop yielding results from the source.</param>
    /// <returns>Items from the source sequence after the predicate first returns true when applied to the item.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="source" /> or <paramref name="predicate" /> is null</exception>
    public static IEnumerable<TSource> SkipUntil<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (predicate == null) throw new ArgumentNullException(nameof(predicate));

        return _();
        IEnumerable<TSource> _() {
            using var iterator = source.GetEnumerator();
            while (iterator.MoveNext()) {
                if (predicate(iterator.Current))
                    break;
            }

            while (iterator.MoveNext()) yield return iterator.Current;
        }
    }
    #endregion

    #region Slice
    /// <summary>Extracts a contiguous count of elements from a sequence at a particular zero-based starting index</summary>
    /// <remarks>
    ///     If the starting position or count specified result in slice extending past the end of the sequence, it will
    ///     return all elements up to that point. There is no guarantee that the resulting sequence will contain the number of
    ///     elements requested - it may have anywhere from 0 to <paramref name="count" />.<br /> This method is implemented in
    ///     an optimized manner for any sequence implementing <c>IList{T}</c>.<br /> The result of Slice() is identical to:
    ///     <c>sequence.Skip(startIndex).Take(count)</c>
    /// </remarks>
    /// <typeparam name="T">The type of the elements in the source sequence</typeparam>
    /// <param name="sequence">The sequence from which to extract elements</param>
    /// <param name="startIndex">The zero-based index at which to begin slicing</param>
    /// <param name="count">The number of items to slice out of the index</param>
    /// <returns>A new sequence containing any elements sliced out from the source sequence</returns>
    public static IEnumerable<T> Slice<T>(this IEnumerable<T> sequence, int startIndex, int count) {
        if (sequence == null) throw new ArgumentNullException(nameof(sequence));
        if (startIndex < 0) throw new ArgumentOutOfRangeException(nameof(startIndex));
        if (count < 0) throw new ArgumentOutOfRangeException(nameof(count));

        // optimization for anything implementing IList<T>
        return sequence is not IList<T> list
            ? sequence.Skip(startIndex).Take(count)
            : _(count);
        IEnumerable<T> _(int countdown) {
            var listCount = list.Count;
            var index = startIndex;
            while (index < listCount && countdown-- > 0) yield return list[index++];
        }
    }
    #endregion

    #region Split
    /// <summary>Splits the source sequence by a separator.</summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <returns>A sequence of splits of elements.</returns>
    public static IEnumerable<IEnumerable<TSource>> Split<TSource>(this IEnumerable<TSource> source,
        TSource separator) =>
        Split(source, separator, int.MaxValue);

    /// <summary>Splits the source sequence by a separator given a maximum count of splits.</summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <param name="count">Maximum number of splits.</param>
    /// <returns>A sequence of splits of elements.</returns>
    public static IEnumerable<IEnumerable<TSource>> Split<TSource>(this IEnumerable<TSource> source,
        TSource separator, int count) =>
        Split(source, separator, count, s => s);

    /// <summary>Splits the source sequence by a separator and then transforms the splits into results.</summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <typeparam name="TResult">Type of the result sequence elements.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <param name="resultSelector">
    ///     Function used to project splits of source elements into elements of the resulting
    ///     sequence.
    /// </param>
    /// <returns>A sequence of values typed as <typeparamref name="TResult" />.</returns>
    public static IEnumerable<TResult> Split<TSource, TResult>(this IEnumerable<TSource> source,
        TSource separator,
        Func<IEnumerable<TSource>, TResult> resultSelector) =>
        Split(source, separator, int.MaxValue, resultSelector);

    /// <summary>
    ///     Splits the source sequence by a separator, given a maximum count of splits, and then transforms the splits
    ///     into results.
    /// </summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <typeparam name="TResult">Type of the result sequence elements.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <param name="count">Maximum number of splits.</param>
    /// <param name="resultSelector">
    ///     Function used to project splits of source elements into elements of the resulting
    ///     sequence.
    /// </param>
    /// <returns>A sequence of values typed as <typeparamref name="TResult" />.</returns>
    public static IEnumerable<TResult> Split<TSource, TResult>(this IEnumerable<TSource> source,
        TSource separator, int count,
        Func<IEnumerable<TSource>, TResult> resultSelector) =>
        Split(source, separator, null, count, resultSelector);

    /// <summary>Splits the source sequence by a separator and then transforms the splits into results.</summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <param name="comparer">Comparer used to determine separator element equality.</param>
    /// <returns>A sequence of splits of elements.</returns>
    public static IEnumerable<IEnumerable<TSource>> Split<TSource>(this IEnumerable<TSource> source,
        TSource separator, IEqualityComparer<TSource> comparer) =>
        Split(source, separator, comparer, int.MaxValue);

    /// <summary>
    ///     Splits the source sequence by a separator, given a maximum count of splits. A parameter specifies how the
    ///     separator is compared for equality.
    /// </summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <param name="comparer">Comparer used to determine separator element equality.</param>
    /// <param name="count">Maximum number of splits.</param>
    /// <returns>A sequence of splits of elements.</returns>
    public static IEnumerable<IEnumerable<TSource>> Split<TSource>(this IEnumerable<TSource> source,
        TSource separator, IEqualityComparer<TSource>? comparer, int count) =>
        Split(source, separator, comparer, count, s => s);

    /// <summary>
    ///     Splits the source sequence by a separator and then transforms the splits into results. A parameter specifies
    ///     how the separator is compared for equality.
    /// </summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <typeparam name="TResult">Type of the result sequence elements.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <param name="comparer">Comparer used to determine separator element equality.</param>
    /// <param name="resultSelector">
    ///     Function used to project splits of source elements into elements of the resulting
    ///     sequence.
    /// </param>
    /// <returns>A sequence of values typed as <typeparamref name="TResult" />.</returns>
    public static IEnumerable<TResult> Split<TSource, TResult>(this IEnumerable<TSource> source,
        TSource separator, IEqualityComparer<TSource>? comparer,
        Func<IEnumerable<TSource>, TResult> resultSelector) =>
        Split(source, separator, comparer, int.MaxValue, resultSelector);

    /// <summary>
    ///     Splits the source sequence by a separator, given a maximum count of splits, and then transforms the splits
    ///     into results. A parameter specifies how the separator is compared for equality.
    /// </summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <typeparam name="TResult">Type of the result sequence elements.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separator">Separator element.</param>
    /// <param name="comparer">Comparer used to determine separator element equality.</param>
    /// <param name="count">Maximum number of splits.</param>
    /// <param name="resultSelector">
    ///     Function used to project splits of source elements into elements of the resulting
    ///     sequence.
    /// </param>
    /// <returns>A sequence of values typed as <typeparamref name="TResult" />.</returns>
    public static IEnumerable<TResult> Split<TSource, TResult>(this IEnumerable<TSource> source,
        TSource separator, IEqualityComparer<TSource>? comparer, int count,
        Func<IEnumerable<TSource>, TResult> resultSelector) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (count <= 0) throw new ArgumentOutOfRangeException(nameof(count));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        comparer ??= EqualityComparer<TSource>.Default;
        return Split(source, item => comparer.Equals(item, separator), count, resultSelector);
    }

    /// <summary>Splits the source sequence by separator elements identified by a function.</summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separatorFunc">Predicate function used to determine the splitter elements in the source sequence.</param>
    /// <returns>A sequence of splits of elements.</returns>
    public static IEnumerable<IEnumerable<TSource>> Split<TSource>(this IEnumerable<TSource> source,
        Func<TSource, bool> separatorFunc) =>
        Split(source, separatorFunc, int.MaxValue);

    /// <summary>Splits the source sequence by separator elements identified by a function, given a maximum count of splits.</summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separatorFunc">Predicate function used to determine the splitter elements in the source sequence.</param>
    /// <param name="count">Maximum number of splits.</param>
    /// <returns>A sequence of splits of elements.</returns>
    public static IEnumerable<IEnumerable<TSource>> Split<TSource>(this IEnumerable<TSource> source,
        Func<TSource, bool> separatorFunc, int count) =>
        Split(source, separatorFunc, count, s => s);

    /// <summary>
    ///     Splits the source sequence by separator elements identified by a function and then transforms the splits into
    ///     results.
    /// </summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <typeparam name="TResult">Type of the result sequence elements.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separatorFunc">Predicate function used to determine the splitter elements in the source sequence.</param>
    /// <param name="resultSelector">
    ///     Function used to project splits of source elements into elements of the resulting
    ///     sequence.
    /// </param>
    /// <returns>A sequence of values typed as <typeparamref name="TResult" />.</returns>
    public static IEnumerable<TResult> Split<TSource, TResult>(this IEnumerable<TSource> source,
        Func<TSource, bool> separatorFunc,
        Func<IEnumerable<TSource>, TResult> resultSelector) =>
        Split(source, separatorFunc, int.MaxValue, resultSelector);

    /// <summary>
    ///     Splits the source sequence by separator elements identified by a function, given a maximum count of splits,
    ///     and then transforms the splits into results.
    /// </summary>
    /// <typeparam name="TSource">Type of element in the source sequence.</typeparam>
    /// <typeparam name="TResult">Type of the result sequence elements.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="separatorFunc">Predicate function used to determine the splitter elements in the source sequence.</param>
    /// <param name="count">Maximum number of splits.</param>
    /// <param name="resultSelector">
    ///     Function used to project a split group of source elements into an element of the resulting
    ///     sequence.
    /// </param>
    /// <returns>A sequence of values typed as <typeparamref name="TResult" />.</returns>
    public static IEnumerable<TResult> Split<TSource, TResult>(this IEnumerable<TSource> source,
        Func<TSource, bool> separatorFunc, int count,
        Func<IEnumerable<TSource>, TResult> resultSelector) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (separatorFunc == null) throw new ArgumentNullException(nameof(separatorFunc));
        if (count <= 0) throw new ArgumentOutOfRangeException(nameof(count));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        return _();
        IEnumerable<TResult> _() {
            if (count == 0) // No splits?
            {
                yield return resultSelector(source);
            } else {
                List<TSource>? items = null;

                foreach (var item in source) {
                    if (count > 0 && separatorFunc(item)) {
                        yield return resultSelector(items ?? Enumerable.Empty<TSource>());
                        count--;
                        items = null;
                    } else {
                        (items ??= new List<TSource>()).Add(item);
                    }
                }

                if (items?.Count > 0) yield return resultSelector(items);
            }
        }
    }
    #endregion

    #region StartsWith
    /// <summary>
    ///     Determines whether the beginning of the first sequence is equivalent to the second sequence, using the
    ///     specified element equality comparer.
    /// </summary>
    /// <typeparam name="T">Type of elements.</typeparam>
    /// <param name="first">The sequence to check.</param>
    /// <param name="second">The sequence to compare to.</param>
    /// <param name="comparer">Equality comparer to use.</param>
    /// <returns><c>true</c> if <paramref name="first" /> begins with elements equivalent to <paramref name="second" />.</returns>
    /// <remarks>
    ///     This is the <see cref="IEnumerable{T}" /> equivalent of <see cref="string.StartsWith(string)" /> and it calls
    ///     <see cref="IEqualityComparer{T}.Equals(T,T)" /> on pairs of elements at the same index.
    /// </remarks>
    public static bool StartsWith<T>(this IEnumerable<T> first, IEnumerable<T> second, IEqualityComparer<T>? comparer = null) {
        if (first == null) throw new ArgumentNullException(nameof(first));
        if (second == null) throw new ArgumentNullException(nameof(second));

        comparer ??= EqualityComparer<T>.Default;

        using var firstIter = first.GetEnumerator();
        return second.All(item => firstIter.MoveNext() && comparer.Equals(firstIter.Current, item));
    }
    #endregion

    #region Subsets
    /// <summary>
    ///     Returns a sequence of <see cref="IList{T}" /> representing all of the subsets of any size that are part of the
    ///     original sequence. In mathematics, it is equivalent to the <em>power set</em> of a set.
    /// </summary>
    /// <remarks>
    ///     This operator produces all of the subsets of a given sequence. Subsets are returned in increasing cardinality,
    ///     starting with the empty set and terminating with the entire original sequence.<br /> Subsets are produced in a
    ///     deferred, streaming manner; however, each subset is returned as a materialized list.<br /> There are 2^N subsets of
    ///     a given sequence, where N => sequence.Count().
    /// </remarks>
    /// <param name="sequence">Sequence for which to produce subsets</param>
    /// <typeparam name="T">The type of the elements in the sequence</typeparam>
    /// <returns>A sequence of lists that represent the all subsets of the original sequence</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="sequence" /> is <see langword="null" /></exception>
    public static IEnumerable<IList<T>> Subsets<T>(this IEnumerable<T> sequence) {
        if (sequence == null) throw new ArgumentNullException(nameof(sequence));

        return _();
        IEnumerable<IList<T>> _() {
            var sequenceAsList = sequence.ToList();
            var sequenceLength = sequenceAsList.Count;

            // the first subset is the empty set
            yield return new List<T>();

            // all other subsets are computed using the subset generator
            // this check also resolves the case of permuting empty sets
            if (sequenceLength > 0) {
                for (var i = 1; i < sequenceLength; i++) {
                    // each intermediate subset is a lexographically ordered K-subset
                    var subsetGenerator = new SubsetGenerator<T>(sequenceAsList, i);
                    foreach (var subset in subsetGenerator) yield return subset;
                }

                yield return sequenceAsList; // the last subet is the original set itself
            }
        }
    }

    /// <summary>
    ///     Returns a sequence of <see cref="IList{T}" /> representing all subsets of a given size that are part of the
    ///     original sequence. In mathematics, it is equivalent to the <em>combinations</em> or <em>k-subsets</em> of a set.
    /// </summary>
    /// <param name="sequence">Sequence for which to produce subsets</param>
    /// <param name="subsetSize">The size of the subsets to produce</param>
    /// <typeparam name="T">The type of the elements in the sequence</typeparam>
    /// <returns>A sequence of lists that represents of K-sized subsets of the original sequence</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="sequence" /> is <see langword="null" /></exception>
    /// <exception cref="ArgumentOutOfRangeException">Thrown if <paramref name="subsetSize" /> is less than zero.</exception>
    public static IEnumerable<IList<T>> Subsets<T>(this IEnumerable<T> sequence, int subsetSize) {
        if (sequence == null) throw new ArgumentNullException(nameof(sequence));
        if (subsetSize < 0) throw new ArgumentOutOfRangeException(nameof(subsetSize), "Subset size must be >= 0");

        // NOTE: Theres an interesting trade-off that we have to make in this operator.
        // Ideally, we would throw an exception here if the {subsetSize} parameter is
        // greater than the sequence length. Unforunately, determining the length of a
        // sequence is not always possible without enumerating it. Herein lies the rub.
        // We want Subsets() to be a deferred operation that only iterates the sequence
        // when the caller is ready to consume the results. However, this forces us to
        // defer the precondition check on the {subsetSize} upper bound. This can result
        // in an exception that is far removed from the point of failure - an unfortunate
        // and undesirable outcome.
        // At the moment, this operator prioritizes deferred execution over fail-fast
        // preconditions. This however, needs to be carefully considered - and perhaps
        // may change after further thought and review.

        return new SubsetGenerator<T>(sequence, subsetSize);
    }

    /// <summary>This class is responsible for producing the lexographically ordered k-subsets</summary>
    private sealed class SubsetGenerator<T>: IEnumerable<IList<T>> {
        #region Fields / Properties
        private readonly IEnumerable<T> _sequence;
        private readonly int _subsetSize;
        #endregion

        #region Constructors
        public SubsetGenerator(IEnumerable<T> sequence, int subsetSize) {
            if (subsetSize < 0) throw new ArgumentOutOfRangeException(nameof(subsetSize), "{subsetSize} must be between 0 and set.Count()");
            _subsetSize = subsetSize;
            _sequence = sequence ?? throw new ArgumentNullException(nameof(sequence));
        }
        #endregion

        #region IEnumerable<IList<T>> Members
        /// <summary>
        ///     Returns an enumerator that produces all of the k-sized subsets of the initial value set. The enumerator
        ///     returns and <see cref="IList{T}" /> for each subset.
        /// </summary>
        /// <returns>an <see cref="IEnumerator" /> that enumerates all k-sized subsets</returns>
        public IEnumerator<IList<T>> GetEnumerator() => new SubsetEnumerator(_sequence.ToList(), _subsetSize);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        #endregion

        #region Nested type: SubsetEnumerator
        /// <summary>
        ///     SubsetEnumerator uses a snapshot of the original sequence, and an iterative, reductive swap algorithm to
        ///     produce all subsets of a predetermined size less than or equal to the original set size.
        /// </summary>
        private class SubsetEnumerator: IEnumerator<IList<T>> {
            #region Fields / Properties
            private readonly int[] _indices; // indices into the original set
            private readonly IList<T> _set; // the original set of elements
            private readonly T[] _subset; // the current subset to return
            private bool _continue; // termination indicator, set when all subsets have been produced
            private int _k; // size of the subset being produced

            private int _m; // previous swap index (upper index)
            private int _m2; // current swap index (lower index)
            private int _n; // size of the original set (sequence)
            private int _z; // count of items excluded from the subet
            #endregion

            #region Constructors
            public SubsetEnumerator(IList<T> set, int subsetSize) {
                // precondition: subsetSize <= set.Count
                if (subsetSize > set.Count) throw new ArgumentOutOfRangeException(nameof(subsetSize), "Subset size must be <= sequence.Count()");

                // initialize set arrays...
                _set = set;
                _subset = new T[subsetSize];
                _indices = new int[subsetSize];
                // initialize index counters...
                Reset();
            }
            #endregion

            #region IEnumerator<IList<T>> Members
            public IList<T> Current => (IList<T>)_subset.Clone();

            object IEnumerator.Current => Current;
            public void Reset() {
                _m = _subset.Length;
                _m2 = -1;
                _k = _subset.Length;
                _n = _set.Count;
                _z = _n - _k + 1;
                _continue = _subset.Length > 0;
            }

            public bool MoveNext() {
                if (!_continue) return false;

                if (_m2 == -1) {
                    _m2 = 0;
                    _m = _k;
                } else {
                    if (_m2 < _n - _m) _m = 0;
                    _m++;
                    _m2 = _indices[_k - _m];
                }

                for (var j = 1; j <= _m; j++) _indices[_k + j - _m - 1] = _m2 + j;

                ExtractSubset();

                _continue = _indices[0] != _z;
                return true;
            }

            void IDisposable.Dispose() { }
            #endregion

            private void ExtractSubset() {
                for (var i = 0; i < _k; i++) _subset[i] = _set[_indices[i] - 1];
            }
        }
        #endregion
    }
    #endregion

    #region TakeEvery
    /// <summary>Returns every N-th element of a sequence.</summary>
    /// <typeparam name="TSource">Type of the source sequence</typeparam>
    /// <param name="source">Source sequence</param>
    /// <param name="step">Number of elements to bypass before returning the next element.</param>
    /// <returns>A sequence with every N-th element of the input sequence.</returns>
    /// <remarks>This operator uses deferred execution and streams its results.</remarks>
    /// <example>
    ///     <code>
    /// int[] numbers = { 1, 2, 3, 4, 5 };
    /// IEnumerable&lt;int&gt; result = numbers.TakeEvery(2);
    /// </code>
    ///     The <c>result</c> variable, when iterated over, will yield 1, 3 and 5, in turn.
    /// </example>
    public static IEnumerable<TSource> TakeEvery<TSource>(this IEnumerable<TSource> source, int step) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (step <= 0) throw new ArgumentOutOfRangeException(nameof(step));
        return source.Where((_, i) => i % step == 0);
    }
    #endregion

    #region TakeUntil
    /// <summary>
    ///     Returns items from the input sequence until the given predicate returns true when applied to the current
    ///     source item; that item will be the last returned.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         TakeUntil differs from Enumerable.TakeWhile in two respects. Firstly, the sense of the predicate is reversed:
    ///         it is expected that the predicate will return false to start with, and then return true - for example, when
    ///         trying to find a matching item in a sequence.
    ///     </para>
    ///     <para>
    ///         Secondly, TakeUntil yields the element which causes the predicate to return true. For example, in a sequence
    ///         <c>{ 1, 2, 3, 4, 5 }</c> and with a predicate of <c>x => x == 3</c>, the result would be
    ///         <c>{ 1, 2, 3 }</c>.
    ///     </para>
    ///     <para>
    ///         TakeUntil is as lazy as possible: it will not iterate over the source sequence until it has to, it won't
    ///         iterate further than it has to, and it won't evaluate the predicate until it has to. (This means that an item
    ///         may be returned which would actually cause the predicate to throw an exception if it were evaluated, so long as
    ///         no more items of data are requested.)
    ///     </para>
    /// </remarks>
    /// <typeparam name="TSource">Type of the source sequence</typeparam>
    /// <param name="source">Source sequence</param>
    /// <param name="predicate">Predicate used to determine when to stop yielding results from the source.</param>
    /// <returns>Items from the source sequence, until the predicate returns true when applied to the item.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="source" /> or <paramref name="predicate" /> is null</exception>
    public static IEnumerable<TSource> TakeUntil<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (predicate == null) throw new ArgumentNullException(nameof(predicate));

        return _();
        IEnumerable<TSource> _() {
            foreach (var item in source) {
                yield return item;
                if (predicate(item)) yield break;
            }
        }
    }
    #endregion

    #region ToArrayByIndex
    /// <summary>
    ///     Creates an array from an <see cref="IEnumerable{T}" /> where a function is used to determine the index at
    ///     which an element will be placed in the array.
    /// </summary>
    /// <param name="source">The source sequence for the array.</param>
    /// <param name="indexSelector">A function that maps an element to its index.</param>
    /// <typeparam name="T">The type of the element in <paramref name="source" />.</typeparam>
    /// <returns>
    ///     An array that contains the elements from the input sequence. The size of the array will be as large as the
    ///     highest index returned by the <paramref name="indexSelector" /> plus 1.
    /// </returns>
    /// <remarks>
    ///     This method forces immediate query evaluation. It should not be used on infinite sequences. If more than one
    ///     element maps to the same index then the latter element overwrites the former in the resulting array.
    /// </remarks>
    public static T[] ToArrayByIndex<T>(this IEnumerable<T> source, Func<T, int> indexSelector) => source.ToArrayByIndex(indexSelector, (e, _) => e);

    /// <summary>
    ///     Creates an array from an <see cref="IEnumerable{T}" /> where a function is used to determine the index at
    ///     which an element will be placed in the array. The elements are projected into the array via an additional function.
    /// </summary>
    /// <param name="source">The source sequence for the array.</param>
    /// <param name="indexSelector">A function that maps an element to its index.</param>
    /// <param name="resultSelector">A function to project a source element into an element of the resulting array.</param>
    /// <typeparam name="T">The type of the element in <paramref name="source" />.</typeparam>
    /// <typeparam name="TResult">The type of the element in the resulting array.</typeparam>
    /// <returns>
    ///     An array that contains the projected elements from the input sequence. The size of the array will be as large
    ///     as the highest index returned by the <paramref name="indexSelector" /> plus 1.
    /// </returns>
    /// <remarks>
    ///     This method forces immediate query evaluation. It should not be used on infinite sequences. If more than one
    ///     element maps to the same index then the latter element overwrites the former in the resulting array.
    /// </remarks>
    public static TResult[] ToArrayByIndex<T, TResult>(this IEnumerable<T> source,
        Func<T, int> indexSelector, Func<T, TResult> resultSelector) {
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));
        return source.ToArrayByIndex(indexSelector, (e, _) => resultSelector(e));
    }

    /// <summary>
    ///     Creates an array from an <see cref="IEnumerable{T}" /> where a function is used to determine the index at
    ///     which an element will be placed in the array. The elements are projected into the array via an additional function.
    /// </summary>
    /// <param name="source">The source sequence for the array.</param>
    /// <param name="indexSelector">A function that maps an element to its index.</param>
    /// <param name="resultSelector">A function to project a source element into an element of the resulting array.</param>
    /// <typeparam name="T">The type of the element in <paramref name="source" />.</typeparam>
    /// <typeparam name="TResult">The type of the element in the resulting array.</typeparam>
    /// <returns>
    ///     An array that contains the projected elements from the input sequence. The size of the array will be as large
    ///     as the highest index returned by the <paramref name="indexSelector" /> plus 1.
    /// </returns>
    /// <remarks>
    ///     This method forces immediate query evaluation. It should not be used on infinite sequences. If more than one
    ///     element maps to the same index then the latter element overwrites the former in the resulting array.
    /// </remarks>
    public static TResult[] ToArrayByIndex<T, TResult>(this IEnumerable<T> source,
        Func<T, int> indexSelector, Func<T, int, TResult> resultSelector) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (indexSelector == null) throw new ArgumentNullException(nameof(indexSelector));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        var lastIndex = -1;
        List<KeyValuePair<int, T>>? indexed = null;
        List<KeyValuePair<int, T>> Indexed() => indexed ??= new List<KeyValuePair<int, T>>();

        foreach (var e in source) {
            var i = indexSelector(e);
            if (i < 0) throw new IndexOutOfRangeException();
            lastIndex = Math.Max(i, lastIndex);
            Indexed().Add(new KeyValuePair<int, T>(i, e));
        }

        var length = lastIndex + 1;
        return length == 0
            ? Array.Empty<TResult>()
            : Indexed().ToArrayByIndex(length, e => e.Key, e => resultSelector(e.Value, e.Key));
    }

    /// <summary>
    ///     Creates an array of user-specified length from an <see cref="IEnumerable{T}" /> where a function is used to
    ///     determine the index at which an element will be placed in the array.
    /// </summary>
    /// <param name="source">The source sequence for the array.</param>
    /// <param name="length">The (non-negative) length of the resulting array.</param>
    /// <param name="indexSelector">A function that maps an element to its index.</param>
    /// <typeparam name="T">The type of the element in <paramref name="source" />.</typeparam>
    /// <returns>An array of size <paramref name="length" /> that contains the elements from the input sequence.</returns>
    /// <remarks>
    ///     This method forces immediate query evaluation. It should not be used on infinite sequences. If more than one
    ///     element maps to the same index then the latter element overwrites the former in the resulting array.
    /// </remarks>
    public static T[] ToArrayByIndex<T>(this IEnumerable<T> source, int length,
        Func<T, int> indexSelector) =>
        source.ToArrayByIndex(length, indexSelector, (e, _) => e);

    /// <summary>
    ///     Creates an array of user-specified length from an <see cref="IEnumerable{T}" /> where a function is used to
    ///     determine the index at which an element will be placed in the array. The elements are projected into the array via
    ///     an additional function.
    /// </summary>
    /// <param name="source">The source sequence for the array.</param>
    /// <param name="length">The (non-negative) length of the resulting array.</param>
    /// <param name="indexSelector">A function that maps an element to its index.</param>
    /// <param name="resultSelector">A function to project a source element into an element of the resulting array.</param>
    /// <typeparam name="T">The type of the element in <paramref name="source" />.</typeparam>
    /// <typeparam name="TResult">The type of the element in the resulting array.</typeparam>
    /// <returns>An array of size <paramref name="length" /> that contains the projected elements from the input sequence.</returns>
    /// <remarks>
    ///     This method forces immediate query evaluation. It should not be used on infinite sequences. If more than one
    ///     element maps to the same index then the latter element overwrites the former in the resulting array.
    /// </remarks>
    public static TResult[] ToArrayByIndex<T, TResult>(this IEnumerable<T> source, int length,
        Func<T, int> indexSelector, Func<T, TResult> resultSelector) {
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));
        return source.ToArrayByIndex(length, indexSelector, (e, _) => resultSelector(e));
    }

    /// <summary>
    ///     Creates an array of user-specified length from an <see cref="IEnumerable{T}" /> where a function is used to
    ///     determine the index at which an element will be placed in the array. The elements are projected into the array via
    ///     an additional function.
    /// </summary>
    /// <param name="source">The source sequence for the array.</param>
    /// <param name="length">The (non-negative) length of the resulting array.</param>
    /// <param name="indexSelector">A function that maps an element to its index.</param>
    /// <param name="resultSelector">A function to project a source element into an element of the resulting array.</param>
    /// <typeparam name="T">The type of the element in <paramref name="source" />.</typeparam>
    /// <typeparam name="TResult">The type of the element in the resulting array.</typeparam>
    /// <returns>An array of size <paramref name="length" /> that contains the projected elements from the input sequence.</returns>
    /// <remarks>
    ///     This method forces immediate query evaluation. It should not be used on infinite sequences. If more than one
    ///     element maps to the same index then the latter element overwrites the former in the resulting array.
    /// </remarks>
    public static TResult[] ToArrayByIndex<T, TResult>(this IEnumerable<T> source, int length,
        Func<T, int> indexSelector, Func<T, int, TResult> resultSelector) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (length < 0) throw new ArgumentOutOfRangeException(nameof(length));
        if (indexSelector == null) throw new ArgumentNullException(nameof(indexSelector));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        var array = new TResult[length];
        foreach (var e in source) {
            var i = indexSelector(e);
            if (i < 0 || i > array.Length) throw new IndexOutOfRangeException();
            array[i] = resultSelector(e, i);
        }
        return array;
    }
    #endregion

    #region Traverse
    /// <summary>
    ///     Traverses a tree in a breadth-first fashion, starting at a root node and using a user-defined function to get
    ///     the children at each node of the tree.
    /// </summary>
    /// <typeparam name="T">The tree node type</typeparam>
    /// <param name="root">The root of the tree to traverse</param>
    /// <param name="childrenSelector">The function that produces the children of each element</param>
    /// <returns>A sequence containing the traversed values</returns>
    /// <remarks>
    ///     This function defers traversal until needed and streams the results. The tree is not checked for loops. If the
    ///     resulting sequence needs to be finite then it is the responsibility of <paramref name="childrenSelector" /> to
    ///     ensure that loops are not produced.
    /// </remarks>
    public static IEnumerable<T> TraverseBreadthFirst<T>(T root, Func<T, IEnumerable<T>> childrenSelector) {
        if (childrenSelector == null) throw new ArgumentNullException(nameof(childrenSelector));

        return _();
        IEnumerable<T> _() {
            var queue = new Queue<T>();
            queue.Enqueue(root);

            while (queue.Count != 0) {
                var current = queue.Dequeue();
                yield return current;
                foreach (var child in childrenSelector(current)) queue.Enqueue(child);
            }
        }
    }

    /// <summary>
    ///     Traverses a tree in a depth-first fashion, starting at a root node and using a user-defined function to get
    ///     the children at each node of the tree.
    /// </summary>
    /// <typeparam name="T">The tree node type</typeparam>
    /// <param name="root">The root of the tree to traverse</param>
    /// <param name="childrenSelector">The function that produces the children of each element</param>
    /// <returns>A sequence containing the traversed values</returns>
    /// <remarks>
    ///     This function defers traversal until needed and streams the results. The tree is not checked for loops. If the
    ///     resulting sequence needs to be finite then it is the responsibility of <paramref name="childrenSelector" /> to
    ///     ensure that loops are not produced.
    /// </remarks>
    public static IEnumerable<T> TraverseDepthFirst<T>(T root, Func<T, IEnumerable<T>> childrenSelector) {
        if (childrenSelector == null) throw new ArgumentNullException(nameof(childrenSelector));

        return _();
        IEnumerable<T> _() {
            var stack = new Stack<T>();
            stack.Push(root);

            while (stack.Count != 0) {
                var current = stack.Pop();
                yield return current;
                // because a stack pops the elements out in LIFO order, we need to push them in reverse
                // if we want to traverse the returned list in the same order as was returned to us
                foreach (var child in childrenSelector(current).Reverse()) stack.Push(child);
            }
        }
    }
    #endregion

    #region Windowed
    /// <summary>Processes a sequence into a series of subsequences representing a windowed subset of the original</summary>
    /// <remarks>
    ///     The number of sequences returned is: <c>Max(0, sequence.Count() - windowSize) + 1</c><br /> Returned
    ///     subsequences are buffered, but the overall operation is streamed.<br />
    /// </remarks>
    /// <typeparam name="TSource">The type of the elements of the source sequence</typeparam>
    /// <param name="source">The sequence to evaluate a sliding window over</param>
    /// <param name="size">The size (number of elements) in each window</param>
    /// <returns>A series of sequences representing each sliding window subsequence</returns>
    public static IEnumerable<IEnumerable<TSource>> Windowed<TSource>(this IEnumerable<TSource> source, int size) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (size <= 0) throw new ArgumentOutOfRangeException(nameof(size));

        return _();
        IEnumerable<IEnumerable<TSource>> _() {
            using var iter = source.GetEnumerator();
            // generate the first window of items
            var window = new TSource[size];
            int i;
            for (i = 0; i < size && iter.MoveNext(); i++) window[i] = iter.Current;

            if (i < size) yield break;

            // return the first window (whatever size it may be)
            yield return window;

            // generate the next window by shifting forward by one item
            while (iter.MoveNext()) {
                // NOTE: If we used a circular queue rather than a list, 
                //       we could make this quite a bit more efficient.
                //       Sadly the BCL does not offer such a collection.
                var newWindow = new TSource[size];
                Array.Copy(window, 1, newWindow, 0, size - 1);
                newWindow[size - 1] = iter.Current;
                yield return newWindow;
                window = newWindow;
            }
        }
    }
    #endregion
    #endregion

    #region Sorts
    /// <summary>Order By</summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="source"></param>
    /// <param name="keySelector"></param>
    /// <param name="sortType"></param>
    /// <param name="comparer"></param>
    /// <returns></returns>
    public static IComposableSortEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, SortType sortType, IComparer<TKey>? comparer = null) =>
        new ComposableSortEnumerable<TSource, TKey>(source, keySelector, sortType, comparer, false);

    /// <summary>Order By Descending</summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="source"></param>
    /// <param name="keySelector"></param>
    /// <param name="sortType"></param>
    /// <param name="comparer"></param>
    /// <returns></returns>
    public static IComposableSortEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, SortType sortType,
        IComparer<TKey>? comparer = null) =>
        new ComposableSortEnumerable<TSource, TKey>(source, keySelector, sortType, comparer, true);

    /// <summary>Then By</summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="source"></param>
    /// <param name="keySelector"></param>
    /// <param name="sortType"></param>
    /// <param name="comparer"></param>
    /// <returns></returns>
    public static IComposableSortEnumerable<TSource> ThenBy<TSource, TKey>(this IComposableSortEnumerable<TSource> source, Func<TSource, TKey> keySelector, SortType sortType,
        IComparer<TKey>? comparer = null) {
        if (source == null) throw new ArgumentNullException(nameof(source));

        source.AppendSorter(keySelector, sortType, comparer, false);
        return source;
    }
    /// <summary>Then By Descending</summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="source"></param>
    /// <param name="keySelector"></param>
    /// <param name="sortType"></param>
    /// <param name="comparer"></param>
    /// <returns></returns>
    public static IComposableSortEnumerable<TSource> ThenByDescending<TSource, TKey>(this IComposableSortEnumerable<TSource> source, Func<TSource, TKey> keySelector, SortType sortType,
        IComparer<TKey>? comparer = null) {
        if (source == null) throw new ArgumentNullException(nameof(source));

        source.AppendSorter(keySelector, sortType, comparer, true);
        return source;
    }
    #endregion
}