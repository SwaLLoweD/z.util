﻿using System.Collections.Generic;

namespace Z.Sort.Sorters;

internal class SelectSort<TKey> : Sort<TKey>, ISort<TKey>
{
    #region Constructors
    internal SelectSort(MapItem<TKey>[] map, IComparer<TKey> comparer, bool descending)
        : base(map, comparer, descending) { }
    #endregion

    #region ISort<TKey> Members
    public MapItem<TKey>[] Sort() {
        var lastItem = Map.Length - 1;
        for (var i = 0; i < lastItem; i++) {
            var min = i;
            for (var j = i + 1; j <= lastItem; j++) {
                if (CompareKeys(j, min) < 0)
                    min = j;
            }
            Swap(i, min);
        }

        return Map;
    }
    #endregion
}