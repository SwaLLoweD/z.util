﻿using System.Collections.Generic;

namespace Z.Sort.Sorters;

internal class ShellSort<TKey> : Sort<TKey>, ISort<TKey>
{
    #region Constructors
    internal ShellSort(MapItem<TKey>[] map, IComparer<TKey> comparer, bool descending)
        : base(map, comparer, descending) { }
    #endregion

    #region ISort<TKey> Members
    public MapItem<TKey>[] Sort() {
        var len = Map.Length;
        var increment = 3;

        while (increment > 0) {
            for (var i = 0; i < len; i++) {
                var j = i;
                var temp = Map[i];
                while (j >= increment && CompareKeys(Map[j - increment].Key, temp.Key) > 0) {
                    Map[j] = Map[j - increment];
                    j -= increment;
                }
                Map[j] = temp;
            }

            var newIncrement = increment >> 1;

            if (newIncrement != 0)
                increment = newIncrement;
            else if (increment == 1)
                increment = 0;
            else
                increment = 1;
        }

        return Map;
    }
    #endregion
}
