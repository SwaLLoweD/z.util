﻿using System.Collections.Generic;

namespace Z.Sort.Sorters;

internal class BubbleSort<TKey> : Sort<TKey>, ISort<TKey>
{
    #region Constructors
    internal BubbleSort(MapItem<TKey>[] map, IComparer<TKey> comparer, bool descending)
        : base(map, comparer, descending) { }
    #endregion

    #region ISort<TKey> Members
    public MapItem<TKey>[] Sort() {
        var itemsCount = Map.Length;

        for (var i = 0; i <= itemsCount; i++) {
            // Loop a second time from the back of the array
            for (var j = itemsCount - 1; j > i; j--) {
                // Swap the elements if necessary
                if (CompareKeys(j - 1, j) > 0)
                    Swap(j - 1, j);
            }
        }

        return Map;
    }
    #endregion
}