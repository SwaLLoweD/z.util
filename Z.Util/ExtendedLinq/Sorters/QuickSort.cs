﻿using System.Collections.Generic;

namespace Z.Sort.Sorters
{
    internal class QuickSort<TKey> : Sort<TKey>, ISort<TKey>
    {
        #region Constructors
        internal QuickSort(MapItem<TKey>[] map, IComparer<TKey> comparer, bool descending)
            : base(map, comparer, descending) { }
        #endregion

        #region ISort<TKey> Members
        public MapItem<TKey>[] Sort() {
            Sort(0, Map.Length - 1);
            return Map;
        }
        #endregion

        private void Sort(int left, int right) {
            var pivot = Map[(left + right) >> 1].Key;
            var l = left - 1;
            var r = right + 1;

            while (true) {
                do {
                    r--;
                } while (CompareKeys(pivot, Map[r].Key) < 0);

                do {
                    l++;
                } while (CompareKeys(pivot, Map[l].Key) > 0);

                if (l < r)
                    Swap(l, r);
                else
                    break;
            }
            var middle = r; // Set middle index  

            if (left + 1 < right) {
                Sort(left, middle); // Sort first section
                Sort(middle + 1, right); // Sort second section
            }
        }
    }
}