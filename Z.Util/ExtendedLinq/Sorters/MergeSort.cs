﻿using System.Collections.Generic;

namespace Z.Sort.Sorters
{
    internal class MergeSort<TKey> : Sort<TKey>, ISort<TKey>
    {
        #region Constructors
        internal MergeSort(MapItem<TKey>[] map, IComparer<TKey> comparer, bool descending)
            : base(map, comparer, descending) { }
        #endregion

        #region ISort<TKey> Members
        public MapItem<TKey>[] Sort() {
            var tempMap = new MapItem<TKey>[Map.Length];
            Sort(0, Map.Length - 1, tempMap);
            return Map;
        }
        #endregion

        public void Sort(int left, int right, MapItem<TKey>[] tempMap) {
            if (right > left) {
                var mid = (right + left) >> 1;
                Sort(left, mid, tempMap);
                Sort(mid + 1, right, tempMap);
                Merge(left, mid + 1, right, tempMap);
            }
        }

        public void Merge(int left, int mid, int right, MapItem<TKey>[] tempMap) {
            var leftEnd = mid - 1;
            var tmpPos = left;
            var numElements = right - left + 1;

            while (left <= leftEnd && mid <= right) {
                if (CompareKeys(Map[left].Key, Map[mid].Key) <= 0) {
                    tempMap[tmpPos] = Map[left];
                    left++;
                } else {
                    tempMap[tmpPos] = Map[mid];
                    mid++;
                }
                tmpPos++;
            }

            while (left <= leftEnd) {
                tempMap[tmpPos] = Map[left];
                left++;
                tmpPos++;
            }

            while (mid <= right) {
                tempMap[tmpPos] = Map[mid];
                mid++;
                tmpPos++;
            }

            for (var i = 0; i < numElements; i++) {
                Map[right] = tempMap[right];
                right--;
            }
        }
    }
}