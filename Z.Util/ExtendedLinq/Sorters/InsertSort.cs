﻿using System.Collections.Generic;

namespace Z.Sort.Sorters
{
    internal class InsertSort<TKey> : Sort<TKey>, ISort<TKey>
    {
        #region Constructors
        internal InsertSort(MapItem<TKey>[] map, IComparer<TKey> comparer, bool descending)
            : base(map, comparer, descending) { }
        #endregion

        #region ISort<TKey> Members
        public MapItem<TKey>[] Sort() {
            var lastindex = Map.Length - 1;
            // Loop through all elements in the original array from the second element
            for (var j = 1; j <= lastindex; j++) {
                var item = Map[j];
                var i = j - 1;
                // Loop through all elements from the key to the start
                // Check if the current element is smaller than the key
                while (i >= 0 && CompareKeys(Map[i].Key, item.Key) > 0) {
                    // Move the current element backward
                    Map[i + 1] = Map[i];
                    i--;
                }
                // Finally move the key
                Map[i + 1] = item;
            }

            return Map;
        }
        #endregion
    }
}