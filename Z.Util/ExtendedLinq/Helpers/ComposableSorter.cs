﻿using System;
using System.Collections.Generic;
using System.Linq;
using Z.Sort;
using Z.Sort.Sorters;

namespace Z.Linq;

#nullable disable
#pragma warning disable IDE0066
internal abstract class ComposableSorter<TElement>
{
    #region Fields / Properties
    internal ComposableSorter<TElement> Next;
    #endregion

    internal abstract IEnumerator<TElement> Sort(IEnumerable<TElement> source);
}

internal class ComposableSorter<TElement, TKey> : ComposableSorter<TElement>
{
    #region Fields / Properties
    private readonly IComparer<TKey> comparer;
    private readonly bool descending;

    private readonly Func<TElement, TKey> keySelector;
    private readonly SortType sortType;

    private MapItem<TKey>[] map;
    #endregion

    #region Constructors
    internal ComposableSorter(Func<TElement, TKey> keySelector, SortType sortType, IComparer<TKey> comparer, bool descending) {
        this.keySelector = keySelector;
        this.comparer = comparer;
        this.descending = descending;
        this.sortType = sortType;
    }
    #endregion

    internal int CompareKeys(int index1, int index2) {
        var res = comparer.Compare(map[index1].Key, map[index2].Key);
        return descending ? -res : res;
    }

    internal int CompareKeys(TKey key1, TKey key2) {
        var res = comparer.Compare(key1, key2);
        return descending ? -res : res;
    }

    private void DoSort() {
        if (map.Length > 1) {
            ISort<TKey> sorter;
            switch (sortType) {
                case SortType.Quick:
                    sorter = new QuickSort<TKey>(map, comparer, descending);
                    break;
                case SortType.Merge:
                    sorter = new MergeSort<TKey>(map, comparer, descending);
                    break;
                case SortType.Insert:
                    sorter = new InsertSort<TKey>(map, comparer, descending);
                    break;
                case SortType.Heap:
                    sorter = new HeapSort<TKey>(map, comparer, descending);
                    break;
                case SortType.Bubble:
                    sorter = new BubbleSort<TKey>(map, comparer, descending);
                    break;
                case SortType.Shell:
                    sorter = new ShellSort<TKey>(map, comparer, descending);
                    break;
                case SortType.Select:
                    sorter = new SelectSort<TKey>(map, comparer, descending);
                    break;
                default:
#if DEBUG
                    throw new NotImplementedException();
#else
            sorter = new QuickSort<TKey>(map, comparer, descending);
            break;
#endif
            }
            map = sorter.Sort();
        }
    }

    internal override IEnumerator<TElement> Sort(IEnumerable<TElement> source) {
        var sourceArr = source.ToArray();
        var itemsCount = sourceArr.Length;

        if (itemsCount == 0) yield break;

        if (itemsCount == 1) {
            yield return sourceArr[0];
        } else {
            map = new MapItem<TKey>[itemsCount];
            for (var i = 0; i < itemsCount; i++) map[i] = new MapItem<TKey>(i, keySelector(sourceArr[i]));

            DoSort();
            if (Next == null) {
                for (var i = 0; i < itemsCount; i++) yield return sourceArr[map[i].Index];
            } else {
                var last = itemsCount - 1;
                var current = 0;
                var start = -1;

                while (current < last) {
                    var nextindex = current + 1;
                    var cmpRes = CompareKeys(current, nextindex);
                    if (cmpRes == 0) // Equal
                    {
                        if (start == -1) start = current;
                    } else // Diff
                    {
                        if (start != -1) {
                            var elements = nextindex - start;
                            var subSource = new TElement[elements];
                            for (var i = 0; i < elements; i++) subSource[i] = sourceArr[map[i + start].Index];

                            var sa = Next.Sort(subSource);
                            while (sa.MoveNext()) yield return sa.Current;
                            start = -1;
                        } else {
                            yield return sourceArr[map[current].Index];
                        }
                    }
                    current++;
                }
                if (start != -1) {
                    var elements = current + 1 - start;
                    var subSource = new TElement[elements];
                    for (var i = 0; i < elements; i++) subSource[i] = sourceArr[map[i + start].Index];

                    var sa = Next.Sort(subSource);
                    while (sa.MoveNext()) yield return sa.Current;
                } else if (current == last) {
                    yield return sourceArr[map[last].Index];
                }
            }
        }
    }
}