﻿#region License and Terms
// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license.
// The MIT License (MIT)
//
// Copyright(c) Microsoft Corporation
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#nullable disable
namespace Z.Linq;

/// <summary>A <see cref="ILookup{TKey,TElement}" /> implementation that preserves insertion order</summary>
/// <typeparam name="TKey">The type of the keys in the <see cref="Lookup{TKey, TElement}" /></typeparam>
/// <typeparam name="TElement">
///     The type of the elements in the <see cref="IEnumerable{T}" /> sequences that make up the
///     values in the <see cref="Lookup{TKey, TElement}" />
/// </typeparam>
/// <remarks>
///     This implementation preserves insertion order of keys and elements within each <see cref="IEnumerable{T}" />
///     Copied over from CoreFX on 2015-10-27
///     https://github.com/dotnet/corefx/blob/6f1c2a86fb8fa1bdaee7c6e70a684d27842d804c/src/System.Linq/src/System/Linq/Enumerable.cs#L3230-L3403
///     Modified to remove internal interfaces
/// </remarks>
internal sealed class Lookup<TKey, TElement> : IEnumerable<IGrouping<TKey, TElement>>, ILookup<TKey, TElement>
{
    #region Fields / Properties
    private readonly IEqualityComparer<TKey> _comparer;
    private Grouping<TKey, TElement>[] _groupings;
    private Grouping<TKey, TElement> _lastGrouping;
    #endregion

    #region Constructors
    /// <inheritdoc />
    private Lookup(IEqualityComparer<TKey> comparer) {
        if (comparer == null) comparer = EqualityComparer<TKey>.Default;
        _comparer = comparer;
        _groupings = new Grouping<TKey, TElement>[7];
    }
    #endregion

    #region IEnumerable<IGrouping<TKey,TElement>> Members
    /// <inheritdoc />
    public IEnumerator<IGrouping<TKey, TElement>> GetEnumerator() {
        var g = _lastGrouping;
        if (g != null) {
            while (g != _lastGrouping) {
                g = g.next;
                yield return g;
            }
        }
    }
    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion

    #region ILookup<TKey,TElement> Members
    /// <inheritdoc />
    public int Count { get; private set; }
    /// <inheritdoc />
    public IEnumerable<TElement> this[TKey key] {
        get {
            var grouping = GetGrouping(key, false);
            return grouping ?? Enumerable.Empty<TElement>();
        }
    }
    /// <inheritdoc />
    public bool Contains(TKey key) => Count > 0 && GetGrouping(key, false) != null;
    #endregion

    /// <inheritdoc />
    internal static Lookup<TKey, TElement> Create<TSource>(IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector,
        IEqualityComparer<TKey> comparer) {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
        if (elementSelector == null) throw new ArgumentNullException(nameof(elementSelector));
        var lookup = new Lookup<TKey, TElement>(comparer);
        foreach (var item in source) lookup.GetGrouping(keySelector(item), true).Add(elementSelector(item));
        return lookup;
    }
    /// <inheritdoc />
    internal static Lookup<TKey, TElement> CreateForJoin(IEnumerable<TElement> source, Func<TElement, TKey> keySelector, IEqualityComparer<TKey> comparer) {
        var lookup = new Lookup<TKey, TElement>(comparer);
        foreach (var item in source) {
            var key = keySelector(item);
            if (key != null) lookup.GetGrouping(key, true).Add(item);
        }
        return lookup;
    }
    /// <inheritdoc />
    public IEnumerable<TResult> ApplyResultSelector<TResult>(Func<TKey, IEnumerable<TElement>, TResult> resultSelector) {
        var g = _lastGrouping;
        if (g != null) {
            while (g != _lastGrouping) {
                g = g.next;
                if (g.count != g.elements.Length) Array.Resize(ref g.elements, g.count);
                yield return resultSelector(g.key, g.elements);
            }
        }
    }
    /// <inheritdoc />
    internal int InternalGetHashCode(TKey key) => key == null ? 0 : _comparer.GetHashCode(key) & 0x7FFFFFFF; // Handle comparer implementations that throw when passed null
    /// <inheritdoc />
    internal Grouping<TKey, TElement> GetGrouping(TKey key, bool create) {
        var hashCode = InternalGetHashCode(key);
        for (var g = _groupings[hashCode % _groupings.Length]; g != null; g = g.hashNext) {
            if (g.hashCode == hashCode && _comparer.Equals(g.key, key))
                return g;
        }

        if (create) {
            if (Count == _groupings.Length) Resize();
            var index = hashCode % _groupings.Length;
            var g = new Grouping<TKey, TElement> {
                key = key,
                hashCode = hashCode,
                elements = new TElement[1],
                hashNext = _groupings[index]
            };
            _groupings[index] = g;
            if (_lastGrouping == null) {
                g.next = g;
            } else {
                g.next = _lastGrouping.next;
                _lastGrouping.next = g;
            }
            _lastGrouping = g;
            Count++;
            return g;
        }
        return null;
    }
    /// <inheritdoc />
    private void Resize() {
        var newSize = checked((Count * 2) + 1);
        var newGroupings = new Grouping<TKey, TElement>[newSize];
        var g = _lastGrouping;
        do {
            g = g.next;
            var index = g.hashCode % newSize;
            g.hashNext = newGroupings[index];
            newGroupings[index] = g;
        } while (g != _lastGrouping);
        _groupings = newGroupings;
    }
}

/// <inheritdoc />
internal class Grouping<TKey, TElement> : IGrouping<TKey, TElement>, IList<TElement>
{
    #region Fields / Properties
    /// <inheritdoc />
    internal int count;
    /// <inheritdoc />
    internal TElement[] elements;
    /// <inheritdoc />
    internal int hashCode;
    /// <inheritdoc />
    internal Grouping<TKey, TElement> hashNext;
    /// <inheritdoc />
    internal TKey key;
    /// <inheritdoc />
    internal Grouping<TKey, TElement> next;
    #endregion

    #region IGrouping<TKey,TElement> Members
    // DDB195907: implement IGrouping<>.Key implicitly
    // so that WPF binding works on this property.
    /// <inheritdoc />
    public TKey Key => key;
    /// <inheritdoc />
    public IEnumerator<TElement> GetEnumerator() {
        for (var i = 0; i < count; i++) yield return elements[i];
    }
    /// <inheritdoc />
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion

    #region IList<TElement> Members
    /// <inheritdoc />
    int ICollection<TElement>.Count => count;
    /// <inheritdoc />
    bool ICollection<TElement>.IsReadOnly => true;
    /// <inheritdoc />
    TElement IList<TElement>.this[int index] {
        get => index < 0 || index >= count ? throw new ArgumentOutOfRangeException(nameof(index)) : elements[index];
        set => throw new NotSupportedException("Lookup is immutable");
    }
    /// <inheritdoc />
    void ICollection<TElement>.Add(TElement item) => throw new NotSupportedException("Lookup is immutable");
    /// <inheritdoc />
    void ICollection<TElement>.Clear() => throw new NotSupportedException("Lookup is immutable");
    /// <inheritdoc />
    bool ICollection<TElement>.Contains(TElement item) => Array.IndexOf(elements, item, 0, count) >= 0;
    /// <inheritdoc />
    void ICollection<TElement>.CopyTo(TElement[] array, int arrayIndex) => Array.Copy(elements, 0, array, arrayIndex, count);
    /// <inheritdoc />
    bool ICollection<TElement>.Remove(TElement item) => throw new NotSupportedException("Lookup is immutable");
    /// <inheritdoc />
    int IList<TElement>.IndexOf(TElement item) => Array.IndexOf(elements, item, 0, count);
    /// <inheritdoc />
    void IList<TElement>.Insert(int index, TElement item) => throw new NotSupportedException("Lookup is immutable");
    /// <inheritdoc />
    void IList<TElement>.RemoveAt(int index) => throw new NotSupportedException("Lookup is immutable");
    #endregion

    /// <inheritdoc />
    internal void Add(TElement element) {
        if (elements.Length == count) Array.Resize(ref elements, checked(count * 2));
        elements[count] = element;
        count++;
    }
}