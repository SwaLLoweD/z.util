﻿using System;
using System.Collections.Generic;

namespace Z.Linq;

/// <summary>Creates an <see cref="IComparer{T}" /> instance for the given delegate function.</summary>
internal sealed class ComparerFactory<T> : IComparer<T>
{
    #region Fields / Properties
    private readonly Func<T?, T?, int> _comparison;
    #endregion

    #region Constructors
    private ComparerFactory(Func<T?, T?, int> comparison) => _comparison = comparison;
    #endregion

    #region IComparer<T> Members
    /// <summary>Compare method</summary>
    public int Compare(T? x, T? y) => _comparison(x, y);
    #endregion

    /// <summary>Create Comparer</summary>
    public static IComparer<T> Create(Func<T?, T?, int> comparison) => new ComparerFactory<T>(comparison);
}