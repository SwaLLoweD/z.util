﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime;

namespace Z.Linq;

internal class ComposableSortEnumerable<TElement, TKey> : IComposableSortEnumerable<TElement>
{
    #region Fields / Properties
    internal ComposableSorter<TElement> ComposableSorter;
    internal IEnumerable<TElement> Source;
    internal IComparer<TKey> Comparer;
    internal bool Descending;
    internal Func<TElement, TKey> KeySelector;
    internal SortType SortType;
    #endregion

    #region Constructors
    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
    internal ComposableSortEnumerable(IEnumerable<TElement> source, Func<TElement, TKey> keySelector, SortType sortType, IComparer<TKey>? comparer, bool descending) {
        if (!Enum.IsDefined(typeof(SortType), sortType)) sortType = SortType.Quick;

        Source = source ?? throw new ArgumentNullException(nameof(source));
        KeySelector = keySelector ?? throw new ArgumentNullException(nameof(keySelector));
        SortType = sortType;

        var defaultComparer = comparer ?? Comparer<TKey>.Default;

        Comparer = defaultComparer;
        Descending = descending;

        ComposableSorter = new ComposableSorter<TElement, TKey>(KeySelector, SortType, Comparer, Descending);
    }
    #endregion

    #region IComposableSortEnumerable<TElement> Members
#pragma warning disable
    /// <inheritdoc />
    public void AppendSorter<TKey>(Func<TElement, TKey> keySelector, SortType sortType, IComparer<TKey>? comparer, bool descending) {
        var composableSortEnumerable = new ComposableSortEnumerable<TElement, TKey>(this, keySelector, sortType, comparer, descending);
        SetNext(ComposableSorter, composableSortEnumerable.ComposableSorter);
    }
#pragma warning restore

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public IEnumerator<TElement> GetEnumerator() => ComposableSorter.Sort(Source);
    #endregion

    private static void SetNext(ComposableSorter<TElement> firstSorter, ComposableSorter<TElement> secondSorter) {
        if (firstSorter.Next == null)
            firstSorter.Next = secondSorter;
        else
            SetNext(firstSorter.Next, secondSorter);
    }
}

/// <summary>IComposableSortEnumerable</summary>
/// <typeparam name="TElement"></typeparam>
public interface IComposableSortEnumerable<out TElement> : IEnumerable<TElement>
{
    /// <summary>AppendSorter</summary>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="keySelector"></param>
    /// <param name="sortType"></param>
    /// <param name="comparer"></param>
    /// <param name="descending"></param>
    void AppendSorter<TKey>(Func<TElement, TKey> keySelector, SortType sortType, IComparer<TKey>? comparer, bool descending);
}

/// <summary>T</summary>
public enum SortType
{
    /// <summary>Quick</summary>
    Quick,
    /// <summary>Heap</summary>
    Heap,
    /// <summary>Bubble</summary>
    Bubble,
    /// <summary>Insert</summary>
    Insert,
    /// <summary>Merge</summary>
    Merge,
    /// <summary>Shell</summary>
    Shell,
    /// <summary>Select</summary>
    Select
}