﻿/* Copyright (c) <202> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Linq.Expressions;

namespace Z.Linq;

/// <summary>Extension methods for all kind of Queryables implementing the IQueryable&lt;T&gt; interface</summary>
public static class ExtIQueryable {
    /// <summary>Converts the specified Enumerable to a different type.</summary>
    /// <param name="value">The value to be converted.</param>
    /// <returns>An universal converter supplying additional target conversion methods</returns>
    public static SortPortion<T> Sort<T>(this IQueryable<T> value) => new(value);

    #region Sorter
    /// <summary>Multi-method library for list conversion related commands.</summary>
    public class SortPortion<T>: ExtendablePortion<IQueryable<T>> {
        #region Constructors
        /// <summary>Constructor</summary>
        public SortPortion(IQueryable<T> value) : base(value) { }
        #endregion

        /// <summary>OrderBy using string propert names</summary>
        public IOrderedQueryable<T> Asc(string propertyName, IComparer<object>? comparer = null) => CallOrderMethod("OrderBy", propertyName, comparer);
        /// <summary>OrderBy using string propert names</summary>
        public IOrderedQueryable<T> Desc(string propertyName, IComparer<object>? comparer = null) => CallOrderMethod("OrderByDescending", propertyName, comparer);
        /// <summary>OrderBy using string propert names</summary>
        public IOrderedQueryable<T> ThenAsc(string propertyName, IComparer<object>? comparer = null) => CallOrderMethod("ThenBy", propertyName, comparer);
        /// <summary>OrderBy using string propert names</summary>
        public IOrderedQueryable<T> ThenDesc(string propertyName, IComparer<object>? comparer = null) => CallOrderMethod("ThenByDescending", propertyName, comparer);

        /// <summary>
        ///     Builds the Queryable functions using a TSource property name.
        /// </summary>
        private IOrderedQueryable<T> CallOrderMethod(string methodName, string propertyName, IComparer<object>? comparer = null) {
            var param = Expression.Parameter(typeof(T), "x");

            var body = propertyName.Split('.').Aggregate<string, Expression>(param, Expression.PropertyOrField);

            return comparer != null
                ? (IOrderedQueryable<T>)Value.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        [typeof(T), body.Type],
                        Value.Expression,
                        Expression.Lambda(body, param),
                        Expression.Constant(comparer)
                    )
                )
                : (IOrderedQueryable<T>)Value.Provider.CreateQuery(
                    Expression.Call(
                        typeof(Queryable),
                        methodName,
                        [typeof(T), body.Type],
                        Value.Expression,
                        Expression.Lambda(body, param)
                    )
                );
        }
        #endregion
    }
}
