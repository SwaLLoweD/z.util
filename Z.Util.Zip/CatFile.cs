﻿using System;
using System.Collections.Generic;
using System.IO;

#pragma warning disable 1591
namespace Z.Util;

public class CatEntry
{
    #region Fields / Properties
    public string Hash;
    public string Path;
    public int Size;
    public DateTime Timestamp;
    #endregion

    #region Constructors
    public CatEntry(string catLine) {
        var split = catLine.Split(new[] { " " }, StringSplitOptions.None);
        Hash = split[^1];
        Timestamp = UnixTimeStampToDateTime(int.Parse(split[^2]));
        Size = int.Parse(split[^3]);
        split = catLine.Split(new[] { " " + split[^3] }, StringSplitOptions.None);
        Path = split[0];
    }
    #endregion

    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp) {
        // Unix timestamp is seconds past epoch
        var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        return dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
    }
}

public class CatFile
{
    #region Fields / Properties
    public FileStream DatStream;
    public List<CatEntry> Entries;
    #endregion

    #region Constructors
    public CatFile(string catPath) {
        Entries = new List<CatEntry>();
        var ents = File.ReadAllLines(catPath);
        foreach (var ent in ents) Entries.Add(new CatEntry(ent));

        DatStream = File.OpenRead(Path.Combine(Path.GetDirectoryName(catPath), Path.GetFileNameWithoutExtension(catPath)) + ".dat");
    }
    #endregion

    public long GetEntryOffset(CatEntry entry) {
        long offset = 0;
        foreach (var ent in Entries) {
            if (ent != entry)
                offset += ent.Size;
            else
                break;
        }

        return offset;
    }
    public byte[] ExtractFile(CatEntry entry) {
        var offset = GetEntryOffset(entry);
        DatStream.Seek(offset, SeekOrigin.Begin);
        var data = new byte[entry.Size];
        DatStream.Read(data, 0, entry.Size);
        return data;
    }

    public void ExtractAll(string path) {
        for (var i = 0; i < Entries.Count; i++) {
            var ent = Entries[i];
            Console.WriteLine("Extracting " + ent.Path);
            var newpath = Path.Combine(path, ent.Path);
            try {
                Directory.CreateDirectory(Path.GetDirectoryName(newpath));
            } catch { }
            File.WriteAllBytes(newpath, ExtractFile(ent));
        }
    }

    public string GetLongestPath() {
        var longest = "";
        foreach (var ent in Entries) {
            if (ent.Path.Length > longest.Length)
                longest = ent.Path;
        }

        return longest;
    }
}