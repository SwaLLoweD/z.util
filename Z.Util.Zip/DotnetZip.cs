﻿using System;
using System.Collections.Generic;
using System.IO;
using Ionic.Zip;
using Ionic.Zlib;
using tar_cs;
using Z.Extensions;

namespace Z.Util
{
    /// <summary>Compressed Archive File</summary>
    public abstract class ArchiveFile
    {
        /// <summary>Compressed data</summary>
        protected Hashtable<string, byte[]> bArrays = new Hashtable<string, byte[]>();
        /// <summary>Compressed data</summary>
        protected Hashtable<string, Stream> streams = new Hashtable<string, Stream>();
        /// <summary>File names</summary>
        protected List<string> files = new List<string>();
        /// <summary>Compression level 0-9 (0=fastest, 9=maxCompression)</summary>
        public virtual byte CompressionLevel { get; set; }
        /// <summary>Data is encrypted or not</summary>
        public virtual bool Encrypt { get; set; }
        /// <summary>Self-extracting archive or not</summary>
        public virtual bool Sfx { get; set; }

        /// <summary>Constructor</summary>
        /// <param name="compressionLevel">Compression level 0-9 (0=fastest, 9=maxCompression)</param>
        /// <param name="encrypt">Data is encrypted or not</param>
        /// <param name="sfx">Self-extracting archive or not</param>
        public ArchiveFile(byte compressionLevel = 6, bool encrypt = false, bool sfx = false)
        {
            CompressionLevel = compressionLevel;
            Encrypt = encrypt;
            Sfx = sfx;
        }
        /// <summary>Add file</summary>
        /// <param name="file">File stream</param>
        /// <param name="name">File name</param>
        public virtual void Add(Stream file, string name) { streams[name] = file; }

        /// <summary>Add file</summary>
        /// <param name="file">File data</param>
        /// <param name="name">File name</param>
        public virtual void Add(byte[] file, string name) { bArrays[name] = file; }

        /// <summary>Add file</summary>
        /// <param name="files">File name and stream</param>
        public virtual void Add(IDictionary<string, Stream> files) { streams.AddUnique(files); }

        /// <summary>Add file</summary>
        /// <param name="files">File name data</param>
        public virtual void Add(IDictionary<string, byte[]> files) { bArrays.AddUnique(files); }

        /// <summary>Add file</summary>
        /// <param name="files">File stream and name</param>
        public virtual void Add(params string[] files) { files.AddUnique(files); }

        /// <summary>Create archive</summary>
        /// <param name="file">Archive path/name</param>
        public abstract void Create(string file);

        /// <summary>Create archive</summary>
        /// <param name="file">Archive path/name</param>
        public abstract void Create(Stream file);
    }
    #region Zip
    /// <summary>Zip Archive File</summary>
    public class Zip : ArchiveFile
    {
        private ZipFile zipFl { get; set; }
        private void prepare(ZipFile zip)
        {
            if (Encrypt) zip.Encryption = EncryptionAlgorithm.PkzipWeak;
            zip.CompressionLevel = getCompressionLevel();
            foreach (var f in files) zip.AddFile(f);
            foreach (var s in streams) zip.AddEntry(s.Key, s.Value);
            foreach (var b in bArrays) zip.AddEntry(b.Key, b.Value);
            zipFl = zip;
        }
        private CompressionLevel getCompressionLevel()
        {
            if (CompressionLevel < 0) CompressionLevel = 0;
            if (CompressionLevel > 9) CompressionLevel = 9;
            return (Ionic.Zlib.CompressionLevel)CompressionLevel;
        }
        /// <inheritdoc/>
        public override void Create(string zipFile)
        {
            ZipFile zip = new ZipFile();
            prepare(zip);
            zip = zipFl;
            //zip.ParallelDeflateThreshold = -1;
            if (Sfx) zip.SaveSelfExtractor(zipFile, SelfExtractorFlavor.WinFormsApplication);
            else zip.Save(zipFile);
            zip.Dispose();
        }
        /// <inheritdoc/>
        public override void Create(Stream zipFile)
        {
            using (ZipFile zip = new ZipFile())
            {
                prepare(zip);
                zip.Save(zipFile);
            }
        }
        /// <summary>Extract files</summary>
        /// <param name="zipFile">Archive file</param>
        /// <param name="extractDir">Extract to</param>
        /// <param name="overwrite">Overwrite existing</param>
        /// <returns>extracted file names</returns>
        public static string[] Extract(string zipFile, string extractDir, bool overwrite = true)
        {
            var rval = new List<string>();
            using (ZipFile zip1 = ZipFile.Read(zipFile))
            {
                var opt = overwrite ? ExtractExistingFileAction.OverwriteSilently : ExtractExistingFileAction.DoNotOverwrite;
                foreach (ZipEntry e in zip1)
                {

                    e.Extract(extractDir, opt);
                    rval.Add(e.FileName);
                }
            }
            return rval.ToArray();
        }
        /// <summary>Extract files</summary>
        /// <param name="zipFile">Archive file</param>
        /// <param name="extractDir">Extract to</param>
        /// <param name="overwrite">Overwrite existing</param>
        /// <returns>extracted file names</returns>
        public static string[] Extract(Stream zipFile, string extractDir, bool overwrite = true)
        {
            var rval = new List<string>();
            using (ZipFile zip1 = ZipFile.Read(zipFile))
            {
                var opt = overwrite ? ExtractExistingFileAction.OverwriteSilently : ExtractExistingFileAction.DoNotOverwrite;
                foreach (ZipEntry e in zip1)
                {

                    e.Extract(extractDir, opt);
                    rval.Add(e.FileName);
                }
            }
            return rval.ToArray();
        }
    }
    #endregion
    #region Tar
    /// <summary>Tar Archive File</summary>
    public class Tar : ArchiveFile
    {
        private void prepare(TarWriter tw)
        {
            //if (Encrypt) zip.Encryption = EncryptionAlgorithm.PkzipWeak;
            foreach (var f in files) tw.Write(f);
            foreach (var s in streams) tw.Write(s.Value, s.Value.Length, s.Key);
            foreach (var b in bArrays)
            {
                using (MemoryStream ms = new MemoryStream(b.Value))
                {
                    tw.Write(ms, b.Value.Length, b.Key);
                }
            }
        }
        private CompressionLevel getCompressionLevel()
        {
            if (CompressionLevel < 0) CompressionLevel = 0;
            if (CompressionLevel > 9) CompressionLevel = 9;
            return (Ionic.Zlib.CompressionLevel)CompressionLevel;
        }

        /// <inheritdoc/>
        public override void Create(string file)
        {
            using (var outFile = File.Create("myOutFile.tar.gz"))
            {
                using (var outStream = new GZipStream(outFile, CompressionMode.Compress, getCompressionLevel()))
                {
                    using (var writer = new TarWriter(outStream))
                    {
                        prepare(writer);
                    }
                }
            }
        }
        /// <inheritdoc/>
        public override void Create(Stream zipFile) { throw new NotImplementedException("Stream output is not supported in Tar format"); }

        /// <summary>Extract files</summary>
        /// <param name="tarFile">Archive file</param>
        /// <param name="extractDir">Extract to</param>
        /// <param name="overwrite">Overwrite existing</param>
        /// <returns>extracted file names</returns>
        public static string[] Extract(string tarFile, string extractDir, bool overwrite = true)
        {
            if (!Directory.Exists(extractDir)) Directory.CreateDirectory(extractDir);
            var rval = new List<string>();
            FileStream unarchFile = File.OpenRead(tarFile);
            Stream fileStream = unarchFile;
            if (tarFile.EndsWith(".gz") || tarFile.EndsWith(".tgz")) fileStream = new GZipStream(unarchFile, CompressionMode.Decompress);

            TarReader reader = new TarReader(fileStream);
            while (reader.MoveNext(true))
            {
                var fileName = reader.FileInfo.FileName.Replace("./", "").Replace("/", SysInf.bs).Trim();
                if (fileName.Is().Empty) continue;
                var fullFn = extractDir.Path().Append(fileName);
                if (fullFn.EndsWith(SysInf.bs))
                {
                    if (!Directory.Exists(fullFn)) Directory.CreateDirectory(fullFn);
                    continue;
                }
                if (!overwrite && File.Exists(fullFn)) continue;
                rval.Add(fileName);
                using (FileStream outStream = File.OpenWrite(fullFn))
                {
                    reader.Read(outStream);
                }
            }
            try { fileStream.Close(); }
            catch (Exception exp) { if (!exp.Message.StartsWith("Bad CRC32")) throw exp; }
            fileStream.Dispose();
            fileStream = null;
            if (unarchFile != null)
            {
                unarchFile.Close();
                unarchFile.Dispose();
            }
            return rval.ToArray();
        }
    }
    #endregion
}

