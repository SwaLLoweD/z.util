﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using SharpCompress.Common;
using SharpCompress.Readers;
using SharpCompress.Writers;
using Z.Extensions;

namespace Z.Util;

/// <summary>Compressed Archive File</summary>
public class ArchiveFile
{
    #region ArchiveTypes enum
    //private CompressionLevel getCompressionLevel()
    //{
    //    if (CompressionLevel < 0) CompressionLevel = 0;
    //    if (CompressionLevel > 9) CompressionLevel = 9;
    //    return (CompressionLevel)CompressionLevel;
    //}

    /// <summary>Archive Types</summary>
    public enum ArchiveTypes
    {
        /// <summary>Auto</summary>
        Auto = 0,
        /// <summary>Zip</summary>
        Zip = 1,
        /// <summary>Tar</summary>
        Tar = 2,
        /// <summary>Bzip2</summary>
        Bzip2 = 4,
        /// <summary>Gzip</summary>
        Gzip = 5,
        /// <summary>7-zip</summary>
        SevenZip = 6,
        /// <summary>Rar</summary>
        Rar = 7,
    }
    #endregion

    #region Fields / Properties
    /// <summary>Compressed data</summary>
    protected IDictionary<string, byte[]> bArrays = new Dictionary<string, byte[]>();
    /// <summary>File names</summary>
    protected List<string> fileNames = new();
    /// <summary>Compressed data</summary>
    protected IDictionary<string, Stream> streams = new Dictionary<string, Stream>();
    /// <summary>Archive Type</summary>
    public virtual ArchiveTypes ArchiveType { get; set; }
    /// <summary>Compression level 0-9 (0=fastest, 9=maxCompression)</summary>
    public virtual byte CompressionLevel { get; set; }
    /// <summary>Data is encrypted or not</summary>
    public virtual bool Encrypt { get; set; }
    /// <summary>Self-extracting archive or not</summary>
    public virtual bool Sfx { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    /// <param name="compressionLevel">Compression level 0-9 (0=fastest, 9=maxCompression)</param>
    /// <param name="encrypt">Data is encrypted or not</param>
    /// <param name="sfx">Self-extracting archive or not</param>
    public ArchiveFile(byte compressionLevel = 6, bool encrypt = false, bool sfx = false) {
        CompressionLevel = compressionLevel;
        Encrypt = encrypt;
        Sfx = sfx;
    }
    #endregion

    /// <summary>Add file</summary>
    /// <param name="file">File stream</param>
    /// <param name="name">File name</param>
    public virtual void Add(Stream file, string name) => streams[name] = file;

    /// <summary>Add file</summary>
    /// <param name="file">File data</param>
    /// <param name="name">File name</param>
    public virtual void Add(byte[] file, string name) => bArrays[name] = file;

    /// <summary>Add file</summary>
    /// <param name="files">File name and stream</param>
    public virtual void Add(IDictionary<string, Stream> files) => streams.AddUnique(files);

    /// <summary>Add file</summary>
    /// <param name="files">File name data</param>
    public virtual void Add(IDictionary<string, byte[]> files) => bArrays.AddUnique(files);

    /// <summary>Add file</summary>
    /// <param name="files">File stream and name</param>
    public virtual void Add(params string[] files) => fileNames.AddUnique(files);

    /// <summary>Create archive</summary>
    /// <param name="file">Archive path/name</param>
    /// <param name="archiveType">Archive type</param>
    public virtual void Create(string file, ArchiveTypes archiveType = ArchiveTypes.Auto) {
        using var zip = File.OpenWrite(file);
        Create(zip, DetectArchiveType(file));
    }

    /// <summary>Create archive</summary>
    /// <param name="file">Archive path/name</param>
    /// <param name="archiveType">Archive type</param>
    public virtual void Create(Stream file, ArchiveTypes archiveType) {
        ArchiveType = archiveType;
        MatchArchiveType(ArchiveType, out var sArchiveType, out var compressionType);
        using var writer = WriterFactory.Open(file, sArchiveType, new WriterOptions(compressionType));
        foreach (var f in fileNames) writer.Write(Path.GetFileName(f), f);
        foreach (var s in streams) writer.Write(s.Key, s.Value, DateTime.Now);
        foreach (var b in bArrays) {
            using var memStream = new MemoryStream(b.Value);
            writer.Write(b.Key, memStream, DateTime.Now);
        }
    }

    /// <summary>Extract files</summary>
    /// <param name="archive">Archive file</param>
    /// <param name="extractDir">Extract to</param>
    /// <param name="overwrite">Overwrite existing</param>
    /// <param name="extractFullPath">Extract Path Info</param>
    /// <returns>extracted file names</returns>
    public static string[] Extract(Stream archive, string extractDir, bool overwrite = true, bool extractFullPath = true) {
        var rval = new List<string>();
        using (var rd = ReaderFactory.Open(archive)) {
            var opt = new ExtractionOptions {
                Overwrite = overwrite,
                ExtractFullPath = extractFullPath
            };
            while (rd.MoveToNextEntry()) {
                if (!rd.Entry.IsDirectory) {
                    //rd.WriteAllToDirectory(extractDir, ExtractOptions.ExtractFullPath);
                    rd.WriteEntryToDirectory(extractDir, opt);
                    rval.Add(rd.Entry.Key);
                }
            }
        }
        return rval.ToArray();
    }

    /// <summary>Extract files</summary>
    /// <param name="archiveFile">Archive file</param>
    /// <param name="extractDir">Extract to</param>
    /// <param name="overwrite">Overwrite existing</param>
    /// <param name="extractFullPath">Extract Path Info</param>
    /// <returns>extracted file names</returns>
    public static string[] Extract(string archiveFile, string extractDir, bool overwrite = true, bool extractFullPath = true) {
        using Stream stream = File.OpenRead(archiveFile);
        return Extract(stream, extractDir, overwrite, extractFullPath);
    }

    private static ArchiveTypes DetectArchiveType(string fileName) {
        if (fileName.EndsWith(".zip", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.Zip;
        if (fileName.EndsWith(".bz2", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.Bzip2;
        //if (fileName.EndsWith(".tgz", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.TarGz;
        //if (fileName.EndsWith(".tar.gz", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.TarGz;
        if (fileName.EndsWith(".7z", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.SevenZip;
        if (fileName.EndsWith(".rar", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.Rar;
        if (fileName.EndsWith(".gz", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.Gzip;
        if (fileName.EndsWith(".tar", StringComparison.InvariantCultureIgnoreCase)) return ArchiveTypes.Tar;
        return ArchiveTypes.Zip;
    }
    private static void MatchArchiveType(ArchiveTypes aType, out ArchiveType archiveType, out CompressionType compressionType) {
        switch (aType) {
            case ArchiveTypes.Gzip:
                archiveType = SharpCompress.Common.ArchiveType.GZip;
                compressionType = CompressionType.GZip;
                break;
            case ArchiveTypes.Bzip2:
                archiveType = SharpCompress.Common.ArchiveType.GZip;
                compressionType = CompressionType.BZip2;
                break;
            case ArchiveTypes.Tar:
                archiveType = SharpCompress.Common.ArchiveType.Tar;
                compressionType = CompressionType.None;
                break;
            case ArchiveTypes.SevenZip:
                archiveType = SharpCompress.Common.ArchiveType.SevenZip;
                compressionType = CompressionType.LZMA;
                break;
            case ArchiveTypes.Rar:
                archiveType = SharpCompress.Common.ArchiveType.Rar;
                compressionType = CompressionType.Rar;
                break;
            default:
                archiveType = SharpCompress.Common.ArchiveType.Zip;
                compressionType = CompressionType.Deflate;
                break;
        }
    }
}