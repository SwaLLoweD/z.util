﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharpCompress.Compressors;
using SharpCompress.Compressors.Deflate;
namespace Z.Extensions;

/// <summary>Zip extensions</summary>
public static partial class ExtZip
{
    /// <summary>Compress byte Array in ZIP format</summary>
    /// <param name="byteArray"></param>
    /// <param name="compressionLevel">1-9 (0=fastest, 9=maxCompresion)</param>
    /// <returns>Compressed byte Array</returns>
    public static byte[] Compress(this IEnumerable<byte> byteArray, byte compressionLevel = 6) {
        if (byteArray?.Any() != true) return null;
        var b = byteArray.ToArray();
        byte[] rval;
        using (var ms = new MemoryStream()) {
            //GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true);  //Ms way
            var zip = new GZipStream(ms, CompressionMode.Compress, (CompressionLevel)compressionLevel); //Ionic/SharpCompress way
            zip.Write(b, 0, b.Length);
            zip.Close();
            ms.Position = 0;

            //MemoryStream outStream = new MemoryStream();

            var compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            var gzBuffer = new byte[compressed.Length + 4];
            Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(b.Length), 0, gzBuffer, 0, 4);
            rval = gzBuffer;
        }
        return rval;
    }
    /// <summary>Uncompress byte Array in ZIP format</summary>
    /// <param name="byteArray"></param>
    /// <returns>Uncompressed (original) byte Array</returns>
    public static byte[] Decompress(this IEnumerable<byte> byteArray) {
        if (byteArray?.Any() != true) return null;
        var b = byteArray.ToArray();
        byte[] rval;
        using (var ms = new MemoryStream()) {
            var msgLength = BitConverter.ToInt32(b, 0);
            ms.Write(b, 4, b.Length - 4);

            var buffer = new byte[msgLength];

            ms.Position = 0;
            //GZipStream zip = new GZipStream(ms, CompressionMode.Decompress); //Ms way
            var zip = new GZipStream(ms, CompressionMode.Decompress); //Ionic/SharpCompress way
            zip.Read(buffer, 0, buffer.Length);
            rval = buffer;
        }
        return rval;
    }
}