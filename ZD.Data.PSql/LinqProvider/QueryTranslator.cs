﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Z.Collections.Generic;
using Z.Data.PSql.Common;
using Z.Data.PSql.Mapping;
using Z.Extensions;
using Z.Util;

namespace Z.Data.PSql.Linq;

/// <summary>DbType and Value</summary>
internal class DbTypeValue

{/// <summary>Value</summary>
    public object Value { get; set; }
    /// <summary>Database Type</summary>
    public DbType DbType { get; set; }

    /// <summary>Constructor</summary>
    public DbTypeValue(object value, DbType dbType) {
        Value = value;
        DbType = dbType;
    }
}

/// <summary>PSql Linq Query Visitor</summary>
public class QueryTranslator<T> : ExpressionVisitor
{
    //private readonly Type originalType = typeof(T);
    private readonly StringBuilder sb = new();

    ///<summary>Translator</summary>
    public QueryTranslator(Repo repository) {
        repo = repository;
    }

    #region Query information
    /// <summary>Gets last translated query.</summary>
    /// <value>The query.</value>
    public string Query { get; private set; }
    private int queryParamCount = 0;
    private readonly IDictionary<string, DbTypeValue> queryParams = new Dictionary<string, DbTypeValue>();
    private readonly Repo repo;
    #endregion Query information

    //private readonly HashSet<PropertyMap> properties = new();

    #region BuildProjection (Disabled)
    // /// <summary>
    // /// Helper method for projection clauses (Select).
    // /// </summary>
    // /// <param name="p">Lambda expression representing the projection.</param>
    // private void BuildProjection(LambdaExpression p)
    // {
    //     // Store projection information including the compiled lambda for subsequent execution
    //     // and a minimal set of properties to be retrieved (improves efficiency of queries).
    //     p.Compile();

    //     // Original type is kept for reflection during querying.
    //     originalType = p.Parameters[0].Type;

    //     // Support for (anonymous) type initialization based on "member init expressions".
    //     MemberInitExpression mi = p.Body as MemberInitExpression;
    //     if (mi != null)
    //         foreach (MemberAssignment b in mi.Bindings)
    //             FindProperties(b.Expression);
    //     // Support for identity projections (e.g. user => user), getting all properties back.
    //     else
    //         properties.Add(repo.Config.GetMappedProperties<T>());
    // }

    // /// <summary>
    // /// Recursive helper method to finds all required properties for projection.
    // /// </summary>
    // /// <param name="e">Expression to detect property uses for.</param>
    // private void FindProperties(Expression e)
    // {
    //     //
    //     // Record member accesses to properties or fields from the entity.
    //     //
    //     if (e.NodeType == ExpressionType.MemberAccess) {
    //         MemberExpression me = e as MemberExpression;
    //         if (me.Member.DeclaringType == originalType) {
    //             var propInfo = repo.Config.GetMappedProperty(originalType, me.Member as PropertyInfo);
    //             if (propInfo != null) properties.AddUnique(propInfo);
    //             //properties.Add(me.Member.Name);
    //         }
    //     }
    //     else {
    //         if (e is BinaryExpression) {
    //             BinaryExpression b = e as BinaryExpression;
    //             FindProperties(b.Left);
    //             FindProperties(b.Right);
    //         }
    //         else if (e is UnaryExpression) {
    //             UnaryExpression u = e as UnaryExpression;
    //             FindProperties(u.Operand);
    //         }
    //         else if (e is ConditionalExpression) {
    //             ConditionalExpression c = e as ConditionalExpression;
    //             FindProperties(c.IfFalse);
    //             FindProperties(c.IfTrue);
    //             FindProperties(c.Test);
    //         }
    //         else if (e is InvocationExpression) {
    //             InvocationExpression i = e as InvocationExpression;
    //             FindProperties(i.Expression);
    //             foreach (Expression ex in i.Arguments)
    //                 FindProperties(ex);
    //         }
    //         else if (e is LambdaExpression) {
    //             LambdaExpression l = e as LambdaExpression;
    //             FindProperties(l.Body);
    //             foreach (Expression ex in l.Parameters)
    //                 FindProperties(ex);
    //         }
    //         /*else if (e is ListInitExpression)
    //         {
    //             ListInitExpression li = e as ListInitExpression;
    //             FindProperties(li.NewExpression);
    //             foreach (Expression ex in li.Expressions)
    //                 FindProperties(ex);
    //         }*/
    //         else if (e is MemberInitExpression) {
    //             MemberInitExpression mi = e as MemberInitExpression;
    //             FindProperties(mi.NewExpression);
    //             foreach (MemberAssignment b in mi.Bindings)
    //                 FindProperties(b.Expression);
    //         }
    //         else if (e is MethodCallExpression) {
    //             MethodCallExpression mc = e as MethodCallExpression;
    //             FindProperties(mc.Object);
    //             foreach (Expression ex in mc.Arguments)
    //                 FindProperties(ex);
    //         }
    //         else if (e is NewExpression) {
    //             NewExpression n = e as NewExpression;
    //             foreach (Expression ex in n.Arguments)
    //                 FindProperties(ex);
    //         }
    //         else if (e is NewArrayExpression) {
    //             NewArrayExpression na = e as NewArrayExpression;
    //             foreach (Expression ex in na.Expressions)
    //                 FindProperties(ex);
    //         }
    //         else if (e is TypeBinaryExpression) {
    //             TypeBinaryExpression tb = e as TypeBinaryExpression;
    //             FindProperties(tb.Expression);
    //         }
    //     }
    // }
    #endregion BuildProjection

    /// <summary>Translate to SQL</summary>
    internal string Translate(Expression node, out IDictionary<string, DbTypeValue> commandParams) {
        Visit(node);
        commandParams = queryParams;
        return GetTSql();
    }
    /// <inheritdoc/>
    public override Expression Visit(Expression node) {
        return base.Visit(node);
    }
    // /// <inheritdoc/>
    // protected override Expression VisitConstant(ConstantExpression c) {
    //     if (c.Value is IQueryable q) {
    //         // assume constant nodes w/ IQueryables are table references
    //         //sb.Append("SELECT * FROM ");
    //         sb.Append(repo.Config.GetMappedTableName<T>());
    //     } else if (c.Value == null) {
    //         sb.Append("NULL");
    //     } else {
    //         sb.Append(GetConditionParam(c.Value, repo.Config.GetDbTypeForType(c.Value.GetType())));
    //         //switch (Type.GetTypeCode(c.Value.GetType())) {
    //         //    case TypeCode.Boolean:
    //         //        sb.Append(getConditionParam(((bool)c.Value) ? "1" : "0"));
    //         //        break;

    //         //    default:
    //         //        sb.Append(getConditionParam(Obj2Db.Convert(c.Type, c.Value)));
    //         //        break;
    //         //}
    //     }
    //     return c;
    // }
    // Support for boolean operators & and |. Support for "raw" conditions (like equality).
    /// <inheritdoc/>
    protected override Expression VisitBinary(BinaryExpression b) {
        sb.Append('(');
        this.Visit(b.Left);
        switch (b.NodeType) {
            case ExpressionType.AndAlso:
            case ExpressionType.And:
                sb.Append(" AND ");
                break;

            case ExpressionType.OrElse:
            case ExpressionType.Or:
                sb.Append(" OR ");
                break;

            case ExpressionType.Equal:
                sb.Append(" = ");
                break;

            case ExpressionType.NotEqual:
                sb.Append(" <> ");
                break;

            case ExpressionType.LessThan:
                sb.Append(" < ");
                break;

            case ExpressionType.LessThanOrEqual:
                sb.Append(" <= ");
                break;

            case ExpressionType.GreaterThan:
                sb.Append(" > ");
                break;

            case ExpressionType.GreaterThanOrEqual:
                sb.Append(" >= ");
                break;

            default:
                throw new NotSupportedException(string.Format("The binary operator '{0}' is not supported", b.NodeType));
        }
        this.Visit(b.Right);
        sb.Append(')');
        return b;
    }
    /// <inheritdoc/>
    protected override Expression VisitUnary(UnaryExpression u) {
        switch (u.NodeType) {
            case ExpressionType.Not:
                sb.Append(" NOT ");
                this.Visit(u.Operand);
                break;

            default:
                throw new NotSupportedException(string.Format("The unary operator '{0}' is not supported", u.NodeType));
        }
        return u;
    }
    // Support for string operations.
    /// <inheritdoc/>
    protected override Expression VisitMethodCall(MethodCallExpression m) {
        MemberExpression o = (m.Object as MemberExpression);
        if (m.Method.DeclaringType == typeof(Queryable) && m.Method.Name == "Where") {
            sb.Append("SELECT * FROM ");
            this.Visit(m.Arguments[0]);
            sb.Append(" WHERE ");
            LambdaExpression lambda = (LambdaExpression)StripQuotes(m.Arguments[1]);
            this.Visit(lambda.Body);
            return m;
        } else if (m.Method.DeclaringType == typeof(string)) {
            switch (m.Method.Name) {
                case "Contains": {
                        ConstantExpression c = m.Arguments[0] as ConstantExpression;
                        sb.AppendFormat("{0}=*{1}*", GetFieldName(o.Member), c.Value);
                        break;
                    }
                case "StartsWith": {
                        ConstantExpression c = m.Arguments[0] as ConstantExpression;
                        sb.AppendFormat("{0}={1}*", GetFieldName(o.Member), c.Value);
                        break;
                    }
                case "EndsWith": {
                        ConstantExpression c = m.Arguments[0] as ConstantExpression;
                        sb.AppendFormat("{0}=*{1}", GetFieldName(o.Member), c.Value);
                        break;
                    }
                default:
                    throw new NotSupportedException("Unsupported string filtering query expression detected. Cannot translate to LDAP equivalent.");
            }
        } else {
            throw new NotSupportedException(string.Format("The method '{0}' is not supported", m.Method.Name));
        }

        return base.VisitMethodCall(m);
    }
    /// <inheritdoc/>
    protected override Expression VisitMember(MemberExpression m) {
        if (m.Expression?.NodeType == ExpressionType.Parameter) {
            var pi = GetFieldName(m.Member);
            if (pi.Is().Empty)
                throw new NotSupportedException(string.Format("The member '{0}' is not found", m.Member.Name));
            sb.Append(pi);
            return m;
        } else if (m.Expression?.NodeType == ExpressionType.Constant) {
            var c = (m.Expression as ConstantExpression);
            var cVal = m.Member.GetValue(c.Value);
            if (cVal == null) {
                sb.Append("NULL");
            } else {
                sb.Append(GetConditionParam(cVal, repo.Config.GetDbTypeForType(cVal.GetType())));
            }
            return m;
        } else {
            throw new NotSupportedException(string.Format("The member '{0}' is not supported", m.Member.Name));
        }
        //return m;
    }
    /// <inheritdoc/>
    protected override Expression VisitLambda<Tin>(Expression<Tin> node) {
        return base.VisitLambda(node);
    }

    #region ProcessExpression
    /// <inheritdoc/>
    public string GetTSql() {
        return Query = sb.ToString();
    }
    private static Expression StripQuotes(Expression e) {
        while (e.NodeType == ExpressionType.Quote) {
            e = ((UnaryExpression)e).Operand;
        }
        return e;
    }

    // /// <summary>Helper expression to translate conditions to SQL filters.</summary>
    // /// <param name="e">Conditional expression to be translated to an SQL filter.</param>
    // /// <returns>String representing the condition in SQL.</returns>
    // private string GetCondition(BinaryExpression e) {
    //     PropertyMap prop;
    //     string fieldName;
    //     object valDb;
    //     object val;

    //     bool neg;

    //     // Find the order of the operands in the binary expression. At least one should refer to the entity type.
    //     if (e.Left is MemberExpression expression1 && expression1.Member is PropertyInfo && originalType.Is().CastableTo(expression1.Member.DeclaringType)) {
    //         neg = false;
    //         prop = repo.Config.GetMappedProperty<T>(expression1.Member as PropertyInfo);
    //         val = Expression.Lambda(e.Left).Compile().DynamicInvoke();
    //     } else if (e.Right is MemberExpression expression && expression.Member is PropertyInfo && originalType.Is().CastableTo(expression.Member.DeclaringType)) {
    //         neg = true;
    //         prop = repo.Config.GetMappedProperty<T>(expression.Member as PropertyInfo);
    //         val = Expression.Lambda(e.Left).Compile().DynamicInvoke();
    //     } else {
    //         throw new NotSupportedException("A filtering expression should contain an entity member selection expression.");
    //     }

    //     if (prop == null) throw new InvalidExpressionException("Related property mapping and field name could not be found");

    //     fieldName = prop.Column;
    //     valDb = prop.GetDbValue(val);

    //     // Determine the operator and swap the operandi if necessary (LDAP requires a field=value order).
    //     return e.NodeType switch {
    //         ExpressionType.Equal => valDb == null ? "${fieldName} IS NULL" : $"{fieldName} = {GetConditionParam(valDb, prop.DbType)}",
    //         ExpressionType.NotEqual => valDb == null ? $"{fieldName} IS NOT NULL" : $"{fieldName} <> {GetConditionParam(valDb, prop.DbType)}",
    //         ExpressionType.GreaterThanOrEqual => neg ? $"{fieldName} < {GetConditionParam(valDb, prop.DbType)}" : $"{fieldName} >= {GetConditionParam(valDb, prop.DbType)}",
    //         ExpressionType.GreaterThan => neg ? $"{fieldName} <= {GetConditionParam(valDb, prop.DbType)}" : $"{fieldName} > {GetConditionParam(valDb, prop.DbType)})",
    //         ExpressionType.LessThanOrEqual => neg ? $"{fieldName} > {GetConditionParam(valDb, prop.DbType)}" : $"{fieldName} <= {GetConditionParam(valDb, prop.DbType)}",
    //         ExpressionType.LessThan => neg ? $"{fieldName} >= {GetConditionParam(valDb, prop.DbType)}" : $"{fieldName} < {GetConditionParam(valDb, prop.DbType)}",
    //         _ => throw new NotSupportedException("Unsupported filtering operator detected: " + e.NodeType),
    //     };
    // }
    private string GetFieldName(MemberInfo member) {
        var pi = member as PropertyInfo;
        return repo.Config.GetMappedField<T>(pi, true);
    }
    private string GetConditionParam(object val, DbType dbType) {
        var paramName = $"@V{queryParamCount++}";
        queryParams[paramName] = new DbTypeValue(val, dbType);
        return paramName;
    }
    #endregion ProcessExpression
}
