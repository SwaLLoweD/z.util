﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Z.Collections.Generic;
using Z.Extensions;
using Z.Util;

namespace Z.Data.PSql.Linq;

/// <summary>Linq Queryable provider</summary>
public class PSqlQueryable<T> : IQueryable<T>, IQueryProvider
{
    private Expression _expression = null;
    /// <summary>PSqlQueryable</summary>
    public PSqlQueryable(Repo repository) {
        repo = repository;
    }

    #region IQueryable Members
    Type IQueryable.ElementType {
        get { return typeof(T); }
    }
    Expression IQueryable.Expression {
        get { return Expression.Constant(this); }
    }
    IQueryProvider IQueryable.Provider {
        get { return this; }
    }
    #endregion IQueryable Members

    #region IEnumerable<T> Members
    IEnumerator IEnumerable.GetEnumerator() {
        return (IEnumerator<T>)(this as IQueryable).GetEnumerator();
    }
    IEnumerator<T> IEnumerable<T>.GetEnumerator() {
        return (this as IQueryable).Provider.Execute<IEnumerator<T>>(_expression);
    }
    #endregion IEnumerable<T> Members

    #region Query information
    private readonly Repo repo;
    #endregion Query information

    #region IQueryProvider Members
    /// <summary>Create Query</summary>
    public IQueryable<TElement> CreateQuery<TElement>(Expression expression) {
        //if (typeof(TElement) != typeof(IPoco)) throw new Exception("Only Poco Entities are supported");
        this._expression = expression;
        return (IQueryable<TElement>)this;
    }
    /// <summary>Create Query</summary>
    public IQueryable CreateQuery(Expression expression) {
        return CreateQuery<T>(expression);
    }
    /// <summary>Execute</summary>
    public TResult Execute<TResult>(Expression expression) {
        //MethodCallExpression methodcall = _expression as MethodCallExpression;

        var sb = new StringBuilder();
        var visitor = new QueryTranslator<T>(repo);
        sb.Append(visitor.Translate(_expression, out IDictionary<string, DbTypeValue> commandParams));
        //foreach (var param in methodcall.Arguments)
        //sb.Append(new QueryVisitor<T>().Visit());
        //
        using var cmd = repo.CreateCommand();
        cmd.CommandText = sb.ToString();
        foreach (var p in commandParams) {
            if (repo.Config.ConnectionType == DBConTypes.MySql) {
                cmd.Parameters.GetType().InvokeMember("AddWithValue",
                    System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public,
                    null, cmd.Parameters, new object[] { p.Key, p.Value.Value });
            } else {
                cmd.ParameterAdd(p.Key, p.Value.Value, p.Value.DbType);
            }
        }
        if (typeof(TResult).Is().HasInterface<ICollection>()) return (TResult)repo.Get<T>().All(cmd);
        var t = repo.Get<T>().All(cmd);
        return (TResult)(object)t.FirstOrDefault();
    }
    /// <summary>Execute</summary>
    public object Execute(Expression expression) {
        return (this as IQueryProvider).Execute<IEnumerator<T>>(expression);
    }
    #endregion IQueryProvider Members
}