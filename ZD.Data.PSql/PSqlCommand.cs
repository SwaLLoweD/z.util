/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;

namespace Z.Data.PSql
{
    /// <summary>PSql Repository command object</summary>
    public class PSqlCommand : IDbCommand
    {
        private bool _disposed = false;
        /// <summary>Keep the dbconnection open after command is disposed</summary>
        public bool KeepConnectionOpen { get; set; }
        /// <summary>PSql Repository this command is derived from</summary>
        public Repo Repo { get; protected set; }
        /// <summary>Encapsulated Inner IDbCommand object</summary>
        public IDbCommand InnerDbCommand { get; protected set; }
        /// <summary>Constructor</summary>
        public PSqlCommand(Repo repo) {
            Repo = repo;
            InnerDbCommand = Repo.CreateOrGetDbConnection().CreateCommand();
            KeepConnectionOpen = false;
        }

        /// <inheritdoc/>
        public string CommandText { get { return InnerDbCommand.CommandText; } set { InnerDbCommand.CommandText = value; } }
        /// <inheritdoc/>
        public int CommandTimeout { get { return InnerDbCommand.CommandTimeout; } set { InnerDbCommand.CommandTimeout = value; } }
        /// <inheritdoc/>
        public CommandType CommandType { get { return InnerDbCommand.CommandType; } set { InnerDbCommand.CommandType = value; } }
        /// <inheritdoc/>
        public IDbConnection Connection { get { return InnerDbCommand.Connection; } set { InnerDbCommand.Connection = value; } }
        /// <inheritdoc/>
        public IDataParameterCollection Parameters { get { return InnerDbCommand.Parameters; } }
        /// <inheritdoc/>
        public IDbTransaction Transaction { get { return InnerDbCommand.Transaction; } set { InnerDbCommand.Transaction = value; } }
        /// <inheritdoc/>
        public UpdateRowSource UpdatedRowSource { get { return InnerDbCommand.UpdatedRowSource; } set { InnerDbCommand.UpdatedRowSource = value; } }

        /// <inheritdoc/>
        public void Cancel() { }
        /// <inheritdoc/>
        public IDbDataParameter CreateParameter() { return InnerDbCommand.CreateParameter(); }
        /// <inheritdoc/>
        public int ExecuteNonQuery() { return InnerDbCommand.ExecuteNonQuery(); }
        /// <inheritdoc/>
        public IDataReader ExecuteReader() { return InnerDbCommand.ExecuteReader(); }
        /// <inheritdoc/>
        public IDataReader ExecuteReader(CommandBehavior behavior) { return InnerDbCommand.ExecuteReader(behavior); }
        /// <inheritdoc/>
        public object ExecuteScalar() { return InnerDbCommand.ExecuteScalar(); }
        /// <inheritdoc/>
        public void Prepare() { InnerDbCommand.Prepare(); }

        #region Dispose
        /// <inheritdoc/>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <inheritdoc/>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed) {
                if (disposing) {
                    if (InnerDbCommand!= null) {
                        if (!KeepConnectionOpen) Repo.ReleaseConnection(InnerDbCommand.Connection);
                        InnerDbCommand.Dispose();
                    }
                    //DetachEvents();
                    //SessionFlush();
                    //SessionClose();
                }
                // Call the appropriate methods to clean up
                // unmanaged resources here.
                _disposed = true;
            }
        }
        #endregion Dispose
    }
}