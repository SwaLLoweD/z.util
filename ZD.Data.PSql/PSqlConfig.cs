﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using Z.Collections.Generic;
using Z.Data.PSql.Mapping;
using Z.Extensions;
using Z.Util;

namespace Z.Data.PSql;

/// <summary>NHibernate configuration</summary>
public class PSqlConfig {
    #region Properties
    /// <summary>Gets or sets the configuration name.</summary>
    public string Name { get; private set; }
    /*
    /// <summary>Collection batch size</summary>
    public int ADONetBatchSize { get; set; }
    /// <summary>SQL Connection String</summary>
    public string ConnectionStringName { get; set; }*/

    /// <summary>Default SQL Command Timeout (in seconds)</summary>
    public int CommandTimeout { get; set; }

    /// <summary>Default Timeout for non-persistent connections (in seconds)</summary>
    public int ConnectionTimeout { get; set; }

    /// <summary>Keep connections open (Should not be used with pooling)</summary>
    public bool PersistentConnections { get; set; }

    /// <summary>ConnectionString for pooled connections</summary>
    internal string ConnectionString { get; set; }

    private IDbConnection connection;
    /// <summary>Example DbConnection</summary>
    public IDbConnection Connection {
        get { return connection; }
        set {
            connection = value;
            if (connection != null) ConnectionString = connection.ConnectionString;
            DetectDBConType();
            SetDbTypeMap();
        }
    }
    /// <summary>Quote Style used by Db</summary>
    public string FieldQuote { get; set; }
    /// <summary>Db Connection Type</summary>
    public DBConTypes ConnectionType { get; private set; }
    private static readonly IDictionary<string, PSqlConfig> configStore = new Dictionary<string, PSqlConfig>();
    private IDictionary<Type, DbType> typeMap;

    /*
    /// <summary>Db Connection Provider</summary>
    public string ConnectionProvider { get; set; }
    /// <summary>Dialect</summary>
    public string DbDialect { get; set; }
    /// <summary>Db Driver</summary>
    public string DriverClass { get; set; }
    /// <summary>Show SQL output in console</summary>
    public bool ShowSql { get; set; }*/
    /// <summary>Update Schema</summary>
    public bool ExportSchema { get; set; }
    /*
    /// <summary>Enable NHibernate events</summary>
    public bool EnableListeners { get; set; }
    /// <summary>Apply Default conventions</summary>
    public bool ApplyDefaultConventions { get; set; } */
    /// <summary>Maps of entities</summary>
    public IDictionary<Type, IClassMap> EntityMaps { get; set; }
    /*
    /// <summary>Default String Byte Field Size</summary>
    public int DefaultFieldSize { get; set; }*/

    /// <summary>Current Configuration doing the mapping</summary>
    [ThreadStatic]
    internal static PSqlConfig MappingConfig = null;
    #endregion

    #region Constructors
    /// <summary>Creates a persistent PSQL repo using a single connection</summary>
    public PSqlConfig(IDbConnection defaultConnection, string configName = null) {
        Init(defaultConnection, configName);
    }

    /// <summary>Constructor</summary>
    public PSqlConfig() { LoadFromConfig(); }
    ///// <summary></summary>
    //public PSqlConfig(string connStrName = "ApplicationServices", string configName = null)
    //{
    //    IDbConnection conn = null;
    //    if (ConfigurationManager.ConnectionStrings.Count > 0) {
    //        var css = ConfigurationManager.ConnectionStrings[connStrName] ?? ConfigurationManager.ConnectionStrings[0];
    //        conn = css.CreateConnection();
    //    }
    //    init(conn, configName);
    //}

    private void Init(IDbConnection defaultConnection, string configName = null) {
        EntityMaps = new Dictionary<Type, IClassMap>();
        Name = configName ?? "default";
        LoadFromConfig();
        ConfigAdd(Name, this);
        Connection = defaultConnection;
    }
    /// <summary>Creates a pooling PSQL repo using creating connections via the string provided</summary>
    public void SetConnection<T>(string connectionString, string configName = null) where T : IDbConnection {
        var con = typeof(T).CreateInstance<IDbConnection>();
        Init(con, configName);
        ConnectionString = connectionString;
    }
    #endregion

    #region Helpers
    private void DetectDBConType() {
        if (Connection == null) {
            ConnectionType = DBConTypes.SqlServer;
            return;
        }

        string dbtype = Connection.GetType().FullName;
        if (dbtype.StartsWith("MySql")) { ConnectionType = DBConTypes.MySql; FieldQuote = "`{0}`"; } else if (dbtype.StartsWith("SqlCe")) {
            ConnectionType = DBConTypes.SqlServerCE;
        } else if (dbtype.StartsWith("Npgsql")) {
            ConnectionType = DBConTypes.PostgreSQL;
        } else if (dbtype.StartsWith("Oracle")) {
            ConnectionType = DBConTypes.Oracle;
        } else if (dbtype.StartsWith("SQLite")) {
            ConnectionType = DBConTypes.SQLite;
        } else if (dbtype.StartsWith("System.Data.SqlClient.")) { ConnectionType = DBConTypes.SqlServer; FieldQuote = "[{0}]"; }
    }

    private void SetDbTypeMap() {
        typeMap = new Dictionary<Type, DbType> {
            [typeof(byte)] = DbType.Byte,
            [typeof(sbyte)] = DbType.SByte,
            [typeof(short)] = DbType.Int16,
            [typeof(ushort)] = DbType.UInt16,
            [typeof(int)] = DbType.Int32,
            [typeof(uint)] = DbType.UInt32,
            [typeof(long)] = DbType.Int64,
            [typeof(ulong)] = DbType.UInt64,
            [typeof(float)] = DbType.Single,
            [typeof(double)] = DbType.Double,
            [typeof(decimal)] = DbType.Decimal,
            [typeof(bool)] = DbType.Boolean,
            [typeof(string)] = DbType.String,
            [typeof(char)] = DbType.StringFixedLength,
            [typeof(Guid)] = DbType.Guid,
            [typeof(DateTime)] = DbType.DateTime,
            [typeof(DateTimeOffset)] = DbType.DateTimeOffset,
            [typeof(byte[])] = DbType.Binary,
            [typeof(byte?)] = DbType.Byte,
            [typeof(sbyte?)] = DbType.SByte,
            [typeof(short?)] = DbType.Int16,
            [typeof(ushort?)] = DbType.UInt16,
            [typeof(int?)] = DbType.Int32,
            [typeof(uint?)] = DbType.UInt32,
            [typeof(long?)] = DbType.Int64,
            [typeof(ulong?)] = DbType.UInt64,
            [typeof(float?)] = DbType.Single,
            [typeof(double?)] = DbType.Double,
            [typeof(decimal?)] = DbType.Decimal,
            [typeof(bool?)] = DbType.Boolean,
            [typeof(char?)] = DbType.StringFixedLength,
            [typeof(Guid?)] = DbType.Guid,
            [typeof(DateTime?)] = DbType.DateTime,
            [typeof(DateTimeOffset?)] = DbType.DateTimeOffset
        };
    }

    /// <summary>Find the associated DbType for given Type</summary>
    /// <param name="t">.Net type</param>
    /// <returns></returns>
    public DbType GetDbTypeForType(Type t) {
        if (t == null) return DbType.Object;
        if (typeMap.ContainsKey(t)) return typeMap[t];
        if (t.IsAssignableTo(typeof(long))) return DbType.Int64;
        if (t.IsAssignableTo(typeof(byte[]))) return DbType.Binary;
        if (t.IsAssignableTo(typeof(string))) return DbType.String;
        return DbType.Object;
    }
    #endregion

    #region Configuration
    /// <summary>Add new Configuration to ConfigStore.
    /// </summary>
    /// <param name="configName">Config name.</param>
    /// <param name="config">Config.</param>
    public static void ConfigAdd(string configName, PSqlConfig config) {
        configStore[configName] = config;
    }
    /// <summary>Gets the Config with specified name.</summary>
    /// <returns>The get.</returns>
    /// <param name="configName">Config name.</param>
    public static PSqlConfig ConfigGet(string configName = "default") {
        return configStore.TryGetValue(configName);
    }

    /// <summary>Load Values from Application config file</summary>
    public void LoadFromConfig() {
        FieldQuote = "\"{0}\"";
        ExportSchema = true; //ConfigurationManager.AppSettings["ExportSchema"].To().Type<bool>(true);
        PersistentConnections = false;
        CommandTimeout = 120; // ConfigurationManager.AppSettings["DbDefaultTimeout"].To().Type<int>(120);
        ConnectionTimeout = 30;
        //ADONetBatchSize = ConfigurationManager.AppSettings["DbBatchSize"].Convert<int>(10);
        //ConnectionStringName = ConfigurationManager.AppSettings["DbConnString"] ?? "ApplicationServices";
        //ConnectionProvider = ConfigurationManager.AppSettings["DbProvider"] ?? "NHibernate.Connection.DriverConnectionProvider";
        //DbDialect = ConfigurationManager.AppSettings["DbDialect"] ?? "NHibernate.Dialect.MsSql2008Dialect";
        //DriverClass = ConfigurationManager.AppSettings["DbDriverClass"] ?? "NHibernate.Driver.SqlClientDriver";
        //ShowSql = ConfigurationManager.AppSettings["DbShowSql"].Convert<bool>(false);
        //EnableListeners = ConfigurationManager.AppSettings["EnableListeners"].Convert<bool>(true);
        //ApplyDefaultConventions = ConfigurationManager.AppSettings["ApplyDefaultConventions"].Convert<bool>(true);
        //DefaultFieldSize = ConfigurationManager.AppSettings["DefaultFieldSize"].Convert<int>(200);
    }
    #endregion

    #region Mapping
    /// <summary>Load Entity and Validation maps from assembly</summary>
    public void LoadMaps(System.Reflection.Assembly asmb) {
        foreach (var typ in asmb.GetTypes()) {
            LoadEntityMap(typ);
        }

        //ValidationMaps.Add(valMaps);
    }
    /// <summary>Load Entity and Validation maps from Assembly</summary>
    public void LoadMaps<T>() {
        LoadMaps(typeof(T).Assembly);
    }
    /// <summary>Load Entity Map</summary>
    public void LoadEntityMap<T>() //where T :IConformistHoldersProvider
    {
        LoadEntityMap(typeof(T));
    }
    /// <summary>Load Entity Map</summary>
    public void LoadEntityMap(params Type[] types) //where T :IConformistHoldersProvider
    {
        MappingConfig = this;
        foreach (var mapType in types) {
            if (!mapType.IsAssignableTo<IClassMap>()) continue;
            if (Activator.CreateInstance(mapType) is not IClassMap inst) continue;
            EntityMaps[inst.Type] = inst;
        }
        MappingConfig = null;
    }

    /// <summary>Gets the mapped properties.</summary>
    /// <returns>The mapped properties.</returns>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public IEnumerable<PropertyMap> GetMappedProperties<T>() { return GetMappedProperties(typeof(T)); }
    /// <summary> Gets the mapped properties.</summary>
    /// <returns>The mapped properties.</returns>
    /// <param name="type">Type.</param>
    public IEnumerable<PropertyMap> GetMappedProperties(Type type) {
        var rval = new List<PropertyMap>();
        if (type == null) return rval;
        var typeMap = EntityMaps.TryGetValue(type);
        if (typeMap == null) {//AutoMap
            var idPropMaps = GetMappedIdProperties(type);
            var idPropMapLookup = idPropMaps.Select(x => x.Property).To().HashSet();
            rval.Add(idPropMaps);
            foreach (var prop in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)) {
                if (!idPropMapLookup.Contains(prop)) rval.Add(new PropertyMap(prop));
            }
            return rval;
        }
        return typeMap.Properties.Values;
    }
    /// <summary>Gets the mapped identifier fields.</summary>
    /// <returns>The mapped identifier fields.</returns>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public IEnumerable<PropertyIdMap> GetMappedIdProperties<T>() { return GetMappedIdProperties(typeof(T)); }
    /// <summary>Gets the mapped identifier fields.</summary>
    /// <returns>The mapped identifier fields.</returns>
    /// <param name="type">Type.</param>
    public IEnumerable<PropertyIdMap> GetMappedIdProperties(Type type) {
        var rval = new List<PropertyIdMap>();
        if (type == null) return rval;
        var typeMap = EntityMaps.TryGetValue(type);
        if (typeMap == null) { //AutoMap
            var idProps = type.GetProperties().Where(pi => pi.GetCustomAttributes(typeof(Id), false).Length > 0).ToArray();  //AttributeMap
            if (idProps?.Length > 0) {
                foreach (var idProp in idProps) {
                    if (idProp.GetCustomAttributes(typeof(Id), false).FirstOrDefault() is not Id attr) continue;
                    var idMap = new PropertyIdMap(idProp) {
                        Manual = attr.Manual
                    };
                    rval.Add(idMap);
                }
                return rval;
            }
            var idPi = type.GetProperty("id", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);  //Look for Id property
            if (idPi != null) rval.Add(new PropertyIdMap(idPi));
            return rval;
        } else {
            return typeMap.IdProperties.Values;
        }
        //return rval;
    }

    /// <summary>Gets the mapped property.</summary>
    /// <returns>The mapped property.</returns>
    /// <param name="fieldName">Field name.</param>
    public PropertyMap GetMappedProperty<T>(string fieldName) {
        return GetMappedProperty(typeof(T), fieldName);
    }
    /// <summary>Gets the mapped property.</summary>
    /// <returns>The mapped property.</returns>
    /// <param name="type">Type.</param>
    /// <param name="fieldName">Field name.</param>
    public PropertyMap GetMappedProperty(Type type, string fieldName) {
        PropertyMap pm = null;
        if (fieldName.Is().Empty || type == null) return null;

        var propName = fieldName.Replace(" ", "").Replace("-", "_");
        if (EntityMaps.ContainsKey(type)) {
            pm = EntityMaps[type].Properties.Where(x => x.Value != null && string.Equals(x.Value.Column, propName, StringComparison.InvariantCulture)).Select(x => x.Value).FirstOrDefault();
            if (pm == null && EntityMaps.ContainsKey(type)) {
                pm = EntityMaps[type].Properties.Where(x => x.Value?.IgnoreCase == true && string.Equals(x.Value.Column, propName, StringComparison.InvariantCultureIgnoreCase)).Select(x => x.Value).FirstOrDefault();
            }
        } else if (pm == null) {  //FallBack unmapped
            var pi = type.GetProperty(propName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                ?? type.GetProperty(propName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
            if (pi != null) pm = new PropertyMap(pi, null);
        }
        return pm;
    }
    /// <summary>Gets the mapped property.</summary>
    /// <returns>The mapped property.</returns>
    /// <param name="pi">Property.</param>
    public PropertyMap GetMappedProperty<T>(PropertyInfo pi) {
        return GetMappedProperty(typeof(T), pi);
    }
    /// <summary>Gets the mapped property.</summary>
    /// <returns>The mapped property.</returns>
    /// <param name="type">Type.</param>
    /// <param name="pi">Property.</param>
    public PropertyMap GetMappedProperty(Type type, PropertyInfo pi) {
        if (pi == null || type == null) return null;
        return EntityMaps.ContainsKey(type) && EntityMaps[type].Properties.ContainsKey(pi)
            ? EntityMaps[type].Properties[pi]
            : new PropertyMap(pi);
    }
    /// <summary>Gets the mapped field.</summary>
    /// <returns>The mapped field.</returns>
    /// <param name="pi">Pi.</param>
    /// <param name="useDbQuotes">Use Db Column Quotation for Fields</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public string GetMappedField<T>(PropertyInfo pi, bool useDbQuotes = false) { return GetMappedField(typeof(T), pi, useDbQuotes); }
    /// <summary>Gets the mapped field.</summary>
    /// <returns>The mapped field.</returns>
    /// <param name="type">Type.</param>
    /// <param name="pi">Pi.</param>
    /// <param name="useDbQuotes">Use Db Column Quotation for Fields</param>
    public string GetMappedField(Type type, PropertyInfo pi, bool useDbQuotes = false) {
        if (type == null || pi == null) return null;
        var typeMap = EntityMaps.TryGetValue(type);
        if (typeMap == null) return useDbQuotes ? string.Format(FieldQuote, pi.Name) : pi.Name;
        var propMap = typeMap.Properties[pi];
        if (propMap == null) return useDbQuotes ? string.Format(FieldQuote, pi.Name) : pi.Name;
        return useDbQuotes ? string.Format(FieldQuote, propMap.Column) : propMap.Column;
    }
    /// <summary>Gets the name of the mapped table.</summary>
    /// <returns>The mapped table name.</returns>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public string GetMappedTableName<T>() { return GetMappedTableName(typeof(T)); }
    /// <summary>Gets the name of the mapped table.</summary>
    /// <returns>The mapped table name.</returns>
    /// <param name="type">Type.</param>
    public string GetMappedTableName(Type type) {
        if (type == null) return null;
        var typeMap = EntityMaps.TryGetValue(type);
        if (typeMap == null) return type.Name;
        return typeMap.TableName;
    }
    #endregion

    #region Validation (old code)
    /*
    /// <summary>Load Validation Map</summary>
    public void LoadValidationMap<T>() //where T :IConstraintAggregator
    {
        LoadValidationMap(typeof(T));
    }
    /// <summary>Load Validation Map</summary>
    public void LoadValidationMap(params Type[] types) //where T :IConstraintAggregator
    {
        foreach (var t in types)
            ValidationMaps.Add(t);
    }*/

    /*
    /// <summary>Apply default Db Parameters</summary>
    internal void InjectDbStaticParams(NHibernate.Cfg.Configuration config)
    {
        config.SetProperty("adonet.batch_size", ADONetBatchSize.ToString());
        config.SetProperty("command_timeout", CommandTimeout.ToString());
        config.SetProperty("connection.connection_string_name", ConnectionStringName);
        config.SetProperty("connection.provider", ConnectionProvider);
        config.SetProperty("dialect", DbDialect);
        config.SetProperty("connection.driver_class", DriverClass);
        config.SetProperty("show_sql", ShowSql.ToString().ToLower());
        if (CacheProvider.Is().NotEmpty) {
            config.SetProperty("cache.provider_class", CacheProvider);
            config.SetProperty("cache.default_expiration", CacheDefaultExpiration.ToString());
            config.SetProperty("cache.use_second_level_cache", "true");
            config.SetProperty("cache.use_query_cache", UseQueryCache.ToString().ToLower());
        }
    //{"current_session_context_class","thread_static"},
    //{"proxyfactory.factory_class","NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle"},
    //{"adonet.batch_size", defaultBatchSize},
    //{"cache.provider_class", "NHibernate.Caches.SysCache.SysCacheProvider, NHibernate.Caches.SysCache"},
    //{"cache.use_second_level_cache", "true"},
    //{"command_timeout", System.Configuration.ConfigurationManager.AppSettings["DbDefaultTimeout"] ?? "120"},
    //{"connection.connection_string_name", System.Configuration.ConfigurationManager.AppSettings["DbConnString"]},
    //{"connection.provider", System.Configuration.ConfigurationManager.AppSettings["DbProvider"] ?? "NHibernate.Connection.DriverConnectionProvider"},
    //{"dialect", System.Configuration.ConfigurationManager.AppSettings["DbDialect"] ?? "NHibernate.Dialect.MsSql2008Dialect"},
    //{"connection.driver_class", System.Configuration.ConfigurationManager.AppSettings["DbDriverClass"] ?? "NHibernate.Driver.SqlClientDriver"},
    //{"show_sql", System.Configuration.ConfigurationManager.AppSettings["DbShowSql"] ?? "false" }
    }
     * */
    #endregion
}