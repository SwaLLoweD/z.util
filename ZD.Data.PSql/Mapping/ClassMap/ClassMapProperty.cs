﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;
using System.Reflection;

namespace Z.Data.PSql.Mapping;

/// <summary>Map property extended options</summary>
[Serializable]
public class ClassMapProperty
{
    private PropertyMap Property { get; }

    /// <summary>Initializes a new instance of the ClassMapProperty class.</summary>
    /// <param name="property">Property.</param>
    /// <param name="classMap">Class map.</param>
    public ClassMapProperty(PropertyInfo property = null, IClassMap classMap = null) {
        Property = new PropertyMap(property, classMap);
    }
    /// <summary>The property accepts null values or not.</summary>
    /// <param name="nullable">If set to <c>true</c> nullable.</param>
    public ClassMapProperty Nullable(bool nullable = true) {
        Property.Nullable = nullable;
        return this;
    }
    /// <summary>Length of the field.</summary>
    /// <param name="maxLength">Max length.</param>
    /// <param name="minLength">Minimum length.</param>
    public ClassMapProperty Length(uint maxLength = uint.MaxValue, uint minLength = 0) {
        Property.LengthMax = maxLength;
        Property.LengthMin = minLength;
        return this;
    }
    /// <summary>Value of the specified property should be unique or not.</summary>
    /// <param name="unique">If set to <c>true</c> unique.</param>
    public ClassMapProperty Unique(bool unique = true) {
        Property.Unique = unique;
        return this;
    }
    /// <summary>Persist using a custom column name for the property.</summary>
    /// <param name="columnName">Column name.</param>
    public ClassMapProperty Column(string columnName) {
        Property.Column = columnName;
        return this;
    }
    ///// <summary>Set DbType for this property. (If unset, default mappings are used.)</summary>
    ///// <param name="dbType">Database Type.</param>
    //public ClassMapProperty DbType(DbType dbType)
    //{
    //    Property.DbType = dbType;
    //    return this;
    //}
}