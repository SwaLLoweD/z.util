﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;
using System.Reflection;
using Z.Data.PSql.Common;
using Z.Extensions;

namespace Z.Data.PSql.Mapping;

/// <summary>Property mapping.</summary>
[Serializable]
public class PropertyMap
{
    private PropertyInfo property;
    /// <summary>Gets or sets the property.</summary>
    public PropertyInfo Property { get { return property; } set { property = value; SetDefaultsFor(value); } }
    /// <summary>Gets the class mapping.</summary>
    public IClassMap Class { get; protected set; }
    /// <summary>Gets or sets the column.</summary>
    public string Column { get; set; }
    /// <summary>Gets or sets a value indicating whether this Property is unique.</summary>
    /// <value><c>true</c> if unique; otherwise, <c>false</c>.</value>
    public bool Unique { get; set; }
    /// <summary>Gets or sets a value indicating whether this Property is nullable.</summary>
    /// <value><c>true</c> if nullable; otherwise, <c>false</c>.</value>
    public bool Nullable { get; set; }
    /// <summary>Gets or sets the length minimum.</summary>
    public uint LengthMin { get; set; }
    /// <summary>Gets or sets the length max.</summary>
    public uint LengthMax { get; set; }
    /// <summary>Gets or sets wheter column name matching should ignore case or not.</summary>
    public bool IgnoreCase { get; set; }
    /// <summary>Gets or sets matching DbType for the column.</summary>
    public DbType DbType { get; set; }

    /// <summary>Initializes a new instance of the <see cref="Z.Data.PSql.Mapping.PropertyMap"/> class.</summary>
    /// <param name="pi">Pi.</param>
    /// <param name="classMap">Class map.</param>
    public PropertyMap(PropertyInfo pi = null, IClassMap classMap = null) {
        IgnoreCase = true;
        Unique = false;
        Nullable = true;
        LengthMin = 0;
        LengthMax = uint.MaxValue;

        if (pi != null) {
            Property = pi;
            if (PSqlConfig.MappingConfig != null) DbType = PSqlConfig.MappingConfig.GetDbTypeForType(pi.PropertyType);
        }
        if (classMap != null) {
            Class = classMap;
            classMap.Properties[pi] = this;
        }
    }
    private void SetDefaultsFor(PropertyInfo pi) {
        //Property = pi;
        Column = pi.Name;
        Nullable = pi.PropertyType.Is().Nullable();
        //DbType = Obj2Db.FindDbType(pi.PropertyType);
    }
    /// <summary>Returns the db object for specified property</summary>
    public static object GetDbValue(object value) { return value; /*Obj2Db.Convert(this, value); */ }
    ///// <summary>Returns the db string for specified property</summary>
    //public string GetDbValueString(object value) { return Obj2Db.Convert(this, value); }
    /// <summary>Return the db string of the current value of the property for specified object instance</summary>
    public object GetDbValueForEntity(object entity) { return Property.GetValue(entity); }
}