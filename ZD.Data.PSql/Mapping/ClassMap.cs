﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Z.Collections.Generic;
using Z.Data.PSql.Mapping;
using Z.Util;

namespace Z.Data.PSql;

/// <summary>Database-Object Converters</summary>
[Serializable]
public class ClassMap<T> : IClassMap where T : class
{
    /// <summary>Mapping for the type.</summary>
    public Type Type { get; set; }
    /// <summary>Gets or sets the identifier properties.</summary>
    /// <value>The identifier properties.</value>
    public IDictionary<PropertyInfo, PropertyIdMap> IdProperties { get; set; }
    /// <summary>Gets or sets the properties.</summary>
    /// <value>The properties.</value>
    public IDictionary<PropertyInfo, PropertyMap> Properties { get; set; }
    /// <summary>Gets or sets the table.</summary>
    /// <value>The table.</value>
    public string TableName { get; set; }

    private bool _disposed = false;

    /// <summary>Initializes a new instance of Z.Data.PSql.ClassMap/> class.</summary>
    public ClassMap() {
        IdProperties = new Dictionary<PropertyInfo, PropertyIdMap>();
        Properties = new Dictionary<PropertyInfo, PropertyMap>();
        Type = typeof(T);
        TableName = Type.Name;
    }
    /// <summary>Map property as the Identifier for the entity</summary>
    /// <param name="property">Property.</param>
    public ClassMapPropertyId Id<U>(Expression<Func<T, U>> property) {
        //Map.IdProperties.Add(Fnc.LambdaProperty(property));
        var pi = Fnc.LambdaProperty(property);
        return new ClassMapPropertyId(pi, this);
    }
    /// <summary>Map the specified property.</summary>
    /// <param name="property">Property.</param>
    public ClassMapProperty Property<U>(Expression<Func<T, U>> property) {
        var pi = Fnc.LambdaProperty(property);
        return new ClassMapProperty(pi, this);
    }
    /// <summary>Map the specified collection property.</summary>
    /// <param name="property">Property.</param>
    public ClassMapPropertyCsv PropertyCsv<U>(Expression<Func<T, ICollection<U>>> property) {
        var pi = Fnc.LambdaProperty(property);
        return new ClassMapPropertyCsv(pi, this);
    }
    /// <summary>Maps the remaining unmapped properties.</summary>
    public void MapRemainingProperties() {
        var props = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        foreach (var pi in props) {
            if (Properties.ContainsKey(pi)) continue;
            _ = new ClassMapProperty(pi, this);
        }
    }
    /// <summary>Table name for persisting current entity</summary>
    /// <param name="tableName">Table name.</param>
    public void Table(string tableName) {
        TableName = tableName;
    }

    #region Dispose
    /// <inheritdoc/>
    public virtual void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    /// <inheritdoc/>
    protected virtual void Dispose(bool disposing) {
        if (!_disposed) {
            if (disposing) {
                if (IdProperties != null) {
                    IdProperties.Clear();
                    IdProperties = null;
                }
                if (Properties != null) {
                    Properties.Clear();
                    Properties = null;
                }
                //DetachEvents();
            }
            // Call the appropriate methods to clean up
            // unmanaged resources here.
            _disposed = true;
        }
    }
    #endregion Dispose
}