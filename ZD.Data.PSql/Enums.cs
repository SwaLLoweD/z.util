/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

namespace Z.Data.PSql
{
    #region ChangeTypes

    /// <summary>Change Types</summary>
    public enum ChangeTypes : short
    {
        /// <summary>None</summary>
        None = 0,
        /// <summary>Save</summary>
        Save = 1,
        /// <summary>Update</summary>
        Update = 2,
        /// <summary>SaveOrUpdate</summary>
        SaveOrUpdate = 3,
        /// <summary>Delete</summary>
        Delete = 4
    }

    #endregion ChangeTypes

    #region RepoTypes

    /// <summary>Repository Types</summary>
    public enum RepoTypes : short
    {
        /// <summary>Thread Static</summary>
        Ts = 0,
        /// <summary>Web</summary>
        Web = 1,
    }

    #endregion RepoTypes

    /// <summary>DB connection types.</summary>
    public enum DBConTypes
    {
        /// <summary>SqlServer</summary>
        SqlServer,
        /// <summary>SqlServerCE</summary>
        SqlServerCE,
        /// <summary>mySQL</summary>
        MySql,
        /// <summary>postgreSQL</summary>
        PostgreSQL,
        /// <summary>Oracle</summary>
        Oracle,
        /// <summary>SQLite</summary>
        SQLite
    }
}