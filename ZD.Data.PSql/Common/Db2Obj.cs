﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Z.Data.PSql.Mapping;
using Z.Extensions;
using Z.Util;

namespace Z.Data.PSql.Common;

/// <summary>Database-Object Converters</summary>
public static class Db2Obj {
    /// <summary>Database Value to Object value converter</summary>
    static Db2Obj() {
    }
    /// <summary>Fill an object from database</summary>
    public static T FillObject<T>(IDataReader rd, T obj = default, string configName = "default") {
        return FillReaderToObject<T>(rd, obj, configName);
    }

    private static T FillReaderToObject<T>(IDataReader rd, T obj = default, string configName = "default") {
        if (rd == null || rd.FieldCount <= 0) return obj;
        if (obj == null) obj = Activator.CreateInstance<T>();
        for (int i = 0; i < rd.FieldCount; i++) {
            if (rd[i] == null || rd[i] == DBNull.Value) continue;
            var pm = PSqlConfig.ConfigGet(configName).GetMappedProperty(typeof(T), rd.GetName(i));
            if (pm == null) continue;
            FillValueToMember(pm, obj, rd[i], rd.GetFieldType(i));
            /*foreach (var member in members) {
                if (member != null && fillValueToMember(member, obj, rd[i], dbType)) { break; }
            }*/
        }
        return obj;
    }
    private static bool FillValueToMember(PropertyMap pm, object obj, object dbValue, Type dbType) {
        if (pm == null) return false;
        MemberInfo member = pm.Property;
        if (member == null) return false;
        var memberType = member.GetMemberType();
        if (member is PropertyInfo propertyInfo && !(propertyInfo).CanWrite) return false;
        if (dbType.IsAssignableTo(memberType)) {
            member.SetValue(obj, dbValue);
        } else if (dbValue == null || dbValue == DBNull.Value) {
            member.SetValue(obj, null);
        }
         //DateTime
         else if (memberType.IsAssignableTo<DateTime>()) {
            if (dbType.IsAssignableTo<string>())
                member.SetValue(obj, DateTime.Parse((string)dbValue, null, System.Globalization.DateTimeStyles.AssumeUniversal).ToUniversalTime());
            else if (dbType.IsAssignableTo<long>())
                member.SetValue(obj, new DateTime((long)dbValue, DateTimeKind.Utc).ToUniversalTime());
            else
                return false; //property.SetValue(obj, null, null);  //Invalid type match
        }
         //Guid
         else if (memberType.IsAssignableTo<Guid>()) {
            if (dbType.IsAssignableTo<string>())
                member.SetValue(obj, Guid.Parse((string)dbValue));
            else if (dbType.IsAssignableTo<byte[]>())
                member.SetValue(obj, new Guid((byte[])dbValue));
            else
                return false; // property.SetValue(obj, null, null); //Invalid type match
        }
         ////Bitmask
         //else if (memberType.Is().CastableTo<Bitmask>()) {
         //    if (dbType.Is().CastableTo<string>())
         //        member.SetValue(obj, new Bitmask((long)dbValue));
         //    else if (dbType.Is().CastableTo<long>())
         //        member.SetValue(obj, new Bitmask((long)dbValue));
         //    else if (dbType.Is().CastableTo<int>())
         //        member.SetValue(obj, new Bitmask((long)dbValue));
         //    else if (dbType.Is().CastableTo<byte[]>())
         //        member.SetValue(obj, new Bitmask(BitConverter.ToInt64((byte[])dbValue,0)));
         //    else
         //        return false;
         //}
         //CSV
         else if (memberType.IsAssignableTo<ICollection<string>>() && pm is PropertyCsvMap) {
            var csvMap = pm as PropertyCsvMap;
            if (dbType.IsAssignableTo<string>()) {
                var values = Activator.CreateInstance(memberType) as ICollection<string>;
                var csv = dbValue as string;
                foreach (var str in csv.Split(csvMap.Delimiter)) {
                    values.Add(str);
                }
                if (member.GetValue(obj) is ICollection<string> curValue)
                    curValue.Add(values);
                else
                    member.SetValue(obj, values);
            }
        } else {
            //Use custom converters
            member.SetValue(obj, dbValue.To().Type(memberType, member.GetValue(obj), SysInf.InvariantFormat));
        }
        return true; //Done
    }
}