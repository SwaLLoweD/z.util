﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using Z.Collections.Generic;
using Z.Data.PSql.Mapping;
using Z.Extensions;
using Z.Util;

namespace Z.Data.PSql.Common;

/// <summary>Database-Object Converters</summary>
public static class Obj2Db {
    /// <summary>Collection batch size</summary>
    private static readonly IDictionary<Type, Func<PropertyMap, object, string>> valueConverterString = new Dictionary<Type, Func<PropertyMap, object, string>>();

    /// <summary>Obj2Db</summary>
    static Obj2Db() {
        valueConverterString[typeof(string)] = ValueConverterDefault<string>;
        valueConverterString[typeof(char)] = ValueConverterDefault<char>;
        valueConverterString[typeof(sbyte)] = ValueConverterDefault<sbyte>;
        valueConverterString[typeof(short)] = ValueConverterDefault<short>;
        valueConverterString[typeof(int)] = ValueConverterDefault<int>;
        valueConverterString[typeof(long)] = ValueConverterDefault<long>;
        valueConverterString[typeof(byte)] = ValueConverterDefault<byte>;
        valueConverterString[typeof(ushort)] = ValueConverterDefault<ushort>;
        valueConverterString[typeof(uint)] = ValueConverterDefault<uint>;
        valueConverterString[typeof(ulong)] = ValueConverterDefault<ulong>;
        valueConverterString[typeof(Guid)] = ValueConverterDefault<Guid>;
        valueConverterString[typeof(byte[])] = ValueConverterByteArray;
        valueConverterString[typeof(float)] = ValueConverterDefault2<float>;
        valueConverterString[typeof(double)] = ValueConverterDefault2<double>;
        valueConverterString[typeof(decimal)] = ValueConverterDefault2<decimal>;
        valueConverterString[typeof(DateTime)] = ValueConverterDateTime;
        valueConverterString[typeof(bool)] = ValueConverterBool;
        valueConverterString[typeof(ICollection<string>)] = ValueConverterCsv;
    }

    /*
    /// <summary>Determines whether this instance can convert.</summary>
    /// <typeparam name="T"></typeparam>
    /// <returns><c>true</c> if this instance can convert; otherwise, <c>false</c>.</returns>
    public static bool CanConvert<T>() { return CanConvert(typeof(T)); }

    /// <summary>Determines whether this instance can convert the specified type.</summary>
    /// <param name="type">The type.</param>
    /// <returns><c>true</c> if this instance can convert the specified type; otherwise, <c>false</c>. </returns>
    public static bool CanConvert(Type type) { return findMatchingType(type) != null; } */

    /// <summary> Converts the specified property.</summary>
    /// <param name="property">The property.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static string Convert(PropertyMap property, object value) => valueConverterString.TryGetValue(FindMatchingType(property))(property, value);

    /// <summary> Converts the specified property.</summary>
    /// <param name="property">The property.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static string ConvertString(PropertyMap property, object value) => valueConverterString.TryGetValue(FindMatchingType(property))(property, value);

    /// <summary>Convert the specified value to db string (less reliable, does not match all props)</summary>
    /// <param name="value">Value.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static string Convert<T>(T value) => Convert(typeof(T), value);

    /// <summary>Convert the specified value to db string (less reliable, does not match all props)</summary>
    /// <param name="t">Value type</param>
    /// <param name="value">Value.</param>
    public static string Convert(Type t, object value) => valueConverterString.TryGetValue(FindMatchingType(t))(null, value);

    private static Type FindMatchingType(PropertyMap property) {
        var t = property.Property.PropertyType;
        if (t == null) return null;
        if (property is PropertyCsvMap && t.IsAssignableTo<ICollection<string>>()) return typeof(ICollection<string>);
        return FindMatchingType(t);
    }
    private static Type FindMatchingType(Type t) {
        if (t == null) return null;
        if (t.IsAssignableTo<ICollection<string>>()) return typeof(ICollection<string>);
        if (valueConverterString.ContainsKey(t)) return t;
        //foreach (var k in valueConverter.Keys)
        //    if (t.Is().CastableTo(k)) return k;
        var baseType = FindMatchingType(t.BaseType);
        return baseType;
    }
#pragma warning disable
    private static string ValueConverterDefault<T>(PropertyMap pm, object e) {
        var val = e; //pi.GetValue(e, null);
        if (val == null) return null;
        return ((T)val).ToString();
    }

    private static string ValueConverterDefault2<T>(PropertyMap pm, object e) {
        var val = e; //pi.GetValue(e, null);
        if (val == null) return null;
        return ((T)val).To().Type<string>(null, SysInf.InvariantFormat);
    }
#pragma warning restore

    private static string ValueConverterByteArray(PropertyMap pm, object e) {
        if (e is not byte[] val) return null;
        return val.To().String.AsHex();
    }
    /*
    private static string valueConverterSingle(PropertyMapping pm, object e)
    {
        var val = e; //pi.GetValue(e, null);
        if (val == null) return null;
        return ((float)val).ToString(SysInf.InvariantFormat);
    }
    private static string valueConverterDouble(PropertyMapping pm, object e)
    {
        var val = e; //pi.GetValue(e, null);
        if (val == null) return null;
        return ((double)val).ToString(SysInf.InvariantFormat);
    }
    private static string valueConverterDecimal(PropertyMapping pm, object e)
    {
        var val = e; //pi.GetValue(e, null);
        if (val == null) return null;
        return ((decimal)val).ToString(SysInf.InvariantFormat);
    } */
    private static string ValueConverterDateTime(PropertyMap pm, object e) {
        var val = e; //pi.GetValue(e, null);
        if (val == null) return null;
        return ((DateTime)val).ToUniversalTime().To().StringGMT(false);
    }
    private static string ValueConverterBool(PropertyMap pm, object e) {
        var val = e; //pi.GetValue(e, null);
        if (val == null) return null;
        return (((bool)val) ? 1 : 0).ToString();
    }
    private static string ValueConverterCsv(PropertyMap pm, object e) {
        //pi.GetValue(e, null);
        if (e is not ICollection<string> val) return null;
        if (pm is PropertyCsvMap pmCsv) val.To().CSV(pmCsv.Delimiter);
        return val.To().CSV("|");
    }
    // private static string valueConverterBitmask(PropertyMap pm, object e) {
    //     var val = e; //pi.GetValue(e, null);
    //     if (val == null) return null;
    //     return ((Bitmask)val).Mask.ToString();
    // }
}