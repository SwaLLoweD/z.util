﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using Z.Collections.Generic;
using Z.Data.PSql.Mapping;
using Z.Extensions;
using Z.Util;

namespace Z.Data.PSql.Common
{
    /// <summary>Database-Object Converters</summary>
    public static class Obj2Db
    {
        /// <summary>Collection batch size</summary>
        private static IDictionary<Type, Func<PropertyMap, object, object>> valueConverter = new Dictionary<Type, Func<PropertyMap, object, object>>();

        /// <summary></summary>
        static Obj2Db()
        {
            //valueConverter[typeof(string)] = valueConverterDefault<string>;
            //valueConverter[typeof(char)] = valueConverterDefault<char>;
            //valueConverter[typeof(sbyte)] = valueConverterDefault<sbyte>;
            //valueConverter[typeof(short)] = valueConverterDefault<short>;
            //valueConverter[typeof(int)] = valueConverterDefault<int>;
            //valueConverter[typeof(long)] = valueConverterDefault<long>;
            //valueConverter[typeof(byte)] = valueConverterDefault<byte>;
            //valueConverter[typeof(ushort)] = valueConverterDefault<ushort>;
            //valueConverter[typeof(uint)] = valueConverterDefault<uint>;
            //valueConverter[typeof(ulong)] = valueConverterDefault<ulong>;
            //valueConverter[typeof(Guid)] = valueConverterDefault<Guid>;
            //valueConverter[typeof(byte[])] = valueConverterByteArray;
            //valueConverter[typeof(float)] = valueConverterDefault2<float>;
            //valueConverter[typeof(double)] = valueConverterDefault2<double>;
            //valueConverter[typeof(decimal)] = valueConverterDefault2<decimal>;
            //valueConverter[typeof(DateTime)] = valueConverterDateTime;
            //valueConverter[typeof(bool)] = valueConverterBool;
            valueConverter[typeof(ICollection<string>)] = valueConverterCsv;
            //valueConverter[typeof(Bitmask)] = valueConverterBitmask;
        }

        /*
        /// <summary>Determines whether this instance can convert.</summary>
        /// <typeparam name="T"></typeparam>
        /// <returns><c>true</c> if this instance can convert; otherwise, <c>false</c>.</returns>
        public static bool CanConvert<T>() { return CanConvert(typeof(T)); }

        /// <summary>Determines whether this instance can convert the specified type.</summary>
        /// <param name="type">The type.</param>
        /// <returns><c>true</c> if this instance can convert the specified type; otherwise, <c>false</c>. </returns>
        public static bool CanConvert(Type type) { return findMatchingType(type) != null; } */

        /// <summary> Converts the specified property.</summary>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static object Convert(PropertyMap property, object value, out DbType dbType) {
            dbType = DbType.String;
            var t = property.Property.PropertyType;           
            if (t == null) return null;
            var match = lookupDefaultType(t, out dbType);
            if (match != null) return value;
            match = lookupConvertibleType(t);
            if (match != null) {
                var result = valueConverter.TryGetValue(match)(null, value);
                dbType = result.DbType;
                return result.Value;
            }
            if (property is PropertyCsvMap && t.Is().CastableTo<ICollection<string>>()) {
                var result = valueConverter[typeof(ICollection<string>)](property, value);
                dbType = result.DbType;
                return result.Value; 
            }
            return value; 
        }

        ///// <summary>Convert the specified value to db string (less reliable, does not match all props)</summary>
        ///// <param name="value">Value.</param>
        ///// <param name="dbType">DbType of the value</param>
        ///// <typeparam name="T">The 1st type parameter.</typeparam>
        //public static object Convert<T>(T value, out DbType dbType) { return Convert(typeof(T), value, out dbType); }

        ///// <summary>Convert the specified value to db string (less reliable, does not match all props)</summary>
        ///// <param name="t">Value type</param>
        ///// <param name="value">Value.</param>
        //public static object Convert(Type t, object value, out DbType dbType) {
        //    dbType = DbType.String;
        //    if (t == null) return null;
        //    var match = lookupDefaultType(t, out dbType);
        //    if (match != null) return value;
        //    match = lookupConvertibleType(t);
        //    if (match != null) {
        //        var result = valueConverter.TryGetValue(match)(null, value);
        //        dbType = result.DbType;
        //        return result.Value;
        //    }
        //    if (t.Is().CastableTo<ICollection<string>>()) {
        //        var result = valueConverter[typeof(ICollection<string>)](null, value);
        //        dbType = result.DbType;
        //        return result.Value; 
        //    }
        //    return value; 
        //}
        //private static Type lookupDefaultType(Type t, out DbType dbType)
        //{
        //    dbType = DbType.String;
        //    if (t == null || t == typeof(object)) return null;
        //    if (typeMap.ContainsKey(t)) { dbType = typeMap[t]; return t; }
        //    return lookupDefaultType(t.BaseType, out dbType);
        //}

        //private static Type lookupConvertibleType(Type t)
        //{
        //    if (t == null || t == typeof(object)) return null;
        //    if (valueConverter[t] != null) return t;
        //    return lookupConvertibleType(t.BaseType);
        //}


        //private static string valueConverterDefault<T>(PropertyMap pm, object e)
        //{
        //    var val = e; //pi.GetValue(e, null);
        //    if (val == null) return null;
        //    return ((T)val).ToString();
        //}

        //private static string valueConverterDefault2<T>(PropertyMap pm, object e)
        //{
        //    var val = e; //pi.GetValue(e, null);
        //    if (val == null) return null;
        //    return ((T)val).To().Type<string>(null, SysInf.InvariantFormat);
        //}

        //private static string valueConverterByteArray(PropertyMap pm, object e)
        //{
        //    var val = e as byte[];
        //    if (val == null) return null;
        //    return val.To().String.AsHex();
        //}
        /*
        private static string valueConverterSingle(PropertyMapping pm, object e)
        {
            var val = e; //pi.GetValue(e, null);
            if (val == null) return null;
            return ((float)val).ToString(SysInf.InvariantFormat);
        }
        private static string valueConverterDouble(PropertyMapping pm, object e)
        {
            var val = e; //pi.GetValue(e, null);
            if (val == null) return null;
            return ((double)val).ToString(SysInf.InvariantFormat);
        }
        private static string valueConverterDecimal(PropertyMapping pm, object e)
        {
            var val = e; //pi.GetValue(e, null);
            if (val == null) return null;
            return ((decimal)val).ToString(SysInf.InvariantFormat);
        } */
        //private static string valueConverterDateTime(PropertyMap pm, object e)
        //{
        //    var val = e; //pi.GetValue(e, null);
        //    if (val == null) return null;
        //    return ((DateTime)val).ToUniversalTime().To().StringGMT(false);
        //}
        //private static string valueConverterBool(PropertyMap pm, object e)
        //{
        //    var val = e; //pi.GetValue(e, null);
        //    if (val == null) return null;
        //    return (((bool)val) ? 1 : 0).ToString();
        //}
        private static object valueConverterCsv(PropertyMap pm, object e)
        {
            var val = e as ICollection<string>; //pi.GetValue(e, null);
            var pmCsv = pm as PropertyCsvMap;
            if (val == null) return null;
            if (pmCsv != null) val.To().CSV(pmCsv.Delimiter);
            return val.To().CSV("|");
            //val.To().CSV("|");
        }
        private static object valueConverterBitmask(PropertyMap pm, object e)
        {
            var val = e; //pi.GetValue(e, null);
            if (val == null) return null;
            return ((Bitmask)val).Mask;
        }
    }
}