/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;
using Z.Collections.Concurrent;
using Z.Data.Common;
using Z.Extensions;
using System.Threading;
using Z.Util;
using System.Linq;

namespace Z.Data.PSql;

/// <summary>Db Data Source Repository</summary>
public partial class Repo : IRepoDbPSql, IDisposable
{
    private bool _disposed = false;

    [ThreadStatic]
    private static Repo repoTs;
    /// <summary>Thread Static Repo</summary>
    public static Repo Ts => repoTs ??= new Repo();

    /// <summary>Open connections and their timeout timers</summary>
    private readonly SyncSet<IDbConnection> connections = new();
    //[ThreadStatic]
    private IDbConnection activeConnection;

    /// <summary>Original DbConnection</summary>
    public IDbConnection ActiveConnection {
        get {
            activeConnection ??= CreateActiveConnection();
            activeConnection.OpenIfNot();
            return activeConnection;
        }
    }

    /// <summary>Gets or sets the current config.</summary>
    public PSqlConfig Config { get; set; }

    /// <summary>constructor</summary>
    public Repo(string configName = "default") {
        Config = PSqlConfig.ConfigGet(configName);
    }

    #region Connection operations

    private IDbConnection CreateActiveConnection() {
        var rval = Config.Connection;
        //var rval = Activator.CreateInstance(Config.Connection.GetType()) as IDbConnection;
        //rval.ConnectionString = Config.Connection.ConnectionString;
        rval.OpenIfNot();
        return rval;
    }
    /// <summary>If persistent returns active connection, else creates a new connection</summary>
    internal IDbConnection CreateOrGetDbConnection() {
        if (Config.PersistentConnections) return ActiveConnection;
        //var rval = Config.Connection;
        //Create new connection
        var rval = Config.Connection.GetType().CreateInstance<IDbConnection>();
        rval.ConnectionString = Config.ConnectionString;
        rval.OpenIfNot();
        //Set timeout timer
        var tmr = new Timer(TmrConnection_Elapsed, rval, Config.ConnectionTimeout * 1000, 0);
        return rval;
    }

    private void TmrConnection_Elapsed(object state) {
        //Connection cleanup
        var con = state as IDbConnection;
        if (con != null) {
            if (con.State == ConnectionState.Executing || con.State == ConnectionState.Fetching) {
                //Connection is active, do not close
                var tmr = new Timer(TmrConnection_Elapsed, con, Config.ConnectionTimeout * 1000, 0);
                return;
            }
            if (con.State != ConnectionState.Closed) con.Close();
            con.Dispose();
            connections.Remove(con);
        } else {
            connections.Remove(con);
        }
    }

    /// <summary>Close current connection if connections are not persistent</summary>
    internal void ReleaseConnection(IDbConnection connection) {
        if (Config.PersistentConnections) return;
        if (connection == null) return;
        if (connection.State != ConnectionState.Closed) connection.Close();
        connection.Dispose();
        connections.Remove(connection);
    }
    #endregion

    /// <summary>Get Methods</summary>
    public IRepoDbGetPart<T> Get<T>() { return new RepoGetPart<T>(this); }

    /// <inheritdoc/>
    public ITransactionDb Transaction { get; set; }
    /// <inheritdoc/>
    public ITransactionDb BeginTransaction(IsolationLevel isolationLevel) { return null; }
    /// <inheritdoc/>
    public bool IsOpen { get { return ActiveConnection.State == ConnectionState.Open; } }
    /// <inheritdoc/>
    public void Clear() { }
    /// <inheritdoc/>
    public void Flush() { }
    /// <inheritdoc/>
    public void Close() { activeConnection?.Close(); }

    ///// <summary>Gets the db connection.</summary>
    //public static System.Data.IDbConnection GetDbConnection(Repo repo)
    //{
    //    return repo.Session.Connection;
    //}

    #region Dispose
    /// <inheritdoc/>
    public virtual void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    /// <inheritdoc/>
    protected virtual void Dispose(bool disposing) {
        if (!_disposed) {
            if (disposing) {
                if (activeConnection != null) {
                    activeConnection.Close();
                    activeConnection.Dispose();
                    activeConnection = null;
                }
                //DetachEvents();
                //SessionFlush();
                //SessionClose();
            }
            // Call the appropriate methods to clean up
            // unmanaged resources here.
            _disposed = true;
        }
    }
    #endregion Dispose
}