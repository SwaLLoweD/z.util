/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Z.Data.Common;
using Z.Data.PSql.Common;
using Z.Extensions;
using Z.Util;

namespace Z.Data.PSql;

public partial class Repo {
    internal static object readerLock = new();
    #region Get Methods

    /// <summary>Get Methods</summary>
    public class RepoGetPart<T>: IRepoDbGetPartPSql<T> {
        /// <inheritdoc/>
        public IRepoDb Repository { get; set; }
        private Repo Repo => Repository as Repo;
        /// <summary>Repo Get</summary>
        public RepoGetPart(Repo repository) { Repository = repository; }
        /// <inheritdoc/>
        public T ById<U>(U id, bool skipData = false) {
            T rval = default;
            var idProps = Repo.Config.GetMappedIdProperties<T>();
            var sb = new StringBuilder();
            sb.AppendString("SELECT * FROM ", Repo.Config.GetMappedTableName<T>(), " WHERE ");
            using (var cmd = Repo.CreateCommand()) {
                var i = 0;
                foreach (var idProp in idProps) {
                    sb.AppendString(idProp.Column, $" = @V{i} AND");
                    cmd.ParameterAdd($"@V{i}", Mapping.PropertyMap.GetDbValue(id), idProp.DbType);
                    i++;
                }
                sb.Remove(sb.Length - 4, 4);
                cmd.CommandText = sb.ToString();
                rval = All(cmd).FirstOrDefault();
            }
            return rval;
        }

        /// <inheritdoc/>
        public IList<T> ByProperty<U>(Expression<Func<T, U>> prop, params U[] values) {
            return ByProperty(Fnc.LambdaProperty(prop), values);
        }
        /// <inheritdoc/>
        public IList<T> ByProperty<U>(PropertyInfo prop, params U[] values) {
            var pm = Repo.Config.GetMappedProperty(typeof(T), prop);
            if (pm == null || values == null) throw new InvalidOperationException();
            using var cmd = Repo.CreateCommand();
            var sb = new StringBuilder();
            sb.AppendString("SELECT * FROM ", Repo.Config.GetMappedTableName<T>(), " WHERE ", pm.Column);
            if (values.Length <= 1) {
                var cValue = Mapping.PropertyMap.GetDbValue(values[0]);
                if (cValue != null) {
                    sb.AppendString(" = @V1");
                    cmd.ParameterAdd("@V1", cValue, pm.DbType);
                } else {
                    sb.AppendString(" IS NULL ");
                }
            } else {
                sb.AppendString(" IN (");
                for (int i = 0; i < values.Length; i++) {
                    var value = values[i];
                    var cValue = Mapping.PropertyMap.GetDbValue(value);
                    if (cValue == null) continue;
                    sb.AppendString($"@V{i}, ");
                    cmd.ParameterAdd($"@V{i}", cValue, pm.DbType);
                }
                sb.Remove(sb.Length - 2, 2);
                sb.Append(')');
            }

            cmd.CommandText = sb.ToString();
            return All(cmd);
        }

        /// <inheritdoc/>
        public IList<T> ByExample(T exampleInstance, bool excludeNulls = true, bool excludeZeros = false, params string[] propertiesToExclude) { throw new NotImplementedException(); }
        #region All
        /// <summary>Get All Entities recorded in db of specific type</summary>
        public IList<T> All() {
            using var cmd = Repo.CreateCommand();
            cmd.CommandText = $"SELECT * FROM {Repo.Config.GetMappedTableName<T>()}";
            return All(cmd);
        }
        /// <inheritdoc/>
        public IList<T> All(string SQLquery, params object[] parameters) {
            using var cmd = Repo.CreateCommand();
            cmd.CommandText = SQLquery;
            foreach (var p in parameters) {
                if (p == null) continue;
                cmd.ParameterAdd(p);
            }
            return All(cmd);
        }
        /// <inheritdoc/>
        public IList<T> All(IDbCommand cmd) {
            IList<T> rval;
            lock (Repo.readerLock) {
                using var rd = cmd.ExecuteReader();
                rval = All(rd);
            }
            return rval;
        }
        /// <summary>Fills all results from a reader into a list</summary>
        /// <param name="reader">Reader to read</param>
        public IList<T> All(IDataReader reader) {
            var rval = new List<T>();
            while (reader.Read()) {
                T obj = default;
                if (typeof(T).IsValueType && reader.FieldCount == 1) {
                    var val = reader[0];
                    if (val != null && val != DBNull.Value && val.GetType().IsAssignableTo<T>()) obj = (T)val;
                } else {
                    obj = Db2Obj.FillObject<T>(reader, default, Repo.Config.Name);
                    if (obj == null) continue;
                }
                rval.Add(obj);
            }
            return rval;
        }
        #endregion All

        #region Paged
        /// <inheritdoc/>
        public IList<T> Paged(int limit, int? offset = null) {
            using var cmd = Repo.CreateCommand();
            cmd.CommandText = $"SELECT * FROM {Repo.Config.GetMappedTableName<T>()}";
            return Paged(cmd, limit, offset);
        }
        /// <inheritdoc/>
        public IList<T> Paged(string SQLquery, int limit, int? offset = null, params object[] parameters) {
            using var cmd = Repo.CreateCommand();
            cmd.CommandText = SQLquery;
            foreach (var p in parameters) {
                if (p == null) continue;
                cmd.ParameterAdd(p);
            }
            return Paged(cmd, limit, offset);
        }
        /// <inheritdoc/>
        public IList<T> Paged(IDbCommand cmd, int limit, int? offset = null) {
            if (cmd.GetType().FullName.Contains("System.Data.SqlClient.SqlCommand")) {
                if (offset != null) cmd.CommandText += $" OFFSET {offset} ";
                cmd.CommandText += $" ROWS FETCH NEXT {limit} ROWS ONLY ";
            } else {
                cmd.CommandText += $" LIMIT {limit} ";
                if (offset != null) cmd.CommandText += $" OFFSET {offset} ";
            }
            lock (Repo.readerLock) {
                using var rd = cmd.ExecuteReader();
                return All(rd);
            }
        }
        #endregion Paged

        /// <summary>Get maximum allowable string length of a string property</summary>
        public uint PropertyStrLen(System.Linq.Expressions.Expression<Func<T, string>> property) {
            return PropertyStrLen(Z.Util.Fnc.LambdaProperty<T, string>(property));
        }
        /// <summary>Get maximum allowable string length of a string property</summary>
        public uint PropertyStrLen(PropertyInfo property) {
            if (property == null) return uint.MaxValue;
            var typeMap = Repo.Config.EntityMaps.TryGetValue(typeof(T));
            if (typeMap == null) return uint.MaxValue;
            var propMap = typeMap.Properties[property];
            if (propMap == null) return uint.MaxValue;
            return propMap.LengthMax;
        }
    }

    #endregion Get Methods
}