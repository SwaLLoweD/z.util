/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using Z.Data.Common;
using Z.Util.Common;

namespace Z.Data.PSql;

/// <summary>Repository Event Handler</summary>
/// <param name="repo">Repository</param>
/// <param name="args">Arguments</param>
public delegate void RepositoryEventHandler(IRepo repo, RepositoryEventArgs args);

public partial class Repo
{
    #region Events
    private static readonly object eventLock = new();
    private static RepositoryEventHandler onError;
    private static EventHandler<ChangeTypes> onLoad;
    private static EventHandler<ChangeTypes> onChange;
    private static EventHandler<ChangeTypes> onDelete;
    private static EventHandler<ChangeTypes> onSaveOrUpdate;
    //private static OnEntityChangedEventHandler onUpdated;

    /// <summary>Fired on Entity Load</summary>
    public static event RepositoryEventHandler OnError { add { lock (eventLock) { onError += value; } } remove { lock (eventLock) { onError -= value; } } }

    /// <summary>Fired on Entity Load</summary>
    public static event EventHandler<ChangeTypes> OnLoad { add { lock (eventLock) { onLoad += value; } } remove { lock (eventLock) { onLoad -= value; } } }

    /// <summary>Fired on Entity Change</summary>
    public static event EventHandler<ChangeTypes> OnChange { add { lock (eventLock) { onChange += value; } } remove { lock (eventLock) { onChange -= value; } } }

    /// <summary>Fired on Entity Delete</summary>
    public static event EventHandler<ChangeTypes> OnDelete { add { lock (eventLock) { onDelete += value; } } remove { lock (eventLock) { onDelete -= value; } } }

    /// <summary>Fired on Entity Save or Update</summary>
    public static event EventHandler<ChangeTypes> OnSaveOrUpdate { add { lock (eventLock) { onSaveOrUpdate += value; } } remove { lock (eventLock) { onSaveOrUpdate -= value; } } }

    //public static event OnEntityChangedEventHandler OnUpdated { add { lock (eventLock) { onUpdated += value; } } remove { lock (eventLock) { onUpdated -= value; } } }

    /// <summary>Load event trigger</summary>
    internal static void HandleLoad(object e) {
        EventCall(onLoad, e, ChangeTypes.None);
    }
    /// <summary>Event Trigger</summary>
    internal static void HandleEvent(object e, ChangeTypes ct) {
        switch (ct) {
            case ChangeTypes.Delete:
                EventCall(onDelete, e, ct);
                break;

            case ChangeTypes.Save:
            case ChangeTypes.Update:
            case ChangeTypes.SaveOrUpdate:
                EventCall(onSaveOrUpdate, e, ct);
                //if (onUpdated != null) onUpdated(e, ct);
                break;
        }
        EventCall(onChange, e, ct);
    }
    #endregion Events

    #region DetachEvents
    /// <summary>Detach all events attached to this repo</summary>
    public static void DetachEvents() {
        DetachEvent(onLoad);
        DetachEvent(onChange);
        DetachEvent(onDelete);
        DetachEvent(onSaveOrUpdate);
        //detachEvent(onUpdated);

        lock (eventLock) {
            if (onError == null) return;
            foreach (Delegate d in onError.GetInvocationList()) {
                try { onError -= (RepositoryEventHandler)d; } catch { }
            }
        }
    }
    private static void DetachEvent(EventHandler<ChangeTypes> e) {
        lock (eventLock) {
            if (e == null) return;
            foreach (Delegate d in e.GetInvocationList()) {
                try { e -= (EventHandler<ChangeTypes>)d; } catch { }
            }
        }
    }
    #endregion DetachEvents

    #region EventCall
    /// <summary>Event Caller</summary>
    protected static bool EventCall(EventHandler<ChangeTypes> handler, object entity, ChangeTypes changeType, bool lockingEvent = true) {
        if (lockingEvent) {
            lock (eventLock) {
                return EventCallPriv(handler, entity, changeType);
            }
        }
        return EventCallPriv(handler, entity, changeType);
    }

    private static bool EventCallPriv(EventHandler<ChangeTypes> handler, object entity, ChangeTypes changeType) {
        try {
            handler?.Invoke(entity, changeType);
        } catch (Exception exp) {
            onError?.Invoke(null, new RepositoryEventArgs("Could not perform outer event call", null, null, exp));
            return false;
        }
        return true;
    }

    /// <summary>Event Caller</summary>
    protected static bool EventCall(RepositoryEventHandler handler, Repo repo, RepositoryEventArgs args, bool lockingEvent = true) {
        if (lockingEvent) {
            lock (eventLock) {
                return EventCallPriv(handler, repo, args);
            }
        }
        return EventCallPriv(handler, repo, args);
    }

    private static bool EventCallPriv(RepositoryEventHandler handler, Repo repo, RepositoryEventArgs args) {
        try {
            handler?.Invoke(repo, args);
        } catch (Exception exp) {
            onError?.Invoke(repo, new RepositoryEventArgs("Could not perform outer event call", null, null, exp));
            return false;
        }
        return true;
    }
    #endregion
}