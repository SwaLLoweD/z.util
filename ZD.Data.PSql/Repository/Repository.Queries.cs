/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Z.Data.Common;
using Z.Data.PSql.Mapping;
using Z.Extensions;

namespace Z.Data.PSql;

public partial class Repo
{
    /// <summary>Creates an IDbCommand</summary>
    public IDbCommand CreateCommand() {
        var rval = new PSqlCommand(this) {
            CommandTimeout = Config.CommandTimeout
        };
        return rval;
    }
    /// <inheritdoc/>
    public object Execute(string SQLquery) {
        using var cmd = CreateCommand();
        cmd.CommandText = SQLquery;
        return cmd.ExecuteScalar();
    }
    /// <inheritdoc/>
    public object Execute(IDbCommand cmd) {
        return cmd.ExecuteScalar();
    }
    /// <inheritdoc/>
    public object GetOriginalEntityProperty<T>(T entity, string propertyName) {
        var orig = Reload(entity);
        if (orig == null) return null;
        var pi = orig.GetType().GetProperty(propertyName);
        if (pi == null) return null;
        return pi.GetValue(orig, null);
    }

    /// <inheritdoc/>
    public bool IsMapped<T>(T entity) { return Config.GetMappedTableName<T>().Is().NotEmpty; }

    #region IsDirty
    /// <inheritdoc/>
    public bool IsDirtyEntity<T>(T entity) => GetDirtyProperties(entity).Any();
    /// <inheritdoc/>
    public bool IsDirtyProperty<T>(T entity, String propertyName) => GetDirtyProperties(entity).Any(x => x.Property.Name == propertyName);
    /// <summary>Check if Entity has different values than the one in db</summary>
    protected IEnumerable<PropertyMap> GetDirtyProperties<T>(T entity) {
        var rval = new List<PropertyMap>();
        if (entity == null) return rval;
        var dbEntity = Reload<T>(entity);

        var typeProps = Config.GetMappedProperties<T>().ToArray();
        for (int i = 0; i < typeProps.Length; i++) {
            var prop = typeProps[i];
            if (!prop.Property.CanRead) continue;
            dynamic dbValue = prop.Property.GetValue(dbEntity, null);
            dynamic curValue = prop.Property.GetValue(entity, null);
            if (object.Equals(dbValue, curValue)) continue;
            try {
                if ((dbValue is System.Collections.IEnumerable || curValue is System.Collections.IEnumerable) && Enumerable.SequenceEqual(dbValue, curValue)) continue;
            }
            //var dbValue = prop.Property.GetValue(dbEntity, null);
            //var curValue = prop.Property.GetValue(entity, null);
            //if (object.Equals(dbValue, curValue)) continue;
            //try {
            //    if (dbValue is System.Collections.IEnumerable || curValue is System.Collections.IEnumerable) {
            //        var mi = typeof(Enumerable).GetMethod("SequenceEqual", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            //        var fooRef = mi.MakeGenericMethod(prop.Property.PropertyType);
            //        fooRef.Invoke(null, new object[] { dbValue, curValue });
            //        continue;
            //        //Enumerable.SequenceEqual(dbValue, curValue)  Requires dynamic keyword to work
            //    }
            //}
            catch { }
            rval.Add(prop);
        }
        return rval;
    }
    #endregion IsDirty

    #region Validation
    /// <summary>Checks if entity is valid or not</summary>
    public bool IsValid<T>(T entity) {
        foreach (var prop in Config.GetMappedProperties(entity.GetType())) {
            if (GetInvalidValues(entity.GetType(), prop.Property, prop.Property.GetValue(entity)).Any()) return false;
        }
        return true;
    }
    /// <summary>Returns invalid values of an entity</summary>
    public IEnumerable<InvalidValue> GetInvalidValues<T>(T entity) {
        var rval = new List<InvalidValue>();
        foreach (var prop in Config.GetMappedProperties(entity.GetType())) {
            rval.Add(GetInvalidValues(entity.GetType(), prop.Property, prop.Property.GetValue(entity)));
        }
        return rval;
    }
    /// <summary>Returns invalid values of an entity property</summary>
    public IEnumerable<InvalidValue> GetInvalidValues(Type type, System.Reflection.PropertyInfo property, object value) {
        var rval = Array.Empty<InvalidValue>();
        var propMap = Config.GetMappedProperty(type, property);
        if (propMap == null) return rval;
        try {
            var dbValue = PropertyMap.GetDbValue(value);
            if (dbValue == null) {
                if (!propMap.Nullable) return new InvalidValue[] { new InvalidValue("can not be null!", null, type, property, dbValue) };
            } else if (dbValue is System.Collections.IList dbValueL) {
                if (propMap.LengthMax != uint.MaxValue && dbValueL.Count > propMap.LengthMax) return new InvalidValue[] { new InvalidValue($"can not be longer than {propMap.LengthMax}", null, type, property, dbValue) };
                if (propMap.LengthMin != uint.MinValue && dbValueL.Count < propMap.LengthMin) return new InvalidValue[] { new InvalidValue($"can not be shorter than {propMap.LengthMin}", null, type, property, dbValue) };
            }
        } catch {
            return rval;
        }
        return rval;
    }
    /// <summary>Returns invalid valiues of an entity property</summary>
    public IEnumerable<Z.Data.Common.InvalidValue> GetInvalidValues(string entityFN, string property, object value) {
        Type t = typeof(PSqlConfig).Assembly.GetType(entityFN);
        if (t == null) return null;
        var p = t.GetProperty(property);
        return GetInvalidValues(t, p, value);
    }
    #endregion Validation

    #region Query* Methods
    ///// <summary>Return LinQ Queryable for entity type</summary>
    ///// <typeparam name="T">Entity Type</typeparam>
    //public IQueryable QueryAll<T>()
    //{
    //    var rval = from r in LinQ<T>()
    //               select r;
    //    return rval;
    //}
    ///// <summary>Return entity list matching specified property-value combination</summary>
    ///// <param name="property">Entity property to be matched</param>
    ///// <param name="value">Property value to be matched</param>
    //public IList GetByField(System.Reflection.PropertyInfo property, object value)
    //{
    //    if (property == null) return null;
    //    return CreateCriteria(property.DeclaringType).Add(Restrictions.Eq(property.Name, value)).List();
    //}
    /// <summary>Return LinQ Queryable for Entity type</summary>
    /// <typeparam name="T">Entity Type</typeparam>
    public IQueryable<T> LinQ<T>(Expression<Func<T, bool>> exp = null) {
        IQueryable<T> rval = (new Linq.PSqlQueryable<T>(this)) as IQueryable<T>;
        return (exp == null) ? rval : rval.Where(exp);
    }
    #endregion Query* Methods
}