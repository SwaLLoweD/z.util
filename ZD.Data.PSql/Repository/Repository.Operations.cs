/* Copyright (c) <2008> <A. Zafer YURDA�ALI�>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Z.Data.PSql.Mapping;
using Z.Extensions;

namespace Z.Data.PSql;

public partial class Repo
{
    #region Save/Delete
    private string SelectSameEntityWhereClause<T>(T entity, IDbCommand cmd) {
        var idProps = Config.GetMappedIdProperties<T>();
        var sb = new StringBuilder();
        //sb.AppendString("SELECT * FROM ", typeof(T).Name, " WHERE ");
        var i = 0;
        foreach (var idProp in idProps) {
            sb.AppendString(string.Format(Config.FieldQuote, idProp.Column), $" = @Id{i} AND");
            cmd.ParameterAdd($"@Id{i}", idProp.GetDbValueForEntity(entity));
            i++;
        }

        sb.Remove(sb.Length - 4, 4);
        return sb.ToString();
    }

    /// <inheritdoc/>
    public void Refresh<T>(params T[] entities) {
        foreach (var e in entities) {
            if (e == null) continue;
            var newObj = Reload(e);
            if (newObj == null) continue;
            newObj.Populate(e);
        }
    }
    /// <summary>Reloads specified entities from db</summary>
    public T Reload<T>(T entity) {
        if (entity == null) return default;
        using (var cmd = CreateCommand()) {
            var sb = new StringBuilder();
            sb.AppendString("SELECT * FROM ", Config.GetMappedTableName<T>(), " WHERE ");
            sb.AppendString(SelectSameEntityWhereClause(entity, cmd));
            cmd.CommandText = sb.ToString();
            var obj = Get<T>().All(cmd).FirstOrDefault();
            if (obj != null) return obj;
        }
        return default;
    }
    /// <inheritdoc/>
    public void UpdateMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) {
        var rval = 0;
        foreach (var entity in entities) {
            if (entity == null) continue;
            //var dbEntity = Reload<T>(entity);
            using var cmd = CreateCommand();
            var sb = new StringBuilder();
            sb.AppendString("UPDATE ", Config.GetMappedTableName<T>(), " SET ");
            var dirtyProps = GetDirtyProperties(entity).ToArray();
            if (dirtyProps.Length == 0) continue;  //Nothing to update
            for (int i = 0; i < dirtyProps.Length; i++) {
                var prop = dirtyProps[i];
                if (!prop.Property.CanRead) continue;
                if (prop is PropertyIdMap propertyIdMap && !(propertyIdMap).Manual) continue; //Skip auto id properties
                var curValue = prop.Property.GetValue(entity, null);
                sb.AppendString(" ", string.Format(Config.FieldQuote, prop.Column), $" = @V{i}, ");
                cmd.ParameterAdd($"@V{i}", PropertyMap.GetDbValue(curValue), prop.DbType);
            }
            sb.Remove(sb.Length - 2, 2);
            sb.AppendString(" WHERE ", SelectSameEntityWhereClause(entity, cmd));
            cmd.CommandText = sb.ToString();
            rval += cmd.ExecuteNonQuery();
        }
        //return rval;
    }
    /// <inheritdoc/>
    public void Update<T>(T entity, bool commit = true) => UpdateMany<T>(new T[] { entity }, commit);

    /// <inheritdoc/>
    public ICollection<object> SaveMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) {
        var rval = 0;
        foreach (var entity in entities) {
            if (entity == null) continue;
            using var cmd = CreateCommand();
            var sb = new StringBuilder();
            sb.AppendString();
            var lstNames = new List<string>();
            var lstValues = new List<string>();
            var typeProps = Config.GetMappedProperties<T>().ToArray();
            for (int i = 0; i < typeProps.Length; i++) {
                var prop = typeProps[i];
                if (!prop.Property.CanRead) continue;
                if (prop is PropertyIdMap propertyIdMap && !propertyIdMap.Manual) continue;
                lstNames.Add(string.Format(Config.FieldQuote, prop.Column));
                lstValues.Add($"@V{i}");
                cmd.ParameterAdd($"@V{i}", prop.GetDbValueForEntity(entity) ?? "NULL");
            }
            cmd.CommandText = $"INSERT INTO {Config.GetMappedTableName<T>()} ({lstNames.To().CSV(", ")}) VALUES ({lstValues.To().CSV(", ")})";
            SavePostIdFetch(entity, cmd);
            rval++;
        }
        return null;
    }
    /// <inheritdoc/>
    public ICollection<object> Save<T>(T entity, bool commit = true) { return SaveMany<T>(new T[] { entity }, commit); }

    #region savePostIdFetch
    private void SavePostIdFetch<T>(T entity, IDbCommand cmd) {
        object id = null;
        var pk = Config.GetMappedIdProperties<T>().FirstOrDefault(x => !x.Manual);
        if (pk == null) {
            //No auto increment field
            cmd.ExecuteNonQuery();
            return;
        }
        switch (Config.ConnectionType) {
            case DBConTypes.SqlServerCE:
                cmd.ExecuteNonQuery();
                id = Execute("SELECT @@@IDENTITY AS NewID;");
                break;

            case DBConTypes.SqlServer:
                cmd.CommandText += ";\nSELECT SCOPE_IDENTITY() AS NewID;";
                id = cmd.ExecuteScalar();
                break;

            case DBConTypes.PostgreSQL:

                if (pk != null) {
                    cmd.CommandText += $" returning {pk.Column} as NewID";
                    id = cmd.ExecuteScalar();
                } else {
                    id = -1;
                    cmd.ExecuteNonQuery();
                }
                break;

            case DBConTypes.Oracle:
                if (pk != null) {
                    cmd.CommandText += $" returning {pk.Column} into :newid";
                    var param = cmd.CreateParameter();
                    param.ParameterName = ":newid";
                    param.Value = DBNull.Value;
                    param.Direction = ParameterDirection.ReturnValue;
                    param.DbType = DbType.Int64;
                    cmd.Parameters.Add(param);
                    cmd.ExecuteNonQuery();
                    id = param.Value;
                } else {
                    id = -1;
                    cmd.ExecuteNonQuery();
                }
                break;

            case DBConTypes.SQLite:
                if (pk != null) {
                    cmd.CommandText += ";\nSELECT last_insert_rowid();";
                    id = cmd.ExecuteScalar();
                } else {
                    id = -1;
                    cmd.ExecuteNonQuery();
                }
                break;

            default:
                cmd.CommandText += ";\nSELECT @@IDENTITY AS NewID;";
                id = cmd.ExecuteScalar();
                break;
        }

        if (id != null) pk.Property.SetValue(entity, id.To().Type(pk.Property.PropertyType, null, null));
    }
    #endregion savePostIdFetch

    /// <inheritdoc/>
    public void SaveOrUpdateMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) {
        foreach (var entity in entities) {
            if (entity == null) continue;
            if (Reload(entity) != null) UpdateMany(entities, commit, batchSize);
            else SaveMany(entities, commit, batchSize);
        }
    }
    /// <inheritdoc/>
    public void SaveOrUpdate<T>(T entity, bool commit = true) { SaveOrUpdateMany<T>(new T[] { entity }, commit); }

    /// <inheritdoc/>
    public void DeleteMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) {
        var rval = 0;
        foreach (var entity in entities) {
            if (entity == null) continue;
            using var cmd = CreateCommand();
            var sql = $"DELETE FROM {Config.GetMappedTableName<T>()} WHERE {SelectSameEntityWhereClause(entity, cmd)}";
            cmd.CommandText = sql;
            rval += cmd.ExecuteNonQuery();
        }
        //return rval;
    }
    /// <inheritdoc/>
    public void Delete<T>(T entity, bool commit = true) => DeleteMany<T>(new T[] { entity }, commit);
    #endregion Save/Delete
}