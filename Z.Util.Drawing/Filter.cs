﻿/*
Copyright (c) 2012 <a href="http://www.gutgames.com">James Craig</a>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

using System;
using System.Drawing;
using System.Threading.Tasks;
using Z.Extensions;

namespace Z.Util.ImageProcessing;

/// <summary>Used when applying convolution filters to an image</summary>
public class Filter {
    #region Constructors
    #region Constructor
    /// <summary>Constructor</summary>
    /// <param name="Width">Width</param>
    /// <param name="Height">Height</param>
    public Filter(int Width = 3, int Height = 3) {
        MyFilter = new int[Width, Height];
        this.Width = Width;
        this.Height = Height;
        Offset = 0;
        Absolute = false;
    }
    #endregion Constructor
    #endregion

    #region Public Functions
    /// <summary>Applies the filter to the input image</summary>
    /// <param name="Input">input image</param>
    /// <returns>Returns a separate image with the filter applied</returns>
    public virtual Bitmap ApplyFilter(Bitmap Input) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Input.ThrowIfNull("Input");
        var NewBitmap = new Bitmap(Input.Width, Input.Height);
        var NewData = NewBitmap.LockImage();
        var OldData = Input.LockImage();
        var NewPixelSize = NewData.GetPixelSize();
        var OldPixelSize = OldData.GetPixelSize();
        var Width2 = Input.Width;
        var Height2 = Input.Height;
        Parallel.For(0, Width2, x => {
            for (var y = 0; y < Height2; ++y) {
                var RValue = 0;
                var GValue = 0;
                var BValue = 0;
                var Weight = 0;
                var XCurrent = -Width / 2;
                for (var x2 = 0; x2 < Width; ++x2) {
                    if (XCurrent + x < Width2 && XCurrent + x >= 0) {
                        var YCurrent = -Height / 2;
                        for (var y2 = 0; y2 < Height; ++y2) {
                            if (YCurrent + y < Height2 && YCurrent + y >= 0) {
                                var Pixel = OldData.GetPixel(XCurrent + x, YCurrent + y, OldPixelSize);
                                RValue += MyFilter[x2, y2] * Pixel.R;
                                GValue += MyFilter[x2, y2] * Pixel.G;
                                BValue += MyFilter[x2, y2] * Pixel.B;
                                Weight += MyFilter[x2, y2];
                            }
                            ++YCurrent;
                        }
                    }
                    ++XCurrent;
                }
                var MeanPixel = OldData.GetPixel(x, y, OldPixelSize);
                if (Weight == 0) Weight = 1;
                if (Weight > 0) {
                    if (Absolute) {
                        RValue = System.Math.Abs(RValue);
                        GValue = System.Math.Abs(GValue);
                        BValue = System.Math.Abs(BValue);
                    }
                    RValue = (RValue / Weight) + Offset;
                    RValue = RValue.Clamp(255, 0);
                    GValue = (GValue / Weight) + Offset;
                    GValue = GValue.Clamp(255, 0);
                    BValue = (BValue / Weight) + Offset;
                    BValue = BValue.Clamp(255, 0);
                    MeanPixel = Color.FromArgb(RValue, GValue, BValue);
                }
                NewData.SetPixel(x, y, MeanPixel, NewPixelSize);
            }
        });
        NewBitmap.UnlockImage(NewData);
        Input.UnlockImage(OldData);
        return NewBitmap;
    }
    #endregion Public Functions

    #region Public Properties
    /// <summary>The actual filter array</summary>
    public virtual int[,] MyFilter { get; set; }

    /// <summary>Width of the filter box</summary>
    public virtual int Width { get; set; }

    /// <summary>Height of the filter box</summary>
    public virtual int Height { get; set; }

    /// <summary>Amount to add to the red, blue, and green values</summary>
    public virtual int Offset { get; set; }

    /// <summary>Determines if we should take the absolute value prior to clamping</summary>
    public virtual bool Absolute { get; set; }
    #endregion Public Properties
}