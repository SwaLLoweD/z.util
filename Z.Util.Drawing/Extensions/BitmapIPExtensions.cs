﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading.Tasks;
using Z.Util.ImageProcessing;
using Z.Util.ImageProcessing.Procedural;

namespace Z.Extensions;

/// <summary>Extension methods for the System.Drawing.Bitmap class</summary>
public static partial class ExtIPBitmap {
    #region Constants / Static Fields
    #region Variables
    /// <summary>Characters used for ASCII art</summary>
    private static readonly string[] _ASCIICharacters = { "#", "#", "@", "%", "=", "+", "*", ":", "-", ".", " " };
    #endregion Variables
    #endregion

    #region BitmapProcess
    /// <summary>Adjusts the brightness</summary>
    /// <param name="process"></param>
    /// <param name="Value">-255 to 255</param>
    /// <returns>A bitmap object</returns>
    public static Bitmap AdjustBrightness(this ExtBitmap.BitmapProcessPortion process, int Value = 0) {
        //Image.ThrowIfNull("Image");
        var FinalValue = (float)Value / 255.0f;
        var TempMatrix = new ColorMatrix2 {
            Matrix = new float[][] {
                new float[] {1, 0, 0, 0, 0},
                new float[] {0, 1, 0, 0, 0},
                new float[] {0, 0, 1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {FinalValue, FinalValue, FinalValue, 1, 1}
            }
        };
        var NewBitmap = TempMatrix.Apply(process.Image);
        return NewBitmap;
    }

    /// <summary>Converts an image to black and white</summary>
    /// <returns>A bitmap object of the black and white image</returns>
    public static Bitmap BlackAndWhite(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempMatrix = new ColorMatrix2 {
            Matrix = new float[][] {
                new float[] {.3f, .3f, .3f, 0, 0},
                new float[] {.59f, .59f, .59f, 0, 0},
                new float[] {.11f, .11f, .11f, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            }
        };
        var NewBitmap = TempMatrix.Apply(process.Image);
        return NewBitmap;
    }

    /// <summary>Gets the blue filter for an image</summary>
    /// <returns>A bitmap object</returns>
    public static Bitmap BlueFilter(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempMatrix = new ColorMatrix2 {
            Matrix = new float[][] {
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            }
        };
        var NewBitmap = TempMatrix.Apply(process.Image);
        return NewBitmap;
    }

    /// <summary>Does smoothing using a box blur</summary>
    /// <param name="process"></param>
    /// <param name="Size">Size of the aperture</param>
    /// <returns>A bitmap object</returns>
    public static Bitmap BoxBlur(this ExtBitmap.BitmapProcessPortion process, int Size = 3) {
        //Image.ThrowIfNull("Image");
        var TempFilter = new Filter(Size, Size);
        for (var x = 0; x < Size; ++x) {
            for (var y = 0; y < Size; ++y)
                TempFilter.MyFilter[x, y] = 1;
        }

        var NewBitmap = TempFilter.ApplyFilter(process.Image);
        return NewBitmap;
    }

    /// <summary>Creates the bump map</summary>
    /// <param name="process"></param>
    /// <param name="Direction">Direction of the bump map</param>
    /// <param name="Invert">Inverts the direction of the bump map</param>
    /// <returns>The resulting bump map</returns>
    public static Bitmap BumpMap(this ExtBitmap.BitmapProcessPortion process, ExtBitmap.Direction Direction = ExtBitmap.Direction.TopBottom, bool Invert = false) {
        //Image.ThrowIfNull("Image");
        var EdgeDetectionFilter = new Filter(3, 3);
        if (Direction == ExtBitmap.Direction.TopBottom) {
            if (!Invert) {
                EdgeDetectionFilter.MyFilter[0, 0] = 1;
                EdgeDetectionFilter.MyFilter[1, 0] = 2;
                EdgeDetectionFilter.MyFilter[2, 0] = 1;
                EdgeDetectionFilter.MyFilter[0, 1] = 0;
                EdgeDetectionFilter.MyFilter[1, 1] = 0;
                EdgeDetectionFilter.MyFilter[2, 1] = 0;
                EdgeDetectionFilter.MyFilter[0, 2] = -1;
                EdgeDetectionFilter.MyFilter[1, 2] = -2;
                EdgeDetectionFilter.MyFilter[2, 2] = -1;
            } else {
                EdgeDetectionFilter.MyFilter[0, 0] = -1;
                EdgeDetectionFilter.MyFilter[1, 0] = -2;
                EdgeDetectionFilter.MyFilter[2, 0] = -1;
                EdgeDetectionFilter.MyFilter[0, 1] = 0;
                EdgeDetectionFilter.MyFilter[1, 1] = 0;
                EdgeDetectionFilter.MyFilter[2, 1] = 0;
                EdgeDetectionFilter.MyFilter[0, 2] = 1;
                EdgeDetectionFilter.MyFilter[1, 2] = 2;
                EdgeDetectionFilter.MyFilter[2, 2] = 1;
            }
        } else {
            if (!Invert) {
                EdgeDetectionFilter.MyFilter[0, 0] = -1;
                EdgeDetectionFilter.MyFilter[0, 1] = -2;
                EdgeDetectionFilter.MyFilter[0, 2] = -1;
                EdgeDetectionFilter.MyFilter[1, 0] = 0;
                EdgeDetectionFilter.MyFilter[1, 1] = 0;
                EdgeDetectionFilter.MyFilter[1, 2] = 0;
                EdgeDetectionFilter.MyFilter[2, 0] = 1;
                EdgeDetectionFilter.MyFilter[2, 1] = 2;
                EdgeDetectionFilter.MyFilter[2, 2] = 1;
            } else {
                EdgeDetectionFilter.MyFilter[0, 0] = 1;
                EdgeDetectionFilter.MyFilter[0, 1] = 2;
                EdgeDetectionFilter.MyFilter[0, 2] = 1;
                EdgeDetectionFilter.MyFilter[1, 0] = 0;
                EdgeDetectionFilter.MyFilter[1, 1] = 0;
                EdgeDetectionFilter.MyFilter[1, 2] = 0;
                EdgeDetectionFilter.MyFilter[2, 0] = -1;
                EdgeDetectionFilter.MyFilter[2, 1] = -2;
                EdgeDetectionFilter.MyFilter[2, 2] = -1;
            }
        }
        EdgeDetectionFilter.Offset = 127;
        using var TempImage = EdgeDetectionFilter.ApplyFilter(process.Image);
        return TempImage.Process().BlackAndWhite();
    }

    /// <summary>Emboss function</summary>
    /// <returns>A bitmap image</returns>
    public static Bitmap Emboss(this ExtBitmap.BitmapProcessPortion process) {
        //if (Image == null) throw new ArgumentNullException("Image");
        var TempFilter = new Filter(3, 3);
        TempFilter.MyFilter[0, 0] = -2;
        TempFilter.MyFilter[0, 1] = -1;
        TempFilter.MyFilter[1, 0] = -1;
        TempFilter.MyFilter[1, 1] = 1;
        TempFilter.MyFilter[2, 1] = 1;
        TempFilter.MyFilter[1, 2] = 1;
        TempFilter.MyFilter[2, 2] = 2;
        TempFilter.MyFilter[0, 2] = 0;
        TempFilter.MyFilter[2, 0] = 0;
        var NewBitmap = TempFilter.ApplyFilter(process.Image);
        return NewBitmap;
    }

    /// <summary>Uses an RGB histogram to equalize the image</summary>
    /// <returns>The resulting bitmap image</returns>
    public static Bitmap Equalize(this ExtBitmap.BitmapProcessPortion process) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //OriginalImage.ThrowIfNull("OriginalImage");
        var NewBitmap = new Bitmap(process.Image.Width, process.Image.Height);
        var TempHistogram = new RGBHistogram(process.Image);
        TempHistogram.Equalize();
        var NewData = NewBitmap.LockImage();
        var OldData = process.Image.LockImage();
        var NewPixelSize = NewData.GetPixelSize();
        var OldPixelSize = OldData.GetPixelSize();
        var Width = NewBitmap.Width;
        var Height = NewBitmap.Height;
        Parallel.For(0, Width, x => {
            for (var y = 0; y < Height; ++y) {
                var Current = OldData.GetPixel(x, y, OldPixelSize);
                var NewR = (int)TempHistogram.R[Current.R];
                var NewG = (int)TempHistogram.G[Current.G];
                var NewB = (int)TempHistogram.B[Current.B];
                NewR = NewR.Clamp(255, 0);
                NewG = NewG.Clamp(255, 0);
                NewB = NewB.Clamp(255, 0);
                NewData.SetPixel(x, y, Color.FromArgb(NewR, NewG, NewB), NewPixelSize);
            }
        });
        NewBitmap.UnlockImage(NewData);
        process.Image.UnlockImage(OldData);
        return NewBitmap;
    }
    /// <summary>Does smoothing using a gaussian blur</summary>
    /// <param name="process"></param>
    /// <param name="Size">Size of the aperture</param>
    /// <returns>The resulting bitmap</returns>
    public static Bitmap GaussianBlur(this ExtBitmap.BitmapProcessPortion process, int Size = 3) {
        //Image.ThrowIfNull("Image");
        using var ReturnBitmap = process.Image.Process().BoxBlur(Size);
        using var ReturnBitmap2 = ReturnBitmap.Process().BoxBlur(Size);
        var ReturnBitmap3 = ReturnBitmap2.Process().BoxBlur(Size);
        return ReturnBitmap3;
    }

    /// <summary>Gets the Green filter for an image</summary>
    /// <returns>A bitmap object</returns>
    public static Bitmap GreenFilter(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempMatrix = new ColorMatrix2 {
            Matrix = new float[][] {
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 1, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            }
        };
        var NewBitmap = TempMatrix.Apply(process.Image);
        return NewBitmap;
    }

    /// <summary>Laplace edge detection function</summary>
    /// <returns>A bitmap object</returns>
    public static Bitmap LaplaceEdgeDetection(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        using var TempImage = process.Image.Process().BlackAndWhite();
        var TempFilter = new Filter(5, 5);
        TempFilter.MyFilter[0, 0] = -1;
        TempFilter.MyFilter[0, 1] = -1;
        TempFilter.MyFilter[0, 2] = -1;
        TempFilter.MyFilter[0, 3] = -1;
        TempFilter.MyFilter[0, 4] = -1;
        TempFilter.MyFilter[1, 0] = -1;
        TempFilter.MyFilter[1, 1] = -1;
        TempFilter.MyFilter[1, 2] = -1;
        TempFilter.MyFilter[1, 3] = -1;
        TempFilter.MyFilter[1, 4] = -1;
        TempFilter.MyFilter[2, 0] = -1;
        TempFilter.MyFilter[2, 1] = -1;
        TempFilter.MyFilter[2, 2] = 24;
        TempFilter.MyFilter[2, 3] = -1;
        TempFilter.MyFilter[2, 4] = -1;
        TempFilter.MyFilter[3, 0] = -1;
        TempFilter.MyFilter[3, 1] = -1;
        TempFilter.MyFilter[3, 2] = -1;
        TempFilter.MyFilter[3, 3] = -1;
        TempFilter.MyFilter[3, 4] = -1;
        TempFilter.MyFilter[4, 0] = -1;
        TempFilter.MyFilter[4, 1] = -1;
        TempFilter.MyFilter[4, 2] = -1;
        TempFilter.MyFilter[4, 3] = -1;
        TempFilter.MyFilter[4, 4] = -1;
        using var NewImage = TempFilter.ApplyFilter(TempImage);
        var NewBitmap = NewImage.Process().Negative();
        return NewBitmap;
    }

    /// <summary>Runs a simplistic motion detection algorithm</summary>
    /// <param name="process"></param>
    /// <param name="OldImage">The "old" frame</param>
    /// <param name="Threshold">The threshold used to detect changes in the image</param>
    /// <param name="DetectionColor">Color to display changes in the images as</param>
    /// <returns>A bitmap indicating where changes between frames have occurred overlayed on top of the new image.</returns>
    public static Bitmap MotionDetection(this ExtBitmap.BitmapProcessPortion process, Bitmap OldImage, int Threshold, Color DetectionColor) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //NewImage.ThrowIfNull("NewImage");
        //OldImage.ThrowIfNull("OldImage");
        //DetectionColor.ThrowIfNull("DetectionColor");
        using var NewImage1 = process.Image.Process().BlackAndWhite();
        using var OldImage1 = OldImage.Process().BlackAndWhite();
        using var NewImage2 = NewImage1.Process().SNNBlur(5);
        using var OldImage2 = OldImage1.Process().SNNBlur(5);
        using var OutputImage = new Bitmap(NewImage2, NewImage2.Width, NewImage2.Height);
        using var Overlay = new Bitmap(process.Image, process.Image.Width, process.Image.Height);
        var NewImage2Data = NewImage2.LockImage();
        var NewImage2PixelSize = NewImage2Data.GetPixelSize();
        var OldImage2Data = OldImage2.LockImage();
        var OldImage2PixelSize = OldImage2Data.GetPixelSize();
        var OverlayData = Overlay.LockImage();
        var OverlayPixelSize = OverlayData.GetPixelSize();
        var Width = OutputImage.Width;
        var Height = OutputImage.Height;
        Parallel.For(0, Width, x => {
            for (var y = 0; y < Height; ++y) {
                var NewPixel = NewImage2Data.GetPixel(x, y, NewImage2PixelSize);
                var OldPixel = OldImage2Data.GetPixel(x, y, OldImage2PixelSize);
                if (Math.Pow((double)(NewPixel.R - OldPixel.R), 2.0) > Threshold)
                    OverlayData.SetPixel(x, y, Color.FromArgb(100, 0, 100), OverlayPixelSize);
                else
                    OverlayData.SetPixel(x, y, Color.FromArgb(200, 0, 200), OverlayPixelSize);
            }
        });
        Overlay.UnlockImage(OverlayData);
        NewImage2.UnlockImage(NewImage2Data);
        OldImage2.UnlockImage(OldImage2Data);
        using var Overlay2 = Overlay.Process().EdgeDetection(25, DetectionColor);
        var Overlay2Data = Overlay2.LockImage();
        var Overlay2PixelSize = Overlay2Data.GetPixelSize();
        Width = OutputImage.Width;
        Height = OutputImage.Height;
        Parallel.For(0, Width, x => {
            for (var y = 0; y < Height; ++y) {
                var Pixel1 = Overlay2Data.GetPixel(x, y, Overlay2PixelSize);
                if (Pixel1.R != DetectionColor.R || Pixel1.G != DetectionColor.G || Pixel1.B != DetectionColor.B)
                    Overlay2Data.SetPixel(x, y, Color.FromArgb(200, 0, 200), Overlay2PixelSize);
            }
        });
        Overlay2.UnlockImage(Overlay2Data);
        return OutputImage.Process().Watermark(Overlay2, 1.0f, 0, 0, Color.FromArgb(200, 0, 200));
    }

    /// <summary>Creates the normal map</summary>
    /// <param name="process"></param>
    /// <param name="InvertX">Invert the X direction</param>
    /// <param name="InvertY">Invert the Y direction</param>
    /// <returns>Returns the resulting normal map</returns>
    public static Bitmap NormalMap(this ExtBitmap.BitmapProcessPortion process, bool InvertX = false, bool InvertY = false) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //ImageUsing.ThrowIfNull("ImageUsing");
        using var TempImageX = process.Image.Process().BumpMap(ExtBitmap.Direction.LeftRight, InvertX);
        using var TempImageY = process.Image.Process().BumpMap(ExtBitmap.Direction.TopBottom, InvertY);
        var ReturnImage = new Bitmap(TempImageX.Width, TempImageX.Height);
        var TempImageXData = TempImageX.LockImage();
        var TempImageYData = TempImageY.LockImage();
        var ReturnImageData = ReturnImage.LockImage();
        var TempImageXPixelSize = TempImageXData.GetPixelSize();
        var TempImageYPixelSize = TempImageYData.GetPixelSize();
        var ReturnImagePixelSize = ReturnImageData.GetPixelSize();
        var Width = TempImageX.Width;
        var Height = TempImageX.Height;
        Parallel.For(0, Height, y => {
            var TempVector = new Util.Mathematics.Vector3(0.0, 0.0, 0.0);
            for (var x = 0; x < Width; ++x) {
                var TempPixelX = TempImageXData.GetPixel(x, y, TempImageXPixelSize);
                var TempPixelY = TempImageYData.GetPixel(x, y, TempImageYPixelSize);
                TempVector.X = (double)TempPixelX.R / 255.0;
                TempVector.Y = (double)TempPixelY.R / 255.0;
                TempVector.Z = 1.0;
                TempVector.Normalize();
                TempVector.X = (TempVector.X + 1.0) / 2.0 * 255.0;
                TempVector.Y = (TempVector.Y + 1.0) / 2.0 * 255.0;
                TempVector.Z = (TempVector.Z + 1.0) / 2.0 * 255.0;
                ReturnImageData.SetPixel(x, y,
                    Color.FromArgb((int)TempVector.X,
                        (int)TempVector.Y,
                        (int)TempVector.Z),
                    ReturnImagePixelSize);
            }
        });
        TempImageX.UnlockImage(TempImageXData);
        TempImageY.UnlockImage(TempImageYData);
        ReturnImage.UnlockImage(ReturnImageData);
        return ReturnImage;
    }

    /// <summary>Slow but interesting function that applies an oil painting effect</summary>
    /// <param name="process"></param>
    /// <param name="Seed">Randomization seed</param>
    /// <param name="NumberOfPoints">Number of points for the painting</param>
    /// <returns>The resulting bitmap</returns>
    public static Bitmap OilPainting(this ExtBitmap.BitmapProcessPortion process, int Seed, int NumberOfPoints = 100) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Image.ThrowIfNull("Image");
        var _Image = new Bitmap(process.Image);
        var Map = new CellularMap(Seed, process.Image.Width, process.Image.Height, NumberOfPoints);
        var ImageData = _Image.LockImage();
        var ImagePixelSize = ImageData.GetPixelSize();
        var Width = _Image.Width;
        var Height = _Image.Height;
        Parallel.For(0, NumberOfPoints, i => {
            var Red = 0;
            var Green = 0;
            var Blue = 0;
            var Counter = 0;
            for (var x = 0; x < Width; ++x) {
                for (var y = 0; y < Height; ++y) {
                    if (Map.ClosestPoint[x, y] == i) {
                        var Pixel = ImageData.GetPixel(x, y, ImagePixelSize);
                        Red += Pixel.R;
                        Green += Pixel.G;
                        Blue += Pixel.B;
                        ++Counter;
                    }
                }
            }

            var Counter2 = 0;
            for (var x = 0; x < Width; ++x) {
                for (var y = 0; y < Height; ++y) {
                    if (Map.ClosestPoint[x, y] == i) {
                        ImageData.SetPixel(x, y, Color.FromArgb(Red / Counter, Green / Counter, Blue / Counter), ImagePixelSize);
                        ++Counter2;
                        if (Counter2 == Counter) break;
                    }
                }

                if (Counter2 == Counter) break;
            }
        });
        _Image.UnlockImage(ImageData);
        return _Image;
    }

    /// <summary>Gets the Red filter for an image</summary>
    /// <returns>A bitmap image</returns>
    public static Bitmap RedFilter(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempMatrix = new ColorMatrix2 {
            Matrix = new float[][] {
                new float[] {1, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            }
        };
        var NewBitmap = TempMatrix.Apply(process.Image);
        return NewBitmap;
    }

    /// <summary>Converts an image to sepia tone</summary>
    /// <returns>A bitmap object of the sepia tone image</returns>
    public static Bitmap SepiaTone(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempMatrix = new ColorMatrix2 {
            Matrix = new float[][] {
                new float[] {.393f, .349f, .272f, 0, 0},
                new float[] {.769f, .686f, .534f, 0, 0},
                new float[] {.189f, .168f, .131f, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            }
        };
        var NewBitmap = TempMatrix.Apply(process.Image);
        return NewBitmap;
    }

    /// <summary>Sharpens an image</summary>
    /// <returns>A bitmap image</returns>
    public static Bitmap Sharpen(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempFilter = new Filter(3, 3);
        TempFilter.MyFilter[0, 0] = -1;
        TempFilter.MyFilter[0, 2] = -1;
        TempFilter.MyFilter[2, 0] = -1;
        TempFilter.MyFilter[2, 2] = -1;
        TempFilter.MyFilter[0, 1] = -2;
        TempFilter.MyFilter[1, 0] = -2;
        TempFilter.MyFilter[2, 1] = -2;
        TempFilter.MyFilter[1, 2] = -2;
        TempFilter.MyFilter[1, 1] = 16;
        var NewBitmap = TempFilter.ApplyFilter(process.Image);
        return NewBitmap;
    }

    /// <summary>Sharpens an image</summary>
    /// <returns>A bitmap image</returns>
    public static Bitmap SharpenLess(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempFilter = new Filter(3, 3);
        TempFilter.MyFilter[0, 0] = -1;
        TempFilter.MyFilter[0, 1] = 0;
        TempFilter.MyFilter[0, 2] = -1;
        TempFilter.MyFilter[1, 0] = 0;
        TempFilter.MyFilter[1, 1] = 7;
        TempFilter.MyFilter[1, 2] = 0;
        TempFilter.MyFilter[2, 0] = -1;
        TempFilter.MyFilter[2, 1] = 0;
        TempFilter.MyFilter[2, 2] = -1;
        var NewBitmap = TempFilter.ApplyFilter(process.Image);
        return NewBitmap;
    }

    /// <summary>Sobel edge detection function</summary>
    /// <returns>A bitmap image</returns>
    public static Bitmap SobelEdgeDetection(this ExtBitmap.BitmapProcessPortion process) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Input.ThrowIfNull("Input");
        using var TempImage = process.Image.Process().BlackAndWhite();
        var TempFilter = new Filter(3, 3);
        TempFilter.MyFilter[0, 0] = -1;
        TempFilter.MyFilter[0, 1] = 0;
        TempFilter.MyFilter[0, 2] = 1;
        TempFilter.MyFilter[1, 0] = -2;
        TempFilter.MyFilter[1, 1] = 0;
        TempFilter.MyFilter[1, 2] = 2;
        TempFilter.MyFilter[2, 0] = -1;
        TempFilter.MyFilter[2, 1] = 0;
        TempFilter.MyFilter[2, 2] = 1;
        TempFilter.Absolute = true;
        using var TempImageX = TempFilter.ApplyFilter(TempImage);
        TempFilter = new Filter(3, 3);
        TempFilter.MyFilter[0, 0] = 1;
        TempFilter.MyFilter[0, 1] = 2;
        TempFilter.MyFilter[0, 2] = 1;
        TempFilter.MyFilter[1, 0] = 0;
        TempFilter.MyFilter[1, 1] = 0;
        TempFilter.MyFilter[1, 2] = 0;
        TempFilter.MyFilter[2, 0] = -1;
        TempFilter.MyFilter[2, 1] = -2;
        TempFilter.MyFilter[2, 2] = -1;
        TempFilter.Absolute = true;
        using var TempImageY = TempFilter.ApplyFilter(TempImage);
        using var NewBitmap = new Bitmap(TempImage.Width, TempImage.Height);
        var NewData = NewBitmap.LockImage();
        var OldData1 = TempImageX.LockImage();
        var OldData2 = TempImageY.LockImage();
        var NewPixelSize = NewData.GetPixelSize();
        var OldPixelSize1 = OldData1.GetPixelSize();
        var OldPixelSize2 = OldData2.GetPixelSize();
        var Width = NewBitmap.Width;
        var Height = NewBitmap.Height;
        Parallel.For(0, Width, x => {
            for (var y = 0; y < Height; ++y) {
                var Pixel1 = OldData1.GetPixel(x, y, OldPixelSize1);
                var Pixel2 = OldData2.GetPixel(x, y, OldPixelSize2);
                NewData.SetPixel(x, y,
                    Color.FromArgb((Pixel1.R + Pixel2.R).Clamp(255, 0),
                        (Pixel1.G + Pixel2.G).Clamp(255, 0),
                        (Pixel1.B + Pixel2.B).Clamp(255, 0)),
                    NewPixelSize);
            }
        });
        NewBitmap.UnlockImage(NewData);
        TempImageX.UnlockImage(OldData1);
        TempImageY.UnlockImage(OldData2);
        var NewBitmap2 = NewBitmap.Process().Negative();
        return NewBitmap2;
    }

    /// <summary>Sobel emboss function</summary>
    /// <returns>A bitmap image</returns>
    public static Bitmap SobelEmboss(this ExtBitmap.BitmapProcessPortion process) {
        //Image.ThrowIfNull("Image");
        var TempFilter = new Filter(3, 3);
        TempFilter.MyFilter[0, 0] = -1;
        TempFilter.MyFilter[0, 1] = 0;
        TempFilter.MyFilter[0, 2] = 1;
        TempFilter.MyFilter[1, 0] = -2;
        TempFilter.MyFilter[1, 1] = 0;
        TempFilter.MyFilter[1, 2] = 2;
        TempFilter.MyFilter[2, 0] = -1;
        TempFilter.MyFilter[2, 1] = 0;
        TempFilter.MyFilter[2, 2] = 1;
        TempFilter.Offset = 127;
        var NewBitmap = TempFilter.ApplyFilter(process.Image);
        return NewBitmap;
    }

    /// <summary>Converts an image to ASCII art</summary>
    /// <returns>A string containing the art</returns>
    public static string ToASCIIArt(this ExtBitmap.BitmapProcessPortion process) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        var ShowLine = true;
        using var TempImage = process.Image.Process().BlackAndWhite();
        var OldData = TempImage.LockImage();
        var OldPixelSize = OldData.GetPixelSize();
        var Builder = new StringBuilder();
        for (var x = 0; x < TempImage.Height; ++x) {
            for (var y = 0; y < TempImage.Width; ++y) {
                if (ShowLine) {
                    var CurrentPixel = OldData.GetPixel(y, x, OldPixelSize);
                    Builder.Append(_ASCIICharacters[CurrentPixel.R * _ASCIICharacters.Length / 255]);
                }
            }

            if (ShowLine) {
                Builder.Append(Environment.NewLine);
                ShowLine = false;
            } else {
                ShowLine = true;
            }
        }
        TempImage.UnlockImage(OldData);
        return Builder.ToString();
    }

    /// <summary>Does turbulence manipulation of the image</summary>
    /// <param name="process"></param>
    /// <param name="Roughness">Roughness of the movement</param>
    /// <param name="Power">How strong the movement is</param>
    /// <param name="Seed">Random seed</param>
    /// <returns>A bitmap object containing the new image</returns>
    public static Bitmap Turbulence(this ExtBitmap.BitmapProcessPortion process, int Roughness = 8, float Power = 5.0f, int Seed = 25123864) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //OriginalImage.ThrowIfNull("OriginalImage");
        var Width = process.Image.Width;
        var Height = process.Image.Height;
        var OriginalData = process.Image.LockImage();
        var OriginalPixelSize = OriginalData.GetPixelSize();
        var NewBitmap = new Bitmap(Width, Height);
        var ReturnData = NewBitmap.LockImage();
        var ReturnPixelSize = ReturnData.GetPixelSize();
        using (var XNoise = PerlinNoise.Generate(Width, Height, 255, 0, 0.0625f, 1.0f, 0.5f, Roughness, Seed)) {
            var XNoiseData = XNoise.LockImage();
            var XNoisePixelSize = XNoiseData.GetPixelSize();
            using (var YNoise = PerlinNoise.Generate(Width, Height, 255, 0, 0.0625f, 1.0f, 0.5f, Roughness, Seed * 2)) {
                var YNoiseData = YNoise.LockImage();
                var YNoisePixelSize = YNoiseData.GetPixelSize();
                Parallel.For(0, Height, y => {
                    for (var x = 0; x < Width; ++x) {
                        var XDistortion = x + (GetHeight(x, y, XNoiseData, XNoisePixelSize) * Power);
                        var YDistortion = y + (GetHeight(x, y, YNoiseData, YNoisePixelSize) * Power);
                        var X1 = ((int)XDistortion).Clamp(Width - 1, 0);
                        var Y1 = ((int)YDistortion).Clamp(Height - 1, 0);
                        ReturnData.SetPixel(x, y, OriginalData.GetPixel(X1, Y1, OriginalPixelSize), ReturnPixelSize);
                    }
                });
                YNoise.UnlockImage(YNoiseData);
            }
            XNoise.UnlockImage(XNoiseData);
        }
        NewBitmap.UnlockImage(ReturnData);
        process.Image.UnlockImage(OriginalData);
        return NewBitmap;
    }
    #endregion BitmapProcess

    #region GetHeight
    internal static float GetHeight(int x, int y, BitmapData BlackAndWhiteData, int BlackAndWhitePixelSize) {
        //BlackAndWhiteData.ThrowIfNull("BlackAndWhiteData");
        var TempColor = BlackAndWhiteData.GetPixel(x, y, BlackAndWhitePixelSize);
        return GetHeight(TempColor);
    }

    //Color.ThrowIfNull("Color");
    internal static float GetHeight(Color Color) => (float)Color.R / 255.0f;
    #endregion GetHeight
}