﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace Z.Extensions;

/// <summary>Extension methods for the System.Drawing.Bitmap class</summary>
public static partial class ExtBitmap {
    #region BitmapProcess
    /// <summary>Processing Options part for Bitmap extensions</summary>
    public class BitmapProcessPortion {
        #region Fields / Properties
        /// <summary>Bitmap to be processed</summary>
        public Bitmap Image { get; }
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public BitmapProcessPortion(Bitmap image) => Image = image;
        #endregion

        /// <summary>adds noise to the image</summary>
        /// <param name="Amount">Amount of noise to add (defaults to 10)</param>
        /// <returns>New image object with the noise added</returns>
        public Bitmap AddNoise(int Amount = 10) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var Height = NewBitmap.Height;
            var Width = NewBitmap.Width;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var CurrentPixel = OldData.GetPixel(x, y, OldPixelSize);
                    var R = CurrentPixel.R + new Random().Next(-Amount, Amount + 1); //Can be attached to global seed with thread safety for better results
                    var G = CurrentPixel.G + new Random().Next(-Amount, Amount + 1);
                    var B = CurrentPixel.B + new Random().Next(-Amount, Amount + 1);
                    R = R.Clamp(255, 0);
                    G = G.Clamp(255, 0);
                    B = B.Clamp(255, 0);
                    var TempValue = Color.FromArgb(R, G, B);
                    NewData.SetPixel(x, y, TempValue, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Adjusts the Contrast</summary>
        /// <param name="Value">Used to set the contrast (-100 to 100)</param>
        /// <returns>A bitmap object</returns>
        public Bitmap AdjustContrast(float Value = 0) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            Value = (100.0f + Value) / 100.0f;
            Value *= Value;
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;

            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var Pixel = OldData.GetPixel(x, y, OldPixelSize);
                    var Red = Pixel.R / 255.0f;
                    var Green = Pixel.G / 255.0f;
                    var Blue = Pixel.B / 255.0f;
                    Red = (((Red - 0.5f) * Value) + 0.5f) * 255.0f;
                    Green = (((Green - 0.5f) * Value) + 0.5f) * 255.0f;
                    Blue = (((Blue - 0.5f) * Value) + 0.5f) * 255.0f;
                    NewData.SetPixel(x, y,
                        Color.FromArgb(((int)Red).Clamp(255, 0),
                            ((int)Green).Clamp(255, 0),
                            ((int)Blue).Clamp(255, 0)),
                        NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Adjusts the Gamma</summary>
        /// <param name="Value">Used to build the gamma ramp (usually .2 to 5)</param>
        /// <returns>A bitmap object</returns>
        public Bitmap AdjustGamma(float Value = 1.0f) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();

            var Ramp = new int[256];
            Parallel.For(0, 256, x => Ramp[x] = ((int)((255.0 * Math.Pow(x / 255.0, 1.0 / Value)) + 0.5)).Clamp(255, 0));
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;

            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var Pixel = OldData.GetPixel(x, y, OldPixelSize);
                    var Red = Ramp[Pixel.R];
                    var Green = Ramp[Pixel.G];
                    var Blue = Ramp[Pixel.B];
                    NewData.SetPixel(x, y, Color.FromArgb(Red, Green, Blue), NewPixelSize);
                }
            });

            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Colorizes a black and white image</summary>
        /// <param name="Colors">Color array to use for the image</param>
        /// <returns>The colorized image</returns>
        public Bitmap Colorize(Color[] Colors) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            if (Colors.Length < 256) return new Bitmap(1, 1);
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var Width = Image.Width;
            var Height = Image.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    int ColorUsing = OldData.GetPixel(x, y, OldPixelSize).R;
                    NewData.SetPixel(x, y, Colors[ColorUsing], NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Crops an image</summary>
        /// <param name="Width">Width of the cropped image</param>
        /// <param name="Height">Height of the cropped image</param>
        /// <param name="VAlignment">The verticle alignment of the cropping (top or bottom)</param>
        /// <param name="HAlignment">The horizontal alignment of the cropping (left or right)</param>
        /// <returns>A Bitmap object of the cropped image</returns>
        public Bitmap Crop(int Width, int Height, Align VAlignment, Align HAlignment) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //ImageUsing.ThrowIfNull("ImageUsing");
            var TempBitmap = Image;
            var TempRectangle = new Rectangle {
                Height = Height,
                Width = Width
            };
            if (VAlignment == Align.Top) {
                TempRectangle.Y = 0;
            } else {
                TempRectangle.Y = TempBitmap.Height - Height;
                if (TempRectangle.Y < 0) TempRectangle.Y = 0;
            }
            if (HAlignment == Align.Left) {
                TempRectangle.X = 0;
            } else {
                TempRectangle.X = TempBitmap.Width - Width;
                if (TempRectangle.X < 0) TempRectangle.X = 0;
            }
            var NewBitmap = TempBitmap.Clone(TempRectangle, TempBitmap.PixelFormat);
            return NewBitmap;
        }

        /// <summary>Does dilation</summary>
        /// <param name="Size">Size of the aperture</param>
        /// <returns>A Bitmap object of the resulting image</returns>
        public Bitmap Dilate(int Size) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var ApetureMin = -(Size / 2);
            var ApetureMax = Size / 2;
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var RValue = 0;
                    var GValue = 0;
                    var BValue = 0;
                    for (var x2 = ApetureMin; x2 < ApetureMax; ++x2) {
                        var TempX = x + x2;
                        if (TempX >= 0 && TempX < Width) {
                            for (var y2 = ApetureMin; y2 < ApetureMax; ++y2) {
                                var TempY = y + y2;
                                if (TempY >= 0 && TempY < Height) {
                                    var TempColor = OldData.GetPixel(TempX, TempY, OldPixelSize);
                                    RValue = RValue.Max(TempColor.R);
                                    GValue = GValue.Max(TempColor.G);
                                    BValue = BValue.Max(TempColor.B);
                                }
                            }
                        }
                    }
                    var TempPixel = Color.FromArgb(RValue, GValue, BValue);
                    NewData.SetPixel(x, y, TempPixel, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Draws a rounded rectangle on a bitmap</summary>
        /// <param name="BoxColor">The color that the box should be</param>
        /// <param name="XPosition">The upper right corner's x position</param>
        /// <param name="YPosition">The upper right corner's y position</param>
        /// <param name="Height">Height of the box</param>
        /// <param name="Width">Width of the box</param>
        /// <param name="CornerRadius">Radius of the corners</param>
        /// <returns>The bitmap with the rounded box on it</returns>
        public Bitmap DrawRoundedRectangle(Color BoxColor, int XPosition, int YPosition, int Height, int Width, int CornerRadius) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //Image.ThrowIfNull("Image");
            //BoxColor.ThrowIfNull("BoxColor");
            var NewBitmap = new Bitmap(Image, Image.Width, Image.Height);
            using (var NewGraphics = Graphics.FromImage(NewBitmap)) {
                using var BoxPen = new Pen(BoxColor);
                using var Path = new GraphicsPath();
                Path.AddLine(XPosition + CornerRadius, YPosition, XPosition + Width - (CornerRadius * 2), YPosition);
                Path.AddArc(XPosition + Width - (CornerRadius * 2), YPosition, CornerRadius * 2, CornerRadius * 2, 270, 90);
                Path.AddLine(XPosition + Width, YPosition + CornerRadius, XPosition + Width, YPosition + Height - (CornerRadius * 2));
                Path.AddArc(XPosition + Width - (CornerRadius * 2), YPosition + Height - (CornerRadius * 2), CornerRadius * 2, CornerRadius * 2, 0, 90);
                Path.AddLine(XPosition + Width - (CornerRadius * 2), YPosition + Height, XPosition + CornerRadius, YPosition + Height);
                Path.AddArc(XPosition, YPosition + Height - (CornerRadius * 2), CornerRadius * 2, CornerRadius * 2, 90, 90);
                Path.AddLine(XPosition, YPosition + Height - (CornerRadius * 2), XPosition, YPosition + CornerRadius);
                Path.AddArc(XPosition, YPosition, CornerRadius * 2, CornerRadius * 2, 180, 90);
                Path.CloseFigure();
                NewGraphics.DrawPath(BoxPen, Path);
            }
            return NewBitmap;
        }

        /// <summary>Draws text on an image within the bounding box specified.</summary>
        /// <param name="TextToDraw">The text to draw on the image</param>
        /// <param name="FontToUse">Font in which to draw the text</param>
        /// <param name="BrushUsing">Defines the brush using</param>
        /// <param name="BoxToDrawWithin">Rectangle to draw the image within</param>
        /// <returns>A bitmap object with the text drawn on it</returns>
        public Bitmap DrawText(string TextToDraw, Font FontToUse, Brush BrushUsing, RectangleF BoxToDrawWithin) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //Image.ThrowIfNull("Image");
            //FontToUse.ThrowIfNull("FontToUse");
            //BrushUsing.ThrowIfNull("BrushUsing");
            //BoxToDrawWithin.ThrowIfNull("BoxToDrawWithin");
            var NewBitmap = new Bitmap(Image, Image.Width, Image.Height);
            using (var TempGraphics = Graphics.FromImage(NewBitmap)) {
                TempGraphics.DrawString(TextToDraw, FontToUse, BrushUsing, BoxToDrawWithin);
            }
            return NewBitmap;
        }

        /// <summary>Does basic edge detection on an image</summary>
        /// <param name="Threshold">Decides what is considered an edge</param>
        /// <param name="EdgeColor">Color of the edge</param>
        /// <returns>A bitmap which has the edges drawn on it</returns>
        public Bitmap EdgeDetection(float Threshold, Color EdgeColor) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            //EdgeColor.ThrowIfNull("EdgeColor");
            var NewBitmap = new Bitmap(Image, Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var CurrentColor = OldData.GetPixel(x, y, OldPixelSize);
                    if (y < Height - 1 && x < Width - 1) {
                        var TempColor = OldData.GetPixel(x + 1, y + 1, OldPixelSize);
                        if (Distance(CurrentColor.R, TempColor.R, CurrentColor.G, TempColor.G, CurrentColor.B, TempColor.B) > Threshold) NewData.SetPixel(x, y, EdgeColor, NewPixelSize);
                    } else if (y < Height - 1) {
                        var TempColor = OldData.GetPixel(x, y + 1, OldPixelSize);
                        if (Distance(CurrentColor.R, TempColor.R, CurrentColor.G, TempColor.G, CurrentColor.B, TempColor.B) > Threshold) NewData.SetPixel(x, y, EdgeColor, NewPixelSize);
                    } else if (x < Width - 1) {
                        var TempColor = OldData.GetPixel(x + 1, y, OldPixelSize);
                        if (Distance(CurrentColor.R, TempColor.R, CurrentColor.G, TempColor.G, CurrentColor.B, TempColor.B) > Threshold) NewData.SetPixel(x, y, EdgeColor, NewPixelSize);
                    }
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Flips an image</summary>
        /// <param name="FlipX">Flips an image along the X axis</param>
        /// <param name="FlipY">Flips an image along the Y axis</param>
        /// <returns>A bitmap which is flipped</returns>
        public Bitmap Flip(bool FlipX, bool FlipY) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //Image.ThrowIfNull("Image");
            var NewBitmap = new Bitmap(Image, Image.Width, Image.Height);
            if (FlipX && !FlipY)
                NewBitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
            else if (!FlipX && FlipY)
                NewBitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
            else if (FlipX && FlipY) NewBitmap.RotateFlip(RotateFlipType.RotateNoneFlipXY);
            return NewBitmap;
        }

        /// <summary>Causes a "Jitter" effect</summary>
        /// <param name="MaxJitter">Maximum number of pixels the item can move</param>
        /// <returns>A bitmap object</returns>
        public Bitmap Jitter(int MaxJitter = 5) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image, Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var NewX = new Random().Next(-MaxJitter, MaxJitter); //Could be global seed with thread safety
                    var NewY = new Random().Next(-MaxJitter, MaxJitter);
                    NewX += x;
                    NewY += y;
                    NewX = NewX.Clamp(Width - 1, 0);
                    NewY = NewY.Clamp(Height - 1, 0);

                    NewData.SetPixel(x, y, OldData.GetPixel(NewX, NewY, OldPixelSize), NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Does smoothing using a kuwahara blur</summary>
        /// <param name="Size">Size of the aperture</param>
        /// <returns>A bitmap object</returns>
        public Bitmap KuwaharaBlur(int Size = 3) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            int[] ApetureMinX = { -(Size / 2), 0, -(Size / 2), 0 };
            int[] ApetureMaxX = { 0, Size / 2, 0, Size / 2 };
            int[] ApetureMinY = { -(Size / 2), -(Size / 2), 0, 0 };
            int[] ApetureMaxY = { 0, 0, Size / 2, Size / 2 };
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    int[] RValues = { 0, 0, 0, 0 };
                    int[] GValues = { 0, 0, 0, 0 };
                    int[] BValues = { 0, 0, 0, 0 };
                    int[] NumPixels = { 0, 0, 0, 0 };
                    int[] MaxRValue = { 0, 0, 0, 0 };
                    int[] MaxGValue = { 0, 0, 0, 0 };
                    int[] MaxBValue = { 0, 0, 0, 0 };
                    int[] MinRValue = { 255, 255, 255, 255 };
                    int[] MinGValue = { 255, 255, 255, 255 };
                    int[] MinBValue = { 255, 255, 255, 255 };
                    for (var i = 0; i < 4; ++i) {
                        for (var x2 = ApetureMinX[i]; x2 < ApetureMaxX[i]; ++x2) {
                            var TempX = x + x2;
                            if (TempX >= 0 && TempX < Width) {
                                for (var y2 = ApetureMinY[i]; y2 < ApetureMaxY[i]; ++y2) {
                                    var TempY = y + y2;
                                    if (TempY >= 0 && TempY < Height) {
                                        var TempColor = OldData.GetPixel(TempX, TempY, OldPixelSize);
                                        RValues[i] += TempColor.R;
                                        GValues[i] += TempColor.G;
                                        BValues[i] += TempColor.B;
                                        if (TempColor.R > MaxRValue[i])
                                            MaxRValue[i] = TempColor.R;
                                        else if (TempColor.R < MinRValue[i]) MinRValue[i] = TempColor.R;

                                        if (TempColor.G > MaxGValue[i])
                                            MaxGValue[i] = TempColor.G;
                                        else if (TempColor.G < MinGValue[i]) MinGValue[i] = TempColor.G;

                                        if (TempColor.B > MaxBValue[i])
                                            MaxBValue[i] = TempColor.B;
                                        else if (TempColor.B < MinBValue[i]) MinBValue[i] = TempColor.B;

                                        ++NumPixels[i];
                                    }
                                }
                            }
                        }
                    }

                    var j = 0;
                    var MinDifference = 10000;
                    for (var i = 0; i < 4; ++i) {
                        var CurrentDifference = MaxRValue[i] - MinRValue[i] + (MaxGValue[i] - MinGValue[i]) + (MaxBValue[i] - MinBValue[i]);
                        if (CurrentDifference < MinDifference && NumPixels[i] > 0) {
                            j = i;
                            MinDifference = CurrentDifference;
                        }
                    }

                    var MeanPixel = Color.FromArgb(RValues[j] / NumPixels[j],
                        GValues[j] / NumPixels[j],
                        BValues[j] / NumPixels[j]);
                    NewData.SetPixel(x, y, MeanPixel, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Does smoothing using a median filter</summary>
        /// <param name="Size">Size of the aperture</param>
        /// <returns>A bitmap image</returns>
        public Bitmap MedianFilter(int Size = 3) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var ApetureMin = -(Size / 2);
            var ApetureMax = Size / 2;
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var RValues = new List<int>();
                    var GValues = new List<int>();
                    var BValues = new List<int>();
                    for (var x2 = ApetureMin; x2 < ApetureMax; ++x2) {
                        var TempX = x + x2;
                        if (TempX >= 0 && TempX < Width) {
                            for (var y2 = ApetureMin; y2 < ApetureMax; ++y2) {
                                var TempY = y + y2;
                                if (TempY >= 0 && TempY < Height) {
                                    var TempColor = OldData.GetPixel(TempX, TempY, OldPixelSize);
                                    RValues.Add(TempColor.R);
                                    GValues.Add(TempColor.G);
                                    BValues.Add(TempColor.B);
                                }
                            }
                        }
                    }
                    var MedianPixel = Color.FromArgb(RValues.Find().Median(),
                        GValues.Find().Median(),
                        BValues.Find().Median());
                    NewData.SetPixel(x, y, MedianPixel, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>gets the negative of the image</summary>
        /// <returns>A bitmap image</returns>
        public Bitmap Negative() {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var CurrentPixel = OldData.GetPixel(x, y, OldPixelSize);
                    var TempValue = Color.FromArgb(255 - CurrentPixel.R, 255 - CurrentPixel.G, 255 - CurrentPixel.B);
                    NewData.SetPixel(x, y, TempValue, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Pixelates an image</summary>
        /// <param name="PixelSize">Size of the "pixels" in pixels</param>
        /// <returns>A bitmap image</returns>
        public Bitmap Pixelate(int PixelSize = 5) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            for (var x = 0; x < NewBitmap.Width; x += PixelSize / 2) {
                var MinX = (x - (PixelSize / 2)).Clamp(NewBitmap.Width, 0);
                var MaxX = (x + (PixelSize / 2)).Clamp(NewBitmap.Width, 0);
                for (var y = 0; y < NewBitmap.Height; y += PixelSize / 2) {
                    var RValue = 0;
                    var GValue = 0;
                    var BValue = 0;
                    var MinY = (y - (PixelSize / 2)).Clamp(NewBitmap.Height, 0);
                    var MaxY = (y + (PixelSize / 2)).Clamp(NewBitmap.Height, 0);
                    for (var x2 = MinX; x2 < MaxX; ++x2) {
                        for (var y2 = MinY; y2 < MaxY; ++y2) {
                            var Pixel = OldData.GetPixel(x2, y2, OldPixelSize);
                            RValue += Pixel.R;
                            GValue += Pixel.G;
                            BValue += Pixel.B;
                        }
                    }

                    RValue /= PixelSize * PixelSize;
                    GValue /= PixelSize * PixelSize;
                    BValue /= PixelSize * PixelSize;
                    var TempPixel = Color.FromArgb(RValue, GValue, BValue);
                    Parallel.For(MinX, MaxX, x2 => {
                        for (var y2 = MinY; y2 < MaxY; ++y2) NewData.SetPixel(x2, y2, TempPixel, NewPixelSize);
                    });
                }
            }
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Resizes an image to a certain height</summary>
        /// <param name="MaxSide">Max height/width for the final image</param>
        /// <param name="Quality">Quality of the resizing</param>
        /// <returns>A bitmap object of the resized image</returns>
        public Bitmap Resize(int MaxSide, Quality Quality = Quality.Low) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //Image.ThrowIfNull("Image");
            int NewWidth;
            int NewHeight;

            var OldWidth = Image.Width;
            var OldHeight = Image.Height;
            int OldMaxSide = OldWidth >= OldHeight ? OldWidth : OldHeight;

            var Coefficient = (double)MaxSide / (double)OldMaxSide;
            NewWidth = Convert.ToInt32(Coefficient * OldWidth);
            NewHeight = Convert.ToInt32(Coefficient * OldHeight);
            if (NewWidth <= 0) NewWidth = 1;
            if (NewHeight <= 0) NewHeight = 1;
            return Image.Process().Resize(NewWidth, NewHeight, Quality);
        }

        /// <summary>Resizes an image to a certain height. (If width or height is entered 0, scale is preserved)</summary>
        /// <param name="width">New width for the final image</param>
        /// <param name="height">New height for the final image</param>
        /// <param name="Quality">Quality of the resizing</param>
        /// <returns>A bitmap object of the resized image</returns>
        public Bitmap Resize(int width, int height, Quality Quality = Quality.Low) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            float proportionalWidth = width;
            float proportionalHeight = height;
            var bitmap = Image;
            if (width.Equals(0)) {
                proportionalWidth = (float)height / bitmap.Size.Height * bitmap.Width;
                proportionalHeight = height;
            } else if (height.Equals(0)) {
                proportionalWidth = width;
                proportionalHeight = (float)width / bitmap.Size.Width * bitmap.Height;
            }
            //else if (((float)width) / bitmap.Size.Width * bitmap.Size.Height <= height)
            //{
            //    proportionalWidth = width;
            //    proportionalHeight = ((float)width) / bitmap.Size.Width * bitmap.Height;
            //}
            //else
            //{
            //    proportionalWidth = ((float)height) / bitmap.Size.Height * bitmap.Width;
            //    proportionalHeight = height;
            //}

            width = (int)proportionalWidth;
            height = (int)proportionalHeight;
            //Image.ThrowIfNull("Image");
            var NewBitmap = new Bitmap(width, height);
            using (var NewGraphics = Graphics.FromImage(NewBitmap)) {
                if (Quality == Quality.High) {
                    NewGraphics.CompositingQuality = CompositingQuality.HighQuality;
                    NewGraphics.SmoothingMode = SmoothingMode.HighQuality;
                    NewGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                } else {
                    NewGraphics.CompositingQuality = CompositingQuality.HighSpeed;
                    NewGraphics.SmoothingMode = SmoothingMode.HighSpeed;
                    NewGraphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                }
                NewGraphics.DrawImage(Image, new Rectangle(0, 0, width, height));
            }
            return NewBitmap;
        }

        /// <summary>Rotates an image</summary>
        /// <param name="DegreesToRotate">Degrees to rotate the image</param>
        /// <returns>A bitmap object containing the rotated image</returns>
        public Bitmap Rotate(float DegreesToRotate) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //Image.ThrowIfNull("Image");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            using (var NewGraphics = Graphics.FromImage(NewBitmap)) {
                NewGraphics.TranslateTransform((float)Image.Width / 2.0f, (float)Image.Height / 2.0f);
                NewGraphics.RotateTransform(DegreesToRotate);
                NewGraphics.TranslateTransform(-(float)Image.Width / 2.0f, -(float)Image.Height / 2.0f);
                NewGraphics.DrawImage(Image,
                    new Rectangle(0, 0, Image.Width, Image.Height),
                    new Rectangle(0, 0, Image.Width, Image.Height),
                    GraphicsUnit.Pixel);
            }
            return NewBitmap;
        }

        /// <summary>Does a "wave" effect on the image</summary>
        /// <param name="Amplitude">Amplitude of the sine wave</param>
        /// <param name="Frequency">Frequency of the sine wave</param>
        /// <param name="XDirection">Determines if this should be done in the X direction</param>
        /// <param name="YDirection">Determines if this should be done in the Y direction</param>
        /// <returns>A bitmap which has been modified</returns>
        public Bitmap SinWave(float Amplitude, float Frequency, bool XDirection, bool YDirection) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    double Value1 = 0;
                    double Value2 = 0;
                    if (YDirection) Value1 = Math.Sin(x * Frequency * Math.PI / 180.0d) * Amplitude;
                    if (XDirection) Value2 = Math.Sin(y * Frequency * Math.PI / 180.0d) * Amplitude;
                    Value1 = y - (int)Value1;
                    Value2 = x - (int)Value2;
                    while (Value1 < 0) Value1 += Height;
                    while (Value2 < 0) Value2 += Width;
                    while (Value1 >= Height) Value1 -= Height;
                    while (Value2 >= Width) Value2 -= Width;
                    NewData.SetPixel(x, y,
                        OldData.GetPixel((int)Value2, (int)Value1, OldPixelSize),
                        NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Does smoothing using a SNN blur</summary>
        /// <param name="Size">Size of the aperture</param>
        /// <returns>The resulting bitmap</returns>
        public Bitmap SNNBlur(int Size = 3) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var ApetureMinX = -(Size / 2);
            var ApetureMaxX = Size / 2;
            var ApetureMinY = -(Size / 2);
            var ApetureMaxY = Size / 2;
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var RValue = 0;
                    var GValue = 0;
                    var BValue = 0;
                    var NumPixels = 0;
                    for (var x2 = ApetureMinX; x2 < ApetureMaxX; ++x2) {
                        var TempX1 = x + x2;
                        var TempX2 = x - x2;
                        if (TempX1 >= 0 && TempX1 < Width && TempX2 >= 0 && TempX2 < Width) {
                            for (var y2 = ApetureMinY; y2 < ApetureMaxY; ++y2) {
                                var TempY1 = y + y2;
                                var TempY2 = y - y2;
                                if (TempY1 >= 0 && TempY1 < Height && TempY2 >= 0 && TempY2 < Height) {
                                    var TempColor = OldData.GetPixel(x, y, OldPixelSize);
                                    var TempColor2 = OldData.GetPixel(TempX1, TempY1, OldPixelSize);
                                    var TempColor3 = OldData.GetPixel(TempX2, TempY2, OldPixelSize);
                                    if (Distance(TempColor.R, TempColor2.R, TempColor.G, TempColor2.G, TempColor.B, TempColor2.B) <
                                        Distance(TempColor.R, TempColor3.R, TempColor.G, TempColor3.G, TempColor.B, TempColor3.B)) {
                                        RValue += TempColor2.R;
                                        GValue += TempColor2.G;
                                        BValue += TempColor2.B;
                                    } else {
                                        RValue += TempColor3.R;
                                        GValue += TempColor3.G;
                                        BValue += TempColor3.B;
                                    }
                                    ++NumPixels;
                                }
                            }
                        }
                    }
                    var MeanPixel = Color.FromArgb(RValue / NumPixels,
                        GValue / NumPixels,
                        BValue / NumPixels);
                    NewData.SetPixel(x, y, MeanPixel, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Stretches the contrast</summary>
        /// <returns>A bitmap image</returns>
        public Bitmap StretchContrast() {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            GetMinMaxPixel(out var MinValue, out var MaxValue, OldData, OldPixelSize);
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var CurrentPixel = OldData.GetPixel(x, y, OldPixelSize);
                    var TempValue = Color.FromArgb(Map(CurrentPixel.R, MinValue.R, MaxValue.R),
                        Map(CurrentPixel.G, MinValue.G, MaxValue.G),
                        Map(CurrentPixel.B, MinValue.B, MaxValue.B));
                    NewData.SetPixel(x, y, TempValue, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Does threshold manipulation of the image</summary>
        /// <param name="Threshold">Float defining the threshold at which to set the pixel to black vs white.</param>
        /// <returns>A bitmap object containing the new image</returns>
        public Bitmap Threshold(float Threshold = 0.5f) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //OriginalImage.ThrowIfNull("OriginalImage");
            var NewBitmap = new Bitmap(Image.Width, Image.Height);
            var NewData = NewBitmap.LockImage();
            var OldData = Image.LockImage();
            var NewPixelSize = NewData.GetPixelSize();
            var OldPixelSize = OldData.GetPixelSize();
            var Width = NewBitmap.Width;
            var Height = NewBitmap.Height;
            Parallel.For(0, Width, x => {
                for (var y = 0; y < Height; ++y) {
                    var TempColor = OldData.GetPixel(x, y, OldPixelSize);
                    if ((TempColor.R + TempColor.G + TempColor.B) / 755.0f > Threshold)
                        NewData.SetPixel(x, y, Color.White, NewPixelSize);
                    else
                        NewData.SetPixel(x, y, Color.Black, NewPixelSize);
                }
            });
            NewBitmap.UnlockImage(NewData);
            Image.UnlockImage(OldData);
            return NewBitmap;
        }

        /// <summary>Adds a watermark to an image</summary>
        /// <param name="WatermarkImage">Watermark image</param>
        /// <param name="Opacity">Opacity of the watermark (1.0 to 0.0 with 1 being completely visible and 0 being invisible)</param>
        /// <param name="X">X position in pixels for the watermark</param>
        /// <param name="Y">Y position in pixels for the watermark</param>
        /// <param name="KeyColor">Transparent color used in watermark image, set to null if not used</param>
        /// <returns>The results in the form of a bitmap object</returns>
        public Bitmap Watermark(Bitmap WatermarkImage, float Opacity, int X, int Y, Color KeyColor) {
            if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
            //Image.ThrowIfNull("Image");
            //WatermarkImage.ThrowIfNull("WatermarkImage");
            var NewBitmap = new Bitmap(Image, Image.Width, Image.Height);
            using (var NewGraphics = Graphics.FromImage(NewBitmap)) {
                float[][] FloatColorMatrix = {
                        new float[] {1, 0, 0, 0, 0},
                        new float[] {0, 1, 0, 0, 0},
                        new float[] {0, 0, 1, 0, 0},
                        new float[] {0, 0, 0, Opacity, 0},
                        new float[] {0, 0, 0, 0, 1}
                    };

                var NewColorMatrix = new ColorMatrix(FloatColorMatrix);
                using var Attributes = new ImageAttributes();
                Attributes.SetColorMatrix(NewColorMatrix);
                Attributes.SetColorKey(KeyColor, KeyColor);
                NewGraphics.DrawImage(WatermarkImage,
                    new Rectangle(X, Y, WatermarkImage.Width, WatermarkImage.Height),
                    0, 0, WatermarkImage.Width, WatermarkImage.Height,
                    GraphicsUnit.Pixel,
                    Attributes);
            }
            return NewBitmap;
        }
    }
    #endregion BitmapProcess

    #region Functions
    /// <summary>Process the image</summary>
    public static BitmapProcessPortion Process(this Bitmap bitmap) => new(bitmap);

    #region Save
    /// <summary>Saves the image to a file</summary>
    /// <param name="Image">Image to save</param>
    /// <param name="FileName">File to save to</param>
    public static void Save(this Bitmap Image, string FileName) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        var FormatUsing = GetImageFormat(FileName);
        if (!string.IsNullOrEmpty(FileName)) Image.Save(FileName, FormatUsing);
    }
    #endregion Save

    #region Distance
    internal static double Distance(int R1, int R2, int G1, int G2, int B1, int B2) => Math.Sqrt((double)(((R1 - R2) * (R1 - R2)) + ((G1 - G2) * (G1 - G2)) + ((B1 - B2) * (B1 - B2))));
    #endregion Distance

    #region Map
    internal static int Map(int Value, int Min, int Max) {
        double TempVal = Value - Min;
        TempVal /= (double)(Max - Min);
        return ((int)(TempVal * 255)).Clamp(255, 0);
    }
    #endregion Map

    #region GetHTMLPalette (disabled)
    ///// <summary>
    ///// Gets a palette listing in HTML string format
    ///// </summary>
    ///// <param name="Image">Image to get the palette of</param>
    ///// <returns>A list containing HTML color values (ex: #041845)</returns>
    //public static List<string> GetHTMLPalette(this Bitmap Image)
    //{
    //    //OriginalImage.ThrowIfNull("OriginalImage");
    //    HashSet<string> ReturnArray = new HashSet<string>();
    //    if (Image.Palette != null && Image.Palette.Entries.Length > 0) {
    //        Image.Palette.Entries.ForEach(x => ReturnArray.AddUnique(ColorTranslator.ToHtml(x)));
    //        return ReturnArray.To().List();
    //    }
    //    BitmapData ImageData = Image.LockImage();
    //    int PixelSize = ImageData.GetPixelSize();
    //    for (int x = 0; x < Image.Width; ++x) {
    //        for (int y = 0; y < Image.Height; ++y) {
    //            ReturnArray.AddUnique(ColorTranslator.ToHtml(ImageData.GetPixel(x, y, PixelSize)));
    //        }
    //    }
    //    Image.UnlockImage(ImageData);
    //    return ReturnArray.To().List();
    //}
    #endregion GetHTMLPalette

    #region GetBytes
    /// <summary>Converts an image to a byte[] string and returns it</summary>
    /// <param name="Image">Image</param>
    /// <param name="DesiredFormat">Desired image format (defaults to Jpeg)</param>
    /// <returns>The image in byte[] format</returns>
    public static byte[] GetBytes(this Bitmap Image, ImageFormat DesiredFormat = null) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        DesiredFormat ??= ImageFormat.Jpeg;
        using var Stream = new MemoryStream();
        Image.Save(Stream, DesiredFormat);
        return Stream.ToArray();
    }
    #endregion GetBytes

    #region GetImageFormat
    /// <summary>Returns the image format this file is using</summary>
    /// <param name="FileName"></param>
    /// <returns></returns>
    internal static ImageFormat GetImageFormat(string FileName) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        if (string.IsNullOrEmpty(FileName)) return ImageFormat.Bmp;
        if (FileName.EndsWith("jpg", StringComparison.InvariantCultureIgnoreCase) || FileName.EndsWith("jpeg", StringComparison.InvariantCultureIgnoreCase)) return ImageFormat.Jpeg;
        if (FileName.EndsWith("png", StringComparison.InvariantCultureIgnoreCase)) return ImageFormat.Png;
        if (FileName.EndsWith("tiff", StringComparison.InvariantCultureIgnoreCase)) return ImageFormat.Tiff;
        if (FileName.EndsWith("ico", StringComparison.InvariantCultureIgnoreCase)) return ImageFormat.Icon;
        if (FileName.EndsWith("gif", StringComparison.InvariantCultureIgnoreCase)) return ImageFormat.Gif;
        return ImageFormat.Bmp;
    }
    #endregion GetImageFormat

    #region GetMinMaxPixel
    internal static void GetMinMaxPixel(out Color Min, out Color Max, BitmapData ImageData, int PixelSize) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //ImageData.ThrowIfNull("ImageData");
        int MinR = 255, MinG = 255, MinB = 255;
        int MaxR = 0, MaxG = 0, MaxB = 0;
        for (var x = 0; x < ImageData.Width; ++x) {
            for (var y = 0; y < ImageData.Height; ++y) {
                var TempImage = ImageData.GetPixel(x, y, PixelSize);
                if (MinR > TempImage.R) MinR = TempImage.R;
                if (MaxR < TempImage.R) MaxR = TempImage.R;

                if (MinG > TempImage.G) MinG = TempImage.G;
                if (MaxG < TempImage.G) MaxG = TempImage.G;

                if (MinB > TempImage.B) MinB = TempImage.B;
                if (MaxB < TempImage.B) MaxB = TempImage.B;
            }
        }

        Min = Color.FromArgb(MinR, MinG, MinB);
        Max = Color.FromArgb(MaxR, MaxG, MaxB);
    }
    #endregion GetMinMaxPixel

    #region GetPixelSize
    /// <summary>Gets the pixel size (in bytes)</summary>
    /// <param name="Data">Bitmap data</param>
    /// <returns>The pixel size (in bytes)</returns>
    public static int GetPixelSize(this BitmapData Data) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Data.ThrowIfNull("Data");
        if (Data.PixelFormat == PixelFormat.Format24bppRgb) {
            return 3;
        } else if (Data.PixelFormat == PixelFormat.Format32bppArgb
                  || Data.PixelFormat == PixelFormat.Format32bppPArgb
                  || Data.PixelFormat == PixelFormat.Format32bppRgb) {
            return 4;
        }

        return 0;
    }
    #endregion GetPixelSize

    #region GetPixel
    /// <summary>Gets a pixel from an x,y coordinate</summary>
    /// <param name="Data">Bitmap data</param>
    /// <param name="x">X coord</param>
    /// <param name="y">Y coord</param>
    /// <param name="PixelSizeInBytes">Pixel size in bytes</param>
    /// <returns>The pixel at the x,y coords</returns>
    public static unsafe Color GetPixel(this BitmapData Data, int x, int y, int PixelSizeInBytes) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Data.ThrowIfNull("Data");
        var DataPointer = (byte*)Data.Scan0;
        DataPointer = DataPointer + (y * Data.Stride) + (x * PixelSizeInBytes);
        return PixelSizeInBytes == 3 ? Color.FromArgb(DataPointer[2], DataPointer[1], DataPointer[0]) : Color.FromArgb(DataPointer[3], DataPointer[2], DataPointer[1], DataPointer[0]);
    }
    #endregion GetPixel

    #region SetPixel
    /// <summary>Sets a pixel at the x,y coords</summary>
    /// <param name="Data">Bitmap data</param>
    /// <param name="x">X coord</param>
    /// <param name="y">Y coord</param>
    /// <param name="PixelColor">Pixel color information</param>
    /// <param name="PixelSizeInBytes">Pixel size in bytes</param>
    public static unsafe void SetPixel(this BitmapData Data, int x, int y, Color PixelColor, int PixelSizeInBytes) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Data.ThrowIfNull("Data");
        //PixelColor.ThrowIfNull("PixelColor");
        var DataPointer = (byte*)Data.Scan0;
        DataPointer = DataPointer + (y * Data.Stride) + (x * PixelSizeInBytes);
        if (PixelSizeInBytes == 3) {
            DataPointer[2] = PixelColor.R;
            DataPointer[1] = PixelColor.G;
            DataPointer[0] = PixelColor.B;
            return;
        }
        DataPointer[3] = PixelColor.A;
        DataPointer[2] = PixelColor.R;
        DataPointer[1] = PixelColor.G;
        DataPointer[0] = PixelColor.B;
    }
    #endregion SetPixel

    #region Or
    /// <summary>Ors two images</summary>
    /// <param name="Image1">Image to manipulate</param>
    /// <param name="Image2">Image to manipulate</param>
    /// <param name="FileName">File to save to</param>
    /// <returns>A bitmap image</returns>
    public static Bitmap Or(this Bitmap Image1, Bitmap Image2, string FileName = "") {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Image1.ThrowIfNull("Image1");
        //Image2.ThrowIfNull("Image2");
        var FormatUsing = GetImageFormat(FileName);
        var NewBitmap = new Bitmap(Image1.Width, Image1.Height);
        var NewData = NewBitmap.LockImage();
        var OldData1 = Image1.LockImage();
        var OldData2 = Image2.LockImage();
        var NewPixelSize = NewData.GetPixelSize();
        var OldPixelSize1 = OldData1.GetPixelSize();
        var OldPixelSize2 = OldData2.GetPixelSize();
        var Width = NewBitmap.Width;
        var Height = NewBitmap.Height;
        Parallel.For(0, Width, x => {
            for (var y = 0; y < Height; ++y) {
                var Pixel1 = OldData1.GetPixel(x, y, OldPixelSize1);
                var Pixel2 = OldData2.GetPixel(x, y, OldPixelSize2);
                NewData.SetPixel(x, y,
                    Color.FromArgb(Pixel1.R | Pixel2.R,
                        Pixel1.G | Pixel2.G,
                        Pixel1.B | Pixel2.B),
                    NewPixelSize);
            }
        });
        NewBitmap.UnlockImage(NewData);
        Image1.UnlockImage(OldData1);
        Image2.UnlockImage(OldData2);
        if (!string.IsNullOrEmpty(FileName)) NewBitmap.Save(FileName, FormatUsing);
        return NewBitmap;
    }
    #endregion Or

    #region And
    /// <summary>ands two images</summary>
    /// <param name="Image1">Image to manipulate</param>
    /// <param name="Image2">Image to manipulate</param>
    /// <param name="FileName">File to save to</param>
    /// <returns>A bitmap image</returns>
    public static Bitmap And(this Bitmap Image1, Bitmap Image2, string FileName = "") {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Image1.ThrowIfNull("Image1");
        //Image2.ThrowIfNull("Image2");
        var FormatUsing = GetImageFormat(FileName);
        var NewBitmap = new Bitmap(Image1.Width, Image1.Height);
        var NewData = NewBitmap.LockImage();
        var OldData1 = Image1.LockImage();
        var OldData2 = Image2.LockImage();
        var NewPixelSize = NewData.GetPixelSize();
        var OldPixelSize1 = OldData1.GetPixelSize();
        var OldPixelSize2 = OldData2.GetPixelSize();
        var Width = NewBitmap.Width;
        var Height = NewBitmap.Height;
        Parallel.For(0, Width, x => {
            for (var y = 0; y < Height; ++y) {
                var Pixel1 = OldData1.GetPixel(x, y, OldPixelSize1);
                var Pixel2 = OldData2.GetPixel(x, y, OldPixelSize2);
                NewData.SetPixel(x, y,
                    Color.FromArgb(Pixel1.R & Pixel2.R,
                        Pixel1.G & Pixel2.G,
                        Pixel1.B & Pixel2.B),
                    NewPixelSize);
            }
        });
        NewBitmap.UnlockImage(NewData);
        Image1.UnlockImage(OldData1);
        Image2.UnlockImage(OldData2);
        if (!string.IsNullOrEmpty(FileName)) NewBitmap.Save(FileName, FormatUsing);
        return NewBitmap;
    }
    #endregion And

    #region Xor
    /// <summary>Xors two images</summary>
    /// <param name="Image1">Image to manipulate</param>
    /// <param name="Image2">Image to manipulate</param>
    /// <param name="FileName">File to save to</param>
    /// <returns>A bitmap image</returns>
    public static Bitmap Xor(this Bitmap Image1, Bitmap Image2, string FileName = "") {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Image1.ThrowIfNull("Image1");
        //Image2.ThrowIfNull("Image2");
        var FormatUsing = GetImageFormat(FileName);
        var NewBitmap = new Bitmap(Image1.Width, Image1.Height);
        var NewData = NewBitmap.LockImage();
        var OldData1 = Image1.LockImage();
        var OldData2 = Image2.LockImage();
        var NewPixelSize = NewData.GetPixelSize();
        var OldPixelSize1 = OldData1.GetPixelSize();
        var OldPixelSize2 = OldData2.GetPixelSize();
        var Width = NewBitmap.Width;
        var Height = NewBitmap.Height;
        Parallel.For(0, Width, x => {
            for (var y = 0; y < Height; ++y) {
                var Pixel1 = OldData1.GetPixel(x, y, OldPixelSize1);
                var Pixel2 = OldData2.GetPixel(x, y, OldPixelSize2);
                NewData.SetPixel(x, y,
                    Color.FromArgb(Pixel1.R ^ Pixel2.R,
                        Pixel1.G ^ Pixel2.G,
                        Pixel1.B ^ Pixel2.B),
                    NewPixelSize);
            }
        });
        NewBitmap.UnlockImage(NewData);
        Image1.UnlockImage(OldData1);
        Image2.UnlockImage(OldData2);
        if (!string.IsNullOrEmpty(FileName)) NewBitmap.Save(FileName, FormatUsing);
        return NewBitmap;
    }
    #endregion Xor

    #region LockImage
    /// <summary>Locks an image</summary>
    /// <param name="Image">Image to lock</param>
    /// <returns>The bitmap data for the image</returns>
    public static BitmapData LockImage(this Bitmap Image) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Image.ThrowIfNull("Image");
        return Image.LockBits(new Rectangle(0, 0, Image.Width, Image.Height),
            ImageLockMode.ReadWrite, Image.PixelFormat);
    }
    #endregion LockImage

    #region UnlockImage
    /// <summary>Unlocks the image's data</summary>
    /// <param name="Image">Image to unlock</param>
    /// <param name="ImageData">The image data</param>
    /// <returns>Returns the image</returns>
    public static Bitmap UnlockImage(this Bitmap Image, BitmapData ImageData) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        //Image.ThrowIfNull("Image");
        //ImageData.ThrowIfNull("ImageData");
        Image.UnlockBits(ImageData);
        return Image;
    }
    #endregion UnlockImage
    #endregion Functions

    #region Enums
    /// <summary>Enum defining alignment</summary>
    public enum Align {
        /// <summary>Top</summary>
        Top,
        /// <summary>Bottom</summary>
        Bottom,
        /// <summary>Left</summary>
        Left,
        /// <summary>Right</summary>
        Right
    }

    /// <summary>Enum defining quality</summary>
    public enum Quality {
        /// <summary>High</summary>
        High,
        /// <summary>Low</summary>
        Low
    }

    /// <summary>Direction</summary>
    public enum Direction {
        /// <summary>Top to bottom</summary>
        TopBottom = 0,
        /// <summary>Left to right</summary>
        LeftRight
    };
    #endregion Enums
}