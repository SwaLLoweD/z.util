﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Drawing;
using System.IO;
using System.Linq;
namespace Z.Extensions;

/// <summary>Extension methods for byte[] values...</summary>
public static partial class ExtDrawingByteArray {
    /// <summary>Converts the specified byte[] to a Binary File.</summary>
    /// <param name="p"></param>
    /// <param name="offset">offset x amount of bytes</param>
    /// <param name="len">write x amount of bytes</param>
    public static Image Image(this ExtByteArray.ConvertPortion p, int offset = 0, int? len = null) {
        if (!OperatingSystem.IsWindowsVersionAtLeast(6, 1)) throw new PlatformNotSupportedException();
        var bytes = p.Value.ToArray();
        using var stream = new MemoryStream(bytes, offset, len ?? bytes.Length - offset, false);
        return new Bitmap(stream);
    }
}