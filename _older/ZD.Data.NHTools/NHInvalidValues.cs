﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Collections.Generic;

namespace Z.Data.NHTools
{
    /// <summary>Invalid Entity Values</summary>
    public class NHInvalidVal : Z.Data.Common.InvalidValue
    {
        /// <summary></summary>
        public NHInvalidVal(NHibernate.Validator.Engine.InvalidValue val)
        {
            importNHInvalidValue(val);
        }

        private void importNHInvalidValue(NHibernate.Validator.Engine.InvalidValue val)
        {
            Entity = val.Entity;
            EntityType = val.EntityType;
            Message = val.Message;
            PropertyName = val.PropertyName;
            PropertyPath = val.PropertyPath;
            Value = val.Value;
            MatchTags = val.MatchTags;
        }
        /// <summary>Cast NHibernate InvalidValue to Securiskop InvalidVal</summary>
        public static implicit operator NHInvalidVal(NHibernate.Validator.Engine.InvalidValue val)
        {
            NHInvalidVal rval = new NHInvalidVal(val);
            return rval;
        }
        /// <summary>Convert NHibernate InvalidValues to Securiskop InvalidVal</summary>
        public static IEnumerable<Z.Data.Common.InvalidValue> ImportInvalidValues(NHibernate.Validator.Engine.InvalidValue[] val)
        {
            if (val == null) return null;
            var rval = new List<Z.Data.Common.InvalidValue>();
            for (int i = 0; i < val.Length; i++) {
                rval.Add(new NHInvalidVal(val[i]));
            }
            return rval;
        }
    }
}