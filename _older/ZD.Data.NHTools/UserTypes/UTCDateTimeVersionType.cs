﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;
using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace Z.Data.NHTools.UserTypes
{
    /// <summary>UTC Datetime Type</summary>
    internal class UTCDateTimeVersionType : IUserVersionType, IEnhancedUserType
    {
        /// <summary></summary>
        public SqlType[] SqlTypes { get { return new SqlType[] { new SqlType(DbType.DateTime) }; } }
        /// <summary></summary>
        public System.Type ReturnedType { get { return typeof(DateTime); } }
        /// <summary></summary>
        public new bool Equals(object x, object y)
        {
            if (x == null) return false;
            return x.Equals(y);
        }
        /// <summary></summary>
        public int GetHashCode(object x) { return x.GetHashCode(); }
        /// <summary></summary>
        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var value = NHibernateUtil.UtcDateTime.NullSafeGet(rs, names[0]);
            if (value == null) return null;
            return (DateTime)value;
        }
        /// <summary></summary>
        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            if (value == null) {
                NHibernateUtil.UtcDateTime.NullSafeSet(cmd, null, index);
                return;
            }
            NHibernateUtil.UtcDateTime.NullSafeSet(cmd, value, index);
        }
        /// <summary></summary>
        public object DeepCopy(object value)
        {
            if (value == null) return null;
            var copy = ((DateTime)value);
            return copy;
        }
        /// <summary></summary>
        public bool IsMutable { get { return false; } }
        /// <summary></summary>
        public object Replace(object original, object target, object owner) { return original; }
        /// <summary></summary>
        public object Assemble(object cached, object owner) { return DeepCopy(cached); }
        /// <summary></summary>
        public object Disassemble(object value) { return DeepCopy(value); }
        /// <summary></summary>
        public int Compare(object x, object y) { return ((DateTime)x).CompareTo((DateTime)y); }
        /// <summary></summary>
        public object Seed(ISessionImplementor session) { return DateTime.UtcNow; }
        /// <summary></summary>
        public object Next(object current, ISessionImplementor session) { return current; }
        /// <summary></summary>
        public object FromXMLString(string xml) { return DateTime.Parse(xml); }
        /// <summary></summary>
        public string ObjectToSQLString(object value) { return value as string; }
        /// <summary></summary>
        public string ToXMLString(object value) { return value as string; }
    }
}