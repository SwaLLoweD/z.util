﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;
using System.Net;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace Z.Data.NHTools.UserTypes
{
    /// <summary>IPAddress Mapping for NHibernate (as byte[] - IPv6 compliant)</summary>
    public class IPAddressUserType : IUserType
    {
        /// <summary></summary>
        public bool IsMutable { get { return false; } }
        /// <summary></summary>
        public Type ReturnedType { get { return typeof(IPAddress); } }
        /// <summary></summary>
        public SqlType[] SqlTypes { get { return new SqlType[] { new SqlType(DbType.Binary, 127) }; } }
        /// <summary></summary>
        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            object obj = NHibernateUtil.Binary.NullSafeGet(rs, names[0]);
            if (obj == null) return null;
            var bytes = obj as byte[];
            var rval = new IPAddress(bytes); //create from bytes
            return rval;
            //var obj = NHibernateUtil.String.NullSafeGet(rs, names[0]);
            //if (obj == null) return null;

            //var ip = new IPAddress((byte[])obj);
            //return ip;
        }
        /// <summary></summary>
        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            var parameter = (IDataParameter)cmd.Parameters[index];
            parameter.Value = value == null ? DBNull.Value : (object)((IPAddress)value).GetAddressBytes();

            //if (value == null) {
            //    ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
            //}
            //else {
            //    var ip = (IPAddress)value;
            //    ((IDataParameter)cmd.Parameters[index]).Value = ip.GetAddressBytes();
            //}
        }
        /// <summary></summary>
        public object DeepCopy(object value)
        { return value == null ? null : new IPAddress((value as IPAddress).GetAddressBytes()); }
        /// <summary></summary>
        public object Replace(object original, object target, object owner) { return original; }
        /// <summary></summary>
        public object Assemble(object cached, object owner) { return cached; }
        /// <summary></summary>
        public object Disassemble(object value) { return value; }
        /// <summary></summary>
        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (x == null || y == null) return false;
            return x.Equals(y);
        }
        /// <summary></summary>
        public int GetHashCode(object x) { return x.GetHashCode(); }
    }
}