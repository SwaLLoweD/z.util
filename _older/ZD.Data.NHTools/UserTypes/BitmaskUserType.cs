﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using Z.Util;

namespace Z.Data.NHTools.UserTypes
{
    #region Bitmask<T>

    /// <summary>Bitmask Mapping for NHibernate (as Int64)</summary>
    public class BitmaskUserType<T> : IUserType
    {
        /// <summary></summary>
        public bool IsMutable { get { return false; } }
        /// <summary></summary>
        public virtual Type ReturnedType { get { return typeof(Bitmask<T>); } }
        /// <summary></summary>
        public SqlType[] SqlTypes { get { return new SqlType[] { new SqlType(DbType.Int64) }; } }
        /// <summary></summary>
        public virtual object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            object obj = NHibernateUtil.Int64.NullSafeGet(rs, names[0]);
            if (obj == null) return null;
            var bmask = new Bitmask<T>((long)obj);
            return bmask;
        }
        /// <summary></summary>
        public virtual void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            var parameter = (IDataParameter)cmd.Parameters[index];
            parameter.Value = value == null ? DBNull.Value : (object)((Bitmask<T>)value).Mask;
        }
        /// <summary></summary>
        public virtual object DeepCopy(object value)
        { return value == null ? null : new Bitmask<T>((value as Bitmask<T>).Mask); }
        /// <summary></summary>
        public object Replace(object original, object target, object owner) { return original; }
        /// <summary></summary>
        public object Assemble(object cached, object owner) { return cached; }
        /// <summary></summary>
        public object Disassemble(object value) { return value; }
        /// <summary></summary>
        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (x == null || y == null) return false;
            return x.Equals(y);
        }
        /// <summary></summary>
        public int GetHashCode(object x) { return x.GetHashCode(); }
    }

    #endregion Bitmask<T>

    #region Bitmask

    /// <inheritdoc/>
    public class BitmaskUserType : BitmaskUserType<short>
    {
        /// <inheritdoc/>
        public override Type ReturnedType { get { return typeof(Bitmask); } }
        /// <inheritdoc/>
        public override object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            object obj = NHibernateUtil.Int64.NullSafeGet(rs, names[0]);
            if (obj == null) return null;
            var bmask = new Bitmask((long)obj);
            return bmask;
        }
    }

    #endregion Bitmask
}