﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using NHibernate.Linq.Functions;

namespace Z.Data.NHTools.UserTypes.LinqExt
{
    /// <summary>Custom Linq extensions</summary>
    public class ExtendedLinqtoHqlGeneratorsRegistry : DefaultLinqToHqlGeneratorsRegistry
    {
        /// <summary></summary>
        public ExtendedLinqtoHqlGeneratorsRegistry()
        {
            //RegisterGenerator(ReflectionHelper.GetMethod(() => MyLinqExtensions.IsLike(null, null)), new IsLikeGenerator());
            this.Merge(new BitmaskHasGenerator());
            this.Merge(new BitmaskNotHasGenerator());
            this.Merge(new BitmaskHasAnyGenerator());
            this.Merge(new ExtObjectCoalesceGenerator());
            this.Merge(new ExtObjectConvertGenerator());
            this.Merge(new ExtObjectConvertTGenerator());
            this.Merge(new DateTimeAddDaysGenerator());
            this.Merge(new DateTimeAddSecondsGenerator());
            this.Merge(new DateTimeAddGenerator());
        }
    }
}