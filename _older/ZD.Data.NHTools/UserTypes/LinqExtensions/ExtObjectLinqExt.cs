﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using NHibernate;
using NHibernate.Hql.Ast;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using Z.Extensions;
using Z.Util;

namespace Z.Data.NHTools.UserTypes.LinqExt
{
    #region Object.Coalesce<T>(object)

    /// <summary>Object Coalesce extension method linq extension</summary>
    public class ExtObjectCoalesceGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public ExtObjectCoalesceGenerator()
        { SupportedMethods = new[] { ReflectionHelper.GetMethodDefinition(() => ExtObject.Coalesce<object>(null, null)) }; }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        { return (treeBuilder.Coalesce(visitor.Visit(arguments[0]).AsExpression(), visitor.Visit(arguments[1]).AsExpression())); }
    }

    #endregion Object.Coalesce<T>(object)

    #region Object.Convert<T>(object, IFormat)

    /// <summary>Object Convert extension method linq extension</summary>
    public class ExtObjectConvertTGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public ExtObjectConvertTGenerator()
        { SupportedMethods = new[] { ReflectionHelper.GetMethodDefinition(() => ExtObject.To(null).Type<object>(null, null)) }; }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        { return (treeBuilder.Cast(visitor.Visit(arguments[0]).AsExpression(), method.ReturnType)); }
    }

    #endregion Object.Convert<T>(object, IFormat)

    #region Object.Convert(object, Type, IFormat)

    /// <summary>Object Convert extension method linq extension</summary>
    public class ExtObjectConvertGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public ExtObjectConvertGenerator()
        { SupportedMethods = new[] { ReflectionHelper.GetMethodDefinition(() => ExtObject.To(null).Type(typeof(string), null, null)) }; }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        { return (treeBuilder.Cast(visitor.Visit(arguments[0]).AsExpression(), (arguments[1] as ConstantExpression).Value as Type)); }
    }

    #endregion Object.Convert(object, Type, IFormat)
}