﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;
using NHibernate.Hql.Ast;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;

namespace Z.Data.NHTools.UserTypes.LinqExt
{
    #region DateTime.Add()

    /// <summary>Bitmask, Has method linq extension</summary>
    public class DateTimeAddGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public DateTimeAddGenerator()
        {
            // the methods call are used only to get info about the signature, the actual parameter is just ignored
            SupportedMethods = new[] {
                ReflectionHelper.GetMethodDefinition<DateTime?>(x => x.Value.Add(TimeSpan.Zero))
            };
        }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            return treeBuilder.MethodCall("AddTimeSpan",
                                                  visitor.Visit(targetObject).AsExpression(),
                                                  visitor.Visit(arguments[0]).AsExpression()
                                                  );
        }
    }

    #endregion DateTime.Add()

    #region DateTime.AddDays()

    /// <summary>Bitmask, Has method linq extension</summary>
    public class DateTimeAddDaysGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public DateTimeAddDaysGenerator()
        {
            // the methods call are used only to get info about the signature, the actual parameter is just ignored
            SupportedMethods = new[] {
                ReflectionHelper.GetMethodDefinition<DateTime?>(x => x.Value.AddDays(0))
            };
        }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            return treeBuilder.MethodCall("AddDays",
                                                  visitor.Visit(targetObject).AsExpression(),
                                                  visitor.Visit(arguments[0]).AsExpression()
                                                  );
        }
    }

    #endregion DateTime.AddDays()

    #region DateTime.AddSeconds()

    /// <summary>Bitmask, NotHas method linq extension</summary>
    public class DateTimeAddSecondsGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public DateTimeAddSecondsGenerator()
        {
            // the methods call are used only to get info about the signature, the actual parameter is just ignored
            SupportedMethods = new[] {
                ReflectionHelper.GetMethodDefinition<DateTime?>(x => x.Value.AddSeconds(0))
            };
        }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            return treeBuilder.MethodCall("AddSeconds",
                                                  visitor.Visit(targetObject).AsExpression(),
                                                  visitor.Visit(arguments[0]).AsExpression()
                                                  );
        }
    }

    #endregion DateTime.AddSeconds()
}