﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using NHibernate;
using NHibernate.Hql.Ast;
using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using Z.Extensions;
using Z.Util;

namespace Z.Data.NHTools.UserTypes.LinqExt
{
    #region Bitmask.Has()

    /// <summary>Bitmask, Has method linq extension</summary>
    public class BitmaskHasGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public BitmaskHasGenerator()
        {
            // the methods call are used only to get info about the signature, the actual parameter is just ignored
            SupportedMethods = new[] {
                ReflectionHelper.GetMethodDefinition<Bitmask>(x => x.Has(0)),
                ReflectionHelper.GetMethodDefinition<Bitmask<int>>(x=> x.Has(0))
                //ReflectionHelper.GetMethodDefinition<Bitmask<int>>(x => x.Equals(0))
            };
        }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            var arg = arguments[0];
            var mask = Bitmask.GetMask((arg as ConstantExpression).Value);
            var getMask = treeBuilder.Constant(mask).AsExpression();

            var bitwiseComp = treeBuilder.BitwiseAnd(
                visitor.Visit(targetObject).AsExpression(),
                getMask)
                .AsExpression();

            return treeBuilder.Equality(bitwiseComp, getMask);
        }
    }

    #endregion Bitmask.Has()

    #region Bitmask.NotHas()

    /// <summary>Bitmask, NotHas method linq extension</summary>
    public class BitmaskNotHasGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public BitmaskNotHasGenerator()
        {
            // the methods call are used only to get info about the signature, the actual parameter is just ignored
            SupportedMethods = new[] {
                ReflectionHelper.GetMethodDefinition<Bitmask>(x => x.NotHas(0)),
                ReflectionHelper.GetMethodDefinition<Bitmask<int>>(x => x.NotHas(0))
            };
        }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            var arg = arguments[0];
            var mask = Bitmask.GetMask((arg as ConstantExpression).Value);
            var getMask = treeBuilder.Constant(mask).AsExpression();

            var bitwiseComp = treeBuilder.BitwiseAnd(
                visitor.Visit(targetObject).AsExpression(),
                getMask)
                .AsExpression();

            return treeBuilder.Equality(bitwiseComp, treeBuilder.Constant((long)0).AsExpression());
        }
    }

    #endregion Bitmask.NotHas()

    #region Bitmask.HasAny()

    /// <summary>Bitmask, HasAny method linq extension</summary>
    public class BitmaskHasAnyGenerator : BaseHqlGeneratorForMethod
    {
        /// <summary></summary>
        public BitmaskHasAnyGenerator()
        {
            // the methods call are used only to get info about the signature, the actual parameter is just ignored
            SupportedMethods = new[] {
                ReflectionHelper.GetMethodDefinition<Bitmask>(x => x.HasAny(0)),
                ReflectionHelper.GetMethodDefinition<Bitmask<int>>(x => x.HasAny(0))
        };
        }
        /// <inheritdoc/>
        public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor)
        {
            var arg = arguments[0];
            var mask = Bitmask.GetMask((arg as ConstantExpression).Value);
            var getMask = treeBuilder.Constant(mask).AsExpression();

            var bitwiseComp = treeBuilder.BitwiseAnd(
                visitor.Visit(targetObject).AsExpression(),
                getMask)
                .AsExpression();

            return treeBuilder.Inequality(bitwiseComp, treeBuilder.Constant((long)0).AsExpression());
        }
    }

    #endregion Bitmask.HasAny()
}