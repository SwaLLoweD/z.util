using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using NHibernate;
using Z.Data.Common;
using Z.Extensions;
using Z.Util;

namespace Z.Data.NHTools
{
    public partial class Repo
    {
        #region LoadAllData / Import
        /// <summary>Load All Lazily-loaded properties for specified entity</summary>
        /// <param name="entity">Entity to load all properties for</param>
        /// <param name="excludeProps">Properties to skip</param>
        /// <typeparam name="T">Entity Type</typeparam>
        public static void LoadAllData<T>(T entity, params string[] excludeProps)
        {
            if (entity == null) return;
            NHibernateUtil.Initialize(entity);
            var piS = entity.GetType().GetProperties();
            foreach (var pi in piS) {
                if (excludeProps != null && excludeProps.Any(x => x == pi.Name)) continue;
                if (!pi.CanRead) continue;
                var val = pi.GetValue(entity, null) as IList;
                if (val != null) {
                    try { NHibernateUtil.Initialize(val); }
                    catch { }
                }
            }
        }
        /// <summary>Load All Lazily-loaded properties for specified entity</summary>
        /// <param name="entity">Entity to load all properties for</param>
        /// <param name="excludeProps">Properties to skip</param>
        /// <typeparam name="T">Entity Type</typeparam>
        public static void LoadAllData<T>(T entity, params Expression<Func<T, object>>[] excludeProps)
        {
            string[] exProps = excludeProps == null ? null : new string[excludeProps.Length];
            for (int i = 0; i < excludeProps.Length; i++)
                exProps[i] = Fnc.LambdaPropertyName<T>(excludeProps[i]);
            LoadAllData<T>(entity, exProps);
        }
        #endregion LoadAllData / Import

        #region Save/Delete

        #region Transactions
        private ICollection<object> doTransaction<T>(bool commit, ChangeTypes changeType, IEnumerable<T> entities, int? batchSize)
        {
            var rval = new List<object>();
            if (entities == null) return rval;
            var cnt = entities.Count();
            if (cnt <= 0) return rval;
            rval = new List<object>(cnt);

            Func<object, object> deleg = null;

            deleg = chooseTransactionDeleg(changeType);

            if (commit) {
                var batchSizeInt = batchSize ?? calcBatchSize(cnt);

                #region Commit
                IStatelessSession sSession = null;
                ITransaction transaction = null;

                if (cnt > Config.WriteSessionBatchLimit && changeType != ChangeTypes.SaveOrUpdate) {

                    #region Stateless Session
                    sSession = Config.SessionGetStateless();
                    if (batchSizeInt > 0) sSession.SetBatchSize(batchSizeInt);
                    transaction = sSession.BeginTransaction();
                    deleg = chooseTransactionDeleg(changeType, sSession);
                    #endregion Stateless Session
                }
                else {
                    if (batchSizeInt > 0) Session.SetBatchSize(batchSizeInt);
                    transaction = Session.BeginTransaction();
                }
                if (deleg == null) return rval; //Nothing to do
                object currentEntity = null;

                foreach (var e in entities) {
                    if (e == null) continue;
                    currentEntity = e;
                    //e.UpdateDateModified();
                    var rEntity = deleg(e);
                    if (rEntity != null) rval.Add(rEntity);
                    else {
                        //e.UpdateDateModified();
                        deleg(e);
                    }
                }
                try { transaction.Commit(); }
                catch (Exception exp) {
                    transaction.Rollback();
                    Clear();
                    //if (typeof(T).Is().CastableTo<SKLog>()) throw new Exception("Can not write to db");
                    EventCall(onError, this, new RepositoryEventArgs("Persistance Error", typeof(T), currentEntity, exp));
                    throw exp;
                }
                finally {
                    transaction.Dispose();
                    if (sSession != null) {
                        sSession.Close();
                        sSession.Dispose();
                    }
                    else {
                        Clear();
                    }
                }
                #endregion Commit
            }
            else {

                #region Add to session
                if (batchSize.HasValue) Session.SetBatchSize(batchSize.Value);
                foreach (var e in entities) {
                    if (e == null) continue;
                    //e.UpdateDateModified();
                    object rEntity = null;
                    try { rEntity = deleg(e); }
                    catch (Exception exp) {
                        Clear();
                        EventCall(onError, this, new RepositoryEventArgs("Persistance Error", typeof(T), e, exp));
                        throw exp;
                    }
                    if (rEntity != null) rval.Add(rEntity);
                }
                #endregion Add to session
            }
            return rval;
        }

        private Func<object, object> chooseTransactionDeleg(ChangeTypes changeType, IStatelessSession sSession = null)
        {
            Func<object, object> deleg = null;
            if (sSession != null) {
                switch (changeType) {
                    case ChangeTypes.Delete:
                        deleg = x => { sSession.Delete(x); return null; };
                        break;

                    case ChangeTypes.Persist:
                    case ChangeTypes.Save:
                        deleg = sSession.Insert;
                        break;
                    //case ChangeTypes.SaveOrUpdate:
                    //    deleg = x => { sSession.SaveOrUpdate(x); return null; };
                    //    break;
                    case ChangeTypes.Update:
                        deleg = x => { sSession.Update(x); return null; };
                        break;
                }
            }
            else {
                switch (changeType) {
                    case ChangeTypes.Delete:
                        deleg = x => { Session.Delete(x); return null; };
                        break;

                    case ChangeTypes.Persist:
                        deleg = x => { Session.Persist(x); return null; };
                        break;

                    case ChangeTypes.Save:
                        deleg = Session.Save;
                        break;

                    case ChangeTypes.SaveOrUpdate:
                        deleg = x => { Session.SaveOrUpdate(x); return null; };
                        break;

                    case ChangeTypes.Update:
                        deleg = x => { Session.Update(x); return null; };
                        break;
                }
            }
            return deleg;
        }
        #endregion Transactions

        /// <summary>Import (Reattach-lock) specified entities to session</summary>
        public void Import<T>(params T[] entities)
        {
            foreach (var e in entities)
                Session.Lock(e, LockMode.None);
        }
        /// <summary>Detach (evict) specified entities from session</summary>
        public void Detach<T>(params T[] entities)
        {
            foreach (var e in entities)
                Session.Evict(e);
        }
        /// <summary>Try to detach (evict) specified entities from session</summary>
        /// <returns>True if detach successful, False if detach failed</returns>
        public bool TryDetach<T>(params T[] entities)
        {
            bool rval = true;
            foreach (var e in entities) {
                try {
                    Session.Evict(e);
                }
                catch { rval = false; }
            }
            return rval;
        }
        /// <inheritdoc/>
        public void Refresh<T>(params T[] entities)
        {
            foreach (var e in entities)
                Session.Refresh(e, LockMode.None);
        }
        //public T Merge<T>(T entity) where T : Entity { return entity = Session.Merge<T>(entity); }
        /// <summary>Merges specified entities into session</summary>
        public T[] Merge<T>(params T[] entities) where T : class
        {
            var rval = new HashSet<T>();
            foreach (var e in entities)
                rval.AddUnique(Session.Merge<T>(e));
            return rval.ToArray();
        }
        /// <inheritdoc/>
        public void UpdateMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) { doTransaction<T>(commit, ChangeTypes.Update, entities, batchSize); }
        /// <inheritdoc/>
        public void Update<T>(T entity, bool commit = true) { UpdateMany<T>(new T[] { entity }, commit); }
        /// <inheritdoc/>
        public ICollection<object> SaveMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) { return doTransaction<T>(commit, ChangeTypes.Save, entities, batchSize); }
        /// <inheritdoc/>
        public ICollection<object> Save<T>(T entity, bool commit = true) { return SaveMany<T>(new T[] { entity }, commit); }
        /// <summary>Persists specified new entities into db (and may commit changes without waiting for session flush)</summary>
        public void PersistMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) { doTransaction<T>(commit, ChangeTypes.Persist, entities, batchSize); }
        /// <summary>Persists specified new entities into db (and may commit changes without waiting for session flush)</summary>
        public void Persist<T>(T entity, bool commit = true) { PersistMany<T>(new T[] { entity }, commit); }
        /// <inheritdoc/>
        public void SaveOrUpdateMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) { doTransaction<T>(commit, ChangeTypes.SaveOrUpdate, entities, batchSize); }
        /// <inheritdoc/>
        public void SaveOrUpdate<T>(T entity, bool commit = true) { SaveOrUpdateMany<T>(new T[] { entity }, commit); }
        /// <inheritdoc/>
        public void DeleteMany<T>(IEnumerable<T> entities, bool commit = true, int? batchSize = null) { doTransaction<T>(commit, ChangeTypes.Delete, entities, batchSize); }
        /// <inheritdoc/>
        public void Delete<T>(T entity, bool commit = true) { DeleteMany<T>(new T[] { entity }, commit); }

        //private int batchCounter = 0;
        //private int batchSize = 10;
        private int calcBatchSize(int entitiesCount)
        {
            var batchSize = Config.WriteBatchSize;
            if (batchSize == 0) return 0; //Batching is disabled
            //batchCounter = 0;
            int cnt = entitiesCount;
            if (cnt < 10) batchSize = Config.WriteBatchSize;
            else if (cnt < 100) {
                int bs = (int)(cnt * 0.1f);
                batchSize = (bs < 1) ? 1 : bs;
            }
            else batchSize = 10;
            return batchSize;
            //Session.SetBatchSize(batchSize);
        }
        //private bool batchItem()
        //{
        //    if (batchSize <= Config.ADONetBatchSize) return false;
        //    if ((batchCounter++ % batchSize) == 0) return true;
        //    return false;
        //}
        #endregion Save/Delete
    }
}