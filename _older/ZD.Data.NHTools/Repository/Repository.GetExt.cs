using System.Collections;
using System.Collections.Generic;
using System.Linq;
/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using NHibernate;
using NHibernate.Engine;
using NHibernate.Hql.Ast.ANTLR;
using NHibernate.Linq;
using Z.Data.Common;
using Z.Data.NHTools;

namespace Z.Extensions
{
    /// <summary>Repository Get Extensions</summary>
    public static class RepositoryGetExtensions
    {
        /// <summary>Return HQL Query result</summary>
        /// <param name="HQLquery">HQL Query</param>
        /// <param name="getPart"></param>
        public static IList<T> ByHQL<T>(this IRepoDbGetPartNH<T> getPart, string HQLquery)
        {
            var repo = getPart.Repository as Repo;
            using (ITransaction transaction = (repo.Session.BeginTransaction())) {
                IQuery query = repo.Session.CreateQuery(HQLquery);
                IList<T> rval = query.List<T>();
                transaction.Commit();
                return rval;
            }
        }

        /// <summary>Return GeneratedSQL</summary>
        public static string GenerateSQL<T>(this IRepoDbGetPartNH<T> getPart, IQueryable queryable)
        {
            var session = (getPart.Repository as Repo).Session;
            var sessionImp = (ISessionImplementor)session;
            var nhLinqExpression = new NhLinqExpression(queryable.Expression, sessionImp.Factory);
            var translatorFactory = new ASTQueryTranslatorFactory();
            var translators = translatorFactory.CreateQueryTranslators(nhLinqExpression, nhLinqExpression.Key, false, sessionImp.EnabledFilters, sessionImp.Factory);

            return translators[0].SQLString;
        }

        //public static string GenerateSQL<T>(this IRepoDbGetPartNH<T> getPart, ICriteria criteria)
        //{
        //    var criteriaImpl = (CriteriaImpl)criteria;
        //    var sessionImpl = (SessionImpl)criteriaImpl.Session;
        //    var factory = (SessionFactoryImpl)sessionImpl.SessionFactory;
        //    var implementors = factory.GetImplementors(criteriaImpl.EntityOrClassName);
        //    var loader = new CriteriaLoader((NHibernate.Persister.Entity.IOuterJoinLoadable)factory.GetEntityPersister(implementors[0]), factory, criteriaImpl, implementors[0], sessionImpl.EnabledFilters);

        //    return loader.SqlString.ToString();
        //}

        //public static string GenerateSQL<T>(this IRepoDbGetPartNH<T> getPart, IQueryOver queryOver)
        //{
        //    return GenerateSQL<T>(getPart, queryOver.UnderlyingCriteria);
        //}

        //public static string GenerateSQL<T>(this IRepoDbGetPartNH<T> getPart, IQuery query)
        //{
        //    var session = (getPart.Repository as Repo).Session;
        //    var sessionImp = (ISessionImplementor)session;
        //    var translatorFactory = new ASTQueryTranslatorFactory();
        //    var translators = translatorFactory.CreateQueryTranslators(query.QueryString, null, false, sessionImp.EnabledFilters, sessionImp.Factory);

        //    return translators[0].SQLString;
        //}
    }
}