using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using NHibernate;
using NHibernate.Linq;
using NHibernate.Proxy;

namespace Z.Data.NHTools
{
    public partial class Repo
    {
        /// <inheritdoc/>
        public bool IsMapped<T>(T entity)
        {
            var nhProxy = NHibernateProxyHelper.GuessClass(entity);
            if (nhProxy == null) return false;
            string className = nhProxy.FullName;
            var sessionImpl = Session.GetSessionImplementation();
            var persistenceContext = sessionImpl.PersistenceContext;
            var persister = sessionImpl.Factory.GetEntityPersister(className);
            return persister != null;
        }

        #region IsDirty
        /// <inheritdoc/>
        public bool IsDirtyEntity<T>(T entity)
        {
            String className = NHibernateProxyHelper.GuessClass(entity).FullName;
            var sessionImpl = Session.GetSessionImplementation();
            var persistenceContext = sessionImpl.PersistenceContext;
            var persister = sessionImpl.Factory.GetEntityPersister(className);
            var oldEntry = persistenceContext.GetEntry(entity);

            if ((oldEntry == null) && (entity is INHibernateProxy)) {
                INHibernateProxy proxy = entity as INHibernateProxy;
                Object obj = persistenceContext.Unproxy(proxy);
                oldEntry = persistenceContext.GetEntry(obj);
            }
            if (oldEntry == null || oldEntry.LoadedState == null) return true; //New entry

            Object[] currentState = persister.GetPropertyValues(entity, sessionImpl.EntityMode);
            Int32[] dirtyProps = persister.FindDirty(currentState, oldEntry.LoadedState, entity, sessionImpl);

            return (dirtyProps != null);
        }
        /// <inheritdoc/>
        public bool IsDirtyProperty<T>(T entity, String propertyName)
        {
            String className = NHibernateProxyHelper.GuessClass(entity).FullName;
            var sessionImpl = Session.GetSessionImplementation();
            var persistenceContext = sessionImpl.PersistenceContext;
            var persister = sessionImpl.Factory.GetEntityPersister(className);
            var oldEntry = sessionImpl.PersistenceContext.GetEntry(entity);

            if ((oldEntry == null) && (entity is INHibernateProxy)) {
                INHibernateProxy proxy = entity as INHibernateProxy;
                Object obj = sessionImpl.PersistenceContext.Unproxy(proxy);
                oldEntry = sessionImpl.PersistenceContext.GetEntry(obj);
            }

            Object[] oldState = oldEntry.LoadedState;
            Object[] currentState = persister.GetPropertyValues(entity, sessionImpl.EntityMode);
            Int32[] dirtyProps = persister.FindDirty(currentState, oldState, entity, sessionImpl);
            Int32 index = Array.IndexOf(persister.PropertyNames, propertyName);

            Boolean isDirty = (dirtyProps != null) ? (Array.IndexOf(dirtyProps, index) != -1) : false;

            return (isDirty);
        }
        /// <inheritdoc/>
        public object GetOriginalEntityProperty<T>(T entity, String propertyName)
        {
            String className = NHibernateProxyHelper.GuessClass(entity).FullName;
            var sessionImpl = Session.GetSessionImplementation();
            var persistenceContext = sessionImpl.PersistenceContext;
            var persister = sessionImpl.Factory.GetEntityPersister(className);
            var oldEntry = sessionImpl.PersistenceContext.GetEntry(entity);

            if ((oldEntry == null) && (entity is INHibernateProxy)) {
                INHibernateProxy proxy = entity as INHibernateProxy;
                Object obj = sessionImpl.PersistenceContext.Unproxy(proxy);
                oldEntry = sessionImpl.PersistenceContext.GetEntry(obj);
            }

            Object[] oldState = oldEntry.LoadedState;
            Object[] currentState = persister.GetPropertyValues(entity, sessionImpl.EntityMode);
            Int32[] dirtyProps = persister.FindDirty(currentState, oldState, entity, sessionImpl);
            Int32 index = Array.IndexOf(persister.PropertyNames, propertyName);

            Boolean isDirty = (dirtyProps != null) ? (Array.IndexOf(dirtyProps, index) != -1) : false;

            return ((isDirty == true) ? oldState[index] : currentState[index]);
        }

        #endregion IsDirty

        #region Query* Methods
        /// <summary>Return NHibernate Criteria for entity type</summary>
        /// <typeparam name="T">Entity Type</typeparam>
        public ICriteria CreateCriteria<T>()
        {
            using (ITransaction transaction = Session.BeginTransaction()) {
                ICriteria rval = Session.CreateCriteria(typeof(T));
                transaction.Commit();
                return rval;
            }
        }
        /// <summary>Return NHibernate Criteria for entity Type</summary>
        /// <param name="t">Entity Type</param>
        public ICriteria CreateCriteria(Type t)
        {
            using (ITransaction transaction = Session.BeginTransaction()) {
                ICriteria rval = Session.CreateCriteria(t);
                transaction.Commit();
                return rval;
            }
        }
        /// <summary>Return HQL Query</summary>
        /// <param name="hqlQuery">HQL Query</param>
        public IQuery Hql(string hqlQuery) { return Session.CreateQuery(hqlQuery); }
        /// <inheritdoc/>
        public IQueryable<T> LinQ<T>(Expression<Func<T, bool>> where = null)
        {
            IQueryable<T> rval = Session.Query<T>();
            if (where != null) rval = rval.Where<T>(where) as IQueryable<T>;
            return rval;
        }
        /// <summary>Return NHibernate Queryover Queryable for Entity Type</summary>
        /// <typeparam name="T">Entity Type</typeparam>
        public IQueryOver<T, T> QueryOver<T>() where T : class
        {
            var rval = Session.QueryOver<T>();
            return rval;
        }
        /// <summary>Return NHibernate Queryover Queryable for Entity Type</summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="alias"></param>
        public IQueryOver<T, T> QueryOver<T>(Expression<Func<T>> alias) where T : class
        {
            var rval = Session.QueryOver<T>(alias);
            return rval;
        }
        #endregion Query* Methods

        #region Execute Methods
        /// <inheritdoc/>
        public object Execute(string SQLquery)
        {
            using (ITransaction transaction = Session.BeginTransaction()) {
                IQuery query = Session.CreateSQLQuery(SQLquery);
                var rval = query.ExecuteUpdate();
                transaction.Commit();
                return rval;
            }
        }
        /// <inheritdoc/>
        public object Execute(IDbCommand cmd)
        {
            using (ITransaction transaction = Session.BeginTransaction()) {
                transaction.Enlist(cmd);
                var rval = cmd.ExecuteScalar();
                transaction.Commit();
                return rval;
            }
        }
        /// <inheritdoc/>
        public IDbCommand CreateCommand()
        {
            var rval = ActiveConnection.CreateCommand();
            rval.CommandTimeout = Config.CommandTimeout;
            return rval;
        }
        #endregion Execute Methods
    }
}