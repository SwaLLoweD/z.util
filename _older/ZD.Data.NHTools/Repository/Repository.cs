/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using NHibernate;
using Z.Collections.Generic;
using Z.Data.Common;
using Z.Util;
using Z.Extensions;

namespace Z.Data.NHTools
{
    /// <summary>Db Data Source Repository</summary>
    public partial class Repo : IRepoDbNH, IDisposable
    {
        private bool _disposed = false;

        /// <summary>Db Session (should be closed if not web type)</summary>
        public ISession Session { get { return Config.SessionGet(); } }

        [ThreadStatic]
        private static IDictionary<string, Repo> _repoTs;
        private static IDictionary<string, Repo> repoTs
        {
            get
            {
                if (_repoTs == null)
                    _repoTs = new Dictionary<string, Repo>();
                return _repoTs;
            }
        }
        /// <summary>Thread Static repo (Closed manually)</summary>
        public static Repo Ts
        {
            get
            {
                if (repoTs.TryGetValue("default") == null) {
                    repoTs["default"] = new Repo();
                }
                return repoTs.TryGetValue("default");
            }
        }
        /// <summary>Gets or sets the current config.</summary>
        public NHConfig Config { get; set; }

        /// <summary>constructor</summary>
        public Repo(string configName = "default") { Config = NHConfig.ConfigGet(configName); }

        /// <inheritdoc/>
        public IRepoDbGetPart<T> Get<T>() { return new RepoGetPart<T>(this); }

        /// <summary>Gets the db connection.</summary>
        public static System.Data.IDbConnection GetDbConnection(Repo repo) { return repo.ActiveConnection; }
        /// <inheritdoc/>
        public System.Data.IDbConnection ActiveConnection { get { return Session.Connection; } }

        /// <inheritdoc/>
        public void Clear() { Session.Clear(); }
        /// <inheritdoc/>
        public void Flush() { Session.Flush(); }
        /// <inheritdoc/>
        public void Close() { Config.SessionClose(); }
        /// <inheritdoc/>
        public bool IsOpen { get { return Session != null && Session.IsOpen; } }

        /// <inheritdoc/>
        public ITransactionDb BeginTransaction(System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.Unspecified)
        {
            Transaction = new Transaction(Session, isolationLevel);
            return Transaction;
        }
        private ITransactionDb transaction;

        /// <inheritdoc/>
        public ITransactionDb Transaction
        {
            get
            {
                if (transaction == null) return null;
                if (Session.Transaction == null || !Session.Transaction.Equals(transaction)) {
                    transaction.Dispose();
                    transaction = null;
                }
                return Transaction;
            }
            private set { transaction = value; }
        }

        #region Dispose
        /// <inheritdoc/>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary></summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed) {
                if (disposing) {
                    //DetachEvents();
                    //SessionFlush();
                    Close();
                    if (Config != null && _repoTs != null && _repoTs[Config.Name] != null) {
                        _repoTs.Remove(Config.Name);
                    }
                }
                // Call the appropriate methods to clean up
                // unmanaged resources here.
                _disposed = true;
            }
        }
        #endregion Dispose
    }
}