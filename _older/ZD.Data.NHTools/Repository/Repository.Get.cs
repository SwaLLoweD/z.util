using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Z.Data.Common;
using Z.Util;

namespace Z.Data.NHTools
{
    public partial class Repo : IRepoDbNH
    {
        #region Get Methods

        /// <summary>Get Methods</summary>
        public class RepoGetPart<T> : IRepoDbGetPartNH<T>
        {
            /// <inheritdoc/>
            public IRepoDb Repository { get; set; }
            private Repo repo { get { return Repository as Repo; } }
            /// <summary></summary>
            public RepoGetPart(Repo repository) { Repository = repository; }

            /// <inheritdoc/>
            public T ById<U>(U id, bool skipData = false)
            {
                T rval = default(T);
                rval = skipData ? rval = repo.Session.Load<T>(id) : repo.Session.Get<T>(id);
                //using (ITransaction transaction = Session.BeginTransaction()) {
                //rval = Session.Get<T>(id);
                //transaction.Commit();
                return rval;
            }
            /// <inheritdoc/>
            public IList<T> ByProperty<U>(Expression<Func<T, U>> prop, params U[] values)
            {
                var pi = Fnc.LambdaProperty(prop);
                if (pi == null) throw new InvalidOperationException();
                if (values.Length < 1000) {
                    return repo.CreateCriteria<T>().Add(Restrictions.InG<U>(pi.Name, values)).List<T>();
                    /*return Hql("from " + typeof(T).Name + " as e where e." + pi.Name + " in (:ids)")
                    .SetParameterList("ids", values.ToList()).Enumerable<T>(); */
                }
                var lookup = new HashSet<U>(values);
                var rval = new List<T>();
                var all = repo.LinQ<T>();
                foreach (var e in all)
                    if (lookup.Contains((U)pi.GetValue(e, null))) rval.Add(e);
                return rval;
            }
            /// <inheritdoc/>
            public IList<T> ByProperty<U>(System.Reflection.PropertyInfo property, params U[] values)
            {
                if (property == null) return null;
                if (values.Length < 1000) {
                    return repo.CreateCriteria(property.DeclaringType).Add(Restrictions.In(property.Name, values)).List<T>();
                    /*return Hql("from " + typeof(T).Name + " as e where e." + pi.Name + " in (:ids)")
                    .SetParameterList("ids", values.ToList()).Enumerable<T>(); */
                }
                var lookup = new HashSet<U>(values);
                var rval = new List<T>();
                var all = repo.LinQ<T>();
                foreach (var e in all)
                    if (lookup.Contains((U)property.GetValue(e, null))) rval.Add(e);
                return rval;
            }
            /// <inheritdoc/>
            public IList<T> ByExample(T exampleInstance, bool excludeNulls = true, bool excludeZeros = false, params string[] propertiesToExclude)
            {
                using (ITransaction transaction = repo.Session.BeginTransaction()) {
                    ICriteria criteria = repo.Session.CreateCriteria(typeof(T));
                    Example example = Example.Create(exampleInstance);

                    if (excludeNulls) example.ExcludeNulls();
                    if (excludeZeros) example.ExcludeZeroes();
                    foreach (string propertyToExclude in propertiesToExclude) {
                        example.ExcludeProperty(propertyToExclude);
                    }
                    criteria.Add(example);
                    IList<T> rval = criteria.List<T>();
                    transaction.Commit();
                    return rval;
                }
            }
            /// <inheritdoc/>
            public IList<T> All()
            {
                return repo.LinQ<T>().ToList();
            }
            /*public IList<T> GetByCriteria<T>(ICriteria criteria) where T : Entity
            { return criteria.List<T>(); }
            public IList<T> GetByCriteria<T>(params ICriterion[] criterion) where T : Entity
            {
                using (ITransaction transaction = Session.BeginTransaction())
                {
                    ICriteria criteria = Session.CreateCriteria(typeof(T));
                    foreach (ICriterion criterium in criterion)
                    {
                        criteria.Add(criterium);
                    }
                    IList<T> rval = criteria.List<T>();
                    transaction.Commit();
                    return rval;
                }
            }*/
            /// <inheritdoc/>
            public IList<T> All(string SQLquery, params object[] parameters)
            {
                using (ITransaction transaction = repo.Session.BeginTransaction()) {
                    IQuery query = repo.Session.CreateSQLQuery(SQLquery);
                    for (int i = 0; i < parameters.Length; i++)
                        query.SetParameter(i, parameters[i]);
                    IList<T> rval = query.SetResultTransformer(Transformers.AliasToBean<T>()).List<T>();
                    //IList<T> rval = query.List<T>();
                    transaction.Commit();
                    return rval;
                }
            }
            /// <inheritdoc/>
            public IList<T> All(IDbCommand cmd)
            {
                using (ITransaction transaction = repo.Session.BeginTransaction()) {
                    IQuery query = repo.Session.CreateSQLQuery(cmd.CommandText);
                    foreach (IDataParameter param in cmd.Parameters)
                        query.SetParameter(param.ParameterName, param.Value);
                    IList<T> rval = query.SetResultTransformer(Transformers.AliasToBean<T>()).List<T>();
                    //IList<T> rval = query.List<T>();
                    transaction.Commit();
                    return rval;
                }
            }
            /// <inheritdoc/>
            public IList<T> Paged(int limit, int? offset = null)
            {
                var query = repo.LinQ<T>();
                if (offset.HasValue) query = query.Skip(offset.Value);
                return query.Take(limit).ToList();
            }
            /// <inheritdoc/>
            public IList<T> Paged(string SQLquery, int limit, int? offset = null, params object[] parameters)
            {
                using (ITransaction transaction = repo.Session.BeginTransaction()) {
                    IQuery query = repo.Session.CreateSQLQuery(SQLquery);
                    IList<T> rval = query.SetMaxResults(limit).SetResultTransformer(Transformers.AliasToBean<T>()).List<T>();
                    //TODO: Skip part is missing
                    //IList<T> rval = query.List<T>();
                    transaction.Commit();
                    return rval;
                }
            }
            /// <inheritdoc/>
            public IList<T> Paged(IDbCommand cmd, int limit, int? offset = null)
            {
                using (ITransaction transaction = repo.Session.BeginTransaction()) {
                    IQuery query = repo.Session.CreateSQLQuery(cmd.CommandText);
                    foreach (IDataParameter param in cmd.Parameters)
                        query.SetParameter(param.ParameterName, param.Value);
                    IList<T> rval = query.SetMaxResults(limit).SetResultTransformer(Transformers.AliasToBean<T>()).List<T>();
                    //TODO: Skip part is missing
                    //IList<T> rval = query.List<T>();
                    transaction.Commit();
                    return rval;
                }
            }
        }

        #endregion Get Methods
    }
}