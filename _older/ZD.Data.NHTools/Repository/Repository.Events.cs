/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using Z.Data.Common;
using Z.Util.Common;

namespace Z.Data.NHTools
{
    /// <summary>Change Event Handler</summary>
    /// <param name="e">Entity being changed</param>
    /// <param name="ct">Change Type</param>
    public delegate void OnEntityChangedEventHandler(object e, ChangeTypes ct);

    /// <summary>Repository Event Handler</summary>
    /// <param name="repo">Repository</param>
    /// <param name="args">Arguments</param>
    public delegate void RepositoryEventHandler(IRepo repo, RepositoryEventArgs args);
    public partial class Repo
    {
        #region Events
        private static object eventLock = new object();
        private static RepositoryEventHandler onError;
        private static OnEntityChangedEventHandler onLoad;
        private static OnEntityChangedEventHandler onChange;
        private static OnEntityChangedEventHandler onDelete;
        private static OnEntityChangedEventHandler onSaveOrUpdate;
        //private static OnEntityChangedEventHandler onUpdated;

        /// <summary>Fired on Entity Load</summary>
        public static event RepositoryEventHandler OnError { add { lock (eventLock) { onError += value; } } remove { lock (eventLock) { onError -= value; } } }

        /// <summary>Fired on Entity Load</summary>
        public static event OnEntityChangedEventHandler OnLoad { add { lock (eventLock) { onLoad += value; } } remove { lock (eventLock) { onLoad -= value; } } }

        /// <summary>Fired on Entity Change</summary>
        public static event OnEntityChangedEventHandler OnChange { add { lock (eventLock) { onChange += value; } } remove { lock (eventLock) { onChange -= value; } } }

        /// <summary>Fired on Entity Delete</summary>
        public static event OnEntityChangedEventHandler OnDelete { add { lock (eventLock) { onDelete += value; } } remove { lock (eventLock) { onDelete -= value; } } }

        /// <summary>Fired on Entity Save or Update</summary>
        public static event OnEntityChangedEventHandler OnSaveOrUpdate { add { lock (eventLock) { onSaveOrUpdate += value; } } remove { lock (eventLock) { onSaveOrUpdate -= value; } } }

        //public static event OnEntityChangedEventHandler OnUpdated { add { lock (eventLock) { onUpdated += value; } } remove { lock (eventLock) { onUpdated -= value; } } }

        /// <summary>Load event trigger</summary>
        internal static void HandleLoad(object e)
        {
            EventCall(onLoad, e, ChangeTypes.None);
        }
        /// <summary>Event Trigger</summary>
        internal static void HandleEvent(object e, ChangeTypes ct)
        {
            switch (ct) {
                case ChangeTypes.Delete:
                    EventCall(onDelete, e, ct);
                    break;

                case ChangeTypes.Save:
                case ChangeTypes.Update:
                case ChangeTypes.SaveOrUpdate:
                    EventCall(onSaveOrUpdate, e, ct);
                    //if (onUpdated != null) onUpdated(e, ct);
                    break;
            }
             EventCall(onChange, e, ct);
            
        }
        #endregion Events

        #region DetachEvents
        /// <summary>Detach all events attached to this repo</summary>
        public static void DetachEvents()
        {
            detachEvent(onLoad);
            detachEvent(onChange);
            detachEvent(onDelete);
            detachEvent(onSaveOrUpdate);
            //detachEvent(onUpdated);

            lock (eventLock) {
                if (onError == null) return;
                var l = onError.GetInvocationList();
                foreach (Delegate d in l) {
                    try { onError -= (RepositoryEventHandler)d; }
                    catch { }
                }
            }
        }
        private static void detachEvent(OnEntityChangedEventHandler e)
        {
            lock (eventLock) {
                if (e == null) return;
                var l = e.GetInvocationList();
                foreach (Delegate d in l) {
                    try { e -= (OnEntityChangedEventHandler)d; }
                    catch { }
                }
            }
        }
        #endregion DetachEvents

        #region EventCall
        /// <summary>Event Caller</summary>
        protected static bool EventCall(OnEntityChangedEventHandler handler, object entity, ChangeTypes changeType, bool lockingEvent = true)
        {
            if (lockingEvent) {
                lock (eventLock) {
                    return eventCall(handler, entity, changeType);
                }
            }
            return eventCall(handler, entity, changeType);
        }

        private static bool eventCall(OnEntityChangedEventHandler handler, object entity, ChangeTypes changeType)
        {
            try {
                if (handler != null) handler(entity, changeType);
            }
            catch (Exception exp) {
                if (onError != null)
                    onError(null, new RepositoryEventArgs("Could not perform outer event call", null, null, exp));
                return false;
            }
            return true;
        }

        /// <summary>Event Caller</summary>
        protected static bool EventCall(RepositoryEventHandler handler, Repo repo, RepositoryEventArgs args, bool lockingEvent = true)
        {
            if (lockingEvent) {
                lock (eventLock) {
                    return eventCall(handler, repo, args);
                }
            }
            return eventCall(handler, repo, args);
        }

        private static bool eventCall(RepositoryEventHandler handler, Repo repo, RepositoryEventArgs args)
        {
            try {
                if (handler != null) handler(repo, args);
            }
            catch (Exception exp) {
                if (onError != null)
                    onError(repo, new RepositoryEventArgs("Could not perform outer event call", null, null, exp));
                return false;
            }
            return true;
        }
        #endregion
    }
}