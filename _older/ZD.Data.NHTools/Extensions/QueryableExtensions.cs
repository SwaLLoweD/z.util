﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NHibernate.Linq;

namespace Z.Data.NHTools
{
    /// <summary>Queryable Extensions (Nhibernate extension overloads)</summary>
    public static partial class ExtQueryable
    {
        /// <summary>Shortcut for NHibernate extension Fetch</summary>
        public static IQueryable<T> Fetch<T, U>(this IQueryable<T> query, Expression<Func<T, U>> relatedObjectSelector)
        {
            return NHibernate.Linq.EagerFetchingExtensionMethods.Fetch(query, relatedObjectSelector);
        }

        /// <summary>Shortcut for NHibernate extension FetchMany</summary>
        public static IQueryable<T> FetchMany<T, U>(this IQueryable<T> query, Expression<Func<T, IEnumerable<U>>> relatedObjectSelector)
        {
            return NHibernate.Linq.EagerFetchingExtensionMethods.FetchMany(query, relatedObjectSelector);
        }

        /// <summary>Shortcut for NHibernate extension ThenFetch</summary>
        public static IQueryable<TQueried> ThenFetch<TQueried, TFetch, TRelated>(this IQueryable<TQueried> query, Expression<Func<TFetch, TRelated>> relatedObjectSelector)
        {
            return NHibernate.Linq.EagerFetchingExtensionMethods.ThenFetch(query as INhFetchRequest<TQueried, TFetch>, relatedObjectSelector);
        }

        /// <summary>Shortcut for NHibernate extension ThenFetchMany</summary>
        public static IQueryable<TQueried> ThenFetchMany<TQueried, TFetch, TRelated>(this IQueryable<TQueried> query, Expression<Func<TFetch, System.Collections.Generic.IEnumerable<TRelated>>> relatedObjectSelector)
        {
            return NHibernate.Linq.EagerFetchingExtensionMethods.ThenFetchMany(query as INhFetchRequest<TQueried, TFetch>, relatedObjectSelector);
        }

        /// <summary>Shortcut for NHibernate extension Cacheable</summary>
        public static IQueryable<T> Cacheable<T>(this IQueryable<T> query) { return NHibernate.Linq.LinqExtensionMethods.Cacheable(query); }
        /// <summary>Shortcut for NHibernate extension CacheMode</summary>
        public static IQueryable<T> CacheMode<T>(this IQueryable<T> query, string cacheMode) { return NHibernate.Linq.LinqExtensionMethods.CacheMode(query, (NHibernate.CacheMode)Enum.Parse(typeof(NHibernate.CacheMode), cacheMode)); }
        /// <summary>Shortcut for NHibernate extension CacheRegion</summary>
        public static IQueryable<T> CacheRegion<T>(this IQueryable<T> query, string region) { return NHibernate.Linq.LinqExtensionMethods.CacheRegion(query, region); }
        /// <summary>Shortcut for NHibernate extension Timeout</summary>
        public static IQueryable<T> Timeout<T>(this IQueryable<T> query, int timeout) { return NHibernate.Linq.LinqExtensionMethods.Timeout(query, timeout); }
        /// <summary>Shortcut for NHibernate extension ToFuture</summary>
        public static IEnumerable<T> ToFuture<T>(this IQueryable<T> query) { return NHibernate.Linq.LinqExtensionMethods.ToFuture(query); }
        /// <summary>Shortcut for NHibernate extension ToFutureValue</summary>
        public static dynamic /*IFutureValue<T>*/ ToFutureValue<T>(this IQueryable<T> query) { return NHibernate.Linq.LinqExtensionMethods.ToFutureValue(query); }
        /// <summary>Shortcut for NHibernate extension ToFutureValue</summary>
        public static dynamic /*IFutureValue<TResult>*/ ToFutureValue<T, TResult>(this IQueryable<T> query, Expression<Func<IQueryable<T>, TResult>> selector) { return NHibernate.Linq.LinqExtensionMethods.ToFutureValue(query, selector); }
    }
}