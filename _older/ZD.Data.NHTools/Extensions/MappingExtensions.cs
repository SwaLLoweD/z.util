﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Validator.Cfg.Loquacious;
using Z.Extensions;
using Z.Util;

namespace Z.Data.NHTools
{
    /// <summary>NHibernate Extensions</summary>
    public static class ExtensionNhibernateMapping
    {
        //public static PropertyPart Length(this PropertyPart p, Enum v) { return p.Length(Convert.ToInt32(v)); }
        /// <summary>Checks string length is between specified numbers</summary>
        public static IChainableConstraint<IStringConstraints> LengthBetween(this IStringConstraints p, Enum f, Enum v) { return p.LengthBetween(Convert.ToInt32(f), Convert.ToInt32(v)); }
        /// <summary>Property Clob/Blob Mapping shortcut</summary>
        public static void PropertyXlob<T, TProperty>(this IComponentElementMapper<T> comp, Expression<Func<T, TProperty>> property, Action<IPropertyMapper> mapping = null)
        {
            comp.Property(property, c => {
                if (typeof(TProperty).Is().CastableTo<string>()) c.Type(NHibernateUtil.StringClob);
                else if (typeof(TProperty).Is().CastableTo<byte[]>()) c.Type(NHibernateUtil.BinaryBlob);
                c.NotNullable(false);
                c.Length(10000);
                if (mapping != null) mapping(c);
            });
        }
        /// <summary>Clob/Blob mapping shortcut method</summary>
        /// <param name="mapper">Mapping class</param>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public static void PropertyXlob<TEntity, TProperty>(this ClassMapping<TEntity> mapper, Expression<Func<TEntity, TProperty>> property, Action<IPropertyMapper> mapping = null) where TEntity : class
        {
            mapper.Property(property, c => {
                if (typeof(TProperty).Is().CastableTo<string>())
                    c.Type(NHibernateUtil.StringClob);
                else if (typeof(TProperty).Is().CastableTo<byte[]>())
                    c.Type(NHibernateUtil.BinaryBlob);
                c.NotNullable(false);
                c.Length(10000);
                if (mapping != null) mapping(c);
            });
        }
        /// <summary>Clob/Blob mapping shortcut method</summary>
        /// <param name="mapper">Mapping class</param>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public static void PropertyHiddenXlob<TEntity, TProperty>(this ClassMapping<TEntity> mapper, Expression<Func<TEntity, TProperty>> property, Action<IPropertyMapper> mapping = null) where TEntity : class
        {
            PropertyHidden(mapper, property, c => {
                if (typeof(TProperty).Is().CastableTo<string>())
                    c.Type(NHibernateUtil.StringClob);
                else if (typeof(TProperty).Is().CastableTo<byte[]>())
                    c.Type(NHibernateUtil.BinaryBlob);
                c.NotNullable(false);
                c.Length(10000);
                if (mapping != null) mapping(c);
            });
        }
        /// <summary>Hidden Property mapping shortcut method</summary>
        /// <param name="mapper">Mapping class</param>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public static void PropertyHidden<TEntity, TProperty>(this ClassMapping<TEntity> mapper, Expression<Func<TEntity, TProperty>> property, Action<IPropertyMapper> mapping = null) where TEntity : class
        {
            var propName = Fnc.LambdaPropertyName(property);
            mapper.Property(propName, c => { c.Access(Accessor.Property); if (mapping != null) mapping(c); });
        }
        /// <summary>Nullable Property mapping shortcut method</summary>
        /// <param name="mapper">Mapping class</param>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public static void PropertyNullable<TEntity, TProperty>(this ClassMapping<TEntity> mapper, Expression<Func<TEntity, TProperty>> property, Action<IPropertyMapper> mapping = null) where TEntity : class
        {
            var propName = Fnc.LambdaPropertyName(property);
            mapper.Property(propName, c => { c.NotNullable(false); if (mapping != null) mapping(c); });
        }
        /// <summary>OneToMany Relation Mapping Shortcut</summary>
        /// <param name="mapper">Mapping class</param>
        /// <param name="property"></param>
        /// <param name="collectionMapping"></param>
        /// <param name="mapping"></param>
        /// <param name="bidirectional">Relation is bidirectional or not</param>
        public static void OneToMany<TEntity, TElement>(this ClassMapping<TEntity> mapper,
            Expression<Func<TEntity, IEnumerable<TElement>>> property,
            Action<IBagPropertiesMapper<TEntity, TElement>> collectionMapping = null,
            Action<ICollectionElementRelation<TElement>> mapping = null, bool bidirectional = true) where TEntity : class
        {
            //var propName = Fnc.LambdaPropertyName(property);
            mapper.Bag<TElement>(
                property,
                c => {
                    c.Cascade(Cascade.All.Include(Cascade.DeleteOrphans));
                    c.Key(k =>
                        //k.ForeignKey("{0}_{1}_{2}_{3}_{4}".ToFormat(
                        k.ForeignKey($"{typeof(TElement).Name.Transform().Pluralize()}_{typeof(TEntity).Name.Transform().Pluralize()}_{(bidirectional ? "FKM1" : "FK1M")}"
                        //typeof(TElement).GetFirstPropertyOfType(typeof(TEntity)).Name,
                        //"Id",
                        //Use the same foreign key if bidirectional
                        ));
                    if (bidirectional) c.Inverse(true); //Invert relation if bidirectional
                    if (collectionMapping != null) collectionMapping(c);
                },
                a => {
                    a.OneToMany();
                    if (mapping != null) mapping(a);
                }
                );
        }
        /// <summary>ManyToMany Relation Mapping Shortcut</summary>
        /// <param name="mapper">Mapping class</param>
        /// <param name="property"></param>
        /// <param name="collectionMapping"></param>
        /// <param name="mapping"></param>
        /// <param name="inverse">Relation is inversed or not</param>
        public static void ManyToMany<TEntity, TElement>(this ClassMapping<TEntity> mapper,
            Expression<Func<TEntity, IEnumerable<TElement>>> property,
            Action<IBagPropertiesMapper<TEntity, TElement>> collectionMapping = null,
            Action<ICollectionElementRelation<TElement>> mapping = null,
            bool inverse = false) where TEntity : class
        {
            //var propName = Fnc.LambdaPropertyName(property);
            var aType = typeof(TEntity);
            var bType = typeof(TElement);
            var aName = aType.Name;
            var bName = bType.Name;

            var aFieldName = aName + "Id";
            var bFieldName = bName + ((bName == aName) ? "Id2" : "Id");

            var aNamePl = aName.Transform().Pluralize();
            var bNamePl = bName.Transform().Pluralize();

            var tblName = inverse ? $"{bNamePl}To{aNamePl}" : $"{aNamePl}To{bNamePl}";

            mapper.Bag<TElement>(
                property,
                c => {
                    c.Cascade(Cascade.All.Exclude(Cascade.DeleteOrphans).Exclude(Cascade.Remove));
                    c.Table(tblName);
                    c.Inverse(inverse);
                    c.Key(k => {
                        k.Column(aFieldName);
                        //k.ForeignKey("{0}_{1}_{2}_{3}_{4}".ToFormat(tblName, aFieldName, aNamePl, "Id", "FKMM"));
                        k.ForeignKey($"{tblName}_{aNamePl}_FKMM");
                    });
                    if (collectionMapping != null) collectionMapping(c);
                },
                a => a.ManyToMany(k => {
                    k.Column(bFieldName);
                    //k.ForeignKey("{0}_{1}_{2}_{3}_{4}".ToFormat(tblName, bFieldName, bNamePl, "Id", "FKMM"));
                    k.ForeignKey($"{tblName}_{bNamePl}_FKMM");
                    if (mapping != null) mapping(a);
                })
                );
        }
        /// <summary>Get the owner entity of the specified member</summary>
        public static Type Owner(this PropertyPath member)
        {
            return member.GetRootMember().DeclaringType;
        }
        /// <summary>Get the related entity on the other side of the relation</summary>
        public static Type OtherSideType(this PropertyPath member)
        {
            var propType = member.LocalMember.GetPropertyOrFieldType();
            if (propType.GetTypeInfo().GetInterface<ICollection>() != null) return propType.DetermineCollectionElementOrDictionaryValueType();
            return propType;
        }
        /// <summary>Get the other side property of the relation</summary>
        public static MemberInfo OtherSideProperty(this PropertyPath member)
        {
            return member.OtherSideType().GetFirstPropertyOfType(member.Owner());
        }

        /*public static string ManyToManyIntermediateTableName(this PropertyPath member)
        {
            return String.Join(
                ManyToManyIntermediateTableInfix,
                member.ManyToManySidesNames().OrderBy(x => x)
            );
        }*/

        /*private static IEnumerable<string> ManyToManySidesNames(this PropertyPath member)
        {
            yield return member.Owner().Name;
            yield return member.CollectionElementType().Name;
        }*/
    }
}