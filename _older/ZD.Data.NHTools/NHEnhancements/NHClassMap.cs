﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Validator.Cfg.Loquacious;

namespace Z.Data.NHTools
{
    /// <summary>ClassMapping NHUtil wrapper</summary>
    [Serializable]
    public class NHClassMap<T> : ClassMapping<T> where T : class
    {
        /// <summary>Entity Mapping</summary>
        /// <param name="mapBaseEntity">Base entity map should be executed or not</param>
        public NHClassMap(bool mapBaseEntity = true)
        {
            if (mapBaseEntity) DynamicUpdate(true);
        }

        #region PropertyHidden
        /// <summary>Hidden Property mapping shortcut method</summary>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public void PropertyHidden<TProperty>(Expression<Func<T, TProperty>> property, Action<IPropertyMapper> mapping = null)
        { ExtensionNhibernateMapping.PropertyHidden(this, property, mapping); }
        #endregion PropertyHidden

        #region PropertyNullable
        /// <summary>Hidden Property mapping shortcut method</summary>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public void PropertyNullable<TProperty>(Expression<Func<T, TProperty>> property, Action<IPropertyMapper> mapping = null)
        { ExtensionNhibernateMapping.PropertyNullable(this, property, mapping); }
        #endregion PropertyNullable

        #region PropertyXlob
        /// <summary>Clob/Blob mapping shortcut method</summary>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public void PropertyXlob<TProperty>(Expression<Func<T, TProperty>> property, Action<IPropertyMapper> mapping = null)
        { ExtensionNhibernateMapping.PropertyXlob(this, property, mapping); }
        /// <summary>Clob/Blob mapping shortcut method</summary>
        /// <param name="mapping">Mapping attributes</param>
        /// <param name="property">Property to map</param>
        public void PropertyHiddenXlob<TProperty>(Expression<Func<T, TProperty>> property, Action<IPropertyMapper> mapping = null)
        { ExtensionNhibernateMapping.PropertyHiddenXlob(this, property, mapping); }
        #endregion PropertyXlob

        #region OneToMany ManyToMany
        /// <summary>OneToMany Relation Mapping Shortcut</summary>
        /// <param name="property"></param>
        /// <param name="collectionMapping"></param>
        /// <param name="mapping"></param>
        public void OneToMany<TElement>(Expression<Func<T, IEnumerable<TElement>>> property,
            Action<IBagPropertiesMapper<T, TElement>> collectionMapping = null,
            Action<ICollectionElementRelation<TElement>> mapping = null)
        { ExtensionNhibernateMapping.OneToMany(this, property, collectionMapping, mapping); }
        /// <summary>ManyToMany Relation Mapping Shortcut</summary>
        /// <param name="property"></param>
        /// <param name="collectionMapping"></param>
        /// <param name="mapping"></param>
        /// <param name="inverse">Relation is inversed or not</param>
        public void ManyToMany<TElement>(Expression<Func<T, IEnumerable<TElement>>> property,
            Action<IBagPropertiesMapper<T, TElement>> collectionMapping = null,
            Action<ICollectionElementRelation<TElement>> mapping = null,
            bool inverse = false)
        { ExtensionNhibernateMapping.ManyToMany(this, property, collectionMapping, mapping, inverse); }
        #endregion OneToMany ManyToMany
    }

    /// <summary>Entity Validation Mapping</summary>
    public class NHValidationMap<T> : ValidationDef<T> where T : class
    {
        /// <summary>Entity Validation Mapping</summary>
        /// <param name="mapBaseEntity">Base validation map should be executed or not</param>
        public NHValidationMap(bool mapBaseEntity = true) { }
    }

    #region Old Code
    /*
    /// <summary>Base strongly-typed entity</summary>
    [Serializable]
    public class NHEntity<T> : NHEntity where T : NHEntity<T>
    {
        #region Map
        /// <inheritdoc/>
        public class NHEntityMap : NHEntityMap<T>
        {
            /// <inheritdoc/>
            public NHEntityMap(bool mapBaseEntity = true) : base(mapBaseEntity) { }
        }
        #endregion Map

        #region ValidationMap
        /// <inheritdoc/>
        public class ValidationMap : NHValidationMap<T>
        {
            /// <inheritdoc/>
            public ValidationMap(bool mapBaseEntity = true) { }
        }
        #endregion ValidationMap
    }
    /// <summary>Base Data Entity</summary>
    [Serializable]
    public class NHEntity : ICloneable, IDisposable
    {
        #region Map
        /// <summary>Entity Mapping</summary>
        public class NHEntityMap<T> : NHClassMapping<T> where T : NHEntity//ClassMapping<T> where T: Entity
        {
            /// <summary>Entity Mapping</summary>
            /// <param name="mapBaseEntity">Base entity map should be executed or not</param>
            public NHEntityMap(bool mapBaseEntity = true)
            {
                if (mapBaseEntity) DynamicUpdate(true);
            }
        }
        #endregion Map

        #region ValidationMap
        /// <summary>Entity Validation Mapping</summary>
        public class NHValidationMap<T> : ValidationDef<T> where T : NHEntity
        {
            /// <summary>Entity Validation Mapping</summary>
            /// <param name="mapBaseEntity">Base validation map should be executed or not</param>
            public NHValidationMap(bool mapBaseEntity = true) { }
        }
        #endregion ValidationMap

        private bool _disposed = false;
        /// <summary>Base Data Entity</summary>
        public NHEntity()
        {
        }

        #region Clone
        /// <inheritdoc/>
        public virtual NHEntity Clone() { return (NHEntity)((ICloneable)this).Clone(); }
        object ICloneable.Clone() { return (ICloneable)MemberwiseClone(); }
        #endregion Clone

        #region Dispose
        /// <inheritdoc/>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <inheritdoc/>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                //if (disposing) DetachEvents();
                // Call the appropriate methods to clean up
                // unmanaged resources here.
                _disposed = true;
            }
        }
        #endregion Dispose
    }
    */
    #endregion Old Code
}