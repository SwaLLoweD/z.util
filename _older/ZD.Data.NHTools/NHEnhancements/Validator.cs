﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Validator.Engine;

namespace Z.Data.NHTools
{
    /// <summary>NHibernate Validation Manager</summary>
    public static class Validator
    {
        /// <summary>Checks if entity is valid or not</summary>
        public static bool IsValid(object entity)
        {
            var ve = NHibernate.Validator.Cfg.Environment.SharedEngineProvider.GetEngine();//new ValidatorEngine();
            bool rval = ve.IsValid(entity);
            ve.Clear();
            return rval;
        }
        /// <summary>Returns invalid values of an entity</summary>
        public static IEnumerable<Z.Data.Common.InvalidValue> GetInvalidValues(object entity)
        {
            var ve = NHibernate.Validator.Cfg.Environment.SharedEngineProvider.GetEngine();//new ValidatorEngine();
            InvalidValue[] ivs = ve.Validate(entity);
            var rval = NHInvalidVal.ImportInvalidValues(ivs);
            ve.Clear();
            return rval;
        }
        /// <summary>Returns invalid values of an entity property</summary>
        public static IEnumerable<Z.Data.Common.InvalidValue> GetInvalidValues(System.Reflection.PropertyInfo property, object value)
        {
            if (property == null) return null;
            var ve = NHibernate.Validator.Cfg.Environment.SharedEngineProvider.GetEngine();//new NHibernate.Validator.Engine.ValidatorEngine();
            try {
                // May be not defined
                InvalidValue[] ivs = ve.ValidatePropertyValue(property.DeclaringType, property.Name, value);
                return NHInvalidVal.ImportInvalidValues(ivs.ToArray<InvalidValue>());
            }
            catch { return new List<Z.Data.Common.InvalidValue>(); }
        }
        /// <summary>Returns invalid valiues of an entity property</summary>
        public static IEnumerable<Z.Data.Common.InvalidValue> GetInvalidValues(string entityFN, string property, object value)
        {
            Type t = typeof(NHConfig).Assembly.GetType(entityFN);
            if (t == null) return null;
            var p = t.GetProperty(property);
            return GetInvalidValues(p, value);
        }
        /// <summary>Checks if property value should be unique</summary>
        public static bool GetPropertyUnique(Type t, string property)
        {
            if (t == null) return false;
            return GetPropertyUnique(t.FullName, property);
        }
        /// <summary>Checks if property value should be unique</summary>
        public static bool GetPropertyUnique(string entityFN, string property, string configName = "default")
        {
            var metadata = NHConfig.ConfigGet(configName).ToConfiguration().GetClassMapping(entityFN); //Might be too slow
            if (metadata == null) return false;
            var col = metadata.Table.GetColumn(new NHibernate.Mapping.Column(property));
            return (col == null) ? false : col.IsUnique;
        }
        /// <summary>Get maximum allowable string length of a string property</summary>
        public static int GetPropertyStrLen<T>(System.Linq.Expressions.Expression<Func<T, string>> property)
        {
            return GetPropertyStrLen(typeof(T).FullName, Z.Util.Fnc.LambdaProperty<T, string>(property).Name);
        }
        /// <summary>Get maximum allowable string length of a string property</summary>
        public static int GetPropertyStrLen(Type t, string property)
        {
            if (t == null) return 0;
            return GetPropertyStrLen(t.FullName, property);
        }
        /// <summary>Get maximum allowable string length of a string property</summary>
        public static int GetPropertyStrLen(string entityFN, string property, string configName = "default")
        {
            var metadata = NHConfig.ConfigGet(configName).FactoryGet().GetClassMetadata(entityFN);
            var pt = metadata.GetPropertyType(property);
            if (pt is NHibernate.Type.StringType) {
                var propertyType = metadata.GetPropertyType(property) as NHibernate.Type.StringType;
                return propertyType.SqlType.Length;
            }
            return 0;
        }
        /*public static bool GetPropertyAllowNull<T>(System.Linq.Expressions.Expression<Func<T, object>> property)
       {
           var _metadata = SessionFactory.GetClassMetadata(typeof(T));

           PropertyInfo propertyInfo = Z.Util.Fnc.LambdaProperty<T>(property);

           if (_metadata.GetPropertyType(propertyInfo.Name) == null) return false;
           if (_metadata.GetPropertyType(propertyInfo.Name).SqlTypes
           if (_metadata.GetPropertyType(propertyInfo.Name) is NHibernate.Type.StringType)
           {
               var propertyType = _metadata.GetPropertyType(propertyInfo.Name) as NHibernate.Type.StringType;
               return propertyType.SqlType.Length;
           }
           return 0;
       }*/
    }
}