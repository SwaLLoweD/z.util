﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Globalization;
using System.Resources;
using NHibernate.Validator.Engine;
using Z.Extensions;

namespace Z.Data.NHTools
{
    /// <summary>NHibnernate Validator Language Support</summary>
    public class NHValidatorInterpolator : IMessageInterpolator
    {
        //private readonly string ResourceBaseName = "Securiskop.Data.Properties.DefaultValidatorMessages";
        /// <summary>Custom ResourceManager to use</summary>
        public static ResourceManager ResourceMan;
        /// <summary></summary>
        public NHValidatorInterpolator()
        {
            //this.ResourceMan = new ResourceManager(this.ResourceBaseName, Assembly.GetExecutingAssembly());
        }
        /// <summary></summary>
        public string Interpolate(InterpolationInfo info)
        {
            var message = info.Message;
            var culture = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            // It's a tempate
            if (!message.StartsWith("{") && !message.EndsWith("}")) return message;
            var resource = message.Substring(1, message.Length - 2);
            if (ResourceMan != null /*&& culture == "tr" */) {
                var m = ResourceMan.GetString(resource, CultureInfo.CurrentCulture);
                if (m.Is().NotEmpty) info.Message = m;
            }
            return info.DefaultInterpolator.Interpolate(info);
        }
    }
}