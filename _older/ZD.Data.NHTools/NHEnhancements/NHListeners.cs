﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Linq;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Event;
using NHibernate.Proxy;

namespace Z.Data.NHTools
{
    /// <summary>NHibernate event listeners</summary>
    public class NHListeners : ILoadEventListener, ISaveOrUpdateEventListener, IDeleteEventListener
    {
        private string[] skipEntity = new string[] { "Setting", "SKLog", "RecycledObject" };
        //private string[] recycleEntity = new string[] { "Asset", "AssetGroup" };

        /// <summary>Load Event</summary>
        public void OnLoad(LoadEvent @event, LoadType loadType)
        {
            if (skipEntity.Contains(@event.EntityClassName)) return;
            //string id = string.Empty;
            var e = @event.Result;
            if (e == null) return;
            //if (@event != null && @event.EntityId != null) id = @event.EntityId.ToString();
            handleLoad(e);
            //Log.Debug("Entity Loaded: {0} {1}".ToFormat(@event.EntityClassName, id), LogTypes.Data);
            //var dl = new Entities.LogData();
        }
        /// <summary>SaveOrUpdate Event</summary>
        public void OnSaveOrUpdate(SaveOrUpdateEvent @event)
        {
            //string id = string.Empty;
            //if (@event != null && @event.ResultId != null) id = @event.ResultId.ToString();
            var e = @event.Entity;
            if (e == null || skipEntity.Contains(e.GetType().Name)) return;
            //if (@event.Session != null && Repo.IsDirtyEntity(@event.Session, e)) e.DateModified = DateTime.UtcNow;
            if (@event.Session != null && NHibernateUtil.IsInitialized(@event.Entity) && @event.Entity is INHibernateProxy) e = @event.Session.PersistenceContext.Unproxy(@event.Entity);
            handleEvent(e, ChangeTypes.SaveOrUpdate);
            //Log.Debug("Entity Saved: {0} {1}".ToFormat(@event.EntityName, id), LogTypes.Data);
        }
        /// <summary>Delete Event</summary>
        public void OnDelete(DeleteEvent @event)
        {
            var e = @event.Entity;
            //@event.EntityName
            if (e == null || skipEntity.Contains(e.GetType().Name)) return;
            //Log.Debug("Entity Deleted: {0} {1}".ToFormat(@event.EntityName, e.Id), LogTypes.Data);
            handleEvent(e, ChangeTypes.Delete);
            //recycle(e, @event.Session);
        }
        /// <summary>Delete Event</summary>
        public void OnDelete(DeleteEvent @event, ISet<object> transientEntities)
        {
            var e = @event.Entity;
            if (e == null || skipEntity.Contains(@event.EntityName)) return;
            //Log.Debug("Entity Deleted: {0} {1}".ToFormat(@event.EntityName, e.Id), LogTypes.Data);
            handleEvent(e, ChangeTypes.Delete);
            //recycle(e, @event.Session as NHibernate.ISession);
        }

        #region Events
        private static void handleLoad(object e) { Repo.HandleLoad(e); }
        private static void handleEvent(object e, ChangeTypes ct) { Repo.HandleEvent(e, ct); }
        #endregion Events
    }
}