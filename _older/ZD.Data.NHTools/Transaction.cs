﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Data;
using Z.Data.Common;

namespace Z.Data.NHTools
{
    /// <summary>Transaction</summary>
    public class Transaction : ITransactionNH
    {
        private NHibernate.ITransaction transaction;
        private bool _disposed = false;

        //public Transaction(NHibernate.ITransaction nhTransaction) { transaction = nhTransaction; }
        /// <summary></summary>
        public Transaction(NHibernate.ISession session, IsolationLevel isolationLevel = IsolationLevel.Unspecified) { transaction = session.BeginTransaction(isolationLevel); }
        /// <summary></summary>
        public Transaction(NHibernate.IStatelessSession session, IsolationLevel isolationLevel = IsolationLevel.Unspecified) { transaction = session.BeginTransaction(isolationLevel); }

        /// <inheritdoc/>
        public void Begin(IsolationLevel isolationLevel = IsolationLevel.Unspecified) { transaction.Begin(isolationLevel); }
        /// <inheritdoc/>
        public void Commit() { transaction.Commit(); }
        /// <inheritdoc/>
        public void Enlist(IDbCommand command) { transaction.Enlist(command); }
        /// <inheritdoc/>
        public bool IsActive { get { return transaction.IsActive; } }
        /// <inheritdoc/>
        public void Rollback() { transaction.Rollback(); }
        /// <inheritdoc/>
        public bool WasCommited { get { return transaction.WasCommitted; } }
        /// <inheritdoc/>
        public bool WasRolledBack { get { return transaction.WasRolledBack; } }
        /// <inheritdoc/>
        public override int GetHashCode() { return transaction.GetHashCode(); }
        /// <inheritdoc/>
        public override bool Equals(object obj) { return transaction.Equals(obj); }
        /// <inheritdoc/>
        public override string ToString() { return transaction.ToString(); }

        #region Dispose
        /// <inheritdoc/>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary></summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed) {
                if (disposing) {
                    //DetachEvents();
                    //SessionFlush();
                    transaction.Dispose();
                }
                // Call the appropriate methods to clean up
                // unmanaged resources here.
                _disposed = true;
            }
        }
        #endregion Dispose
    }
}