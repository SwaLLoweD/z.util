﻿namespace Z.Data.NHTools
{
    // connstr: "uri=file://:memory:,Version=3";
    /* May have to do this
        private SqliteConnection GetConnection()
        {
            if (mConnection == null) {
            mConnection = new SqliteConnection(mConnectionString);
            mConnection.Open();
        }
        return mConnection;
        mFactory.OpenSession(GetConnection());
    } */

    /// <summary>SqlLite CSharp Driver</summary>
    public class SqliteCsharpDriver : NHibernate.Driver.ReflectionBasedDriver
    {
        /// <summary>
        /// Initializes a new instance of SQLiteDriver/>.
        /// </summary>
        public SqliteCsharpDriver()
            : base(
                "Community.CsharpSqlite.SQLiteClient",
                "Community.CsharpSqlite.SQLiteClient.SqliteConnection",
                "Community.CsharpSqlite.SQLiteClient.SqliteCommand")
        {
        }
        /// <summary></summary>
        public override bool UseNamedPrefixInSql
        {
            get { return true; }
        }
        /// <summary></summary>
        public override bool UseNamedPrefixInParameter
        {
            get { return true; }
        }
        /// <summary></summary>
        public override string NamedPrefix
        {
            get { return "@"; }
        }
        /// <summary></summary>
        public override bool SupportsMultipleOpenReaders
        {
            get { return false; }
        }
        /// <summary></summary>
        public override bool SupportsMultipleQueries
        {
            get { return true; }
        }
    }
}