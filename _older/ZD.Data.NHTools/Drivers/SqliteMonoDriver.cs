﻿namespace Z.Data.NHTools
{
    /// <summary>SQLite Mono Driver</summary>
    public class SqliteMonoDriver : NHibernate.Driver.ReflectionBasedDriver
    {
        ///<summary></summary>
        public SqliteMonoDriver() :
            base("Mono.Data.Sqlite",
            "Mono.Data.Sqlite.SqliteConnection",
            "Mono.Data.Sqlite.SqliteCommand")
        {
        }
        /// <summary></summary>
        public override bool UseNamedPrefixInParameter
        {
            get
            {
                return true;
            }
        }
        /// <summary></summary>
        public override bool UseNamedPrefixInSql
        {
            get
            {
                return true;
            }
        }
        /// <summary></summary>
        public override string NamedPrefix
        {
            get
            {
                return "@";
            }
        }
        /// <summary></summary>
        public override bool SupportsMultipleOpenReaders
        {
            get
            {
                return false;
            }
        }
    }
}