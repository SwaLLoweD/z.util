﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Data;
using NHibernate;
using NHibernate.Dialect.Function;

namespace Z.Data.NHTools.Dialect
{
    /// <summary>MySQL 5.5 Dialect with custom function support</summary>
    public class MySQL55Dialect : NHibernate.Dialect.MySQL55Dialect
    {
        /// <summary></summary>
        public MySQL55Dialect()
        {
            //Hacks
            //RegisterColumnType(DbType.Guid, "CHAR(36)");

            RegisterFunction("AddDays", new SQLFunctionTemplate(NHibernateUtil.DateTime, "ADDDATE(?1,?2)"));
            RegisterFunction("AddSeconds", new SQLFunctionTemplate(NHibernateUtil.DateTime, "ADDTIME(?1,SEC_TO_TIME(?2))"));
            RegisterFunction("AddTimeSpan", new SQLFunctionTemplate(NHibernateUtil.DateTime, "ADDTIME(?1,?2)"));
        }
    }
}