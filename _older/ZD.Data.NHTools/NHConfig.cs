﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Resources;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using NHibernate.Event;
using NHibernate.Mapping.ByCode;
using NHibernate.Validator.Cfg;
using NHibernate.Validator.Cfg.Loquacious;
using NHibernate.Validator.Engine;
using Z.Collections.Generic;
using Z.Data.NHTools.UserTypes;
using Z.Extensions;
using Z.Util;
using System.Reflection;

namespace Z.Data.NHTools
{
    /// <summary>NHibernate configuration</summary>
    [Serializable]
    public class NHConfig
    {
        #region Config Properties
        /// <summary>Configuration Name</summary>
        public string Name { get; private set; }
        /// <summary>Collection lazy-load default batch size (Default=10, Given the operation is executed in the same transaction)</summary>
        public int CollectionBatchSize { get; set; }
        /// <summary>Insert, update, delete batch size (Default=10, Given the operation is executed in the same transaction)</summary>
        public int WriteBatchSize { get; set; }
        /// <summary>Insert, update, delete batch size limit after which the operation is not cached in Session (Default 500, Stateless Session)</summary>
        public int WriteSessionBatchLimit { get; set; }
        /// <summary>SQL Command Timeout in seconds (Default=120)</summary>
        public int CommandTimeout { get; set; }
        /// <summary>SQL Connection String</summary>
        public string ConnectionString { get; private set; }
        /// <summary>SQL Connection String</summary>
        public string ConnectionStringName { get; set; }
        /// <summary>Db Connection Provider</summary>
        public string ConnectionProvider { get; set; }
        /// <summary>Dialect</summary>
        public string DbDialect { get; set; }
        /// <summary>Db Driver</summary>
        public string DriverClass { get; set; }
        /// <summary>Show SQL output in console (Default=false)</summary>
        public bool ShowSql { get; set; }
        /// <summary>Update Schema (Default=true)</summary>
        public bool ExportSchema { get; set; }
        /// <summary>Enable NHibernate events (Default=true)</summary>
        public bool EnableListeners { get; set; }
        /// <summary>Apply Default conventions (Default=true)</summary>
        public bool ApplyDefaultConventions { get; set; }
        /// <summary>Apply Default userTypes (Default=true)</summary>
        public bool ApplyDefaultUserTypes { get; set; }
        /// <summary>Maps of entities</summary>
        public IList<Type> EntityMaps { get; set; }
        /// <summary>Maps of validatiors</summary>
        public IList<Type> ValidationMaps { get; set; }
        /// <summary>Maps of custom usertypes</summary>
        public IDictionary<Type, Type> UserTypeMaps { get; set; }
        /// <summary>Custom Validator messages resource file</summary>
        public ResourceManager ValidatorResource { get; set; }
        /// <summary>Cache Provider</summary>
        public string CacheProvider { get; set; }
        /// <summary>Cache Default Expiration in seconds</summary>
        public int CacheDefaultExpiration { get; set; }
        /// <summary>Cache Queries (Default=true)</summary>
        public bool UseQueryCache { get; set; }
        /// <summary>Default String or Byte Field Length (Default=200)</summary>
        public int DefaultFieldLength { get; set; }
        #endregion Config Properties

        private ISessionFactory factory;
        private object factoryLock = new object();
        private static IDictionary<string, NHConfig> configStore = new Dictionary<string, NHConfig>();

        #region Constructors
        /// <summary></summary>
        public NHConfig(string connStrName = "ApplicationServices", string configName = "default")
        {
            System.Data.IDbConnection conn = null;
            //if (ConfigurationManager.ConnectionStrings.Count > 0) {
            //    var css = ConfigurationManager.ConnectionStrings[connStrName] ?? ConfigurationManager.ConnectionStrings[0];
            //    ConnectionStringName = css.Name;
            //    conn = css.CreateConnection();
            //}
            init(conn, configName);
        }
        /// <summary></summary>
        public NHConfig(System.Data.IDbConnection defaultConnection, string configName = "default")
        { init(defaultConnection, configName); }

        private void init(System.Data.IDbConnection defaultConnection, string configName = null)
        {
            configName = configName ?? "default";
            EntityMaps = new List<Type>();
            ValidationMaps = new List<Type>();
            UserTypeMaps = new Dictionary<Type, Type>();
            Name = configName;
            LoadFromConfig(defaultConnection);
            ConfigAdd(configName, this);
        }
        #endregion Constructors

        #region Config Add/Remove
        /// <summary>Add new Configuration to ConfigStore.
        /// </summary>
        /// <param name="configName">Config name.</param>
        /// <param name="config">Config.</param>
        public static void ConfigAdd(string configName, NHConfig config)
        {
            configStore[configName] = config;
        }
        /// <summary>Gets the Config with specified name.</summary>
        /// <returns>The get.</returns>
        /// <param name="configName">Config name.</param>
        public static NHConfig ConfigGet(string configName = "default")
        {
            return configStore.TryGetValue(configName);
        }
        #endregion Config Add/Remove

        #region LoadFromConfig / Connection
        /// <summary>Load Values from Application config file</summary>
        public void LoadFromConfig(System.Data.IDbConnection connection = null)
        {
            string dbDialect = null;
            string dbDriverclass = null;
            int? adoNetBatchSize = null;
            string connectionString = null;

            #region Pull config from connection
            if (connection != null) {
                var dbtype = connection.GetType().FullName;
                connectionString = connection.ConnectionString;
                if (dbtype.StartsWith("MySql")) {
                    dbDialect = typeof(Z.Data.NHTools.Dialect.MySQL55Dialect).AssemblyQualifiedName; //"Z.Data.NHTools.Dialect.MySQL55Dialect, Z.Util";
                    dbDriverclass = "NHibernate.Driver.MySqlDataDriver";
                    adoNetBatchSize = 0;
                }
                else if (dbtype.StartsWith("SqlCe")) {
                    dbDialect = "NHibernate.Dialect.MsSqlCe40Dialect";
                    dbDriverclass = "NHibernate.Driver.SqlServerCeDriver";
                }
                else if (dbtype.StartsWith("Npgsql")) {
                    dbDialect = "NHibernate.Dialect.PostgreSQL82Dialect";
                    dbDriverclass = "NHibernate.Driver.NpgsqlDriver";
                    adoNetBatchSize = 0;
                }
                else if (dbtype.StartsWith("Oracle")) {
                    dbDialect = "NHibernate.Dialect.Oracle10gDialect";
                    dbDriverclass = "NHibernate.Driver.OracleClientDriver";
                }
                else if (dbtype.StartsWith("SQLite")) {
                    dbDialect = "NHibernate.Dialect.SQLiteDialect";
                    dbDriverclass = "NHibernate.Driver.SQLite20Driver";
                    adoNetBatchSize = 0;
                }
                else if (dbtype.StartsWith("System.Data.SqlClient.")) {
                    dbDialect = typeof(Z.Data.NHTools.Dialect.MsSql2012Dialect).AssemblyQualifiedName;
                    dbDriverclass = "NHibernate.Driver.SqlClientDriver";
                }
                else if (dbtype.StartsWith("System.Data.OleDb.")) {
                    dbDialect = "NHibernate.Dialect.GenericDialect";
                    dbDriverclass = "NHibernate.Driver.OleDbDriver";
                    adoNetBatchSize = 0;
                }
                else if (dbtype.StartsWith("System.Data.Odbc.")) {
                    dbDialect = "NHibernate.Dialect.GenericDialect";
                    dbDriverclass = "NHibernate.Driver.OdbcDriver";
                    adoNetBatchSize = 0;
                }
            }
            #endregion Pull config from connection

            CollectionBatchSize = 10;
            WriteBatchSize = adoNetBatchSize ?? 10;
            WriteSessionBatchLimit = 500;
            CommandTimeout = 120;
            ConnectionStringName = "ApplicationServices";
            ConnectionString = connectionString;
            ConnectionProvider = "NHibernate.Connection.DriverConnectionProvider";
            DbDialect = "NHibernate.Dialect.MsSql2008Dialect";
            DriverClass = dbDriverclass ?? "NHibernate.Driver.SqlClientDriver";
            ShowSql = false;
            ExportSchema = true;
            EnableListeners = true;
            ApplyDefaultConventions = true;
            ApplyDefaultUserTypes = true;
            CacheProvider = findCacheProviderFromAssembly();
            CacheDefaultExpiration = 120;
            UseQueryCache = true;
            DefaultFieldLength = 200;

            //CollectionBatchSize = ConfigurationManager.AppSettings["DbCollectionBatchSize"].To().Type<int>(10);
            //WriteBatchSize = ConfigurationManager.AppSettings["DbBatchSize"].To().Type<int?>(null) ?? adoNetBatchSize ?? 10;
            //WriteSessionBatchLimit = ConfigurationManager.AppSettings["DbBatchSizeLimit"].To().Type<int>(500);
            //CommandTimeout = ConfigurationManager.AppSettings["DbDefaultTimeout"].To().Type<int>(120);
            //ConnectionStringName = ConfigurationManager.AppSettings["DbConnString"] ?? "ApplicationServices";
            //ConnectionString = ConfigurationManager.AppSettings["DbConnectionString"] ?? connectionString;
            //ConnectionProvider = ConfigurationManager.AppSettings["DbProvider"] ?? "NHibernate.Connection.DriverConnectionProvider";
            //DbDialect = ConfigurationManager.AppSettings["DbDialect"] ?? dbDialect ?? "NHibernate.Dialect.MsSql2008Dialect";
            //DriverClass = ConfigurationManager.AppSettings["DbDriverClass"] ?? dbDriverclass ?? "NHibernate.Driver.SqlClientDriver";
            //ShowSql = ConfigurationManager.AppSettings["DbShowSql"].To().Type<bool>(false);
            //ExportSchema = ConfigurationManager.AppSettings["ExportSchema"].To().Type<bool>(true);
            //EnableListeners = ConfigurationManager.AppSettings["EnableListeners"].To().Type<bool>(true);
            //ApplyDefaultConventions = ConfigurationManager.AppSettings["ApplyDefaultConventions"].To().Type<bool>(true);
            //ApplyDefaultUserTypes = ConfigurationManager.AppSettings["ApplyDefaultUserTypes"].To().Type<bool>(true);
            //CacheProvider = ConfigurationManager.AppSettings["CacheProvider"] ?? findCacheProviderFromAssembly();
            //CacheDefaultExpiration = ConfigurationManager.AppSettings["CacheDefaultExpiration"].To().Type<int>(120);
            //UseQueryCache = ConfigurationManager.AppSettings["UseQueryCache"].To().Type<bool>(true);
            //DefaultFieldLength = ConfigurationManager.AppSettings["DefaultFieldLength"].To().Type<int>(200);

            //if (ConnectionString.Is().Empty && ConnectionStringName.Is().NotEmpty) {
            //    var cstr = ConfigurationManager.ConnectionStrings[ConnectionStringName];
            //    if (cstr != null) ConnectionString = cstr.ConnectionString;
            //}
        }
        #endregion LoadFromConfig / Connection

        #region CacheProvider
        /// <summary>Sets CacheProvider</summary>
        public void SetCacheProvider<T>(T provider = default(T))
        {
            if (typeof(T).GetTypeInfo().GetInterface<NHibernate.Cache.ICacheProvider>() != null) CacheProvider = $"{typeof(T).FullName}, {typeof(T).Assembly.FullName}";
        }
        /*
        /// <summary>SetCacheProvider</summary>
        public static void SetCacheProvider<T>(int defaultExpiration = 120, bool useQueryCache = true) where T:NHibernate.Cache.ICacheProvider {
            Config.Cache(x => { x.Provider<T>(); x.UseQueryCache = useQueryCache; });
            Config.SessionFactory().Caching.Through<T>().WithDefaultExpiration(defaultExpiration);
        }*/
        private string findCacheProviderFromAssembly()
        {
            var providers = new string[] {
                "NHibernate.Caches.SysCache.SysCacheProvider, NHibernate.Caches.SysCache",
                "NHibernate.Caches.SysCache2.SysCache2Provider, NHibernate.Caches.SysCache2",
                "NHibernate.Caches.MemCache.MemCacheProvider, NHibernate.Caches.MemCache",
                "NHibernate.Caches.Velocity.VelocityProvider, NHibernate.Caches.Velocity"
                };
            foreach (var provider in providers) {
                if (Type.GetType(provider) != null) return provider;
            }
            return null;
        }

        #endregion CacheProvider

        #region LoadMap
        /// <summary>Load Entity and Validation maps from assembly</summary>
        public void LoadMaps(System.Reflection.Assembly asmb)
        {
            var exportedTypes = asmb.GetExportedTypes();

            var entityMaps = exportedTypes.Where(t => t.Is().HasInterface<IConformistHoldersProvider>() /*&& !t.IsGenericType*/).ToArray();
            LoadEntityMap(entityMaps);

            var valMaps = exportedTypes.ValidationDefinitions().ToList();

            var usrTypes = exportedTypes.Where(t => t.Is().HasInterface<NHibernate.UserTypes.IUserType>()).ToList();
            foreach (var usrType in usrTypes) {
                var obj = Activator.CreateInstance(usrType) as NHibernate.UserTypes.IUserType;
                LoadUserTypeMap(usrType);
            }

            ValidationMaps.Add(valMaps);
        }
        /// <summary>Load Entity and Validation maps from Assembly</summary>
        public void LoadMaps<T>() { LoadMaps(typeof(T).Assembly); }
        /// <summary>Load Entity Map</summary>
        public void LoadEntityMap<T>() { LoadEntityMap(typeof(T)); } //where T :IConformistHoldersProvider
        /// <summary>Load Entity Map</summary>
        public void LoadEntityMap(params Type[] types) { EntityMaps.Add(types); } //where T :IConformistHoldersProvider
        /// <summary>Load Validation Map</summary>
        public void LoadValidationMap<T>() { LoadValidationMap(typeof(T)); } //where T :IConstraintAggregator
        /// <summary>Load Validation Map</summary>
        public void LoadValidationMap(params Type[] types) { ValidationMaps.Add(types); } //where T :IConstraintAggregator
        /// <summary>Load UserTypeMap</summary>
        public void LoadUserTypeMap<T>() { LoadUserTypeMap(typeof(T)); } //where T :IUserType
        /// <summary>Load UserTypeMap</summary>
        public void LoadUserTypeMap(Type userType)
        {
            if (userType == null || !userType.Is().HasInterface<NHibernate.UserTypes.IUserType>()) return;
            var obj = Activator.CreateInstance(userType) as NHibernate.UserTypes.IUserType;
            UserTypeMaps[obj.ReturnedType] = userType;
        }
        #endregion LoadMap

        #region ToConfiguration
        /// <summary>Apply default Db Parameters</summary>
        internal NHibernate.Cfg.Configuration ToConfiguration(NHibernate.Cfg.Configuration cfg = null)
        {
            if (ApplyDefaultUserTypes) loadDefaultUserTypes();

            //_nhConfig.Configure(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DB_CFG_FILE));
            cfg = cfg ?? new NHibernate.Cfg.Configuration();

            toConfigurationOptions(cfg);
            if (CacheProvider.Is().NotEmpty) toConfigurationCacheOptions(cfg);
            if (EnableListeners) toConfigurationListeners(cfg);
            toConfigurationMapping(cfg);

            //_nhConfig.LinqToHqlGeneratorsRegistry<TypeMaps.ExtendedLinqtoHqlGeneratorsRegistry>();

            //Manipulate output XML (Hacky versioning as timespan)
            //var dmMappingDocument = domainMapping.AsString();
            //dmMappingDocument = dmMappingDocument.Replace("<version name=\"DateModified\" generated=\"always\"",
            //"<timestamp name=\"DateModified\" generated=\"always\" "); //type=\"timestamp\" unsaved-value=\"1970-01-01 00:00\"
            //_nhConfig.AddXml(dmMappingDocument);

            if (ValidationMaps.Count > 0) configureNhibernateValidator(cfg);
            if (ExportSchema) buildSchema(cfg);
            return cfg;
        }

        internal void toConfigurationOptions(NHibernate.Cfg.Configuration cfg)
        {
            cfg.Properties[NHibernate.Cfg.Environment.BatchSize] = WriteBatchSize.ToString();
            cfg.Properties[NHibernate.Cfg.Environment.CommandTimeout] = CommandTimeout.ToString();
            cfg.Properties[NHibernate.Cfg.Environment.ConnectionStringName] = ConnectionStringName;
            cfg.Properties[NHibernate.Cfg.Environment.ConnectionProvider] = ConnectionProvider;
            cfg.Properties[NHibernate.Cfg.Environment.Dialect] = DbDialect;
            cfg.Properties[NHibernate.Cfg.Environment.ConnectionDriver] = DriverClass;
            cfg.Properties[NHibernate.Cfg.Environment.ShowSql] = ShowSql.ToString().ToLower();
            //cfg.Properties[NHibernate.Cfg.Environment.CollectionTypeFactoryClass] = typeof(Net4CollectionTypeFactory).AssemblyQualifiedName;
            cfg.Properties[NHibernate.Cfg.Environment.CurrentSessionContextClass] = typeof(HybridWebSessionContext).AssemblyQualifiedName;

            #region Old Code
            //cfg.SetProperty("adonet.batch_size", ADONetBatchSize.ToString());
            //cfg.SetProperty("command_timeout", CommandTimeout.ToString());
            //cfg.SetProperty("connection.connection_string_name", ConnectionStringName);
            //cfg.SetProperty("connection.provider", ConnectionProvider);
            //cfg.SetProperty("dialect", DbDialect);
            //cfg.SetProperty("connection.driver_class", DriverClass);
            //cfg.SetProperty("show_sql", ShowSql.ToString().ToLower());
            //if (CacheProvider.Is().NotEmpty) {
            //    cfg.SetProperty("cache.provider_class", CacheProvider);
            //    cfg.SetProperty("cache.default_expiration", CacheDefaultExpiration.ToString());
            //    cfg.SetProperty("cache.use_second_level_cache", "true");
            //    cfg.SetProperty("cache.use_query_cache", UseQueryCache.ToString().ToLower());
            //}
            //{"current_session_context_class","thread_static"},
            //{"proxyfactory.factory_class","NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle"},
            #endregion Old Code
        }

        internal void toConfigurationCacheOptions(NHibernate.Cfg.Configuration cfg)
        {
            cfg.Properties[NHibernate.Cfg.Environment.CacheProvider] = CacheProvider;
            cfg.Properties[NHibernate.Cfg.Environment.CacheDefaultExpiration] = CacheDefaultExpiration.ToString();
            cfg.Properties[NHibernate.Cfg.Environment.UseSecondLevelCache] = "true";
            cfg.Properties[NHibernate.Cfg.Environment.UseQueryCache] = UseQueryCache.ToString().ToLower();
        }

        internal void toConfigurationListeners(NHibernate.Cfg.Configuration cfg)
        {
            // Entity Change Logging Functionality
            cfg.EventListeners.LoadEventListeners = new ILoadEventListener[] { new NHListeners(), new NHibernate.Event.Default.DefaultLoadEventListener() };
            cfg.EventListeners.SaveOrUpdateEventListeners = new ISaveOrUpdateEventListener[] { new NHListeners(), new NHibernate.Event.Default.DefaultSaveOrUpdateEventListener() };
            cfg.EventListeners.DeleteEventListeners = new IDeleteEventListener[] { new NHListeners(), new NHibernate.Event.Default.DefaultDeleteEventListener() };
        }

        internal void toConfigurationMapping(NHibernate.Cfg.Configuration cfg)
        {
            var mapper = new ModelMapper();
            toConfigurationUserTypes(cfg, mapper);
            if (ApplyDefaultConventions) mapper = NHDefaultConventions.SetConventions(mapper, CollectionBatchSize, this);
            /*var asmb = typeof(SessionHelper).Assembly;
            var entityMaps = asmb.GetExportedTypes()
                .Where(t => t.Is().HasInterface<IConformistHoldersProvider>()).ToList(); */

            //entityMaps.Add(NHConfig.EntityMaps);

            if (EntityMaps.Count > 0) mapper.AddMappings(EntityMaps);
            var domainMapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            cfg.AddMapping(domainMapping);
        }
        private void loadDefaultUserTypes()
        {
            LoadUserTypeMap<IPAddressUserType>();
            LoadUserTypeMap<BitmaskUserType>();
        }
        internal void toConfigurationUserTypes(NHibernate.Cfg.Configuration cfg, ModelMapper mapper)
        {
            mapper.BeforeMapProperty += (insp, prop, map) => {
                Type propType = prop.LocalMember.GetPropertyOrFieldType();
                var mappedType = UserTypeMaps.TryGetValue(propType);
                if (mappedType != null && mappedType.Is().CastableTo<NHibernate.UserTypes.IUserType>()) map.Type(mappedType, null);

                if (typeof(DateTime).Equals(propType)) map.Type(NHibernateUtil.UtcDateTime);
                //else if (typeof(Bitmask).Equals(propType)) map.Length(127);
                else if (typeof(Uri).Equals(propType)) { map.Type(NHibernateUtil.Uri); map.Length(DefaultFieldLength); }
                else if (typeof(string).Equals(propType) || typeof(byte[]).Equals(propType)) map.Length(DefaultFieldLength);
                //else if (propType.IsEnum || (propType.GetGenericTypeDefinition() == typeof(Nullable<>) &&
                //propType.GetGenericArguments()[0].IsEnum))
            };
            cfg.LinqToHqlGeneratorsRegistry<NHTools.UserTypes.LinqExt.ExtendedLinqtoHqlGeneratorsRegistry>();
        }

        internal void toConfigurationColBatchSize(NHibernate.Cfg.Configuration cfg, ModelMapper mapper)
        {
            mapper.BeforeMapBag += (insp, prop, map) => map.BatchSize(CollectionBatchSize);
            mapper.BeforeMapSet += (insp, prop, map) => map.BatchSize(CollectionBatchSize);
        }
        #endregion ToConfiguration

        #region configureNhibernateValidator
        private void configureNhibernateValidator(NHibernate.Cfg.Configuration config)
        {
            var provider = new NHibernate.Validator.Event.NHibernateSharedEngineProvider();
            NHibernate.Validator.Cfg.Environment.SharedEngineProvider = provider;
            var vtor = NHibernate.Validator.Cfg.Environment.SharedEngineProvider.GetEngine();//new ValidatorEngine();
            var configuration = new NHibernate.Validator.Cfg.Loquacious.FluentConfiguration();

            //Register base entities
            var asmb = typeof(NHConfig).Assembly;
            var entityMaps = asmb.GetExportedTypes().ValidationDefinitions().ToList();

            entityMaps.Add(ValidationMaps);

            //Set custom resource manager
            NHValidatorInterpolator.ResourceMan = ValidatorResource;

            configuration
                .Register(entityMaps //.Assembly //Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace.Contains("Nhibernation.Data"))
                          )
                    .SetDefaultValidatorMode(ValidatorMode.OverrideAttributeWithExternal)
                    .SetMessageInterpolator<NHValidatorInterpolator>()
                //.SetCustomResourceManager("Securiskop.Data.Properties.DefaultValidatorMessages", System.Reflection.Assembly.GetExecutingAssembly())
                    .IntegrateWithNHibernate
                    .ApplyingDDLConstraints().And
                //.AvoidingListenersRegister();
                    .RegisteringListeners();
            vtor.Configure(configuration);
            ValidatorInitializer.Initialize(config, vtor);
        }
        #endregion configureNhibernateValidator

        #region BuildSchema
        /// <summary>Build Db Schema</summary>
        public void BuildSchema()
        {
            //Nhibernate version
            /*var export = new NHibernate. SchemaExport(_nhConfig);
            var sb = new StringBuilder();
            System.IO.TextWriter output = new System.IO.StringWriter(sb);
            export.Execute(true, false, false, false, null, output);*/
        }
        private void buildSchemaOld(ISessionFactory f, NHibernate.Cfg.Configuration config)
        {
            //Fluent NHibernate Version

            // delete the existing db on each run
            // if (File.Exists(DbFile)) File.Delete(DbFile);

            // this NHibernate tool takes a configuration (with mapping info in)
            // and exports a database schema from it
            //new SchemaExport(config).Create(false, true);
            //if (UpdateSchema)

            string s = "";
            var schemaExport = new NHibernate.Tool.hbm2ddl.SchemaUpdate(config);
            try {
                using (var session = f.OpenSession()) {
                    using (var tx = session.BeginTransaction()) {
                        //if (ExportSchemaDelimiter != null) schemaExport.SetDelimiter(";");
                        schemaExport.Execute(a => { s += a; }, true);
                        //new SchemaExport(configuration).Execute(showBuildScript, true, false, session.Connection, str);
                        tx.Commit();
                    }
                }
            }
            catch (Exception exp) {
                //schemaExport.Execute(a => { s += a; }, false);
                throw new Exception($"Schema Update failed, SQL Trace: {s}", exp);
            }

            //var schemaExport = new NHibernate.Tool.hbm2ddl.SchemaExport(config);
            //if (ExportSchemaDelimiter != null) schemaExport.SetDelimiter(";");
            //schemaExport.Create(false, true);
        }

        private void buildSchema(NHibernate.Cfg.Configuration config)
        {
            //Fluent NHibernate Version

            // delete the existing db on each run
            // if (File.Exists(DbFile)) File.Delete(DbFile);

            // this NHibernate tool takes a configuration (with mapping info in)
            // and exports a database schema from it
            //new SchemaExport(config).Create(false, true);
            //if (UpdateSchema)

            string s = "";
            var schemaExport = new NHibernate.Tool.hbm2ddl.SchemaUpdate(config);
            try {
                schemaExport.Execute(a => { s += a; }, true);
                if (ShowSql) System.Diagnostics.Debug.WriteLine("Db Update Script:" + s);
                /*if (s != "") {
                    using (var con = new ConnectionStringSettings("Default", ConnectionString).CreateConnection(true)) {
                        using (var cmd = con.CreateCommand()) {
                            cmd.CommandText = s;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }*/
            }
            catch (Exception exp) {
                //schemaExport.Execute(a => { s += a; }, false);
                throw new Exception($"Schema Update failed, SQL Trace: {s}", exp);
            }

            //var schemaExport = new NHibernate.Tool.hbm2ddl.SchemaExport(config);
            //if (ExportSchemaDelimiter != null) schemaExport.SetDelimiter(";");
            //schemaExport.Create(false, true);
        }

        #endregion BuildSchema

        #region Session Methods
        /// <summary>Open new session</summary>
        public void SessionOpen() { SessionGet(); }
        /// <summary>Get existing session</summary>
        public ISession SessionGet() { return sessionGet(factoryGet()); }
        private ISession sessionGet(ISessionFactory s)
        {
            lock (factoryLock) {
                if (CurrentSessionContext.HasBind(s)) return s.GetCurrentSession();
                ISession session = s.OpenSession();
                CurrentSessionContext.Bind(session);
                return session;
            }
        }
        /// <summary>Close session</summary>
        public void SessionClose() { sessionClose(factoryGet()); }
        private void sessionClose(ISessionFactory s)
        {
            lock (factoryLock) {
                if (s == null) return;
                if (CurrentSessionContext.HasBind(s)) {
                    ISession session = CurrentSessionContext.Unbind(s);
                    if (session != null && session.IsOpen) session.Close();
                }
            }
        }
        /// <summary>Get existing session</summary>
        public IStatelessSession SessionGetStateless()
        {
            lock (factoryLock) {
                return factoryGet().OpenStatelessSession();
            }
        }
        #endregion Session Methods

        #region Factory Methods
        /// <summary>Start factories</summary>
        public void FactoriesStartup()
        {
            factoryGet();
        }
        /// <summary>Stop factories</summary>
        public void FactoriesShutDown()
        {
            factoryClose();
        }
        /// <summary>Get/Create SessionFactory</summary>
        public ISessionFactory FactoryGet() { return factoryGet(); }
        private ISessionFactory factoryGet()
        {
            lock (factoryLock) {
                if (factory == null) {
                    factory = factoryCreate<HybridWebSessionContext>();
                    if (factory.IsClosed) throw new NHibernate.Exceptions.GenericADOException("Database Connection/Structure Error", null);
                }
                return factory;
            }
        }
        private void factoryClose()
        {
            lock (factoryLock) {
                if (factory == null) return;
                factory.Close();
                factory.Dispose();
                factory = null;
            }
        }
        private ISessionFactory factoryCreate<TCurrentSessionContext>() where TCurrentSessionContext : ICurrentSessionContext
        {
            var cfg = ToConfiguration();
            cfg.CurrentSessionContext<TCurrentSessionContext>();
            var rval = cfg.BuildSessionFactory();
            return rval;
        }

        #endregion Factory Methods
    }
}