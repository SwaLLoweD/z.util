using System;
using System.Linq.Expressions;
using NHibernate.Mapping.ByCode;
using Z.Util;
using NHibernate.Mapping.ByCode.Conformist;

namespace Z.Data.NHTools {
    /// <summary>Securiskop Log Entry Entity</summary>
    [Serializable]
    public class SKLog {
        #region Entity
        /// <summary>Log Id</summary>
        public virtual Guid Id { get; set; }
        /// <summary>Log Type</summary>
        public virtual LogTypes Type { get; set; }
        /// <summary>Log Level</summary>
        public virtual LogLevels Level { get; set; }
        /// <summary>Log Message</summary>
        public virtual string Message { get; set; }
        /// <summary>Log Exception Message</summary>
        public virtual string ExpMessage { get; set; }
        /// <summary>Log Exception Trace</summary>
        public virtual string ExpTrace { get; set; }
        ///<summary>Date Added</summary>
        public virtual DateTime DateAdded { get; set; }
        #endregion
        #region Map
        /// <inheritdoc/>
        public sealed class Map :ClassMapping<SKLog> {
            /// <inheritdoc/>
            public Map()
            {
                Id(x => x.Id, c => c.Generator(Generators.GuidComb));
                Property(x => x.Type);
                Property(x => x.Level);
                this.PropertyXlob(x => x.Message, c => c.NotNullable(true));
                this.PropertyXlob(x => x.ExpMessage);
                this.PropertyXlob(x => x.ExpTrace);
                Property(x => x.DateAdded);
            }
        }
        #endregion
        /// <inheritdoc/>
        public SKLog() { }
        /// <summary>Save Log Entry</summary>
        public virtual new void Save(Repo repo = null)
        {
            (repo ?? Repo.Ts).SaveOrUpdateCommit(this);
        }
        /// <summary>Delete logs older than specified amount of days</summary>
        public static void DeleteOlderThan(int days, Repo repo)
        {
            var oldest = DateTime.UtcNow.AddDays(0 - days);
            repo.Hql("DELETE FROM SKLog l WHERE l.DateAdded < :oldest")
                .SetDateTime("oldest", oldest)
                //.SetParameterList("oldest", oldest)
                .ExecuteUpdate();
        }
        /// <summary>Create Log Entry</summary>
        public static SKLog Create(SLogEntry log)
        {
            if (log == null) return null;
            var type = LogTypes.General;
            Enum.TryParse<LogTypes>(log.Type.ToString(), out type);
            var level = LogLevels.Debug;
            Enum.TryParse<LogLevels>(log.Level.ToString(), out level);
            var rval = new SKLog {
                Type = type,
                Level = level,
                Message = log.Title,
                ExpMessage = log.ExpMessage,
                ExpTrace = log.ExpTrace
            };
            return rval;
        }
        #region Expression* Methods
        /// <summary>Expression to search logs by type and/or level</summary>
        public static Expression<Func<SKLog, bool>> ExpSearch(LogTypes? type, LogLevels? level)
        {
            var pb = PredicateBuilder.True<SKLog>();
            if (type.HasValue) pb = pb.And(x => x.Type == type);
            if (level.HasValue) pb = pb.And(x => x.Level == level);
            return pb;
        }
        #endregion
    }
}