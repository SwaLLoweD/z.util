using System;
using System.Linq;
using NHibernate.Mapping.ByCode;
using Z.Util;
using Z.Data.NHTools;
using NHibernate.Mapping.ByCode.Conformist;

namespace Z.Data.NHTools {
    /// <summary>Deleted object store Entity</summary>
    [Serializable]
    public class RecycledObject {
        #region Entity
        /// <summary>Id</summary>
        public virtual Guid Id { get; set; }
        /// <summary>Recycled Entity Id</summary>
        public virtual int EntityId { get; set; }
        /// <summary>Recycled Entity Hash</summary>
        public virtual int EntityHash { get; set; }
        /// <summary>Recycled Entity Type</summary>
        public virtual string Type { get; set; }
        /// <summary>Recycled Entity Assembly Version</summary>
        public virtual string Version { get; set; }
        /// <summary>Recycled Entity Data</summary>
        public virtual byte[] Data { get; set; }
        ///<summary>Date Added</summary>
        public virtual DateTime DateAdded { get; set; }
        /// <summary>Entity Created By Id</summary>
        public virtual int? CreatedBy { get; set; }
        /// <summary>Entity Status Flags Mask</summary>
        protected virtual long StatusMask { get { return Status == null ? 0 : Status.Mask; } set { Status = value; } }
        #endregion
        private Bitmask status;
        /// <summary>Status Flags</summary>
        public virtual Bitmask Status { get { return status = status ?? new Bitmask(); } set { status = value; } }
        #region Map
        /// <inheritdoc/>
        public sealed class Map :ClassMapping<RecycledObject> {
            /// <inheritdoc/>
            public Map() 
            {
                Id(x => x.Id, c => c.Generator(Generators.GuidComb));
                Property(x => x.EntityId);
                Property(x => x.EntityHash);
                Property(x => x.Type);
                Property(x => x.Version);
                this.PropertyXlob(x => x.Data, c => c.NotNullable(true));
                Property(x => x.DateAdded);
                Property(x => x.CreatedBy, c => c.NotNullable(false));
                this.PropertyHidden(x => x.StatusMask);
            }
        }
        #endregion
        /// <summary>Get Recycled object by Entity Id</summary>
        public static RecycledObject GetByEntityId(int id, Repo repo, int hash = -1)
        {
            var pb = PredicateBuilder.True<RecycledObject>();
            pb = pb.And(x => x.EntityId == id);
            if (hash > 0) pb = pb.And(x => x.EntityHash == hash);
            return repo.LinQ<RecycledObject>().Where(pb).FirstOrDefault();
        }
        /// <summary>Purge Recycled entities older than specified days</summary>
        public static void DeleteOlderThan(int days, Repo repo)
        {
            var oldest = DateTime.UtcNow.AddDays(0 - days);
            repo.Hql("DELETE FROM RecycledObject l WHERE l.DateAdded < :oldest")
                .SetDateTime("oldest", oldest)
                //.SetParameterList("oldest", oldest)
                .ExecuteUpdate();
        }
    }
}