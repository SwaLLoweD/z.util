﻿using System;
using NHibernate;
using NHibernate.Mapping.ByCode;
using Z.Extensions;

namespace Z.Data.NHTools
{
    /// <summary>NHibernate configuration</summary>
    public class NHDefaultConventions
    {
        /// <summary>Apply Default conventions</summary>
        public static ModelMapper SetConventions(ModelMapper mapper = null, int defaultCollectionBatchSize = 10, NHConfig cfg = null)
        { return setConventions(mapper, defaultCollectionBatchSize, cfg); }
        private static ModelMapper setConventions(ModelMapper mapper = null, int defaultCollectionBatchSize = 10, NHConfig cfg = null)
        {
            mapper = mapper ?? new ModelMapper();

            mapper.BeforeMapProperty += (insp, prop, map) => map.NotNullable(true); //NotNullable

            mapper.BeforeMapOneToOne += (insp, prop, map) => {
                map.Cascade(Cascade.All.Include(Cascade.DeleteOrphans));
            };
            mapper.BeforeMapManyToOne += (insp, prop, map) => {
                map.Cascade(Cascade.All.Exclude(Cascade.DeleteOrphans).Exclude(Cascade.Remove)); //SaveUpdate
                //map.Column(prop.LocalMember.Name); //+ prop.LocalMember.GetPropertyOrFieldType().Name + "Id");
                map.ForeignKey($"{prop.Owner().Name.Transform().Pluralize()}_{prop.OtherSideType().Name.Transform().Pluralize()}_FKM1");
            };
            mapper.BeforeMapMapKey += (insp, prop, map) => {
                map.Column(prop.GetContainerEntity(insp).Name + "Id");
            };

            mapper.BeforeMapClass += (mi, t, map) => {
                var tableName = t.Name;
                if (cfg != null && cfg.DriverClass.Is().NotEmpty && cfg.DriverClass.EndsWith("MySqlDataDriver")) tableName = t.Name.ToLower();
                map.Id(m => { m.Generator(Generators.Identity); });
                map.Table(tableName.Transform().Pluralize());
            };
            mapper.BeforeMapJoinedSubclass += (mi, t, map) => map.Table(t.Name.Transform().Pluralize());
            mapper.BeforeMapUnionSubclass += (mi, t, map) => map.Table(t.Name.Transform().Pluralize());

            #region Old Code
            // Moved to OneToMany ManyToMany mappings
            //mapper.BeforeMapManyToMany += (insp, prop, map) => {
            //    //map.Column(prop.LocalMember.GetPropertyOrFieldType().Name + "Id");
            //    //map.ForeignKey(prop.LocalMember.Name + "_FK_MM");
            //};
            //mapper.BeforeMapBag += (insp, prop, map) => {
            //    //map.Cascade(Cascade.All.Include(Cascade.DeleteOrphans));
            //    //map.Key(km => km.Column(prop.GetContainerEntity(insp).Name + "Id"));
            //    //map.Inverse(true);
            //};
            //mapper.BeforeMapSet += (insp, prop, map) => {
            //    //map.Cascade(Cascade.All.Include(Cascade.DeleteOrphans));
            //    //map.Key(km => km.Column(prop.GetContainerEntity(insp).Name + "Id"));
            //    //map.Inverse(true);
            //};

            // Map all IPAddress classes to IPAddressUserType
            //public class IPAddressUserTypeConvention : UserTypeConvention<TypeMaps.IPAddressUserType> { }
            // Map all Bitmask classes to BitmaskUserType
            //public class BitmaskUserTypeConvention : UserTypeConvention<TypeMaps.BitmaskUserType> { }

            /*public class BitmaskConvention : IUserTypeConvention
            {
                public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
                { criteria.Expect(x => x.Property.PropertyType == typeof(Bitmask)); }
                public void Apply(IPropertyInstance target)
                { target.CustomType(typeof(long)); }
            }*/
            #endregion Old Code

            return mapper;
        }
    }
}