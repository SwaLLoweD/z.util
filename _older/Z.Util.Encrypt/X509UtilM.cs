﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.X509;
using Mono.Security.X509.Extensions;
using Z.Extensions;

namespace Z.Cryptography
{
    /// <summary>Mono Security encrypt library wrapper</summary>
    public class X509UtilM : X509Util
    {
        #region Constructors
        /// <summary></summary>
        public X509UtilM() { }
        #endregion

        /// <inheritdoc />
        public override X509Certificate2 Generate(X509GenerationRequest request) {
            var subjectKey = getKeyPairGenerator(request.Algorithm, request.Strength);
            AsymmetricAlgorithm issuerKey = null;
            Mono.Security.X509.X509Certificate caCert = null;
            if (request.IssuerCert == null) {
                issuerKey = subjectKey;
            } else {
                if (!request.IssuerCert.HasPrivateKey) throw new InvalidOperationException("Issuer Certificate does not contain private key data");
                caCert = new Mono.Security.X509.X509Certificate(request.IssuerCert.RawData);
                issuerKey = fixCSPVersion(request.IssuerCert.PrivateKey);
            }

            var sn = Guid.NewGuid().ToByteArray();
            if ((sn[0] & 0x80) == 0x80) sn[0] -= 0x80; //Serial Key must be positive

            var cb = new X509CertificateBuilder(3) {
                SerialNumber = sn,
                SubjectName = request.SubjectName,
                IssuerName = request.IssuerCert == null ? request.SubjectName : request.IssuerCert.IssuerName.Name,
                NotBefore = request.StartDate,
                NotAfter = request.ExpiryDate,
                SubjectPublicKey = subjectKey,
                Hash = request.Algorithm.ToString().SubstringBefore("With")
            };

            var bce = new BasicConstraintsExtension {
                PathLenConstraint = BasicConstraintsExtension.NoPathLengthConstraint,
                CertificateAuthority = request.IssuerCert == null
            };
            var eku = new ExtendedKeyUsageExtension();
            foreach (var keyPurpose in request.Purposes) eku.KeyPurpose.Add($"1.3.6.1.5.5.7.3.{(byte) keyPurpose}");

            //eku.KeyPurpose.Add("1.3.6.1.5.5.7.3.1"); // Indicates the cert is intended for server auth
            //eku.KeyPurpose.Add("1.3.6.1.5.5.7.3.2"); // Indicates the cert is intended for client auth

            SubjectAltNameExtension alt = null;


            if (bce != null) cb.Extensions.Add(bce);
            if (eku != null) cb.Extensions.Add(eku);
            if (alt != null) cb.Extensions.Add(alt);

            //var digest = new SHA512Managed();
            //digest.
            //byte[] resBuf = new byte[digest.GetDigestSize()];
            //var spki = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(DotNetUtilities.GetRsaPublicKey(issuerKey));                    
            //byte[] bytes = spki.PublicKeyData.GetBytes();

            //digest.BlockUpdate(bytes, 0, bytes.Length);
            //digest.DoFinal(resBuf, 0);
            //cb.Hash = hashName;

            //cb.Extensions.Add(new SubjectKeyIdentifierExtension { Identifier = resBuf });
            //cb.Extensions.Add(new AuthorityKeyIdentifierExtension { Identifier = resBuf });
            // signature

            var rawcert = cb.Sign(issuerKey);


            var p12 = new PKCS12 {
                Password = request.ExportPassword //pasword
            };
            var list = new ArrayList {
                // we use a fixed array to avoid endianess issues 
                // (in case some tools requires the ID to be 1).
                new byte[4] {1, 0, 0, 0}
            };

            var attributes = new Dictionary<string, ArrayList>(1) {
                {PKCS9.localKeyId, list}
            };

            p12.AddCertificate(new Mono.Security.X509.X509Certificate(rawcert), attributes);
            if (request.IssuerCert != null) p12.AddCertificate(caCert);
            p12.AddPkcs8ShroudedKeyBag(subjectKey, attributes);
            var expCert = p12.GetBytes();

            var rval = new X509Certificate2(expCert, request.ExportPassword, X509KeyStorageFlags.Exportable);
            return rval;
        }

        private AsymmetricAlgorithm getKeyPairGenerator(SignatureAlgorithms alg, int strength) {
            AsymmetricAlgorithm kpgen = null;
            var algStr = alg.ToString();
            if (algStr.EndsWith("WithRSA"))
                kpgen = new RSACryptoServiceProvider(strength);
            else if (algStr.EndsWith("WithDSA"))
                kpgen = new RSACryptoServiceProvider(strength);
            else
                throw new NotSupportedException("Unsupported Algorithm.");
            return kpgen;
        }

        private AsymmetricAlgorithm fixCSPVersion(AsymmetricAlgorithm alg) {
            //WORKAROUND:CSP Parameters provider should be type 24 not 1
            var cp = new CspParameters(24);
            if (alg as RSACryptoServiceProvider != null) {
                var caKey = new RSACryptoServiceProvider(cp);
                caKey.ImportCspBlob((alg as RSACryptoServiceProvider).ExportCspBlob(true));
                return caKey;
            } else if (alg as DSACryptoServiceProvider != null) {
                var caKey = new DSACryptoServiceProvider(cp);
                caKey.ImportCspBlob((alg as DSACryptoServiceProvider).ExportCspBlob(true));
                return caKey;
            }
            return alg;
        }
    }
}