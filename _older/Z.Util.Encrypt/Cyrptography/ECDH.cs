﻿/* Copyright (c) <2017> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Security.Cryptography;

namespace Z.Cryptography;

/// <summary>Defines a base class from which all Diffie-Hellman implementations inherit.</summary>
public abstract class ECDH : AsymmetricAlgorithm
{
    /// <summary>Creates an instance of the default implementation of the <see cref="ECDH" /> algorithm.</summary>
    /// <returns>A new instance of the default implementation of ECDH.</returns>
    public new static ECDH Create() => Create("Z.Cryptography.ECDH");
    /// <summary>Creates an instance of the specified implementation of <see cref="ECDH" />.</summary>
    /// <param name="algName">The name of the implementation of ECDH to use.</param>
    /// <returns>A new instance of the specified implementation of ECDH.</returns>
    public new static ECDH Create(string algName) => (ECDH)CryptoConfig.CreateFromName(algName);

    /// <summary>When overridden in a derived class, creates the key exchange data.</summary>
    /// <returns>The key exchange data to be sent to the intended recipient.</returns>
    public abstract byte[] CreateKeyExchange();
    /// <summary>When overridden in a derived class, extracts secret information from the key exchange data.</summary>
    /// <param name="keyex">The key exchange data within which the secret information is hidden.</param>
    /// <returns>The secret information derived from the key exchange data.</returns>
    public abstract byte[] DecryptKeyExchange(byte[] keyex);

    /// <summary>When overridden in a derived class, exports the <see cref="ECDHParameters" />.</summary>
    /// <param name="includePrivate"><b>true</b> to include private parameters; otherwise, <b>false</b>.</param>
    /// <returns>The parameters for ECDH.</returns>
    public abstract ECDHParameters ExportParameters(bool includePrivate);
    /// <summary>When overridden in a derived class, imports the specified <see cref="ECDHParameters" />.</summary>
    /// <param name="parameters">The parameters for ECDH.</param>
    public abstract void ImportParameters(ECDHParameters parameters);

    // private byte[] GetNamedParam(SecurityElement se, string param) {
    //     var sep = se.SearchForChildByTag(param);
    //     if (sep == null) return null;
    //     return Convert.FromBase64String(sep.Text);
    // }
    // /// <summary>Reconstructs a <see cref="ECDH" /> object from an XML string.</summary>
    // /// <param name="xmlString">The XML string to use to reconstruct the ECDH object.</param>
    // /// <exception cref="CryptographicException">One of the values in the XML string is invalid.</exception>
    // public override void FromXmlString(string xmlString) {
    //     if (xmlString == null) throw new ArgumentNullException("xmlString");
    //
    //     var dhParams = new ECDHParameters();
    //     try {
    //         var sp = new SecurityParser();
    //         sp.LoadXml(xmlString);
    //         var se = sp.ToXml();
    //         if (se.Tag != "ECDHKeyValue") throw new CryptographicException();
    //         dhParams.P = GetNamedParam(se, "P");
    //         dhParams.X = GetNamedParam(se, "X");
    //         ImportParameters(dhParams);
    //     } finally {
    //         if (dhParams.P != null) Array.Clear(dhParams.P, 0, dhParams.P.Length);
    //         if (dhParams.X != null) Array.Clear(dhParams.X, 0, dhParams.X.Length);
    //     }
    // }
    // /// <summary>Creates and returns an XML string representation of the current <see cref="ECDH" /> object.</summary>
    // /// <param name="includePrivateParameters"><b>true</b> to include private parameters; otherwise, <b>false</b>.</param>
    // /// <returns>An XML string encoding of the current ECDH object.</returns>
    // public override string ToXmlString(bool includePrivateParameters) {
    //     var sb = new StringBuilder();
    //     var dhParams = ExportParameters(includePrivateParameters);
    //     try {
    //         sb.Append("<ECDHKeyValue>");
    //
    //         sb.Append("<P>");
    //         sb.Append(Convert.ToBase64String(dhParams.P));
    //         sb.Append("</P>");
    //
    //         if (includePrivateParameters) {
    //             sb.Append("<X>");
    //             sb.Append(Convert.ToBase64String(dhParams.X));
    //             sb.Append("</X>");
    //         }
    //
    //         sb.Append("</ECDHKeyValue>");
    //     } finally {
    //         Array.Clear(dhParams.P, 0, dhParams.P.Length);
    //         if (dhParams.X != null) Array.Clear(dhParams.X, 0, dhParams.X.Length);
    //     }
    //     return sb.ToString();
    // }

    #region Nested type: ECDHParameters
    /// <summary>Represents the parameters of the ECDH algorithm.</summary>
    [Serializable]
    public struct ECDHParameters
    {
        /// <summary>Represents the public <b>P</b> parameter of the ECDH algorithm.</summary>
        public byte[] P;
        /// <summary>Represents the private <b>X</b> parameter of the ECDH algorithm.</summary>
        [NonSerialized] public byte[] X;
    }
    #endregion
}