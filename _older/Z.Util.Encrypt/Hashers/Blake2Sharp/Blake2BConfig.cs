﻿// BLAKE2 reference source code package - C# implementation

// Written in 2012 by Christian Winnerlein  <codesinchaos@gmail.com>

// To the extent possible under law, the author(s) have dedicated all copyright
// and related and neighboring rights to this software to the public domain
// worldwide. This software is distributed without any warranty.

// You should have received a copy of the CC0 Public Domain Dedication along with
// this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

using System;

namespace Blake2Sharp;

/// <summary>Blake2 Encryption Config</summary>
public sealed class Blake2BConfig : ICloneable
{
    #region Fields / Properties
    /// <summary>Key</summary>
    public byte[] Key { get; set; }
    /// <summary>OutputSizeInBits</summary>
    public int OutputSizeInBits {
        get => OutputSizeInBytes * 8;
        set {
            if (value % 8 == 0) throw new ArgumentException("Output size must be a multiple of 8 bits");
            OutputSizeInBytes = value / 8;
        }
    }
    /// <summary>OutputSizeInBytes</summary>
    public int OutputSizeInBytes { get; set; }
    /// <summary>Personalization</summary>
    public byte[] Personalization { get; set; }
    /// <summary>Salt</summary>
    public byte[] Salt { get; set; }
    #endregion

    #region Constructors
    /// <summary><Constructor/summary>
    public Blake2BConfig() => OutputSizeInBytes = 64;
    #endregion

    #region ICloneable Members
    /// <summary>Clone</summary>
    object ICloneable.Clone() => Clone();
    #endregion

    /// <summary>Clone</summary>
    public Blake2BConfig Clone() {
        var result = new Blake2BConfig {
            OutputSizeInBytes = OutputSizeInBytes
        };
        if (Key != null) result.Key = (byte[])Key.Clone();
        if (Personalization != null) result.Personalization = (byte[])Personalization.Clone();
        if (Salt != null) result.Salt = (byte[])Salt.Clone();
        return result;
    }
}