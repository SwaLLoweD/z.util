﻿// BLAKE2 reference source code package - C# implementation

// Written in 2012 by Christian Winnerlein  <codesinchaos@gmail.com>

// To the extent possible under law, the author(s) have dedicated all copyright
// and related and neighboring rights to this software to the public domain
// worldwide. This software is distributed without any warranty.

// You should have received a copy of the CC0 Public Domain Dedication along with
// this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

using System.Security.Cryptography;

namespace Blake2Sharp;

/// <summary>Hasher</summary>
public abstract class Hasher
{
    /// <summary>Init</summary>
    public abstract void Init();
    /// <summary>Finish</summary>
    public abstract byte[] Finish();
    /// <summary>Update</summary>
    public abstract void Update(byte[] data, int start, int count);
    /// <summary>Update</summary>
    public void Update(byte[] data) => Update(data, 0, data.Length);
    /// <summary>HashAlgorithm</summary>
    public HashAlgorithm AsHashAlgorithm() => new HashAlgorithmAdapter(this);

    #region Nested type: HashAlgorithmAdapter
    /// <summary>HashAlgorithmAdapter</summary>
    internal class HashAlgorithmAdapter : HashAlgorithm
    {
        #region Fields / Properties
        private readonly Hasher _hasher;
        #endregion

        #region Constructors
        /// <summary>Constructor</summary>
        public HashAlgorithmAdapter(Hasher hasher) => _hasher = hasher;
        #endregion

        /// <summary>Core</summary>
        protected override void HashCore(byte[] array, int ibStart, int cbSize) => _hasher.Update(array, ibStart, cbSize);
        /// <summary>Final</summary>
        protected override byte[] HashFinal() => _hasher.Finish();
        /// <summary>Init</summary>
        public override void Initialize() => _hasher.Init();
    }
    #endregion
}