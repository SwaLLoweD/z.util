﻿// BLAKE2 reference source code package - C# implementation

// Written in 2012 by Christian Winnerlein  <codesinchaos@gmail.com>

// To the extent possible under law, the author(s) have dedicated all copyright
// and related and neighboring rights to this software to the public domain
// worldwide. This software is distributed without any warranty.

// You should have received a copy of the CC0 Public Domain Dedication along with
// this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

using System;

namespace Blake2Sharp;

/// <summary>Blake2BTreeConfig</summary>
public sealed class Blake2BTreeConfig : ICloneable
{
    #region Fields / Properties
    /// <summary>FanOut</summary>
    public int FanOut { get; set; }
    /// <summary>IntermediateHashSize</summary>
    public int IntermediateHashSize { get; set; }
    /// <summary>LeafSize</summary>
    public long LeafSize { get; set; }
    /// <summary>MaxHeight</summary>
    public int MaxHeight { get; set; }
    #endregion

    #region Constructors
    /// <summary>Constructor</summary>
    public Blake2BTreeConfig() => IntermediateHashSize = 64;
    #endregion

    #region ICloneable Members
    /// <summary>Clone</summary>
    object ICloneable.Clone() => Clone();
    #endregion

    /// <summary>Clone</summary>
    public Blake2BTreeConfig Clone() => new() {
        IntermediateHashSize = IntermediateHashSize,
        MaxHeight = MaxHeight,
        LeafSize = LeafSize,
        FanOut = FanOut
    };
    /// <summary><CreateInterleaved/summary>
    public static Blake2BTreeConfig CreateInterleaved(int parallelism) => new() {
        FanOut = parallelism,
        MaxHeight = 2,
        IntermediateHashSize = 64
    };
}