﻿#region License
/* 
Project home: https://github.com/judwhite/Grassfed.MurmurHash3

MIT License

Copyright (c) 2017 Jud White

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

https://gist.github.com/automatonic/3725443
This code is public domain.
The MurmurHash3 algorithm was created by Austin Appleby and put into the public domain.  See http://code.google.com/p/smhasher/
This C# variant was authored by
Elliott B. Edwards and was placed into the public domain as a gist
Status...Working on verification (Test Suite)
Set up to run as a LinqPad (linqpad.net) script (thus the ".Dump()" call)

Mods
Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. 
*/
#endregion

using System;
using System.IO;

namespace Z.Cryptography;

#region Murmur128x65Hash
/// <summary>MurMur3 128-bit Fast-Hashing algorithm</summary>
public static class Murmur128x64Hash
{
    // 128 bit output, 64 bit platform version

    ///// <summary>Gets the size, in bits, of the computed hash code.</summary>
    ///// <returns>The size, in bits, of the computed hash code.</returns>
    //public static int HashSize => 128;

    /// <summary>Computes the hash value for the specified byte array.</summary>
    /// <param name="buffer">The input to compute the hash code for.</param>
    /// <returns>The computed hash code.</returns>
    /// <exception cref="T:System.ArgumentNullException"><paramref name="buffer" /> is null.</exception>
    public static byte[] ComputeHash(byte[] buffer) => ComputeHash(buffer, 0, buffer.Length);

    /// <summary>Computes the hash value for the specified region of the specified byte array.</summary>
    /// <param name="buffer">The input to compute the hash code for.</param>
    /// <param name="offset">The offset into the byte array from which to begin using data.</param>
    /// <param name="count">The number of bytes in the array to use as data.</param>
    /// <returns>The computed hash code.</returns>
    /// <exception cref="T:System.ArgumentException">
    ///     <paramref name="count" /> is an invalid value.-or-
    ///     <paramref name="buffer" /> length is invalid.
    /// </exception>
    /// <exception cref="T:System.ArgumentNullException"><paramref name="buffer" /> is null.</exception>
    /// <exception cref="T:System.ArgumentOutOfRangeException">
    ///     <paramref name="offset" /> is out of range. This parameter
    ///     requires a non-negative number.
    /// </exception>
    public static unsafe byte[] ComputeHash(byte[] buffer, int offset, int count) {
        if (buffer == null) throw new ArgumentNullException(nameof(buffer));
        if (offset < 0) throw new ArgumentOutOfRangeException(nameof(offset), offset, "offset must be >= 0");
        if (count < 0) throw new ArgumentOutOfRangeException(nameof(count), count, "count must be >= 0");
        if (offset + count > buffer.Length) throw new ArgumentException($"offset ({offset}) + count ({count}) exceed buffer length ({buffer.Length})");

        const ulong c1 = 0x87c37b91114253d5;
        const ulong c2 = 0x4cf5ad432745937f;

        var nblocks = count / 16;

        ulong h1 = 0;
        ulong h2 = 0;

        // body
        fixed (byte* pbuffer = buffer) {
            var pinput = pbuffer + offset;
            var body = (ulong*)pinput;

            ulong k1;
            ulong k2;

            for (var i = 0; i < nblocks; i++) {
                k1 = body[i * 2];
                k2 = body[(i * 2) + 1];

                k1 *= c1;
                k1 = (k1 << 31) | (k1 >> (64 - 31)); // ROTL64(k1, 31);
                k1 *= c2;
                h1 ^= k1;

                h1 = (h1 << 27) | (h1 >> (64 - 27)); // ROTL64(h1, 27);
                h1 += h2;
                h1 = (h1 * 5) + 0x52dce729;

                k2 *= c2;
                k2 = (k2 << 33) | (k2 >> (64 - 33)); // ROTL64(k2, 33);
                k2 *= c1;
                h2 ^= k2;

                h2 = (h2 << 31) | (h2 >> (64 - 31)); // ROTL64(h2, 31);
                h2 += h1;
                h2 = (h2 * 5) + 0x38495ab5;
            }

            // tail

            k1 = 0;
            k2 = 0;

            var tail = pinput + (nblocks * 16);
            switch (count & 15) {
                case 15:
                    k2 ^= (ulong)tail[14] << 48;
                    goto case 14;
                case 14:
                    k2 ^= (ulong)tail[13] << 40;
                    goto case 13;
                case 13:
                    k2 ^= (ulong)tail[12] << 32;
                    goto case 12;
                case 12:
                    k2 ^= (ulong)tail[11] << 24;
                    goto case 11;
                case 11:
                    k2 ^= (ulong)tail[10] << 16;
                    goto case 10;
                case 10:
                    k2 ^= (ulong)tail[9] << 8;
                    goto case 9;
                case 9:
                    k2 ^= tail[8];
                    k2 *= c2;
                    k2 = (k2 << 33) | (k2 >> (64 - 33)); // ROTL64(k2, 33);
                    k2 *= c1;
                    h2 ^= k2;
                    goto case 8;
                case 8:
                    k1 ^= (ulong)tail[7] << 56;
                    goto case 7;
                case 7:
                    k1 ^= (ulong)tail[6] << 48;
                    goto case 6;
                case 6:
                    k1 ^= (ulong)tail[5] << 40;
                    goto case 5;
                case 5:
                    k1 ^= (ulong)tail[4] << 32;
                    goto case 4;
                case 4:
                    k1 ^= (ulong)tail[3] << 24;
                    goto case 3;
                case 3:
                    k1 ^= (ulong)tail[2] << 16;
                    goto case 2;
                case 2:
                    k1 ^= (ulong)tail[1] << 8;
                    goto case 1;
                case 1:
                    k1 ^= tail[0];
                    k1 *= c1;
                    k1 = (k1 << 31) | (k1 >> (64 - 31)); // ROTL64(k1, 31);
                    k1 *= c2;
                    h1 ^= k1;
                    break;
            }
        }

        // finalization
        h1 ^= (ulong)count;
        h2 ^= (ulong)count;

        h1 += h2;
        h2 += h1;

        h1 = FMix64(h1);
        h2 = FMix64(h2);

        h1 += h2;
        h2 += h1;

        var ret = new byte[16];
        fixed (byte* pret = ret) {
            var ulpret = (ulong*)pret;

            ulpret[0] = Reverse(h1);
            ulpret[1] = Reverse(h2);
        }
        return ret;
    }

    private static ulong FMix64(ulong k) {
        k ^= k >> 33;
        k *= 0xff51afd7ed558ccd;
        k ^= k >> 33;
        k *= 0xc4ceb9fe1a85ec53;
        k ^= k >> 33;
        return k;
    }

    private static ulong Reverse(ulong value) =>
        ((value & 0x00000000000000FFUL) << 56) | ((value & 0x000000000000FF00UL) << 40) |
        ((value & 0x0000000000FF0000UL) << 24) | ((value & 0x00000000FF000000UL) << 8) |
        ((value & 0x000000FF00000000UL) >> 8) | ((value & 0x0000FF0000000000UL) >> 24) |
        ((value & 0x00FF000000000000UL) >> 40) | ((value & 0xFF00000000000000UL) >> 56);
}
#endregion

#region Murmur32Hash
public static class MurMur32Hash
{
    #region Constants / Static Fields
    //Change to suit your needs
    private const uint defaultSeed = 144;
    #endregion

    /// <summary>Computes the hash value for the specified byte array.</summary>
    /// <param name="buffer">The input to compute the hash code for.</param>
    /// <returns>The computed hash code.</returns>
    /// <exception cref="T:System.ArgumentNullException"><paramref name="buffer" /> is null.</exception>
    public static byte[] ComputeHash(byte[] buffer, uint seed = defaultSeed) {
        using var s = new MemoryStream(buffer);
        return ComputeHash(buffer, seed);
    }

    public static int ComputeHash(Stream stream, uint seed = defaultSeed) {
        const uint c1 = 0xcc9e2d51;
        const uint c2 = 0x1b873593;

        var h1 = seed;
        uint streamLength = 0;

        using (var reader = new BinaryReader(stream)) {
            var chunk = reader.ReadBytes(4);
            while (chunk.Length > 0) {
                streamLength += (uint)chunk.Length;
                uint k1;
                switch (chunk.Length) {
                    case 4:
                        /* Get four bytes from the input into an uint */
                        k1 = (uint)
                            (chunk[0]
                             | (chunk[1] << 8)
                             | (chunk[2] << 16)
                             | (chunk[3] << 24));

                        /* bitmagic hash */
                        k1 *= c1;
                        k1 = Rotl32(k1, 15);
                        k1 *= c2;

                        h1 ^= k1;
                        h1 = Rotl32(h1, 13);
                        h1 = (h1 * 5) + 0xe6546b64;
                        break;
                    case 3:
                        k1 = (uint)
                            (chunk[0]
                             | (chunk[1] << 8)
                             | (chunk[2] << 16));
                        k1 *= c1;
                        k1 = Rotl32(k1, 15);
                        k1 *= c2;
                        h1 ^= k1;
                        break;
                    case 2:
                        k1 = (uint)
                            (chunk[0]
                             | (chunk[1] << 8));
                        k1 *= c1;
                        k1 = Rotl32(k1, 15);
                        k1 *= c2;
                        h1 ^= k1;
                        break;
                    case 1:
                        k1 = (uint)chunk[0];
                        k1 *= c1;
                        k1 = Rotl32(k1, 15);
                        k1 *= c2;
                        h1 ^= k1;
                        break;
                }
                chunk = reader.ReadBytes(4);
            }
        }

        // finalization, magic chants to wrap it all up
        h1 ^= streamLength;
        h1 = Fmix(h1);

        unchecked //ignore overflow
        {
            return (int)h1;
        }
    }

    private static uint Rotl32(uint x, byte r) => (x << r) | (x >> (32 - r));

    private static uint Fmix(uint h) {
        h ^= h >> 16;
        h *= 0x85ebca6b;
        h ^= h >> 13;
        h *= 0xc2b2ae35;
        h ^= h >> 16;
        return h;
    }
}
    #endregion