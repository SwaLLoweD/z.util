﻿/* Copyright (c) <2008> <Jasmin Muharemovic>
modified by A.Zafer Yurdaçalış

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

// This is the FNV1-a Variant of the Hash algorithm (but this has slow)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Z.Cryptography;

/// <summary>Fowler-Noll-Vo Hashing algorithm</summary>
public static class FnvHash
{
    /*public static void GetHashBytes()
    {
        var sb = new System.Text.StringBuilder();

        sb.Append("private static readonly byte[] fnv_prime_32");
        sb.Append(GetByteConstructor(fnv_prime_32.ToByteArray(), 32));
        sb.Append("private static readonly byte[] fnv_prime_64");
        sb.Append(GetByteConstructor(fnv_prime_64.ToByteArray(), 64));
        sb.Append("private static readonly byte[] fnv_prime_128");
        sb.Append(GetByteConstructor(fnv_prime_128.ToByteArray(), 128));
        sb.Append("private static readonly byte[] fnv_prime_256");
        sb.Append(GetByteConstructor(fnv_prime_256.ToByteArray(), 256));
        sb.Append("private static readonly byte[] fnv_prime_512");
        sb.Append(GetByteConstructor(fnv_prime_512.ToByteArray(), 512));
        sb.Append("private static readonly byte[] fnv_prime_1024");
        sb.Append(GetByteConstructor(fnv_prime_1024.ToByteArray(), 1024));

        sb.Append("private static readonly byte[] fnv_offset_32");
        sb.Append(GetByteConstructor(fnv_offset_32.ToByteArray(), 32));
        sb.Append("private static readonly byte[] fnv_offset_64");
        sb.Append(GetByteConstructor(fnv_offset_64.ToByteArray(), 64));
        sb.Append("private static readonly byte[] fnv_offset_128");
        sb.Append(GetByteConstructor(fnv_offset_128.ToByteArray(), 128));
        sb.Append("private static readonly byte[] fnv_offset_256");
        sb.Append(GetByteConstructor(fnv_offset_256.ToByteArray(), 256));
        sb.Append("private static readonly byte[] fnv_offset_512");
        sb.Append(GetByteConstructor(fnv_offset_512.ToByteArray(), 512));
        sb.Append("private static readonly byte[] fnv_offset_1024");
        sb.Append(GetByteConstructor(fnv_offset_1024.ToByteArray(), 1024));

        sb.Append("private static readonly byte[] fnv_mod_32");
        sb.Append(GetByteConstructor(fnv_mod_32.ToByteArray(),32));
        sb.Append("private static readonly byte[] fnv_mod_64");
        sb.Append(GetByteConstructor(fnv_mod_64.ToByteArray(), 64));
        sb.Append("private static readonly byte[] fnv_mod_128");
        sb.Append(GetByteConstructor(fnv_mod_128.ToByteArray(), 128));
        sb.Append("private static readonly byte[] fnv_mod_256");
        sb.Append(GetByteConstructor(fnv_mod_256.ToByteArray(), 256));
        sb.Append("private static readonly byte[] fnv_mod_512");
        sb.Append(GetByteConstructor(fnv_mod_512.ToByteArray(), 512));
        sb.Append("private static readonly byte[] fnv_mod_1024");
        sb.Append(GetByteConstructor(fnv_mod_1024.ToByteArray(), 1024));

        var cons = sb.ToString();
    }
    public static string GetByteConstructor(byte[] bytes, int bits)
    {
        var sb = new System.Text.StringBuilder();
        sb.Append(" = new byte[] {");
        for (int i = 0; i< bytes.Length; i++) {
            //if (i >= bytes.Length) sb.Append("0x00, ");
            //else
            sb.Append("0x" + bytes[i].ToString("X").PadLeft(2,'0') + ", ");
        }
        sb.Remove(sb.Length - 2, 2);
        sb.Append("};\r\n");
        return sb.ToString();
    } */

    /// <summary>Get Fowler-Noll-Vo hash</summary>
    /// <param name="bytes"></param>
    /// <param name="hashBitSize">32,64,128,256,512,1024 bits</param>
    /// <returns></returns>
    public static byte[] ComputeHash(IEnumerable<byte> bytes, int hashBitSize = 64) {
        if (bytes == null) return null;
        var value = bytes.ToArray();
        BigInteger fnv_prime;
        BigInteger fnv_offset;
        BigInteger fnv_mod;
        if (hashBitSize <= 32) {
            fnv_prime = fnv_prime_32;
            fnv_offset = fnv_offset_32;
            fnv_mod = fnv_mod_32;
        } else if (hashBitSize <= 64) {
            fnv_prime = fnv_prime_64;
            fnv_offset = fnv_offset_64;
            fnv_mod = fnv_mod_64;
        } else if (hashBitSize <= 128) {
            fnv_prime = fnv_prime_128;
            fnv_offset = fnv_offset_128;
            fnv_mod = fnv_mod_128;
        } else if (hashBitSize <= 256) {
            fnv_prime = fnv_prime_256;
            fnv_offset = fnv_offset_256;
            fnv_mod = fnv_mod_256;
        } else if (hashBitSize <= 512) {
            fnv_prime = fnv_prime_512;
            fnv_offset = fnv_offset_512;
            fnv_mod = fnv_mod_512;
        } else if (hashBitSize <= 1024) {
            fnv_prime = fnv_prime_1024;
            fnv_offset = fnv_offset_1024;
            fnv_mod = fnv_mod_1024;
        } else {
            throw new ArgumentOutOfRangeException(nameof(hashBitSize));
        }
        var hash = fnv_offset;
        for (var i = 0; i < value.Length; i++) {
            hash ^= (uint)value[i];
            hash %= fnv_mod;
            hash *= fnv_prime;
        }
        if (!IsPowerOfTwo(hashBitSize)) {
            var mask = BigInteger.Parse(
                new string('f', (hashBitSize / 4) + (hashBitSize % 4 != 0 ? 1 : 0)
                ) /*, System.Globalization.NumberStyles.HexNumber*/);
            hash = (hash >> hashBitSize) ^ (mask & hash);
        }
        var len = (int)Math.Ceiling((double)hashBitSize / 8);
        var hashBytes = hash.ToByteArray();
        if (hashBytes.Length < len) {
            var zeroFill = new byte[len];
            Buffer.BlockCopy(hashBytes, 0, zeroFill, 0, hashBytes.Length);
            hashBytes = zeroFill;
        }
        return hashBytes.Reverse().ToArray();
    }

    private static bool IsPowerOfTwo(int number) => (number & (number - 1)) == 0;

    #region Constants
    private static readonly BigInteger fnv_prime_32 = BigInteger.Parse("16777619");
    private static readonly BigInteger fnv_prime_64 = BigInteger.Parse("1099511628211");
    private static readonly BigInteger fnv_prime_128 = BigInteger.Parse("309485009821345068724781371");
    private static readonly BigInteger fnv_prime_256 = BigInteger.Parse("374144419156711147060143317175368453031918731002211");
    private static readonly BigInteger fnv_prime_512 = BigInteger.Parse("35835915874844867368919076489095108449946327955754392558399825615420669938882575126094039892345713852759");
    private static readonly BigInteger fnv_prime_1024 =
        BigInteger.Parse("5016456510113118655434598811035278955030765345404790"
                         + "74430301752383111205510814745150915769222029538271616265187852689"
                         + "52493852922918165243750837466913718040942718731604847379667202603"
                         + "89217684476157468082573");
    private static readonly BigInteger fnv_offset_32 =
        BigInteger.Parse("2166136261");
    private static readonly BigInteger fnv_offset_64 =
        BigInteger.Parse("14695981039346656037");
    private static readonly BigInteger fnv_offset_128 =
        BigInteger.Parse("275519064689413815358837431229664493455");
    private static readonly BigInteger fnv_offset_256 =
        BigInteger.Parse("10002925795805258090707096862062570483709279601424119"
                         + "3945225284501741471925557");
    private static readonly BigInteger fnv_offset_512 =
        BigInteger.Parse("96593031294966694980094354007163104660904187456726378"
                         + "961083743294344626579945829321977164384498130518922065398057844953"
                         + "28239340083876191928701583869517785");
    private static readonly BigInteger fnv_offset_1024 =
        BigInteger.Parse("14197795064947621068722070641403218320880622795441933"
                         + "960878474914617582723252296732303717722150864096521202355549365628"
                         + "174669108571814760471015076148029755969804077320157692458563003215"
                         + "304957150157403644460363550505412711285966361610267868082893823963"
                         + "790439336411086884584107735010676915");

    private static readonly BigInteger fnv_mod_32 = BigInteger.Pow(2, 32);
    private static readonly BigInteger fnv_mod_64 = BigInteger.Pow(2, 64);
    private static readonly BigInteger fnv_mod_128 = BigInteger.Pow(2, 128);
    private static readonly BigInteger fnv_mod_256 = BigInteger.Pow(2, 256);
    private static readonly BigInteger fnv_mod_512 = BigInteger.Pow(2, 512);
    private static readonly BigInteger fnv_mod_1024 = BigInteger.Pow(2, 1024);

    /*
private static readonly byte[] fnv_prime_32 = new byte[] {0x93, 0x01, 0x00, 0x01};
private static readonly byte[] fnv_prime_64 = new byte[] {0xB3, 0x01, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_prime_128 = new byte[] {0x3B, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_prime_256 = new byte[] {0x63, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_prime_512 = new byte[] {0x57, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_prime_1024 = new byte[] {0x8D, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_offset_32 = new byte[] {0xC5, 0x9D, 0x1C, 0x81, 0x00};
private static readonly byte[] fnv_offset_64 = new byte[] {0x25, 0x23, 0x22, 0x84, 0xE4, 0x9C, 0xF2, 0xCB, 0x00};
private static readonly byte[] fnv_offset_128 = new byte[] {0x8F, 0x7F, 0x30, 0x32, 0xBF, 0x88, 0x2F, 0xF5, 0xD2, 0x93, 0xB2, 0x6C, 0xAC, 0x0A, 0x47, 0xCF, 0x00};
private static readonly byte[] fnv_offset_256 = new byte[] {0x35, 0x05, 0xEE, 0xCA, 0xC8, 0xB4, 0x23, 0x10, 0xB3, 0xBB, 0xB6, 0x47, 0x68, 0x53, 0xB1, 0xC8, 0xCC, 0x76, 0xE5, 0xC4, 0x84, 0xC3, 0x98, 0x2D, 0x36, 0x50, 0xC5, 0xAA, 0xBC, 0x8D, 0x26, 0xDD, 0x00};
private static readonly byte[] fnv_offset_512 = new byte[] {0xD9, 0x9F, 0xFE, 0x4A, 0xAC, 0x2A, 0x98, 0xAC, 0x4B, 0xE3, 0x56, 0x5F, 0x41, 0x36, 0x20, 0x18, 0xCE, 0xE7, 0xDB, 0x42, 0xC9, 0x9B, 0xA7, 0x2E, 0xF6, 0x92, 0xC1, 0x34, 0x8A, 0xF6, 0x48, 0xE9, 0x21, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC9, 0x59, 0xD0, 0x87, 0xAC, 0xAC, 0x90, 0x99, 0x30, 0x0F, 0xE5, 0xA1, 0xDC, 0x16, 0x44, 0x1F, 0x17, 0xB1, 0xB0, 0x6D, 0xB8, 0x00};
private static readonly byte[] fnv_offset_1024 = new byte[] {0xB3, 0x90, 0xEE, 0x71, 0x6C, 0xB1, 0xF4, 0xAF, 0x21, 0x3B, 0xA9, 0xC6, 0xC9, 0x8C, 0xDE, 0x6B, 0x55, 0xAE, 0x05, 0xC0, 0x6C, 0x25, 0x5F, 0x55, 0x0A, 0x51, 0x34, 0x27, 0x80, 0x73, 0x6E, 0xEB, 0xD7, 0xC6, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xD9, 0x21, 0x9A, 0xDA, 0x74, 0x36, 0xDA, 0x4E, 0xF3, 0x3B, 0x6C, 0xA1, 0xAD, 0xFD, 0x23, 0x42, 0xFC, 0x29, 0x4B, 0xB7, 0x28, 0x10, 0x59, 0x5A, 0x6D, 0xE5, 0x32, 0x4D, 0xCC, 0x8E, 0x75, 0x76, 0x7A, 0x5F};
private static readonly byte[] fnv_mod_32 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_mod_64 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_mod_128 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_mod_256 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_mod_512 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
private static readonly byte[] fnv_mod_1024 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};

     */
    #endregion Constants
}