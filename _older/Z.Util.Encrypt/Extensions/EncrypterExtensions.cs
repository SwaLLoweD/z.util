﻿using Z.Cryptography;
using Z.Util;

namespace Z.Extensions
{
    /// <summary>Extension methods for Encrypter</summary>
    public static partial class ExtZEncrypter
    {
        // ///// <summary>Bouncy Castle AES Encryption.(Key sizes: 0 to 256, Block sizes: 128)</summary>
        // //public static EncryptBouncyBase BC_Aes(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        // //{ return new BouncyAes(enc, keySize, feedbackSize, salt, padding, mode); }
        // /// <summary>Mono X509 Certificate tools.</summary>
        // public static X509UtilM M_X509(this Encrypter enc) => new X509UtilM();
    }
}