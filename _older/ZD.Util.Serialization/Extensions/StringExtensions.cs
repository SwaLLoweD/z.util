﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using ServiceStack.Text;

namespace Z.Extensions
{
    /// <summary>Extension methods for Object Deserialization...</summary>
    public static partial class ExtStringSerialize
    {
        /// <summary>Deserialize a string into an object using ServiceStack JSON Serializer.</summary>
        public static T SSJson<T>(this ExtString.DeserializePortion p) { return JsonSerializer.DeserializeFromString<T>(p.Value); }
        /// <summary>Deserialize a string into an object using ServiceStack JSV Serializer.</summary>
        public static T SSJsv<T>(this ExtString.DeserializePortion p) { return TypeSerializer.DeserializeFromString<T>(p.Value); }
        /// <summary>Deserialize a string into an object using ServiceStack CSV Serializer.</summary>
        public static T SSCsv<T>(this ExtString.DeserializePortion p) { return (T) SSCsv(p, typeof(T)); }
        /// <summary>Deserialize a string into an object using ServiceStack XML Serializer.</summary>
        public static T SSXml<T>(this ExtString.DeserializePortion p) { return XmlSerializer.DeserializeFromString<T>(p.Value); }


        /// <summary>Deserialize a string into an object using ServiceStack JSON Serializer.</summary>
        public static object SSJson(this ExtString.DeserializePortion p, Type objectType) { return JsonSerializer.DeserializeFromString(p.Value, objectType); }
        /// <summary>Deserialize a string into an object using ServiceStack JSV Serializer.</summary>
        public static object SSJsv(this ExtString.DeserializePortion p, Type objectType) { return TypeSerializer.DeserializeFromString(p.Value, objectType); }
        /// <summary>Deserialize a string into an object using ServiceStack XML Serializer.</summary>
        public static object SSXml(this ExtString.DeserializePortion p, Type objectType) { return XmlSerializer.DeserializeFromString(p.Value, objectType); }
        /// <summary>Deserialize a string into an object using ServiceStack CSV Serializer.</summary>
        public static object SSCsv(this ExtString.DeserializePortion p, Type objectType)
        {
            var rval = objectType.DefaultValue();
            if (p.Value.Is().Empty) return rval;
            using (var m = new MemoryStream(p.Value.To().ByteArray.AsUtf8)) {
                rval = m.Deserialize().DotNetXml(objectType);
            }
            return rval;
        }
    }
}