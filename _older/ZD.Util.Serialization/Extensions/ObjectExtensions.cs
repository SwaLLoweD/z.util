﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.IO;
using ServiceStack.Text;

namespace Z.Extensions
{
    /// <summary>Extension methods for Object Serialization...</summary>
    public static partial class ExtObjectSerialize
    {
        #region SSJson
        /// <summary>Serialize an object into a stream using ServiceStack JSON Serializer.</summary>
        public static Stream SSJson<T>(this ExtObject.SerializePortion<T> p, Stream s)
        {
            JsonSerializer.SerializeToStream(p.Value, p.Value.GetType(), s);
            return s;
        }
        /// <summary>Serialize an object into a string using ServiceStack JSON Serializer.</summary>
        public static string SSJson<T>(this ExtObject.SerializePortion<T> p)
        { return JsonSerializer.SerializeToString(p.Value, p.Value.GetType()); }
        /// <summary>Serialize an object into a file using ServiceStack JSON Serializer.</summary>
        public static void SSJson<T>(this ExtObject.SerializePortion<T> p, string file)
        {
            using (var s = new FileStream(file, FileMode.OpenOrCreate)) {
                p.SSJson(s);
            }
        }
        #endregion SSJson

        #region SSJsv
        /// <summary>Serialize an object into a stream using ServiceStack JSV Serializer.</summary>
        public static Stream SSJsv<T>(this ExtObject.SerializePortion<T> p, Stream s)
        {
            TypeSerializer.SerializeToStream(p.Value, p.Value.GetType(), s);
            return s;
        }
        /// <summary>Serialize an object into a string using ServiceStack JSV Serializer.</summary>
        public static string SSJsv<T>(this ExtObject.SerializePortion<T> p)
        { return TypeSerializer.SerializeToString(p.Value, p.Value.GetType()); }
        /// <summary>Serialize an object into a file using ServiceStack JSV Serializer.</summary>
        public static void SSJsv<T>(this ExtObject.SerializePortion<T> p, string file)
        {
            using (var s = new FileStream(file, FileMode.OpenOrCreate)) {
                p.SSJsv(s);
            }
        }
        #endregion SSJsv

        #region SSCsv
        /// <summary>Serialize an object into a stream using ServiceStack CSV Serializer.</summary>
        public static Stream SSCsv<T>(this ExtObject.SerializePortion<T> p, Stream s)
        {
            CsvSerializer.SerializeToStream(p.Value, s);
            return s;
        }
        /// <summary>Serialize an object into a string using ServiceStack CSV Serializer.</summary>
        public static string SSCsv<T>(this ExtObject.SerializePortion<T> p)
        { return CsvSerializer.SerializeToString(p.Value); }
        /// <summary>Serialize an object into a file using ServiceStack CSV Serializer.</summary>
        public static void SSCsv<T>(this ExtObject.SerializePortion<T> p, string file)
        {
            using (var s = new FileStream(file, FileMode.OpenOrCreate)) {
                p.SSCsv(s);
            }
        }
        #endregion SSCsv

        #region SSXml
        /// <summary>Serialize an object into a stream using ServiceStack Xml Serializer.</summary>
        public static Stream SSXml<T>(this ExtObject.SerializePortion<T> p, Stream s)
        {
            XmlSerializer.SerializeToStream(p.Value, s);
            return s;
        }
        /// <summary>Serialize an object into a string using ServiceStack Xml Serializer.</summary>
        public static string SSXml<T>(this ExtObject.SerializePortion<T> p)
        { return XmlSerializer.SerializeToString(p.Value); }
        /// <summary>Serialize an object into a file using ServiceStack Xml Serializer.</summary>
        public static void SSXml<T>(this ExtObject.SerializePortion<T> p, string file)
        {
            using (var s = new FileStream(file, FileMode.OpenOrCreate)) {
                p.SSCsv(s);
            }
        }
        #endregion SSXml

        #region SSQueryString
        /// <summary>Serialize an object into a string using ServiceStack QueryString Serializer.</summary>
        public static string SSQueryString<T>(this ExtObject.SerializePortion<T> p)
        { return ServiceStack.Text.QueryStringSerializer.SerializeToString(p.Value); }
        #endregion SSQueryString
    }
}