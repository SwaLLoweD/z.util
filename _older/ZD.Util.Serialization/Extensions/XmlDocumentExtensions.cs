﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.IO;
using ServiceStack.Text;

namespace Z.Extensions
{
    /// <summary>Extension methods for Object Deserialization...</summary>
    public static partial class ExtXmlDocumentSerialize
    {
        /// <summary>Deserialize a Xml into an object using ServiceStack XML Serializer.</summary>
        public static T SSXml<T>(this ExtXmlDocument.DeserializePortion p) { return (T)SSXml(p, typeof(T)); }
        /// <summary>Deserialize a Xml into an object using ServiceStack XML Serializer.</summary>
        public static object SSXml(this ExtXmlDocument.DeserializePortion p, Type objectType)
        {
            var rval = objectType.DefaultValue();
            if (p.Value == null) return rval;
            using (var m = new MemoryStream(p.Value.InnerText.To().ByteArray.AsUtf8)) {
                rval = XmlSerializer.DeserializeFromStream(objectType, m);
            }
            return rval;
        }
    }
}