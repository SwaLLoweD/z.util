﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using ServiceStack.Text;
using Z.Collections.Generic;
using Z.Extensions;

namespace Z.Util.Localization
{
    /// <summary>Json based ResourceManager</summary>
    public class ResourceManagerJson : System.Resources.ResourceManager
    {
        /// <summary>Resource Storage</summary>
        public ResourceStoreJson ResourceStore { get; set; }

        /// <summary>Json based ResourceManager</summary>
        /// <param name="baseName">Path of the JSON embedded Resource</param>
        /// <param name="assembly">Assembly JSON resource is embedded into</param>
        public ResourceManagerJson(string baseName, Assembly assembly = null)
        { ResourceStore = new ResourceStoreJson(baseName, assembly); }

        /// <inheritdoc/>
        public override string GetString(string key, CultureInfo culture = null)
        { return ResourceStore.GetString(key, culture); }

        /// <inheritdoc/>
        public override void ReleaseAllResources()
        {
            ResourceStore.Clear();
        }
    }

    /// <summary>Multiple Resource Storage</summary>
    public class ResourceStoreJson : Dictionary<string, ResourceJson>
    {
        /// <summary>Path of the JSON-based embedded Resource</summary>
        public string Name { get; set; }
        /// <summary>Assembly of this resource</summary>
        public System.Reflection.Assembly Assembly { get; set; }

        /// <inheritdoc/>
        public new ResourceJson this[string key]
        {
            get
            {
                lock (this) {
                    if (!ContainsKey(key)) {
                        if (loadFile(key) && ContainsKey(key)) return base[key];
                    }
                    return base[key];
                }
            }
            set
            {
                lock (this) {
                    base[key] = value;
                }
            }
        }
        /// <summary>Multiple Resource Storage</summary>
        /// <param name="name">Path of the JSON-based embedded Resource</param>
        /// <param name="assembly">Assembly JSON resource is embedded into</param>
        public ResourceStoreJson(string name, Assembly assembly = null)
        {
            Name = name;
            Assembly = assembly ?? Assembly.GetCallingAssembly();
            loadFile();
            loadFile(CultureInfo.CurrentCulture);
        }
        private bool loadFile(CultureInfo culture = null)
        {
            return loadFile(getCultureString(culture));
        }
        private bool loadFile(string cult)
        {
            var cultExt = (cult == "default") ? null : "-" + cult;
            if (base[cult] != null) return true; //Already loaded
            try {
                using (Stream stream = Assembly.GetManifestResourceStream($"{Name}{cultExt}.json")) {
                    if (stream == null) return false;
                    using (StreamReader reader = new StreamReader(stream)) {
                        //string result = reader.ReadToEnd();
                        var rj = JsonSerializer.DeserializeFromReader<ResourceJson>(reader);
                        if (rj == null) return false;
                        base[cult] = rj;
                        return true;
                    }
                }
            }
            catch {
                base.Remove(cult);
                return false;
            }
        }
        private string getCultureString(CultureInfo culture)
        {
            var cult = culture == null ? null : culture.TwoLetterISOLanguageName;
            return cult ?? "default";
        }
        /// <summary>Get string for specified key and culture</summary>
        public string GetString(string key, CultureInfo culture = null)
        {
            var cult = getCultureString(culture ?? CultureInfo.CurrentCulture);
            var store = this[cult];
            if (store != null && store.TryGetValue(key) != null) return store[key];
            store = this[getCultureString(null)];
            if (store != null) return store.TryGetValue(key);
            return null;
        }
    }

    /// <summary>Json-based ResourceManager</summary>
    [Serializable]
    public class ResourceJson : Dictionary<string, string>
    {
    }
}