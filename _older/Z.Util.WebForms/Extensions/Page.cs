﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.IO;
using System.Text;
using System.Web.UI;

namespace Z.Extensions
{
    /// <summary>Web Page extensions</summary>
    public static partial class ExtWebPage
    {
        /// <summary>Executes ASPX page and returns output</summary>
        /// <param name="p"></param>
        /// <param name="fileName">ASP.NET page to execute</param>
        /// <returns></returns>
        public static string PageExecute(this Page p, string fileName)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter hWriter = new HtmlTextWriter(sw);
            p.Server.Execute(fileName, hWriter);
            return sb.ToString();
        }
        /// <summary>Gets control request name ("$ format" to "_ format")</summary>
        /// <param name="p"></param>
        /// <param name="c">Control to get name for</param>
        /// <returns></returns>
        public static string RequestName(this Page p, Control c)
        {
            if (p == null || c == null) return null;
            var requestId = getClientIdPrefix(p) + c.ClientID.Replace("_", "$");
            return requestId;
        }
        /// <summary>Returns current value of a control using POST/GET Requests</summary>
        /// <param name="p"></param>
        /// <param name="c">Control to get value for</param>
        /// <returns></returns>
        public static string RequestControl(this Page p, Control c)
        {
            return p.Request[RequestName(p, c)];
        }
        private static string getClientIdPrefix(Page p)
        {
            if (p.Master == null) return "";
            return "ctl00$";
        }
    }
}