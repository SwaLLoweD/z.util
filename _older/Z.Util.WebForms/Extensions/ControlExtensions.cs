﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;

namespace Z.Extensions
{
    /// <summary>Web control extensions</summary>
    public static partial class ExtWebControls
    {
        #region Find Controls
        /// <summary>Performs a typed search of a control within the current naming container.</summary>
        /// <typeparam name = "T">The control type</typeparam>
        /// <param name = "control">The parent control / naming container to search within.</param>
        /// <param name = "id">The id of the control to be found.</param>
        /// <returns>The found control</returns>
        public static T FindControl<T>(this Control control, string id) where T : class
        {
            return (control.FindControl(id) as T);
        }

        /// <summary>Finds the control.</summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "control">The root control.</param>
        /// <param name = "comparison">The comparison.</param>
        /// <returns>The T</returns>
        public static T FindControl<T>(this Control control, Func<T, bool> comparison) where T : class
        {
            foreach (Control child in control.Controls) {
                var ctl = (child as T);
                if ((ctl != null) && comparison(ctl)) return ctl;

                ctl = FindControl(child, comparison);
                if (ctl != null) return ctl;
            }
            return null;
        }

        /// <summary>Finds a control by its ID recursively</summary>
        /// <param name = "control">The root parent control.</param>
        /// <param name = "id">The id of the control to be found.</param>
        /// <returns>The found control</returns>
        public static Control FindControlRecursive(this Control control, string id)
        {
            foreach (Control child in control.Controls) {
                if ((child.ID != null) && string.Equals(child.ID, id, StringComparison.InvariantCultureIgnoreCase)) return child;

                var ctl = FindControlRecursive(child, id);
                if (ctl != null) return ctl;
            }
            return null;
        }

        /// <summary> Finds a control by its ID recursively</summary>
        /// <typeparam name = "T">The control type</typeparam>
        /// <param name = "control">The root parent control.</param>
        /// <param name = "id">The id of the control to be found.</param>
        /// <returns>The found control</returns>
        public static T FindControlRecursive<T>(this Control control, string id) where T : class
        {
            foreach (Control child in control.Controls) {
                if ((child.ID != null) && string.Equals(child.ID, id, StringComparison.InvariantCultureIgnoreCase) && (child is T)) return (child as T);

                var ctl = FindControlRecursive<T>(child, id);
                if (ctl != null) return ctl;
            }
            return null;
        }

        /// <summary>Returns the first matching parent control.</summary>
        /// <typeparam name = "T">The typ of the requested parent control.</typeparam>
        /// <param name = "control">The control to start the search on.</param>
        /// <returns>The found control</returns>
        public static T GetParent<T>(this Control control) where T : class
        {
            var parent = control.Parent;
            while (parent != null) {
                if (parent is T) return (parent as T);
                parent = parent.Parent;
            }
            return null;
        }

        /// <summary>Returns all direct child controls matching to the supplied type.</summary>
        /// <typeparam name = "T"></typeparam>
        /// <param name = "control">The control.</param>
        /// <returns></returns>
        /// <example>
        ///   <code>
        ///     foreach(var textControl in GetChildControlsByType&lt;ITextControl&gt;()) {
        ///     textControl.Text = "...";
        ///     }
        ///   </code>
        /// </example>
        public static IEnumerable<T> GetChildControlsByType<T>(this Control control) where T : class
        {
            foreach (Control childControl in control.Controls) {
                if (childControl is T) yield return (childControl as T);
            }
        }

        #endregion Find Controls

        #region Visiblity (Disabled)
        /*
        /// <summary>Sets the visibility of one or more controls</summary>
        /// <param name = "control">The root control.</param>
        /// <param name = "controls">The controls to be set visible.</param>
        public static void SetVisibility(this Control control, params Control[] controls)
        {
            control.SetVisibility(true, controls);
        }

        /// <summary>Sets the visibility of one or more controls</summary>
        /// <param name = "control">The root control.</param>
        /// <param name = "visible">if set to <c>true</c> [visible].</param>
        /// <param name = "controls">The controls to be set visible.</param>
        public static void SetVisibility(this Control control, bool visible, params Control[] controls)
        {
            Array.ForEach(controls, c => c.Visible = visible);
        }

        /// <summary>Sets the visibility of one or more controls</summary>
        /// <param name = "control">The root control.</param>
        /// <param name = "controlIDs">The control IDs.</param>
        public static void SetVisibility(this Control control, params string[] controlIDs)
        {
            control.SetVisibility(true, controlIDs);
        }

        /// <summary>
        ///   Sets the visibility of one or more controls
        /// </summary>
        /// <param name = "control">The root control.</param>
        /// <param name = "visible">if set to <c>true</c> [visible].</param>
        /// <param name = "controlIDs">The control IDs.</param>
        public static void SetVisibility(this Control control, bool visible, params string[] controlIDs)
        {
            foreach (var id in controlIDs) {
                var ctl = control.FindControlRecursive(id);
                if (ctl != null)
                    ctl.Visible = visible;
            }
        }

        /// <summary>
        ///   Sets the visibility of one or more controls
        /// </summary>
        /// <param name = "control">The root control.</param>
        /// <param name = "condition">The condition.</param>
        /// <param name = "controls">The controls to be set visible.</param>
        public static void SetVisibility(this Control control, Predicate<Control> condition, params Control[] controls)
        {
            Array.ForEach(controls, c => c.Visible = condition(c));
        }

        /// <summary>
        ///   Sets the visibility of one or more controls
        /// </summary>
        /// <param name = "control">The root control.</param>
        /// <param name = "condition">The condition.</param>
        /// <param name = "controlIDs">The control IDs.</param>
        public static void SetVisibility(this Control control, Predicate<Control> condition, params string[] controlIDs)
        {
            foreach (var id in controlIDs) {
                var ctl = control.FindControlRecursive(id);
                if (ctl != null) ctl.Visible = condition(ctl);
            }
        }

        /// <summary>
        ///   Switches the visibility of two controls.
        /// </summary>
        /// <param name = "control">The root control.</param>
        /// <param name = "visible">The visible control.</param>
        /// <param name = "notVisible">The not visible control.</param>
        public static void SwitchVisibility(this Control control, Control visible, Control notVisible)
        {
            visible.Visible = true;
            notVisible.Visible = false;
        }
        */
        #endregion Visiblity (Disabled)

        /// <summary>Render control HTML</summary>
        /// <param name="ctrl"></param>
        /// <returns>Control HTML as string</returns>
        public static string RenderToString(this Control ctrl)
        {
            string rval = string.Empty;
            StringBuilder sb = new StringBuilder();
            using (StringWriter tw = new StringWriter(sb)) {
                using (HtmlTextWriter hw = new HtmlTextWriter(tw)) {
                    ctrl.RenderControl(hw);
                    rval = sb.ToString();
                }
            }
            return rval;
        }
        /// <summary>Checks if control is inside an update panel</summary>
        /// <param name="ctrl"></param>
        /// <returns>true if control is inside an update panel, else false</returns>
        public static bool IsInsideUpdatePanel(this Control ctrl)
        {
            if (ctrl == null || ctrl.Parent == null) return false;
            if (ctrl.Parent.GetType() == typeof(UpdatePanel)) return true;
            if (IsInsideUpdatePanel(ctrl.Parent)) return true;
            return false;
        }
    }
}