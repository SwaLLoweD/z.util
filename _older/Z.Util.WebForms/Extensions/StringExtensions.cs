﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Web;

namespace Z.Extensions
{
    /// <summary>Web-related string extensions</summary>
    public static partial class ExtWebString
    {
        #region Encode Decode
        /// <summary>Encode string for HTML Attribute</summary>
        public static string EncodeHtmlAttribute(this ExtString.TransformPortion s) { return s.ValueStr == null ? null : HttpUtility.HtmlAttributeEncode(s.ValueStr); }
        /// <summary>Encode string for HTML</summary>
        public static string EncodeHtml(this ExtString.TransformPortion s) { return s.ValueStr == null ? null : HttpUtility.HtmlEncode(s.ValueStr); }
        /// <summary>Dencode HTML-encoded string</summary>
        public static string DecodeHtml(this ExtString.TransformPortion s) { return s.ValueStr == null ? null : HttpUtility.HtmlDecode(s.ValueStr); }
        /// <summary>Encode string for URL</summary>
        public static string EncodeUrl(this ExtString.TransformPortion s) { return s.ValueStr == null ? null : HttpUtility.UrlEncode(s.ValueStr); }
        /// <summary>Dencode URL-encoded string </summary>
        public static string DecodeUrl(this ExtString.TransformPortion s) { return s.ValueStr == null ? null : HttpUtility.UrlDecode(s.ValueStr); }
        #endregion Encode Decode
    }
}