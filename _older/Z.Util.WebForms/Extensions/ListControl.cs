﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Web.UI.WebControls;

namespace Z.Extensions
{
    /// <summary>ListControl extensions</summary>
    public static partial class ExtWebListControl
    {
        /// <summary>Fills all values from an enum to a listcontrol</summary>
        /// <param name="l">ListControl</param>
        /// <typeparam name="T">Enum to fill</typeparam>
        public static void FillFromEnum<T>(this ListControl l) where T : struct
        {
            var enumType = typeof(T);
            Array Values = System.Enum.GetValues(enumType);
            foreach (int Value in Values) {
                string Display = Enum.GetName(enumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                l.Items.Add(Item);
            }
        }
        /// <summary>Return all of the selected items</summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public static ListItemCollection GetSelectedItems(this ListControl l)
        {
            var rval = new ListItemCollection();
            if (l == null) return rval;
            foreach (ListItem item in l.Items) {
                if (item != null && item.Selected) rval.Add(item);
            }
            return rval;
        }
        /// <summary>Add new list item</summary>
        /// <param name="l"></param>
        /// <param name="name">Item Name</param>
        /// <param name="value">Item Value</param>
        /// <param name="enabled">Item Enabled</param>
        /// <returns></returns>
        public static ListItem Add(this ListItemCollection l, string name, string value, bool enabled = true)
        {
            var li = new ListItem(name, value, enabled);
            l.Add(li);
            return li;
        }
        /// <summary>Add new list items</summary>
        /// <param name="l"></param>
        /// <param name="names">Item names to add</param>
        public static void AddRange(this ListItemCollection l, params string[] names)
        {
            names.ForEach(x => l.Add(x));
        }
    }
}