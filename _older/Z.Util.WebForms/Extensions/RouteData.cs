﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Web.Routing;

namespace Z.Extensions
{
    /// <summary>Web Page extensions</summary>
    public static partial class ExtRouteData
    {
        /// <summary>Get value for specified key</summary>
        /// <param name="rd"></param>
        /// <param name="key">Key to get value for</param>
        /// <param name="defaultValue">Value to return if no value is found</param>
        /// <returns></returns>
        public static string GetValue(this RouteData rd, string key, string defaultValue = null)
        {
            string rval = defaultValue;
            if (!rd.Values.ContainsKey(key)) return rval;
            if (rd.Values[key] == null) return rval;
            rval = rd.Values[key].ToString();
            return rval;
        }
    }
}