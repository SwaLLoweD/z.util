﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Web.UI.WebControls;

namespace Z.Extensions
{
    /// <summary>Table extensions</summary>
    public static partial class ExtWebTable
    {
        /// <summary>Add rows to table</summary>
        public static Table AddRows(this Table thisTable, params TableRow[] rows)
        {
            thisTable.Rows.AddRange(rows);
            return thisTable;
        }

        /// <summary>Add cells to row</summary>
        public static TableRow AddCells(this TableRow thisTableRow, params TableCell[] cells)
        {
            thisTableRow.Cells.AddRange(cells);
            return thisTableRow;
        }

        /// <summary>Add rows to table</summary>
        public static TableRow AddRows(this Table p, int count = 1, int index = -1)
        {
            if (count <= 0) return null;
            var tr = new TableRow();
            if (index < 0) {
                p.Rows.Add(tr);
                AddRows(p, count - 1);
            }
            else {
                p.Rows.AddAt(index, tr);
                AddRows(p, count - 1, index + 1);
            }
            return tr;
        }
        /// <summary>Add columns to table</summary>
        public static void AddColumns(this Table p, int count = 1, int index = -1)
        {
            if (count <= 0) return;
            if (index < 0) {
                foreach (TableRow tr in p.Rows) {
                    var tc = new TableCell();
                    tr.Cells.Add(tc);
                    AddColumns(p, count - 1);
                }
            }
            else {
                foreach (TableRow tr in p.Rows) {
                    var tc = new TableCell();
                    tr.Cells.AddAt(index, tc);
                    AddColumns(p, count - 1, index + 1);
                }
            }
        }
        /// <summary>Get a table cell at specified ordinals</summary>
        public static TableCell GetCell(this Table p, int row, int column)
        {
            return p.Rows[row].Cells[column];
        }
    }
}