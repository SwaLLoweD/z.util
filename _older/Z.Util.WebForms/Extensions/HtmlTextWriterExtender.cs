﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Securist.Util
{
    public static class HtmlTextWriterExtender
    {
        public static void RenderCell(this HtmlTextWriter writer, string content, string cssClass = "")
        {
            writer.WriteBeginTag("td");

            writer.WriteAttribute("valign", "top");

            if (!string.IsNullOrEmpty(cssClass))
                writer.WriteAttribute("class", cssClass);

            writer.Write(HtmlTextWriter.TagRightChar);

            writer.Write(content);

            writer.WriteEndTag("td");
        }

        public static void RenderTable(this HtmlTextWriter writer, Action callback = null, string cssClass = "", string border = "")
        {
            writer.WriteBeginTag("table");

            if (!string.IsNullOrEmpty(cssClass))
                writer.WriteAttribute("class", cssClass);

            if (!string.IsNullOrEmpty(border))
                writer.WriteAttribute("border", border);

            writer.Write(HtmlTextWriter.TagRightChar);

            if (callback != null)
                callback();

            writer.WriteEndTag("table");
        }

        public static void RenderRow(this HtmlTextWriter writer, Action callback = null, string cssClass = "")
        {
            writer.WriteBeginTag("tr");

            if (!string.IsNullOrEmpty(cssClass))
                writer.WriteAttribute("class", cssClass);

            writer.Write(HtmlTextWriter.TagRightChar);

            if (callback != null)
                callback();

            writer.WriteEndTag("tr");
        }

        public static void RenderCell(this HtmlTextWriter writer, Action callback = null, int colspan = 0, string cssClass = null)
        {
            writer.WriteBeginTag("td");

            writer.WriteAttribute("class", "rpt-cell " + cssClass);

            if (colspan > 0)
                writer.WriteAttribute("colspan", colspan.ToString(System.Globalization.CultureInfo.InvariantCulture));

            writer.Write(HtmlTextWriter.TagRightChar);

            if (callback != null)
                callback();

            writer.WriteEndTag("td");
        }

        public static void RenderDiv(this HtmlTextWriter writer, string cssClass, Action callback = null)
        {
            writer.WriteBeginTag("div");

            if (!string.IsNullOrEmpty(cssClass))
                writer.WriteAttribute("class", cssClass);

            writer.Write(HtmlTextWriter.TagRightChar);

            if (callback != null)
                callback();

            writer.WriteEndTag("div");
        }

        public static void RenderLink(this HtmlTextWriter writer, bool printMode, Action callback = null, string cssClass = null, string url = "#", string target = "")
        {
            if (!printMode)
            {
                writer.WriteBeginTag("a");

                writer.WriteAttribute("href", url);

                if (!string.IsNullOrEmpty(cssClass))
                    writer.WriteAttribute("class", cssClass);

                if (!string.IsNullOrEmpty(target))
                    writer.WriteAttribute("target", target);

                writer.Write(HtmlTextWriter.TagRightChar);
            }

            if (callback != null)
                callback();

            if (!printMode)
                writer.WriteEndTag("a");
        }

        public static void RenderHeader(this HtmlTextWriter writer, string level, Action callback = null, string cssClass = "")
        {
            writer.WriteBeginTag("h" + level);

            if (!string.IsNullOrEmpty(cssClass))
                writer.WriteAttribute("class", cssClass);

            writer.Write(HtmlTextWriter.TagRightChar);

            if (callback != null)
                callback();

            writer.WriteEndTag("h" + level);
        }

        public static void RenderLineEnd(this HtmlTextWriter writer)
        {
            writer.WriteBeginTag("br");
            writer.Write("/>");
        }

        /*public static void RenderAccordion(this HtmlTextWriter writer, Action callback)
        {
            writer.RenderDiv("rpt-accordion", callback);
        }

        public static void RenderAccordionLine(this HtmlTextWriter writer, string level, Action header, Action body)
        {
            writer.RenderHeader(level, header, "rpt-accordion-header ui-state-default ui-corner-all");

            writer.RenderDiv("", body);
        }*/
    }
}
