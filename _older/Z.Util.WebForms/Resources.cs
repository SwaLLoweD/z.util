/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Web.UI;

//RateIt
[assembly: WebResource("Z.Util.Scripts.jquery.rateit.min.js", "application/x-javascript")]
[assembly: WebResource("Z.Util.Images.rateit.delete.gif", "image/gif")]
[assembly: WebResource("Z.Util.Images.rateit.star.gif", "image/gif")]
[assembly: WebResource("Z.Util.Styles.rateit.css", "text/css", PerformSubstitution = true)]

//McDropdown
[assembly: WebResource("Z.Util.Scripts.jquery.bgiframe.js", "application/x-javascript")]
[assembly: WebResource("Z.Util.Scripts.jquery.mcdropdown.min.js", "application/x-javascript")]
[assembly: WebResource("Z.Util.Images.mcdropdown.mcdd_select_button_sprite.gif", "image/gif")]
[assembly: WebResource("Z.Util.Images.mcdropdown.mcdd_icon_normal.gif", "image/gif")]
[assembly: WebResource("Z.Util.Images.mcdropdown.mcdd_icon_hover.gif", "image/gif")]
[assembly: WebResource("Z.Util.Images.mcdropdown.shadow.png", "image/png")]
[assembly: WebResource("Z.Util.Styles.jquery.mcdropdown.css", "text/css", PerformSubstitution = true)]

//IpBox
[assembly: WebResource("Z.Util.Scripts.jquery.caret.js", "application/x-javascript")]
[assembly: WebResource("Z.Util.Scripts.jquery.ipaddress.js", "application/x-javascript")]

//Collapsible Panel
[assembly: WebResource("Z.Util.Scripts.jquery.collapsiblepanel.js", "application/x-javascript")]