﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Web.UI.WebControls;

namespace Z.Util
{
    /// <summary>Web-related method library</summary>
    public static partial class FncWeb
    {
        /// <summary>Fills all enum values to a list</summary>
        /// <param name="enumType">Enum</param>
        /// <param name="list">List to fill</param>
        /// <param name="clearFirst">Clear the list first or not</param>
        public static void EnumToList(Type enumType, IList list, bool clearFirst = true)
        {
            if (list == null) return;
            if (clearFirst) list.Clear();
            Array Values = System.Enum.GetValues(enumType);
            foreach (object Value in Values) {
                string Display = enumToString(enumType, Value);
                ListItem Item = new ListItem(Display, (Value).ToString());
                list.Add(Item);
            }
        }
        /// <summary>Fills all enum values to a list</summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="list">List to fill</param>
        /// <param name="clearFirst">Clear the list first or not</param>
        public static void EnumToList<T>(IList list, bool clearFirst = true) where T : struct
        {
            var enumType = typeof(T);
            if (list == null) return;
            if (clearFirst) list.Clear();
            Array Values = System.Enum.GetValues(enumType);
            foreach (object Value in Values) {
                string Display = enumToString(enumType, Value);
                ListItem Item = new ListItem(Display, (Value).ToString());
                list.Add(Item);
            }
        }
        private static string enumToString(Type EnumType, object Value)
        {
            string Display = "{Enum_" + EnumType.Name + "_" + Enum.GetName(EnumType, Value) + "}";
            return Display;
        }
    }
}