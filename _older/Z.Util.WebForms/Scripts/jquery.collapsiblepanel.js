﻿function CPUncollapse(cPanel, props, timer) {
    if ($(cPanel).is(':visible')) return;
    if (props) {
        $(cPanel).animate(props, timer).show();
    }
    else $(cPanel).show(timer);
}
function CPCollapse(cPanel, props, timer) {
    if ($(cPanel).is(':hidden')) return;
    if (props) {
        $(cPanel).animate(props, timer).hide();
    }
    else $(cPanel).hide(timer);
}