﻿(function ($) {
    $.fn.updnValidatorCallout = function (options) {
        options = $.extend({}, $.updnValidatorCallout.defaults, options);

        function _updateInputs(val, className, add) {
            if (val.controltovalidate) {
                $('#' + val.controltovalidate).toggleClass(className, add);
            }
            if (val.controlhookup) {
                $('#' + val.controlhookup).toggleClass(className, add);
            }
        }

        function _updateLabels(val, className, add) {
            if (val.controltovalidate) {
                $('label[for=' + val.controltovalidate + ']').toggleClass(className, add);
            }
            if (val.controlhookup) {
                $('label[for=' + val.controltovalidate + ']').toggleClass(className, add);
            }
        }

        function _updateControls(validators) {
            if (AllValidatorsValid(validators)) {
                $.each(validators, function () {
                    _updateInputs(this, options.errorInputCssClass, false);
                    _updateLabels(this, options.errorLabelCssClass, false);
                });
            } else {
                $.each(validators, function () {
                    if (!this.isvalid) {
                        _updateInputs(this, options.errorInputCssClass, true);
                        _updateLabels(this, options.errorLabelCssClass, true);
                    }
                });
            }
        }

        function _hideAllCallouts() {
            $('.' + options.calloutCssClass).hide();
        }

        function _showFirstCallout(control) {
            $.each(control.Validators, function () {
                if (!this.isvalid) {
                    $(this).data('callout').show().position({
                        my: options.my,
                        at: options.at,
                        of: control,
                        offset: options.offset,
                        collision: options.collision
                    });
                    return false;
                }
            });
        }

        var _ValidatorOnChange = ValidatorOnChange;
        ValidatorOnChange = function (event) {
            _ValidatorOnChange(event);
            var ev = $.event.fix(event || window.event);
            var input = ev.target;
            if (input && input.Validators) {
                _updateControls(input.Validators);
            }
        };

        var _Page_ClientValidate = Page_ClientValidate;
        Page_ClientValidate = function (validationGroup) {
            var isValid = _Page_ClientValidate(validationGroup);
            _updateControls(Page_Validators);
            return isValid;
        };

        return this.each(function () {
            this.focusOnError = 't';
            if (this.controltovalidate) {
                var $controltovalidate = $('#' + this.controltovalidate);
                var $callout = $('<div></div>').addClass(options.calloutCssClass).appendTo(document.body).hide();
                $('<div></div>').addClass(options.pointerCssClass).appendTo($callout);
                $(this).data('callout', $callout).appendTo($callout);
                $controltovalidate.focus(function () {
                    _hideAllCallouts();
                    _showFirstCallout(this);
                }).blur(function () {
                    _hideAllCallouts();
                });
            }
        });
    }

    $.updnValidatorCallout = {
        defaults: {
            calloutCssClass: 'updnValidatorCallout',
            pointerCssClass: 'updnValidatorCalloutPointer',
            errorInputCssClass: 'updnValidationErrorInput',
            errorLabelCssClass: 'updnValidationErrorLabel',
            my: 'left top',
            at: 'right top',
            collision: 'fit fit',
            offset: '10 -3'
        },
        attachAll: function (options) {
            if (window.Page_Validators) {
                $(Page_Validators).updnValidatorCallout(options);
            }
        }
    };
})(jQuery);