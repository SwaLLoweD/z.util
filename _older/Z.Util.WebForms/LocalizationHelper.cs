﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections;
using System.Linq;
using System.Resources;
using System.Web.UI;
using Z.Util.Text;

namespace Z.Util
{
    /// <summary>Localization related resource management</summary>
    public class LocalizationHelper : LocalizationHelperBase
    {
        private static Type[] excludedControls = new Type[] {
            typeof(System.Web.UI.HtmlControls.HtmlGenericControl),
            typeof(LiteralControl)};
        private static string[] propNames = new string[] { "Text", "ToolTip", "ErrorMessage", "GroupingText" };
        private static string[] propNamesTree = new string[] { "Items", "ChildItems" };

        /// <summary>Performs resource manager replacements on a control and its all children</summary>
        /// <param name="p">Control to search for replacements</param>
        /// <param name="rm">Custom resource manager</param>
        public static void Translate(Control p, ResourceManager rm = null) => translate(p, rm);
        private static void translate(Control p, ResourceManager rm = null)
        {
            foreach (System.Web.UI.Control c in p.Controls) {
                if (excludedControls.Contains<Type>(c.GetType())) continue;
                foreach (string propName in propNames) {
                    var pi = c.GetType().GetProperty(propName);
                    if (pi == null) continue;
                    if (pi.PropertyType != typeof(string)) continue;
                    string curval = (pi.GetValue(c, null) == null) ? null : pi.GetValue(c, null).ToString();
                    if (string.IsNullOrEmpty(curval)) continue;
                    if (pi != null) try { pi.SetValue(c, Translate(curval, rm), null); }
                        catch { continue; }
                }
                translate(c, rm);
            }
            foreach (string propNameTree in propNamesTree) {
                var piItems = p.GetType().GetProperty(propNameTree);
                if (piItems == null) continue;
                if (piItems.PropertyType.GetInterface("ICollection") != null) translate(piItems.GetValue(p, null) as ICollection, rm);
            }
        }
        private static void translate(ICollection p, ResourceManager rm = null)
        {
            if (p == null) return;
            foreach (object it in p) {
                if (excludedControls.Contains<Type>(it.GetType())) continue;
                foreach (string propName in propNames) {
                    var pi = it.GetType().GetProperty(propName);
                    if (pi == null) continue;
                    if (pi.PropertyType != typeof(string)) continue;
                    string curval = (pi.GetValue(it, null) == null) ? null : pi.GetValue(it, null).ToString();
                    if (string.IsNullOrEmpty(curval)) continue;
                    if (pi != null) pi.SetValue(it, Translate(curval, rm), null);
                }
                if (it is Control) translate(it as Control, rm);
                else {
                    foreach (string propNameTree in propNamesTree) {
                        var piItems = it.GetType().GetProperty(propNameTree);
                        if (piItems == null) continue;
                        if (piItems.PropertyType.GetInterface("ICollection") != null) translate(piItems.GetValue(it, null) as ICollection, rm);
                    }
                }
            }
        }
    }
}