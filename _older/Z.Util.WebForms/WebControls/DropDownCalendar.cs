/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
namespace Securist.Util.WebControls {
    using System;
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    [DefaultProperty("SelectedDate"), ToolboxData("<{0}:DropDownCalendar runat=server></{0}:DropDownCalendar>")]
    public class DropDownCalendar :CompositeControl {
        public DropDownList lday = new DropDownList();
        public DropDownList lmonth = new DropDownList();
        public DropDownList lyear = new DropDownList();
        public static int[] monthdays = new int[] { 0x1f, 0x1d, 0x1f, 30, 0x1f, 30, 0x1f, 0x1f, 30, 0x1f, 30, 0x1f };

        #region Control Code
        protected override void CreateChildControls()
        {
            int i;
            lday.ID = "Day";
            lmonth.ID = "Month";
            lyear.ID = "Year";
            for (i = 1; i < 0x20; i++) {
                lday.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (i = 1; i < 13; i++) {
                lmonth.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (i = MinYear; i <= MaxYear; i++) {
                lyear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            lday.SelectedIndex = SelectedDate.Day - 1;
            lmonth.SelectedIndex = SelectedDate.Month - 1;
            lyear.ClearSelection();
            lyear.Items.FindByText(SelectedDate.Year.ToString()).Selected = true;
            Controls.Add(lday);
            Controls.Add(GetSeperator());
            Controls.Add(lmonth);
            Controls.Add(GetSeperator());
            Controls.Add(lyear);
            base.CreateChildControls();
        }
        protected Control GetSeperator()
        {
            Literal lit = new Literal();
            lit.Text = HttpUtility.HtmlEncode(DateSeperator);
            return lit;
        }
        #endregion
        #region Properties
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true)]
        public string DateSeperator
        {
            get
            {
                string s = (string) ViewState["DateSeperator"];
                return ((s == null) ? "-" : s);
            }
            set
            {
                ViewState["DateSeperator"] = value;
            }
        }

        [Localizable(true), Category("Appearance"), DefaultValue(""), Bindable(true)]
        public int MaxYear
        {
            get
            {
                int d = DateTime.UtcNow.AddYears(0).Year;
                if (ViewState["MaxYear"] != null) {
                    d = (int) ViewState["MaxYear"];
                }
                return d;
            }
            set
            {
                ViewState["MaxYear"] = value;
            }
        }

        [DefaultValue(""), Bindable(true), Category("Appearance"), Localizable(true)]
        public int MinYear
        {
            get
            {
                int d = DateTime.UtcNow.AddYears(-80).Year;
                if (ViewState["MinYear"] != null) {
                    d = (int) ViewState["MinYear"];
                }
                return d;
            }
            set
            {
                ViewState["MinYear"] = value;
            }
        }

        [Localizable(true), Bindable(true), Category("Appearance"), DefaultValue("")]
        public DateTime SelectedDate
        {
            get
            {
                DateTime d = DateTime.UtcNow;
                int nmonth = d.Month;
                int nday = d.Day;
                int nyear = d.Year;
                int maxday = 0x1f;
                if (lmonth.SelectedIndex != -1) {
                    maxday = monthdays[lmonth.SelectedIndex];
                    if (((lmonth.SelectedIndex == 1) && (lyear.SelectedItem != null)) && ((Convert.ToInt32(lyear.SelectedItem.Text) % 4) == 0)) {
                        maxday = 0x1c;
                    }
                    nmonth = lmonth.SelectedIndex + 1;
                }
                if ((lday.SelectedIndex + 1) > maxday) {
                    lday.SelectedIndex = maxday - 1;
                }
                if (lday.SelectedIndex != -1) {
                    nday = lday.SelectedIndex + 1;
                }
                if (lyear.SelectedIndex != -1) {
                    nyear = Convert.ToInt32(lyear.SelectedItem.Text);
                }
                return new DateTime(nyear, nmonth, nday);
            }
            set
            {
                lday.SelectedIndex = value.Day - 1;
                lmonth.SelectedIndex = value.Month - 1;
                lyear.ClearSelection();
                if (lyear.Items.Count > 0) {
                    lyear.Items.FindByText(value.Year.ToString()).Selected = true;
                }
            }
        }
        #endregion
    }
}

