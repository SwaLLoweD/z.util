/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Z.Extensions;

namespace Z.Util.WebControls
{
    /// <summary>Rating control to use with different sources</summary>
    [DefaultProperty("Value"), ToolboxData("<{0}:RateItCustom runat=server></{0}:RateItCustom>")]
    public class RateItCustom : CompositeControl
    {
        /// <summary>Constructor</summary>
        public RateItCustom() { }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            if (Step == 0) Step = 1;

            if (PnlRateIt == null) PnlRateIt = new Panel();
            if (DdlRateIt == null) DdlRateIt = new DropDownList();

            Control c = null;
            if (BackingControl.Is().NotEmpty) c = Page.FindControlRecursive(BackingControl);
            if (c != null) {
                PnlRateIt.Attributes.Add("data-rateit-backingfld", "#" + c.ClientID);
            }
            else {
                for (float i = MinValue; i <= MaxValue; i = i + Step)
                    DdlRateIt.Items.Add(new ListItem(i.ToString(), i.ToString()));

                PnlRateIt.Attributes.Add("data-rateit-backingfld", "#" + DdlRateIt.ClientID);
            }
            PnlRateIt.Attributes.Add("data-rateit-min", MinValue.ToString());
            PnlRateIt.Attributes.Add("data-rateit-max", MaxValue.ToString());
            PnlRateIt.Attributes.Add("data-rateit-value", Value.ToString());
            PnlRateIt.Attributes.Add("data-rateit-step", Step.ToString());
            PnlRateIt.Attributes.Add("data-rateit-readonly", Readonly ? "true" : "false");
            PnlRateIt.Attributes.Add("data-rateit-resetable", Resetable ? "true" : "false");
            if (StarWidth > 0) PnlRateIt.Attributes.Add("data-rateit-starwidth", StarWidth.ToString());
            if (StarHeight > 0) PnlRateIt.Attributes.Add("data-rateit-starheight", StarHeight.ToString());
            base.CreateChildControls();
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (CssRateIt.Is().Empty) {
                CssRateIt = "rateit";
                string cssUrl = this.Page.ClientScript.GetWebResourceUrl(typeof(RateIt), "Z.Util.Styles.rateit.css");
                HtmlLink cssLink = new HtmlLink();
                cssLink.Href = cssUrl;
                cssLink.Attributes.Add("rel", "stylesheet");
                cssLink.Attributes.Add("type", "text/css");
                Page.Header.Controls.Add(cssLink);
            }
            PnlRateIt.CssClass = CssRateIt + " " + CssClass;
            //Need testing for non-default css class
            string script = "$(document).ready(function (){ " +
            "$('#" + PnlRateIt.ClientID + "').rateit({" +
            "max: " + MaxValue;
            /*", step: " + Step;
            if (BackingControl.Is().NotEmpty)
            {
                var c = FindControl(BackingControl);
                if (c != null) script += ", backingfld: '#" + c.ClientID + "' ";
            }*/
            script += "}); });";

            if (this.IsInsideUpdatePanel()) {
                //ScriptManager.RegisterClientScriptResource(this, typeof(RateIt), "Z.Util.Scripts.jquery.min.js");
                ScriptManager.RegisterClientScriptResource(this, typeof(RateIt), "Z.Util.Scripts.jquery.rateit.min.js");
                ScriptManager.RegisterStartupScript(this, typeof(RateIt), this.ID, script, true);
            }
            else {
                //Page.ClientScript.RegisterClientScriptResource(typeof(RateIt), "Z.Util.Scripts.jquery.min.js");
                Page.ClientScript.RegisterClientScriptResource(typeof(RateIt), "Z.Util.Scripts.jquery.rateit.min.js");
                Page.ClientScript.RegisterStartupScript(typeof(RateIt), this.ID, script, true);
            }
        }
        #endregion Control Code

        #region Properties
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(0)]
        public float MinValue { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(5)]
        public float MaxValue { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(0)]
        public float Value { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(1)]
        public float Step { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(false)]
        public bool Readonly { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(true)]
        public bool Resetable { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public int StarWidth { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public int StarHeight { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue("rateIt")]
        public string CssRateIt { get; set; }
        /// <summary></summary>
        [TypeConverter(typeof(Control)), DefaultValue(""), IDReferenceProperty, PersistenceMode(PersistenceMode.InnerProperty)]
        public string BackingControl
        {
            get
            {
                object obj = this.ViewState["BackingControl"];
                if (obj != null) {
                    return (string)obj;
                }
                return string.Empty;
            }
            set
            {
                this.ViewState["BackingControl"] = value;
            }
        }
        // [PersistenceMode(PersistenceMode.InnerProperty)]
        private Panel pnlRateIt;
        /// <summary></summary>
        public Panel PnlRateIt
        {
            get
            {
                if (pnlRateIt == null) {
                    pnlRateIt = new Panel();
                    Controls.Add(pnlRateIt);
                    pnlRateIt.ID = ID + "rateItPnl";
                }
                return pnlRateIt;
            }
            set
            {
                pnlRateIt = value;
            }
        }
        // [PersistenceMode(PersistenceMode.InnerProperty)]
        private DropDownList ddlRateIt;
        /// <summary></summary>
        public DropDownList DdlRateIt
        {
            get
            {
                if (ddlRateIt == null) {
                    ddlRateIt = new DropDownList();
                    Controls.Add(ddlRateIt);
                    ddlRateIt.ID = ID + "rateItHddn";
                }
                return ddlRateIt;
            }
            set
            {
                ddlRateIt = value;
            }
        }
        #endregion Properties
    }

    /// <summary>Rating control</summary>
    [DefaultProperty("Value"), ToolboxData("<{0}:RateIt runat=server></{0}:RateIt>")]
    public class RateIt : CompositeControl
    {
        /// <summary>Constructor</summary>
        public RateIt()
        {
            AutoPostBack = false;
        }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            if (Step == 0) Step = 1;

            if (PnlRateIt == null) PnlRateIt = new Panel();
            if (DdlRateIt == null) DdlRateIt = new DropDownList();
            DdlRateIt.AutoPostBack = AutoPostBack;
            base.CreateChildControls();
        }
        /// <inheritdoc/>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (Page.IsPostBack) {
                var postVal = Page.RequestControl(DdlRateIt);
                if (postVal.Is().NotEmpty) Value = Convert.ToSingle(postVal);
                //Value = Convert.ToSingle(DdlRateIt.SelectedValue);
            }
        }
        /// <inheritdoc/>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!Page.IsPostBack) {
                for (float i = MinValue; i <= MaxValue; i = i + Step)
                    DdlRateIt.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DdlRateIt.Items.FindByValue(Value.ToString()).Selected = true;
            DdlRateIt.SelectedIndexChanged += new EventHandler(ddl_SelectedIndexChanged);
        }

        private void ddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            Value = Convert.ToSingle(DdlRateIt.SelectedValue);
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            PnlRateIt.Attributes["data-rateit-backingfld"] = "#" + DdlRateIt.ClientID;
            PnlRateIt.Attributes["data-rateit-min"] = MinValue.ToString();
            PnlRateIt.Attributes["data-rateit-max"] = MaxValue.ToString();
            PnlRateIt.Attributes["data-rateit-value"] = Value.ToString();
            PnlRateIt.Attributes["data-rateit-step"] = Step.ToString();
            PnlRateIt.Attributes["data-rateit-readonly"] = Readonly ? "true" : "false";
            PnlRateIt.Attributes["data-rateit-resetable"] = Resetable ? "true" : "false";
            if (StarWidth > 0) PnlRateIt.Attributes["data-rateit-starwidth"] = StarWidth.ToString();
            if (StarHeight > 0) PnlRateIt.Attributes["data-rateit-starheight"] = StarHeight.ToString();

            if (CssRateIt.Is().Empty) {
                CssRateIt = "rateit";
                string cssUrl = this.Page.ClientScript.GetWebResourceUrl(typeof(RateIt), "Z.Util.Styles.rateit.css");
                HtmlLink cssLink = new HtmlLink();
                cssLink.Href = cssUrl;
                cssLink.Attributes.Add("rel", "stylesheet");
                cssLink.Attributes.Add("type", "text/css");
                Page.Header.Controls.Add(cssLink);
            }
            PnlRateIt.CssClass = CssRateIt + " " + CssClass;
            //Need testing for non-default css class
            var script = new StringBuilder();
            script.AppendString("$(document).ready(function (){ ",
             "$('#", PnlRateIt.ClientID, "').rateit({",
             "max: ", MaxValue.ToString());
            /*", step: " + Step;
            if (BackingControl.Is().NotEmpty)
            {
                var c = FindControl(BackingControl);
                if (c != null) script += ", backingfld: '#" + c.ClientID + "' ";
            }*/
            script.AppendString("});\n\r");
            if (AutoPostBack) {
                script.AppendString("$('#", PnlRateIt.ClientID, "').bind('rated', function() { __doPostBack('" + Page.RequestName(DdlRateIt) + "',''); });\n\r");
                script.AppendString("$('#", PnlRateIt.ClientID, "').bind('reset', function() { __doPostBack('" + Page.RequestName(DdlRateIt) + "',''); });\n\r");
            }
            script.AppendString("});\n\r");

            if (this.IsInsideUpdatePanel()) {
                //ScriptManager.RegisterClientScriptResource(this, typeof(RateIt), "Z.Util.Scripts.jquery.min.js");
                ScriptManager.RegisterClientScriptResource(this, typeof(RateIt), "Z.Util.Scripts.jquery.rateit.min.js");
                ScriptManager.RegisterStartupScript(this, typeof(RateIt), this.ID, script.ToString(), true);
            }
            else {
                //Page.ClientScript.RegisterClientScriptResource(typeof(RateIt), "Z.Util.Scripts.jquery.min.js");
                Page.ClientScript.RegisterClientScriptResource(typeof(RateIt), "Z.Util.Scripts.jquery.rateit.min.js");
                Page.ClientScript.RegisterStartupScript(typeof(RateIt), this.ID, script.ToString(), true);
            }
        }
        #endregion Control Code

        #region Properties
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(0)]
        public float MinValue { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(5)]
        public float MaxValue { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(0)]
        public float Value { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(1)]
        public float Step { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(false)]
        public bool Readonly { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(true)]
        public bool Resetable { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public int StarWidth { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public int StarHeight { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue("rateIt")]
        public string CssRateIt { get; set; }

        // [PersistenceMode(PersistenceMode.InnerProperty)]
        private Panel pnlRateIt;
        /// <summary></summary>
        public Panel PnlRateIt
        {
            get
            {
                if (pnlRateIt == null) {
                    pnlRateIt = new Panel();
                    Controls.Add(pnlRateIt);
                    pnlRateIt.ID = ID + "rateItPnl";
                }
                return pnlRateIt;
            }
            set
            {
                pnlRateIt = value;
            }
        }
        // [PersistenceMode(PersistenceMode.InnerProperty)]
        private DropDownList ddlRateIt;
        /// <summary></summary>
        public DropDownList DdlRateIt
        {
            get
            {
                if (ddlRateIt == null) {
                    ddlRateIt = new DropDownList();
                    Controls.Add(ddlRateIt);
                    ddlRateIt.ID = ID + "rateItHddn";
                }
                return ddlRateIt;
            }
            set
            {
                ddlRateIt = value;
            }
        }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public bool AutoPostBack { get; set; }
        #endregion Properties
    }
}