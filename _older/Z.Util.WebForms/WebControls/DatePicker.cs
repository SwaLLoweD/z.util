/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Z.Extensions;

//Time Picker Part requires jquery-ui-timepicker-addon.js
namespace Z.Util.WebControls
{
    /// <summary>Date / DateTime Picker</summary>
    [DefaultProperty("SelectedDate"), ToolboxData("<{0}:DatePicker runat=server></{0}:DatePicker>")]
    public class DatePicker : TextBox
    {
        /// <summary>Constructor</summary>
        public DatePicker()
        {
            EnableTimePicker = false;
        }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }
        private string listToJsArray(ICollection<DateTime> dates)
        {
            if (dates == null || dates.Count == 0) return null;
            var lstDateString = new List<string>(dates.Count);
            dates.ForEach(x => lstDateString.Add("'" + x.ToLocalTime().ToString("d", SysInf.InvariantFormat) + "'"));
            return String.Join(",", lstDateString.ToArray());
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            var enableDays = listToJsArray(DatesEnabled);
            var disableDays = listToJsArray(DatesDisabled);
            var dateFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToLowerInvariant().Replace("yyyy", "yy");
            var timeFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern.ToLowerInvariant().Replace("tt", "TT");
            var amPm = timeFormat.Contains("TT") ? "true" : "false";
            var jqPlugin = EnableTimePicker ? ".datetimepicker" : ".datepicker";
            var jqOptions = new StringBuilder();
            jqOptions.AppendString("dateFormat: '", dateFormat, "', ");
            if (EnableTimePicker) jqOptions.AppendString("timeFormat: '", timeFormat, "', ampm: ", amPm, ", ");
            //if (AutoPostBack) jqOptions.AppendString("onSelect: __doPostBack('" + Page.RequestName(this) + "',''), ");

            var script = @"
            var enableDays = [" + enableDays + @"];
            var disableDays = [" + disableDays + @"];
            function dayEnabled(date) {
                if(enableDays.length == 0 && disableDays.length == 0) return [true];
                var toSearch = $.datepicker.formatDate('mm/dd/yy', date);
                if(enableDays.length == 0 && disableDays.length > 0) return [($.inArray(toSearch, disableDays) == -1)];
                return [($.inArray(toSearch, enableDays) > -1 && $.inArray(toSearch, disableDays) == -1)];
            }
            $(document).ready(function () {
                $('#" + ClientID + @"')" + jqPlugin + @"({"
                   + jqOptions + @"beforeShowDay: dayEnabled
                });
            });";

            if (this.IsInsideUpdatePanel()) {
                //ScriptManager.RegisterClientScriptResource(this, typeof(RateIt), "Z.Util.Scripts.jquery.min.js");
                //ScriptManager.RegisterClientScriptResource(this, typeof(RateIt), "Z.Util.Scripts.jquery.rateit.min.js");
                ScriptManager.RegisterStartupScript(this, typeof(DatePicker), this.ID, script, true);
            }
            else {
                //Page.ClientScript.RegisterClientScriptResource(typeof(RateIt), "Z.Util.Scripts.jquery.min.js");
                //Page.ClientScript.RegisterClientScriptResource(typeof(RateIt), "Z.Util.Scripts.jquery.rateit.min.js");
                Page.ClientScript.RegisterStartupScript(typeof(DatePicker), this.ID, script, true);
            }
            base.OnPreRender(e);
        }
        #endregion Control Code

        #region Properties
        /// <summary>Current Value</summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true)]
        public DateTime? Value
        {
            get
            {
                if (Text.Is().NotEmpty) {
                    DateTime rval;
                    if (DateTime.TryParse(Text, System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeLocal, out rval))
                        return rval;
                }
                return null;
            }
            set
            {
                if (value.HasValue) {
                    var valDate = value.Value.ToLocalTime();
                    Text = EnableTimePicker ?
                        valDate.ToShortDateString() + " " + valDate.ToShortTimeString()
                        : valDate.ToShortDateString();
                }
            }
        }
        /// <summary>Enabled Dates (Default=All Enabled)</summary>
        public ISet<DateTime> DatesEnabled
        {
            get { return (ViewState["DatesEnabled"] as ISet<DateTime>) ?? new HashSet<DateTime>(); }
            set { ViewState["DatesEnabled"] = value; }
        }
        //[Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true)]
        /// <summary>Disabled Dates (Default=None Disabled)</summary>
        public ISet<DateTime> DatesDisabled
        {
            get { return (ViewState["DatesDisabled"] as ISet<DateTime>) ?? new HashSet<DateTime>(); }
            set { ViewState["DatesDisabled"] = value; }
        }
        /// <summary>Show Time picker portion (Requires related plugin-js)</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public bool EnableTimePicker { get; set; }
        #endregion Properties
    }
}