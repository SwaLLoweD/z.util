/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.ComponentModel;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Z.Extensions;

namespace Z.Util.WebControls
{
    /// <summary>IP Address Box</summary>
    [DefaultProperty("TextBox"), ToolboxData("<{0}:IpBox2 runat=server></{0}:IpBox2>")]
    public class IpBox2 : CompositeControl
    {
        private CustomValidator validator = new CustomValidator();

        /// <summary>Constructor</summary>
        public IpBox2()
        {
            TxtBox = new TextBox();
            TxtBox.ID = ID + "txt";
            TxtBox.AutoPostBack = true;

            validator.ID = ID + "vdt";
            validator.ControlToValidate = TxtBox.ID;
            validator.ErrorMessage = "{SK_Invalid} {SK_Value}";
            validator.ServerValidate += new ServerValidateEventHandler(validator_ServerValidate);
        }

        private void validator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //validator.Validate();
        }

        private void validator_Validate()
        {
            if (TxtBox.Text.Is().Empty) return;
            IPAddress ip = null;
            if (!IPAddress.TryParse(TxtBox.Text, out ip)) validator.IsValid = false;
            if (ip != null) TxtBox.Text = ip.ToString();
        }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            Controls.Add(TxtBox);
            Controls.Add(validator);
            base.CreateChildControls();
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            validator_Validate();
            base.OnPreRender(e);
        }
        #endregion Control Code

        #region Properties
        /// <summary>Inner-Textbox</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public TextBox TxtBox { get; set; }
        /// <summary>Value</summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public IPAddress Ip
        {
            get
            {
                IPAddress adr = null;
                IPAddress.TryParse(TxtBox.Text, out adr);
                return adr;
            }
            set
            {
                TxtBox.Text = value == null ? null : value.ToString();
            }
        }
        #endregion Properties
    }
}