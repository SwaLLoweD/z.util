/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Securist.Util;
using System.Reflection;

/*
 * Missing Feature: Themes plugin is not supported (Because no theme files are included), instead uses theme-roller of jqueryui
 * */
[assembly: WebResource("Securist.Util.Scripts.jquery.jstree.js", "application/x-javascript")]

namespace Securist.Util.WebControls
{

    [DefaultProperty("TextBox"), ToolboxData("<{0}:JsTree runat=server></{0}:JsTree>")]
    public partial class JsTree : CompositeControl
    {
        private TextBox txtBox = new TextBox();
        private LiteralControl dataLt = new LiteralControl();
        private Literal br = new Literal();
        private Literal sp = new Literal();
        public event EventHandler SelectedIdChanged;
        public JsTree() { }
        #region Control Code
        protected override void CreateChildControls()
        {
            Controls.Add(TxtBox);
            Controls.Add(dataLt);
            br.Text = "<br/>";
            sp.Text = "&nbsp;";

            TxtBox.ID = "MDDTxtBox";
            txtBox.Text = SelectedId > -1 ? SelectedId.ToString() : null;
            txtBox.TextChanged += new EventHandler(txtBox_TextChanged);

            dataLt.ID = "DataLt";

            base.CreateChildControls();
        }
        private string getStringFor(JsTreeNode node)
        {
            var sb = new StringBuilder();
            sb.Append("<li id='" + node.Id + "'><a href='#'>" + node.Title + "</a>");

            if (node.Children.Count > 0) {
                sb.Append("<ul>");
                foreach (var child in node.Children)
                    sb.Append(getStringFor(child));

                sb.Append("</ul>");
            }
            sb.Append("</li>");

            return sb.ToString();
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var sbData = new StringBuilder();
            sbData.Append("<ul id=\"" + dataLt.ClientID + "\" class=\"mcdropdown_menu\">");
            Items.ForEach(item => sbData.Append(getStringFor(item)));
            sbData.Append("</ul>");
            dataLt.Text = sbData.ToString();

            var sb = new StringBuilder();
            sb.Append("{allowParentSelect: ");
            sb.Append(AllowParentSelect ? "true" : "false");
            string mcddOpts = sb.ToString();

            var JavaScriptFName = "Register_" + ClientID;

            var jstreePlugins = "";
            var JavaScriptCode =
                //" function " + JavaScriptFName + "() {" + SysInf.lf +
                @" $(document).ready(function (){
                $(""#{0}"").jstree(""#{0}"",
                 ""plugins"" : [ ""{1}"" ""ui"", ""themeroller"" ]{2} });
                 }); ".ToFormat(dataLt.ClientID, jstreePlugins, mcddOpts);

            if (this.IsInsideUpdatePanel())
            {
                ScriptManager.RegisterClientScriptResource(this, typeof(McDropDown), "Securist.Util.WebControls.Scripts.jquery.jstree.js");
                ScriptManager.RegisterStartupScript(this, typeof(McDropDown), this.ID, JavaScriptCode, true);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptResource(typeof(McDropDown), "Securist.Util.WebControls.Scripts.jquery.jstree.js");
                Page.ClientScript.RegisterStartupScript(typeof(McDropDown), this.ID, JavaScriptCode, true);
            }
        }
        #endregion
        #region Events
        void txtBox_TextChanged(object sender, EventArgs e)
        {
            int ind = -1;
            Int32.TryParse((sender as TextBox).Text, out ind);
            SelectedId = ind;
            if (SelectedIdChanged != null) SelectedIdChanged(this, e);
        }
        #endregion
        #region Properties
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public TextBox TxtBox
        {
            get { return txtBox; }
            set { txtBox = value; }
        }
        [PersistenceMode(PersistenceMode.InnerProperty)]
        private bool allowParentSelect = true;
        public bool AllowParentSelect
        {
            get { return allowParentSelect; }
            set { allowParentSelect = value; }
        }
        public IList<JsTreeNode> Items
        {
            get { return (IList<JsTreeNode>) (ViewState["Items"] ?? new List<JsTreeNode>()); }
            set { ViewState["Items"] = value; }
        }
        #endregion

        public enum Plugins : short
        {
            HTML_DATA,
            JSON_DATA,
            XML_DATA,
            Hotkeys,
            Sort,
            Search,
            Languages,

        }
        public class Checkbox : IJsTreeModule
        {
            public bool OverrideUI { get; set; }
            public bool OpenParentsOfCheckedNode { get; set; }
            public bool TwoState { get; set; }
            public bool RealCheckboxes { get; set; }
            //public TreeCheckbox RealCheckboxName { get; set; }
            //public List<string> CheckedNodes { get; set; }

            //public delegate CheckBox TreeCheckbox(string nodeId);
        }
        public class HtmlData : IJsTreeModule
        {
            public List<JsTreeNode> Data { get; set; }
            public string AjaxUrl { get; set; }

            public string GetInitJs() { return null; }
            public string GetConfigJs() { return null; }
        }
        public class JsonData : IJsTreeModule
        {
            public List<JsTreeNode> Data { get; set; }
            public string AjaxUrl { get; set; }
            public bool ProgressiveRender { get; set; }
            public bool ProgressiveUnload { get; set; }
            public bool CorrectState { get; set; }

            public string GetInitJs() { return null; }
            public string GetConfigJs() { return null; }
        }
        public interface IJsTreeModule
        {
            public string GetInitJs();
            public string GetConfigJs();

        }
        public class JsTreeNode
        {
            public string Title { get; set; }
            public string Id { get; set; }
            public string IconStyleOrUrl { get; set; }
            public bool IsOpen { get; set; }
            public bool CheckState { get; set; }
            public IList<JsTreeNode> Children { get; set; }
            public JsTreeNode Parent { get; set; }

            public JsTreeNode()
            {
                Children = new List<JsTreeNode>();
            }
        }
    }
}

