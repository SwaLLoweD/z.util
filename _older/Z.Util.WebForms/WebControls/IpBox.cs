/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.ComponentModel;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Z.Extensions;

namespace Z.Util.WebControls
{
    /// <summary>IP Address Box</summary>
    [DefaultProperty("TextBox"), ToolboxData("<{0}:IpBox runat=server></{0}:IpBox>")]
    public class IpBox : CompositeControl
    {
        /// <summary>Constructor</summary>
        public IpBox()
        {
            TxtBox = new TextBox();
            TxtBox.ID = "ipTxtBox";
        }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            Controls.Add(TxtBox);
            base.CreateChildControls();
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            TxtBox.Text = Ip == null ? null : Ip.ToString();

            base.OnPreRender(e);
            string script = "$(function(){ $('#" + TxtBox.ClientID + "').ipaddress({" +
                "cidr:" + (Cidr ? "true" : "false") +
                ", cssIpContainer: '" + (CssIpContainer ?? "ipContainer") +
                "', cssIpOctet: '" + (CssIpOctet ?? "ipOctet") +
                "', cssIpCidr: '" + (CssIpCidr ?? "ipCidr") +
                "'}); });";

            if (this.IsInsideUpdatePanel()) {
                //ScriptManager.RegisterClientScriptResource(this, typeof(IpBox), "Z.Util.Scripts.jquery.min.js");
                ScriptManager.RegisterClientScriptResource(this, typeof(IpBox), "Z.Util.Scripts.jquery.caret.js");
                ScriptManager.RegisterClientScriptResource(this, typeof(IpBox), "Z.Util.Scripts.jquery.ipaddress.js");
                ScriptManager.RegisterStartupScript(this, typeof(IpBox), this.ID, script, true);
            }
            else {
                //Page.ClientScript.RegisterClientScriptResource(typeof(IpBox), "Z.Util.Scripts.jquery.min.js");
                Page.ClientScript.RegisterClientScriptResource(typeof(IpBox), "Z.Util.Scripts.jquery.caret.js");
                Page.ClientScript.RegisterClientScriptResource(typeof(IpBox), "Z.Util.Scripts.jquery.ipaddress.js");
                Page.ClientScript.RegisterStartupScript(typeof(IpBox), this.ID, script, true);
            }
        }
        #endregion Control Code

        #region Properties
        /// <summary>Inner TextBox</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public TextBox TxtBox { get; set; }

        /// <summary>Value</summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public IPAddress Ip
        {
            get
            {
                IPAddress adr = null;
                IPAddress.TryParse(TxtBox.Text, out adr);
                return adr;
            }
            set
            {
                TxtBox.Text = value == null ? null : value.ToString();
            }
        }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue("ipContainer")]
        public string CssIpContainer { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue("ipOctet")]
        public string CssIpOctet { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue("ipCidr")]
        public string CssIpCidr { get; set; }
        /// <summary>Cidr entry is allowed or not</summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(false)]
        public bool Cidr { get; set; }
        #endregion Properties
    }
}