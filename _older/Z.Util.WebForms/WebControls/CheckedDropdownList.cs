/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Z.Extensions;

namespace Z.Util.WebControls
{
    /// <summary>DropDownList with checkboxes for multi-select</summary>
    [DefaultProperty("TextNoneSelected"), ToolboxData("<{0}:CheckedDropDownList runat=server></{0}:CheckedDropDownList>")]
    public class CheckedDropDownList : ListBox
    {
        private int[] selectedIndices;
        /// <summary>Constructor</summary>
        public CheckedDropDownList()
        {
            ShowHeader = false;
            SelectionMode = ListSelectionMode.Multiple;
        }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            var sb = new StringBuilder();
            sb.Append("header: ");
            if (ShowHeader) {
                if (TextHeader.Is().NotEmpty) sb.AppendString("'", TextHeader, "'");
                else sb.Append("true");
            }
            else sb.Append("false");

            if (TextNoneSelected.Is().NotEmpty) sb.AppendString(", noneSelectedText: '", TextNoneSelected, "'");
            if (TextSelected.Is().NotEmpty) sb.AppendString(", selectedText: '", TextSelected, "'");
            if (TextCheckAll.Is().NotEmpty) sb.AppendString(", checkAllText: '", TextCheckAll, "'");
            if (TextUncheckAll.Is().NotEmpty) sb.AppendString(", uncheckAllText: '", TextUncheckAll, "'");
            if (ScriptBeforeClose.Is().NotEmpty) sb.AppendString(", beforeclose: ", ScriptBeforeClose);

            var script = @"$(document).ready(function () { $('#" + ClientID + @"').multiselect({" + sb.ToString() + "}); });";
            if (this.IsInsideUpdatePanel()) {
                ScriptManager.RegisterStartupScript(this, typeof(CheckedDropDownList), this.ID, script.ToString(), true);
            }
            else {
                Page.ClientScript.RegisterStartupScript(typeof(CheckedDropDownList), this.ID, script.ToString(), true);
            }
            selectedIndices = GetSelectedIndices();
            base.OnPreRender(e);
        }
        /// <inheritdoc/>
        protected override void Render(HtmlTextWriter writer)
        {
            //WORKAROUND
            //TODO: Find what is causing multiple select values to be get lost between prerender and render
            var cnt = Items.Count;
            foreach (var selIndice in selectedIndices) {
                if (selIndice >= cnt) continue;
                Items[selIndice].Selected = true;
            }
            base.Render(writer);
        }
        #endregion Control Code

        #region Properties
        /// <summary>Show Header</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public bool ShowHeader { get; set; }
        /// <summary>Header Text</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string TextHeader { get; set; }
        /// <summary>None selected Text</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string TextNoneSelected { get; set; }
        /// <summary>Selected Text</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string TextSelected { get; set; }
        /// <summary>Check All Text</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string TextCheckAll { get; set; }
        /// <summary>Uncheck All Text</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string TextUncheckAll { get; set; }
        /// <summary>BeforeClose Client Script</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string ScriptBeforeClose { get; set; }
        #endregion Properties
    }
}