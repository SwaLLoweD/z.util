/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Z.Extensions;

namespace Z.Util.WebControls
{
    /// <summary>Collapsible Panel</summary>
    [DefaultProperty("TextBox"), ToolboxData("<{0}:CollapsiblePanel runat=server></{0}:CollapsiblePanel>")]
    public class CollapsiblePanel : Panel
    {
        /// <summary>Constructor</summary>
        public CollapsiblePanel() { }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            Control ctrlShow = null;
            if (ControlToShow.Is().NotEmpty) ctrlShow = Page.FindControlRecursive(ControlToShow);
            Control ctrlHide = null;
            if (ControlToHide.Is().NotEmpty) ctrlHide = Page.FindControlRecursive(ControlToHide);
            Control ctrlToggle = null;
            if (ControlToToggle.Is().NotEmpty) ctrlToggle = Page.FindControlRecursive(ControlToToggle);

            var scr = new StringBuilder();
            if (ctrlToggle != null || ctrlShow != null || ctrlHide != null)
                scr.Append("jQuery(document).ready(function(){");
            if (ctrlShow != null) {
                scr.Append("$('#" + ctrlShow.ClientID + "').click(function(){");
                scr.Append("CPUncollapse('#" + ClientID + "', " + (ShowAnimProperties ?? "null") + ", " + ShowAnimTimer + ");");
                scr.Append("}");
            }
            if (ctrlHide != null) {
                scr.Append("$('#" + ctrlHide.ClientID + "').click(function(){");
                scr.Append("CPCollapse('#" + ClientID + "', " + (HideAnimProperties ?? "null") + ", " + HideAnimTimer + ");");
                scr.Append("}");
            }
            if (ctrlToggle != null) {
                scr.Append("$('#" + ctrlToggle.ClientID + "').click(function(){");
                scr.Append("if ($('#" + ClientID + "').is(':visible')) {");
                scr.Append("CPCollapse('#" + ClientID + "', " + (HideAnimProperties ?? "null") + ", " + HideAnimTimer + ");");
                scr.Append("} else {");
                scr.Append("CPUncollapse('#" + ClientID + "', " + (ShowAnimProperties ?? "null") + ", " + ShowAnimTimer + ");");
                scr.Append("} });");
            }
            scr.Append("});");
            if (this.IsInsideUpdatePanel()) {
                //ScriptManager.RegisterClientScriptResource(this, typeof(CollapsiblePanel), "Z.Util.Scripts.jquery.min.js");
                ScriptManager.RegisterClientScriptResource(this, typeof(CollapsiblePanel), "Z.Util.Scripts.jquery.collapsiblepanel.js");
                ScriptManager.RegisterStartupScript(this, typeof(CollapsiblePanel), this.ID, scr.ToString(), true);
            }
            else {
                //Page.ClientScript.RegisterClientScriptResource(typeof(CollapsiblePanel), "Z.Util.Scripts.jquery.min.js");
                Page.ClientScript.RegisterClientScriptResource(typeof(CollapsiblePanel), "Z.Util.Scripts.jquery.collapsiblepanel.js");
                Page.ClientScript.RegisterStartupScript(typeof(CollapsiblePanel), this.ID, scr.ToString(), true);
            }
            base.OnPreRender(e);
        }
        #endregion Control Code

        #region Properties
        /// <summary>Show Animation Timer</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public int ShowAnimTimer { get; set; }
        /// <summary>Hide Animation Timer</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public int HideAnimTimer { get; set; }
        /// <summary>Show Animation properties ("{ opacity: 0.25, left: '+=50', height: 'toggle'}")</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string ShowAnimProperties { get; set; }
        /// <summary>Hide Animation properties</summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string HideAnimProperties { get; set; }
        /// <summary>Control to toggle on/off</summary>
        [TypeConverter(typeof(Control)), DefaultValue(""), IDReferenceProperty, PersistenceMode(PersistenceMode.InnerProperty)]
        public string ControlToToggle { get; set; }
        /// <summary>Control to hide</summary>
        [TypeConverter(typeof(Control)), DefaultValue(""), IDReferenceProperty, PersistenceMode(PersistenceMode.InnerProperty)]
        public string ControlToHide { get; set; }
        /// <summary>control to show</summary>
        [TypeConverter(typeof(Control)), DefaultValue(""), IDReferenceProperty, PersistenceMode(PersistenceMode.InnerProperty)]
        public string ControlToShow { get; set; }
        #endregion Properties
    }
}