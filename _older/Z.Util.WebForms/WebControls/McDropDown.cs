/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Z.Extensions;

/*
 * Missing Feature: Requires use of a basic (non-javascript) group selector input (not textbox)
 * */

namespace Z.Util.WebControls
{
    /// <summary>Hierarchial dropdown</summary>
    [DefaultProperty("TextBox"), ToolboxData("<{0}:McDropDown runat=server></{0}:McDropDown>")]
    public partial class McDropDown : CompositeControl
    {
        private TextBox txtBox = new TextBox();
        private LiteralControl dataLt = new LiteralControl();
        private Literal br = new Literal();
        private Literal sp = new Literal();

        /// <summary></summary>
        public event EventHandler SelectedIdChanged;

        /// <summary>Constructor</summary>
        public McDropDown()
        {
            AllowParentSelect = true;
            CssInclude = true;
            AutoPostBack = false;
        }

        #region Control Code
        /// <inheritdoc/>
        protected override void CreateChildControls()
        {
            Controls.Add(TxtBox);
            Controls.Add(dataLt);
            br.Text = "<br/>";
            sp.Text = "&nbsp;";

            TxtBox.ID = "MDDTxtBox";
            txtBox.Text = SelectedId != 0 ? SelectedId.ToString() : null;
            txtBox.AutoPostBack = AutoPostBack;
            txtBox.TextChanged += new EventHandler(txtBox_TextChanged);
            txtBox.Attributes.Add("defaultValue", SelectedId.ToString());

            dataLt.ID = "DataLt";
            base.CreateChildControls();
        }
        /// <inheritdoc/>
        protected override void OnLoad(EventArgs e)
        {
            EnsureChildControls();
            dataLt.Text = "<ul id=\"" + dataLt.ClientID + "\" class=\"mcdropdown_menu\">" + SysInf.lf
            + ListItemString
            + "</ul>" + SysInf.lf;
            base.OnLoad(e);
        }
        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (CssInclude) {
                string cssUrl = this.Page.ClientScript.GetWebResourceUrl(typeof(McDropDown), "Z.Util.Styles.jquery.mcdropdown.css");
                HtmlLink cssLink = new HtmlLink();
                cssLink.Href = cssUrl;
                cssLink.Attributes.Add("rel", "stylesheet");
                cssLink.Attributes.Add("type", "text/css");
                Page.Header.Controls.Add(cssLink);
            }

            var sb = new StringBuilder();
            sb.Append("{allowParentSelect: ");
            sb.Append(AllowParentSelect ? "true" : "false");

            if (AutoPostBack) {
                sb.Append(", select:");
                sb.Append(" function() { __doPostBack('" + Page.RequestName(txtBox) + "',''); }");
            }

            sb.Append("}");
            string mcddOpts = sb.ToString();

            //var JavaScriptFName = "Register_" + ClientID;

            var JavaScriptCode =
                //" function " + JavaScriptFName + "() {" + SysInf.lf +
                " $(document).ready(function (){" + SysInf.lf +
                " $(\"#" + txtBox.ClientID + "\").mcDropdown(\"#" + dataLt.ClientID + "\"," +
                " " + mcddOpts + ");" + SysInf.lf +
                " }); " + SysInf.lf;

            if (this.IsInsideUpdatePanel()) {
                //ScriptManager.RegisterClientScriptResource(this, typeof(McDropDown), "Z.Util.Scripts.jquery.min.js");
                ScriptManager.RegisterClientScriptResource(this, typeof(McDropDown), "Z.Util.Scripts.jquery.bgiframe.js");
                ScriptManager.RegisterClientScriptResource(this, typeof(McDropDown), "Z.Util.Scripts.jquery.mcdropdown.min.js");
                ScriptManager.RegisterStartupScript(this, typeof(McDropDown), this.ClientID, JavaScriptCode, true);
            }
            else {
                //Page.ClientScript.RegisterClientScriptResource(typeof(McDropDown), "Z.Util.Scripts.jquery.min.js");
                Page.ClientScript.RegisterClientScriptResource(typeof(McDropDown), "Z.Util.Scripts.jquery.bgiframe.js");
                Page.ClientScript.RegisterClientScriptResource(typeof(McDropDown), "Z.Util.Scripts.jquery.mcdropdown.min.js");
                Page.ClientScript.RegisterStartupScript(typeof(McDropDown), this.ClientID, JavaScriptCode, true);
            }
        }
        #endregion Control Code

        #region Events
        private void txtBox_TextChanged(object sender, EventArgs e)
        {
            int ind = 0;
            Int32.TryParse((sender as TextBox).Text, out ind);
            SelectedId = ind;
            if (SelectedIdChanged != null) SelectedIdChanged(this, e);
        }
        #endregion Events

        #region Properties
        /// <summary>Inner textbox</summary>
        public TextBox TxtBox
        {
            get { return txtBox; }
            set { txtBox = value; }
        }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(true)]
        public bool CssInclude { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public bool AutoPostBack { get; set; }
        /// <summary>Parents can be selected or not</summary>
        [PersistenceMode(PersistenceMode.Attribute)]
        public bool AllowParentSelect { get; set; }
        /// <summary></summary>
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string ListItemString
        {
            get { return (string)(ViewState["ListItemString"] ?? null); }
            set { ViewState["ListItemString"] = value; }
        }
        /// <summary></summary>
        [Bindable(true), DefaultValue(0), Localizable(true)]
        public int SelectedId
        {
            get
            {
                int retVal;

                if (!int.TryParse(txtBox.Text, out retVal))
                    return 0;

                return retVal;
            }
            set
            {
                txtBox.Text = value.ToString();
                txtBox.Attributes.Add("defaultValue", SelectedId.ToString());
            }
        }
        #endregion Properties
    }
}