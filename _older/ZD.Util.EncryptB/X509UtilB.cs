﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509.Extension;
using Z.Extensions;
using bc = Org.BouncyCastle.X509;
using Org.BouncyCastle.Crypto.Operators;

namespace Z.Cryptography
{
    /// <summary>Bouncy Castle encrypt library wrapper</summary>
    public class X509UtilB : X509Util
    {
        /// <summary></summary>
        public X509UtilB() { }

        /// <summary>Generate a X509 certificate and return raw data</summary>
        public MemoryStream GenerateAndExport(X509GenerationRequest request)
        {
            AsymmetricCipherKeyPair kp;
            bc.X509Certificate caCert;
            var newCert = generate(out kp, out caCert, request);

            var friendlyName = request.SubjectName.Replace("CN=", "");
            var chainCerts = new List<X509CertificateEntry>();

            var storeB = new Pkcs12StoreBuilder();
            var newStore = storeB.Build();

            var chain = new List<X509CertificateEntry>();
            chain.Add(new X509CertificateEntry(newCert));
            if (caCert != null) chain.Add(new X509CertificateEntry(caCert));

            //newStore.SetCertificateEntry(friendlyName, certEntry);

            // Add chain entries            
            //if (caCert != null) {
            //    var additionalCertsAsBytes = new List<byte[]>();
            //    additionalCertsAsBytes.Add(caCert.GetEncoded());
            //    var addicionalCertsAsX09Chain = buildCertificateChainBC(newCert.GetEncoded(), additionalCertsAsBytes);

            //    foreach (bc.X509Certificate addCertAsX09 in addicionalCertsAsX09Chain)
            //        chainCerts.Add(new X509CertificateEntry(addCertAsX09));
            //}

            newStore.SetKeyEntry(friendlyName, new AsymmetricKeyEntry(kp.Private), chain.ToArray());
            var certFile = new MemoryStream();
            newStore.Save(
                certFile,
                request.ExportPassword.To().Array(),
                new SecureRandom(new CryptoApiRandomGenerator())
                );
            return certFile;
        }

        public override X509Certificate2 Generate(X509GenerationRequest request)
        {
            var ms = GenerateAndExport(request);
            var cert = new X509Certificate2(ms.ReadAllBytes(), request.ExportPassword);
            return cert;
        }

        private bc.X509Certificate generate(out AsymmetricCipherKeyPair kp, out bc.X509Certificate caCert, X509GenerationRequest request)
        {
            IAsymmetricCipherKeyPairGenerator kpgen = getKeyPairGenerator(request.Algorithm, request.Strength);

            kp = kpgen.GenerateKeyPair();
            var gen = new bc.X509V3CertificateGenerator();

            caCert = null; 
            AsymmetricCipherKeyPair caKeys = null;
            if (request.IssuerCert != null) 
            {
                if (!request.IssuerCert.HasPrivateKey) throw new InvalidOperationException("Issuer Certificate does not contain private key data");
                caCert = DotNetUtilities.FromX509Certificate(request.IssuerCert);
                caKeys = DotNetUtilities.GetKeyPair(request.IssuerCert.PrivateKey);
            }

            var subjectName = new X509Name(request.SubjectName);
            var issuerName = request.IssuerCert == null ? subjectName : caCert.IssuerDN;
            var serialNo = Org.BouncyCastle.Math.BigInteger.ProbablePrime(120, new Random());

            // certificate fields
            gen.SetSerialNumber(serialNo);
            gen.SetSubjectDN(subjectName);
            gen.SetIssuerDN(issuerName);
            gen.SetNotAfter(request.ExpiryDate);
            gen.SetNotBefore(request.StartDate);
            //gen.SetSignatureAlgorithm(request.Algorithm.ToString());
            gen.SetPublicKey(kp.Public);

            // extended information
            gen.AddExtension(X509Extensions.AuthorityKeyIdentifier.Id, false, new AuthorityKeyIdentifierStructure(caCert == null ? kp.Public : caCert.GetPublicKey()));
            gen.AddExtension(X509Extensions.SubjectKeyIdentifier, false, new SubjectKeyIdentifierStructure(kp.Public));
            //new AuthorityKeyIdentifier(bc.SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(kp.Public), new GeneralNames(new GeneralName(certName)), serialNo);

            foreach (var keyUsage in request.Purposes)
                gen.AddExtension(X509Extensions.ExtendedKeyUsage.Id, false, new DerObjectIdentifier("1.3.6.1.5.5.7.3.{0}".ToFormat((byte)keyUsage)));
            //gen.AddExtension(X509Extensions.ExtendedKeyUsage.Id, false, new ExtendedKeyUsage(KeyPurposeID.IdKPServerAuth)

            //Cert gen
            ISignatureFactory signatureFactory = new Asn1SignatureFactory(request.Algorithm.ToString(), caKeys == null ? kp.Private : caKeys.Private);
            var newCert = gen.Generate(signatureFactory);
            return newCert;
        }
        private IAsymmetricCipherKeyPairGenerator getKeyPairGenerator(SignatureAlgorithms alg, int strength)
        {
            IAsymmetricCipherKeyPairGenerator kpgen = null;
            var algStr = alg.ToString();
            if (algStr.EndsWith("WithGOST3410")) {
                kpgen = new Gost3410KeyPairGenerator();
                var gGen = new Gost3410ParametersGenerator();
                gGen.Init(strength, 1, new SecureRandom(new CryptoApiRandomGenerator()));
                kpgen.Init(new Gost3410KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), gGen.GenerateParameters()));
            }
            else if (algStr.EndsWith("WithECDSA")) {
                kpgen = new ECKeyPairGenerator();
                //var ecSpec = Org.BouncyCastle.Asn1.X9.ECNamedCurveTable.GetByName("");
                kpgen.Init(new ECKeyGenerationParameters(Org.BouncyCastle.Asn1.X9.X962NamedCurves.GetOid("prime256v1"), new SecureRandom(new CryptoApiRandomGenerator())));
            }
            else if (algStr.EndsWith("WithDSA")) {
                kpgen = new DsaKeyPairGenerator();
                var dGen = new DsaParametersGenerator();
                dGen.Init(strength, 80, new SecureRandom(new CryptoApiRandomGenerator()));
                kpgen.Init(new DsaKeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), dGen.GenerateParameters()));
            }
            else {
                kpgen = new RsaKeyPairGenerator();
                kpgen.Init(new KeyGenerationParameters(new SecureRandom(new CryptoApiRandomGenerator()), strength));
            }
            return kpgen;
        }

        /*private static IList buildCertificateChainBC(byte[] primary, IEnumerable<byte[]> additional)
        {
            var parser = new bc.X509CertificateParser();
            var builder = new Org.BouncyCastle.Pkix.PkixCertPathBuilder();

            // Separate root from itermediate
            var intermediateCerts = new List<Org.BouncyCastle.X509.X509Certificate>();
            var rootCerts = new Org.BouncyCastle.Utilities.Collections.HashSet();

            foreach (byte[] cert in additional) {
                var x509Cert = parser.ReadCertificate(cert);

                // Separate root and subordinate certificates
                if (x509Cert.IssuerDN.Equivalent(x509Cert.SubjectDN))
                    rootCerts.Add(new Org.BouncyCastle.Pkix.TrustAnchor(x509Cert, null));
                else
                    intermediateCerts.Add(x509Cert);
            }

            // Create chain for this certificate
            var holder = new bc.Store.X509CertStoreSelector();
            holder.Certificate = parser.ReadCertificate(primary);

            // WITHOUT THIS LINE BUILDER CANNOT BEGIN BUILDING THE CHAIN
            intermediateCerts.Add(holder.Certificate);

            var builderParams = new Org.BouncyCastle.Pkix.PkixBuilderParameters(rootCerts, holder);
            builderParams.IsRevocationEnabled = false;

            var intermediateStoreParameters = new bc.Store.X509CollectionStoreParameters(intermediateCerts);

            builderParams.AddStore(bc.Store.X509StoreFactory.Create("Certificate/Collection", intermediateStoreParameters));

            var result = builder.Build(builderParams);

            return result.CertPath.Certificates;
        }*/

        /// <summary>Sign with X509 certificate</summary>
        /// <param name="b">Data to sign</param>
        /// <param name="cert">X509 certificate that has its private key set</param>
        /// <remarks>Private key of the used X509Ceritificate must be set</remarks>
        public static new byte[] Sign(byte[] b, X509Certificate2 cert)
        {
            //var bcert = DotNetUtilities.FromX509Certificate(cert);
            ISigner sig = SignerUtilities.GetSigner(new DerObjectIdentifier(cert.SignatureAlgorithm.Value));

            var pk = cert.PrivateKey;
            sig.Init(true, DotNetUtilities.GetKeyPair((System.Security.Cryptography.RSACryptoServiceProvider)cert.PrivateKey).Private);
            /* Calc the signature */
            sig.BlockUpdate(b, 0, b.Length);
            byte[] signature = sig.GenerateSignature();

            return signature;
        }
        /// <summary>Verify signature with X509 certificate</summary>
        /// <param name="b">Signed Data</param>
        /// <param name="signature">Signature to verify</param>
        /// <param name="cert">X509 certificate</param>
        /// <returns>True if signature is authentic, false if not</returns>
        public static new bool SignVerify(byte[] b, byte[] signature, X509Certificate2 cert)
        {
            var c = DotNetUtilities.FromX509Certificate(cert);
            ISigner sig = SignerUtilities.GetSigner(new DerObjectIdentifier(cert.SignatureAlgorithm.Value));
            sig.Init(false, c.GetPublicKey());

            sig.BlockUpdate(b, 0, b.Length);
            return sig.VerifySignature(signature);
        }
    }
}