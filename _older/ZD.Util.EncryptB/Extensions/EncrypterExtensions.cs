﻿using Z.Cryptography;
using Z.Util;

namespace Z.Extensions
{
    /// <summary>Extension methods for Encrypter</summary>
    public static partial class ExtBouncyEncrypter
    {
        /// <summary>Bouncy Castle AES Encryption.(Key sizes: 0 to 256, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_Aes(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyAes(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Rijndael Encryption. (Key sizes: 0 to 256, Block sizes: 128, 160, 192, 224, 256)</summary>
        public static EncryptBouncyBase BC_Rijndael(this Encrypter enc, int keySize = 128, int blockSize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyRijndael(enc, keySize, blockSize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle RC2 Encryption. (Key sizes: 8-1024, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_RC2(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyRC2(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle 3DES Encryption. (Key sizes: 128, 192, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_TDES(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyTDES(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle DES Encryption. (Key sizes: 64, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_DES(this Encrypter enc, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyDES(enc, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Twofish Encryption.(Key sizes: 128, 192, 256, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_Twofish(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyTwofish(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Blowfish Encryption. (Key sizes: 32-448, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_Blowfish(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyBlowfish(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Camellia Encryption.(Key sizes: 128, 192, 256, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_Camellia(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyCamellia(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle CAST5 Encryption. (Key sizes: 0-128, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_CAST5(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyCAST5(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle CAST6 Encryption. (Key sizes: 0-256, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_CAST6(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyCAST6(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle GOST28147 Encryption. (Key sizes: 256, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_GOST28147(this Encrypter enc, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyGOST28147(enc, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle IDEA Encryption. (Key sizes: 128, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_IDEA(this Encrypter enc, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyIDEA(enc, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Noekeon Encryption. (Key sizes: 128, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_Noekeon(this Encrypter enc, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyNoekeon(enc, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle RC5 32 Encryption. (Key sizes: 0-128, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_RC532(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyRC532(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle RC5 64 Encryption. (Key sizes: 0-128, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_RC564(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyRC564(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle RC6 Encryption. (Key sizes: 0-256, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_RC6(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyRC6(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Seed Encryption. (Key sizes: 128, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_Seed(this Encrypter enc, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncySeed(enc, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Serpent Encryption.(Key sizes: 128, 192, 256, Block sizes: 128)</summary>
        public static EncryptBouncyBase BC_Serpent(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncySerpent(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Skipjack Encryption. (Key sizes: 0-128, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_Skipjack(this Encrypter enc, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncySkipjack(enc, keySize, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle Tea Encryption. (Key sizes: 128, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_Tea(this Encrypter enc, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyTea(enc, feedbackSize, salt, padding, mode); }
        /// <summary>Bouncy Castle XTEA Encryption. (Key sizes: 128, Block sizes: 64)</summary>
        public static EncryptBouncyBase BC_XTEA(this Encrypter enc, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
        { return new BouncyXTEA(enc, feedbackSize, salt, padding, mode); }


        /// <summary>Bouncy Castle RSA Encryption.</summary>
        public static BouncyRSA BC_RSA(this Encrypter enc) { return new BouncyRSA(enc); }
        /// <summary>Bouncy Castle X509 Certificate tools.</summary>
        public static X509UtilB BC_X509(this Encrypter enc) { return new X509UtilB(); }
    }
}