﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;
using Z.Collections.Generic;
using Z.Util;
using Z.Extensions;
using System.Linq;

namespace Z.Cryptography
{
    /// <summary>Base Encrpytor</summary>
    public abstract class EncryptBouncyBase : EncryptBase
    {
        private static readonly IEnumerable<byte> tableName = new byte[] { 0xFD, 0x02 };
        /// <inheritdoc/>
        public override SymmetricAlgorithm Algorithm => null;

        /// <inheritdoc/>
        public abstract IBlockCipher Cipher { get; }

        /// <inheritdoc/>
        protected new virtual Encrypter helper { get; set; }

        /// <inheritdoc/>
        public EncryptBouncyBase(Encrypter e, int keySize, int blockSize, int feedbackSize, byte[] salt, CipherPadding padding, CipherMode mode)
            : base(e, keySize, blockSize, feedbackSize, salt, padding, mode)
        {
            //if (e != null && e.CacheEncrypter.TryGetValue(tableName, null) == null) e.CacheEncrypter[tableName] = new Dictionary<Type, CacheTable2<byte[], object>>();
        }

        #region Helpers
        private PaddedBufferedBlockCipher getEncryptor(IBlockCipher enc, byte[] key)
        {
            //var tbl = helper.CacheEncrypter.TryGetValue(tableName, null);
            //var cache = tbl.TryGetValue(enc.GetType(), null);
            //if (cache == null) return null;
            //return cache.TryGetValue(key) as PaddedBufferedBlockCipher;

            var cacheKey = tableName.Concat(enc.GetType().Name.To().ByteArray.AsAscii).Concat(key).ToArray();
            var cache = helper.CacheEncrypter[cacheKey] as PaddedBufferedBlockCipher;
            return cache;
        }

        private void setEncryptor(IBlockCipher enc, byte[] key, PaddedBufferedBlockCipher c)
        {
            //var tbl = helper.CacheEncrypter.TryGetValue(tableName);
            //var cache = tbl.TryGetValue(enc.GetType(), null);
            //if (cache == null) cache = tbl[enc.GetType()] = new CacheTable2<byte[], object>(helper.CacheTtl);
            //cache[key] = c;
            var cacheKey = tableName.Concat(enc.GetType().Name.To().ByteArray.AsAscii).Concat(key).ToArray();
            helper.CacheEncrypter[cacheKey] = c;
        }

        private IBlockCipherPadding setPadding()
        {
            switch (Padding) {
                case CipherPadding.Default:
                case CipherPadding.PKCS7:
                    return new Pkcs7Padding();

                case CipherPadding.None:
                    return null; //Should be different cipher anyway
                case CipherPadding.ISO10126d2:
                    return new ISO10126d2Padding();

                case CipherPadding.X932:
                    return new X923Padding();

                case CipherPadding.ISO7816d4:
                    return new ISO7816d4Padding();

                case CipherPadding.ZeroByte:
                    return new ZeroBytePadding();

                default:
                    throw new Exception("Chosen padding type is not supported by this encryption algorithm.");
            }
        }
        #endregion Helpers

        #region cryptoSym
        private byte[] cryptoSym(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            switch (Mode) {
                case CipherMode.Default:
                    return cryptoPadded(enc, b, key, iv, encrypt);

                case CipherMode.CBC:
                    return cryptoCbc(enc, b, key, iv, encrypt);

                case CipherMode.CCM:
                    return cryptoCcm(enc, b, key, iv, encrypt);

                case CipherMode.CFB:
                    return cryptoCfb(enc, b, key, iv, encrypt);

                case CipherMode.CTS:
                    return cryptoCts(enc, b, key, iv, encrypt);

                case CipherMode.EAX:
                    return cryptoEax(enc, b, key, iv, encrypt);

                case CipherMode.ECB:
                    return cryptoEcb(enc, b, key, iv, encrypt);

                case CipherMode.GCM:
                    return cryptoGcm(enc, b, key, iv, encrypt);

                case CipherMode.GOFB:
                    return cryptoGofb(enc, b, key, iv, encrypt);

                case CipherMode.OFB:
                    return cryptoOfb(enc, b, key, iv, encrypt);

                case CipherMode.OpenPGPCF:
                    return cryptoOpenPgpcf(enc, b, key, iv, encrypt);

                case CipherMode.SIC:
                    return cryptoSic(enc, b, key, iv, encrypt);

                default:
                    throw new Exception("Chosen mode is not supported by this encryption algorithm.");
            }
        }

        private byte[] cryptoPadded(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            if (Padding == CipherPadding.None) return cryptoEcb(enc, b, key, iv, encrypt);
            PaddedBufferedBlockCipher cipher = null;
            if (helper != null) cipher = getEncryptor(enc, key);
            if (cipher == null) cipher = new PaddedBufferedBlockCipher(enc, setPadding());
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = cipher.DoFinal(b);
            if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }

        private byte[] cryptoCbc(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            CbcBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new CbcBlockCipher(enc);
            cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[b.Length + (BlockSize / 8)];
            var len = cipher.ProcessBlock(b, 0, rval, 0);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoCcm(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            CcmBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new CcmBlockCipher(enc);
            cipher.Init(encrypt, new AeadParameters(new KeyParameter(key), 64, iv, new byte[] { }));
            var rval = new byte[cipher.GetOutputSize(b.Length)];
            var len = cipher.ProcessBytes(b, 0, b.Length, rval, 0);
            cipher.DoFinal(rval, len);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoCfb(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            CfbBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new CfbBlockCipher(enc, BlockSize);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[b.Length + (BlockSize / 8)];
            var len = cipher.ProcessBlock(b, 0, rval, 0);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoCts(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            CtsBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new CtsBlockCipher(enc);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[cipher.GetOutputSize(b.Length)];
            var len = cipher.ProcessBytes(b, 0, b.Length, rval, 0);
            cipher.DoFinal(rval, len);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoEax(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            EaxBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new EaxBlockCipher(enc);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[cipher.GetOutputSize(b.Length)];
            var len = cipher.ProcessBytes(b, 0, b.Length, rval, 0);
            cipher.DoFinal(rval, len);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoEcb(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            BufferedBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new BufferedBlockCipher(enc);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[cipher.GetOutputSize(b.Length)];
            var len = cipher.ProcessBytes(b, 0, b.Length, rval, 0);
            cipher.DoFinal(rval, len);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoGcm(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            GcmBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new GcmBlockCipher(enc);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[cipher.GetOutputSize(b.Length)];
            var len = cipher.ProcessBytes(b, 0, b.Length, rval, 0);
            cipher.DoFinal(rval, len);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoGofb(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            GOfbBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new GOfbBlockCipher(enc);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[b.Length + (BlockSize / 8)];
            var len = cipher.ProcessBlock(b, 0, rval, 0);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoOfb(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            OfbBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new OfbBlockCipher(enc, BlockSize);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[b.Length + (BlockSize / 8)];
            var len = cipher.ProcessBlock(b, 0, rval, 0);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoOpenPgpcf(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            OpenPgpCfbBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new OpenPgpCfbBlockCipher(enc);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[b.Length + (BlockSize / 8)];
            var len = cipher.ProcessBlock(b, 0, rval, 0);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        private byte[] cryptoSic(IBlockCipher enc, byte[] b, byte[] key, byte[] iv, bool encrypt)
        {
            SicBlockCipher cipher = null;
            //if (helper != null) cipher = getEncryptor(enc, key); //TODO: No caching right now
            if (cipher == null) cipher = new SicBlockCipher(enc);
            cipher.Init(encrypt, new KeyParameter(key));
            //cipher.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            var rval = new byte[b.Length + (BlockSize / 8)];
            var len = cipher.ProcessBlock(b, 0, rval, 0);
            //if (helper != null) setEncryptor(enc, key, cipher);
            return rval;
        }
        #endregion cryptoSym

        #region Encrypt
        /// <inheritdoc/>
        public override byte[] Encrypt(byte[] b, byte[] key, byte[] iv) => cryptoSym(Cipher, b, key, iv, true);

        /// <inheritdoc/>
        public override byte[] Decrypt(byte[] b, byte[] key, byte[] iv) => cryptoSym(Cipher, b, key, iv, false);
        #endregion Encrypt
    }
}