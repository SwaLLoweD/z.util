﻿/* Copyright (c) <2008> <A. Zafer YURDAÇALIŞ>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Z.Extensions;
using Z.Util;

namespace Z.Cryptography
{
    #region AES

    /// <summary>Bouncy Castle AES Encrpyter (Valid key sizes: 0 to 256)</summary>
    public class BouncyAes : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128, 192, 256 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new AesFastEngine(); } }

        /// <inheritdoc/>
        public BouncyAes(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion AES

    #region Rijndael

    /// <summary>Bouncy Castle Rijndael Encrpyter (Valid key sizes: 0 to 256)</summary>
    public class BouncyRijndael : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128, 160, 192, 224, 256 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new RijndaelEngine(); } }

        /// <inheritdoc/>
        public BouncyRijndael(Encrypter e = null, int keySize = 128, int blockSize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, blockSize, feedbackSize, salt, padding, mode) { }
    }

    #endregion Rijndael

    #region RC2

    /// <summary>Bouncy Castle RC2 Encrpytor (Valid key sizes: Between 40 and 1024 in increments of 8 bits)</summary>
    public class BouncyRC2 : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new RC2Engine(); } }

        /// <inheritdoc/>
        public BouncyRC2(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion RC2

    #region 3DES

    /// <summary>Bouncy Castle 3DES Encrpytor (Valid key sizes: 128, 192)</summary>
    public class BouncyTDES : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128, 192 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new DesEdeEngine(); } }

        /// <inheritdoc/>
        public BouncyTDES(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion 3DES

    #region DES

    /// <summary>Bouncy Castle DES Encrpyter (Valid key sizes: 64)</summary>
    public class BouncyDES : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new DesEngine(); } }

        /// <inheritdoc/>
        public BouncyDES(Encrypter e = null, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, 64, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion DES

    #region Twofish

    /// <summary>Bouncy Castle Twofish Encrpytor (Valid key sizes: 128, 192, 256)</summary>
    public class BouncyTwofish : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128, 192, 256 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new TwofishEngine(); } }

        /// <inheritdoc/>
        public BouncyTwofish(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion Twofish

    #region Blowfish

    /// <summary>Bouncy Castle Blowfish Encrpytor (Valid key sizes: 32 to 448)</summary>
    public class BouncyBlowfish : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new BlowfishEngine(); } }

        /// <inheritdoc/>
        public BouncyBlowfish(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion Blowfish

    #region Camellia

    /// <summary>Bouncy Castle Camellia Encrpyter (Valid key sizes: 128, 192, 256)</summary>
    public class BouncyCamellia : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128, 192, 256 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new CamelliaEngine(); } }

        /// <inheritdoc/>
        public BouncyCamellia(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion Camellia

    #region CAST5

    /// <summary>Bouncy Castle CAST5 Encrpyter (Valid key sizes: 0 to 128)</summary>
    public class BouncyCAST5 : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new Cast5Engine(); } }

        /// <inheritdoc/>
        public BouncyCAST5(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion CAST5

    #region CAST6

    /// <summary>Bouncy Castle CAST6 Encrpyter (Valid key sizes: 0 to 256)</summary>
    public class BouncyCAST6 : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new Cast6Engine(); } }

        /// <inheritdoc/>
        public BouncyCAST6(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion CAST6

    #region GOST28147

    /// <summary>Bouncy Castle GOST28147 Encrpyter (Valid key sizes: 256)</summary>
    public class BouncyGOST28147 : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 256 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new Gost28147Engine(); } }

        /// <inheritdoc/>
        public BouncyGOST28147(Encrypter e = null, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, 256, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion GOST28147

    #region IDEA

    /// <summary>Bouncy Castle IDEA Encrpyter (Valid key sizes: 128)</summary>
    public class BouncyIDEA : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new Gost28147Engine(); } }

        /// <inheritdoc/>
        public BouncyIDEA(Encrypter e = null, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, 128, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion IDEA

    #region Noekeon

    /// <summary>Bouncy Castle Noekeon Encrpyter (Valid key sizes: 128)</summary>
    public class BouncyNoekeon : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new NoekeonEngine(); } }

        /// <inheritdoc/>
        public BouncyNoekeon(Encrypter e = null, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, 128, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion Noekeon

    #region RC532

    /// <summary>Bouncy Castle RC532 Encrpyter (Valid key sizes: 0 to 128)</summary>
    public class BouncyRC532 : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new RC532Engine(); } }

        /// <inheritdoc/>
        public BouncyRC532(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion RC532

    #region RC564

    /// <summary>Bouncy Castle RC564 Encrpyter (Valid key sizes: 0 to 128)</summary>
    public class BouncyRC564 : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new RC564Engine(); } }

        /// <inheritdoc/>
        public BouncyRC564(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion RC564

    #region RC6

    /// <summary>Bouncy Castle RC6 Encrpyter (Valid key sizes: 0 to 256)</summary>
    public class BouncyRC6 : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new RC6Engine(); } }

        /// <inheritdoc/>
        public BouncyRC6(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion RC6

    #region Seed

    /// <summary>Bouncy Castle Seed Encrpyter (Valid key sizes: 128)</summary>
    public class BouncySeed : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new SeedEngine(); } }

        /// <inheritdoc/>
        public BouncySeed(Encrypter e = null, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, 128, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion Seed

    #region Serpent

    /// <summary>Bouncy Castle Serpent Encrpyter (Valid key sizes: 128, 192, 256)</summary>
    public class BouncySerpent : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128, 192, 256 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new SerpentEngine(); } }

        /// <inheritdoc/>
        public BouncySerpent(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 128, feedbackSize, salt, padding, mode) { }
    }

    #endregion Serpent

    #region Skipjack

    /// <summary>Bouncy Castle Skipjack Encrpyter (Valid key sizes: 0 to 128)</summary>
    public class BouncySkipjack : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return null; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new SkipjackEngine(); } }

        /// <inheritdoc/>
        public BouncySkipjack(Encrypter e = null, int keySize = 128, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, keySize, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion Skipjack

    #region Tea

    /// <summary>Bouncy Castle Tea Encrpyter (Valid key sizes: 128)</summary>
    public class BouncyTea : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new TeaEngine(); } }

        /// <inheritdoc/>
        public BouncyTea(Encrypter e = null, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, 128, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion Tea

    #region XTEA

    /// <summary>Bouncy Castle XTEA Encrpyter (Valid key sizes: 128)</summary>
    public class BouncyXTEA : EncryptBouncyBase
    {
        /// <inheritdoc/>
        public override int[] LegalKeySizes { get { return new int[] { 128 }; } }
        /// <inheritdoc/>
        public override int[] LegalBlockSizes { get { return new int[] { 64 }; } }
        /// <inheritdoc/>
        public override IBlockCipher Cipher { get { return new XteaEngine(); } }

        /// <inheritdoc/>
        public BouncyXTEA(Encrypter e = null, int feedbackSize = 0, byte[] salt = null, CipherPadding padding = CipherPadding.Default, CipherMode mode = CipherMode.Default)
            : base(e, 128, 64, feedbackSize, salt, padding, mode) { }
    }

    #endregion XTEA

    #region RSA

    /// <summary>Bouncy Castle RSA Encrpytor</summary>
    public class BouncyRSA
    {
        /// <summary></summary>
        public BouncyRSA(Encrypter enc) { }

        /// <summary>Encrypt with RSA</summary>
        /// <param name="b">Data to encrypt</param>
        /// <param name="rsa"></param>
        public byte[] Encrypt(byte[] b, RSA rsa) { return rsa.EncryptValue(b); }
        /// <summary>Decrypt with RSA</summary>
        /// <param name="b">Data to decrypt</param>
        /// <param name="rsa"></param>
        public byte[] Decrypt(byte[] b, RSA rsa) { return rsa.DecryptValue(b); }

        /// <summary>Encrypt with RSA</summary>
        /// <param name="b">Data to encrypt</param>
        /// <param name="cert">X509 certificate that has its private key set</param>
        /// <remarks>Private key of the used X509Ceritificate must be set</remarks>
        public byte[] Encrypt(byte[] b, X509Certificate2 cert) { return Encrypt(b, cert.PrivateKey as RSA); }
        /// <summary>Decrypt with RSA</summary>
        /// <param name="b">Data to decrypt</param>
        /// <param name="cert">X509 certificate</param>
        public byte[] Decrypt(byte[] b, X509Certificate2 cert) { return Decrypt(b, cert.PrivateKey as RSA); }

        /// <summary>Encrypt with RSA</summary>
        /// <param name="b">Data to encrypt</param>
        /// <param name="cert">X509 certificate that has its private key set</param>
        /// <remarks>Private key of the used X509Ceritificate must be set</remarks>
        public string Encrypt(string b, X509Certificate2 cert) { return Encrypt(b.To().ByteArray.AsUtf8, cert).To().String.AsBase64; }
        /// <summary>Decrypt with RSA</summary>
        /// <param name="b">Data to decrypt</param>
        /// <param name="cert">X509 certificate</param>
        public string Decrypt(string b, X509Certificate2 cert) { return Decrypt(b.To().ByteArray.AsBase64, cert).To().String.AsUtf8; }
    }

    #endregion RSA
}